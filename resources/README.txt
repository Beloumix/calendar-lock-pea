Calendar Lock PEA version 1.7 2023

Start the PEA:
Windows: just double click the file "calendar.jar",
if this does not work, try the helper file with extension ".bat",
Linux, BSD or MacOS: try to double click the helper file with extension ".sh" 
(make it executable).
You can always use the terminal/console: change into the directory of the PEA and type:
java -jar calendar.jar

Use the PEA:
Create a new calendar in FILE menu, type and retype the password.
Enter a calendar name and a color and choose the location to store the file 
when you save the content.
You will be asked to remember the file after closing. If you agree, 
the file will be shown the next time, you open the PEA. Otherwise you have to 
open it manually in the FILE menu. 
You can store your file in the cloud and load it when you open the PEA
in the FILE menu. Keep in mind, that the content is always encrypted, 
but at least the cloud provider gets a lot of meta data (e.g. file names...). 
If you want to open several files together: Create new files only if existing files 
are displayed in the password dialog. Otherwise the files cannot be opened together even 
with the same password (due to different salts).

You can find further informations at:
https://eck.cologne/peafactory/en/html/calendar_pea.html



Copyright (C) 2021  Axel von dem Bruch
 * This library is free software; you can redistribute it and/or modify it 
 * under the terms of the GNU General Public License as published 
 * by the Free Software Foundation; either version 2 of the License, 
 * or (at your option) any later version.
 * This library is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
 * or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU General Public License for more details.
 * See:  http://www.gnu.org/licenses/gpl-2.0.html
 * You should have received a copy of the GNU General Public License 
 * along with this library.


Used libraries:

Bouncy Castles Java Cryptography APIs
MIT-like license, see:  https://www.bouncycastle.org/licence.html
Copyright (c) 2000 - 2019 The Legion of the Bouncy Castle Inc. (https://www.bouncycastle.org)
Permission is hereby granted, free of charge, to any person obtaining a copy of this software 
and associated documentation files (the "Software"), to deal in the Software without restriction, 
including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, 
subject to the following conditions:
The above copyright notice and this permission notice shall be included in all copies or substantial 
portions of the Software.
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT 
LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. 
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, 
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE 
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. 

For cloud access: 
- oil-sardine, a fork from Grizzly Labs' fork of the Sardine WevDAV client library. 
- okhttp-3.14.0.jar
- okio-1.17.5.jar
- simple-xml-2.7.1.jar
Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at
   http://www.apache.org/licenses/LICENSE-2.0
Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.


