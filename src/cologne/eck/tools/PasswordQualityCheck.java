package cologne.eck.tools;

/*
 * Peafactory - Production of Password Encryption Archives
 * Copyright (C) 2015  Axel von dem Bruch
 * 
 * This library is free software; you can redistribute it and/or modify it 
 * under the terms of the GNU General Public License as published 
 * by the Free Software Foundation; either version 2 of the License, 
 * or (at your option) any later version.
 * This library is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
 * or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU General Public License for more details.
 * See:  http://www.gnu.org/licenses/gpl-2.0.html
 * You should have received a copy of the GNU General Public License 
 * along with this library.
 */

/**
 * Proactive password checker (password-strength meter).
 * This class can give feedback to users while they are typing a passwords. 
 * 
 * Note: This is a check 
 * without a large dictionary (only the worst 25 passwords, 
 *    12 most common English words and some related words),
 * without leet dictionaries (only one-to-one leet transformations 
 *    are targeted indirectly but for example not  \/ for v),
 * without sequences from keyboard layout, 
 * without repetitions of more than one character, 
 * without reversing and some other patterns.
 * 
 * This is not a perfect check, but it is intended to be a 
 * compromise between safety and performance.  
 * 
 * The bonus-malus system considers:
 * - length (number of characters)
 * - character-set complexity (mix of lower case, upper case, digit, symbol, character from tables) 
 * - containing of 25 worst passwords with leet transformations like replacing a by @ 
 * - containing most common English words in pass phrases, 
 * - space characters (used in pass phrases)
 * - containing of translations of the worst password in 10 common languages 
 * - containing of related words 
 * - prefixes and suffixes: digits, upper case, symbols, characters from tables 
 * - sequence of following (123 abc) and equal characters (xxx) 
 * 
 * This class does not use the password as String to avoid
 * vulnerability to RAM attacks
 */

/* Used in:
 * 			 cologne.eck.peafactory.gui.MainView.
 * 			 cologne.eck.peafactory.peas.gui.NewPasswordDialog
 * 
 *  Note: This is just a simple test. It should 
 *  give only a vague clue. 
 */

public class PasswordQualityCheck {
	
	// worst passwords from: https://github.com/danielmiessler/
	// passwords like "123456" or "abc123" or very short words (< 6)
	// are already marked as weak in the following method, 
	// so the international worst 27 passwords are checked
	// and the translation of "password" into the most common 
	// nativ speaken languages + french
	private static final char[][] worstPasswords = {
		"password".toCharArray(), "pussy".toCharArray(), 
		"dragon".toCharArray(), "qwerty".toCharArray(), 
		"696969".toCharArray(), "mustang".toCharArray(), 
		"letmein".toCharArray(), "baseball".toCharArray(), 
		"master".toCharArray(), "michael".toCharArray(), 
		"football".toCharArray(), "shadow".toCharArray(), 
		"monkey".toCharArray(), "pass".toCharArray(), 
		"fuckme".toCharArray(), "jordan".toCharArray(), 
		"harley".toCharArray(), "ranger".toCharArray()
	};
		
	// worst passwords from: https://github.com/danielmiessler/
	// passwords like "123456" or "abc123" or very short words (< 6)
	// are already marked as weak in the following method, 
	// so the international worst 27 passwords are checked
	// and the translation of "password" into the most common 
	// nativ speaken languages + french
	private static final char[][] worstTranslations = {	
		"密码".toCharArray(), // chinese: password
		"contraseña".toCharArray(), // spanish: password
		"पासवर्ड".toCharArray(), // hindi: password
		"كلمة المرور".toCharArray(), // arabic: password
		// "Senha" portugues, but less than 6 characters
		"পাসওয়ার্ড".toCharArray(), // bengali: password
		"пароль".toCharArray(), // russian: password
		"パスワード".toCharArray(), // japanese: password
		"ਪਾਸਵਰਡ".toCharArray(), // punjabi: password
		// "sandi" jawanese but less than 6 characters
		"passwort".toCharArray(), // german: password
		"mot de passe".toCharArray(), // french: password
	};
	
	/*
	 * Most common English words to check for pass phrases. 
	 */
	private static final char[][] mostCommonEnglishWords = {
		" the ".toCharArray(), " be ".toCharArray(), 
		" to ".toCharArray(), " of ".toCharArray(), 
		" and ".toCharArray(), " a ".toCharArray(), 
		" in ".toCharArray(), " that ".toCharArray(), 
		" have ".toCharArray(), "I ".toCharArray(), 
		" it ".toCharArray(), " for ".toCharArray(), 
	};
	
	private static char[][] relatedWords = null;

	/**
	 * Check the quality of a password
	 * 
	 * @param password	the password as an array of chars
	 * 
	 * @return			the quality, a numeric value that
	 * 					indicates the strength of the password, 
	 * 					higher values indicate stronger passwords
	 */
	public static int checkQuality (char[] password) {
				
		int len = password.length;
		if (len == 0) {
			return 0;
		}
			
		Double bonusPoints = (double) len;
		//Double timeConstantHelper = (double) len;
		
		if (len > 5 ) {
			// check 15 of the worst passwords:		
			// no upper case, leet transformations, 
			// suffixes and prefixes are checked
			// for these passwords
			int wpLen = worstPasswords.length;
			for (int i = 0; i < wpLen; i++) {
				int match = Comparator.containsWithExceptions(password, worstPasswords[i]);
				if (match < 3 ) {
					// give only number of leet transformations as points
					bonusPoints -= worstPasswords[i].length;
					bonusPoints += match;
				} else {
					//timeConstantHelper -= worstPasswords[i].length + match;
				}
			}			
			
			// check for translations of the worst password
			int wtLen = worstTranslations.length;
			for (int i = 0; i < wtLen; i++) {
				if (Comparator.containsArray(password, worstTranslations[i]) == true) {
					// give only one point for these words
					bonusPoints -= worstTranslations[i].length;
					bonusPoints++;
				} else {
					//timeConstantHelper -= mostCommonEnglishWords[i].length;
				}
			}			
			
			// check for pass phrases with most common English words
			if (mostCommonEnglishWords != null){
				int mcewLen = mostCommonEnglishWords.length;
				for (int i = 0; i < mcewLen; i++) {
					if (Comparator.containsArray(password, mostCommonEnglishWords[i]) == true) {
						// give only one point for these words
						bonusPoints -= mostCommonEnglishWords[i].length;
						bonusPoints++;
					} else {
						//timeConstantHelper -= mostCommonEnglishWords[i].length;
					}
				}
			}
			
			// check related words:		
			// no upper case, leet transformations, 
			// suffixes and prefixes are checked
			// for these words
			if (relatedWords != null){
				int rpLen = relatedWords.length;
				for (int i = 0; i < rpLen; i++) {
					if (Comparator.containsArray(password, relatedWords[i]) == true) {
						// only one point for these words
						bonusPoints -= relatedWords[i].length;
						bonusPoints++;
					} else {
						//timeConstantHelper -= relatedWords[i].length;
					}
				}
			}
		}
		
		int digit = 0;
		int upperCase = 0;
		int lowerCase = 0;
		int symbol = 0;
		int tableChar1 = 0;
		int tableChar2 = 0;
		int tableChar3 = 0;
		int tableCharX = 0;
		int space = 0; // extra check: this is already included in table1
		
		// true if only the first letter is upper case
		boolean prefixUppercase = false;
		// true if only the first characters are digits
		boolean prefixDigit = false;
		// true if only the first characters are symbols
		boolean prefixSymbol = false;
		// true if only the first characters are from character tables
		boolean prefixTable = false;
		// true if only the last characters are digits
		boolean suffixDigit = false;
		// true if only the last characters are symbols
		boolean suffixSymbol = false;
		// true if only the last characters are from tables
		boolean suffixTable = false;
	
		char c = '\0';
		
		// get some statistics about the password:
		// How many digits, upper case...
		for (int i = 0; i < password.length; i++) {

			c = password[i];
			
			if (c == 32) { // space
				space++;
			} else if (c > 47 && c < 58){ // 0-9
				// check if only the first characters are digits:
				if (i == 0) {
					prefixDigit = true;
				} else { // i > 0
					
					if (i > digit) {
						prefixDigit = false;
					}
				
					// check if only the last characters are digits:
					if (digit == 0) {
						suffixDigit = true;
					}
					// reset other suffixes:
					suffixSymbol = false;
					suffixTable = false;
				}
				// increment digit:
				digit++;
				
			} else if (c > 64 && c < 91) {// A-Z
				
				// check if only the first letter is upper case
				if (i == 0) {
					prefixUppercase = true;
				} else { // i > 0
					if (i > upperCase) {
						prefixUppercase = false;
					}
					suffixSymbol = false;
					suffixTable = false;
					suffixDigit = false;
				}
				upperCase++;
				
			} else if (c > 96 && c < 123) {// a - z
				
				if (i > 0) {
					suffixSymbol = false;
					suffixTable = false;
					suffixDigit = false;	
				}				
				lowerCase++;
				
			} else if (c < 128) { // ~[{(^|@?... special character

				if (i == 0) {
					prefixSymbol = true;					
				} else { // i > 0
				
					if (i > symbol) {
						prefixSymbol = false;
					}
					if (symbol == 0) {
						suffixSymbol = true;
					}
					suffixTable = false;
					suffixDigit = false;				
				}				
				symbol++;
				
			} else if (c > 128 && c < 50072 ){ // c >= 128: character from table

				if (i == 0) {
					prefixTable = true;
				} else { // i > 0
					int tableSum = tableChar1 + tableChar2 + tableChar3 + tableCharX;
					
					if (i > tableSum) {
						prefixTable = false;
					}
					
					if (tableSum == 0 ) {
						suffixTable = true;
					}
					suffixSymbol = false;
					suffixDigit = false;
				}
				tableChar1++;				
								
			} else if (c >= 50072 && c < 50606) { // table 2

				if (i == 0) {
					prefixTable = true;
				} else { // i > 0
					int tableSum = tableChar1 + tableChar2 + tableChar3 + tableCharX;
					
					if (i > tableSum) {
						prefixTable = false;
					}
					
					if (tableSum == 0 ) {
						suffixTable = true;
					}
					suffixSymbol = false;
					suffixDigit = false;
				}
				tableChar2++;
				
			} else if (c >= 50606 && c < 51341) { // table 3

				if (i == 0) {
					prefixTable = true;
				} else { // i > 0
					int tableSum = tableChar1 + tableChar2 + tableChar3 + tableCharX;
					
					if (i > tableSum) {
						prefixTable = false;
					}
					
					if (tableSum == 0 ) {
						suffixTable = true;
					}
					suffixSymbol = false;
					suffixDigit = false;
				}
				tableChar3++;
								
			} else { // table > 3

				if (i == 0) {
					prefixTable = true;
				} else { // i > 0
					int tableSum = tableChar1 + tableChar2 + tableChar3 + tableCharX;
					
					if (i > tableSum) {
						prefixTable = false;
					}
					
					if (tableSum == 0 ) {
						suffixTable = true;
					}
					suffixSymbol = false;
					suffixDigit = false;
				}
				tableCharX++;
			}
		}
		c = '\0';
		
		//-------------------------------
		// Bonus Points and Malus Points:
		//
		
		//System.out.println("1: " + bonusPoints);
			
		// check if only one sort: only digit or only...
		if (digit == len || lowerCase == len || upperCase == len) {
			bonusPoints /= 1.25;
		} else {		
			// check variety:
			if (digit > 0){
				bonusPoints++;
			}
			if (upperCase > 0) {
				bonusPoints++;
			}
			if (lowerCase > 0) {
				bonusPoints++;
			}
			if (symbol > 0) {
				bonusPoints += symbol;
			}
			// half points for space characters (used in pass phrases)
			bonusPoints -= space/2;
		}
		
		// check characters from tables: 
		if (tableChar1 > 0) {
			bonusPoints += tableChar1 + 2;
		}
		if (tableChar2 > 0) {
			bonusPoints += tableChar2 + 4;
		}
		if (tableChar3 > 0) {
			bonusPoints += tableChar3 + 6;
		}
		if (tableCharX > 0) {
			bonusPoints += tableCharX + 8;
		}
		
		//System.out.println("2: " + bonusPoints);
		
		// check succession: for example abcde or 1234
		// and equality: for example AAAAAAAAA
		// Decrement 1/2 for each
		int previous = 0;
		for (int i = 0; i < password.length; i++) {
			c = password[i];
			if (c == previous || c == previous+1) {
				bonusPoints = bonusPoints - 0.5;
			}
			previous = c;				
		}
		c = '\0';
		
		//System.out.println("3: " + bonusPoints);

		if (prefixUppercase == true) {
			bonusPoints--;
		}
		if (prefixDigit == true) {
			bonusPoints--;
		}
		if (prefixSymbol == true) {
			bonusPoints--;
		}
		if (prefixTable == true) {
			bonusPoints--;
		}
		if (suffixDigit == true) {
			bonusPoints--;
		}
		if (suffixSymbol == true) {
			bonusPoints--;
		}
		if (suffixTable == true) {
			bonusPoints--;
		}
/*		System.out.println("prefixUppercase: " + prefixUppercase + "\n" 
				+ "prefixDigit: " + prefixDigit + "\n"
				+ "prefixSymbol: " + prefixSymbol + "\n"
				+ "prefixTable: " + prefixTable + "\n"
				+ "sufffixDigit: " + suffixDigit + "\n"
				+ "suffixSymbol: " + suffixSymbol + "\n"
				+ "suffixTable: " + suffixTable + "\n"				
				);		
		System.out.println("4: " + bonusPoints); */
		
		if (bonusPoints < 1) {
			bonusPoints = 1.0;
		}
		
		return bonusPoints.intValue();
	}
	
	// for testing only:
/*	public static void main(String[] args) {
		
		System.out.println( " ".toCharArray()[0]);
		String s = "ø()166AxfP";
		int x = checkQuality(s.toCharArray() );
		System.out.println("Quality: " + x);
	} */


	/**
	 * Set words that are related to the program (for example
	 * the name of the program, the name of the pea, the user name...).
	 * These words will be checked directly. 
	 * The words should be larger than 5.
	 * 
	 * @param relatedWords the relatedWords to set
	 */
/*	public static void setRelatedWords(char[][] relatedWords) {
		PasswordQualityCheck.relatedWords = relatedWords;
	} */
}
