package cologne.eck.tools;

public class ExceptionHelper {
	
	private Exception e;
	private String extraMessage = null;
	
	public ExceptionHelper(Exception _e) {
		this.e = _e;
		this.extraMessage = null;
	}
	
	public ExceptionHelper(Exception _e, String _extraMessage) {
		this.e = _e;
		this.extraMessage = _extraMessage;
	}

	/**
	 * Get exception name, message and maximal 5 lines
	 * of the stack trace, inclusive the first four lines
	 * that affect classes of cologne.eck package
	 * 
	 * @return	informations about the exception as String
	 */
	public String getInfos() {
		StringBuilder infos = new StringBuilder();
		if (extraMessage != null) {
			infos.append(extraMessage + "\n");
		}
		infos.append(e.toString() + "\n");
		infos.append( e.getLocalizedMessage() + "\n");
		
		StackTraceElement[] stack = e.getStackTrace();
		int stackLen = stack.length;
		int lineCounter = 0;
		for (int i = 0; i < stackLen; i++) {
			if (i == 0) { // always add first line
				infos.append(stack[i] + "\n");
			}
			if ( stack[i].getClassName().startsWith("cologne.eck")) {
				infos.append(stack[i] + "\n");
				lineCounter++;
			}
			if (lineCounter > 5) {
				break;
			}
		}
		return new String(infos);
	}
}
