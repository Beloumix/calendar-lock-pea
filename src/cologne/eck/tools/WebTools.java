package cologne.eck.tools;

/*
 * Peafactory - Production of Password Encryption Archives
 * Copyright (C) 2020  Axel von dem Bruch
 * 
 * This library is free software; you can redistribute it and/or modify it 
 * under the terms of the GNU General Public License as published 
 * by the Free Software Foundation; either version 2 of the License, 
 * or (at your option) any later version.
 * This library is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
 * or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU General Public License for more details.
 * See:  http://www.gnu.org/licenses/gpl-2.0.html
 * You should have received a copy of the GNU General Public License 
 * along with this library.
 */

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.InetSocketAddress;
import java.net.MalformedURLException;
import java.net.Socket;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.channels.Channels;
import java.nio.channels.ReadableByteChannel;
import java.nio.file.Files;
import java.nio.file.InvalidPathException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.PropertyResourceBundle;
import java.util.ResourceBundle;
import java.util.UUID;

import cologne.eck.all_peas.data.PeaProperties;

public class WebTools {
	
	/**
	 * Checks internet connection (http then https) to Wikipedia.org, 
	 * then openstreetmap.org if Wikipedia fails 
	 * with time out 4 seconds (first check) and 2 seconds (other checks)
	 * 
	 * @return	true, if connection exists
	 */
	public static boolean checkConnection() {
	    // 80 or 443 - webserver
		String address1 = "www.wikipedia.org";
		String address2 = "openstreetmap.org";
		int timeout = 2000;
		// try http first:
    	Socket soc = null;
	    try {
	    	soc = new Socket();
	        soc.connect(new InetSocketAddress(address1, 80), // http
	            		timeout + timeout); // double time for first check
	        return true;
	    } catch (Exception e) {
	    	System.err.println("No http connection..." + address1);
	    	// Check same address on 443
	    	Socket soc2 = null;
	        try {
	        	soc2 = new Socket();
	            soc2.connect(new InetSocketAddress(address1, 443), // https
	            		timeout);
	            return true;
	        } catch (Exception e1) {
	        	System.err.println("No https connection..." + address1);
	        	// check second address
	        	soc2 = new Socket();
	        	try {
					soc2.connect(new InetSocketAddress(address2, 80), // http
							timeout);
					return true;
				} catch (Exception e2) {
					System.err.println("No http connection..." + address2);
					return false;
				}	        	
	        } finally {
	        	try {
	        		if (soc2 != null) {
	        			soc2.close();
	        		}
				} catch (IOException e1) {
					System.err.println(new ExceptionHelper(e1).getInfos());
				}	        
	        }
	    } finally {
	    	try {
	    		if (soc != null) {
	    			soc.close();
	    		}
			} catch (IOException e) {
				System.err.println(new ExceptionHelper(e).getInfos());
			}
	    }
	}
	
	/**
	 * Check the internet connection to an address for the given
	 * timeout. This checks only port 80 (http) and 443 (https)
	 * @param address		the web address
	 * @param timeOutMillis	timeout where checking should break
	 * 
	 * @return	true for success (connection was established)
	 */
	public static boolean checkConnection(String address, int timeOutMillis) {
	    // 80 or 443 - webserver
		// try https first:
    	Socket soc = null;
	    try {
	    	soc = new Socket();
	        soc.connect(new InetSocketAddress(address, 443), // https
	            		timeOutMillis);
	        return true;
	    } catch (Exception e) {
	    	System.err.println("No https connection..." + address);
	    	//System.err.println(e.getLocalizedMessage());
	    	Socket soc2 = null;
	        try {
	        	soc2 = new Socket();
	            soc2.connect(new InetSocketAddress(address, 80), // http
	            		timeOutMillis);
	            return true;
	        } catch (Exception e1) {
	        	System.err.println("No http connection..." + address);
	        	return false;
	        } finally {
	        	try {
	        		if (soc2 != null) {
	        			soc2.close();
	        		}
				} catch (IOException e1) {
					System.err.println(new ExceptionHelper(e1).getInfos());
				}	        
	        }
	    } finally {
	    	try {
	    		if (soc != null) {
	    			soc.close();
	    		}
			} catch (IOException e) {
				System.err.println(new ExceptionHelper(e).getInfos());
			}
	    }
	}

	/**
	 * Check if an web URL exists
	 * 
	 * @param urlString	the URL to check
	 * 
	 * @return	null if exists, an localized error message otherwise
	 */
	public static String checkUrlExistance(String urlString) {
		
		// Check existence of webDavBasePath: First check HTTPS (port 443), then HTTP (port 80)
	    try (Socket socket = new Socket()) {
	    	System.out.println("Try to connect " + urlString + "... ");
	        socket.connect(new InetSocketAddress(urlString, 443), 2000);
	        System.out.println("HTTPS connection successful " + urlString);
	    } catch (MalformedURLException e) {
	        // the URL is not in a valid form
			System.err.println("Web address " + urlString + " has invalid form");
			System.err.println(new ExceptionHelper(e).getInfos());
			return
					PeaProperties.getVmBridge().translate("no_access") + ":\n "
							+ urlString
							+ "\n" + e.getLocalizedMessage()
							+"\n(invalid form)";
	    } catch (IOException e) {
	        //return false; // Either timeout or unreachable or failed DNS lookup.
			System.err.println("No HTTPS access for Web path " + urlString);
			System.out.println("Try HTTP (port 80)...");
		    try (Socket socket = new Socket()) {
		        socket.connect(new InetSocketAddress(urlString, 80), 2000);
		        System.out.println("HTTP connection on port 80 successful " + urlString);
		    } catch (IOException e1) {				 
				System.err.println("Web address " + urlString + " does not exist");
				System.err.println(new ExceptionHelper(e1).getInfos());
				return
						PeaProperties.getVmBridge().translate("no_access") + ":\n "
								+ urlString
								+ "\n" + e1.getLocalizedMessage()
								+"\n(unreachable or timeout)";
		    }
	    }
		return null;
	}
	
	/**
	 * Download a file from web
	 * 
	 * @param url				web address
	 * @param targetFileName	download target
	 * @return					number of bytes from download, 0 if method failed
	 * @throws Exception
	 */
	public static long downloadFile(String url, String downloadedFileNameWithPath) throws Exception {
		
		if (url == null || downloadedFileNameWithPath == null) {
			TestLog.e(WebTools.class, "Missing value " + ((url == null) ? "url" : "downloadedFileNameWithPath"));
			TestLog.ve(WebTools.class, "Missing value", 5);
			return 0;
		}
		try {
			Path target = Paths.get(downloadedFileNameWithPath);
			if (target != null && target.getParent() != null) {
				Files.createDirectories(target.getParent());
			} else {
				TestLog.e(WebTools.class, "Cant find parent of " + downloadedFileNameWithPath);
			}
		} catch (InvalidPathException e) {
			TestLog.e(WebTools.class, e.getLocalizedMessage());
			TestLog.ve(WebTools.class, "Invalid path: " + downloadedFileNameWithPath, 5);
			return 0;
		} catch (Exception e) {
			TestLog.e(WebTools.class, e.getLocalizedMessage() + " - " + e.toString());
			TestLog.ve(WebTools.class, "Create directories failed for: " + downloadedFileNameWithPath, 5);
			return 0;
		}
	    try (InputStream in = URI.create(url).toURL().openStream()) {
	        return Files.copy(in, Paths.get(downloadedFileNameWithPath), StandardCopyOption.REPLACE_EXISTING );
	    }
	}
	
	/**
	 * Get a property from a web property file 
	 * 
	 * @param url	URL of property file: https://...properties
	 * @param key	the key
	 * 
	 * @return		the value of the key
	 * @throws Exception
	 */
	public static String readPropertyFromWeb(String url, String key) throws Exception {
	    try (InputStream inputStream = URI.create(url).toURL().openStream()) {
	    	ResourceBundle bundle = new  PropertyResourceBundle (inputStream);
	    	return bundle.getString(key);
	    }
	}
	
	/**
	 * Get a file from Internet. The content of the file
	 * is stored in a local file in the default temporary directory.
	 * 
	 * @param url	the url string of the file (is checked)
	 * 
	 * @return	the file in the default temp directory or null
	 */
	public static File getFileFromURL(String url) {
		URL webContent;
		FileOutputStream fos = null;
		File file = new File(System.getProperty("java.io.tmpdir") + File.separator + UUID.randomUUID().toString());
		try {
			webContent = new URL(url);
			ReadableByteChannel rbc = Channels.newChannel(webContent.openStream());
			fos = new FileOutputStream(file);
			fos.getChannel().transferFrom(rbc, 0, Long.MAX_VALUE);
		} catch (MalformedURLException e) {
			System.err.println("Can't get file from web " + url);
			System.err.println(new ExceptionHelper(e).getInfos());			
			return null;
		} catch (IOException e) {
			System.err.println("Can't get file from web " + url);
			System.err.println(new ExceptionHelper(e).getInfos());			
			return null;
		}
		try {
			fos.flush();
			fos.close();
		} catch (IOException e) {
			System.err.println("Can't close output stream from web " + url);
			System.err.println(new ExceptionHelper(e).getInfos());			
		}
		return file;
	}
	
	/**
	 * Check if a string is a valid URL. 
	 * 
	 * @param urlString	the String to check
	 * 
	 * @return	null if valid, an error message otherwise
	 */
	public static String checkValidUrl(String urlString) {
	    try {
	        new URL(urlString).toURI();
	    } catch (MalformedURLException e) {
	        return e.getLocalizedMessage();
	    } catch (URISyntaxException e) {
	        return e.getLocalizedMessage();
	    }
	    return null;
	}
}
