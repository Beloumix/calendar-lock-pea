package cologne.eck.tools;

/*
 * Peafactory - Production of Password Encryption Archives
 * Copyright (C) 2015  Axel von dem Bruch
 * 
 * This library is free software; you can redistribute it and/or modify it 
 * under the terms of the GNU General Public License as published 
 * by the Free Software Foundation; either version 2 of the License, 
 * or (at your option) any later version.
 * This library is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
 * or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU General Public License for more details.
 * See:  http://www.gnu.org/licenses/gpl-2.0.html
 * You should have received a copy of the GNU General Public License 
 * along with this library.
 */

/**
 * Write files. 
 */


import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;


public final class WriteResources {
	
	private static String lastErrorMessage;

	/**
	 * Write an array of bytes in a file. 
	 * Error message can be accessed by calling getLastErrorMessage()
	 * 
	 * @param textBytes	the byte array to write
	 * @param fileName	the name of the file
	 * @param dir		the directory of the file
	 * 
	 * @return	true for success, false otherwise
	 */
	public static boolean write(byte[] textBytes , String fileName, String dir) {

		lastErrorMessage = null; // reset
		
		ByteBuffer bytebuff = ByteBuffer.allocate( textBytes.length );
		bytebuff.clear();
		bytebuff.put( textBytes );
		bytebuff.flip();
		
		FileOutputStream fos = null;
		FileChannel chan = null;

		File file = null;
		if ( dir == null) {
			file = new File(fileName);
		} else {
			fileName = dir + java.io.File.separator+ fileName;
			file = new File(fileName);
		}
		if (dir != null && ! new File(dir).exists()) {
			new File(dir).mkdirs();
		}
		if ( ! file.exists() ) {			
			//System.out.println("WriteResources: fileName: " + fileName + " file getName: " + file.getPath() );
			try {
				file.createNewFile();
			} catch (IOException e) {
				registerException("Can't create: " + fileName, e);				
				return false;
			}
		}
		
		try {
			fos = new FileOutputStream(file);
			chan = fos.getChannel();
			while(bytebuff.hasRemaining()) {
			    chan.write(bytebuff);
			}		
		} catch (IOException ioe) {			
			registerException("Can't write: " + fileName, ioe);			
			return false;
		} finally {
			if ( chan != null){
				try {
					chan.close();
				} catch (IOException e) {
					registerException("Can't close file channel: " + fileName, e);				}
			} 
			if ( fos != null) {
				try {
					fos.close();
				} catch (IOException e) {
					registerException("Can't close output stream: " + fileName, e);
				}
			}
		}
		return true;
		// Message ausgeben, dass Datei geschrieben wurde
		//JOptionPane.showMessageDialog(null, "Die Datei " + fileName + " wurde geschrieben.");	
	}	
	
	/**
	 * Write a text in a file. 
	 * Error message can be accessed by calling getLastErrorMessage()
	 * 
	 * @param text		the text to write
	 * @param fileName	the name of the file with absolute path
	 * 
	 * @return	true for success, false otherwise
	 */
	public static boolean writeText(String text, String fileName) {
		
		if (fileName == null) {
			TestLog.e(WriteResources.class,  "Missing file name");
			TestLog.ve(WriteResources.class,  "Missing file name", 5);
			return false;
		}
		
		File f = new File(fileName);
		if ( ! f.exists()) {
			f.getParentFile().mkdirs(); 
			try {
				f.createNewFile();
			} catch (IOException e) {
				registerException("no access: " + fileName, e);
				return false;
			}
		}		
		
		lastErrorMessage = null; // reset
		//FileWriter fw = null;
		BufferedWriter buff = null;
		try {
			 // overwrites file if exists
			buff = new BufferedWriter(new FileWriter(fileName));
			buff.write(text);

		} catch (IOException ioe) {
			TestLog.e(WriteResources.class,  ioe.getLocalizedMessage());
			TestLog.ve(WriteResources.class,  ioe.getLocalizedMessage(), 5);
			registerException("writing failed: " + fileName, ioe);
			return false;
		} finally {
			if (buff != null) {
				try {
					buff.close();
				} catch (IOException ioe) {
					registerException("Can't close Writer for: " +fileName, ioe);
					return false;
				}
			}
		}
		return true;
	}
	
	/**
	 * Generate an error message
	 * 
	 * @param message	specific message
	 * @param e			Exception
	 */
	private static void registerException(String message, Exception e) {
		lastErrorMessage = e.getLocalizedMessage() 
				+"\n" + message
				+ "\n(" + e.toString() + " in cologne.eck.tools.WriteResources)";
		System.err.println(lastErrorMessage);
	}
	
	/**
	 * Get the error message from the last method call in this class
	 * 
	 * @return	the last error message: 	line 1: localizedErrorMessage
	 * 										line 2: specific message (no translation)
	 * 										line 3: Exception name and class name
	 */
	public static String getLastErrorMessage() {
		return lastErrorMessage;
	}
}
