package cologne.eck.tools;

/*
 * Peafactory - Production of Password Encryption Archives
 * Copyright (C) 2015  Axel von dem Bruch
 * 
 * This library is free software; you can redistribute it and/or modify it 
 * under the terms of the GNU General Public License as published 
 * by the Free Software Foundation; either version 2 of the License, 
 * or (at your option) any later version.
 * This library is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
 * or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU General Public License for more details.
 * See:  http://www.gnu.org/licenses/gpl-2.0.html
 * You should have received a copy of the GNU General Public License 
 * along with this library.
 */

/**
 * Read files. 
 */


import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;



public final class ReadResources {

	private static String lastErrorMessage;

	/**
	 * Read a resource file from folder "resources"
	 * 
	 * @param fileName	the file name inside the folder resource
	 * 
	 * @return	content of the file as byte array
	 */
    public static byte[] readResourceFile( String fileName ) {
    	
    	if (fileName == null) {
    		new UnexpectedValueException("fileName", "String", "is null").printDescription();
    		lastErrorMessage = "Missing file name";
    		return null;
    	}
    	lastErrorMessage = null;//reset
    	
    	byte[] byteArray = null; // return value;    	
    	
    	ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
    	if (classLoader == null) {
    	    classLoader = Class.class.getClassLoader();
    	}
    	if (classLoader == null) {
    		System.err.println("ReadResources: Classloader is null");
    		new UnexpectedValueException("classLoader", "Classloader", "is null").printDescription();
    		return null;
    	}

    	InputStream is = classLoader.getResourceAsStream("resources/" + fileName);//"resources/fileName");
    	if (is == null) {
    		registerException("Can't read file in stream: " + fileName, null);
    		return null;
    	}

    	// Stream to write in buffer 
    	//buffer of baos automatically grows as data is written to it
    	ByteArrayOutputStream baos = new ByteArrayOutputStream();
    	int bytesRead;
    	byte[] ioBuf = new byte[4096];
    	 try {
			while ((bytesRead = is.read(ioBuf)) != -1) baos.write(ioBuf, 0, bytesRead);			
		} catch (IOException e1) {
			registerException("Can't read file: " + fileName, e1);
		} finally {

			if (is != null)
				try {
					is.close();
				} catch (IOException e) {
					registerException("Can't close file: " + fileName, e);
				}
		}
    	//System.out.println("baos vor fill: " + baos.toString() );
    	byteArray = baos.toByteArray();
    	
    	// Fill buffer of baos with Zeros
    	int bufferSize = baos.size();
    	baos.reset(); // count of internal buffer = 0
    	try {
			baos.write(new byte[bufferSize]); // fill with Null-Bytes
		} catch (IOException e) {
			registerException("\"Can't fill ByteArrayOutputStream: " + fileName, e);
		}
    	//System.out.println("baos nach fill: " + baos.toString() );
    	return byteArray;    
    }
    
    /**
     * Read a file from within the jar archive
     * 
     * @param fileNameInJar	the file name inside the jar archive
     * 
     * @return	the content of the file as byte array or null (call getLastErrorMessage() )
     */
    public static final byte[] getResourceFromJAR( String fileNameInJar){

    	lastErrorMessage = null;//reset
    	byte[] result = null;

       	URL url = ReadResources.class.getClassLoader().getResource(fileNameInJar);	
    	
    	ByteArrayOutputStream bais = new ByteArrayOutputStream();
    	InputStream is = null;
    	try {
    		is = url.openStream ();
    		byte[] buffer = new byte[4096]; 
    		int n;

    		while ( (n = is.read(buffer)) > 0 ) {
    			bais.write(buffer, 0, n);
    		}
    	} catch (Exception ioe) {
    		registerException("Can't read: " + fileNameInJar , ioe);
    		return null;
    	} finally {
    		if (is != null) { 
    			try {
    				is.close();
    			} catch (IOException e) {
    				registerException("\"Can't close: " + fileNameInJar + ", " + url.toExternalForm(), e);
    			} 
    		}
    	}
    	result = bais.toByteArray();// no need to clear: resource is encrypted
    	return result;
    }
    
    /**
     * Read a file anywhere on the device. 
     * 
     * @param fileName	the file name with absolute path
     * 
     * @return	the content of the file as byte array
     */
    public static byte[] readExternFile(String fileName) {
    	
    	lastErrorMessage = null;//reset
    	byte[] byteArray = null;    	
       	
    	File file = new File(fileName);
    	if (checkFile(file) == false) {
    		return null;
    	}
       	
    	int sizeOfFile = (int) file.length();
    	
    	FileInputStream fis = null;
		try {
			fis = new FileInputStream( file );
		} catch (FileNotFoundException e1) {
			registerException("Can't create input stream from file: " + fileName, e1);
			return null;
		}
    	FileChannel chan = fis.getChannel( );
    	ByteBuffer bytebuff = ByteBuffer.allocateDirect( (int)file.length() );
    	byteArray = new byte[sizeOfFile];
    	//long checkSum = 0L;
    	int nRead, nGet;
    	try {
			while ( (nRead=chan.read( bytebuff )) != -1 )
			{
			    if ( nRead == 0 )
			        continue;
			    bytebuff.position( 0 );
			    bytebuff.limit( nRead );
			    while( bytebuff.hasRemaining( ) )
			    {
			        nGet = Math.min( bytebuff.remaining( ), sizeOfFile );
			        bytebuff.get( byteArray, 0, nGet ); // fills byteArray with bytebuff
			    }
			    bytebuff.clear( );
			}
		} catch (IOException e1) {
			registerException("Can't read length of file: " + fileName, e1);
		} finally {

			if (fis != null){
				try {
					fis.close();
				} catch (IOException e) {
					registerException("Can't close file after reading length: " + fileName, e);
				}
			}
			bytebuff.clear();
		}    	
    	return byteArray;
    }

	/**
	 * Check if a file can be read:
	 * maximal length, existence, read access, not directory, not empty
	 * 
	 * @param file	the file to check
	 * 
	 * @return	true if the file can be read
	 */
	private final static boolean checkFile(File file) {
    	lastErrorMessage = null;//reset
		
       	if (file.length() > Integer.MAX_VALUE) { // > int
       		registerException("File is larger than maximal size (size of int): " + file.getAbsolutePath(), null);
    		return false;//new byte[0];
    	}
       	if (! file.exists()) {
       		registerException("File does not exist: " + file.getAbsolutePath(), null);
       		return false;
       	}
       	if (! file.canRead() ) {
       		registerException("Can't read file: " + file.getAbsolutePath(), null);
       		return false;
       	}
       	if ( file.isDirectory() ) {
       		registerException("File is directory: " + file.getAbsolutePath(), null);
       		return false;
       	}
       	if (file.length() == 0) {
       		registerException("File is empty: " + file.getAbsolutePath(), null);
      //new UnexpectedValueException("File","file","is empty: " + file.getAbsolutePath()).printDescription();
       		return false;
       	}
       	return true;		
	}
	
	/**
	 * Generate an error message
	 * 
	 * @param message	specific message
	 * @param e			Exception
	 */
	private static void registerException(String message, Exception e) {
		if (e != null) {
			lastErrorMessage = e.getLocalizedMessage() 
				+"\n" + message
				+ "\n(" + e.toString() + " in cologne.eck.tools.ReadResources)";
		} else {
			lastErrorMessage = message
					+ "\n(in cologne.eck.tools.ReadResources)";
		}
		System.err.println(lastErrorMessage);
	}
	
	/**
	 * Get the error message from the last method call in this class.
	 * In case of an exception: 
	 *   line 1: localizedErrorMessage
	 *   line 2: specific message (no translation)
	 *   line 3: Exception name and class name
	 * Otherwise:
	 *   line 1: specific message
	 *   line 2: class name
	 * 
	 * @return	the last error message
	 */
	public static String getLastErrorMessage() {
		return lastErrorMessage;
	}
}