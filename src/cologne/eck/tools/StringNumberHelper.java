package cologne.eck.tools;

public class StringNumberHelper {
	
	/**
	 * Check if String is integer. 
	 * Allows leading zero but not +/-, 
	 * allows leading or trailing whitespace, 
	 * but no whitespace inside
	 * 
	 * @param x		Sting to test
	 * @return
	 */
	private static boolean isDec(String x) {
		return x.matches("\\s*\\d+(\\d+)?\\s*");
	}
	
	/**
	 * Get leading decimal integers, ignore leading
	 * whitespace, do not allow +/-, but leading zero, 
	 * stop when any non-decimal character is found
	 * 
	 * @param x	String to get the integer from
	 * 
	 * @return leading decimal integers as int or -1 if there are not digits
	 */
	private static int getLeadingDecs(String x) {
		int len = x.length();
		StringBuilder result = new StringBuilder();
		for (int i = 0; i < len; i++) {
			Character c = x.charAt(i);
			if (Character.isWhitespace(c)) {
				// leading: ignore, trailing: stop
				if (result.length() == 0) {//leading whitespace
					continue;
				} else {
					break;
				}				
			} else if (Character.isDigit(c)) {
				result.append(c);
			} else {
				break;
			}
		}
		if (result.length() == 0) {
			return -1;
		} else {
			return Integer.valueOf(new String(result));
		}
	}
	
	/**
	 * Check two Strings containing version numbers
	 * 
	 * @param currentVersion	current version String
	 * @param availableVersion	available version String
	 * 
	 * @return	0 if the version number is equal, 1 if available version is higher, 
	 * 			-1 is current version is higher, -2 for error
	 */
	public static int checkNewVersionNumber(String currentVersion, String availableVersion) {
		
		if (currentVersion == null || availableVersion == null) {
			System.err.println("Missing value to check version...");
			return -2;
		}
		
		String[] currentNumbers = currentVersion.split("\\."); 
		String[] availableNumbers = availableVersion.split("\\."); 					
		int len = 0;
		if (currentNumbers.length > availableNumbers.length) {
			len = currentNumbers.length;
		} else {
			len = availableNumbers.length;
		}
		int newVersion = 0;//  1 = new version found, -1 = no new version
		//TestLog.o(StringNumberHelper.class, ""+currentVersion + " - " + availableVersion + ",len: "+len);

		for (int i = 0; i < len; i++) {
			//TestLog.o(StringNumberHelper.class, ""+ i);
			if (i >= availableNumbers.length) { // avoid IndexOutOfBound
				if (i < currentNumbers.length) {
					// check if there is a digit
					int cur = getLeadingDecs(currentNumbers[i]);
					if (cur > 0) {
						newVersion = -1;
					}
				} 
				break;
			}  
			if (i >= currentNumbers.length) {
				if (i < availableNumbers.length) {
					// check if there is a digit
					int cur = getLeadingDecs(availableNumbers[i]);
					if (cur > 0) {
						newVersion = 1;
					}
				} 
				break;
			}  			
			//TestLog.o(StringNumberHelper.class, ""+currentNumbers[i] + " - " + availableNumbers[i]);			
			int vCurrent = 0;
			int vAvailable = 0;
			if (StringNumberHelper.isDec(availableNumbers[i])) {
				vAvailable = Integer.valueOf(availableNumbers[i].trim());
			} else {
				vAvailable = getLeadingDecs(availableNumbers[i]);
			}
			if (StringNumberHelper.isDec(currentNumbers[i])) {
				vCurrent = Integer.valueOf(currentNumbers[i].trim());
			} else {
				vCurrent = getLeadingDecs(currentNumbers[i]);
			}
			if (vCurrent > vAvailable) {
				newVersion = -1;
				break;
			} else if (vAvailable > vCurrent) {
				newVersion = 1;
				break;
			} else {
				continue;
			}
		}
		return newVersion;
	}

	public static void main(String[] args) {
		//String x = "  0444444 01 ";
		//System.out.println(isDec(x));
		//System.out.println(getLeadingDecs(x));
		
		String a = "  001.4.5-alpha";
		String c = "01.  04 xxx";
		System.out.println(checkNewVersionNumber(c, a));
	}
}
