package cologne.eck.tools;

/*
 * Peafactory - Production of Password Encryption Archives
 * Copyright (C) 2020  Axel von dem Bruch
 * 
 * This library is free software; you can redistribute it and/or modify it 
 * under the terms of the GNU General Public License as published 
 * by the Free Software Foundation; either version 2 of the License, 
 * or (at your option) any later version.
 * This library is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
 * or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU General Public License for more details.
 * See:  http://www.gnu.org/licenses/gpl-2.0.html
 * You should have received a copy of the GNU General Public License 
 * along with this library.
 */

import java.net.InetAddress;
import java.net.UnknownHostException;

public class UniqueDeviceIdentifier {

	/**
	 * Get an identifier for the current device. This
	 * identifier is not unique in a strong sense.
	 * This is a hash from user name, OS, OS architecture
	 * and host name
	 * 
	 * @return	a unique device identifier
	 */
	public static String getDeviceIdentifier() {
		StringBuilder builder = new StringBuilder();


		String userName = System.getProperty("user.name");
		builder.append(userName);
		//System.out.println(userName);
		
		String os = System.getProperty("os.name");
		builder.append(os);
		//System.out.println(os);
		
		String osArch = System.getProperty("os.arch");
		builder.append(osArch);
		//System.out.println(osArch);
		
		String host = null;
		try {
			host = InetAddress.getLocalHost().getCanonicalHostName();
		} catch (UnknownHostException e) {
			System.err.println("Couldn't figure out the host name to create a device identifier...");
			host = "";
		}
		builder.append(host);
		//System.out.println(host);

		return "" + new String(builder).hashCode();
	}

	// For test
	public static void main(String[] args) {
		String id = getDeviceIdentifier();
		System.out.println("ID: " + id);
	}

}
