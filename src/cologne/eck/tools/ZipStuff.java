package cologne.eck.tools;

/*
 * PeaFactory - Production of Password Encryption Archives
 * Copyright (C) 2015  Axel von dem Bruch
 * 
 * This library is free software; you can redistribute it and/or modify it 
 * under the terms of the GNU General Public License as published 
 * by the Free Software Foundation; either version 2 of the License, 
 * or (at your option) any later version.
 * This library is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
 * or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU General Public License for more details.
 * See:  http://www.gnu.org/licenses/gpl-2.0.html
 * You should have received a copy of the GNU General Public License 
 * along with this library.
 */


import java.awt.Window;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import java.net.URI;
import java.nio.file.FileSystem;
import java.nio.file.FileSystems;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.StandardCopyOption;
import java.nio.file.StandardOpenOption;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.ArrayList;
import java.util.Arrays;

import java.util.HashMap;
import java.util.Map;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import javax.swing.JFileChooser;

import cologne.eck.all_peas.control.PeaControl;
import cologne.eck.all_peas.data.PeaProperties;
import cologne.eck.all_peas.gui.CursorManager;
import cologne.eck.all_peas.gui.PeaDialog;
import cologne.eck.all_peas.gui.PeaFileChooser;
import cologne.eck.all_peas.vm_specific.JREProperties;
import cologne.eck.all_peas.files.ExecutionTimeObserver;
import cologne.eck.all_peas.files.FileTypePanel;



public class ZipStuff {
	
	/**
	 * Unzip a zip archive and store included files in a selected folder. 
	 * This method should be called in EDT.
	 * 
	 * @param zipFileName		the archive to unzip
	 * @param folderNameToStore	the name of the folder to store the zip content
	 * 							this folder must exists
	 * @param window			the parent window
	 * @param bundle			the language bundle
	 * 
	 * @return	an array of created files from the zip archive
	 */
	public static File[] unzip(String zipFileName, String folderNameToStore, Window window) {

		TestLog.v(ZipStuff.class, "Unzip: " + zipFileName + "   " + folderNameToStore);
		Path zipPath = Paths.get(zipFileName);
		File zipFile = zipPath.toFile();// File(zipFileName);
		if (! zipFile.exists() || ! zipFile.isFile() || ! zipFile.canRead() || ! zipFile.canWrite() ) {
			PeaDialog.showMessage(window, PeaProperties.getVmBridge().translate("no_access") + "\n" + zipFile.getAbsolutePath(), "Zip", 0);
			return null;
		}
		if (folderNameToStore == null || folderNameToStore.equals("") ) {
			// files with absolute paths...        	
		} else { 
			// check directory to store
			File folderToStore = new File(folderNameToStore);
			if ((! folderToStore.exists() ) || (! folderToStore.isDirectory())) {
				PeaDialog.showMessage(window, PeaProperties.getVmBridge().translate("select_folder_for_unzipped_files"), "Zip", 0);
				return null;
			}
		}
		try {
			// set the wait cursor: 
			window.setCursor(CursorManager.getWaitCursor());
			
			boolean isWindowsOS = System.getProperty("os.name").toLowerCase().contains("windows");
			boolean isAbs = true;
			if (folderNameToStore != null && folderNameToStore.length() > 1) {
				// this is a zipped folder with relative paths
				isAbs = false;
			}
			final boolean isAbsolutePath = isAbs;
			// Properties for file systrem:  
			Map<String, String> zip_properties = new HashMap<>(); 
			// only read is required
			zip_properties.put("create", "false");// only read the zip file
			URI zipUri = URI.create("jar:" + zipFile.toURI() );// URI already starts with   file:
			
			TestLog.ve(ZipStuff.class, zipUri.toString());//jar:file:/C:/Users/HP/Downloads/Test/ZIP/1-5_pea.zip

			// the list of files from zip files
			ArrayList<File> createdFileList = new ArrayList<File>();

			FileSystem zipFs = null;
			try {
				zipFs = FileSystems.newFileSystem(zipUri, zip_properties);
			} catch (java.nio.file.FileSystemAlreadyExistsException e1) {
				// when unzipping the same zip several times
				TestLog.ve(ZipStuff.class, "Unzip same file again: " + zipFile.getAbsolutePath());
				zipFs = FileSystems.getFileSystem(zipUri);
			} catch (IOException e1) {
				TestLog.ve(ZipStuff.class, e1.toString() + " - " + e1.getLocalizedMessage());
				PeaDialog.showMessage(window, PeaProperties.getVmBridge().translate("no_access") + "\n" + zipFile.getAbsolutePath(), "Zip", 0);
				return null;
			}

			// The path within the zip file you want to start from
			Path root = zipFs.getPath("/"); 
			if (root != null) {

				try {
					Files.walkFileTree(root, new SimpleFileVisitor<Path>() {
						@Override
						public FileVisitResult visitFile(Path path, BasicFileAttributes attrs) throws IOException {
							//TestLog.ve(ZipStuff.class, "path: " + path);
							try {
								if (path == null) {
									TestLog.ve(ZipStuff.class, "no path... ");
									return FileVisitResult.CONTINUE;
								}
								TestLog.v(ZipStuff.class, "System is Windows: " + isWindowsOS);
								TestLog.v(ZipStuff.class, "Path is absolute: " + isAbsolutePath);
								String parentInZip = null;
								String outFileName = null;
								String pathString = path.toString();
								TestLog.ve(ZipStuff.class, "Path in zip: " + pathString);
								
								if (isWindowsOS == false){ // system is Unix

									parentInZip = pathString.substring(0, pathString.lastIndexOf("/"));
									TestLog.v(ZipStuff.class, "parentInZip: " + parentInZip);
									if (pathString.lastIndexOf("/") != -1) {
										outFileName = pathString.substring(pathString.lastIndexOf("/") + 1, pathString.length());
										TestLog.v(ZipStuff.class, "outFileName: " + outFileName);
									} else {
										parentInZip = "";
										outFileName = pathString;
									}
								} else {
									// get the parent folder to store
									if (path.getParent() != null) {
										Path parentPath = path.getParent();
										if (parentPath != null) {
											parentInZip = parentPath.toString();
										} else {
											TestLog.ve(ZipStuff.class, "no parent found: " + path.toString());
										}
									} else {
										TestLog.ve(ZipStuff.class, "no parent found: " + path.toString());
										return FileVisitResult.CONTINUE;
									}
									TestLog.v(ZipStuff.class, "parent in zip: " + parentInZip);
									if (path.getFileName() != null) {
										if (path.getFileName() != null) {
											Path pathFileName = path.getFileName();
											if (pathFileName != null) {
												outFileName = path.getFileName().toString();
											} else {
												TestLog.ve(ZipStuff.class, "No file name found: " + path.toString());
											}
										}
									} else {
										TestLog.ve(ZipStuff.class, "no file name found: " + path.toString());
										return FileVisitResult.CONTINUE;
									}
									TestLog.v(ZipStuff.class, "out file name: " + outFileName);
								}
								if (outFileName == null) {
									TestLog.ve(ZipStuff.class, "Missing outFileName ");
									PeaDialog.showMessage(window, 
											PeaProperties.getVmBridge().translate("unexpected_error") + ": " + pathString, 
											PeaProperties.getVmBridge().translate("unzip"), 0);
									return FileVisitResult.CONTINUE;
								}
								if( outFileName.startsWith("/")) {
									outFileName = outFileName.substring(1, outFileName.length());
									TestLog.v(ZipStuff.class, "outFileName without trailing /: " + outFileName);
								}

								if (isAbsolutePath == true && (parentInZip == null || parentInZip.length() < 3) ) {
									TestLog.ve(ZipStuff.class, "Invalid parent path in zip: " + pathString);
									PeaDialog.showMessage(window, 
											PeaProperties.getVmBridge().translate("invalid_file") + ": " + pathString, 
											PeaProperties.getVmBridge().translate("unzip"), 0);
									return FileVisitResult.CONTINUE;
								}
								if (parentInZip == null) {
									TestLog.ve(ZipStuff.class, "parentInZp is null: " + path.toString());
									return FileVisitResult.CONTINUE;
								}
								// take same folder for output:
								String outFolderName =  parentInZip;
								TestLog.v(ZipStuff.class, "outFolderName: " + outFolderName);
								// create windows absolute path
								if (isAbsolutePath == true && isWindowsOS == true 								// system is windows
										&& (outFolderName.charAt(2) == ':')) {		// zip with abs. paths created in windows
									// remove leading / in absolute path to get windows path e.g. C:\...
									if (path != null && parentInZip != null && path.getRoot() != null && path.getRoot().toString() != null) {
									outFolderName =  parentInZip.substring(path.getRoot().toString().length(), parentInZip.length());
									} else {
										TestLog.v(ZipStuff.class, "Missing value to create outFolderName... ");
										return FileVisitResult.CONTINUE;
									}
									TestLog.v(ZipStuff.class, "outFolderName for Windows: " + outFolderName);
								}
								
								// create the out path for zipped folders and files with absolute path
								Path pathOut = null;
								if (isAbsolutePath == false) { // store in directory
									TestLog.v(ZipStuff.class, "Zip is zipped directory (no _pea inside): " + zipFileName);
									// Zipped folders: (no trailing _pea.zip)
									if (outFolderName != null && outFolderName.length() > 0) {
										if( outFolderName.startsWith("/")) {
											outFolderName = outFolderName.substring(1, outFolderName.length());
											TestLog.v(ZipStuff.class, "outFolderName without trailing /: " + outFolderName);
										}
										pathOut = Paths.get(folderNameToStore + File.separator 
												+ outFolderName + File.separator + outFileName);
									} else {
										pathOut = Paths.get(folderNameToStore + File.separator + outFileName);
									}
								} else { // file with absolute path
									TestLog.v(ZipStuff.class, "Zip contains files with absolute paths ( _pea.zip): " + zipFileName);
									// Zipped selected files with absolute path: trailing _pea.zip
									pathOut = Paths.get(outFolderName + File.separator + outFileName);
								}
								if (pathOut != null) {
									TestLog.v(ZipStuff.class, "pathOut: " + pathOut.toString());
								}

								// special case: different OS:
								if ( (isAbsolutePath == true && isWindowsOS == false 		// system is Unix
									&& (parentInZip.charAt(2) == ':') )					// files created in windows: /C:\
									||
									 (isAbsolutePath == true &&  isWindowsOS == true		// system is windows
									&& (parentInZip.charAt(2) != ':') ) ) { 			// files created in Unix: /
									TestLog.v(ZipStuff.class, "File in zip from other OS: " + pathOut.toString());
									TestLog.ve(ZipStuff.class, "File in zip from other OS: " + pathOut.toString());
									// ask where to store
									PeaFileChooser chooser = new PeaFileChooser(window);
									chooser.customizeFileChooser("Zip", PeaProperties.getVmBridge().translate("file_from_other_os") + "\n" +
											PeaProperties.getVmBridge().translate("select_folder_to_zip"), false);
									chooser.setFileSelectionMode( JFileChooser.DIRECTORIES_ONLY);
									int returnVal = chooser.showOpenDialog(window);
									if (chooser.getSelectedFile() == null || returnVal != JFileChooser.APPROVE_OPTION) {
										TestLog.o(ZipStuff.class, "No folder selected...");
										return FileVisitResult.CONTINUE;				
									} else {
										File folder = chooser.getSelectedFile();
										TestLog.v(ZipStuff.class, "Selected folder to store: " + folder.getAbsolutePath());
										pathOut = Paths.get(folder.getAbsolutePath() + File.separator + outFileName);
										TestLog.v(ZipStuff.class, "Created pathOut: " + pathOut.toString());
									}
								}

								// ask to overwrite existing file
								if (PeaProperties.isOverwriteExistingFiles() == false) {
									FileTypePanel ftp = (FileTypePanel)JREProperties.getTypePanel();
									if (pathOut != null) {
										if ( ftp.askToOverwriteFile(pathOut.toFile(), true) == true){						
											//if ( ftp.askToOverwriteFile(new File(pathOut.toString()), true) == true){
											TestLog.v(ZipStuff.class, "Overwrite existing file: " + pathOut.toString());
											try {
												TestLog.v(ZipStuff.class, "path in copy: " + path.toString());
												TestLog.v(ZipStuff.class, "pathOut in copy: " + pathOut.toString());
												// create directories is required
												if (pathOut != null && pathOut.getParent() != null) {
													Files.createDirectories(pathOut.getParent());
												}
												Files.copy(path, pathOut, StandardCopyOption.REPLACE_EXISTING); 
												TestLog.v(ZipStuff.class, "Copied, add to list...");
												createdFileList.add(pathOut.toFile());
											} catch (java.nio.file.NoSuchFileException e1) {
												TestLog.ve(ZipStuff.class, e1.toString() + " - " + e1.getLocalizedMessage()
												+ "\n( zip: " + zipFileName + ", folder:  " + folderNameToStore + " )"
												+ "\npath: " + pathString + ", out: " + pathOut.toString());
											} catch (Exception e1) {
												TestLog.ve(ZipStuff.class, e1.toString() + " - " + e1.getLocalizedMessage()
												+ "\n( zip: " + zipFileName + ", folder:  " + folderNameToStore + " )"
												+ "\npath: " + pathString + ", out: " + pathOut.toString());
											}
										} else {
											TestLog.v(ZipStuff.class, "Overwriting cancelled: " + pathOut.toString());
											// ??? add file ???
										}
									}
								} else {
									TestLog.v(ZipStuff.class, "Overwrite existing file: " + pathOut.toString());
									TestLog.v(ZipStuff.class, "path in copy: " + path.toString());
									TestLog.v(ZipStuff.class, "pathOut in copy: " + pathOut.toString());
									// create directories is required
									if (pathOut != null && pathOut.getParent() != null) {
										Files.createDirectories(pathOut.getParent());
									}
									Files.copy(path, pathOut, StandardCopyOption.REPLACE_EXISTING);
									TestLog.v(ZipStuff.class, "Copied, add to list...");
									createdFileList.add(pathOut.toFile());
								}

							} catch (IOException e1) {
								TestLog.ve(ZipStuff.class, e1.toString() + " - " + e1.getLocalizedMessage()
								+ "(" + zipFileName + " " + folderNameToStore + ")");
							} catch (Exception e1) {
								TestLog.ve(ZipStuff.class, e1.toString() + " - " + e1.getLocalizedMessage()
								+ "( " + zipFileName + " " + folderNameToStore + " )");
							}
							return FileVisitResult.CONTINUE;
						}
					});
				} catch (java.util.regex.PatternSyntaxException e1) {
					TestLog.ve(ZipStuff.class, e1.toString() + " - " + e1.getLocalizedMessage()
					+ "(" + zipFileName + " " + folderNameToStore + ")" +"\n"
					+ e1.getPattern() + " - " + e1.getIndex());
				} catch (java.nio.file.NoSuchFileException e1) {
					TestLog.ve(ZipStuff.class, e1.toString() + " - " + e1.getLocalizedMessage()
					+ "(" + zipFileName + " " + folderNameToStore + ")");
				} catch (Exception e1) {
					TestLog.ve(ZipStuff.class, e1.toString() + " - " + e1.getLocalizedMessage()
					+ "(" + zipFileName + " " + folderNameToStore + ")");
				}
			} else {
				TestLog.ve(ZipStuff.class, "Missing root", 5);
			}
			if (createdFileList.size() > 0) {
				File[] fileArray = new File[createdFileList.size()];
				return createdFileList.toArray(fileArray);
			} else {
				return null;
			}			
		} finally {
			window.setCursor(CursorManager.getDefaultCursor());
		}
	}
	
	/**
	 * Creates a zip file containing all chosen files
	 * with default compression level.
	 * This method should be called in EDT.
	 * 
	 * @param fileNames			file names to zip
	 * @param zipFileName		the name of the zip file to create
	 * @param zipFolder			the folder to store the zip file to create
	 * 
	 * @return		null if the function performs successfully, 
	 * 				an error message if the function fails
	 */
	public static String zipFiles(String[] fileNames, 
			String zipFileName,  File zipFolder) {

		TestLog.v(ZipStuff.class, "Create zip file " + zipFileName + " with " + fileNames.length + " files", 5);
		if (PeaProperties.isOverwriteExistingFiles() == false) {
			if ( ( (FileTypePanel) ( JREProperties.getTypePanel() ) ).askToOverwriteFile(new File( zipFolder + File.separator + zipFileName), true) == false){
				TestLog.v(ZipStuff.class,"Creating zip file canceled");
				return JREProperties.getVmBridge().translate("cancel") + ":\n " +zipFileName;
			}
		}
		try {
			// set the wait cursor: 
			if (JREProperties.getMainWindow() != null) {
				JREProperties.getMainWindow().setCursor(CursorManager.getWaitCursor());
			}

			Map<String, String> env = new HashMap<>(); 
			env.put("create", "true"); // create new zip

			Path dir = Paths.get(zipFolder.getAbsolutePath());
			Path file = dir.resolve(zipFileName);
			URI uri = URI.create("jar:" + file.toUri()); // URI already contains file:

			try (FileSystem zipfs = FileSystems.newFileSystem(uri, env)) {
				for (String fileName : fileNames) {
					try {

						Path externalTxtFile = Paths.get(fileName);//.normalize();
						//TestLog.ve(ZipStuff.class, externalTxtFile.toString());

						Path pathInZipfile = zipfs.getPath(fileName);// sourceDir.relativize(file);
						if (pathInZipfile != null) {
							Path pParent = pathInZipfile.getParent();
							if (pParent != null) {
								Files.createDirectories(pParent);
							} else {
								TestLog.ve(ZipStuff.class, "Parent of path of zipfs is null", 5);
							}
						} else {
							TestLog.ve(ZipStuff.class, "Path of zipfs is null", 5);
						}
						if (pathInZipfile != null) {
							Files.createFile(pathInZipfile );
							//TestLog.ve(ZipStuff.class, pathInZipfile.toString());
							Files.copy( externalTxtFile, pathInZipfile, 
									StandardCopyOption.REPLACE_EXISTING ); 
						}
					} catch (Exception e) {
						TestLog.ve(ZipStuff.class, e.toString() + " - " + e.getLocalizedMessage());
					}
				}

			} catch (IOException e) {
				TestLog.ve(ZipStuff.class, e.toString() + " - " + e.getLocalizedMessage());
			} 

		} finally {
			if (JREProperties.getMainWindow() != null) {
				JREProperties.getMainWindow().setCursor(CursorManager.getDefaultCursor());
			}
		}
		//TestLog.o(ZipStuff.class, "Created zip file ");
		return null;
	}	

	/**
	 * Creates a zip file containing all files of the given folder, but not
	 * sub-directories and files inside of sub-directories
	 * with default compression level.
	 * The zip file is stored beside the folder. 
	 * This method should be called in EDT.
	 * 
	 * @param folderName	the directory name which contains the files to archive
	 * @param zipName		name of the zip file without extension ".zip"
	 * 
	 * @return			null if the function performs successfully, 
	 * 					an error message if the function fails
	 */
	public static String zipFolder(String folderName, String zipName, Window window) {
		
		File folder = new File(folderName);
		if (! folder.exists()) {
			TestLog.ve(ZipStuff.class, " directory to zip does not exist");
			return "ZipStuff (zipFolder): directory to zip does not exist";
		}
		if (! folder.canRead()){
			TestLog.ve(ZipStuff.class, "can not read directory to zip");
			return "ZipStuff (zipFolder): can not read directory to zip";		
		}
		if (! folder.isDirectory()){
			TestLog.ve(ZipStuff.class, " file to zip is not directory");
			return "ZipStuff (zipFolder): file to zip is not directory";
		}
		if (FileTools.getNumberAndSize(folderName)[1] > 1024*1024*128) { // > 128MB
			if (ExecutionTimeObserver.showWarningAndBreakOption(
		    		PeaControl.getDialog().getLockFrame(), PeaProperties.getVmBridge().translate("size_warning"), false) 
					== false) {
				return "Folder content is too large";
			}
		}
		// for partial error (signle files failed)
		StringBuilder sb = new StringBuilder();
		String path = folderName.substring(0, folderName.lastIndexOf(File.separator));
		String zipFileName = path + File.separator + zipName + ".zip";
		
		if (PeaProperties.isOverwriteExistingFiles() == false) {
			if (JREProperties.getTypePanel() != null) { // is null in PeaFactory
				if ( ( (FileTypePanel) (JREProperties.getTypePanel() ) ).askToOverwriteFile(new File( zipFileName), true) == false){
					TestLog.ve(ZipStuff.class, "Creating zip file canceled");
					return PeaProperties.getVmBridge().translate("cancel") + ":\n " +zipFileName;
				}
			}
		}
	    try {
	    	// set the wait cursor: 
			window.setCursor(CursorManager.getWaitCursor());

			final Path sourceDir = Paths.get(folderName);//dirPath);
			final ZipOutputStream outputStream = new ZipOutputStream(new FileOutputStream(zipFileName));
//	        String zipFileName = dirPath.concat(".zip");
	        try {
	           
	            Files.walkFileTree(sourceDir, new SimpleFileVisitor<Path>() {
	                @Override
	                public FileVisitResult visitFile(Path file, BasicFileAttributes attributes) {
	                    try {
	                        Path targetFile = sourceDir.relativize(file);
	                    	outputStream.putNextEntry(new ZipEntry(targetFile.toString()));
	                    	
	                       // abs path:  outputStream.putNextEntry(new ZipEntry(file.toString()));//targetFile.toString()));
	                        byte[] bytes = Files.readAllBytes(file);
	                        outputStream.write(bytes, 0, bytes.length);
	                        outputStream.closeEntry();
	                    } catch (IOException e) {
	                        TestLog.ve(ZipStuff.class, file.getFileName() + ": " + e.toString() + ", " + e.getMessage());
	                        // append to error messages
	                        sb.append(file.getFileName() + ": " + e.getLocalizedMessage() + "\n");
	                    }
	                    return FileVisitResult.CONTINUE;
	                }
	            });
	            outputStream.close();
	        } catch (IOException e) {
	            e.printStackTrace();
	        }
	    } catch (FileNotFoundException e) {
	    	//e.printStackTrace();
	    	TestLog.ve(ZipStuff.class, e.toString() + ", " + e.getMessage(), 5);
			return e.toString() + ", " + e.getMessage();
		} catch (Exception e) {
			TestLog.ve(ZipStuff.class, e.toString() + ", " + e.getMessage(), 5);
			return e.toString() + ", " + e.getMessage();		
		} finally {
			window.setCursor(CursorManager.getDefaultCursor());
		}
	    // Show if only adding some files failed:
	    if (sb.length() > 0) {
	    	PeaDialog.showMessage(window, PeaProperties.getVmBridge().translate("partial_zip_error") +":\n" + new String(sb), "Zip", 0);
	    }
	    return null;
	}
	
	/**
	 * Get the size of a zip file. 
	 * 
	 * @param zipFileNameWithPath	name of the zip file with path
	 * 
	 * @return	the size (compressed or uncompressed)
	 */
	static void secureWipeZipFile(String zipFileNameWithPath) {

		if (zipFileNameWithPath == null) {
			TestLog.ve(ZipStuff.class, "No zip" + ": " + zipFileNameWithPath, 5);
			return;
		}
		//	 Path myFilePath = Paths.get("c:/dump2/mytextfile.txt");
		TestLog.v(ZipStuff.class,  "Wipe and delete zip file " + zipFileNameWithPath);

		Path zipPath = Paths.get(zipFileNameWithPath);
		File zipFile = zipPath.toFile();// File(zipFileName);
		// Properties for file systrem:  
		Map<String, String> zip_properties = new HashMap<>(); 
		// only read is required
		zip_properties.put("create", "true");// 
		URI zipUri = URI.create("jar:" + zipFile.toURI() );// URI already starts with   file:

		FileSystem fs = null;
		try {
			fs = FileSystems.getFileSystem(zipUri);
		} catch (java.nio.file.FileSystemAlreadyExistsException e1) {
			TestLog.v(ZipStuff.class, e1.toString() + " - " + e1.getLocalizedMessage());
		} catch (Exception e1) {
			TestLog.ve(ZipStuff.class, e1.toString() + " - " + e1.getLocalizedMessage());
			PeaDialog.showMessage(JREProperties.getMainWindow(),
					JREProperties.getVmBridge().translate("unexpected_error") + "\n" + zipFile.getAbsolutePath(), "Zip", 0);
			return;
		}
		final FileSystem zipFs = fs;

		if (zipFs != null) {
			// The path within the zip file you want to start from
			Path root = zipFs.getPath("/"); 
			if (root != null) {

				try {
					Files.walkFileTree(root, new SimpleFileVisitor<Path>() {

						@Override
						public FileVisitResult visitFile(Path filePath, BasicFileAttributes attrs)  {

							if (filePath != null) {
								if ( ! System.getProperty("os.name").toLowerCase().contains("windows")) {
									filePath = zipFs.getPath(filePath.toString().replace("\\", "/"));
									if (filePath == null) {
										TestLog.ve(ZipStuff.class, "File path is null ", 5);
										return null;
									}
								}
								try {
									if ( Files.isWritable(filePath)) {
										if (filePath != null && Files.exists(filePath)) {
											long fileSize;

											fileSize = Files.size(filePath);

											if (fileSize > Integer.MAX_VALUE || fileSize <= 0) {
												try {
													if (Files.exists(filePath)) {
														Files.delete(filePath);
													}
												} catch (Exception e2) {
													TestLog.ve(ZipStuff.class, filePath.getFileName() + ": " +  e2.toString() + " - " + e2.getLocalizedMessage());
												}
											} else {
												try {
													Files.write(filePath, new byte[(int) fileSize], StandardOpenOption.WRITE);
													byte[] ff = new byte[(int) fileSize];
													Arrays.fill(ff, (byte) 0xFF);
													Files.write(filePath, ff, StandardOpenOption.WRITE);
												} catch (Exception e) {
													TestLog.ve(ZipStuff.class, filePath.getFileName() + ": " +  e.toString() + " - " + e.getLocalizedMessage());
												}
												// always delete the file
												try {
													if (Files.exists(filePath)) {
														Files.delete(filePath);
													}
												} catch (Exception e2) {
													TestLog.ve(ZipStuff.class, filePath.getFileName() + ": " +  e2.toString() + " - " + e2.getLocalizedMessage());
												}
											}
										}
									} else { 
										try {
											if (Files.exists(filePath)) {
												Files.delete(filePath);
											}
										} catch (Exception e2) {
											TestLog.ve(ZipStuff.class, filePath.getFileName() + ": " +  e2.toString() + " - " + e2.getLocalizedMessage());
										}
									}			
								} catch (java.nio.file.NoSuchFileException e1) {
									TestLog.ve(ZipStuff.class, filePath.getFileName() + ": " +  e1.toString() + " - " + e1.getLocalizedMessage());
								} catch (IOException e1) {
									TestLog.ve(ZipStuff.class, filePath.getFileName() + ": " +  e1.toString() + " - " + e1.getLocalizedMessage());
								}
							}
							return FileVisitResult.CONTINUE;
						}
						@Override
						public FileVisitResult postVisitDirectory(Path dir, IOException exc) throws IOException {
							try {
								if (Files.exists(dir)) {
									Files.delete(dir); // delete directories
								}
							} catch (java.nio.file.DirectoryNotEmptyException e) {
								TestLog.ve(ZipStuff.class, dir + ": " +  e.toString() + " - " + e.getLocalizedMessage());
							} catch (Exception e) {
								TestLog.ve(ZipStuff.class, dir + ": " +  e.toString() + " - " + e.getLocalizedMessage());
							}
							return FileVisitResult.CONTINUE;
						}
						@Override
						public FileVisitResult visitFileFailed(Path file,
								IOException exc) { // show errors
							TestLog.ve(ZipStuff.class, exc.toString() + " - "+  exc.getLocalizedMessage());
							return FileVisitResult.CONTINUE;
						}
					});
					zipFs.close();


				} catch (Exception e) {
					TestLog.ve(ZipStuff.class, e.toString() + " - " + e.getLocalizedMessage() + ": " + zipFileNameWithPath);
				} finally {
					if (new File(zipFileNameWithPath).exists()) {
						boolean deleted = new File(zipFileNameWithPath).delete();
						if (deleted == false) {
							TestLog.e(ZipStuff.class, "Could not delete zip" + ": " + zipFileNameWithPath);
							PeaDialog.showMessage(JREProperties.getMainWindow(), 
									PeaProperties.getVmBridge().translate("unexpected_error") + ": " + zipFileNameWithPath, 
									PeaProperties.getVmBridge().translate("wipe_and_delete"), 0);

							new File(zipFileNameWithPath).deleteOnExit();
						}
					}
				}
			} else {
				TestLog.ve(ZipStuff.class, "No root found in zip" + ": " + zipFileNameWithPath);
				if (new File(zipFileNameWithPath).exists()) {
					boolean deleted = new File(zipFileNameWithPath).delete();
					if (deleted == false) {
						TestLog.e(ZipStuff.class, "Could not delete zip" + ": " + zipFileNameWithPath);
						PeaDialog.showMessage(JREProperties.getMainWindow(), 
								PeaProperties.getVmBridge().translate("unexpected_error") + ": " + zipFileNameWithPath, 
								PeaProperties.getVmBridge().translate("wipe_and_delete"), 0);

						new File(zipFileNameWithPath).deleteOnExit();
					}
				}
			}
		}else {
			TestLog.ve(ZipStuff.class, "No file system found in zip" + ": " + zipFileNameWithPath);
			if (new File(zipFileNameWithPath).exists()) {
				boolean deleted = new File(zipFileNameWithPath).delete();
				if (deleted == false) {
					TestLog.e(ZipStuff.class, "Could not delete zip" + ": " + zipFileNameWithPath);
					PeaDialog.showMessage(JREProperties.getMainWindow(), 
							PeaProperties.getVmBridge().translate("unexpected_error") + ": " + zipFileNameWithPath, 
							PeaProperties.getVmBridge().translate("wipe_and_delete"), 0);

					new File(zipFileNameWithPath).deleteOnExit();
				}
			}
		}
	}
}
