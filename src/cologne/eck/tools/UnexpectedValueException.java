package cologne.eck.tools;

/*
 * PeaFactory - Production of Password Encryption Archives
 * Copyright (C) 2015  Axel von dem Bruch
 * 
 * This library is free software; you can redistribute it and/or modify it 
 * under the terms of the GNU General Public License as published 
 * by the Free Software Foundation; either version 2 of the License, 
 * or (at your option) any later version.
 * This library is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
 * or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU General Public License for more details.
 * See:  http://www.gnu.org/licenses/gpl-2.0.html
 * You should have received a copy of the GNU General Public License 
 * along with this library.
 */
/**
 * This class should help to find minor bugs. 
 * If printDescription is called, the program
 * does not stop, but prints a description of
 * the problem to stderr. 
 * 
 * call like: 
 * new UnexpectedValueException("newFile", "File", "is null").printDescription();
 */


@SuppressWarnings("serial")
public class UnexpectedValueException extends Exception {
	
	private String valueType = "";
	private String valueName = "";
	private String message = "";	
	
	private static int stackTraceLines = 5;

	/**
	 * An exception for an unexpected value, that should not stop
	 * the program. 
	 * 
	 * @param _valueType	the type of the unexpected value as String
	 * @param _valueName	the name of the value used in the method
	 * @param _message		an optional message about the cause (for example: is null, to large...)
	 */
	public UnexpectedValueException(String _valueName, String _valueType, String _message) {
		if (_valueType == null) {
			this.valueType = "(no specification)";
		} else {
			this.valueType = _valueType;
		}
		if (_valueName == null) {
			this.valueName = "(no specification)";
		} else {
			this.valueName = _valueName;
		}
		if (_message == null) {
			this.message = "";
		} else {
			this.message = _message;
		}
	}

	/**
	 * Prints a description of the exception and
	 * the first lines of the StackTrace to stderr. 
	 */
	public final void printDescription(){
		System.err.println("Unexpected value " 
				+ this.valueName + " of type " 
				+ this.valueType + ": " 
				+ this.message);
		// print the first lines of the StackTrace:
		printStackTraceLines();
	}
	

	 /**
     * Prints  first lines (default = 5) of 
     * StackTrace.
     *
     */
    public void printStackTraceLines() {
    	
        synchronized (System.err) {
    		System.err.println("Unexpected value " 
    				+ this.valueName + " of type " 
    				+ this.valueType + ": " 
    				+ this.message);
            StackTraceElement[] stack = super.getStackTrace();
    		int stackLen = stack.length;
    		int lineCounter = 0;
    		for (int i = 0; i < stackLen; i++) {
    			if (i == 0) { // always add first line
    				System.err.println("\tat " + stack[i]);
    			}
    			if ( stack[i].getClassName().startsWith("cologne.eck")) {
    				System.err.println("\tat " + stack[i]);
    				lineCounter++;
    			}
    			if (lineCounter > stackTraceLines) {
    				break;
    			}
    		}
        }
    }

	/**
	 * @return the stackTraceLines
	 */
	public static int getStackTraceLines() {
		return stackTraceLines;
	}

	/**
	 * Set the stack trace lines
	 */
	public static void setStackTraceLines(int _stackTraceLines) {
		stackTraceLines = _stackTraceLines;
	} 
}
