package cologne.eck.tools;

/*
 * PeaFactory - Production of Password Encryption Archives
 * Copyright (C) 2015  Axel von dem Bruch
 * 
 * This library is free software; you can redistribute it and/or modify it 
 * under the terms of the GNU General Public License as published 
 * by the Free Software Foundation; either version 2 of the License, 
 * or (at your option) any later version.
 * This library is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
 * or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU General Public License for more details.
 * See:  http://www.gnu.org/licenses/gpl-2.0.html
 * You should have received a copy of the GNU General Public License 
 * along with this library.
 */

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.RandomAccessFile;
import java.nio.ByteBuffer;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.ArrayList;
import java.util.Arrays;

import cologne.eck.all_peas.data.PeaProperties;

public class FileTools {
	
	// used as buffer for large files
	private static final int bufferSize = 16 * 1024 * 1024; // 16 MB
	
	/**
	 * Get all files of a directory including sub-directories
	 * as an unsorted list. 
	 * 
	 * @param directory			the directory
	 * @param filesOnly			true if only file should be included
	 * 							false if files and directories should be included
	 * 
	 * @return	an unsorted list of all included files 
	 * 			or null if the directory is null 
	 * 			or is not to a directory 
	 */
	public static ArrayList <File> listAllFilesOfFolder(File directory, boolean filesOnly) {
		
		if (directory == null) {
			System.err.println("FileTools: directory is null");
			return null;
		}
		if (! directory.isDirectory()) {
			System.err.println("FileTools: argument is not directory");
			return null;		
		}
		ArrayList<File> resultList = new ArrayList<File>();

        File[] fileList = directory.listFiles();        
        if (fileList == null) {
			System.err.println("FileTools: No files in directory");
			return null;		
        }
        int len = fileList.length;
        for (int i = 0; i < len; i++) {
        	File file = fileList[i];
        	//resultList.add(file);
            if (file.isFile()) {
            	resultList.add(file);
                //System.out.println(file.getAbsolutePath());
            } else if (file.isDirectory()) {
            	if (filesOnly == false){
            		resultList.add(file);
            	}
            	if (listAllFilesOfFolder(file, filesOnly) != null){
            		resultList.addAll(listAllFilesOfFolder(file, filesOnly));
            	}
            }
        }
        return resultList;
	}
	
	/**
	 * Get all names of files of a directory including sub-directories
	 * as an unsorted list. 
	 * 
	 * @param directoryName		the name of the directory
	 * @param filesOnly			true if only file should be included
	 * 							false if files and directories should be included
	 * 
	 * @return	an unsorted list of all included file names 
	 * 			or null if the directory name is null 
	 * 			or refers not to a directory 
	 */
	private static ArrayList <String> listAllFileNamesOfFolder(String directoryName, boolean filesOnly) {
		
		if (directoryName == null) {
			System.err.println("FileTools: directory nam is null");
			return null;
		}
		File directory = new File(directoryName);
		if (! directory.isDirectory()) {
			System.err.println("FileTools: argument is not directory");
			return null;		
		}
		ArrayList<String> resultList = new ArrayList<String>();

		File[] fileList = directory.listFiles();     
		if (fileList == null) {
			new UnexpectedValueException("fileList", "File[]", "is null").printDescription();
			return null;
		}
		int len = fileList.length;
		for (int i = 0; i < len; i++) {
			File file = fileList[i];
        	//resultList.add(file.getAbsolutePath());
            if (file.isFile()) {
            	resultList.add(file.getAbsolutePath());
                //System.out.println(file.getAbsolutePath());
            } else if (file.isDirectory()) {
            	if (filesOnly == false){
            		resultList.add(file.getAbsolutePath());
            	}
                resultList.addAll(listAllFileNamesOfFolder(file.getAbsolutePath(), filesOnly));
            }
        }
        return resultList;
	}
	
	/**
	 * Get all names of files of a directory including sub-directories
	 * as an array of Strings. 
	 * 
	 * @param directoryName		the name of the directory
	 * @param filesOnly			true if only file should be included
	 * 							false if files and directories should be included
	 * 
	 * @return	an array of all included file names 
	 * 			or null if the directory name is null 
	 * 			or refers not to a directory 
	 */
	public static String[] getAllFileNamesOfFolder(String directoryName, boolean filesOnly) {
		ArrayList <String> list = listAllFileNamesOfFolder(directoryName, filesOnly);
		if (list == null) {
			return null;
		} else {
			String[] array = new String[list.size()];
			return list.toArray(array);
		}
	}
	
	/**
	 * Compare files
	 * 
	 * @param fileName1	file name with path of first file
	 * @param fileName2	file name with path of seconde file
	 * @return	true, if files are equal, false otherwise
	 * @throws IOException
	 */
	public static boolean compareFiles(String fileName1, String fileName2) 
			throws IOException {
			File f1= new File(fileName1);
			File f2= new File(fileName2);
			long len = f1.length();
			if ( ! (len == f2.length() ) ) {
				return false;
			}
			if (len < 1024*1024) {
				byte[] b1 = Files.readAllBytes(f1.toPath());
				byte[] b2 = Files.readAllBytes(f2.toPath());
				if ( ! (Arrays.equals(b1,  b2))) {
					return false;
				} else {
					return true;
				}
			} else if (len < 1024*1024*32) {
				return compareLargeFiles(fileName1, fileName2);
			} else {
				return compareExtremeLargeFiles(fileName1, fileName2);
			}	
	}
	
	/**
	 * Compare large files
	 * 
	 * @param fileName1	file name with path of first file
	 * @param fileName2	file name with path of seconde file
	 * @return	true, if files are equal, false otherwise
	 * @throws IOException
	 */
	private static boolean compareLargeFiles(String fileName1, String fileName2) 
			throws IOException {
		try (final FileChannel fca = FileChannel.open(Paths.get( fileName1), StandardOpenOption.READ);
	             final FileChannel fcb = FileChannel.open(Paths.get( fileName2), StandardOpenOption.READ)) {
	            final MappedByteBuffer mbba = fca.map(FileChannel.MapMode.READ_ONLY, 0, fca.size());
	            final MappedByteBuffer mbbb = fcb.map(FileChannel.MapMode.READ_ONLY, 0, fcb.size());
	            return mbba.equals(mbbb);
	        }
	}
	
	/**
	 * Compare extreme large files
	 * 
	 * @param fileName1	file name with path of first file
	 * @param fileName2	file name with path of seconde file
	 * @return	true, if files are equal, false otherwise
	 * @throws IOException
	 */
	private static boolean compareExtremeLargeFiles(String fileName1, String fileName2) 
			throws IOException {
		try (final FileChannel fca = FileChannel.open(Paths.get( fileName1), StandardOpenOption.READ);
	             final FileChannel fcb = FileChannel.open(Paths.get( fileName2), StandardOpenOption.READ)) {

			  ByteBuffer buffer1 = ByteBuffer.allocate(bufferSize / 2);
			  ByteBuffer buffer2 = ByteBuffer.allocate(bufferSize / 2);

			  while (fca.read(buffer1) > 0  && fcb.read(buffer2) > 0) {
				  if ( ! buffer1.equals(buffer2)) {
					  return false;
				  }
			  }
		}
		return true; // TODO test
	}

	/**
	 * Get all files of a directory including sub-directories
	 * as an array of Strings. 
	 * 
	 * @param directory			the directory
	 * @param filesOnly			true if only file should be included
	 * 							false if files and directories should be included
	 * 
	 * @return	an array of all included files 
	 * 			or null if the directory name is null 
	 * 			or refers not to a directory 
	 */
	public static File[] getAllFilesOfFolder(File directory, boolean filesOnly) {
		ArrayList <File> list = listAllFilesOfFolder(directory, filesOnly);
		if (list == null) {
			return null;
		} else {
			File[] array = new File[list.size()];
			return list.toArray(array);
		}
	}
	
	/**
	 * Get the file size in bytes
	 * 
	 * @param file	the  file
	 * @return			the number of bytes (long value)
	 */
	public final static long getFileSize(File file) {
		RandomAccessFile raf = null;
		if (file.isDirectory() ) {
			return 0;
		}
/*		if (file.getAbsolutePath().endsWith(".zip") || file.getAbsolutePath().endsWith(".jar")) {
			return ZipStuff.getZipFileSize(file.getAbsolutePath(), true);
		} */
		long result = 0;
		try {            			
			raf = new RandomAccessFile(file, "r");	            		
			result = raf.length();
			raf.close(); 
		} catch (FileNotFoundException e) { 
			// this will be thrown if file is not file and not directory
			TestLog.e(FileTools.class, e.toString() + ", " + e.getMessage() + ": " + file.getAbsolutePath());
			TestLog.ve(FileTools.class, e.getLocalizedMessage(), 5);
		} catch (IOException e) {
			TestLog.e(FileTools.class, e.toString() + ", " + e.getMessage() + ": " + file.getAbsolutePath());
			TestLog.ve(FileTools.class, e.getLocalizedMessage(), 5);
		} catch (Exception e) {
			TestLog.e(FileTools.class, e.toString() + ", " + e.getMessage() + ": " + file.getAbsolutePath());
			TestLog.ve(FileTools.class, e.getLocalizedMessage(), 5);
		}
		return result;		
	}
	
	/**
	 * Get the file size in bytes
	 * 
	 * @param fileName	the name of the file
	 * @return			the number of bytes (long value)
	 */
	public final static long getFileSize(String fileName) {

		File file = new File(fileName);
		return getFileSize(file);
	}
	/**
	 *  Get the number and overall size of files.
	 *  There is no warning for long execution time!
	 * 
	 * @param fileNames		an array of file names
	 * @return					an array of long values: the first value [0] is the
	 * 							number of the containing files (no directories), 
	 * 							the second [1] is the size of all included files. 
	 * 							There is no warning for long execution time!
	 */
	public static final long[] getNumberAndSize(String[] fileNames){
		
		long[] result = new long[2];
		
		if (fileNames == null){
			System.err.println("FileTools getNumberAndSize[]: fileNames null");
			return null;
		}
		
		int len = fileNames.length;
		if (len == 0) {
			result[0] = 0;
			result[1] = 0;
			return result;
		}
		
		RandomAccessFile raf = null;
		
		for (int i = 0; i < len; i++) {
			
			if (! new File(fileNames[i]).isFile()){
				// do not count directories and other
				continue;
			}
			
    		try {            			
				raf = new RandomAccessFile(fileNames[i], "r");	            		
        		result[1] += raf.length();
        		raf.close(); 
			} catch (FileNotFoundException e) {
				System.err.println(e.toString() +" FileTools getNumberAndSize[]: " +  e.getMessage() + ", " + fileNames[i]);
				TestLog.ve(FileTools.class, e.getLocalizedMessage(), 5);
			} catch (IOException e) {
				System.err.println(e.toString() + " FileTools getNumberAndSize[]: " +  e.getMessage() + ", " + fileNames[i]);
				TestLog.ve(FileTools.class, e.getLocalizedMessage(), 5);
			} catch (Exception e) {
				System.err.println(e.toString() + " - " + e.getMessage() + ", " + fileNames[i]);
				TestLog.ve(FileTools.class, e.getLocalizedMessage(), 5);
				//e.printStackTrace();								
			}
    		// only count files, not folders:
    		result[0] ++;
		}
		return result;
	}
	
	/**
	 *  Get the number of files of a directory - inclusive 
	 *  all files of sub-directories, but no directory itself.
	 *  There is no warning for long execution time!
	 * 
	 * @param directoryName		the name of the directory to check
	 * @return					an array of long values: the first value [0] is the
	 * 							number of the containing files (no directories), 
	 * 							the second [1] is the size of all included files. 
	 * 							There is no warning for long execution time!
	 */
	public static final long[] getNumberAndSize(String directoryName) {
		
		File directory = new File(directoryName);
		RandomAccessFile raf = null;

		long[] result = new long[2];

        File[] children = directory.listFiles();        
        if (children != null) {        	

            for (int i = 0; i < children.length; i++) {
            	if (children[i].isDirectory() ) {
            		long[] dirResult = getNumberAndSize(children[i].getAbsolutePath());
            		// function was broken because of extremeSizeWarning
            		// this avoids NullPointerException
            		if (dirResult == null) {
            			return null;
            		}
            		result[0] += dirResult[0];
            		result[1] += dirResult[1];    				
 
            	} else if (children[i].isFile() ) {

            		try {            			
						raf = new RandomAccessFile(children[i], "r");	            		
	            		result[1] += raf.length();
	            		raf.close(); 
					} catch (FileNotFoundException e) {
						System.err.println("FileTools getNumberAndSize: " +  e.getMessage() + ", " + children[i]);
						TestLog.ve(FileTools.class, e.getLocalizedMessage(), 5);
					} catch (IOException e) {
						System.err.println("FileTools getNumberAndSize: " +  e.getMessage() + ", " + children[i]);
						TestLog.ve(FileTools.class, e.getLocalizedMessage(), 5);
					} catch (Exception e) {
						System.err.println(e.toString() + " - " + e.getMessage() + ", " + children[i]);
						TestLog.ve(FileTools.class, e.getLocalizedMessage(), 5);							
					}
            		// only count files, not folders:
            		result[0] ++;
            	}
            }
        }       
        return result;
    } 
	
	/**
	 * Overwrite file content and delete file.
	 * 
	 * @param fileName		the file name of the file to delete
	 * @param secureWipe	fill content with random data (expensive)
	 * 						after filling with 0x00 and 0xFF
	 * @return		an error message if an error occurred, otherwise null
	 */
	public final static String wipeFile(String fileName, boolean secureWipe) {
		
		String errorMessage = null;
		
		if (fileName == null) {
			new UnexpectedValueException("fileName", "String", "is null").printDescription();
			return "no file name";
		}

		File file = new File(fileName);

		if (file.isDirectory()) {
			// delete only empty directories: 
			File[] list = file.listFiles();
			if (list == null || list.length == 0) {
				file.delete();
			}
			return "is folder";
		}
		if (file.exists() && file.canRead() && file.canWrite()){				

			if (fileName.endsWith(".zip") || fileName.endsWith(".jar")) {
				ZipStuff.secureWipeZipFile(fileName);
				return null;
			}
			RandomAccessFile raf = null;
			long fileLength = 0;
			try {
				raf = new RandomAccessFile(file, "rw");
				fileLength = raf.length();
			} catch (FileNotFoundException e1) {
				System.err.println("Error " + fileName + "\n" + "\nFileTools.wipeFile() "
						+ e1.toString() + ", " + e1.getMessage() + "\nFileTools.wipeFile() ");
				TestLog.ve(FileTools.class, e1.getLocalizedMessage(), 5);
				return errorMessage = "Error " + fileName + "\n"
						+ e1.toString() + ", " + e1.getMessage();
			} catch (IOException e) {
				System.err.println("Error " + fileName + "\n" + "\nFileTools.wipeFile() "
						+ e.toString() + ", " + e.getMessage() + "\nFileTools.wipeFile() ");
				TestLog.ve(FileTools.class, e.getLocalizedMessage(), 5);
				return errorMessage = "Error " + fileName + "\n"
						+ e.toString() + ", " + e.getMessage() ;			
			}
			// number of buffer blocks
			long blockNumber = fileLength / bufferSize;
			// buffer containing bytes to overwrite the content
			byte[] buffer = null;
			// the rest of the file after buffer blocks
			byte[] restBuffer = new byte[(int) (fileLength - (blockNumber * bufferSize))];
			int restLength = restBuffer.length;
			if ( blockNumber > 0 ) { // file > 16 MB
				buffer = new byte[bufferSize];// initialized with 0
			}
			try {
				// Overwrite with zero
				for (long blockCounter = 0; blockCounter < blockNumber; blockCounter++) {	    		 
					raf.seek(blockCounter * bufferSize);
					raf.write(buffer);
				}
				if (restLength > 0) {
					raf.seek( fileLength - restLength);
					raf.write(restBuffer);	    		 
				}

				// Overwrite with 1
				if (buffer != null) {
					Arrays.fill(buffer, (byte) 0xFF);
				}
				Arrays.fill(restBuffer, (byte) 0xFF);
				for (long blockCounter = 0; blockCounter < blockNumber; blockCounter++) {	    		 
					raf.seek(blockCounter * bufferSize);
					raf.write(buffer);
				}
				if (restLength > 0) {
					raf.seek( fileLength - restLength);
					raf.write(restBuffer);	    		 
				}	    	 

				if (secureWipe == true) {
					/// Overwrite with random byte
					// fill file with random data
					for (long blockCounter = 0; blockCounter < blockNumber; blockCounter++) {	
						// fill buffer with random bytes
						PeaProperties.getSecureRandom().nextBytes(buffer);	    		 
						raf.seek(blockCounter * bufferSize);
						raf.write(buffer);
					}
					if (restLength > 0) {
						// fill buffer with random bytes
						PeaProperties.getSecureRandom().nextBytes(restBuffer);
						raf.seek( fileLength - restLength);
						raf.write(restBuffer);	    		 
					}
				} 
				raf.close();
			} catch (IOException ioe) {
				System.err.println("No access to file " + fileName);
				TestLog.ve(FileTools.class, ioe.getLocalizedMessage(), 5);
				return errorMessage = "Deletion failed. No access to file " + fileName  + "\nFileTools.wipeFile() ";
			} catch (Exception e) {
				System.err.println("Unexpected error " + fileName + "\n"
						+ e.toString() + ", " + e.getMessage() + "\nFileTools.wipeFile() ");
				TestLog.ve(FileTools.class, e.getLocalizedMessage(), 5);
				return errorMessage = "Unexpected error " + fileName + "\n"
						+ e.toString() + ", " + e.getMessage() ;
			}
			file.delete();	    	 
	    	 
	     } else {
	    	 System.err.println("No access to file " + fileName);
	    	 return errorMessage = "Deletion failed. No access to file " + fileName;
	     }
	    return errorMessage;		
	}
	
	/**
	 * Copy a file via FileChannel and create the destination
	 * file if necessary
	 * 
	 * @param sourceFile		the file to copy
	 * @param destinationFile	the new file
	 * 
	 * @return	a localized error message if copy failed
	 * 			or null for success
	 */
	public static String copyFiles(File sourceFile, File destinationFile) {		

		FileChannel sourceChannel = null;
		FileChannel destChannel = null;
		FileInputStream fileInput = null; 
		FileOutputStream fileOutput = null;
		try {
			if (destinationFile.exists() == false) {
				destinationFile.getParentFile().mkdirs(); 
				destinationFile.createNewFile();
			}
			fileInput = new FileInputStream(sourceFile);
			sourceChannel = fileInput.getChannel();
			fileOutput = new FileOutputStream(destinationFile);
			destChannel = fileOutput.getChannel();
			destChannel.transferFrom(sourceChannel, 0, sourceChannel.size());
		} catch (FileNotFoundException e) {
			System.err.println(new ExceptionHelper(e).getInfos());	
			TestLog.ve(FileTools.class, e.getLocalizedMessage(), 5);
			return sourceFile.getAbsolutePath() + " -> " 
					+ destinationFile.getAbsolutePath() + ":\n " 
			+ e.getLocalizedMessage();
		} catch (IOException e) {
			System.err.println(new ExceptionHelper(e).getInfos());	
			TestLog.ve(FileTools.class, e.getLocalizedMessage(), 5);
			return sourceFile.getAbsolutePath() + " -> " 
					+ destinationFile.getAbsolutePath() + ":\n " 
			+ e.getLocalizedMessage();
		} finally{
			if (fileInput != null) {
				try {
					fileInput.close();
				} catch (IOException e) {
					System.err.println("Closing stream failed: "+ sourceFile.getAbsolutePath());
					TestLog.ve(FileTools.class, e.getLocalizedMessage(), 5);
					System.err.println(new ExceptionHelper(e).getInfos());	
				}
			}
			if (sourceChannel != null) {
				try {
					sourceChannel.close();
				} catch (IOException e) {
					System.err.println("Closing channel failed... ");
					System.err.println(new ExceptionHelper(e).getInfos());	
				}
			}
			if (fileOutput != null) {
				try {
					fileOutput.close();
				} catch (IOException e) {
					System.err.println("Closing stream failed: "+ destinationFile.getAbsolutePath());
					System.err.println(new ExceptionHelper(e).getInfos());	
				}
			}
			if (destChannel != null){
				try {
					destChannel.close();
				} catch (IOException e) {
					System.err.println("Closing channel failed... ");
					System.err.println(new ExceptionHelper(e).getInfos());	
				}
			}
		}
		return null;
	}
	
	/**
	 * Create an empty new file with all required directories. 
	 * This will overwrite an existing file without confirmation 
	 * (confirmation must be done before by askToOverwriteExistingFile)
	 * 
	 * @param fileNameWithPath	the name of the file to create
	 * 
	 * @return	the file or null 
	 */
	public static File createEmptyFile(String fileNameWithPath) {
		File file = new File(fileNameWithPath);
		if (! file.exists()) {
			// create required directories
			file.getParentFile().mkdirs();
			// create file
			try {
				file.createNewFile();
			} catch (IOException e) {
				System.err.println("Creating file failed: "+ fileNameWithPath);
				TestLog.ve(FileTools.class, e.getLocalizedMessage(), 5);
			}
		} 
		return file;
	}
	
	/**
	 * Find he lowest common folder of files. 
	 * 
	 * @param paths	the Paths to check
	 * 
	 * @return	the lowest common folder or null
	 * 			if there is no common root
	 */
	public final static Path findCommonParent(Path[] paths) {

		try {
			Path parentFolder = paths[0].getParent();
			int pathsLen = paths.length;
			boolean isChild = true;
			while (parentFolder != null) {
				//TestLog.o(FileTools.class, parentFolder.toString());
				for (int i = 1; i < pathsLen; i++){
					Path p = paths[i];
					if ( ! p.startsWith(parentFolder)) {
						//TestLog.o(FileTools.class, p.toString());
						isChild = false;
						break;
					} 
				}	
				if (isChild == true) {
					return parentFolder;
				} else {
					parentFolder = parentFolder.getParent();
					isChild = true;
				}
			}
			return parentFolder;		
		} catch (Exception e){
			TestLog.ve(FileTools.class,  e.toString() + " - " + e.getLocalizedMessage(), 5);
			return null;
		}
	}
	/**
	 * Get an input stream from the resource folder
	 * inside the jar archive
	 * 
	 * @param fileName	file name without leading separator
	 * 
	 * @return	the InputStream or null
	 */
	public static InputStream getResourceFileFromJar(String fileName) {
		// try to get input stream from inside jar archive
		InputStream is = FileTools.class.getResourceAsStream("/resources/"+ fileName);
		if (is == null) {
			TestLog.ve(FileTools.class, "Could not find resource file " + fileName);
		}
		return is;
		/*		URL fileUrl = FileTools.class.getClassLoader().getResource("resources/"+ fileName);
		if (fileUrl == null) {
			TestLog.v(FileTools.class, "Can't get URL from file name from inside jar archive", 4);
			return null;
		}*/
	}
	/**
	 * Get a file from resource folder
	 * of the file system
	 * 
	 * @param fileName	file name without leading separator
	 * 
	 * @return	the file or null
	 */
	public static File getResourceFileFromFileSystem(String fileName) {
		File file = new File(System.getProperty("user.dir") // current path
				+ File.separator + "resources" + File.separator + fileName);
		//TestLog.e(FileTools.class, "fileName: " + file.getAbsolutePath() + ", file exists: " + file.exists());
		return file;
	}

	/**
	 * Get an input stream from resource folder
	 * from inside the jar archive or
	 * a file from file system (check by instanceof)
	 * 
	 * @param fileName		file name without leading separator
	 * @param insideJarFirst	check first inside jar archive
	 * 
	 * @return	a file (file system) or input stream (jar archive)  or null
	 */
	public static Object getResourceFileFromJarOrFileSystem(String fileName, boolean insideJarFirst) {
		if (fileName == null || fileName.length() == 0) {
			TestLog.e(FileTools.class, "Missing file name", 4);
			return null;
		}
		//TestLog.e(FileTools.class, "fileName: " + fileName);
		File file = null;
		if (insideJarFirst == true) {
			InputStream is = FileTools.getResourceFileFromJar(fileName);
			if (is == null) {
				return FileTools.getResourceFileFromFileSystem(fileName);
			} else {
				return is;
			}
		} else {
			
			file = FileTools.getResourceFileFromFileSystem(fileName);
			if (file == null || file.exists() == false) {
				 return FileTools.getResourceFileFromJar(fileName);
			} else {
				return file;
			}
		}
	}
	
    /**
     * Check if files already exists and ask to overwrite
     * 
     * @param fileNames	the file names to check
     * 
     * @return	the file names to write (overwriting confirmed or
     * 			not yet exist)
     */
	public static String[] askToOverwriteFiles(String[] fileNames) {		
		return PeaProperties.getVmBridge().askToOverwriteFiles(fileNames);
	}
	
    /**
     * Check if a file already exists and ask to overwrite
     * 
     * @param file	the file to check
     * 
     * @return	true, if the file should be overwritten or
     * 			if the file does not exist, 
     * 			false if the file should not be written
     */
	public static boolean askToOverwriteFile(File file) {
		return PeaProperties.getVmBridge().askToOverwriteFile(file);
	}
	
	/**
	 * Replace all characters, that may cause problems in file names
	 * with underscore 
	 * 
	 * @param fileNameNoPath	the file name - WITHOUT path!!!
	 * 
	 * @return	file name without problematic characters
	 */
	public static String replaceCharsForFileName(String fileNameNoPath) {
		
		// replace all characters except...
		return fileNameNoPath.replaceAll("[^a-zA-Z0-9\\.\\-]", "_");
		
	}
	
	/**
	 * Checks if the content of two file is equal
	 * by file size and optionally by content. 
	 * If files are encrypted, the content is never equal
	 * because of different/unique nonce. 
	 * 
	 * @param fileName1	name of file 1 to check
	 * @param fileName2	name of file 2 to check
	 * @param encrypted	true if the file are encrypted
	 * 
	 * @return	0 if content is equal, 1 if different, -1 comparison if failed
	 */
/*	public static int checkEqualContent(String fileName1, String fileName2, boolean encrypted) {
		
		if (fileName1.equals(fileName2)) {
			System.err.println("FileTools: Comparison of equal files: " + fileName1);
			return 0;
		}
		File file1 = new File(fileName1);
		File file2 = new File(fileName2);
		if ( ! file1.exists() || ! file2.exists()) {
			new UnexpectedValueException("File", "file1 or file2", "do not exist").printDescription();
			return -1;
		}
		if ( ! file1.isFile() || ! file2.isFile()) {
			new UnexpectedValueException("File", "file1 or file2", "are not files").printDescription();
			return -1;
		}
		
		long file1Len = file1.length();
		if (file1Len != file2.length()) {
System.out.println("File length differs: " + fileName1 + " and " + fileName2);
			return 1;// not equal
		}		
		if (encrypted == true) {
			// can't check the content because of different nonce
			return 0; // probably equal
		}
		
		// for small file size: readAllBytes, for large: buffer
		int bufferSize = 8192;
		if (file1Len <= bufferSize) {

			try {
				byte[] f1 = Files.readAllBytes(file1.toPath());
				byte[] f2 = Files.readAllBytes(file2.toPath());
				if (Arrays.equals(f1, f2)){
					return 0;
				} else {
					return 1;
				}
			} catch (IOException e) {
				System.err.println(new ExceptionHelper(e).getInfos());
				return -1;
			}

		} else { // large files > 8192 bytes
			
			// TODO test
			RandomAccessFile raf1 = null;
			RandomAccessFile raf2 = null;
			
			try {
				raf1 = new RandomAccessFile(file1, "r");
				raf2 = new RandomAccessFile(file2, "r");

				byte[] dest1      = new byte[bufferSize];
				byte[] dest2      = new byte[bufferSize];
				int    offset    = 0;
				
				while (offset + bufferSize <= file1Len) {
					raf1.seek(offset);
					raf2.seek(offset);
					//int bytesRead1 = 
					raf1.read(dest1);
					//int bytesRead1 = raf1.read(dest1, offset, bufferSize);
					//int bytesRead2 = 
					raf2.read(dest2);
					if (Arrays.equals(dest1, dest2)){
						// 
					} else {
						raf1.close();
						raf2.close();
						return 1;						
					}					
					offset += bufferSize;
				}
				raf1.close();
				raf2.close();
				return 0;
			} catch (FileNotFoundException e) {
				System.err.println(new ExceptionHelper(e).getInfos());
				return -1;
			} catch (IOException e) {
				System.err.println(new ExceptionHelper(e).getInfos());
				return -1;
			}
		}		
	}*/
}
