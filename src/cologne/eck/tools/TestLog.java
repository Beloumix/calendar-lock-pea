package cologne.eck.tools;

/*
 * Peafactory - Production of Password Encryption Archives
 * Copyright (C) 2021  Axel von dem Bruch
 * 
 * This library is free software; you can redistribute it and/or modify it 
 * under the terms of the GNU General Public License as published 
 * by the Free Software Foundation; either version 2 of the License, 
 * or (at your option) any later version.
 * This library is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
 * or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU General Public License for more details.
 * See:  http://www.gnu.org/licenses/gpl-2.0.html
 * You should have received a copy of the GNU General Public License 
 * along with this library.
 */

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintStream;

import cologne.eck.all_peas.data.PeaProperties;

public class TestLog {
	
	/**
	 * Print log messages if the PEA runs in test mode. 
	 * Copy stdout and stderr in a file if the PEA
	 * runs in test mode. 
	 */
	
	private static final String logFileName = "error.log";
	private static PrintStream fileOut = null;
	
	/**
	 * Create a log file for stderr and stdout
	 * 
	 * @return	true for success (file exists and write access)
	 */
	public static boolean createErrLogFile() {
		// Create a new file for every session
		File f = new File(System.getProperty("user.dir") + File.separator + logFileName);
		System.out.println("Log file: "+ f.getAbsolutePath());
		f.getParentFile().mkdirs(); 
		try {
			f.createNewFile();
			fileOut = new PrintStream(new FileOutputStream(f, false));
		} catch (IOException e) {
			System.err.println("Redirection of stderr to file failed: " + e.getLocalizedMessage());
		} catch (Exception e) {
			System.err.println("Redirection of stderr to file failed: " + e.getLocalizedMessage());
		}				
		if (f.exists() && f.canWrite()) {
			return true;
		} else {
			return false;
		}
	} 
	
	/**
	 * Log a message: In test mode along with '<', class name,
	 * method and line number, in normal mode only the message
	 * 
	 * @param c			the class (X.class)
	 * @param message	the message
	 */
	public static final void o(Class<?> c, String message) {
		
		if (PeaProperties.isTestMode() == true) {// test mode: print with class and line
			printOneLine(c.getSimpleName(), message, false);
		} else {
			System.out.println(message); // print without class and line
		}
	}
	
	/**
	 * Log a message: In test mode along with '<', class name,
	 * method and line number, in normal mode only the message
	 * 
	 * @param c			the class (X.class)
	 * @param message	the message
	 */
	public static final void o(String className, String message) {
		
		if (PeaProperties.isTestMode() == true) {// test mode
			printOneLine(className, message, false);
		} else {
			System.out.println(message);
		}
	}

	/**
	 * Log a message: In test mode along with '<', class name,
	 * method and line number, in normal mode only the message
	 * 
	 * @param c			the class (X.class)
	 * @param message	the message
	 */
	public static final void o(Class<?> c, String message, int lineNumber) {
		
		if (PeaProperties.isTestMode() == true) {// test mode
			printMultiLine("   " + c.getSimpleName(), message, lineNumber, false);
		} else {
			System.out.println(message);
		}
	}
	
	/**
	 * Log a message in test mode. 
	 * This will write the class name, method name and line number along
	 * with the message to the standard output if the PEA runs in test mode.
	 * 
	 * @param c			the class (X.class)
	 * @param message	the message
	 */
	public static final void v(Class<?> c, String message) {
		
		if (PeaProperties.isTestMode() == true) {// test mode
			printOneLine("   " + c.getSimpleName(), message, false);
		}
	}
	
	/**
	 * Log a message in test mode. 
	 * This will write the class name, method name and line number along
	 * with the message to the standard output if the PEA runs in test mode.
	 * 
	 * @param className	the class name This.getClass().getSimpleName()
	 * @param message	the message
	 */
	public static final void v(String className, String message) {
		
		if (PeaProperties.isTestMode() == true) {// test mode
			printOneLine("   " + className, message, false);
		}
	}
	
	/**
	 * Log a message in test mode. 
	 * This will write the class name, method name and line number along
	 * with the message to the standard output if the PEA runs in test mode.
	 * 
	 * @param c			the class (X.class)
	 * @param message	the message
	 * @param lineNumber	the number of lines from stack to print
	 * 						- only cologne.eck elements are printed
	 */
	public static final void v(Class<?> c, String message, int lineNumber) {
		
		if (PeaProperties.isTestMode() == true) {// test mode
			printMultiLine("   " + c.getSimpleName(), message, lineNumber, false);
		}
	}
	/**
	 * Log a message in test mode. 
	 * This will write the class name, method name and line number along
	 * with the message to the standard output if the PEA runs in test mode.
	 * 
	 * @param className		the simple class name
	 * @param message		the message
	 * @param lineNumber	the number of lines from stack to print
	 * 						- only cologne.eck elements are printed
	 */
	public static final void v(String className, String message, int lineNumber) {

		if (PeaProperties.isTestMode() == true) {// test mode
			printMultiLine("   " + className, message, lineNumber, false);
		}
	}
	
	/**
	 * Log an error message if program runs in test mode
	 * 
	 * @param c			the class (X.class)
	 * @param message	the message
	 */
	public static final void ve(Class<?> c, String message) {
		
		if (PeaProperties.isTestMode() == true) {
			printOneLine(c.getSimpleName(), message, true);
		}
	}
	
	/**
	 * Log an error message if program runs in test mode
	 * This will write the class name, method name and line number along
	 * with the message to the error output and a number of stack lines.
	 * 
	 * @param className		the class (X.class)
	 * @param message	the message
	 * @param lineNumber	the number of lines from stack to print
	 * 						- only cologne.eck elements are printed
	 */
	public static final void ve(Class<?> c, String message, int lineNumber) {

		if (PeaProperties.isTestMode() == true) {
			printMultiLine(c.getSimpleName(), message, lineNumber, true);
		}
	}
	
	/**
	 * Log an error message if program runs in test mode
	 * This will write the class name, method name and line number along
	 * with the message to the error output.
	 * 
	 * @param className		the class name this.getClass().getSimpleName()
	 * @param message	the message
	 */
	public static final void ve(String className, String message) {
		
		if (PeaProperties.isTestMode() == true) {// test mode
			printOneLine(className, message, true);
		}
	}
	
	/**
	 * Log an error message if program runs in test mode
	 * This will write the class name, method name and line number along
	 * with the message to the error output and a number of stack lines.
	 * 
	 * @param className		the class name this.getClass().getSimpleName()
	 * @param message	the message
	 * @param lineNumber	the number of lines from stack to print
	 * 						- only cologne.eck elements are printed
	 */
	public static final void ve(String className, String message, int lineNumber) {

		if (PeaProperties.isTestMode() == true) {
			printMultiLine(className, message, lineNumber, true);
		}
	}
	
	/**
	 * Log a message. 
	 * In test mode, this will write the class name, method name and line number along
	 * with the message to the error output.
	 * 
	 * @param c			the class MethodHandles.lookup().lookupClass()
	 * @param message	the message
	 */
	public static final void e(Class<?> c, String message) {
		
		if (PeaProperties.isTestMode() == true) { // print also with class and line
			printOneLine(c.getSimpleName(), message, true);
		} else {
			System.err.println(message); // print without class and line
		}
	}
	
	/**
	 * Log a message. 
	 * In test mode, this will write the class name, method name and line number along
	 * with the message to the error output.
	 * 
	 * @param className	the class name
	 * @param message	the message
	 */
	public static final void e(String className, String message) {

		if (PeaProperties.isTestMode() == true) { // print also with class and line
			printOneLine(className, message, true);
		}  else {
			System.err.println(message); // print without class and line
		}
	}
	
	/**
	 * Log a message. 
	 * In test mode, this will write the class name, method name and line number along
	 * with the message to the error output.
	 * 
	 * @param c			the class MethodHandles.lookup().lookupClass()
	 * @param message	the message
	 * @param lineNumber	the number of line to print
	 */
	public static final void e(Class<?> c, String message, int lineNumber) {

		if (PeaProperties.isTestMode() == true) { // print also with class and line
			printMultiLine(c.getSimpleName(), message, lineNumber, true);
		} else {
			System.err.println(message); // print without class and line
		}
	}
	
	/**
	 * Log a message. 
	 * In test mode, this will write the class name, method name and line number along
	 * with the message to the error output.
	 * 
	 * @param className	the class name
	 * @param message	the message
	 * @param lineNumber	the number of line to print
	 */
	public static final void e(String className, String message, int lineNumber) {

		if (PeaProperties.isTestMode() == true) { // print also with class and line
			printMultiLine(className, message, lineNumber, true);
		} else {
			System.err.println(message); // print without class and line
		}
	}
	
	//==================Helper functions==================================
	
	private static final void printOneLine(String className, String message, boolean err) {
		StackTraceElement[] ste = Thread.currentThread().getStackTrace();
		PrintStream std = null;
		if (err == true) {
			std = System.err;
		} else {
			std = System.out;
		}
		std.print(className);
		std.print("." + ste[3].getMethodName());
		std.print(" " + ste[3].getLineNumber() + ": ");
		std.println(message);

		if (PeaProperties.isTestMode() == true) {// test mode
			if (fileOut ==  null) {
				boolean createResult = createErrLogFile();
				if (createResult == false) {
					PeaProperties.setWorkingMode("");
					std.println("Couldn't create log file... \n Reset test mode...");
					return;
				}
				if (fileOut == null) {
					System.err.println("File for output stream missing");
				}
			}
			if (err == true) {
				fileOut.print("E  ");
			} 
			fileOut.print(className);
			fileOut.print("." + ste[3].getMethodName());
			fileOut.print(" " + ste[3].getLineNumber() + ": ");
			fileOut.println(message);
		}
	}
	
	private static final void printMultiLine(String className, String message, int lineNumber, boolean err) {
		StackTraceElement[] ste = Thread.currentThread().getStackTrace();
		int stackLen = ste.length;
		int lineCounter = 0;
		
		PrintStream std = null;
		if (err == true) {
			std = System.err;
		} else {
			std = System.out;
		}
		
		std.print(className);
		std.print("." + ste[3].getMethodName());
		std.print(" " + ste[3].getLineNumber() + ": ");
		std.println(message);
		for (int i = 3; i < stackLen; i++) {
			if (i == 0) { // always add first line
				std.println("\tat " + ste[i]);
			}
			if ( ste[i].getClassName().startsWith("cologne.eck")) {
				std.println("\tat " + ste[i]);
				lineCounter++;
			}
			if (lineCounter > lineNumber) {
				break;
			}
		}
		if (PeaProperties.isTestMode() == true) {// test mode
			if (fileOut ==  null) {
				boolean createResult = createErrLogFile();
				if (createResult == false) {
					PeaProperties.setWorkingMode("");
					std.println("Couldn't create log file... \n Reset test mode...");
					return;
				}
			}
			if (err == true) {
				fileOut.print("E  ");
			}			
			fileOut.print(className);
			fileOut.print("." + ste[3].getMethodName());
			fileOut.print(" " + ste[3].getLineNumber() + ": ");
			fileOut.println(message);
			for (int i = 3; i < stackLen; i++) {
				if (i == 0) { // always add first line
					fileOut.println("\tat " + ste[i]);
				}
				if ( ste[i].getClassName().startsWith("cologne.eck")) {
					fileOut.println("\tat " + ste[i]);
					lineCounter++;
				}
				if (lineCounter > lineNumber) {
					break;
				}
			}
		}
	}		
	
	/**
	 * Get the file name of the log file.
	 * The file is in the current directory.
	 * 
	 * @return	the log file name (without path)
	 */
	public static String getLogFileName() {
		return logFileName;
	}
}
