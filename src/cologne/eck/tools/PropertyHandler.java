package cologne.eck.tools;

import java.awt.Dimension;
import java.awt.Toolkit;

/*
 * PeaFactory - Production of Password Encryption Archives
 * Copyright (C) 2015  Axel von dem Bruch
 * 
 * This library is free software; you can redistribute it and/or modify it 
 * under the terms of the GNU General Public License as published 
 * by the Free Software Foundation; either version 2 of the License, 
 * or (at your option) any later version.
 * This library is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
 * or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU General Public License for more details.
 * See:  http://www.gnu.org/licenses/gpl-2.0.html
 * You should have received a copy of the GNU General Public License 
 * along with this library.
 */


import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Properties;

import javax.swing.JFrame;

import cologne.eck.all_peas.control.PeaControl;
import cologne.eck.all_peas.data.PeaProperties;
import cologne.eck.all_peas.vm_specific.JREProperties;
import cologne.eck.peafactory.crypto.AlgoParamHandler;
import cologne.eck.peafactory.crypto.kdf.KeyDerivation;


public class PropertyHandler {

	// only file name, path inside jar is always /, not File.separator
	private static final String propFileName = "pea.properties";//"resources" + File.separator + 
	
	private InputStream inputStream;
	private OutputStream outputStream;
	
	
	/**
	 * Update all values in properties file
	 * that are not PEA specific. 
	 * This method is called once when a PEA is closed. 
	 * This should be called in EDT.
	 * 
	 * @return	error message or null
	 */
	public String updateAllPropValues() {

		String errorMessage = null;
		try {
			//==================================================
			// set properties for all PEAs
			Properties prop = new Properties();
			File file = new File(System.getProperty("user.dir") + File.separator 
					+ "resources" + File.separator + propFileName);
			if (! file.exists()) {
				// create directory
				file.getParentFile().mkdirs();
				// create file
				file.createNewFile();
			} 
			outputStream = new FileOutputStream(file);

			// Color ===============================
			int[] peaColor = PeaProperties.getPeaColor();
			prop.setProperty("pea_color_r", "" + peaColor[0]);
			prop.setProperty("pea_color_g", "" + peaColor[1]);
			prop.setProperty("pea_color_b", "" + peaColor[2]);
			// Check updates =======================
			int checkUpdates = PeaProperties.getCheckUpdates();
			prop.setProperty("check_updates", "" + checkUpdates);			
			// Font size ================================
			prop.setProperty("font_size", "" + PeaProperties.getFontSize());
			// dropbox access token:
		/*	String token = PeaProperties.getDropboxAccessToken();
			if (token != null) {
				prop.setProperty("dropbox_access_token", token);
			} */
			String lastLanguage = PeaProperties.getLastLanguage();
			if (lastLanguage != null) {
				prop.setProperty("language", lastLanguage);
			}
			boolean installLanguageOptionShown = PeaProperties.isInstallLanguageOptionShown();
			if (installLanguageOptionShown == false) {
				prop.setProperty("install_lang_option_shown", "FALSE");
			} else {
				prop.setProperty("install_lang_option_shown", "TRUE");
			}
			
			//String existingDefaultSalt = prop.getProperty("default_salt");
			if (KeyDerivation.getSalt() != null) {
				String currentlyUsedSalt = Converter.bytes2hex(KeyDerivation.getSalt());
				prop.setProperty("last_salt", currentlyUsedSalt);
			}

			String defaultAlgoParam = PeaProperties.getDefaultAlgoParamString();
			if (defaultAlgoParam != null) {
				prop.setProperty("default_algo_param", defaultAlgoParam);
			}

			String defaultKeyFileName = PeaProperties.getDefaultKeyFileName();
			if (defaultKeyFileName != null) {
				prop.setProperty("key_file", defaultKeyFileName);
			}
			
			// TODO keyfile_only_mode = TRUE/FALSE 
			// TODO direct_start = TRUE/FALSE 
			// TODO last_used_files = comma separated list
			boolean keyFileOnlyMode = PeaProperties.isKeyFileOnlyMode();
			if (keyFileOnlyMode == true) {
				prop.setProperty("keyfile_only_mode", "TRUE");
				boolean directStart = PeaProperties.isDirectStart();
				if (directStart == true) {
					prop.setProperty("direct_start", "TRUE");
				} else {
					prop.setProperty("direct_start", "FALSE");
				}
				ArrayList<String> lastUsedFiles = PeaProperties.getLastUsedFiles();
				if (lastUsedFiles != null ) { 
					if (lastUsedFiles.size() > 0) {
						StringBuilder sb = new StringBuilder();
						String comma = "";
						for (String f : lastUsedFiles) {
							sb.append(comma);
							sb.append(f);
							comma = ",";
						}
						prop.setProperty("last_used_files", new String(sb));
					}
				}
			} else {
				prop.setProperty("keyfile_only_mode", "FALSE");
				prop.setProperty("direct_start", "FALSE");
				prop.setProperty("last_used_files", "");
			}
			
			// language =================================
//			prop.setProperty("lang", PeaProperties.getLanguage());
			// dimension and location ========================
			JFrame frame = (JFrame) PeaControl.getDialog().getLockFrame();
			if (frame == null) {
				new UnexpectedValueException("frame", "JFrame", "is null").printDescription();
				// Fallback
				prop.setProperty("frame_x", "" + 100);
				prop.setProperty("frame_y", "" + 100);
				prop.setProperty("frame_width", "" + 800);
				prop.setProperty("frame_height", "" + 800);
			} else {
				if (frame.isShowing()) {
					prop.setProperty("frame_x", "" + frame.getLocationOnScreen().getX());
					prop.setProperty("frame_y", "" + frame.getLocationOnScreen().getY());
					prop.setProperty("frame_width", "" + frame.getSize().getWidth());
					prop.setProperty("frame_height", "" + frame.getSize().getHeight());
				} else { // Fallback
					prop.setProperty("frame_x", "" + 100);
					prop.setProperty("frame_y", "" + 100);
					prop.setProperty("frame_width", "" + 800);
					prop.setProperty("frame_height", "" + 800);
				}
			}
			if (outputStream != null) {
				prop.store(outputStream, "Properties for all PEAs");

			} else {
				//throw new FileNotFoundException("property file '" + propFileName + "' not found in the classpath");
				TestLog.ve(PropertyHandler.class, "file not found: " + "resources" + File.separator + propFileName);
				errorMessage = "file not found: " + "resources" + File.separator + propFileName;
			}
			try {
				if (outputStream != null) {
				outputStream.close();
				}
			} catch (IOException e) {
				TestLog.ve(PropertyHandler.class, e.getMessage() + " - " + e.toString());
				errorMessage += e + ", " + e.getLocalizedMessage();;
			}
			//==========================================================
			// set PEA specific properties: 
			PeaControl peaControl = PeaControl.getDialog();
			
			// There are no specific properties for Image Lock PEA and File Lock PEA:
			String fileType = PeaProperties.getFileType();
			if (fileType != null) {
				if (fileType.equals("file")) { // File Lock PEA
					return errorMessage;
				} else if (fileType.equals("image")) { // Image Lock PEA
					return  errorMessage;			
				}
			}
			
			prop = new Properties();
			file = new File(System.getProperty("user.dir") + File.separator 
					+ "resources" + File.separator + PeaControl.getDialog().getPeaPropFileName());
			if (! file.exists()) {
				// create directory
				file.getParentFile().mkdirs();
				// create file
				file.createNewFile();
			}
			outputStream = new FileOutputStream(file);
			peaControl.updatePeaPropValues(prop);//, outputStream);
			if (outputStream != null) {
				peaControl.updatePeaPropValues(prop);
				prop.store(outputStream, "Properties for specific PEA");
			} else {
				TestLog.ve(PropertyHandler.class, "OutputStream not found. ", 5);
				errorMessage = "file not found: " + "resources" + File.separator + propFileName;
			}

		} catch (IOException e) {
			TestLog.ve(PropertyHandler.class, "Exception when storig property values: " 
					+ e.getMessage() + " - " + e.toString());
			errorMessage += e + ", " + e.getLocalizedMessage();
		} catch (Exception e) {
			TestLog.ve(PropertyHandler.class, "Exception when storig property values: " 
					+ e.getMessage() + " - " + e.toString(), 5);
			errorMessage += e + ", " + e.getLocalizedMessage();			
		} finally {
			try {
				if (outputStream != null) {
					outputStream.close();
				}
			} catch (IOException e) {
				TestLog.ve(PropertyHandler.class, e.getMessage() + " - " + e.toString());
				errorMessage += e + ", " + e.getLocalizedMessage();;
			}
		}
		return errorMessage;
	}
	
	/**
	 * Set one property in pea.properties
	 * 
	 * @param key		the key to set
	 * @param value		the value to set
	 * 
	 * @return	null for success, an error message otherwise
	 */
	public String setOneProperty(String key, String value) {
		String errorMessage = null;
		try {
			//==================================================
			// set properties for all PEAs
			Properties prop = new Properties();
			File file = new File(System.getProperty("user.dir") + File.separator 
					+ "resources" + File.separator + propFileName);
			if (! file.exists()) {
				return "property file does not exist: " + file.getAbsolutePath();
			} else {
				inputStream = new FileInputStream(file);
						//getClass().getResourceAsStream( "/resources/" +  propFileName);
			}
			if (inputStream != null) {
				prop.load(inputStream);
			} else {// should not happen... 
				//throw new FileNotFoundException("property file '" + propFileName + "' not found in the classpath");
				TestLog.ve(PropertyHandler.class, "File not found: " + propFileName, 5);				
				errorMessage = "file not found: " + propFileName;		
				return errorMessage;
			}
			outputStream = new FileOutputStream(file);
			
			if (key != null && value != null) {
				prop.setProperty(key, value);
			}

			if (outputStream != null) {
				prop.store(outputStream, "Properties for all PEAs");
			} else {
				TestLog.ve(PropertyHandler.class, "OutputStream not found: " + propFileName, 5);
				errorMessage = "file not found: " + "resources" + File.separator + propFileName;
			}
			try {
				if (outputStream != null) {
					outputStream.close();
				}
			} catch (IOException e) {
				TestLog.ve(PropertyHandler.class,  e.getMessage(), 5);
				errorMessage += e + ", " + e.getLocalizedMessage();;
			}

		} catch (IOException e) {
			TestLog.ve(PropertyHandler.class,  "Exception when storig property values: " 
					+ new ExceptionHelper(e).getInfos());
			errorMessage += e + ", " + e.getLocalizedMessage();
		} catch (Exception e) {
			TestLog.ve(PropertyHandler.class,  "Exception when storig property values: " 
					+ new ExceptionHelper(e).getInfos());
			errorMessage += e + ", " + e.getLocalizedMessage();			
		} finally {
			try {
				if (outputStream != null) {
					outputStream.close();
				}
			} catch (IOException e) {
				TestLog.ve(PropertyHandler.class,   e.getMessage());
				errorMessage += e + ", " + e.getLocalizedMessage();;
			}
		}
		return errorMessage;
	}
	
	/**
	 * Read properties from the property file and set the
	 * values in PeaProperties. 
	 * This method is called once when a PEA was started. 
	 * 
	 * @return	an error message or null
	 */
	public String readAndSetAllPropValues() {// throws IOException {
		//TestLog.v(PropertyHandler.class,  "Load properties...");
		String errorMessage = null;
		try {	
			//==================================================
			// set properties for all PEAs
			Properties prop = new Properties();
 
			File file = new File(System.getProperty("user.dir") + File.separator + "resources" + File.separator + propFileName);
			if (file.exists()) {
				// external file in resources
				TestLog.v(PropertyHandler.class,  "Load properties...");
				try {
					inputStream = new FileInputStream(file);
				} catch (FileNotFoundException e) {
					TestLog.ve(PropertyHandler.class, e.getLocalizedMessage() + " - " + e.toString(), 5);
				}
			} else {
				// inside jar: default
				TestLog.v(PropertyHandler.class,  "Load default properties...");
				inputStream = getClass().getResourceAsStream( "/resources/" +  propFileName);
			} 
			if (inputStream != null) {
				//TestLog.e(PropertyHandler.class, "property file: " + inputStream.available());
				try {
					prop.load(inputStream);
				} catch (IOException e) {
					TestLog.ve(PropertyHandler.class, e.getLocalizedMessage() + " - " + e.toString(), 5);
				}
			} else {// should not happen... 
				//throw new FileNotFoundException("property file '" + propFileName + "' not found in the classpath");
				TestLog.ve(PropertyHandler.class,  "file not found: " + "/resources/" + propFileName, 5);	
				TestLog.e(PropertyHandler.class,  "file not found: " + "/resources/" + propFileName);				
				errorMessage = "file not found: " + propFileName;		
				return errorMessage;
			}
			
			// should not happen:
			if (prop.getProperty("pea_color_r") == null
					&& prop.getProperty("font_size") == null
					&& prop.getProperty("frame_x") == null) {
				TestLog.e(PropertyHandler.class, "Can't read properties from file " + "resources" + File.separator + propFileName);
				TestLog.ve(PropertyHandler.class,  "Property,color, font size, x is missing");
				return errorMessage + ("Can't read properties from file " + "resources" + File.separator + propFileName);
			}
			// Color ===============================
			int r, g, b;
			String rString = prop.getProperty("pea_color_r");
			if (rString != null) {
				try {
					r = Integer.parseInt(rString);
				} catch (Exception e) {
					r = PeaProperties.getPeaColor()[0];
					TestLog.ve(PropertyHandler.class, "Can't parse " + rString);
					TestLog.e(PropertyHandler.class, "Can't set color red: " + rString);
					//new UnexpectedValueException("rString", "String", "is not digit: " + rString).printDescription();
				}
			} else {
				r = PeaProperties.getCipherPeaColor()[0];
				TestLog.ve(PropertyHandler.class, "Missing property color red");
			}
			String gString = prop.getProperty("pea_color_g");
			if (gString != null) {
				try {
					g = Integer.parseInt(gString);
				} catch (Exception e) {
					g = PeaProperties.getPeaColor()[1];
					TestLog.ve(PropertyHandler.class, "Can't parse " + gString);
					TestLog.e(PropertyHandler.class, "Can't set color green: " + gString);
					//new UnexpectedValueException("gString", "String", "is not digit: " + gString).printDescription();
				}
			} else {
				g = PeaProperties.getCipherPeaColor()[1];
				TestLog.ve(PropertyHandler.class, "Missing property color green");
			}
			String bString = prop.getProperty("pea_color_b");
			if (bString != null) {
				try {
					b = Integer.parseInt(bString);
				} catch (Exception e) {
					b = PeaProperties.getPeaColor()[2];
					TestLog.ve(PropertyHandler.class, "Can't parse " + bString);
					TestLog.e(PropertyHandler.class, "Can't set color blue: " + bString);
					//new UnexpectedValueException("bString", "String", "is not digit: " + bString).printDescription();
				}
			} else {
				b = PeaProperties.getCipherPeaColor()[2];
				TestLog.ve(PropertyHandler.class, "Missing property color blue");
			}
			int[] color = {r,g,b};
			PeaProperties.setPeaColor(color);
			// Dropbox access token
			String token = prop.getProperty("dropbox_access_token");
			if (token != null) {
				PeaProperties.setDropboxAccessToken(token);
			}
		
			String checkUpdates = prop.getProperty("check_updates");			
			if (checkUpdates != null ) {//&& checkUpdates.toLowerCase().equals("true")) {
				int check = 0;
				try {
					check = Integer.parseInt(checkUpdates);
					if (check == 0 || check == -1 || check == 1) {
						PeaProperties.setCheckUpdates(check);
					}
				} catch (Exception e){
					TestLog.ve(PropertyHandler.class, e.getLocalizedMessage() + " - " + e.toString());
				}
			}
			// TODO keyfile_only_mode = TRUE/FALSE 
			// TODO direct_start = TRUE/FALSE 
			// TODO last_used_files = comma separated list
			String keyFileOnlyModeString = prop.getProperty("keyfile_only_mode");		
			if (keyFileOnlyModeString != null) {
				if (keyFileOnlyModeString.equals("TRUE")) {
					PeaProperties.setKeyFileOnlyMode(true);
					String directStartString = prop.getProperty("direct_start");		
					if (directStartString != null) {
						if (directStartString.equals("TRUE")) {
							PeaProperties.setDirectStart(true);
						} else {
							PeaProperties.setDirectStart(false);
						}
					}
					String lastUsedFilesString = prop.getProperty("last_used_files");	
					if (lastUsedFilesString != null) {
						if (lastUsedFilesString.length() > 0) {					
							String[] luf = lastUsedFilesString.split(",");
							PeaProperties.setLastUsedFiles(Converter.arrayToArrayList(luf));
						}
					}
				} else {
					PeaProperties.setKeyFileOnlyMode(false);
					PeaProperties.setDirectStart(false);
					PeaProperties.setLastUsedFiles(null);
				}
			}


			// main Dropbox API server host name
	/*		String api = prop.getProperty("dropbox_api");
			if (api != null) {
				PeaProperties.setDropboxApi(api);
			}
			// Dropbox API content server host name
			String content = prop.getProperty("dropbox_content");
			if (content != null) {
				PeaProperties.setDropboxContent(content);
			}
			// Dropbox web server host name
			String web = prop.getProperty("dropbox_web");
			if (web != null) {
				PeaProperties.setDropboxWeb(web);
			}
			// Dropbox notification server host name
			String notify = prop.getProperty("dropbox_notify");
			if (notify != null) {
				PeaProperties.setDropboxNotify(notify);
			}*/

			// Font size ================================
			String fontSize = prop.getProperty("font_size");
			if (fontSize != null) {
				try {
					PeaProperties.setFontSize(Integer.parseInt(fontSize));
				} catch (Exception e) {
					//new UnexpectedValueException("fontSize", "String", "is not digit: " + fontSize).printDescription();
					TestLog.ve(PropertyHandler.class, "Can't parse " + fontSize);
					TestLog.e(PropertyHandler.class, "Can't set font size: " + fontSize);
				}
			} else {
				TestLog.ve(PropertyHandler.class, "Missing property font size");
			}

			// dimension and location ========================
			// avoid showing outside the monitor in Windows
			double screenWidth = 0;
			double screenHeight = 0;
			Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize(); 
			screenWidth = screenSize.getWidth();

			int w,h, x, y;			
			String frameX = prop.getProperty("frame_x");
			if (frameX != null) {
				try {
					double xD = Double.parseDouble(frameX); 
					if (xD > screenWidth) {
						xD = 50.0;
					}
					x = (int) xD;
				} catch (Exception e) {
					//new UnexpectedValueException("frameX", "String", "is not digit: " + frameX).printDescription();
					TestLog.ve(getClass(), "Can't parse " + frameX);
					TestLog.e(getClass(), "Can't set location x: " + frameX);
					x = JREProperties.getFrameLocation()[0];
				}			
			} else {
				TestLog.ve(PropertyHandler.class, "Missing property location x");
				x = JREProperties.getFrameLocation()[0];
			}
			String frameY = prop.getProperty("frame_y");
			if (frameY != null) {
				try {
					double yD = Double.parseDouble(frameY);
					if (yD > screenHeight) {
						yD = 50.0;
					}
					y = (int) yD;
				} catch (Exception e) {
					//new UnexpectedValueException("frameY", "String", "is not digit: " + frameY).printDescription();
					TestLog.ve(getClass(), "Can't parse " + frameY);
					TestLog.e(getClass(), "Can't set location y: " + frameY);
					y = JREProperties.getFrameLocation()[1];
				}		
			} else {
				TestLog.ve(PropertyHandler.class, "Missing property location y");
				y = JREProperties.getFrameLocation()[1];
			}
			String frameWidth = prop.getProperty("frame_width");
			if (frameWidth !=  null) {
				try {
					w = (int) Double.parseDouble(frameWidth);
				} catch (Exception e) {
					//new UnexpectedValueException("frameWidth", "String", "is not digit: " + frameWidth).printDescription();
					TestLog.ve(getClass(), "Can't parse " + frameWidth);
					TestLog.e(getClass(), "Can't set width: " + frameWidth);
					w = JREProperties.getFrameDim()[0];
				}
			} else {
				TestLog.ve(PropertyHandler.class, "Missing property width");
				w = JREProperties.getFrameDim()[0];
			}
			String frameHeight = prop.getProperty("frame_height");
			if (frameHeight != null) {
				try {
					h = (int) Double.parseDouble(frameHeight);
				} catch (Exception e) {
					//new UnexpectedValueException("frameHeight", "String", "is not digit: " + frameHeight).printDescription();
					TestLog.ve(getClass(), "Can't parse " + frameHeight);
					TestLog.e(getClass(), "Can't set height: " + frameHeight);
					h = JREProperties.getFrameDim()[1];
				}			
			} else {
				TestLog.ve(PropertyHandler.class, "Missing property height");
				h = JREProperties.getFrameDim()[1];
			}
			int[] dimVals = {w,h};
			int[] locVals = {x,y};
			JREProperties.setFrameDim(dimVals);
			JREProperties.setFrameLocation(locVals);		
			
			String lastSaltProp = prop.getProperty("last_salt");
			if (lastSaltProp != null) { // if no valid file found...
				PeaProperties.setLastSalt(Converter.hex2bytes(lastSaltProp));
			} 
			String lastLanguage = prop.getProperty("language");
			if (lastLanguage != null) {
				PeaProperties.setLastLanguage(lastLanguage);
				//TestLog.e(getClass(), "Set language: " + lastLanguage);
			} else {
				TestLog.ve(PropertyHandler.class, "Missing property language");
			}
			
			String installLangOption = prop.getProperty("install_lang_option_shown");
			if (installLangOption != null) {
				if ( installLangOption.equals("FALSE")) {
					PeaProperties.setInstallLanguageOptionShown(false);
				} else if (installLangOption.equals("TRUE")){
					PeaProperties.setInstallLanguageOptionShown(true);
				} else {
					TestLog.ve(PropertyHandler.class, "Invalid property for boolean install language: " + installLangOption);
				}
			}

			String keyFile = prop.getProperty("key_file");
			if (keyFile != null) {
				TestLog.v(PropertyHandler.class, "Default key file: " + keyFile);
				PeaProperties.setDefaultKeyFileName(keyFile);
				//AlgoParamHandler.readString(keyFile, true);
				PeaProperties.setDefaultKeyFileName(keyFile);
			} else {
				TestLog.v(PropertyHandler.class, "No default key file");
			}
			
			String defaultAlgoParam = prop.getProperty("default_algo_param");
			if (defaultAlgoParam != null) {
				TestLog.v(PropertyHandler.class, "Default algo string: " + defaultAlgoParam);
				PeaProperties.setDefaultAlgoParamString(defaultAlgoParam);
				AlgoParamHandler.readString(defaultAlgoParam, true);
				PeaProperties.setDefaultAlgoParamString(defaultAlgoParam);
			} else {
				TestLog.v(PropertyHandler.class, "No default algo string");
			}
		} catch (Exception e) {
			TestLog.ve(PropertyHandler.class,  e.getMessage() + " - " + e.toString(), 5);
			TestLog.e(PropertyHandler.class,  e.getMessage() + " - " + e.toString());
			errorMessage += e + ", " + e.getLocalizedMessage();			
		} finally {
			try {
				if (inputStream != null) {
					inputStream.close();
				}
			} catch (IOException e) {
				TestLog.ve(PropertyHandler.class,  e.getMessage(), 5);
				TestLog.e(PropertyHandler.class,  e.getMessage() + " - " + e.toString());
				errorMessage += e + ", " + e.getLocalizedMessage();;
			}
		}
		//TestLog.e(getClass(), errorMessage, 5);
		return errorMessage;
	}
	
	/**
	 * Set one property for PEA-specific property file
	 * (e.g. in calendar.properties)
	 * 
	 * @return null for success or an error message if operation failed
	 */
	public String setSpecificPropValue() {
		String errorMessage = null;
		// There are no specific properties for Image Lock PEA and File Lock PEA:
		String fileType = PeaProperties.getFileType();
		if (fileType.equals("file")) { // File Lock PEA
			return null;
		} else if (fileType.equals("image")) { // Image Lock PEA
			return  null;			
		}
		try { 		
			
			//==========================================================
			// set PEA specific properties: 
			PeaControl peaControl = PeaControl.getDialog();
			
			Properties prop = new Properties();
			File file = new File(System.getProperty("user.dir") + File.separator + 
					"resources" + File.separator + 
					PeaControl.getDialog().getPeaPropFileName());
			if (file.exists()) {
				// external file in resources
				inputStream = new FileInputStream(file);
			} else {
				// inside jar: default
				TestLog.v(PropertyHandler.class, "Load default pea specific properties...");
				inputStream = getClass().getResourceAsStream("/resources/" + PeaControl.getDialog().getPeaPropFileName());
			}
			if (inputStream != null) {
				prop.load(inputStream);
			} else {
				TestLog.ve(PropertyHandler.class, "file not found: " + PeaControl.getDialog().getPeaPropFileName());
				errorMessage = "file not found: " + PeaControl.getDialog().getPeaPropFileName();
			}
			peaControl.setPeaPropValues(prop, inputStream);

		} catch (IOException e) {
			TestLog.ve(PropertyHandler.class, e.getMessage());
			errorMessage += e + ", " + e.getLocalizedMessage();
		} catch (Exception e) {
			TestLog.ve(PropertyHandler.class,  e.getMessage());
			errorMessage += e + ", " + e.getLocalizedMessage();			
		} finally {
			try {
				inputStream.close();
			} catch (Exception e) {
				TestLog.ve(PropertyHandler.class,  e.getMessage());
				errorMessage += e + ", " + e.getLocalizedMessage();;
			}
		}
		return errorMessage;
	}
	
	/**
	 * Set one property in the pea.property file, which
	 * is not PEA specific. 
	 * The key must not exist already.
	 * 
	 * @param propKey	the key of the property as String
	 * @param propValue	the value of the property as String
	 * 
	 * @return an error message or null
	 */
/*	public String setSingleAllProperty(String propKey, String propValue) {
		String errorMessage = null;
		try {
			//==================================================
			// set properties for all PEAs
			Properties prop = new Properties();
			File file = new File(propFileName);

			if (! file.exists()) {
				// create directory
				file.getParentFile().mkdirs();
				// create file
				file.createNewFile();
			} 
			inputStream = new FileInputStream(file);
			prop.load(inputStream);
			inputStream.close();
			outputStream = new FileOutputStream(file);
			

			Object oldProp = prop.setProperty(propKey, propValue);
			if (oldProp == null) {
				System.out.println("Created new property: " + propKey);
			} else {
				System.out.println("Property updated: " + propKey);
			}

			prop.store(outputStream,  "Properties for all PEAs");
			outputStream.close();

		} catch (IOException e) {
			errorMessage += e + ", " + e.getLocalizedMessage();	
			System.err.println(new ExceptionHelper(e).getInfos() );
		} catch (Exception e) {
			System.err.println(new ExceptionHelper(e).getInfos() );
			errorMessage += e + ", " + e.getLocalizedMessage();			
		} finally {
			try {
				if (outputStream != null) {
					outputStream.close();
				}
			} catch (IOException e) {
				System.err.println(new ExceptionHelper(e).getInfos() );
				errorMessage += e + ", " + e.getLocalizedMessage();;
			}
			try {
				if (inputStream != null) {
					inputStream.close();
				}
			} catch (IOException e) {
				System.err.println(new ExceptionHelper(e).getInfos() );
				errorMessage += e + ", " + e.getLocalizedMessage();;
			}
		}
		return errorMessage;
	}*/


	public String deleteLastSaltProperty() {
		String errorMessage = null;
		 
		try {
			//==================================================
			// set properties for all PEAs
			Properties prop = new Properties();
 
			File file = new File(System.getProperty("user.dir") + File.separator + "resources" + File.separator + propFileName);
			if (file.exists()) {
				// external file in resources
				inputStream = new FileInputStream(file);
			} else {
				// inside jar: default
				TestLog.v(PropertyHandler.class,  "Load default properties...");
				inputStream = getClass().getResourceAsStream( "/resources/" +  propFileName);
			} 
			if (inputStream != null) {
				prop.load(inputStream);
			} else {// should not happen... 
				//throw new FileNotFoundException("property file '" + propFileName + "' not found in the classpath");
				TestLog.ve(PropertyHandler.class,  "file not found: " + "/resources/" + propFileName);				
				errorMessage = "file not found: " + propFileName;		
				return errorMessage;
			}
			
			// remove lastSalt
			prop.remove("last_salt");
			
			outputStream = new FileOutputStream(file);
			
			if (outputStream != null) {
				prop.store(outputStream, "Properties for all PEAs");

			} else {
				//throw new FileNotFoundException("property file '" + propFileName + "' not found in the classpath");
				TestLog.ve(PropertyHandler.class, "file not found: " + "resources" + File.separator + propFileName);
				errorMessage = "file not found: " + "resources" + File.separator + propFileName;
			}

		} catch (IOException e) {
			TestLog.ve(PropertyHandler.class,  e.getMessage());
			errorMessage += e + ", " + e.getLocalizedMessage();
		} catch (Exception e) {
			TestLog.ve(PropertyHandler.class,  e.getMessage());
			errorMessage += e + ", " + e.getLocalizedMessage();			
		} finally {
			try {
				if (inputStream != null) {
					inputStream.close();
				}
			} catch (IOException e) {
				TestLog.ve(PropertyHandler.class, e.getMessage());
				errorMessage += e + ", " + e.getLocalizedMessage();;
			}
			try {
				if (outputStream != null) {
					outputStream.close();
				}
			} catch (IOException e) {
				TestLog.ve(PropertyHandler.class, e.getMessage());
				errorMessage += e + ", " + e.getLocalizedMessage();;
			}
		}
		return errorMessage;
	}
}
