package cologne.eck.tools;

/*
 * PeaFactory - Production of Password Encryption Archives
 * Copyright (C) 2015  Axel von dem Bruch
 * 
 * This library is free software; you can redistribute it and/or modify it 
 * under the terms of the GNU General Public License as published 
 * by the Free Software Foundation; either version 2 of the License, 
 * or (at your option) any later version.
 * This library is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
 * or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU General Public License for more details.
 * See:  http://www.gnu.org/licenses/gpl-2.0.html
 * You should have received a copy of the GNU General Public License 
 * along with this library.
 */

import java.util.ArrayList;
import java.util.List;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.io.File;
import java.io.IOException;

import javax.swing.TransferHandler;

import cologne.eck.all_peas.files.FileComposer;
import cologne.eck.all_peas.files.FileTypePanel;

// taken from: 
// https://stackoverflow.com/questions/811248/how-can-i-use-drag-and-drop-in-swing-to-get-file-path

@SuppressWarnings("serial")
public
final class FileDropHandler extends TransferHandler {
    @Override
    public boolean canImport(TransferHandler.TransferSupport support) {
        for (DataFlavor flavor : support.getDataFlavors()) {
            if (flavor.isFlavorJavaFileListType()) {
                return true;
            }
        }
        return false;
    }

    @Override
    @SuppressWarnings("unchecked")
    public boolean importData(TransferHandler.TransferSupport support) {
        if (!this.canImport(support)) {
            return false;
        }
        FileTypePanel ftp = null;
        if (support.getComponent() instanceof FileTypePanel) {
        	ftp = (FileTypePanel) support.getComponent();
        } else {
        	new UnexpectedValueException("support.getComponent()", "Component", 
        			"is not FileTypePanel: " + support.getComponent().toString()).printDescription();
        	return false;
        }

        List<File> files;
        try {
            files = (List<File>) support.getTransferable()
                    .getTransferData(DataFlavor.javaFileListFlavor);
        } catch (UnsupportedFlavorException | IOException ex) {
            // should never happen (or JDK is buggy)
            return false;
        }

        int len = files.size();

        ArrayList<String> selectedFileNames = new ArrayList<String>();
        for (int i = 0; i < len; i++) {
        	selectedFileNames.add( files.get(i).getAbsolutePath() );
        }
        //ArrayList<String> list = new ArrayList<String>(Arrays.asList(selectedFileNames));
        //FileTypePanel ftp = PeaControl.getEncryptedFileTypePanel();
        FileComposer fc = ftp.getFileComposer();

        fc.addFilesToMap(selectedFileNames, true, false, true);// true: include sub-directories
        // false: do not check already added files again
        // true: add check boxes

        fc.updateFiles(false);// do not check already listed files again
        ftp.updateWindow();

        return true;
    }
}