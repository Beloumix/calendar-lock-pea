package cologne.eck.tools;

import org.bouncycastle.util.Arrays;

import cologne.eck.all_peas.data.PeaProperties;
import cologne.eck.all_peas.vm_specific.JREProperties;

/*
 * Peafactory - Production of Password Encryption Archives
 * Copyright (C) 2015  Axel von dem Bruch
 * 
 * This library is free software; you can redistribute it and/or modify it 
 * under the terms of the GNU General Public License as published 
 * by the Free Software Foundation; either version 2 of the License, 
 * or (at your option) any later version.
 * This library is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
 * or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU General Public License for more details.
 * See:  http://www.gnu.org/licenses/gpl-2.0.html
 * You should have received a copy of the GNU General Public License 
 * along with this library.
 */


public class Comparator {

	/**
	 * Compare two byte arrays time independent
	 * 
	 * @param one
	 * @param two
	 * @return		true, if arrays are equal, false if not
	 */
	public static final boolean compare(byte[] one, byte[] two) {
		boolean equal = true;

		if (one == null && two == null){
			System.out.println("compared arrays null");
			return true;
		}
		if (one == null && two != null) {
			System.out.println("first compared array null");
			return false;
		}
		if (one != null && two == null) {
			System.out.println("second compared array null");
			return false;
		}
		if (one == null) { // only for SpotBug
			return false;
		}
		int len = one.length;
		if (two.length != len) {
			return false;
		}
		for (int i = 0; i < len; i++) {
			if (one[i] != two[i]) {
				equal = false;
			}
		}
		return equal;	
	}
		
	/**
	 * Compare two char arrays time independent
	 * 
	 * @param one
	 * @param two
	 * @return		true, if arrays are equal, false if not
	 */
	public static final boolean compare(char[] one, char[] two) {
		boolean equal = true;
		int len = one.length;
		if (two.length != len) {
			return false;
		}
		for (int i = 0; i < len; i++) {
			if (one[i] != two[i]) {
				equal = false;
			}
		}
		return equal;	
	}
	
	/**
	 * This function should prevent from using a zero key. 
	 * If the program runs in test mode, the given vector is tested
	 * if it contains only zeros. If so, a message is shown. 
	 * If the program does not run in test mode, nothing is tested. 
	 * 
	 * @param input		the vector to test
	 * @return			false if the vector is not a null vector
	 * 					true if the vector is a null vector
	 */
	public static final boolean checkNullVector(byte[] input){
		if (PeaProperties.isTestMode()) {
			if (input == null) {
				TestLog.e(Comparator.class, "Input to check is null", 5);
				TestLog.e(Comparator.class, "Check:Null input", 8);
				PeaProperties.getVmBridge().showUrgentMessage(JREProperties.getMainWindow(), 
						"Null key found");
				return true;
			}

			byte[] nullVector = new byte[input.length];
			if (compare(input, nullVector) == true) {
				TestLog.e(Comparator.class, "Input to check is zeroized", 5);
				//System.err.println("___________________________");
				TestLog.e(Comparator.class, "Check: Zeroized input", 8);
				PeaProperties.getVmBridge().showUrgentMessage(JREProperties.getMainWindow(), 
						"Zeroized key found");
				return true;
			} else {
				return false;
			}
		} else {
			return false;
		}
	}
	/**
	 * This function should prevent from using a zeroized password. 
	 * If the program runs in test mode, the given vector is tested
	 * if it contains only zeros. If so, a message is shown. 
	 * If the program does not run in test mode, nothing is tested. 
	 * 
	 * @param input		the vector to test
	 * @return			false if the vector is not a null vector
	 * 					true if the vector is a null vector
	 */
	public static final boolean checkNullVector(char[] input){
		if (PeaProperties.isTestMode()) {
			if (input == null) {
				TestLog.e(Comparator.class, "Input to check is null", 5);
				TestLog.e(Comparator.class, "Check:Null input", 8);
				PeaProperties.getVmBridge().showUrgentMessage(JREProperties.getMainWindow(), 
						"Null password found");
				return true;
			}

			char[] nullVector = new char[input.length];
			Arrays.fill(nullVector, '\0');
			if (compare(input, nullVector) == true) {
				TestLog.e(Comparator.class, "Input to check is zeroized", 5);
				//System.err.println("___________________________");
				TestLog.e(Comparator.class, "Check: Zeroized input", 8);
				PeaProperties.getVmBridge().showUrgentMessage(JREProperties.getMainWindow(), 
						"Zeroized password found");
				return true;
			} else {
				return false;
			}
		} else {
			return false;
		}
	}

	/**
	 * Check if one array contains another. This should be
	 * time independent except to the length of the target.
	 * 
	 * @param source
	 * @param target
	 * @return
	 */
	protected static final boolean containsArray(char[] source, char[] target) {
				
		int tLen = target.length;
		int sLen = source.length;
		if (tLen > sLen){
			return false;			
		}
		int gap = sLen - tLen;
		boolean result = false;
		for (int i = 0; i <= gap; i++) {
			boolean partContains = true;
			for (int j = 0; j < tLen; j++) {
				if (source[i + j] == target[j]) {
					//
				} else {
					partContains = false;
				}
			}
			if (partContains == true) {
				result = true;
			}
		}
		return result;		
	}
	
	/**
	 * Find the best match with the fewest exceptions for two
	 * arrays. 
	 * This should detect leet transformations and single upper 
	 * case letters.
	 * 
	 * @param source	the array to check against
	 * @param target	the array to check
	 * @return			the fewest number of exceptions (best match)
	 */
	protected static final int containsWithExceptions(char[] source, char[] target) {
				
		int tLen = target.length;
		int sLen = source.length;
		
		char[] _source = null;
		char[] _target = null;
		if (tLen > sLen){
			_source = new char[tLen];
			_target = new char[sLen];
			// swap target and source
			// and check if source is a part of target
			System.arraycopy(source,  0,  _target,  0, sLen);		
			System.arraycopy(target,  0,  _source,  0, tLen);
		} else {
			_source = new char[sLen];
			_target = new char[tLen];
			System.arraycopy(source,  0,  _source,  0, sLen);		
			System.arraycopy(target,  0,  _target,  0, tLen);
		}
		int gap = sLen - tLen;
		int match = tLen;
		for (int i = 0; i <= gap; i++) {
			int partialMatch = 0;
			for (int j = 0; j < tLen; j++) {
				if (_source[i + j] == _target[j]) {
					partialMatch += 0;
				} else {
					partialMatch++;
				}
			}
			// Find the best match with the fewest exception
			if (partialMatch < match) {
				match = partialMatch;
			}
		}
		Zeroizer.zero(_source);
		Zeroizer.zero(_target);
		return match;		
	}
}
