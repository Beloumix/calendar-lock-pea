package cologne.eck.peafactory.crypto;

/*
 * Peafactory - Production of Password Encryption Archives
 * Copyright (C) 2015  Axel von dem Bruch
 * 
 * This library is free software; you can redistribute it and/or modify it 
 * under the terms of the GNU General Public License as published 
 * by the Free Software Foundation; either version 2 of the License, 
 * or (at your option) any later version.
 * This library is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
 * or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU General Public License for more details.
 * See:  http://www.gnu.org/licenses/gpl-2.0.html
 * You should have received a copy of the GNU General Public License 
 * along with this library.
 */

/**
 * Generation of random values, also random passwords. 
 */


import java.nio.ByteBuffer;

import cologne.eck.all_peas.bridge.VMBridge;
import cologne.eck.all_peas.data.PeaProperties;
import cologne.eck.tools.Comparator;
import cologne.eck.tools.Converter;
import cologne.eck.tools.TestLog;
import cologne.eck.tools.UnexpectedValueException;
import cologne.eck.tools.Zeroizer;

public class RandomStuff {
	
	private final static int pswLength = 50;	
	
	// 90 Printable 7-Bit ASCII (without "  \ 0)
	private final static char[] allChars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ123456789!#$%&()*+,-./:;<=>?@[]^_'{|}~".toCharArray();
	private static byte[] personalBytes = null;
	
	private final static int PERSONAL_BYTES_SIZE = VMBridge.PERSONAL_BYTES_SIZE;// 16;
	private final static int RESEED_BYTES_SIZE = 12;
	
	// pool for unique bytes (digest size)
	private static byte[] uniquePool = null;
	// counter where to get values from uniquePool
	private static int uniquePoolCounter = 0;
	// counter to produce new values for uniquePool
	private static int uniqueCounter = 0; 
	// length of the used digest
	private static int digestSize;
	
	
	/**
	 * Create unique values (for nonce, file identifier...)
	 * 
	 * @param length
	 * @return
	 */
	public final synchronized byte[] createUniqueBytes(int length) {		
		
		if (length <= 0) {
			TestLog.e(RandomStuff.class, "invalid length for unique values");
			new UnexpectedValueException("int", "length", " is invalid: " + length).printDescription();
			return null;
		}
		
		byte[] result = new byte[length];
		
		if (uniquePool == null) { // initialize 
			
			digestSize = HashStuff.getHashAlgo().getDigestSize();
			uniquePool = new byte[digestSize];
			// initialize with secure random:
			PeaProperties.getSecureRandom().nextBytes(uniquePool);			
			//Help.printBytes("initial pool - digestSize : " + digestSize, uniquePool);
		} 
		
		if (uniquePoolCounter + length < uniquePool.length) { // required bytes available
						
			System.arraycopy(uniquePool,  uniquePoolCounter, result, 0, length);
			uniquePoolCounter += length;
			
		} else {
			
			updateUniquePool();
			
			if (length <= digestSize) {
				System.arraycopy(uniquePool,  uniquePoolCounter, result, 0, length);
				uniquePoolCounter += length;

			} else {	// this should never happen...
				// expand:				
				int resultCounter = 0;
				System.arraycopy(uniquePool,  uniquePoolCounter, result, 0, digestSize);
				resultCounter = digestSize;
				while( (resultCounter + digestSize) < length) {
					updateUniquePool();
					System.arraycopy(uniquePool, 0, result, resultCounter, digestSize);
					resultCounter += digestSize;
				}								
				// the rest:
				updateUniquePool();				
				System.arraycopy(uniquePool, 0, result, resultCounter, length - resultCounter);
				uniquePoolCounter = length - resultCounter;
			}
		}
		//Help.printBytes("result uniqueBytes", result);
		return result;		
	}
	
	private void updateUniquePool() {		
		
		if (digestSize <= 0) {
			digestSize = HashStuff.getHashAlgo().getDigestSize();
		}
		// create new pool:
		byte[] counterBytes = Converter.int2bytesLE(uniqueCounter);
		uniqueCounter++;
		// seed: shorter than output (avoids extra-round of hash function)
		byte[] seed = new byte[digestSize + 4];

		if (uniquePool.length < digestSize){ // Hash algo change in running program...
			byte[] newPool = new byte[digestSize];
			System.arraycopy(uniquePool, 0, newPool,  0, uniquePool.length);
			uniquePool = newPool;
		} else if (uniquePool.length > digestSize){
			byte[] newPool = new byte[digestSize];
			System.arraycopy(uniquePool, 0, newPool,  0, digestSize);
			uniquePool = newPool;
		}
		System.arraycopy(uniquePool, 0, seed, 0, digestSize);// - 8);x
		System.arraycopy(counterBytes,  0,  seed,  digestSize,// - 8,  
					4);
		//Help.printBytes("seed for uniquePool", seed);
		uniquePool = HashStuff.hash(seed);			
		uniquePoolCounter = 0;
		//Help.printBytes("new uniquePool - uniquePoolCounter: "+ uniquePoolCounter, uniquePool);
	}
	
	/**
	 * Create random values
	 * 
	 * @param length	number of bytes (size of output)
	 * @return			the random values
	 */
	public final synchronized byte[] createRandomBytes(int length) {			
		
		if (length <= 0) {
			PeaProperties.getVmBridge().exitWithError(null, 
					PeaProperties.getVmBridge().translate("unexpected_error") + 
					"\nRequested length for random value is invalid: "+ length, false);
		}
		
		byte[] result = new byte[length];// return value
		
		byte[] seed = new byte[52];
		int seedIndex = 0;
		
		// seed of SecureRandom alone might not be appropriate for keys > 20 byte because it uses state of SHA-1
		byte[] scSeed = new byte[20];
		PeaProperties.getSecureRandom().nextBytes(scSeed);
		for (int i = 0; i < scSeed.length; i++) {
			seed[i] = scSeed[i];
			scSeed[i] = (byte) 0;
		}
		seedIndex += scSeed.length;

		// get values from RandomCollector: 		
		byte[] tableSeed = PeaProperties.getVmBridge().getAndUpdateEntopyPoolTableValue(2);
		//byte[] tableSeed = getAndUpdateTableValue( 2 );
		System.arraycopy(tableSeed,  0,  seed,  seedIndex, tableSeed.length);
		Zeroizer.zero(tableSeed);
		seedIndex += tableSeed.length;

		// add personal bytes: 16 byte
		if (personalBytes == null) {
			personalBytes = PeaProperties.getVmBridge().generatePersonalRandomBytes();// 16 byte
		}
		System.arraycopy(personalBytes,  0,  seed,  seedIndex, PERSONAL_BYTES_SIZE);

		// hash seed
		Comparator.checkNullVector(seed);// at least bytes from SecureRandom
		byte[] data = HashStuff.hashAndOverwrite(seed);
		Comparator.checkNullVector(data);
		Zeroizer.zero(seed);
		
		// truncate if output size of hash >= required length
		if (data.length >= length) {			
			System.arraycopy(data,  0,  result,  0,  length);
	
		} else {		
			// expand data to result.length:
			int dataIndex = 0; // index of hash value
			int resultIndex = 0; // index of result
			while (resultIndex < length) {
				
				while ( dataIndex < data.length && resultIndex < result.length){ 
					result[resultIndex++] = data[dataIndex++];
				}
				//Help.printBytes("result",  result);
				if (resultIndex < result.length){
					// create new data: 
					byte[] newSeed = new byte[data.length 
					                          + RESEED_BYTES_SIZE 
					                          + PERSONAL_BYTES_SIZE];// reseed with extraSeed + personalBytes
					int newSeedIndex = 0;
					
					System.arraycopy(data,  0,  newSeed,  0,  data.length);// 64 byte
					Zeroizer.zero(data);
					newSeedIndex += data.length;
					// reseed:
					byte[] newExtraSeed = generateExtraSeed(); // 12 byte
					System.arraycopy(newExtraSeed, 0, newSeed, newSeedIndex, RESEED_BYTES_SIZE);
					Zeroizer.zero(newExtraSeed);
					newSeedIndex += newExtraSeed.length;
					// add personal bytes:
					System.arraycopy(personalBytes,  0,  newSeed, newSeedIndex, PERSONAL_BYTES_SIZE);
					
					Comparator.checkNullVector(newSeed);
					data = HashStuff.hashAndOverwrite(newSeed);
					Comparator.checkNullVector(data);
					dataIndex = 0;				
				}
			}
		}
		Zeroizer.zero(seed);
		Zeroizer.zero(data);
		Comparator.checkNullVector(result);
		return result;
	}
	
	//
	// two independent random functions: first mix allChars pseudo-randomly 
	// then choose chars pseudo-randomly
	//
	/**
	 * Create an array of characters randomly
	 * 
	 * @return	randomly chosen array of characters
	 */
	public final char[] createNewRandomChars() {
		
		char[] newRandomChars = new char[pswLength];// return value
		
		int[] mixPositions = getMixPositions (allChars.length / 2, 100);
		mixChars(mixPositions);		
		Zeroizer.zero(mixPositions);

		for(int i = 0; i < pswLength; i++) {
			newRandomChars[i] = allChars[PeaProperties.getSecureRandom().nextInt(allChars.length)];
		}				
		return newRandomChars;
	}
	
	//
	// get array of values > 0 and < maxValue to mix allChars
	// mixNumber = times to swap values / 2
	//
	private final int[] getMixPositions (int maxValue, int mixNumber) {
		int[] result = new int[mixNumber];
		
		byte[] hash = createRandomBytes(mixNumber * 2);// this may not be enough		
		
		// get values from hash and fill result, compute new hashes if necessary
		int resultPosition = 0;
		int inputPosition = 0;
		while(resultPosition < mixNumber) {
			if ( (Math.abs(hash[inputPosition])) < maxValue) {
				result[resultPosition] = Math.abs(hash[inputPosition] );
				hash[inputPosition] = (byte) 0xFF;
				resultPosition++;
				inputPosition++;
			} else { 
				// throw away: 
				inputPosition++;
			}
			if (inputPosition >= hash.length) {
				// new hash
				hash = HashStuff.hashAndOverwrite(hash);
				inputPosition = 0;
			}
		}
		Zeroizer.zero(hash);
		return result;
	}
	
	//
	// mix allChars with positions > 0 and < allChars/2
	private final void mixChars(int[] mixPositions) {
		
		char swap;
		int half = allChars.length/2;
		for (int i = 0; i < mixPositions.length; i+=2) {
			swap = allChars[ mixPositions[i] ];
			allChars[ mixPositions[i] ] = allChars[ mixPositions[i+1] + half];
			allChars[ mixPositions[i+1] + half] = swap;
		}
		Zeroizer.zero(mixPositions);
	}	
	
	private final byte[] generateExtraSeed() {
		byte[] seed = new byte[RESEED_BYTES_SIZE];
		int index = 0;
			
		// nanoTime: 8 bytes (four of them are mostly null) -> take 4 bytes
		long nanoSeed =  System.nanoTime();
		ByteBuffer nanoBuffer = ByteBuffer.allocate(8);
		byte[] nanoBytes = nanoBuffer.putLong(nanoSeed).array();
		System.arraycopy(nanoBytes,  2, seed ,index, 4);
		Zeroizer.zero(nanoBytes);
		index += 4; 
		//Help.printBytes("seed nano", seed);
		
		if (index >= RESEED_BYTES_SIZE){
			return seed;
		}
		    
		// currentTime: 4 bytes (two of them may be null, two are mostly the same) -> take 4 bytes
		long timeSeed =  System.currentTimeMillis(); 
		ByteBuffer buffer = ByteBuffer.allocate(8);
		byte[] timeBytes = buffer.putLong(timeSeed).array();
		System.arraycopy(timeBytes,  4, seed, index, 4);
		Zeroizer.zero(timeBytes);
		index += 4;
		//Main.printBytes("seed nano + time", seed);
		
		if (index >= RESEED_BYTES_SIZE){
			return seed;
		}
		    
		// free memory: 4 bytes (bad values)
		long freeMemory = Runtime.getRuntime().freeMemory();
		seed[index++] = (byte) freeMemory;
		seed[index++] = (byte) (freeMemory >>> 8);
		seed[index++] = (byte) (freeMemory >>> 16);
		seed[index++] = (byte) (freeMemory >>> 32);
		freeMemory = 0;
		//Main.printBytes("seedForStir time + pid + loc + mem", seed);	
		
		if (index >= RESEED_BYTES_SIZE){
			return seed;
		}
		 return seed;
	}
}
