package cologne.eck.peafactory.crypto.kdf;

/*
 * PeaFactory - Production of Password Encryption Archives
 * Copyright (C) 2015  Axel von dem Bruch
 * 
 * This library is free software; you can redistribute it and/or modify it 
 * under the terms of the GNU General Public License as published 
 * by the Free Software Foundation; either version 2 of the License, 
 * or (at your option) any later version.
 * This library is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
 * or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU General Public License for more details.
 * See:  http://www.gnu.org/licenses/gpl-2.0.html
 * You should have received a copy of the GNU General Public License 
 * along with this library.
 */

import cologne.eck.all_peas.data.Attachments;
import cologne.eck.all_peas.data.PeaProperties;
import cologne.eck.crypto.Blake2b_1;
import cologne.eck.crypto.Catena;
import cologne.eck.crypto.CatenaBRG;
import cologne.eck.crypto.CatenaDBG;

/*
 * PeaFactory - Production of Password Encryption Archives
 * Copyright (C) 2015  Axel von dem Bruch
 * 
 * This library is free software; you can redistribute it and/or modify it 
 * under the terms of the GNU General Public License as published 
 * by the Free Software Foundation; either version 2 of the License, 
 * or (at your option) any later version.
 * This library is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
 * or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU General Public License for more details.
 * See:  http://www.gnu.org/licenses/gpl-2.0.html
 * You should have received a copy of the GNU General Public License 
 * along with this library.
 */

/**
 * Password Hashing Scheme Catena (v3) version v3.2 as key derivation function
 */

import cologne.eck.peafactory.crypto.CipherStuff;
import cologne.eck.tools.Comparator;
import cologne.eck.tools.TestLog;
import cologne.eck.tools.UnexpectedValueException;
import settings.PeaSettings;


public class CatenaKDF extends KeyDerivation {
	
	// Default setting:
	private static String versionID = "Dragonfly-Full";
	private static int lambdaDragonfly = 2;
	private static int garlicDragonfly = 18;
	
	private static int lambdaButterfly = 4;
	private static int garlicButterfly = 14;

	public CatenaKDF() {
		if ((versionID.equals("Dragonfly-Full")) || (versionID.equals("Dragonfly"))){
			setVersionString(versionID);
		} else if ((versionID.equals("Butterfly-Full")) || (versionID.equals("Butterfly"))){
			setVersionString(versionID);
		} else {
			TestLog.e(CatenaKDF.class, "Invalid Catena version ID");
		}
		setKdf(this);
	}

	@Override
	public byte[] performKDF(byte[] pswMaterial) {
		
		Catena cat = null;
		if (getVersionString().equals("Dragonfly-Full")){
			cat = new CatenaBRG(false, true); // not fast, wipe password
			cat.setFast(false);
		} else if (getVersionString().equals("Butterfly-Full")){
			cat = new CatenaDBG(false, true);
			cat.setFast(false);
		} else if (getVersionString().equals("Butterfly")){
			cat = new CatenaDBG(true, true);
			cat.setFast(true);
			cat.setFastHash(new Blake2b_1());
		} else if (getVersionString().equals("Dragonfly")){
			cat = new CatenaBRG(true, true);
			cat.setFast(true);
			cat.setFastHash(new Blake2b_1());
		} else {
			throw new IllegalArgumentException("invalid version ID");
		}
		
		byte[] salt = KeyDerivation.getSalt();
		if (salt == null) {
			new UnexpectedValueException("byte[]", "salt","is null").printDescription();
			throw new IllegalArgumentException("Missing salt");
		} else {
			if (PeaProperties.isTestMode()) {
				Comparator.checkNullVector(salt);
				Comparator.checkNullVector(pswMaterial);
			}
		}

		byte[] key = null;
		// figure out if key file property:
		if (PeaSettings.getKeyFileProperty() == false) {
			key = new byte[CipherStuff.getKeySize()];
			cat.deriveKey(
					pswMaterial, 
					salt, // used as salt for initial hash
					salt, // used as public input, vulnerable to cache-timing attacks
					getExtraValues(),  
			       gettCost(), getmCost(), getmCost(), // lambda, minGarlic, garlic
			       0, key); // key id = 0
		} else { // key file property
			// prevent cache-timing attacks on salt (pepper)
			key = new byte[CipherStuff.getKeySize()];
			cat.deriveKey(
					pswMaterial, 
					// secret bytes are also already used in initial HKDF
					Attachments.getProgramRandomBytes(), // used as salt/pepper for initial hash, kept secret
					KeyDerivation.getSalt(), // used as public input, vulnerable to cache-timing attacks
					getExtraValues(),  
			       gettCost(), getmCost(), getmCost(), // lambda, minGarlic, garlic
			       0, key); // key id = 0
		}
		return key;
	}

	@Override
	public String getName() {
		return "Catena";
	}

	/**
	 * @return the versionID
	 */
	public static String getVersionID() {
		return versionID;
	}

	/**
	 * @param versionID the versionID to set
	 */
	public static void setVersionID(String versionID) {
		CatenaKDF.versionID = versionID;
	}

	/**
	 * @return the lambdaDragonfly
	 */
	public static int getLambdaDragonfly() {
		return lambdaDragonfly;
	}

	/**
	 * @param lambda the lambdaDragonfly to set
	 */
	public static void setLambdaDragonfly(int lambda) {
		CatenaKDF.lambdaDragonfly = lambda;
	}

	/**
	 * @return the garlicDragonfly
	 */
	public static int getGarlicDragonfly() {
		return garlicDragonfly;
	}

	/**
	 * @param garlic the garlicDragonfly to set
	 */
	public static void setGarlicDragonfly(int garlic) {
		CatenaKDF.garlicDragonfly = garlic;
	}

	/**
	 * @return the lambdaButterfly
	 */
	public static int getLambdaButterfly() {
		return lambdaButterfly;
	}

	/**
	 * @param lambdaButterfly the lambdaButterfly to set
	 */
	public static void setLambdaButterfly(int lambdaButterfly) {
		CatenaKDF.lambdaButterfly = lambdaButterfly;
	}

	/**
	 * @return the garlicButterfly
	 */
	public static int getGarlicButterfly() {
		return garlicButterfly;
	}

	/**
	 * @param garlicButterfly the garlicButterfly to set
	 */
	public static void setGarlicButterfly(int garlicButterfly) {
		CatenaKDF.garlicButterfly = garlicButterfly;
	}

	/**
	 * Get the required memory for the key derivation function. 
	 * Returns the required memory in MiB. 
	 * 
	 * @return	memory requirement in MiB
	 */
	@Override
	public int getMemoryRequirement() {
		if ((versionID.equals("Dragonfly-Full")) || (versionID.equals("Dragonfly"))){
			return (1 << garlicDragonfly) * 64 / (1024*1024); // 64 is digest output size of Blake2b
		} else if ((versionID.equals("Butterfly-Full")) || (versionID.equals("Butterfly"))){
			return ( (1 << garlicButterfly) + (1 << (garlicButterfly -1) ) ) * 64 / (1024*1024); // 64 is digest output size of Blake2b			
		} else {
			TestLog.e(CatenaKDF.class, "Unknown version of Catena");
			return 0;
		}
	}

	@Override
	public String getInfoString() {
		StringBuilder sb = new StringBuilder();
		sb.append(getVersionID() + ", ");
		sb.append("garlic: " + getmCost() + ", ");
		sb.append("lambda: " + gettCost() );
		return new String(sb);
	}

	@Override
	public synchronized int gettCost() {
		if ((versionID.equals("Dragonfly-Full")) || (versionID.equals("Dragonfly"))){
			return lambdaDragonfly;
		} else {
			return lambdaButterfly;
		}
	}

	@Override
	public synchronized int getmCost() {
		if ((versionID.equals("Dragonfly-Full")) || (versionID.equals("Dragonfly"))){
			return garlicDragonfly;
		} else {
			return garlicButterfly;
		}
	}

	@Override
	public int getArg3() {
		return 0;
	}

	@Override
	public int getArg4() {
		return 0;
	}

	@Override
	public int getArg5() {
		return 0;
	}

	@Override
	public int getArg6() {
		return 0;
	}

	@Override
	public boolean setParameters(int[] args) {

		if (args == null || args.length < 2) {
			TestLog.ve(getClass(), "Catena requires 2 parameters", 5);
			return false;
		}
		if (args[0] < 1) {
			TestLog.ve(getClass(), "Invalid time (lambda) parameter: " + args[0], 5);
			return false;
		}
		if (versionID.startsWith("Dragonfly")) {					
			// memory: garlic < 63
			if (args[1] > 63 || args[1] < 21) { // Dragonfly
				TestLog.ve(getClass(), "Invalid memory (garlic) parameter: " + args[1], 5);
				return false;
			} else {
				lambdaDragonfly = args[0];	
				garlicDragonfly = args[1];
			}
		} else if (versionID.startsWith("Butterfly")) {			
			// memory: garlic < 63
			if (args[1] > 63 || args[1] < 16) { // Butterfly
				TestLog.ve(getClass(), "Invalid memory (garlic) parameter: " + args[1], 5);
				return false;	
			} else {
				lambdaButterfly = args[0];	
				garlicButterfly = args[1];
			}
		} else {
			TestLog.ve(getClass(), "Invalid version ID: " + versionID, 5);
			return false;
		}
		return true;
	}

/*	@Override
	public synchronized void settCost(int _tCost) {
		if ((versionID.equals("Dragonfly-Full")) || (versionID.equals("Dragonfly"))){
			lambdaDragonfly = _tCost;
		} else {
			lambdaButterfly = _tCost;
		}
	}

	@Override
	public synchronized void setmCost(int _mCost) {
		if ((versionID.equals("Dragonfly-Full")) || (versionID.equals("Dragonfly"))){
			garlicDragonfly = _mCost;
		} else {
			garlicButterfly = _mCost;
		}
	}

	@Override
	public void setArg3(int _arg3) {}
	@Override
	public void setArg4(int _arg4) {}
	@Override
	public void setArg5(int _arg5) {}
	@Override
	public void setArg6(int _arg6) {}*/
}
