package cologne.eck.peafactory.crypto.kdf;

import cologne.eck.all_peas.data.PeaProperties;
import cologne.eck.all_peas.gui.KeyFileDialogs;
import cologne.eck.peafactory.crypto.CipherStuff;
import cologne.eck.peafactory.crypto.HashStuff;
import cologne.eck.tools.Comparator;
import cologne.eck.tools.TestLog;
import cologne.eck.tools.UnexpectedValueException;


/*
 * Peafactory - Production of Password Encryption Archives
 * Copyright (C) 2015  Axel von dem Bruch
 * 
 * This library is free software; you can redistribute it and/or modify it 
 * under the terms of the GNU General Public License as published 
 * by the Free Software Foundation; either version 2 of the License, 
 * or (at your option) any later version.
 * This library is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
 * or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU General Public License for more details.
 * See:  http://www.gnu.org/licenses/gpl-2.0.html
 * You should have received a copy of the GNU General Public License 
 * along with this library.
 */

/**
 * parent class of all used key derivation functions 
 */

public abstract class KeyDerivation {
	
	private static KeyDerivation kdf = null;
	
	private static int saltSize = 32;// at most 129 byte for fixed initialized salt
	private static byte[] salt;	

	private static byte[] extraValues = "peafactory_extra".getBytes(); // 16 byte
	
	private static String versionString = "";
	
	public abstract byte[] performKDF(byte[]pswMaterial);
	
	/**
	 * Derive the key. All parameters are set in child classes
	 * 
	 * @param pswMaterial	material build from the password
	 * 
	 * @return				the derived key
	 */
	public static byte[] deriveKey( byte[] pswMaterial) {
		
		TestLog.v(KeyDerivation.class, "Derive key: KDF: " + kdf.getName() + " ,t=" + kdf.gettCost() + ",m=" + kdf.getmCost() + ",p=" + kdf.getArg3()
		+ ", Hash: " + HashStuff.getHashAlgo().getAlgorithmName() 
		+ ", Cipher: " + CipherStuff.getCipherAlgo().getAlgorithmName());
		
		if (pswMaterial == null) { // should never be null because of pre-hashing
			PeaProperties.getVmBridge().showUrgentMessage(null, 
					PeaProperties.getVmBridge().translate("unexpected_error") +":\n" + "null password material");
			TestLog.e(KeyDerivation.class, "KDF failed...");
			TestLog.ve(KeyDerivation.class, "KDF failed...", 5);
			return null;
		}
		
		byte[] keyMaterial = kdf.performKDF(pswMaterial);
		if (keyMaterial == null) {
			PeaProperties.getVmBridge().showUrgentMessage(null, 
					PeaProperties.getVmBridge().translate("unexpected_error") +":\n" + "null key");
			TestLog.e(KeyDerivation.class, "KDF failed...");
			TestLog.ve(KeyDerivation.class, "KDF failed...", 5);
			return null;
		}
		byte[] keyMaterialWithKeyFile = KeyFileDialogs.applyKeyFile(keyMaterial);
		if (keyMaterialWithKeyFile == null) {
			TestLog.e(KeyDerivation.class, "Applying key file failed...");
			TestLog.ve(KeyDerivation.class, "Key file hash failed...", 5);
			return null;
		}		
		return keyMaterialWithKeyFile;
	}
	
	/**
	 * Get the name of the key derivation scheme
	 * 
	 * @return the name of the key derivation scheme
	 */
	public abstract String getName();
	
	/**
	 * Get the required memory for the key derivation function. 
	 * Returns the required memory in MiB. 
	 * 
	 * @return	memory requirement in MiB
	 */
	public abstract int getMemoryRequirement();

	/**
	 * Get a key of required length (expand or extract) using HKDF; 
	 * this is only used for Bcrypt and Scrypt
	 * 
	 * @param keyMaterial	the material from key derivation
	 * @return				the key
	 */
	protected final static byte[] adjustKeyMaterial(byte[] keyMaterial){
		
		if (PeaProperties.isTestMode()){
			Comparator.checkNullVector(keyMaterial);
		}
		//Help.printBytes("key", HashStuff.generateHKDFBytes(keyMaterial, null, CipherStuff.getKeySize()));
		
		return HashStuff.generateHKDFBytes(keyMaterial, salt, CipherStuff.getKeySize());
	}
	
	/**
	 * Get info (name, version, parameters) about the used KDF
	 * 
	 * @return info about the used KDF and parameters as String
	 */
	public abstract String getInfoString();
	
	//=============================================
	// Getter & Setter  
		
	/**
	 * @return the tCost
	 */
	public abstract int gettCost();

	/**
	 * @return the mCost
	 */
	public abstract int getmCost();

	/**
	 * @return the arg3
	 */
	public abstract int getArg3();

	/**
	 * @return the arg4
	 */
	public abstract int getArg4();

	/**
	 * @return the arg5
	 */
	public abstract int getArg5();

	/**
	 * @return the arg6
	 */
	public abstract int getArg6();

	/**
	 * Set the parameters for the key derivation function
	 * and check if the parameters are in the acceptable limits. 
	 * 
	 * @param args	the parameters as Integers: 
	 * 				[0] is time, [1] is memory, [2] parallelism, 
	 * 				other parameters are undefined
	 * 
	 * @return	true if the parameters are in the acceptable limits, 
	 * 			false if at least one parameter is not permitted
	 */
	public abstract boolean setParameters(int[] args);
	
	/**
	 * @return the salt
	 */
	public static byte[] getSalt() {
		if (salt == null) {
			TestLog.v(KeyDerivation.class, "Accessed salt is yet null");
			//estLog.ve(KeyDerivation.class, "Salt is null", 5);
		}
		return salt;
	}

	/**
	 * @param _salt the salt to set
	 */
	public static void setSalt(byte[] _salt) {		
		//TestLog.v(KeyDerivation.class, "set salt "+Converter.bytes2hex(_salt), 9);
		if (_salt == null) {
			salt = null;
			new UnexpectedValueException("byte[]","salt","set salt is null").printDescription();			
		} else {
			if (_salt.length < saltSize) {
				if (_salt.length < 16) {
					new UnexpectedValueException("int","_salt.length","is too short: " + _salt.length).printDescription();			
				} else {
					TestLog.e(KeyDerivation.class, "Warning: short salt size: " + _salt.length);
				}
			}
			Comparator.checkNullVector(_salt);
			salt = new byte[_salt.length];
			System.arraycopy(_salt,  0,  KeyDerivation.salt,  0,  _salt.length);
		}
	} 

	/**
	 * @return the extraValues
	 */
	protected static byte[] getExtraValues() {
		return extraValues;
	}

	/**
	 * @return the kdf
	 */
	public static KeyDerivation getKdf() {
		return kdf;
	}

	/**
	 * Set the key derivation function.
	 * The KDF should always be set with 
	 * setting the parameters in the constructor
	 * 
	 * @param kdf the kdf to set
	 */
	public static void setKdf(KeyDerivation newKdf) {
//TestLog.e(KeyDerivation.class, "===============Set KDF: " + newKdf.getName(), 7);
//TestLog.e(KeyDerivation.class, "Set " + kdf.getInfoString(),5);
//TestLog.o(KeyDerivation.class, "Set " + kdf.getInfoString(),5);
		KeyDerivation.kdf = newKdf;
		//settCost(kdf.gettCost());
	}

	/**
	 * Get the version string: 
	 * For Catena Dragonfly-Full, Dragonfly, Butterfly-Full, Butterfly, 
	 * for Argon2 Argon2d, Argon2i, Argon2id, 
	 * for all other KDFs null
	 * 
	 * @return the versionString
	 */
	public static String getVersionString() {
		return versionString;
	} 
	/**
	 * Set the version string: 
	 * For Catena Dragonfly-Full, Dragonfly, Butterfly-Full, Butterfly, 
	 * for Argon2 Argon2d, Argon2i, Argon2id, 
	 * for all other KDFs null
	 * 
	 * @param versionString the versionString to set
	 */
	public static void setVersionString(String versionString) {
		KeyDerivation.versionString = versionString;
	}
	
	public static int getSaltSize(){
		if (salt == null) {
			return saltSize;
		} else {
			return salt.length;
		}
	}
	
	/**
	 * This is for testing only
	 * 
	 * @param newSaltSize
	 */
	protected static void setSaltSize(int newSaltSize) {
		TestLog.e(KeyDerivation.class, "KeyDerivation: set salt size for testing: " + newSaltSize);
		saltSize = newSaltSize;
	}
}
