package cologne.eck.peafactory.crypto.kdf;

import org.bouncycastle.crypto.Digest;
import org.bouncycastle.crypto.PBEParametersGenerator;
import org.bouncycastle.crypto.generators.PKCS5S2ParametersGenerator;
import org.bouncycastle.crypto.params.KeyParameter;

import cologne.eck.all_peas.data.PeaProperties;
import cologne.eck.peafactory.crypto.HashStuff;
import cologne.eck.tools.Comparator;
import cologne.eck.tools.TestLog;

public class SingleIterationPBKDF2 extends KeyDerivation {

	/**
	 * PBKDF2 with single iteration with salt and password hash. 
	 * This is used only for KDF replacement if a keyfile is applied. 
	 * Input is the initial HKDF hash (with fixed "salt" for standard PEAs and no_password),
	 * to be used as input for HKDF hash with keyfile
	 */
	public SingleIterationPBKDF2() {
		setKdf(this);
	}

	@Override
	public byte[] performKDF(byte[] pswMaterial) {
		
		if (PeaProperties.getKeyFileFlag() == 0) {
			TestLog.e(getClass(), "This class can only be used along with a keyfile");
			return null;
		}
		
		byte[] salt = KeyDerivation.getSalt();
		if (salt == null) {
			TestLog.e(getClass(), "Missing salt in key derivation");
			throw new IllegalArgumentException("Missing salt");
		} else {
			if (PeaProperties.isTestMode()) {
				Comparator.checkNullVector(salt);
				Comparator.checkNullVector(pswMaterial);
			}
		}
		Digest hashAlgo = HashStuff.getHashAlgo();
		PBEParametersGenerator pbe = new PKCS5S2ParametersGenerator(hashAlgo);
		pbe.init(pswMaterial, salt, 1);
		byte[] pbeHash = ((KeyParameter) pbe.generateDerivedMacParameters(256)).getKey(); // fixed size: this is not directly used for a key
				// but as replacement for KDF in case of keyfile		
		if (PeaProperties.isTestMode()) {
			Comparator.checkNullVector(pbeHash);
		}
		return pbeHash;
	}

	@Override
	public String getName() {
		return "One iteration PBKDF2";
	}

	@Override
	public int getMemoryRequirement() {
		return 0;
	}

	@Override
	public String getInfoString() {
		return getName();
	}

	@Override
	public int gettCost() {
		return 0;
	}

	@Override
	public int getmCost() {
		return 0;
	}

	@Override
	public int getArg3() {
		return 0;
	}

	@Override
	public int getArg4() {
		return 0;
	}

	@Override
	public int getArg5() {
		return 0;
	}

	@Override
	public int getArg6() {
		return 0;
	}

	@Override
	public boolean setParameters(int[] args) {
		return true;
	}

}
