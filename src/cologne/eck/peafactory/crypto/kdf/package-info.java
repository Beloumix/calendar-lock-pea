/**
 * Specific key derivation function crypto classes for PeaFactory
 * 
 * Whenever a class of this package is instantiated, 
 * it is set as current KDF in KeyDerivation
 */
/**
 * Package for KDFs
 *
 */
package cologne.eck.peafactory.crypto.kdf;