package cologne.eck.peafactory.crypto.kdf;

/*
 * PeaFactory - Production of Password Encryption Archives
 * Copyright (C) 2015  Axel von dem Bruch
 * 
 * This library is free software; you can redistribute it and/or modify it 
 * under the terms of the GNU General Public License as published 
 * by the Free Software Foundation; either version 2 of the License, 
 * or (at your option) any later version.
 * This library is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
 * or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU General Public License for more details.
 * See:  http://www.gnu.org/licenses/gpl-2.0.html
 * You should have received a copy of the GNU General Public License 
 * along with this library.
 */

import org.bouncycastle.crypto.generators.SCrypt;

import cologne.eck.all_peas.data.PeaProperties;
import cologne.eck.tools.Comparator;
import cologne.eck.tools.TestLog;
import cologne.eck.tools.UnexpectedValueException;
import settings.PeaSettings;

public class ScryptKDF extends KeyDerivation {
	
	private static int memoryFactor = 32; // r Memory cost parameter
	private static int cPUFactor = 16384;//16384; // N CPU cost parameter
	private static int parallelFactor = 1;// 1; // p Parallelization parameter

	public ScryptKDF() {

/*		settCost(cPUFactor);
		setmCost(memoryFactor);
		setArg3(parallelFactor);
		setArg4(0);
		setArg5(0);
		setArg6(0);*/
		setVersionString("");
		setKdf(this);
//		setParameters(cPUFactor, memoryFactor, parallelFactor);
	}

	@Override
	public byte[] performKDF(byte[] pswMaterial) {	
		
		if (KeyDerivation.getSalt() == null) {
			new UnexpectedValueException("byte[]", "Keyderivation.getSalt()","is null").printDescription();
			throw new IllegalArgumentException("Missing salt");
		} else {
			if (PeaProperties.isTestMode()) {
				Comparator.checkNullVector(KeyDerivation.getSalt());
				Comparator.checkNullVector(pswMaterial);
			}
		}

		if (KeyDerivation.getSalt().length < 16) {
			TestLog.e(ScryptKDF.class, "Warning: Scrypt: short salt.");
		}
		if(KeyDerivation.getSalt().length < 8) {
			TestLog.e(ScryptKDF.class, "Srypt: salt too short");
			throw new IllegalArgumentException("Scrypt - invalid salt size");
		}
		if (PeaSettings.getKeyFileProperty() == true) {
			// secret bytes are already used in initial HKDF
		}
		//TODO TEST  get parameters from PeaSettings
	/*	if (PeaSettings.getKdfScheme() instanceof ScryptKDF) {
			memoryFactor = PeaSettings.getMemory();
			cPUFactor = PeaSettings.getIterations();
			parallelFactor = PeaSettings.getParallelization();
		} */

		//long start =  System.currentTimeMillis(); // Startpunkt	
		byte[] keyMaterial = null;
		try {
		    keyMaterial = SCrypt.generate(pswMaterial, KeyDerivation.getSalt(), 
		    		cPUFactor, memoryFactor, parallelFactor, 64);			
		    		//gettCost(), getmCost(), getArg3(), 64);			
		} catch (Exception e) { 
			TestLog.e(ScryptKDF.class, "ScryptKDF Exception.");
			e.printStackTrace();
		}
TestLog.o(ScryptKDF.class, "Scrypt: " + cPUFactor + " iterations, " + memoryFactor + " memory factor" );
		//printInfos(true);
		
		keyMaterial = adjustKeyMaterial(keyMaterial);
//Help.printBytes("keyMaterial", keyMaterial);
		return keyMaterial;
	}

	@Override
	public String getName() {
		return "Scrypt";
	}

	/**
	 * @return the memoryFactor
	 */
	/* public static int getMemoryFactor() {
		return memoryFactor;
	} */

	/**
	 * @param _memoryFactor the memoryFactor to set
	 */
	public static void setMemoryFactor(int _memoryFactor) {
		memoryFactor = _memoryFactor;
	}

	/**
	 * @return the cPUFactor
	 */
	/* public static int getcPUFactor() {
		return cPUFactor;
	} */

	/**
	 * @param _cPUFactor the cPUFactor to set
	 */
	public static void setcPUFactor(int _cPUFactor) {
		cPUFactor = _cPUFactor;
	}

	/**
	 * @return the parallelFactor
	 */
	/* public static int getParallelFactor() {
		return parallelFactor;
	} */

	/**
	 * @param _parallelFactor the parallelFactor to set
	 */
	public static void setParallelFactor(int _parallelFactor) {
		parallelFactor = _parallelFactor;
	}

	/**
	 * Get the required memory for the key derivation function. 
	 * Returns the required memory in MiB. 
	 * 
	 * @return	memory requirement in MiB
	 */
	@Override
	public int getMemoryRequirement() {
		return 128 * cPUFactor * memoryFactor / (1024 * 1024);
	}

	@Override
	public String getInfoString() {
		StringBuilder sb = new StringBuilder();
		sb.append(getName() + ", ");
		sb.append("r: " + getmCost() + ", ");
		sb.append("N: " + gettCost()+ ", ");
		sb.append("p: " + getArg3());
		return new String(sb);
	}

	@Override
	public synchronized int gettCost() {
		return cPUFactor;
	}

	@Override
	public synchronized int getmCost() {
		return memoryFactor;
	}

	@Override
	public synchronized int getArg3() {
		return parallelFactor;
	}

	@Override
	public int getArg4() {
		return 0;
	}

	@Override
	public int getArg5() {
		return 0;
	}

	@Override
	public int getArg6() {
		return 0;
	}

	@Override
	public boolean setParameters(int[] args) {
		int[] paramsToReset = { cPUFactor, memoryFactor, parallelFactor};
		// must be three parameters
		if (args == null || args.length != 3) {
			TestLog.ve(getClass(), "There must be 3 parameters, but " + ((args == null)? "is null" : "got " + args.length), 5);
			return false;
		}
		// Check first memory, because other limits depend on this parameter
		// memory:  r ("blockSize") - recommended r=8 at 2010,but this is not a limit
		if (args[1] < 1) {
			TestLog.ve(getClass(), "Invalid r (memory) parameters: " + args[1], 5);
			return false;
		} else {
			memoryFactor = args[1];
		}
		// time:
		if (args[0] < 2 ) {
			TestLog.ve(getClass(), "Invalid time parameter, too low: " + args[0], 5);
			// reset memory
			memoryFactor = paramsToReset[1];
			return false;
		}
		if	( ! ( (args[0] & (args[0] - 1)) == 0) ) { // must be power of 2
			TestLog.ve(getClass(), "Invalid time parameter, must be power of 2: " + args[0], 5);
			// reset memory
			memoryFactor = paramsToReset[1];
			return false;
		} else if ((long)args[0] * (long)args[1] > Integer.MAX_VALUE) {
			TestLog.ve(getClass(), "Invalid time/memory parameter, too big: " + args[0] + ", " + args[1], 5);
			// reset memory
			memoryFactor = paramsToReset[1];
			return false;
/* never the case since r is at least 2		if ( args[0] > Math.pow(2, (128 * memoryFactor / 8)) )  { // less than   2^(128 * r / 8)			
			TestLog.ve(getClass(), "Invalid time parameter, is greater than " + (Math.pow(2, (128 * memoryFactor / 8))) + " : " + args[0], 5);
			// reset memory
			memoryFactor = paramsToReset[1];
			return false;
		} */
	} else {
			cPUFactor = args[0];
		}
		// parallel: a positive integer greater than or equal  to ((2^32-1) * 32) / (128 * r)
		if (args[2] < 1 || args[2] > (Integer.MAX_VALUE / (128 * memoryFactor * 8)) ) {
			TestLog.ve(getClass(), "Invalid parallel parameter: " + args[2], 5);
			// reset
			cPUFactor = paramsToReset[0];
			memoryFactor = paramsToReset[1];
			return false;
		} else {
			parallelFactor = args[2];
		}
		return true;
	}
}
