package cologne.eck.peafactory.crypto.kdf;

/*
 * Peafactory - Production of Password Encryption Archives
 * Copyright (C) 2020  Axel von dem Bruch
 * 
 * This library is free software; you can redistribute it and/or modify it 
 * under the terms of the GNU General Public License as published 
 * by the Free Software Foundation; either version 2 of the License, 
 * or (at your option) any later version.
 * This library is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
 * or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU General Public License for more details.
 * See:  http://www.gnu.org/licenses/gpl-2.0.html
 * You should have received a copy of the GNU General Public License 
 * along with this library.
 */

import org.bouncycastle.crypto.generators.Argon2BytesGenerator;
import org.bouncycastle.crypto.params.Argon2Parameters;

import cologne.eck.all_peas.data.PeaProperties;
import cologne.eck.peafactory.crypto.CipherStuff;
import cologne.eck.tools.Comparator;
import cologne.eck.tools.TestLog;
import cologne.eck.tools.UnexpectedValueException;
import settings.PeaSettings;


// see: 
// source:
// https://github.com/bcgit/bc-java/tree/master/core/src/main/java/org/bouncycastle/crypto/generators
// test:
// https://github.com/bcgit/bc-java/blob/master/core/src/test/java/org/bouncycastle/crypto/test/Argon2Test.java

public class Argon2KDF extends KeyDerivation {
	
//    private static final int ARGON2_d = 0x00;
//    private static final int ARGON2_i = 0x01;
    private static final int ARGON2_id = 0x02;

    private static final int ARGON2_VERSION_13 = 0x13;

    private static int iterations = 3; // 3 t (time) factor
    private static int memory_cost = 512 * 1024;// in KiB - 512 MiB
    private static int lanes = 4; // 4 p 
    private static int type = ARGON2_id;
    private static final int version = ARGON2_VERSION_13; // support only 13
    
    public Argon2KDF() {
    	
    	setType(type);
    	//TestLog.o(XXXX.class, "Argon2 constructor -  lanes: " + lanes + ", mem: " + memory_cost + ", time: " + iterations);
    	setKdf(this);
    }

	@Override
	public byte[] performKDF(byte[] pswMaterial) {
		TestLog.v(Argon2KDF.class, "ArgoneKDF type: " + type + ", lanes: " + lanes + ", mem: " + memory_cost + ", time: " + iterations);

		if (KeyDerivation.getSalt() == null) {
			new UnexpectedValueException("byte[]", "Keyderivation.getSalt()","is null").printDescription();
			throw new IllegalArgumentException("Missing salt");
		} else {
			if (PeaProperties.isTestMode()) {
				Comparator.checkNullVector(KeyDerivation.getSalt());
				Comparator.checkNullVector(pswMaterial);
			}
		}
		Argon2Parameters.Builder builder = null;
		if (PeaSettings.getKeyFileProperty() == false) {
			builder = new Argon2Parameters.Builder(type)
				.withVersion(version) 
				.withIterations(gettCost())
				.withMemoryAsKB(getmCost())
				.withParallelism(getArg3())
				// .withAdditional(ad)
				// .withSecret(secret)
				.withSalt( KeyDerivation.getSalt());
		} else { // use secret:
			builder = new Argon2Parameters.Builder(type)
					.withVersion(version) 
					.withIterations(gettCost())
					.withMemoryAsKB(getmCost())
					.withParallelism(getArg3())
					// .withAdditional(ad)
					// secret bytes are also already used in initial HKDF
					.withSecret(PeaSettings.getProgramRandomBytes())
					.withSalt( KeyDerivation.getSalt());
		}

		Argon2BytesGenerator  dig = new Argon2BytesGenerator();

		dig.init(builder.build());

		byte[] result = new byte[CipherStuff.getKeySize()];
		dig.generateBytes(pswMaterial, result);

		return result;        
	}

	@Override
	/**
	 * This returns only "Argon2", for appendix id/d/i
	 * append getType()
	 */
	public String getName() {
		return "Argon2";
	}

	@Override
	public int getMemoryRequirement() {
		// return MiB (Argon parameter is KiB)
		return memory_cost / 1024 * lanes;
	}
	/**
	 * Set the type of Argon2: 0 for d, 
	 * 1 for i, 2 for id
	 * 
	 * @param newType	the type (Argon2d, i, id that is 0, 1, 2)
	 */
	public static void setType(int newType) {
		Argon2KDF.type = newType;
		if (newType == 0) {
			KeyDerivation.setVersionString("Argon2d");
		} else if (newType == 1) {
			KeyDerivation.setVersionString("Argon2i");
		} else if (newType == 2) {
			KeyDerivation.setVersionString("Argon2id");
		} else {
			new UnexpectedValueException("int", "newType", "is invalid (!= 0,1,2): " + newType).printDescription();
		}
	}
	/**
	 * Get the type of Argon2: 0 for d, 
	 * 1 for i, 2 for id
	 * 
	 * @return	the type (Argon2d, i, id that is 0, 1, 2)
	 */
	public static int getType() {
		return Argon2KDF.type;
	}
	/**
	 * Set time factor
	 * 
	 * @param newIterations	time factor (iterations over memory)
	 */
	public static void setIterations(int newIterations) {
		Argon2KDF.iterations = newIterations;
	}
	/**
	 * Get time factor
	 * 
	 * @return newIterations	time factor (iterations over memory)
	 */
	public static int getIterations() {
		return Argon2KDF.iterations;
	}
	/**
	 * Set memory cost in KiB
	 * 
	 * @param newMemoryCost	memory cost in KiB
	 */
	public static void setMemoryCost(int newMemoryCost) {
		Argon2KDF.memory_cost = newMemoryCost;
	}
	/**
	 * Get memory cost in KiB
	 * @return	memory cost in KiB
	 */
	public static int getMemoryCost() {
		return Argon2KDF.memory_cost;
	}
	/**
	 * Set parallel factor: double of cores
	 * 
	 * @param newLanes	parallel factor (lanes)
	 */
	public static void setLanes(int newLanes) {
		Argon2KDF.lanes = newLanes;
	}
	/**
	 * Get parallel factor
	 * 
	 * @return newLanes	parallel factor (lanes)
	 */
	public static int getLanes() {
		return Argon2KDF.lanes;
	}

	@Override
	public String getInfoString() {
		StringBuilder sb = new StringBuilder();
		sb.append(getName() + " (" + version + "), ");
		sb.append("lanes: " + getArg3() + ", ");
		sb.append("memory: " + getmCost() + ", ");
		sb.append("time: " + gettCost());
		return new String(sb);
	}

	@Override
	public synchronized int gettCost() {
		return iterations;
	}

	@Override
	public synchronized int getmCost() {
		return memory_cost;
	}

	@Override
	public synchronized int getArg3() {
		return lanes;
	}

	@Override
	public int getArg4() {
		return 0;
	}

	@Override
	public int getArg5() {
		return 0;
	}

	@Override
	public int getArg6() {
		return 0;
	}

	@Override
	public boolean setParameters(int[] args) {
		int[] paramsToReset = { iterations, memory_cost, lanes};
		// must be three parameters
		if (args == null || args.length != 3) {
			TestLog.ve(getClass(), "There must be 3 parameters, but " + ((args == null)? "is null" : "got " + args.length), 5);
			return false;
		}
		if (args[0] <= 0) {
			iterations = args[0];
		} else {
			TestLog.ve(getClass(), "Time parameters is out of range: " + args[0]);
			return false;
		}
		// check lanes before memory (memory limit depends an lanes)
		if (args[2] <= 0) { //parallelism - lanes
			lanes = args[2];
		} else {
			TestLog.ve(getClass(), "Parellel parameters is out of range: " + args[2]);
			// reset
			iterations = paramsToReset[0];
			return false;
		}
		// check last: memory
		if (args[1] < (8 * lanes) ) { // memory in KiB, minimum depends on lanes
			memory_cost = args[1];
		} else {
			TestLog.ve(getClass(), "Memory parameters is out of range: " + args[1]);
			// reset
			iterations = paramsToReset[0];
			lanes = paramsToReset[2];
			return false;
		}
		return true;
	}
}
