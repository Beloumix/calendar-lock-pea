package cologne.eck.peafactory.crypto.kdf;

import org.bouncycastle.crypto.generators.BCrypt;

import cologne.eck.all_peas.data.PeaProperties;
import cologne.eck.tools.Comparator;
import cologne.eck.tools.TestLog;

/*
 * PeaFactory - Production of Password Encryption Archives
 * Copyright (C) 2015  Axel von dem Bruch
 * 
 * This library is free software; you can redistribute it and/or modify it 
 * under the terms of the GNU General Public License as published 
 * by the Free Software Foundation; either version 2 of the License, 
 * or (at your option) any later version.
 * This library is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
 * or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU General Public License for more details.
 * See:  http://www.gnu.org/licenses/gpl-2.0.html
 * You should have received a copy of the GNU General Public License 
 * along with this library.
 */

/**
 * Class to call Bcrypt as key derivation function
 */



public class BcryptKDF extends KeyDerivation {
	
	private static int rounds = 11;

	public BcryptKDF() {
		setVersionString("");
		setKdf(this);
	//	setParameters(rounds);
	}

	@Override
	public byte[] performKDF(byte[] pswMaterial) {

		byte[] salt = getSalt();
		if (PeaProperties.isTestMode()) {
			Comparator.checkNullVector(salt);
			Comparator.checkNullVector(pswMaterial);
		}
		if (salt.length != 16) {
			if (salt.length > 16) {
				System.out.println( "Warning: Bcrypt uses only 16 byte salt - salt is truncated");
				byte[] tmp = new byte[16];
				System.arraycopy(getSalt(), 0, tmp, 0, tmp.length);
				salt = tmp;
			}
			//throw new IllegalArgumentException("Bcrypt - invalid salt size");
		}

	/*	if (PeaSettings.getKdfScheme() instanceof BcryptKDF) {
			rounds = PeaSettings.getIterations();
		}*/
		byte[] keyMaterial = null;
		try {
		    keyMaterial = BCrypt.generate(pswMaterial, salt, gettCost());		    		
		    		// TODO TEST
		    	/*	new BcryptCore().deriveRawKey(
		    		gettCost(), 
		    		salt, 
		    		pswMaterial);*/
		    } catch (Exception e) {
			e.printStackTrace();
		}
		//TestLog.o(XXXX.class, "Bcrypt:  2 ^" + rounds + " iterations" );
		//printInfos(true);

		keyMaterial = adjustKeyMaterial(keyMaterial);

		return keyMaterial;
	}

	@Override
	public String getName() {
		return "Bcrypt";
	}

	/**
	 * @return the rounds
	 */
	/*public static int getRounds() {
		return rounds;
	}*/

	/**
	 * @param _rounds the rounds to set
	 */
	public static void setRounds(int _rounds) {
		rounds = _rounds;
	}


	/**
	 * Get the required memory for the key derivation function. 
	 * Returns the required memory in MiB. 
	 * 
	 * @return	memory requirement in MiB
	 */
	@Override
	public int getMemoryRequirement() {
		// BCrypt uses 4 KiB constant memory in eksblowfish
		return 1;
	}

	@Override
	public String getInfoString() {
		StringBuilder sb = new StringBuilder();
		sb.append(getName() + ", ");
		sb.append("rounds: " + gettCost());
		return new String(sb);
	}

	@Override
	public synchronized int gettCost() {
		return rounds;
	}

	@Override
	public int getmCost() {
		return 0;
	}

	@Override
	public int getArg3() {
		return 0;
	}

	@Override
	public int getArg4() {
		return 0;
	}

	@Override
	public int getArg5() {
		return 0;
	}

	@Override
	public int getArg6() {
		return 0;
	}

	@Override
	public boolean setParameters(int[] args) {
		if (args == null || args.length < 1) {
			TestLog.ve(getClass(), "Missing parameters", 5);
			return false;
		}
		if (args[0] >= 4 || args[0] < 31) { // log2(Iterations)
			rounds = args[0];
		} else {
			TestLog.ve(getClass(), "Invalid time parameter: " + args[0], 5);
			return false;
		}
		return true;
	}

/*	@Override
	public synchronized void settCost(int _tCost) {
		rounds = _tCost;		
	}

	@Override
	public void setmCost(int _mCost) {}
	@Override
	public void setArg3(int _arg3) {}
	@Override
	public void setArg4(int _arg4) {}
	@Override
	public void setArg5(int _arg5) {}
	@Override
	public void setArg6(int _arg6) {} */
}
