package cologne.eck.peafactory.crypto;

import org.bouncycastle.crypto.BlockCipher;

/*
 * Peafactory - Production of Password Encryption Archives
 * Copyright (C) 2015  Axel von dem Bruch
 * 
 * This library is free software; you can redistribute it and/or modify it 
 * under the terms of the GNU General Public License as published 
 * by the Free Software Foundation; either version 2 of the License, 
 * or (at your option) any later version.
 * This library is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
 * or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU General Public License for more details.
 * See:  http://www.gnu.org/licenses/gpl-2.0.html
 * You should have received a copy of the GNU General Public License 
 * along with this library.
 */

/**
 * Handling of session keys to hold the key in RAM.
 * This is required to save new content and to change the password.
 */

/* 
 * Encrypt/decrypt the derived key with 5 session keys: 
 * keys are hopefully difficult to find in RAM, 
 * if an attacker has access it.
 * This is a similar construction like DES-X but only to
 * hamper exhaustive search for the keys in RAM. 
 */


import cologne.eck.all_peas.data.Attachments;
import cologne.eck.all_peas.data.PeaProperties;
import cologne.eck.tools.Comparator;
import cologne.eck.tools.TestLog;
import cologne.eck.tools.UnexpectedValueException;
import cologne.eck.tools.Zeroizer;

public class SessionKeyCrypt {
	
	private static boolean isRunning = false;
	
	public SessionKeyCrypt() {
		if (isRunning == false) {
			isRunning = true;
		} else {
			TestLog.ve(SessionKeyCrypt.class, "Instance of SessionKeyCrypt already running", 5);
			throw new IllegalStateException("SessionKeyCrypt: already running instance");
		}
	}
	// cipher to encrypt and decrypt the key
	private BlockCipher lastUsedCipher = null;
	
	// keys to encrypt encryptedKey
	private static byte[] sessionKey1;
	private static byte[] sessionKey2;
	private static byte[] sessionKey3;
	private static byte[] sessionKey4;
	private static byte[] sessionKey5;
	
	// key to encrypt plainText
	private static byte[] encryptedKey; 
	
	private boolean keysCleared = false;

	
	/**
	 * Decrypt the session key, use the cipher that was used to encrypt
	 * 
	 * @return	the key
	 */
	public final byte[] getKey() {
		// reset after decryption of the keys:
		BlockCipher cipherToReset = CipherStuff.getCipherAlgo();
		// use the cipher, that was used to encrypt the keys
		// the cipher might be changed
		CipherStuff.setCipherAlgo(lastUsedCipher);
		
		if (keysCleared == true) {
			TestLog.e(this.getClass(), "Key was cleared...");
			TestLog.ve(this.getClass(), "Try to get key, but was cleared...", 5);
			//new UnexpectedValueException("byte[]", "encryptedKey", "is null").printDescription();
			CipherStuff.setCipherAlgo(cipherToReset);
			return null;
		}

		// check for program bug
		if (encryptedKey != null) {
			if (PeaProperties.isTestMode()){
				Comparator.checkNullVector(encryptedKey);
				Comparator.checkNullVector(sessionKey5);
			}
		} else {
			TestLog.ve(this.getClass(), "Try to get key, but was not yet set...", 5);
			//new UnexpectedValueException("byte[]", "encryptedKey", "is null").printDescription();
			CipherStuff.setCipherAlgo(cipherToReset);
			return null;
		}
		
		byte[] result = new byte[encryptedKey.length];			

		System.arraycopy(encryptedKey, 0, result, 0, encryptedKey.length);		
		
		int nonceSize = Attachments.getNonceSize();//cipher.getBlockSize();
		int keyLen = CipherStuff.getKeySize();
		int size = keyLen + nonceSize + nonceSize;

		// 1. XOR with sessionKey5, skip IVs from step 2 and 4:
		for (int i = 0; i < size; i++) {
			result[i] ^= sessionKey5[i];
		}
		// 2. decrypt with sessionKey4: 
		result = CipherStuff.processCTR(false, result, sessionKey4, false);
		// 3. XOR with sessionKey3, skip IV from step 2:
		size -= nonceSize;
		for (int i = 0; i < size; i++) {
			result[i] ^= sessionKey3[i];
		}
		// 4. encrypt with sessionKey2: 
		result = CipherStuff.processCTR(false, result, sessionKey2, false);
		// 5. XOR with sessionKey1
		size -= nonceSize;
		for (int i = 0; i < keyLen; i++) {
			result[ i] ^= sessionKey1[i];
		}
		if (result.length != CipherStuff.getKeySize()) {
			new UnexpectedValueException("byte[]", "key","has invalid size: " + result.length
					+ ", but should have: " +  CipherStuff.getKeySize()).printDescription();
		}
		// reset the cipher:
		CipherStuff.setCipherAlgo(cipherToReset);
		return result;
	}
	
	/**
	 * Encrypt the session key
	 * 
	 * @param key	the key to encrypt, this value holds then the key in encrypted form
	 */
	public final void storeKey(byte[] key) {	
		
		// set cipher to use this cipher for decryption, even when cipher was changed
		lastUsedCipher = CipherStuff.getCipherAlgo();
		
		if (key.length % lastUsedCipher.getBlockSize() != 0) {
			if (PeaProperties.isTestMode()){
				TestLog.ve(SessionKeyCrypt.class, "Invalid key length", 5);
				new UnexpectedValueException("byte[]", "key","has invalid size: " + key.length
						+ ", but should have: " +  CipherStuff.getKeySize()).printDescription();
			}
			TestLog.e(SessionKeyCrypt.class, "SessionKeyCrypt: Wrong key size");
			// TODO exit with dialog 
			//PeaProperties.getVmBridge().exitWithError(null, "SessionKeyCrypt.storeKey(): Invalid key size: " + key.length
			//		+ " - " + CipherStuff.getCipherAlgo().getAlgorithmName(), true);	
		}
		if (key.length < lastUsedCipher.getBlockSize() ) {
			if (PeaProperties.isTestMode()){
				TestLog.e(SessionKeyCrypt.class, "SessionKeyCrypt key too short");
				Thread.currentThread().getStackTrace();
			}
			TestLog.e(SessionKeyCrypt.class, "SessionKeyCrypt: key size to short");
			// TODO exit with dialog  
			//PeaProperties.getVmBridge().exitWithError(null, "SessionKeyCrypt.storeKey(): key size to short", true);	
		}
		if (key.length != CipherStuff.getKeySize()) {
			TestLog.ve(SessionKeyCrypt.class, "Invalid key length", 5);
			new UnexpectedValueException("byte[]", "key","has invalid size: " + key.length
					+ ", but should have: " +  CipherStuff.getKeySize()).printDescription();
		}
		// check for program bug
		if (PeaProperties.isTestMode()){
			Comparator.checkNullVector(key);
		}
		checkSessionKeys();
		if (PeaProperties.isTestMode()){
			Comparator.checkNullVector(sessionKey1);
		}
		
		int nonceSize = Attachments.getNonceSize();//cipher.getBlockSize();
		int keyLen = CipherStuff.getKeySize();
		int size = keyLen;
		// 1. XOR with sessionKey1: 
		for (int i = 0; i < keyLen; i++) {
			key[i] ^= sessionKey1[i];
		}
		// 2. encrypt with sessionKey2: 
		key = CipherStuff.processCTR(true, key, sessionKey2, false);		
		// 3. XOR with sessionKey3, skip IV from step 2:
		size += nonceSize;
		for (int i = 0; i < size; i++) {
			key[i] ^= sessionKey3[i];
		}
		// 4. encrypt with sessionKey4: 
		key = CipherStuff.processCTR(true, key, sessionKey4, false);
		// 5. XOR with sessionKey5, skip IVs from step 2 and 4: 
		size += nonceSize;
		for (int i = 0; i < size; i++) {
			key[i] ^= sessionKey5[i];
		}		
		encryptedKey = new byte[key.length];
		System.arraycopy(key,  0,  encryptedKey,  0,  key.length);
		
		keysCleared = false;
		//Help.printBytes("SessionKeyCrypt storeKey\n   encryptedKey", encryptedKey);
	}
	
	private final void checkSessionKeys() {
		
		int keySize = CipherStuff.getKeySize();
		int nonceSize = Attachments.getNonceSize();
		boolean newKeySize = false;
		if (sessionKey1 !=  null && sessionKey1.length != keySize) {
			newKeySize = true;
		}
		
		if (sessionKey1 == null || newKeySize == true) {
			sessionKey1 = generateSessionKey(keySize);
		}
		if (sessionKey2 == null || newKeySize == true) {
			sessionKey2 = generateSessionKey(keySize);
		}
		if (sessionKey3 == null || newKeySize == true) { // key + IV for CTR
			sessionKey3 = generateSessionKey(keySize + nonceSize);
		}
		if (sessionKey4 == null || newKeySize == true) {
			sessionKey4 = generateSessionKey(keySize);
		}
		if (sessionKey5 == null || newKeySize == true) { // key + 2 * IV for CTR
			sessionKey5 = generateSessionKey(keySize + nonceSize + nonceSize);
		}		
	}
	
	/**
	 * Generate temporary key to encrypt the session key
	 * 
	 * @param size	the required size of the key
	 * @return		the generated key
	 */
	private final byte[] generateSessionKey(int size) {
		return new RandomStuff().createRandomBytes(size );
	}
	
	/**
	 * Clear all used keys
	 */
	public final void clearKeys() {

		if ( sessionKey1 != null) {
			Zeroizer.zero(sessionKey1);
			sessionKey1 = null;// throw NullpointerException for access...
		}
		if ( sessionKey2 != null) {
			Zeroizer.zero(sessionKey2);
			sessionKey2 = null;
		}
		if ( sessionKey3 != null) {
			Zeroizer.zero(sessionKey3);
			sessionKey3 = null;
		}
		if ( sessionKey4 != null) {
			Zeroizer.zero(sessionKey4);
			sessionKey4 = null;
		}
		if ( sessionKey5 != null) {
			Zeroizer.zero(sessionKey5);
			sessionKey5 = null;
		}
		if ( encryptedKey != null) {
			Zeroizer.zero(encryptedKey);
			encryptedKey = null; // throw NullpointerException for access...
		}
		isRunning = false;
		keysCleared = true;
	}
	
	/**
	 * Check if the session key was derived and set
	 * 
	 * @return	true if the key was derived and set
	 */
	public boolean isKeySet() {
		if (encryptedKey == null) {
			return false;
		} else {
			if (PeaProperties.isTestMode()){
				Comparator.checkNullVector(encryptedKey);
			}
			return true;
		}
	}
}
