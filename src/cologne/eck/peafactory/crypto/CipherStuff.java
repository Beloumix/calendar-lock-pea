package cologne.eck.peafactory.crypto;

import java.util.Arrays;

/*
 * Peafactory - Production of Password Encryption Archives
 * Copyright (C) 2015  Axel von dem Bruch
 * 
 * This library is free software; you can redistribute it and/or modify it 
 * under the terms of the GNU General Public License as published 
 * by the Free Software Foundation; either version 2 of the License, 
 * or (at your option) any later version.
 * This library is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
 * or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU General Public License for more details.
 * See:  http://www.gnu.org/licenses/gpl-2.0.html
 * You should have received a copy of the GNU General Public License 
 * along with this library.
 */

/**
 * Performs the encryption/decryption of byte blocks
 * ECB mode for session key and key handling.
 * File encryption is done in the mode class.
 */

//import javax.swing.JOptionPane;

import org.bouncycastle.crypto.BlockCipher;
import org.bouncycastle.crypto.BufferedBlockCipher;
import org.bouncycastle.crypto.DataLengthException;
import org.bouncycastle.crypto.InvalidCipherTextException;
import org.bouncycastle.crypto.modes.SICBlockCipher;
import org.bouncycastle.crypto.params.KeyParameter;
import org.bouncycastle.crypto.params.ParametersWithIV;

import cologne.eck.all_peas.data.AttachmentHandler;
import cologne.eck.all_peas.data.Attachments;
import cologne.eck.all_peas.data.PeaProperties;
import cologne.eck.peafactory.crypto.kdf.KeyDerivation;
import cologne.eck.tools.Comparator;
import cologne.eck.tools.ExceptionHelper;
import cologne.eck.tools.TestLog;
import cologne.eck.tools.UnexpectedValueException;
import cologne.eck.tools.Zeroizer;
import settings.PeaSettings;




public class CipherStuff {
	
	private static CipherStuff cipherStuff = new CipherStuff();
	
	private static BlockCipher cipherAlgo;
	
	private static AuthenticatedEncryption authenticatedEncryption = new EAXMode();
	
	private static String errorMessage = null;
	
	private static SessionKeyCrypt skc = null;
	
	
	/**
	 * Get the required length of key in bytes
	 * 
	 * @return	the required key length in bytes
	 */
	public final static int getKeySize() {
		
		int keySize = 0;
		
		if (cipherAlgo == null) {
			TestLog.e(CipherStuff.class, "CipherStuff getKeySize: cipherAlgo null", 5);
			PeaProperties.getVmBridge().exitWithError(null, "CipherStuff.getKeySize(): Missing cipher algo", true);
		}
		
		if (cipherAlgo.getAlgorithmName().startsWith("Threefish") ) {
			// key size = block size for Threefish 256/512/1024
			keySize = cipherAlgo.getBlockSize();
		} else if ( cipherAlgo.getAlgorithmName().equals("Twofish" )) {
			keySize = 32; // Bytes
		} else if (cipherAlgo.getAlgorithmName().equals("Serpent") ) {
			//TestLog.o(CipherStuff.class, "SerpentEngine");
			keySize = 32; // Bytes			
		} else if (cipherAlgo.getAlgorithmName().equals("AES") ) {
			keySize = 32; // Bytes		
/*		} else if (cipherAlgo.getAlgorithmName().equals("AESFast") ) {
			keySize = 32; // Bytes	*/
		} else if (cipherAlgo.getAlgorithmName().equals("Shacal2") ) {
			keySize = 64; // Bytes
		} else if (cipherAlgo.getAlgorithmName().equals("LEA") ) {
			keySize = 32; // Bytes
		} else if (cipherAlgo.getAlgorithmName().equals("RC6") ) {
			keySize = 32; // Bytes
		} else if (cipherAlgo.getAlgorithmName().equals("DSTU7624") ) {
			keySize = 64; // Bytes
		} else {
			TestLog.ve(CipherStuff.class, "CipherStuff getKeySize: invalid Algorithm: " + cipherAlgo.getAlgorithmName(),  8);
			PeaProperties.getVmBridge().exitWithError(null, "CipherStuff.getKeySize(): Invalid cipher algo", true);
		}
		return keySize;
	}
	
	/**
	 * Encrypt/decrypt an array of bytes. This function is only used to 
	 * encrypt keys (session key, cloud provider keys)
	 * 
	 * @param forEncryption - true for encryption, false for decryption
	 * @param input			- plain text or cipher text
	 * @param key			- the cryptographic key for the cipher
	 * @param zeroize		- fills the key with 0 for true (when the key is no longer used)
	 * @return				- plain text or cipher text
	 */
	public final static byte[] processCTR(boolean forEncryption, byte[] input, byte[] key,
			boolean zeroize) {
		
		if (input == null) {
			new UnexpectedValueException("byte[]", "input", "is null").printDescription();
			return null;
		}
		// check for program bug
		if (PeaProperties.isTestMode()){
			Comparator.checkNullVector(key);
		}
		if (key.length != CipherStuff.getKeySize()) {
			TestLog.ve(CipherStuff.class, "CipherStuff getKeySize: invalid Algorithm: " + cipherAlgo.getAlgorithmName(),  8);
			new UnexpectedValueException("byte[]", "key","has invalid size: " + key.length
					+ ", but should have: " +  CipherStuff.getKeySize() + " (" 
					+ CipherStuff.getCipherAlgo().getAlgorithmName() + ")" ).printDescription();
			PeaProperties.getVmBridge().exitWithError(null, 
					PeaProperties.getVmBridge().translate("unexpected_error")
					+ ": invalid key size for encryption/decryption", forEncryption);
		}

		KeyParameter kp = new KeyParameter(key);
		if (zeroize == true) {
			Zeroizer.zero(key);
			key = null;
		}				
		byte[] iv = null;
		
		if (forEncryption == false) {
			// input is ciphertext, IV is stored on top of ciphertext
			// get IV:
			iv = new byte[CipherStuff.getCipherAlgo().getBlockSize()];
			System.arraycopy(input,  0,  iv,  0,  iv.length);
			// truncate ciphertext:
			byte[] tmp = new byte[input.length - iv.length];
			System.arraycopy(input,  iv.length, tmp,  0,  tmp.length);
			input = tmp;
		} else {
			// input is plaintext
			iv = new RandomStuff().createRandomBytes(CipherStuff.getCipherAlgo().getBlockSize());
		}
		ParametersWithIV ivp = new ParametersWithIV(kp, iv);

		BufferedBlockCipher b = new BufferedBlockCipher(new SICBlockCipher(cipherAlgo));

        b.init(true, ivp);
        byte[] out = new byte[input.length];

        int len = b.processBytes(input, 0, input.length, out, 0);

        try {
			len += b.doFinal(out, len);
		} catch (DataLengthException e) {
			if (PeaProperties.isTestMode()){
				TestLog.e(CipherStuff.class, e.toString() + " - " + e.getLocalizedMessage(), 5);
			}
		} catch (IllegalStateException e) {
			if (PeaProperties.isTestMode()){		
				TestLog.e(CipherStuff.class, e.toString() + " - " + e.getLocalizedMessage(), 5);
			}
		} catch (InvalidCipherTextException e) {
			if (PeaProperties.isTestMode()){
				TestLog.e(CipherStuff.class, e.toString() + " - " + e.getLocalizedMessage(), 5);
			}
		}
        Zeroizer.zero(kp.getKey());

		if (forEncryption == false) {// decryption: output plaintext
			return out;
		} else { // encryption: output (IV || ciphertext)
			Zeroizer.zero(input);
			// store IV on top of ciphertext
			byte[] tmp = new byte[out.length + iv.length];
			System.arraycopy(iv,  0,  tmp,  0,  iv.length);
			System.arraycopy(out,  0,  tmp,  iv.length,  out.length);
			return tmp;
		}		
	}

	/**
	 * Encrypt an array of bytes. The plaintext is zeroized. 
	 * 
	 * @param plainBytes			the plain text to encrypt (is to be zeroized)
	 * @param keyMaterial			the derived key or null (there
	 * 								is a session key available)
	 * @param encryptBySessionKey	if true, the derived key is encrypted, 
	 * 								otherwise the key is cleared
	 * @param testNonce				nonce only for testing
	 * 
	 * @return						the cipher text
	 */
	public final byte[] encrypt(byte[] plainBytes, byte[] keyMaterial, 
			boolean encryptBySessionKey, byte[] testNonce){

		checkSessionKeyCrypt();
		
		// Check if plainBytes is too long to be loaded in memory:
		if (plainBytes.length > 8192 * 64 * 16) { // fileBlockSize = 8192 * 64 * 16
			
			// check free memory to avoid swapping:
			System.gc(); // this might not work...
			long freeMemory = Runtime.getRuntime().maxMemory() 
					- (Runtime.getRuntime().totalMemory()
					- Runtime.getRuntime().freeMemory());
			//TestLog.o(CipherStuff.class, "Free memory: " + freeMemory);
			if (plainBytes.length <= freeMemory) {			
				TestLog.o(CipherStuff.class, "Warning: long plain text");
			} else {
				TestLog.e(CipherStuff.class, 
					    "The content you want to encrypt is too large \n"
					    + "to encrypt in one block on your system: \n"
					    + plainBytes.length + "\n\n"
					    + "Use the file encrytion pea instead. \n"
					    + "(If you blew up an existing text based pea, \n"
					    + "you have to save the content by copy-and-paste).");
				PeaProperties.getVmBridge().exitWithError(null, "CipherStuff.encrypt(): The content you want to encrypt is too large", true);
			}			
		}
		byte[] iv = null;
		if (testNonce == null) {
			// unique for each encryption
			iv = Attachments.generateNonce();
		} else { // test case
			if (PeaProperties.isTestMode() == false){
				new UnexpectedValueException("boolean", "testMode", "is false but static nonce in use").printDescription();
				return null;
			} else {
				iv = testNonce;
			}
		}
		// if nonce was used and filled before...
		if (PeaProperties.isTestMode()){
			Comparator.checkNullVector(iv);
		}
		//Help.printBytes("CipherStuff encrypt iv/nonce", iv);

		byte[] key = detectKey(keyMaterial);
		//Help.printBytes("CipherStuff encrypt key", key);
		//
		//------ENCRYPTION: 
		//				
		if (PeaProperties.isTestMode()){
			Comparator.checkNullVector(key);
		}
		// contains the MAC: 
		byte[] cipherBytes = authenticatedEncryption.processBytes(true, plainBytes, key, iv);	
		if (cipherBytes == null) {
			TestLog.e(this.getClass(), "Encryption failed");
			TestLog.ve(this.getClass(), "Encryption failed", 5);
			return null;
		}
		Zeroizer.zero(plainBytes);						
		//Help.printlastBytes("CipherStuff encrypt cipherBytes + MAC", cipherBytes, 64);
		
		// add pswIdentifier to check password in decryption before check of Mac
		byte[] pswIdentifierAttachment = null;
		try {
			if (PeaProperties.isTestMode()){
				Comparator.checkNullVector(key);
			}
			pswIdentifierAttachment = Attachments.createPswIdentifierAttachment(key, iv);
		} catch (IllegalStateException e) {
			errorMessage = "###Unexpected error by encrypting pswIdentifier: Illegal state of cipher";
			if (PeaProperties.isTestMode()){		
				TestLog.e(CipherStuff.class, new ExceptionHelper(e).getInfos());
			} else {
				TestLog.e(CipherStuff.class, e.getLocalizedMessage());
			}
			return null;
			
		} catch (InvalidCipherTextException e) {
			errorMessage = "###Unexpected error by encrypting pswIdentifier: Invalid cipher text";
			if (PeaProperties.isTestMode()){		
				TestLog.e(CipherStuff.class, new ExceptionHelper(e).getInfos());
			} else {
				TestLog.e(CipherStuff.class, e.getLocalizedMessage());
			}
			return null;
		}
		cipherBytes = Attachments.addBytes(cipherBytes, pswIdentifierAttachment);
		// prepare to save:
		cipherBytes = Attachments.addNonce(cipherBytes, iv);	
	
		cipherBytes = Attachments.attachBytes(cipherBytes, KeyDerivation.getSalt());
		
		cipherBytes = Attachments.attachBytes(cipherBytes, AlgoParamHandler.writeString(false).getBytes(AttachmentHandler.getASCIICharset()));

		cipherBytes = Attachments.addFileIdentifier(cipherBytes);	

		//
		// handle the keys
		//
		handleKey(key, encryptBySessionKey);
		return cipherBytes;
	}	

	/**
	 * Encrypt the derived key and use as session key
	 * 
	 * @param keyMaterial	the derived key
	 */
	final void encryptKeyFromKeyMaterial(byte[] keyMaterial) {
		
		checkSessionKeyCrypt();
		
		if (keyMaterial == null) {
			if (PeaProperties.isTestMode()){		
				TestLog.e(CipherStuff.class, "Error: new keyMaterial null (CryptStuff.encryptKeyFromKeyMaterial");
				Thread.currentThread().getStackTrace();
			} else {
				TestLog.e(CipherStuff.class, "Fatal error: CipherStuff encryptKeyFromKeyMaterial");
				PeaProperties.getVmBridge().exitWithError(null, "CipherStuff.encryptKeyFromKeyMatreial(): keyMaterial is null", true);
			}
		} else {
			if (PeaProperties.isTestMode()){
				Comparator.checkNullVector(keyMaterial);	
			}
		}
		byte[] key = detectKey(keyMaterial);
		handleKey(key, true);
	}
	
	/**
	 * Decrypt an array of bytes. 
	 * 
	 * @param cipherText			the cipher text to decrypt
	 * @param keyMaterial			the derived key or null if 
	 * 								there is a session key
	 * @param encryptBySessionKey	true: encrypt the key, false: clear key
	 * @return						the plain text or null if the
	 * 								decryption failed
	 */
	public final byte[] decrypt(byte[] cipherText, byte[] keyMaterial, 		
			boolean encryptBySessionKey) {
		
		// check if SessionKeyCrypt is null
		checkSessionKeyCrypt();

		// check
		if (cipherText == null) {
			TestLog.e(CipherStuff.class, PeaProperties.getVmBridge().translate("no_ciphertext"));
			errorMessage = "no_ciphertext";
			return null;
		}	
		int version = -1;
		byte[] fileID = Attachments.getEndOfBytes(cipherText, Attachments.getFileIdentifierSize());
		if (Arrays.equals(fileID, AttachmentHandler.getVersion1ID())) {
			version = 1;
		} else if (Arrays.equals(fileID, AttachmentHandler.getVersion2ID())) {
			version = 2;
		} else if (Arrays.equals(fileID, Attachments.getFileIdentifier())) {
			version = 0; //  key file property
		} else {
			TestLog.e(CipherStuff.class, PeaProperties.getVmBridge().translate("unsuitable_ciphertext_id"));
			errorMessage = "unsuitable_ciphertext_id";
			return null;
		}
		cipherText = Attachments.checkFileIdentifier(cipherText, true);
		
		if (cipherText == null) { // check returns null if failed
			TestLog.e(CipherStuff.class, PeaProperties.getVmBridge().translate("unsuitable_ciphertext_id"));
			errorMessage = "unsuitable_ciphertext_id";
			return null;
		}
		if (version == 2 || (version == 0 && PeaSettings.getPEA_VERSION() == 2 )) {
			cipherText = Attachments.cutAlgoParamStringFromEnd(cipherText);
		}
		
		cipherText = Attachments.cutSalt(cipherText);

		byte[] iv = Attachments.calculateNonce(cipherText);
		if (iv == null) {
			TestLog.e(CipherStuff.class, PeaProperties.getVmBridge().translate("unsuitable_ciphertext_len"));
			errorMessage = "unsuitable_ciphertext_len";
			return null;
		} else {
			cipherText = Attachments.cutNonce(cipherText);
		}		

		// Check if plainBytes is too long to be loaded in memory:
		if (cipherText.length > 8192 * 64 * 16) { // fileBlockSize = 8192 * 64 * 16
			
			// get free memory to avoid swapping:
			System.gc(); // this might not work...
			long freeMemory = Runtime.getRuntime().maxMemory() 
					- (Runtime.getRuntime().totalMemory()
							- Runtime.getRuntime().freeMemory());
			//TestLog.o(CipherStuff.class, "Free memory: " + freeMemory);
			if (cipherText.length <= freeMemory) {			
				TestLog.o(CipherStuff.class, "Warning: long plain text");
				
				if (cipherText.length > (freeMemory - 8192 * 64)){
					TestLog.e(CipherStuff.class, 
						    "The content you want to decrypt is already large: \n"
						    + cipherText.length + "\n\n"
						    + "Adding more content may cause a memory error. \n");					
				}
			} else {
				TestLog.e(CipherStuff.class, 
					    "The content you want to decrypt is too large \n"
					    + "to decrypt in one block on your system: \n"
					    + cipherText.length + "\n\n"
					    + "It was probably encrypted on a system with more memory. \n"
					    + "You have to decrypt it on another system... \n");
				PeaProperties.getVmBridge().exitWithError(null, "CipherStuff.decrypt(): The content you want to decrypt is too large", false);
			}			
		}
		
		byte[] key = detectKey(keyMaterial);
		//Help.printBytes("CipherStuff decrypt: key", key);	
		if (PeaProperties.isTestMode()){
			Comparator.checkNullVector(key);
		}
		
		byte[] pswIdentifierAttachment = new byte[Attachments.getPswIdentifierSize()];
		System.arraycopy(cipherText,  
				cipherText.length - pswIdentifierAttachment.length, 
				pswIdentifierAttachment,  
				0, 
				pswIdentifierAttachment.length);
		
		if (Attachments.checkPswIdentifier(pswIdentifierAttachment, key, iv) == false) {
			TestLog.o(CipherStuff.class, "Password identifier failed.");
			TestLog.e(CipherStuff.class, PeaProperties.getVmBridge().translate("password_failed"));
			//TestLog.ve(CipherStuff.class, PeaProperties.getVmBridge().translate("password_failed"), 5);
			errorMessage = "password_failed";
			return null;
		} else {
			// cut pswIdentifier
			byte[] tmp = new byte[cipherText.length - pswIdentifierAttachment.length];
			System.arraycopy(cipherText,  0,  tmp,  0,  tmp.length);
			cipherText = tmp;
		}

		int macLength = CipherStuff.getCipherAlgo().getBlockSize();		
		// check mac length:
		if (cipherText.length < macLength) {
			TestLog.e(CipherStuff.class, PeaProperties.getVmBridge().translate("unsuitable_ciphertext_mac"));
			errorMessage = "unsuitable_ciphertext_mac";
			return null;			
		}

		byte[] plainText = authenticatedEncryption.processBytes(false, cipherText, key, iv);

		//
		// handle the keys
		//		
		handleKey(key, encryptBySessionKey);
		return plainText;
	}

	/**
	 * Get the key from keyMaterial or the session key
	 * 
	 * @param keyMaterial	the derived key or null
	 * @return				the key
	 */
	protected final static byte[] detectKey(byte[] keyMaterial) {
		
		byte[] key = null;
		// Check if this is called from CryptoSetter
		boolean calledFromCryptoSetter = false;
		if (keyMaterial != null) { // use as key 
			//Help.printBytes("CipherStuff detectKey\n   keyMaterial: ",keyMaterial);
			if (PeaProperties.isTestMode()){
				Comparator.checkNullVector(keyMaterial);
			}
			
			// check size:
			int keySize = CipherStuff.getKeySize();
			if (keyMaterial.length != keySize) {
				// Check if this is called from CryptoSetter
				calledFromCryptoSetter = false;
				for (StackTraceElement ste : Thread.currentThread().getStackTrace()) {
					if (ste.toString().contains("CryptoSetter.actionPerformed")) {
						System.out.println("Stack: " + ste);
						calledFromCryptoSetter = true;
						break;
					}
				}
				if (calledFromCryptoSetter == false) {
					if (PeaProperties.isTestMode()){
						TestLog.e(CipherStuff.class, "Warning: Different key size", 8);
						Thread.currentThread().getStackTrace();
					}
					TestLog.e(CipherStuff.class, "CryptStuff detectKey: invalid size of keyMaterial");
					PeaProperties.getVmBridge().exitWithError(null, "CipherStuff.detectKey(): invalid size of keyMaterial", true);
				} else {
					TestLog.v(CipherStuff.class, "Cipher algorithm was changed...");
					key = keyMaterial;
				}
				
			} else {
				key = keyMaterial;
			}
			
		} else { // keyMaterial == null: decrypt encryptedKey to get key

			if (skc == null) {
				if (PeaProperties.isTestMode()){
					TestLog.e(CipherStuff.class, "SessionKeyCrypt null");
					Thread.currentThread().getStackTrace();
				}
				TestLog.e(CipherStuff.class, "SessionKeyCrypt null");
				throw new IllegalStateException("SessionKeyCrypt not initialized");
			} else {
				key = skc.getKey();
			}
			//Help.printBytes("CipherStuff detectKey\n   key: ",key);
		}
		if (key != null) {
			if (key.length != CipherStuff.getKeySize()) {
				if (calledFromCryptoSetter == false) {
					new UnexpectedValueException("byte[]", "key","has invalid size: " + key.length
							+ ", but should have: " +  CipherStuff.getKeySize()).printDescription();
				} else {
					TestLog.v(CipherStuff.class, "Cipher algorithm was changed...");
					key = keyMaterial;
				}
			}
		} else {
			TestLog.e(CipherStuff.class, "Could not get stored session key...");
		}
		return key;
	}
	
	/**
	 * Encrypt or clear the key
	 * 
	 * @param key				the key
	 * @param encryptSessionKey	if true, encrypt the key, if
	 * 							false clear it
	 */
	protected final static void handleKey(byte[] key, boolean encryptSessionKey) {	
		
		if (PeaProperties.isTestMode()){
			Comparator.checkNullVector(key);
		}
		if (encryptSessionKey == false) {
			Zeroizer.zero(key);			
		} else {		
			if (skc == null) {
				skc = new SessionKeyCrypt();
			}
			skc.storeKey(key);
		}
	}
	
	private final static void checkSessionKeyCrypt(){
		if (skc == null) {
			skc = new SessionKeyCrypt();
		}
	}
	
	//=========================
	// Getter & Setter:
	public final static void setCipherAlgo (BlockCipher _cipherAlgo) {
		cipherAlgo = _cipherAlgo;
	}
	
	public final static BlockCipher getCipherAlgo () {
		return cipherAlgo;
	}	
	
	protected final static void setCipherMode (AuthenticatedEncryption _cipherMode) {
		authenticatedEncryption = _cipherMode;
	} 
	
	public final static AuthenticatedEncryption getCipherMode(){
		return authenticatedEncryption;
	}
	
	public final static SessionKeyCrypt getSessionKeyCrypt(){
		if (skc == null) {
			skc = new SessionKeyCrypt();
		}
		return skc;
	}

	public final static String getErrorMessage() {
		return errorMessage;
	}
	
	public final static void setErrorMessage(String _errorMessage) {
		errorMessage = _errorMessage;
	}

	/**
	 * @return the cipherStuff
	 */
	public final static CipherStuff getInstance() {
		if(cipherStuff == null){
			cipherStuff = new CipherStuff();
		}
		return cipherStuff;
	}
}
