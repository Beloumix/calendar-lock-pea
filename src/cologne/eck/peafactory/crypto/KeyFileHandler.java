package cologne.eck.peafactory.crypto;

/*
 * PeaFactory - Production of Password Encryption Archives
 * Copyright (C) 2021  Axel von dem Bruch
 * 
 * This library is free software; you can redistribute it and/or modify it 
 * under the terms of the GNU General Public License as published 
 * by the Free Software Foundation; either version 2 of the License, 
 * or (at your option) any later version.
 * This library is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
 * or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU General Public License for more details.
 * See:  http://www.gnu.org/licenses/gpl-2.0.html
 * You should have received a copy of the GNU General Public License 
 * along with this library.
 */

import java.io.File;

import org.bouncycastle.crypto.digests.Blake2bDigest;

import cologne.eck.tools.ReadResources;
import cologne.eck.tools.TestLog;
import cologne.eck.tools.WriteResources;

public class KeyFileHandler {
	
	private static int keyFileSize = 128;
	
	/**
	 * Create a key file. Check before: if file already exist, 
	 * show warning/confirmation, check access
	 * 
	 * @param keyFile	an existing empty file
	 * 
	 * @return	null for success, an error message otherwise
	 */
	public static String createKeyFile(File keyFile) {
		
		if (keyFile.isFile() == false) {
			TestLog.e(KeyFileHandler.class, "Key file is directory");
			TestLog.ve(KeyFileHandler.class, "Key file is directory", 5);
			return "Key file is directory";
		}

		byte[] poolBytes = new RandomStuff().createRandomBytes(keyFileSize);		
		boolean writeSuccess = WriteResources.write(poolBytes, keyFile.getName(), keyFile.getParent());
		
		if (writeSuccess == true) {
			return null;
		} else {
			TestLog.e(KeyFileHandler.class, "Writing key file failed");
			TestLog.ve(KeyFileHandler.class, "Writing key file failed", 5);
			return "Writing key file failed";
		}
	}
	
	/**
	 * Get the Blake2b hash of the content of key file as bytes
	 * 
	 * @param keyFile	the key file
	 * 
	 * @return	the Blake2b hash of the content of the key file as byte array
	 */
	public static byte[] readAndHashKeyFile(File keyFile) {
		
		if ( ! keyFile.exists()) {
			TestLog.e(KeyFileHandler.class, "Missing key file");
			TestLog.ve(KeyFileHandler.class, "Missing key file", 5);
			return null;
		}
		if ( ! keyFile.canRead() ) {
			TestLog.e(KeyFileHandler.class, "Can't read key file");
			TestLog.ve(KeyFileHandler.class, "Can't read key file", 5);
			return null;
		}
		byte[] content = ReadResources.readExternFile(keyFile.getAbsolutePath());
		if (content == null) {
			TestLog.e(KeyFileHandler.class, "Failed to read key file");
			TestLog.ve(KeyFileHandler.class, "Failed to read key file", 5);
			return null;
		} else {
			// Hash the file content with Blake2b (fix hash fanction)
			Blake2bDigest hashAlgo = new Blake2bDigest();
	        hashAlgo.update(content, 0, content.length);
	        byte[] digest = new byte[hashAlgo.getDigestSize()];
	        hashAlgo.doFinal(digest, 0);
	        hashAlgo.reset();
	        return digest;
		}
	}
	
	/**
	 * Get the size of a generated key file in bytes
	 * 
	 * @return	length of generated key file in bytes
	 */
	public static int getKeyFileSize() {
		return keyFileSize;
	}
}
