package cologne.eck.peafactory.crypto;

/*
 * Peafactory - Production of Password Encryption Archives
 * Copyright (C) 2020  Axel von dem Bruch
 * 
 * This library is free software; you can redistribute it and/or modify it 
 * under the terms of the GNU General Public License as published 
 * by the Free Software Foundation; either version 2 of the License, 
 * or (at your option) any later version.
 * This library is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
 * or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU General Public License for more details.
 * See:  http://www.gnu.org/licenses/gpl-2.0.html
 * You should have received a copy of the GNU General Public License 
 * along with this library.
 */

import java.nio.charset.Charset;

import org.bouncycastle.crypto.BlockCipher;
import org.bouncycastle.crypto.Digest;
import org.bouncycastle.crypto.digests.*;
import org.bouncycastle.crypto.engines.*;

import cologne.eck.all_peas.bridge.VMBridge;
import cologne.eck.all_peas.data.PeaProperties;
import cologne.eck.peafactory.crypto.kdf.*;
import cologne.eck.tools.ExceptionHelper;
import cologne.eck.tools.TestLog;
import cologne.eck.tools.UnexpectedValueException;
import settings.PeaSettings;


public class AlgoParamHandler {

	private final static int ALGO_PARAM_STRING_SIZE = 24;
	
	private final static String V1_ALGOPARAM_STRING = "CDUB2TE012$2$-----------";
	
	private final static String[] KDFS = {
			"A-d", "Ai-", "Aid",// Argon 2
			"CDU", "CDA", "CBU", "CBA",  // Catena Dragonfly/Butterfly fUll/fAst
			"PB1", // KDFS[7] single iteration PBKDF2
			}; 
			//"SCR", "Ly2" };
	private final static String[] HASHS = { "B2", "S3", "S2"};// Blake2b, SHA-3, SHA-512
	private final static char[] ciphers = { 'T', 'A', 'S' };

	//private static final String fixedParamString = "XXXXXXXXXXXXXXXX";
	/**
	 * Read an algo-parameter String, set the algorithms and parameters
	 * 
	 * @param algoParams	the algo-parameter String
	 * @param setDefault	true: set only default properties, 
	 * 						false: set for next usage
	 * 
	 * @return	null for success, an error message otherwise
	 */
	public static String readString(String algoParams, boolean setDefaults) {
				
		if (algoParams.length() != ALGO_PARAM_STRING_SIZE) {
			TestLog.ve(AlgoParamHandler.class, "Invalid length of algo-parameter string: " + algoParams.length()
			+ ", must be: " + ALGO_PARAM_STRING_SIZE + " - " + algoParams, 5);
			TestLog.e(AlgoParamHandler.class, "Invalid length of algo-parameter string");		
			return "Invalid length: " + algoParams.length();
		}
		TestLog.v(AlgoParamHandler.class, algoParams);
		
		String kdf = null;
		String[] params = null;
		String hash = null;
		char cipher = '\0';
		int kff = 0; // key file flag
				
		String paramString = algoParams.substring(8, algoParams.length());
		//TestLog.o(XXXX.class, "paramString: " + paramString);
		
		// Check fix parameters from PeaSettings // TODO remove or special mode (no setting allowed)
/*		if (paramString.equals(fixedParamString)) { // 16 'X'
			
			if (setDefaults == true) { 
				PeaProperties.setDefaultKDFInstanceAppendix(null); // Scrypt, Bcrypt or Pomelo
				PeaProperties.setDefaultMCost(PeaSettings.getMemory());
				PeaProperties.setDefaultTCost(PeaSettings.getIterations());
				PeaProperties.setDefaultParallel(PeaSettings.getParallelization());
				PeaProperties.setDefaultKDF(PeaSettings.getKdfScheme()); // Scrypt, Bcrypt or Pomelo
			} else { 
				KeyDerivation.setKdf(PeaSettings.getKdfScheme());
				int[] settingParams = { PeaSettings.getIterations(), PeaSettings.getMemory(), PeaSettings.getParallelization() }; 
				PeaSettings.getKdfScheme().setParameters(settingParams);
				PeaProperties.setAlgoParamString(algoParams.getBytes(AttachmentHandler.getASCIICharset()));
			}
		} else { */

			params = paramString.split("\\$");
			//for(String s : params)TestLog.o(XXXX.class, s);
			if (params.length < 3) { // at least memory and time cost parameter (for Bcrypt memory constant)
				new UnexpectedValueException("String[]", "params", "invalid size: " + params.length).printDescription();
				return "Invalid parameters: " + paramString;
			}
			int tCost = 0,  mCost = 0, pCost = 0;
			try {
				mCost = Integer.parseInt(params[0], 16);
				tCost = Integer.parseInt(params[1], 16);
				// the last part is always trailing "---"
				int paramNumber = params.length;
				if (paramNumber > 3) {
					int p = Integer.parseInt(params[2], 16);
					pCost = p;
				} 

			} catch (NumberFormatException e){
				TestLog.e(AlgoParamHandler.class, e.toString() + " - " + e.getLocalizedMessage(), 5);
				return "Invalid parameters";
			}

			kdf = algoParams.substring(0, 3);
			PeaProperties.setKeyFileOnlyMode(false); // reset
			if (kdf.equals(KDFS[0])) {
				if (setDefaults == true) {
					PeaProperties.setDefaultKDFInstanceAppendix("d");
					PeaProperties.setDefaultMCost(mCost);
					PeaProperties.setDefaultTCost(tCost);
					PeaProperties.setDefaultParallel(pCost);
					PeaProperties.setDefaultKDF(new Argon2KDF());
				} else {
					Argon2KDF.setType(0);
					Argon2KDF.setMemoryCost(mCost);
					Argon2KDF.setIterations(tCost);
					Argon2KDF.setLanes(pCost);
					KeyDerivation.setKdf(new Argon2KDF());
					PeaProperties.setKeyFileOnlyMode(false);
					PeaProperties.setDirectStart(false);
				}
			} else if (kdf.equals(KDFS[1])) {
				if (setDefaults == true) {
					PeaProperties.setDefaultKDFInstanceAppendix("i");
					PeaProperties.setDefaultMCost(mCost);
					PeaProperties.setDefaultTCost(tCost);
					PeaProperties.setDefaultParallel(pCost);
					PeaProperties.setDefaultKDF(new Argon2KDF());
				} else {
					Argon2KDF.setType(1);
					Argon2KDF.setMemoryCost(mCost);
					Argon2KDF.setIterations(tCost);
					Argon2KDF.setLanes(pCost);
					KeyDerivation.setKdf(new Argon2KDF());
					PeaProperties.setKeyFileOnlyMode(false);
					PeaProperties.setDirectStart(false);
				}
			} else if (kdf.equals(KDFS[2])) {
				if (setDefaults == true) {
					PeaProperties.setDefaultKDFInstanceAppendix("id");
					PeaProperties.setDefaultMCost(mCost);
					PeaProperties.setDefaultTCost(tCost);
					PeaProperties.setDefaultParallel(pCost);
					PeaProperties.setDefaultKDF(new Argon2KDF());
				} else {
					Argon2KDF.setType(2);
					Argon2KDF.setMemoryCost(mCost);
					Argon2KDF.setIterations(tCost);
					Argon2KDF.setLanes(pCost);
					KeyDerivation.setKdf(new Argon2KDF());
					PeaProperties.setKeyFileOnlyMode(false);
					PeaProperties.setDirectStart(false);
				}
			} else if (kdf.equals(KDFS[3])) {
				if (setDefaults == true) {
					PeaProperties.setDefaultKDFInstanceAppendix("Dragonfly-Full");
					PeaProperties.setDefaultMCost(mCost);
					PeaProperties.setDefaultTCost(tCost);
					PeaProperties.setDefaultParallel(0);
					PeaProperties.setDefaultKDF(new CatenaKDF());
				} else {
					CatenaKDF.setVersionID("Dragonfly-Full");
					CatenaKDF.setGarlicDragonfly(mCost);
					CatenaKDF.setLambdaDragonfly(tCost);
					KeyDerivation.setKdf( new CatenaKDF() );
					PeaProperties.setKeyFileOnlyMode(false);
					PeaProperties.setDirectStart(false);
				}
			} else if (kdf.equals(KDFS[4])) {
				if (setDefaults == true) {
					PeaProperties.setDefaultKDFInstanceAppendix("Dragonfly");
					PeaProperties.setDefaultMCost(mCost);
					PeaProperties.setDefaultTCost(tCost);
					PeaProperties.setDefaultParallel(0);
					PeaProperties.setDefaultKDF(new CatenaKDF());
				} else {
					CatenaKDF.setVersionID("Dragonfly");
					CatenaKDF.setGarlicDragonfly(mCost);
					CatenaKDF.setLambdaDragonfly(tCost);				
					KeyDerivation.setKdf( new CatenaKDF() );
					PeaProperties.setKeyFileOnlyMode(false);
					PeaProperties.setDirectStart(false);
				}
			} else if (kdf.equals(KDFS[5])) {
				if (setDefaults == true) {
					PeaProperties.setDefaultKDFInstanceAppendix("Butterfly-Full");
					PeaProperties.setDefaultMCost(mCost);
					PeaProperties.setDefaultTCost(tCost);
					PeaProperties.setDefaultParallel(0);
					PeaProperties.setDefaultKDF(new CatenaKDF());
				} else {
					CatenaKDF.setVersionID("Butterfly-Full");
					CatenaKDF.setGarlicButterfly(mCost);
					CatenaKDF.setLambdaButterfly(tCost);
					KeyDerivation.setKdf( new CatenaKDF() );
					PeaProperties.setKeyFileOnlyMode(false);
					PeaProperties.setDirectStart(false);
				}
			} else if (kdf.equals(KDFS[6])){
				if (setDefaults == true) {
					PeaProperties.setDefaultKDFInstanceAppendix("Butterfly");
					PeaProperties.setDefaultMCost(mCost);
					PeaProperties.setDefaultTCost(tCost);
					PeaProperties.setDefaultParallel(0);
					PeaProperties.setDefaultKDF(new CatenaKDF());
				} else {
					CatenaKDF.setVersionID("Butterfly");
					CatenaKDF.setGarlicButterfly(mCost);
					CatenaKDF.setLambdaButterfly(tCost);
					KeyDerivation.setKdf( new CatenaKDF() );
					PeaProperties.setKeyFileOnlyMode(false);
					PeaProperties.setDirectStart(false);
				}
			} else if (kdf.equals(KDFS[7])){
				if (setDefaults == true) {
					PeaProperties.setDefaultKDFInstanceAppendix("");
					PeaProperties.setDefaultMCost(0);
					PeaProperties.setDefaultTCost(0);
					PeaProperties.setDefaultParallel(0);
					PeaProperties.setDefaultKDF(new SingleIterationPBKDF2());
				} else {
					KeyDerivation.setKdf(new SingleIterationPBKDF2() );
				}
				PeaProperties.setKeyFileOnlyMode(true);
			} else if (kdf.equals("XXX")) {
				KeyDerivation keyDev = PeaSettings.getKdfScheme();
				String keyDevName = keyDev.getName();
				if (keyDevName.equals(new CatenaKDF().getName() )
						|| keyDevName.equals(new Argon2KDF().getName())){
							TestLog.e(AlgoParamHandler.class, "Can't read file encrypted with extra KDF");
							return PeaProperties.getVmBridge().translate("key_derivation") + ": " +
									PeaProperties.getVmBridge().translate("invalid") + " (extra KDF)";
									
						}
				
				if (PeaSettings.getKdfScheme() != null) {					
					TestLog.v(AlgoParamHandler.class, "Found extra KDF: " + PeaSettings.getKdfScheme());
					TestLog.v(AlgoParamHandler.class, "KDF: " + kdf);
					if (setDefaults == true) {
						PeaProperties.setDefaultKDFInstanceAppendix(null);
						PeaProperties.setDefaultTCost(PeaSettings.getIterations());
						PeaProperties.setDefaultMCost(PeaSettings.getMemory());							
						PeaProperties.setDefaultParallel(PeaSettings.getParallelization());
						PeaProperties.setDefaultKDF(keyDev);
					} else {							
						int[] paramsXXX = { tCost,  mCost, pCost};
						//PeaSettings.getIterations(),
						//PeaSettings.getMemory(), PeaSettings.getParallelization() };
						keyDev.setParameters(paramsXXX);
						KeyDerivation.setKdf( keyDev );
						//PeaProperties.setAlgoParamString(algoParams.getBytes(AttachmentHandler.getASCIICharset()));
						TestLog.v(AlgoParamHandler.class, "KDF: " + keyDev.getName() + ", t=" + tCost + ",m=" + mCost + ",p=" + pCost);
						PeaProperties.setKeyFileOnlyMode(false);
						PeaProperties.setDirectStart(false);
					}
				} else {
					TestLog.ve(AlgoParamHandler.class, "Invalid KDF marker " + kdf + " - can not set key derivation" , 5);
					return "Invalid key derivation function: " + kdf;
				}		
			} else {
				TestLog.ve(AlgoParamHandler.class, "Invalid KDF marker " + kdf + " - can not set key derivation" , 5);
				return "Invalid key derivation function: " + kdf;
			}
//		}
		hash = algoParams.substring(3, 5);
		if (hash.equals(HASHS[0])) {
			if (setDefaults == true) {
				PeaProperties.setDefaultDigest(new Blake2bDigest());
			} else {
				HashStuff.setHashAlgo(new Blake2bDigest());
			}
		} else if (hash.equals(HASHS[1])) {
			if (setDefaults == true) {
				PeaProperties.setDefaultDigest(new SHA3Digest());
			} else {
				HashStuff.setHashAlgo(new SHA3Digest());
			}
		} else if (hash.equals(HASHS[2])) {
			if (setDefaults == true) {
				PeaProperties.setDefaultDigest(new SHA512Digest());
			} else {
				HashStuff.setHashAlgo(new SHA512Digest());
			}
		} else if (hash.equals("XX")) { // other algo from PeaFactory
			//TestLog.v(AlgoParamHandler.class, "Non default digest: " + PeaSettings.getHashAlgo().getAlgorithmName() + ", set default: " + setDefaults);
			if (setDefaults == true) {
				PeaProperties.setDefaultDigest(PeaSettings.getHashAlgo());
			} else {
				HashStuff.setHashAlgo(PeaSettings.getHashAlgo());
			}
		} else {
			TestLog.e(AlgoParamHandler.class,"Does not match algos: " + hash, 3);
			return "Invalid hash function: " + hash;
		}

		cipher = algoParams.charAt(5);
		if (cipher == ciphers[0]){//'T') {
			if (setDefaults == true) {
				PeaProperties.setDefaultCipher(new ThreefishEngine(512));
			} else {
				CipherStuff.setCipherAlgo(new ThreefishEngine(512));
			}
		} else if (cipher == ciphers[1]){//'A') {
			if (setDefaults == true) {
				PeaProperties.setDefaultCipher(new AESEngine());
			} else {
				CipherStuff.setCipherAlgo(new AESEngine());
			}
		} else if (cipher == ciphers[2]){//'S') {
			if (setDefaults == true) {
				PeaProperties.setDefaultCipher(new SerpentEngine());
			} else {
				CipherStuff.setCipherAlgo(new SerpentEngine());
			}			
		} else if (cipher == 'X') { // other algo from PeaFactory
			//TestLog.v(AlgoParamHandler.class, "Non default cipher: " + PeaSettings.getCipherAlgo().getAlgorithmName() + ", set default: " + setDefaults);
			if (setDefaults == true) {
				PeaProperties.setDefaultCipher(PeaSettings.getCipherAlgo());
			} else {
				CipherStuff.setCipherAlgo(PeaSettings.getCipherAlgo());
			}	
		} else {
			TestLog.e(AlgoParamHandler.class,"Does not match algos: " + cipher, 3);
			return "Invalid cipher: " + cipher;
		}
		char mode = algoParams.charAt(6);
		if (mode == 'E') {
			if (setDefaults == true) {
				PeaProperties.setDefaultMode(new EAXMode());
			} else {
				CipherStuff.setCipherMode(new EAXMode());
			}
		} else {
			new UnexpectedValueException("char", "mode", "does not match algos: " + mode).printDescription();
			return "Invalid mode: " + mode;
		}
		char keyFileFlag = algoParams.charAt(7);
		if (! Character.isDigit(keyFileFlag)) {
			new UnexpectedValueException("char", "keyFileFlag", "is not digit: " + keyFileFlag
					+ " algo parameter string: " + algoParams).printDescription();
			return "Invalid key file flag: " + keyFileFlag;
		}
		kff = Character.getNumericValue(keyFileFlag);
		if (kff >= 0 && kff <= 2) {//== '0' || kff == '1' || kff == '2' ) {
			if (setDefaults == true) {
				PeaProperties.setDefaultKeyFileFlag(kff);
			} else {
				PeaProperties.setKeyFileFlag(kff);
			}
		} else {
			TestLog.o(AlgoParamHandler.class, "kff: " + (kff < 0) + ", " + (kff > 2));
			new UnexpectedValueException("int", "kff", "is invalid: " + kff
					+ " algo parameter string: " + algoParams).printDescription();
			return "Invalid key file flag: " + keyFileFlag;
		}
		TestLog.v(AlgoParamHandler.class, "Set " + ((setDefaults == true)? "defaults" : "current") + " algorithms and parameters" 
		+ ": KDF (current + default): " 
				+  ((KeyDerivation.getKdf() != null) ? KeyDerivation.getKdf().getName() : "Not yet set") + " + " + PeaProperties.getDefaultKDF().getName() 
				+ ", hash (current + default): " 
				+  (( HashStuff.getHashAlgo() != null) ? HashStuff.getHashAlgo().getAlgorithmName() : "Not yet set") + " + " 
				+  ((PeaProperties.getDefaultDigest() != null) ? PeaProperties.getDefaultDigest().getAlgorithmName() : "Not yet set"));
		return null;
	}
	
	/**
	 * Get the cipher algorithm from an algorithm-parameter string
	 * 
	 * @param algoParams	the string as ASCII encoded string
	 * 
	 * @return	the cipher algorithm
	 */
	public static BlockCipher getCipherFromString(String algoParams) {
		if (algoParams.length() != ALGO_PARAM_STRING_SIZE) {
			new UnexpectedValueException("String", "algoParams", "invalidLength: " + algoParams + ", len: " + algoParams.length()).printDescription();
			TestLog.e(AlgoParamHandler.class, "Invalid length: " + algoParams.length());
			return null;
		}
		char cipher = algoParams.charAt(5);
		if (cipher == ciphers[0]){//'T') {
			return new ThreefishEngine(512);
		} else if (cipher == ciphers[1]){//'A') {
			return new AESEngine();
		} else if (cipher == ciphers[2]){//'S') {
			return new SerpentEngine();
		} else if (cipher == 'X') {
			return PeaSettings.getCipherAlgo();
		} else {
			new UnexpectedValueException("char", "cipher", "does not match algos: " + cipher).printDescription();
			return null;
		}
	}
	
	/**
	 * Get the cipher algorithm from an algorithm-parameter string
	 * 
	 * @param algoParams	the string as ASCII encoded string
	 * 
	 * @return	the cipher algorithm
	 */
	public static Digest getHashFromString(String algoParams) {
		if (algoParams.length() != ALGO_PARAM_STRING_SIZE) {
			new UnexpectedValueException("String", "algoParams", "invalidLength: " + algoParams + ", len: " + algoParams.length()).printDescription();
			TestLog.e(AlgoParamHandler.class, "Invalid length: " + algoParams.length());
			return null;
		}
		String hash = algoParams.substring(3, 5);
		if (hash.equals(HASHS[0])) {
			return new Blake2bDigest();
		} else if (hash.equals(HASHS[1])) {
			return new SHA3Digest();
		} else if (hash.equals(HASHS[2])) {
			return new SHA512Digest();
		} else if (hash.equals("XX")) {
			return PeaSettings.getHashAlgo();
		} else {
			new UnexpectedValueException("String", "hash", "does not match algos: " + hash).printDescription();
			return null;
		}
	}
	
	/**
	 * Get the String for current algorithms and parameters
	 * 
	 * @param fromDefaults	true: get values from defaults in PeaProperties, 
	 * 						false: get current algorithms and parameters
	 * 
	 * @return	the String for current algorithms and parameters
	 */
	public static String writeString(boolean fromDefaults) {
		//TestLog.e(AlgoParamHandler.class, "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX", 5);
		String kdfFlag = null, hash = null, mode = null, keyFile = null;
		char cipher = '\0'; 
		int m = 0, t = 0, p = 0;
		Digest digest = null;
		if (fromDefaults == true) {
			digest = PeaProperties.getDefaultDigest();
		} else {
			digest = HashStuff.getHashAlgo();
		}
		if (digest instanceof Blake2bDigest) {
			hash = HASHS[0];//"B2";
		} else if (digest instanceof SHA3Digest) {
			hash = HASHS[1];//"S3";
		} else if (digest instanceof SHA512Digest) {
			hash = HASHS[2];//"S2";
		} else if (digest != null) { // other digest from PeaFactory
			hash = "XX";
		} else {
			TestLog.e(AlgoParamHandler.class, "Hash algorithm is null", 5);
			return null;
		}
		BlockCipher cipherAlgo = null;
		if (fromDefaults == true) {
			cipherAlgo = PeaProperties.getDefaultCipher();
		} else {
			cipherAlgo = CipherStuff.getCipherAlgo();
		}
		if (cipherAlgo instanceof ThreefishEngine && cipherAlgo.getBlockSize() == 64) {
			cipher = ciphers[0];//"T";
		} else if (cipherAlgo instanceof AESEngine) {
			cipher = ciphers[1];//"A";
		} else if (cipherAlgo instanceof SerpentEngine) {
			cipher = ciphers[2];//"S";
		} else if (cipherAlgo != null) { // other cipher from PeaFactory
			cipher = 'X';
		} else {
			TestLog.e(AlgoParamHandler.class, "Cipher algorithm is null", 5);
			return null;
		}
		AuthenticatedEncryption ae = null;
		if (fromDefaults == true) {
			ae = PeaProperties.getDefaultMode();
		} else {
			ae = CipherStuff.getCipherMode();
		}
		if (ae instanceof EAXMode) {
			mode = "E";
		} else {
			new UnexpectedValueException("AuthenticatedEncryption", "ae", "is invalid: " + ae.getClass().getSimpleName()).printDescription();
			return null;
		}

		if (fromDefaults == true) {
			keyFile = "" + PeaProperties.getDefaultKeyFileFlag();
		} else {
			if (PeaProperties.getKeyFileFlag() > 2) {
				new UnexpectedValueException("int", "keyFileFlag", "is invalid: " + PeaProperties.getKeyFileFlag() + " -" + ae.getClass().getSimpleName()).printDescription();
				return null;
			}
			keyFile = "" + PeaProperties.getKeyFileFlag();
		}

		KeyDerivation kdf = null;
		if (fromDefaults == true) {
			kdf = PeaProperties.getDefaultKDF();
			if (kdf == null) {
				new UnexpectedValueException("KeyDerivation", "kdf", "is null.").printDescription();
				return null;
			}

		} else {
			kdf = KeyDerivation.getKdf();
		}
		if (kdf instanceof Argon2KDF) {
			if (fromDefaults == true) {
				String defaultInstance = PeaProperties.getDefaultKDFInstanceAppendix();
				if (defaultInstance == null) {
					TestLog.ve(AlgoParamHandler.class, "Missing instance of Argon2, set to default id", 5);
					defaultInstance = "id";
				}
				if (defaultInstance.equals("i")) {
					kdfFlag = "Ai-";
				} else if (defaultInstance.equals("d")) {
					kdfFlag = "A-d";
				} else if (defaultInstance.equals("id")) {
					kdfFlag = "Aid";
				} else {
					new UnexpectedValueException("String", "kdf appendix", "is null.").printDescription();
					return null;
				}
				m = PeaProperties.getDefaultMCost();
				t = PeaProperties.getDefaultTCost();
				p = PeaProperties.getDefaultParallel();
			} else {
				int type = Argon2KDF.getType();
				if (type == 0) {
					kdfFlag = "A-d";
				} else if (type == 1) {
					kdfFlag = "Ai-";
				} else if (type == 2) {
					kdfFlag = "Aid";
				}
				m = Argon2KDF.getMemoryCost();
				t = Argon2KDF.getIterations();
				p = Argon2KDF.getLanes();
			}
		} else if (kdf instanceof CatenaKDF) {
			if (fromDefaults == true) {
				String defaultInstance = PeaProperties.getDefaultKDFInstanceAppendix();
				if (defaultInstance != null) { // Argon2 or Catena
					if (defaultInstance.equals("Dragonfly")) {
						kdfFlag = "CDA";
					} else if (defaultInstance.equals("Dragonfly-Full")) {
						kdfFlag = "CDU";
					} else if (defaultInstance.equals("Butterfly")) {
						kdfFlag = "CBA";
					} else if (defaultInstance.equals("Butterfly-Full")) {
						kdfFlag = "CBU";
					} else {
						new UnexpectedValueException("String", "versionID", "invalid Catena version: " + defaultInstance).printDescription();
						return null;
					}
				} else { // not set
					// default KDF Catena Dragonfly-Full
					kdfFlag = "CDU";
				}
				m = PeaProperties.getDefaultMCost();
				t = PeaProperties.getDefaultTCost();
				p = 0;
			} else {
				String versionID = CatenaKDF.getVersionID();
				//TestLog.o(XXXX.class, "versionID: " + versionID);
				p = KeyDerivation.getKdf().getArg3();// 0
				if (versionID.startsWith("Dragonfly")) {
					m = CatenaKDF.getGarlicDragonfly();
					t = CatenaKDF.getLambdaDragonfly();				
					if (versionID.equals("Dragonfly-Full")) {
						kdfFlag = "CDU";
					} else if (versionID.equals("Dragonfly")) {
						kdfFlag = "CDA";
					} else {
						new UnexpectedValueException("String", "versionID", "invalid Catena version: " + versionID).printDescription();
						return null;
					}
				} else if (versionID.startsWith("Butterfly")) {
					m = CatenaKDF.getGarlicButterfly();
					t = CatenaKDF.getLambdaButterfly();
					if (versionID.equals("Butterfly-Full")) {
						kdfFlag = "CBU";
					} else if (versionID.equals("Butterfly")) {
						kdfFlag = "CBA";
					} else {
						new UnexpectedValueException("String", "versionID", "invalid Catena version: " + versionID).printDescription();
						return null;
					}
				} else {
					new UnexpectedValueException("String", "version", "invalid Catena version: " + versionID).printDescription();
					return null;
				}
			}
		} else if (kdf instanceof SingleIterationPBKDF2) {
			kdfFlag = "PB1";
			if (fromDefaults == true) {
				TestLog.v(AlgoParamHandler.class, "Keyfile-only mode: " + kdf.getName());
				m = 0;
				t = 0;
				p = 0;
				//TestLog.e(AlgoParamHandler.class, "Set KDF: " + kdf.getName() + ", t=" + t + ",m=" + m +",p=" + p);

			} else {
				TestLog.v(AlgoParamHandler.class, "Keyfile-only mode: " + kdf.getName());
				m = kdf.getmCost();					
				t = kdf.gettCost();
				p = kdf.getArg3();
				TestLog.v(AlgoParamHandler.class, "Set KDF: " + kdf.getName() + ", t=" + t + ",m=" + m +",p=" + p);
			}

		} else if (kdf != null) {
			kdfFlag = "XXX"; 
			if (fromDefaults == true) {
				TestLog.v(AlgoParamHandler.class, "Extra KDF: " + kdf.getName());
				m = PeaProperties.getDefaultMCost();
				t = PeaProperties.getDefaultTCost();
				p = PeaProperties.getDefaultParallel();
				TestLog.e(AlgoParamHandler.class, "Set KDF: " + kdf.getName() + ", t=" + t + ",m=" + m +",p=" + p);

			} else {
		//	if (kdf.getName().equals(PeaSettings.getKdfScheme().getName() ) ) {
				TestLog.v(AlgoParamHandler.class, "Extra KDF: " + kdf.getName());
				m = kdf.getmCost();					
				t = kdf.gettCost();
				p = kdf.getArg3();
				TestLog.v(AlgoParamHandler.class, "Set KDF: " + kdf.getName() + ", t=" + t + ",m=" + m +",p=" + p);
			}
		/*	} else {
				TestLog.e(AlgoParamHandler.class, "Unknowen key derivation algorithm: " + kdf.getName(), 5);
			} */
		} else {
			TestLog.e(AlgoParamHandler.class, "Key derivation algorithm is null", 5);
			return null;
		}
		StringBuilder sb = new StringBuilder();
		//kdfFlag = null, hash = null, cipher = null, mode = null, keyFile = null;
		sb.append(kdfFlag);
		sb.append(hash);
		sb.append(cipher);
		sb.append(mode);
		sb.append(String.valueOf(keyFile));

		sb.append(Integer.toHexString(m) + "$");
		//TestLog.o(XXXX.class, "m: " + m + ", hex: " + Integer.toHexString(m));
		sb.append(Integer.toHexString(t) + "$");
		//TestLog.o(XXXX.class, "t: " + t + ", hex: " + Integer.toHexString(t));
		//TestLog.o(XXXX.class, "p: " +p+", " + new String(sb));
		if (p != 0) {			
			sb.append(Integer.toHexString(p) + "$");
		}
		while (sb.length() < ALGO_PARAM_STRING_SIZE) {
			sb.append('-');
		}

		String algoParamString = new String(sb);		
		TestLog.v(AlgoParamHandler.class, algoParamString, 5);
		return algoParamString;
	}
	
	/**
	 * Check if a String is a valid algorithm-parameter string. 
	 * 
	 * @param algoParams	the string to check
	 * 
	 * @return	null for success, an error message otherwise
	 */
	public static String checkString(String algoParams) {
		
		if (algoParams.length() != ALGO_PARAM_STRING_SIZE) {
			new UnexpectedValueException("String", "algoParams", "invalidLength: " + algoParams + ", len: " + algoParams.length()).printDescription();
			return "Invalid length: " + algoParams.length();
		}
	
		String paramString = algoParams.substring(8, algoParams.length());
/*		if (paramString.equals(fixedParamString) ){ 
			// fixed parameters, set in PeaFactory
		} else { */
			//TestLog.o(XXXX.class, "paramString: " + paramString);
			String[] params = paramString.split("\\$");
			//for(String s : params)TestLog.o(XXXX.class, s);
			if (params.length < 3) { // at least memory and time cost parameter (for Bcrypt memory constant)
				TestLog.e(AlgoParamHandler.class, "Unexpected error");
				TestLog.ve(AlgoParamHandler.class, "algorithm parameter string failed: " + paramString
						+ " from String " + algoParams + ", split in " + params.length + " parts", 5);
				new UnexpectedValueException("String[]", "params", "invalid size: " + params.length).printDescription();
				return "Invalid parameters: " + paramString;
			}
			int tCost = 0,  mCost = 0, pCost = 0;
			try {
				mCost = Integer.parseInt(params[0], 16);
				tCost = Integer.parseInt(params[1], 16);
				// the last part is always trailing "---"
				int paramNumber = params.length;
				if (paramNumber > 3) {
					int p = Integer.parseInt(params[2], 16);
					pCost = p;
					//KeyDerivation.getKdf().setArg3(p);
				} 
			} catch (NumberFormatException e){
				TestLog.e(AlgoParamHandler.class, new ExceptionHelper(e).getInfos());
				TestLog.e(AlgoParamHandler.class, tCost + ", " + mCost + ", " + pCost);
				return "Invalid parameters";
			}
//		}
		
		String kdf = algoParams.substring(0, 3);
		if (kdf.equals(KDFS[0])) {
		} else if (kdf.equals(KDFS[1])) {
		} else if (kdf.equals(KDFS[2])) {
		} else if (kdf.equals(KDFS[3])) {
		} else if (kdf.equals(KDFS[4])) {
		} else if (kdf.equals(KDFS[5])) {
		} else if (kdf.equals(KDFS[6])) {
		} else if (kdf.equals(KDFS[7])) { // SingleIterationPBKDF2
		} else if (kdf.equals("XXX")) { // other KDF from PeaSettings
			//  check if available
			String settingKDF = PeaSettings.getKdfScheme().getName();
			if (settingKDF.equals("Argon2") || settingKDF.equals("Catena")  ) {
				// There is no other KDF available
				return "Used key derivation function not available";				
			}
		} else {
			new UnexpectedValueException("String", "kdf", "does not match algos: " + kdf).printDescription();
			return "Invalid key derivation function: " + kdf;
		}		
		String hash = algoParams.substring(3, 5);
		if (hash.equals(HASHS[0])) {
		} else if (hash.equals(HASHS[1])) {
		} else if (hash.equals(HASHS[2])) {
		} else if (hash.equals("XX")) { // other hash algo from PeaSettings
			String settingHash = PeaSettings.getHashAlgo().getAlgorithmName();
			if (settingHash.equals("BLAKE2b") || settingHash.startsWith("Keccak") || settingHash.equals("SHA-512") ) {
				// There is no other hash available
				return "Used hash function not available";				
			}
		} else {
			new UnexpectedValueException("String", "hash", "does not match algos: " + hash).printDescription();
			return "Invalid hash function: " + hash;
		}
		char cipher = algoParams.charAt(5);
		if (cipher == ciphers[0]){//'T') {
		} else if (cipher == ciphers[1]){//'A') {
		} else if (cipher == ciphers[2]){//'S') {
		} else if (cipher == 'X') { // other cipher algo from PeaSettings
			String settingCipher = PeaSettings.getCipherAlgo().getAlgorithmName();
			if (settingCipher.equals("AES") || settingCipher.equals("Threefish-512") || settingCipher.equals("Serpent") ) {
				// There is no other cipher available
				return "Used cipher function not available";				
			}
		} else {
			new UnexpectedValueException("char", "cipher", "does not match algos: " + cipher).printDescription();
			return "Invalid cipher: " + cipher;
		}
		char mode = algoParams.charAt(6);
		if (mode == 'E') {
		} else {
			new UnexpectedValueException("char", "mode", "does not match algos: " + mode).printDescription();
			return "Invalid mode: " + mode;
		}
		char keyFileFlag = algoParams.charAt(7);
		if (! Character.isDigit(keyFileFlag)) {
			new UnexpectedValueException("char", "keyFileFlag", "is not digit: " + keyFileFlag
					+ " algo parameter string: " + algoParams).printDescription();
			return "Invalid key file flag: " + keyFileFlag;
		}
		int kff = Character.getNumericValue(keyFileFlag);
		if (kff >= 0 && kff <= 2) {//== '0' || kff == '1' || kff == '2' ) {
		} else {
			TestLog.o(AlgoParamHandler.class, "kff: " + (kff < 0) + ", " + (kff > 2));
			new UnexpectedValueException("int", "kff", "is invalid: " + kff
					+ " algo parameter string: " + algoParams).printDescription();
			return "Invalid key file flag: " + keyFileFlag;
		}
		return null;
	}
	
	/**
	 * Get a readable String about algorithms, parameters and key file
	 * 
	 * @param algoParams	the scheme as String
	 * @param multiLine		true: several lines, false; short one line info
	 * 
	 * @return	the readable String about algorithms,parameters and key file
	 */
	public static final String getReadableString(String algoParams, boolean multiLine) {
		//StringBuilder sb = new StringBuilder();
		String keyDerivation = "";
		String mem = "";
		String time = "";
		String parallel = "";
		String cipherAlgo = "";
		String cipherMode = "";
		String hashAlgo = "";
		String keyFile = "";
		
		if (algoParams.length() != ALGO_PARAM_STRING_SIZE) {
			new UnexpectedValueException("String", "algoParams", "invalidLength: " + algoParams + ", len: " + algoParams.length()).printDescription();
			return "Invalid length: " + algoParams.length();
		}
		String kdf = algoParams.substring(0, 3);
		if (kdf.equals(KDFS[0])) {
			keyDerivation = "Argon2d";
		} else if (kdf.equals(KDFS[1])) {
			keyDerivation = "Argon2i";
		} else if (kdf.equals(KDFS[2])) {
			keyDerivation = "Argon2id";
		} else if (kdf.equals(KDFS[3])) {
			keyDerivation = "Dragonfly-Full";
		} else if (kdf.equals(KDFS[4])) {
			keyDerivation = "Dragonfly-Fast";
		} else if (kdf.equals(KDFS[5])) {
			keyDerivation = "Butterfly-Full";
		} else if (kdf.equals(KDFS[6])){
			keyDerivation = "Butterfly-Fast";
		} else if (kdf.equals(KDFS[7])){
			keyDerivation = "Keyfile-only-mode: Single iteration PBKDF2";
		} else if (kdf.equals("XXX")){
			String extraKDFName = PeaSettings.getKdfScheme().getName();
			if (extraKDFName.equals("Catena") || extraKDFName.equals("Argon2")) {
				TestLog.ve(AlgoParamHandler.class, "Invalidn KDF - cant decrypt", 5);
				keyDerivation = "UNKNOWN KDF";
			} else {
				keyDerivation = PeaSettings.getKdfScheme().getName();
			}
			
		} else {
			new UnexpectedValueException("String", "kdf", "does not match algos: " + kdf).printDescription();
			return "Invalid key derivation function: " + kdf;
		}		
//System.out.println("--KDF: " +keyDerivation);
		String hash = algoParams.substring(3, 5);
		if (hash.equals(HASHS[0])) {
			hashAlgo = "Blake2b";
		} else if (hash.equals(HASHS[1])) {
			hashAlgo = "SHA-3";
		} else if (hash.equals(HASHS[2])) {
			hashAlgo = "SHA-512";
		} else if (hash.equals("XX")) {
			hashAlgo = PeaSettings.getHashAlgo().getAlgorithmName();
		} else {
			new UnexpectedValueException("String", "hash", "does not match algos: " + hash).printDescription();
			return "Invalid hash function: " + hash;
		}
		char cipher = algoParams.charAt(5);
		if (cipher == ciphers[0]){//'T') {
			cipherAlgo = "Threefish-512";
		} else if (cipher == ciphers[1]){//'A') {
			cipherAlgo = "AES-256";
		} else if (cipher == ciphers[2]){//'S') {
			cipherAlgo = "Serpent-256";
		} else if (cipher == 'X') {
			cipherAlgo = PeaSettings.getCipherAlgo().getAlgorithmName();
		} else {
			new UnexpectedValueException("char", "cipher", "does not match algos: " + cipher).printDescription();
			return "Invalid cipher: " + cipher;
		}
		char mode = algoParams.charAt(6);
		if (mode == 'E') {
			cipherMode = "EAX mode";
		} else {
			new UnexpectedValueException("char", "mode", "does not match algos: " + mode).printDescription();
			return "Invalid mode: " + mode;
		}
		char keyFileFlag = algoParams.charAt(7);
		if (keyFileFlag == '0' ) {
			keyFile = "";//"no key file";
		} else if (keyFileFlag == '1' ) {
			keyFile = PeaProperties.getVmBridge().translate("with") + " " + PeaProperties.getVmBridge().translate("default")
					+ " " + PeaProperties.getVmBridge().translate("key_file") + ": "
					+ "\n     " + PeaProperties.getDefaultKeyFileName();//"default key file";
		} else if (keyFileFlag == '2' ) {
			keyFile = PeaProperties.getVmBridge().translate("with") + " " + PeaProperties.getVmBridge().translate("key_file");//"chosen key file";
		} else {
			new UnexpectedValueException("char", "keyFileFlag", "is invalid: " + keyFileFlag
					+ "\n" + algoParams).printDescription();
			return "Invalid key file flag: " + keyFileFlag;
		}
		
		String paramString = algoParams.substring(8, algoParams.length());
		
/*		if (paramString.equals(fixedParamString)) {
			mem = "" + PeaSettings.getMemory();
			time = "" + PeaSettings.getIterations();
			parallel = "" + PeaSettings.getParallelization();
		} else { */
			//TestLog.o(XXXX.class, "paramString: " + paramString);
			String[] params = paramString.split("\\$");
			//for(String s : params)TestLog.o(XXXX.class, s);
			if (params.length < 3) { // at least memory and time cost parameter (for Bcrypt memory constant)
				new UnexpectedValueException("String[]", "params", "invalid size: " + params.length).printDescription();
				return "Invalid parameters: " + paramString;
			}
			try {
				mem = "" + Integer.parseInt(params[0], 16);
				time = "" + Integer.parseInt(params[1], 16);

				// the last part is always trailing "---"
				if (params.length > 3) {
					//sb.append(", parallel " + Integer.parseInt(params[2], 16));
					parallel = "" + Integer.parseInt(params[2], 16);
				} 
				TestLog.v(AlgoParamHandler.class, "Readable string -  KDF: " + keyDerivation + ", t=" + time + ",m=" + mem + ",p=" + parallel);
			} catch (NumberFormatException e){
				TestLog.e(AlgoParamHandler.class, e.toString() + " - " + e.getLocalizedMessage(), 5);
				return "Invalid parameters";
			}
//		}
		if (multiLine == true) {
			VMBridge bridge = PeaProperties.getVmBridge();
			return bridge.translate("key_derivation") + ": " + keyDerivation + "\n"
					+ bridge.translate("parameter")+ ": memory " + mem + ", time " + time 
					+ ((parallel.equals("")) ? "\n" : ", parallel " + parallel + "\n" )
					+ bridge.translate("cipher") + ": " + cipherAlgo + " in " + cipherMode + "\n"
					+ bridge.translate("hash") + ": " + hashAlgo + "\n\n"
					+ keyFile;
		} else {
			return keyDerivation + ", memory: " + mem + ", time; " + time 
					+ ((parallel.equals("")) ? ", " : ", Parallel parameter: " + parallel + ", " )
					+ cipherAlgo + " " + cipherMode + ", " + hashAlgo + ", " + keyFile;
		}
	}
	
	/**
	 * Get the size (= number of bytes) of the
	 * algorithm and parameter string
	 * 
	 * @return	size in number of bytes
	 */
	public static final int getAlgoParamStringSize() {
		return ALGO_PARAM_STRING_SIZE;
	}
	
	/**
	 * Get the default algorithm and parameter string
	 * from version 1
	 * 
	 * @return	old version string (version 1)
	 */
	public static final String getV1String() {
		return V1_ALGOPARAM_STRING;
	}
	
	/**
	 * Get the old algorithm and parameter string 
	 * as byte array
	 * 
	 * @return	old version string (version 1) as byte array
	 */
	public static final byte[] getV1Bytes() {
		return V1_ALGOPARAM_STRING.getBytes(Charset.forName("US-ASCII"));
	}
}
