package cologne.eck.peafactory.crypto;



/*
 * Peafactory - Production of Password Encryption Archives
 * Copyright (C) 2015  Axel von dem Bruch
 * 
 * This library is free software; you can redistribute it and/or modify it 
 * under the terms of the GNU General Public License as published 
 * by the Free Software Foundation; either version 2 of the License, 
 * or (at your option) any later version.
 * This library is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
 * or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU General Public License for more details.
 * See:  http://www.gnu.org/licenses/gpl-2.0.html
 * You should have received a copy of the GNU General Public License 
 * along with this library.
 */



/**
 * Interface for mode of operation of authenticated encryption/decryption
 */


public interface AuthenticatedEncryption {
	
	/**
	 * Encrypt/decrypt an array of bytes
	 * 
	 * @param forEncryption - true for encryption, false for decryption
	 * @param input			- plain text or cipher text
	 * @param key			- the cryptographic key for the cipher
	 * @param nonce			- unique nonce of 16 byte
	 * 
	 * @return				- plain text or cipher text
	 */
	public byte[] processBytes(	
			boolean forEncryption, 
			byte[] input, 
			byte[] key, 
			byte[] nonce);
	
	/**
	 * Encrypt an array of bytes but without MAC (MAC size 0)
	 * - this is used to check the password identifier for each file before 
	 * the decryption of the file starts.
	 * 
	 * @param input			- plain text or cipher text
	 * @param key			- the cryptographic key for the cipher
	 * @param nonce			- unique Nonce of 16 byte
	 * @return				- plain text or cipher text
	 */
	public byte[] encryptBytesNoMAC(	
			byte[] input, 
			byte[] key, 
			byte[] nonce);
	
	/**
	 * Decrypt an array of files
	 * 
	 * @param fileNames 				array of file names
	 * @param keyMaterial				derived material from KDF, contains the key
	 * @param encryptBySessionKey		whether encrypt and store the derived key
	 * 									or zeroize it
	 * @param fileComposer				in JRE: cast to FileComposer, 
	 * 									used to display the progressPro100 of encryption
	 * @return							array of error localized messages, with the same indexing as fileNames
	 */
	public String[] decryptFiles( String[] fileNames, byte[] keyMaterial, 
			boolean encryptBySessionKey, Object fileComposer);
	
	/**
	 * Encrypt an array of files
	 * 
	 * @param fileNames 			array of file names
	 * @param keyMaterial			derived material from KDF, contains the key
	 * @param encryptBySessionKey	whether encrypt and store the derived key
	 * 								or zeroize it
	 * @param fileComposer			in JRE: cast to FileComposer, 
	 * 								used to display the progressPro100 of encryption
	 * @param testNonce				null or nonce for testing
	 * @return						array of error localized messages, with the same indexing as fileNames
	 */
	public String[] encryptFiles( String[] fileNames, byte[] keyMaterial, 
			boolean encryptBySessionKey, Object fileComposer, byte[] testNonce);
	
	/**
	 * Encrypt an array of files, used by FilePEA
	 * 
	 * @param plainFileNames 		array of file names to encrypt and copy
	 * @param encryptedFileNames 	array of target file names (before processing empty, afterwards encrypted copy)
	 * @param keyMaterial			derived material from KDF, contains the key
	 * @param encryptBySessionKey	whether encrypt and store the derived key
	 * 								or zeroize it
	 * @param fileComposer			in JRE: cast to FileComposer,  
	 * 								used to display the progressPro100 of encryption
	 * @return						array of error messages, with the same indexing as fileNames
	 */
	public String[] copyEncryptFiles( String[] plainFileNames, String[] encryptedFileNames, 
			byte[] keyMaterial, boolean encryptBySessionKey, Object fileComposer);
}
