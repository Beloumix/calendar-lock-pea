package cologne.eck.peafactory.crypto;

/*
 * Peafactory - Production of Password Encryption Archives
 * Copyright (C) 2015  Axel von dem Bruch
 * 
 * This library is free software; you can redistribute it and/or modify it 
 * under the terms of the GNU General Public License as published 
 * by the Free Software Foundation; either version 2 of the License, 
 * or (at your option) any later version.
 * This library is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
 * or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU General Public License for more details.
 * See:  http://www.gnu.org/licenses/gpl-2.0.html
 * You should have received a copy of the GNU General Public License 
 * along with this library.
 */

/**
 * The EAX mode of operation: Encryption with authentication. 
 * Contains function for byte blocks and files. 
 * 
 * There is no padding, the length of the MAC can be up to the 
 * block length of the underlying cipher. 
 */

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.Arrays;

import org.bouncycastle.crypto.InvalidCipherTextException;
import org.bouncycastle.crypto.modes.EAXBlockCipher;
import org.bouncycastle.crypto.params.AEADParameters;
import org.bouncycastle.crypto.params.KeyParameter;

import cologne.eck.all_peas.bridge.VMBridge;
import cologne.eck.all_peas.data.AttachmentHandler;
import cologne.eck.all_peas.data.Attachments;
import cologne.eck.all_peas.data.PeaProperties;
import cologne.eck.peafactory.crypto.kdf.KeyDerivation;
import cologne.eck.tools.Comparator;
import cologne.eck.tools.ExceptionHelper;
import cologne.eck.tools.FileTools;
import cologne.eck.tools.TestLog;
import cologne.eck.tools.UnexpectedValueException;
import cologne.eck.tools.Zeroizer;


public final class EAXMode implements AuthenticatedEncryption {
	
	// block of file to encrypt/decrypt: Must be a multiple of any used block size64*16
	private static final int FILE_BLOCK_SIZE = 16 * 1024 * 1024;// 16 MiB 
	
	// progress verticalBar:
	private boolean showProgressBar = false; // show the verticalBar or not
											 // - depends on number of files and file size	
	private double progress = 0; // progress value to show in the progress verticalBar
	private double progressPro100 = 0; // helper value: size of 1/100 of size of all files
	private int progressHelper = 0; // helper value, to check for increments of progress
	private double blockProgress = 0; // progress of one block of FILE_BLOCK_SIZE	

	// print error message once if occurred, not for each file
	private boolean read_write_failed = false;
	private boolean file_not_found = false;
	private boolean unexpected_error = false;
	private boolean file_identifier_failed = false;
	private boolean inappropriate_file_length = false;
	private boolean closing_file_failed = false;
	private boolean salt_differs = false;
	private boolean password_failed = false;
	private String unexpectedError = "";
	
	/**
	 * Encrypt/decrypt an array of bytes. 
	 * Key and input is NOT zeroized!
	 * 
	 * @param forEncryption - true for encryption, false for decryption
	 * @param input			- plain text or cipher text
	 * @param key			- the cryptographic key for the cipher
	 * @param nonce			- unique nonce of 16 byte
	 * 
	 * @return				- plain text or cipher text
	 */
	public final byte[] processBytes(	
			boolean forEncryption, 
			byte[] input, 
			byte[] key, 
			byte[] nonce) {
		
		VMBridge bridge = PeaProperties.getVmBridge();
		if (bridge == null) {
			new UnexpectedValueException("VMBridge", "bridge", "is null").printDescription();
		}

		int resultLen = 0;// proceeded bytes
		
		if (PeaProperties.isTestMode()){
			Comparator.checkNullVector(key);
		}

		KeyParameter aeKey = new KeyParameter(key);
		
		int macLength = CipherStuff.getCipherAlgo().getBlockSize();
		if (macLength < 16) {
			 TestLog.o(EAXMode.class, "Warning: short mac size: " + macLength);
		}
		if (nonce.length != Attachments.getNonceSize()) {
			 TestLog.o(EAXMode.class, "Wrong size of nonce: " + nonce.length + ", must be: " + Attachments.getNonceSize());
			 throw new IllegalStateException("Wrong nonce size");
		}

		EAXBlockCipher eaxCipher = new EAXBlockCipher(CipherStuff.getCipherAlgo());

		AEADParameters params = new AEADParameters(aeKey, macLength * 8, nonce);
		 
		eaxCipher.init(forEncryption, params);
		 
		byte[] result = new byte[eaxCipher.getOutputSize(input.length)];
		resultLen = eaxCipher.processBytes(input, 0, input.length, result, 0);
		try {

			resultLen += eaxCipher.doFinal(result, resultLen);
			//TestLog.v(this.getClass(), "input len: " + input.length + ", resultLen: " + resultLen + ", blockSize: " + eaxCipher.getBlockSize());
			
			// KeyParameter uses a copy of the key: 
			Zeroizer.zero(aeKey.getKey());

		} catch (IllegalStateException e) {
			TestLog.e(EAXMode.class, new ExceptionHelper(e).getInfos());
			CipherStuff.setErrorMessage("internal_application_error");
			TestLog.e(EAXMode.class, "EAXMode - processBytes: " + e.toString());
			return null;	
		} catch (InvalidCipherTextException e) {
			TestLog.v(EAXMode.class, e.toString() + " - " + e.getLocalizedMessage());
			TestLog.e(EAXMode.class, e.getLocalizedMessage());
			TestLog.ve(EAXMode.class, e.toString(), 5);
			TestLog.e(EAXMode.class, "Authentication failed. ");					 
			TestLog.e(EAXMode.class, bridge.translate("authentication_error") + "\n"
				+ bridge.translate("integrity_check_failed_message"));	
			CipherStuff.setErrorMessage(bridge.translate("authentication_error") + "\n"
				+ bridge.translate("integrity_check_failed_message"));
			// show urgent message
			PeaProperties.getVmBridge().showUrgentMessage(null, bridge.translate("integrity_check_failed_message") + "\n" +
					bridge.translate("data_integrity_violated"));
			if (PeaProperties.isRescueMode() == true) {
				TestLog.o(EAXMode.class, "Rescue mode: Try to restore the content");
			} else {
				TestLog.o(EAXMode.class, "If want to try to restore the content, run this PEA "
						+ "in rescue mode: java -jar YOUR_PEA.jar -r");
				return null;
			}
		} catch (Exception e) {
			TestLog.e(EAXMode.class, new ExceptionHelper(e).getInfos());
			CipherStuff.setErrorMessage("unexpected_error");
			TestLog.e(EAXMode.class, "EAXMode processBytes: an unexpected error occured: " + e.toString());
			return null;			
		}
		return result;
	}
	
	/**
	 * Encrypt/decrypt an array of bytes but without MAC (MAC size 0)
	 * - this is used to check the password identifier for each file before 
	 * the decryption of the file starts.
	 * 
	 * @param input			- plain text or cipher text
	 * @param key			- the cryptographic key for the cipher
	 * @param nonce			- unique Nonce of 16 byte
	 * @return				- plain text or cipher text
	 */
	public final byte[] encryptBytesNoMAC(	
			byte[] input, 
			byte[] key, 
			byte[] nonce) {

		//TestLog.o(EAXMode.class, "EAXMode processBytesNoMac" + "\nfor encryption: " + forEncryption);
		//Help.printBytes("input", input);
		//Help.printBytes("key", key);
		//Help.printBytes("nonce", nonce);
		if (input == null || key == null || nonce == null) {
			new UnexpectedValueException("byte[]", "input, key, nonce", "is null").printDescription();
			CipherStuff.setErrorMessage("unexpected_error");
			TestLog.ve(EAXMode.class, "EAXMode processBytesNoMac: an unexpected error occured");
			return null;	
		}
		if (PeaProperties.isTestMode()){
			Comparator.checkNullVector(key);
		}		
		int resultLen = 0;// proceeded bytes

		KeyParameter aeKey = new KeyParameter(key);

		if (nonce.length != Attachments.getNonceSize()) {
			TestLog.ve(EAXMode.class, "Wrong size of nonce: " + nonce.length + ", must be: " + Attachments.getNonceSize());
			 throw new IllegalStateException("Wrong nonce size");
		}
		if (key.length != CipherStuff.getKeySize()) {
			// Maybe cipher was changed: Check if this is called from CryptoSetter
			boolean calledFromCryptoSetter = false;
			for (StackTraceElement ste : Thread.currentThread().getStackTrace()) {
				if (ste.toString().contains("CryptoSetter.actionPerformed")) {
					System.out.println("Stack: " + ste);
					calledFromCryptoSetter = true;
					break;
				}
			}
			if (calledFromCryptoSetter == false) {
				TestLog.ve(EAXMode.class, "Wrong size of key: " + key.length + ", must be: " + CipherStuff.getKeySize()
				+ "\nCipher: " + CipherStuff.getCipherAlgo().getAlgorithmName());
				throw new IllegalStateException("Wrong key size");

			} else {
				TestLog.v(EAXMode.class, "Cipher algorithm was changed...");
			}
		}

		EAXBlockCipher eaxCipher = new EAXBlockCipher(CipherStuff.getCipherAlgo());

		AEADParameters params = new AEADParameters(aeKey, 0, nonce);
		 
		eaxCipher.init(true, params);
		 
		byte[] result = new byte[eaxCipher.getOutputSize(input.length)];
		resultLen = eaxCipher.processBytes(input, 0, input.length, result, 0);
		try {
			 resultLen += eaxCipher.doFinal(result, resultLen);
			 // KeyParameter uses a copy of the key: 
			 Zeroizer.zero(aeKey.getKey());
		} catch (IllegalStateException e) {
			 TestLog.e(EAXMode.class, new ExceptionHelper(e).getInfos());
			 CipherStuff.setErrorMessage("internal_application_error");
			 TestLog.ve(EAXMode.class, "EAXMode - processBytesNoMAC: " + e.toString());
			 return null;	
		} catch (InvalidCipherTextException e) {
			 TestLog.e(EAXMode.class, new ExceptionHelper(e).getInfos());
			 TestLog.e(EAXMode.class, "Authentication failed. ");	
			 CipherStuff.setErrorMessage("authentication_failed");
			 return null;	
		} catch (Exception e) {
			TestLog.ve(EAXMode.class, "EAXMode processBytesNoMac: an unexpected error occured: " + e.toString());
			CipherStuff.setErrorMessage("unexpected_error");
			return null;			
		}
		return result;
	}
	
	/**
	 * Encrypt an array of files, used by FilePEA
	 * 
	 * @param fileNames 			array of file names
	 * @param keyMaterial			derived material from KDF, contains the key
	 * @param encryptBySessionKey	whether encrypt and store the derived key
	 * 								or zeroize it
	 * @param fileComposer			in JRE: cast to FileComposer, 
	 * 								used to display the progressPro100 of encryption
	 * @param testNonce				null or nonce for testing
	 * @return						array of error localized messages, with the same indexing 
	 * 								as fileNames. 
	 * 								
	 */
	public final String[] encryptFiles( String[] fileNames, byte[] keyMaterial, 
			boolean encryptBySessionKey, Object fileComposer, byte[] testNonce){
		
		// Attachments: 
		// NO PADDING (EAX mode)
		// 1. MAC size == block size
		// 2. 16 byte pswIdentifier to ciphertext (encrypted _PEA_FACTORY_ID_)
		// 3. 16 byte nonce to ciphertext
		// 4. 32 byte salt to ciphertext
		// 5. 8 byte fileIdentifier to ciphertext				

		//return value: 
		String[] errorMessages = new String[fileNames.length];			
		VMBridge bridge = PeaProperties.getVmBridge();
		if (bridge == null) {
			new UnexpectedValueException("VMBridge", "bridge", "is null").printDescription();
		}

		int fileNumber = fileNames.length;
		long allSize = 0;
		Object filePanel = null;
		if (fileComposer != null) {
			allSize = bridge.getAllSizeForProgress(fileComposer);
			filePanel = bridge.getProgressWidget(fileComposer);
		}
		
		// initialize the progress verticalBar if required: 
		progress = 0;
		setProgressValues(allSize, fileNumber);	
		
		//------------------
		// get the key: 
		//------------------
		byte[] keyBytes = CipherStuff.detectKey(keyMaterial);
		if (keyBytes == null) {
			for (int i = 0; i < errorMessages.length; i++) {
				errorMessages[i] = "Missing key";
			}
			return errorMessages;
		}
		if (PeaProperties.isTestMode()){
			Comparator.checkNullVector(keyBytes);
		}

		if (keyBytes.length != CipherStuff.getKeySize()) {
			TestLog.ve(EAXMode.class, "Wrong size of key: " + keyBytes.length + ", must be: " + CipherStuff.getKeySize()
			+ "\nCipher: " + CipherStuff.getCipherAlgo().getAlgorithmName(), 5);
			for (int i = 0; i < errorMessages.length; i++) {
				errorMessages[i] = "Wrong key size";
			}
			return errorMessages;
			// throw new IllegalStateException("Wrong key size");
		}
		KeyParameter key = new KeyParameter(keyBytes);
		
		int blockSize = CipherStuff.getCipherAlgo().getBlockSize();	
		int macSizeBytes = blockSize;// maximum MAC size for EAX
		if (macSizeBytes < 16) { // this should not happen
			TestLog.o(EAXMode.class, "Warning: short mac size: " + macSizeBytes);
		}
		int nonceSize = Attachments.getNonceSize();
		
		 EAXBlockCipher eaxCipher = new EAXBlockCipher(CipherStuff.getCipherAlgo());
		 
		 byte[] attachedSalt = null;
			 
		 if (KeyDerivation.getSalt() == null) {
			attachedSalt = new RandomStuff().createRandomBytes(KeyDerivation.getSaltSize());
			//Help.printBytes("new attachedSalt",  attachedSalt);			
			KeyDerivation.setSalt(attachedSalt);// this will set attachedSalt and update the salt		
		 } 
		 String algoParamString = AlgoParamHandler.writeString(false);
		 
		//----------------------------------
		// FILE LOOP: ======================	
		for (int fileCounter = 0; fileCounter < fileNames.length; fileCounter++) {

			RandomAccessFile f = null;
			
			File file = new File(fileNames[fileCounter]);
			if (! file.canRead() || ! file.canWrite() ){
				errorMessages[fileCounter] = bridge.translate("read_write_failed");
				TestLog.e(EAXMode.class, bridge.translate("read_write_failed") + ": " + fileNames[fileCounter]);
				read_write_failed = true;
				continue;
			}

			try {					

				if (new File( fileNames[fileCounter]).isDirectory() ) {
					continue;
				}
				f = new RandomAccessFile( fileNames[fileCounter], "rwd" );				

				long fileSizeLong = f.length();					
				
				// check overflow:
				if (fileSizeLong < 0) {
					TestLog.ve(EAXMode.class, "ERROR: this file is too large...");
					f.close();
					errorMessages[fileCounter] = bridge.translate("file_too_large");
					continue;
				}

				// check if file is too large to append attachments
				if (fileSizeLong > (Long.MAX_VALUE - 200)){ // at most 200 for block size 128...
	
					//filePanel.closeProgressTask();
					bridge.closeProgressTask(filePanel);
					// FileTypePanel will not display errorMessages...
					TestLog.ve(EAXMode.class, 
							bridge.translate("file") + ": " + fileNames[fileCounter] + "\n"
									+ bridge.translate("file_too_large") + " (1)\n"
									+ bridge.translate("file_skipped") + ".");	
	
					errorMessages[fileCounter] = bridge.translate("file_too_large");
					TestLog.e(EAXMode.class, "File is too large: \n" + fileNames[fileCounter]);

					f.close();
					continue;
				}
		
				// round down: get block number except last block
				long blockNumber = fileSizeLong / FILE_BLOCK_SIZE;
				if (fileSizeLong % FILE_BLOCK_SIZE == 0 && fileSizeLong != 0) {
					// always process a last block: 
					blockNumber--;
				}
				
				// update the progress verticalBar, depending on file size
				showFileProgress(filePanel, fileSizeLong, bridge);	
				
				if (blockNumber < 0){ // bug, error or overflow
					errorMessages[fileCounter] = "Inappropriate file size";
					TestLog.ve(EAXMode.class, "Inappropriate file size: \n" + fileNames[fileCounter]);
			
					//filePanel.closeProgressTask();
					bridge.closeProgressTask(filePanel);
					// FileTypePanel will not display errorMessages...
					TestLog.ve(EAXMode.class, 
							bridge.translate("file") + ": " + fileNames[fileCounter] + "\n"
							+ "~~ Inappropriate file size ~~~\n"
							+ bridge.translate("file_skipped") + ".");	
					// the file was not modified
					f.close();
					continue;
				}

				// prepare values for encryption: ================================
				byte[] nonce = null;
				// new nonce for each file
				if (testNonce == null) {
					nonce = new RandomStuff().createUniqueBytes(nonceSize);
				} else {
					if (PeaProperties.isTestMode() == true) {
						nonce = testNonce;
					} else {
						new UnexpectedValueException("boolean", "testMode", "is false, but statuc nonce in use").printDescription();
						PeaProperties.getVmBridge().exitWithError(null, "static nonce in use", true);;
					}
				}

				 // initialize the cipher and the parameters: 
				 eaxCipher = new EAXBlockCipher(CipherStuff.getCipherAlgo());
				 AEADParameters params = new AEADParameters(key, macSizeBytes * 8, nonce, null);//macSize in Bits!
				 eaxCipher.init(true, params);
				 
				 int processedBytes = 0;// number of processed bytes
				 byte[] block = null; // input buffer
				 byte[] out = null;	// output buffer
				 if (blockNumber > 0) {
					 out = new byte[FILE_BLOCK_SIZE];
					 block = new byte[FILE_BLOCK_SIZE];
				 }				 
				 //-----------------------------------------------------
				// BLOCK LOOP: =========================================
				 
				for (long blockCounter = 0; blockCounter < blockNumber; blockCounter++) { // only full blocks					

					// read one block from file and store in block
					f.seek(blockCounter * FILE_BLOCK_SIZE);
					f.read(block, 0, FILE_BLOCK_SIZE);					

					// we need the processedBytes value later to write at the right point of the file
					int newProcessedBytes = eaxCipher.processBytes(block, 0, FILE_BLOCK_SIZE, out, 0);
					
					// store the encrypted block in the file:
					f.seek(processedBytes);
					f.write(out, 0, FILE_BLOCK_SIZE );		
					
					// update processedBytes
					processedBytes += newProcessedBytes;					

					//bridge.setProgress(filePanel, newProcessedBytes);
					showBlockProgress(filePanel, bridge);
				} // END BLOCK LOOP

				//------------------------------------------------
				// process the last (maybe only) block: ==========
				
				// get length of last part of plaintext:
				int lastSize = (int) (fileSizeLong - (FILE_BLOCK_SIZE * blockNumber));
				// buffer for last bytes
				byte[] lastBlock = new byte[lastSize];
				// read last bytes
				f.seek(processedBytes);
				f.read(lastBlock, 0, lastSize);
				// get required size for last part of ciphertext 
				byte[] lastOut = new byte[eaxCipher.getOutputSize( lastSize)];
				
				// process the last part and get number of new processed bytes
				int lastProcessedBytes = eaxCipher.processBytes(lastBlock, 0, lastSize, lastOut, 0);			

				// finalize the encryption process: This will add the MAC 
				lastProcessedBytes += eaxCipher.doFinal(lastOut, lastProcessedBytes);
				
				if (lastProcessedBytes < eaxCipher.getBlockSize()) {
					new UnexpectedValueException("byte[]", "lastProcessedBytes", 
							lastProcessedBytes + " is shorter than block size " + eaxCipher.getBlockSize()).printDescription();
				}

				// clear the last plaintext block
				Zeroizer.zero(lastBlock);
				
				// reset the cipher
				eaxCipher.reset();
			
				// write new last bytes
				f.seek(processedBytes);
				f.write(lastOut, 0, lastOut.length );

				// create an identifier to check the password
				// this is the encrypted string _PEA_FACTORY_ID_
				if (keyBytes == null) {
					TestLog.e(EAXMode.class, "Missing key for " + fileNames[fileCounter]);
					TestLog.ve(EAXMode.class, "Missing key for " + fileNames[fileCounter], 5);
					continue;
				}
				byte[] pswIdentifierAttachment = Attachments.createPswIdentifierAttachment(keyBytes, nonce);
				

				//TestLog.v(EAXMode.class, "encryptFiles 1: \n" + new String(ReadResources.readExternFile(fileNames[fileCounter])));
				// add the password identifier and the nonce to the file:
				errorMessages[fileCounter] = addAllFileAttachments(f, 
						pswIdentifierAttachment, nonce, bridge, 2, algoParamString);// always add algoParamString
				
				if (errorMessages[fileCounter] != null) {
					try{ 
						f.close();
					} catch (Exception e) {
						if ( ! PeaProperties.isTestMode()) {
							unexpected_error = true;
							unexpectedError = e.toString() + " - " + e.getMessage();
						} else {
							TestLog.e(EAXMode.class, new ExceptionHelper(e).getInfos());
						}
					}
					continue;
				}
				f.close();
	
			} catch (FileNotFoundException e) {
				errorMessages[fileCounter] = bridge.translate("file_not_found");
				file_not_found = true;
				TestLog.ve(EAXMode.class, "EAXMode " + e.toString() + ", file: " + fileNames[fileCounter]);
				TestLog.ve(EAXMode.class, "file_probably_used_by_other_program");
				closeAfterException(fileNames[fileCounter], f, null, eaxCipher);
				continue;

			} catch (IOException e) {
				errorMessages[fileCounter] = bridge.translate("read_write_failed");
				TestLog.ve(EAXMode.class, bridge.translate("read_write_failed"));
				read_write_failed = true;
				closeAfterException(fileNames[fileCounter], f, null, eaxCipher);
				continue;
				
			} catch (Exception e) {
				errorMessages[fileCounter] = bridge.translate("unexpected_error");
				TestLog.ve(EAXMode.class, 
					unexpectedError = e.toString() + " - " + e.getMessage());
				unexpected_error = true;
				//TestLog.e(EAXMode.class, new ExceptionHelper(e).getInfos());
				
				closeAfterException(fileNames[fileCounter], f, null, eaxCipher);
				continue;
			}
		} // end for		
		
		//-----------------
		// Handle the keys:
		// encrypt or fill
		//-----------------
		CipherStuff.handleKey(key.getKey(), encryptBySessionKey);
		
		if (showProgressBar == true) {
			//filePanel.closeProgressTask();
			bridge.closeProgressTask(filePanel);
		}
		printErrors("encrypting files"); // print all occurred errors to std.err		
		return errorMessages;
	}
	
	/**
	 * Decrypt an array of files, used by FilePEA
	 * 
	 * @param fileNames 				array of file names
	 * @param keyMaterial				derived material from KDF, contains the key
	 * @param encryptKeyBySessionKey	whether encrypt and store the derived key
	 * 									or zeroize it
	 * @param fileComposer				in JRE: cast to FileComposer, 
	 * 									used to display the progressPro100 of encryption
	 * @return							array of localized error messages, with the same indexing as fileNames
	 */
	public final String[] decryptFiles( String[] fileNames, byte[] keyMaterial, 
			boolean encryptKeyBySessionKey, Object fileComposer){		
		
		int fileNamesLen = fileNames.length;		
		if (fileNamesLen == 0) {
			return null;
		}		
		VMBridge bridge = PeaProperties.getVmBridge();
		if (bridge == null) {
			new UnexpectedValueException("VMBridge", "bridge", "is null").printDescription();
		}		
		//return value: 
		String[] errorMessages = new String[fileNamesLen];

		int fileNumber = fileNames.length;
		//long allSize = fileComposer.getFileModel().getAllSize();
		long allSize = 0;
		Object filePanel = null;
		if (fileComposer != null) {
			allSize = bridge.getAllSizeForProgress(fileComposer);
			filePanel = bridge.getProgressWidget(fileComposer);
		}

		
		// initialize the progress verticalBar if required: 
		progress = 0;
		setProgressValues(allSize, fileNumber);	
		
		// Attachments: 
		// 1. cut fileIdentifier from cipher text - 8 bytes
		// 2. cut attachedSalt from cipher text - 32 bytes
		// 3. cut nonce from cipher text - 16 bytes
		// 4. cut encryptedPswIdentifier from cipher text - 16 bytes
		// 5. verify MAC at last block (block size length) 
		// NO PADDING (EAX)

		//------------------
		// get the key: 
		byte[] keyBytes = CipherStuff.detectKey(keyMaterial);
		if (PeaProperties.isTestMode()){
			Comparator.checkNullVector(keyBytes);
		}
		
		KeyParameter key = new KeyParameter(keyBytes);

		int blockSize = CipherStuff.getCipherAlgo().getBlockSize();	
		int macSize = blockSize;
		int nonceSize = Attachments.getNonceSize();
		int pswIdentifierSize = Attachments.getPswIdentifierSize();
		
		long blockNumber = 0;

		EAXBlockCipher eaxCipher = new EAXBlockCipher(CipherStuff.getCipherAlgo());

		//----------------------------------
		// decrypt the files: 
		// decrypt block by block, 
		// add short plain text and cipher text to check password
		// attach Nonce, attach fileIdentifier
		//----------------------------------
		
		int saltLen = KeyDerivation.getSaltSize();
		
		int minimum_size = macSize  // = block size of cipher + mac
				+ nonceSize // 16
				+ pswIdentifierSize // 16 
				+ saltLen // 32
		+ Attachments.getFileIdentifierSize();
			// fileIdentifier is truncated before length check...	+ Attachments.getFileIdentifierSize();

		int algoParamSize = AlgoParamHandler.getAlgoParamStringSize();
		// the algoParamString of this decryption session: Set from first file
		byte[] sessionAlgoParamBytes = null;
		String algoParamString = AlgoParamHandler.writeString(false);
		
		//--------------------------------------
		// FILE LOOP ===========================
		for (int fileCounter = 0; fileCounter < fileNamesLen; fileCounter++) {		

			TestLog.v(EAXMode.class, fileNames[fileCounter]);
			RandomAccessFile f = null;			

			File file = new File(fileNames[fileCounter]);
			if (! file.canRead() || ! file.canWrite() ){
				read_write_failed = true;
				errorMessages[fileCounter] = bridge.translate("read_write_failed");
				TestLog.ve(EAXMode.class, bridge.translate("read_write_failed"));
				continue;
			}

			// last block and processedBytes: 
			// required to continue writing for authentication error
			byte[] lastOut = null; // the last part to write
			int processedBytes = 0; // the processed bytes in decryption process
			
			byte[] nonce = null;
			
			try {		

				if (new File( fileNames[fileCounter]).isDirectory() ) {
					continue;
				}
				f = new RandomAccessFile( fileNames[fileCounter], "rwd" );
				
				// cut fileIdentifier: version 0 (key file property), 1 or 2
				int version = Attachments.checkFileIdentifier(f, false);
				if (version == -1) {
			//	if (Attachments.checkFileIdentifier(f, true) == false){ // truncates if true
					TestLog.ve(EAXMode.class, bridge.translate("unsuitable_ciphertext_id") + ": " + fileNames[fileCounter]);
					errorMessages[fileCounter] = bridge.translate("unsuitable_ciphertext_id");
					file_identifier_failed = true;
					f.close();
					continue;
				}	
								
				long fileSizeLong = f.length();	
				long originalFileSize = fileSizeLong;// needed for progressBar
				
				
				
				// we need the original file size without attachments...
				long relevantFileSize = fileSizeLong 
						- macSize 
						- nonceSize 
						- pswIdentifierSize 
						- Attachments.getFileIdentifierSize()
						- saltLen;
				if (version == 2 || version == 0) {
					relevantFileSize -= algoParamSize;
					//minimum_size += algoParamSize;
				}
				
				// get the number of blocks:
				blockNumber = (relevantFileSize / (FILE_BLOCK_SIZE));
				if ( blockNumber != 0 && relevantFileSize / (FILE_BLOCK_SIZE) == 0 ) {
					// always process the last block extra
					blockNumber--;
				}
				if (version == 1 ) {
					if (fileSizeLong < minimum_size){ 

						errorMessages[fileCounter] = bridge.translate("unsuitable_ciphertext_len");
						TestLog.ve(EAXMode.class, file.getName() +", min size: " + minimum_size + ", file size: " + fileSizeLong
								+ "\n mac-nonce-pswId-salt-algo: " + macSize + ", " + nonceSize + ", " +  pswIdentifierSize + ", " +  saltLen 
								+ Attachments.getFileIdentifierSize());
						inappropriate_file_length = true;						
						f.close();
						continue;
					}
				} else if (version == 2 || version == 0) {
					if (fileSizeLong < (minimum_size + algoParamSize) ){ 

						errorMessages[fileCounter] = bridge.translate("unsuitable_ciphertext_len");
						TestLog.ve(EAXMode.class, file.getName() +", min size: " + (minimum_size+algoParamSize) + ", file size: " + fileSizeLong
								+ "\n mac-nonce-pswId-salt-algo: " + macSize + ", " + nonceSize + ", " +  pswIdentifierSize + ", " +  saltLen 
								+ Attachments.getFileIdentifierSize() + ", " + algoParamSize);
						//TestLog.e(EAXMode.class, file.getName() +", min size: " + minimum_size + ", file size: " + fileSizeLong);					
						inappropriate_file_length = true;
						f.close();
						continue;
					}
				}
				// Cut the file Identifier:
				Attachments.checkFileIdentifier(f, true);
				// From here: continue must add fileIdentifier
				
				byte[] algoParamBytes = null;
				if (version == 2 || version == 0) { // do not truncate yet
					algoParamBytes = Attachments.getAndCutAlgoParamStringFromEnd(f, false);
				} else if (version == 1) {
					AlgoParamHandler.readString(AlgoParamHandler.getV1String(), false);
					algoParamBytes = AlgoParamHandler.getV1Bytes();
				}
				if (algoParamBytes == null) {
					TestLog.ve(EAXMode.class, file.getName() + bridge.translate("unsuitable_ciphertext_id"));
					errorMessages[fileCounter] = bridge.translate("unsuitable_ciphertext_id");
					file_identifier_failed = true;
					Attachments.addFileIdentifier(f);
					f.close();					
					continue;
				}			
				// check
				if (sessionAlgoParamBytes == null) {
					// set algos and parameters
					AlgoParamHandler.readString(new String(algoParamBytes, AttachmentHandler.getASCIICharset()), false);
					sessionAlgoParamBytes = algoParamBytes;
				} else { // compare with previous algoParamBytes:
					if ( ! Arrays.equals(algoParamBytes,  sessionAlgoParamBytes)) {
						new UnexpectedValueException("byte[]", "algoParamBytes", "differs from previos value").printDescription();
						errorMessages[fileCounter] = bridge.translate("algorithm_parameter") + " - " + bridge.translate("error");
						Attachments.addFileIdentifier(f);
						f.close();
						//progressBar:
						if (showProgressBar == true) {
							// close if last file:
							if (fileCounter == fileNames.length - 1) {
								//filePanel.closeProgressTask();
								bridge.closeProgressTask(filePanel);
							}
						}
						TestLog.ve(EAXMode.class, "sessionAlgoParam: " + new String(sessionAlgoParamBytes, AttachmentHandler.getASCIICharset())
								+ " - but algoParam is : " + new String(algoParamBytes, AttachmentHandler.getASCIICharset()));
						continue;
					}
				}					
				// Cut algoParamString
				if (version == 2 || version == 0) {
					Attachments.getAndCutAlgoParamStringFromEnd(f, true);
				} 
				// From here: continue must add fileIdentifier and algoParams if version = 2,0

				byte[] attachedSalt = Attachments.getAndCutSaltFromEnd(f, true);

				// check if the same salt is used, otherwise the decryption would fail
				if (Comparator.compare(attachedSalt, KeyDerivation.getSalt()) == false){

					errorMessages[fileCounter] = bridge.translate("salt_differs_from_first_selected_file");
					salt_differs = true;
					// restore truncated file:
					Attachments.addSalt(f, attachedSalt);//.addBytes(f, attachedSalt);
					if (version == 2 || version == 0) {
						Attachments.addAlgoParamString(f, algoParamString);
					}
					Attachments.addFileIdentifier(f);
					f.close();
					//progressBar:
					if (showProgressBar == true) {
						// close if last file:
						if (fileCounter == fileNames.length - 1) {
							//filePanel.closeProgressTask();
							bridge.closeProgressTask(filePanel);
						} 
					}
					TestLog.ve(EAXMode.class, "Different salt found: " + fileNames[fileCounter]);

					continue;
				}

				
				// get and cut random Nonce for each file
				nonce = Attachments.getAndCutNonce( f, true);	

				// Check and cut pswIdentifier: the encrypted string _PEA_FACTORY_ID_
				f.seek(f.length() - pswIdentifierSize);
				byte[] pswIdentifierAttachment = new byte[pswIdentifierSize];
				f.read(pswIdentifierAttachment);
				
				boolean check = Attachments.checkPswIdentifier(
						pswIdentifierAttachment, keyBytes, nonce);
				if (check == false) {
					errorMessages[fileCounter] = bridge.translate("password_failed");
					password_failed = true;
					TestLog.ve(EAXMode.class, "Algos, parameters, salt are OK, but password identifier failed: " + fileNames[fileCounter]);
					// Restore the file: 
					addNonceSaltAlgoFileId(f, nonce, version, algoParamString);
					f.close();
					continue;
				} else {
					// cut PswIdentifier and encryptedPswIdentifiere 
					f.setLength( f.length() - pswIdentifierSize);
				}			
				
				byte[] block = null; // buffer for one block
				byte[] out = null; // buffer for output
				if (blockNumber > 0) {
					block = new byte[FILE_BLOCK_SIZE ];
					out = new byte[FILE_BLOCK_SIZE];
				}
				// get the current file size
				fileSizeLong = f.length();
				
				// update progress verticalBar
				showFileProgress(filePanel, originalFileSize, bridge);
								
				// decrypt block-wise
				AEADParameters params = new AEADParameters(key, macSize * 8, nonce, null);//associatedText);
				eaxCipher = new EAXBlockCipher(CipherStuff.getCipherAlgo());
				eaxCipher.init(false, params);
		 
				// the number of processed bytes: 
				processedBytes = 0;

				//----------------------------------------
				// BLOCK LOOP: process the blocks ========
				for (long blockCounter = 0; blockCounter < blockNumber; blockCounter++) { // only full blocks
				
					// read one block from file and store in block
					f.seek(blockCounter * FILE_BLOCK_SIZE);
					f.read(block, 0, FILE_BLOCK_SIZE);					

					// we need the processedBytes value later to write at the right point of the file
					int newProcessedBytes = eaxCipher.processBytes(block, 0, FILE_BLOCK_SIZE, out, 0);
						
					// Not all bytes of first block are processed in decryption process!
					// the rest will be written in the next step
					
					// store the encrypted block in the file:
					f.seek(processedBytes);
					f.write(out, 0, FILE_BLOCK_SIZE );		
					
					// update processedBytes
					processedBytes += newProcessedBytes;
					
					showBlockProgress(filePanel, bridge);
				} // END BLOCK LOOP
				Zeroizer.zero(out);
				
				// ===  process the last (maybe only) block: ===
				
				// get the length of the ciphertext buffer:
				byte[] lastBlock = new byte[(int) (fileSizeLong - (FILE_BLOCK_SIZE * blockNumber) )];
				
				// read the last block and store in buffer:
				f.seek(blockNumber * FILE_BLOCK_SIZE);				
				f.read(lastBlock, 0, lastBlock.length );
				
				// get the length of the plaintext buffer (without MAC and padding)
				lastOut = new byte[eaxCipher.getOutputSize(lastBlock.length)];

				// get the number of new processedBytes, to use in doFinal
				int lastProcessedBytes = eaxCipher.processBytes(lastBlock, 0, lastBlock.length, lastOut, 0);
				// finalize the decryptioN. This may throw an InvalidCipherTextException, 
				// the last block then is written in handleAuthenticationError
				lastProcessedBytes += eaxCipher.doFinal(lastOut, lastProcessedBytes); // + macSize

				// seek at the end of the BLOCK LOOP
				// this is != (FILE_BLOCK_SIZE * blockNumber) for decryption
				f.seek(processedBytes);
				// write the last block of the plaintext
				f.write(lastOut, 0, lastOut.length );
				// truncate the file (there are some zero bytes at the end)
				f.setLength(processedBytes + lastOut.length);
				f.close();
				
								
			} catch (InvalidCipherTextException icte) {// Authentication failed
				
				errorMessages[fileCounter] = bridge.translate("data_integrity_violated");
				// this will occur after doFinal for the last block
				// the last block must be written...
				// if there is only one block: option to break
				TestLog.e(EAXMode.class, fileNames[fileCounter] + " - " + new ExceptionHelper(icte).getInfos());
				String errorMessage = handleAuthenticationError(
						f, fileNames[fileCounter], filePanel, 
						processedBytes, lastOut, nonce, keyBytes);
				//errorMessages[fileCounter] = errorMessage;
				if (errorMessage != null) {				
					errorMessages[fileCounter] = errorMessage;
				} 				
				TestLog.o(EAXMode.class, "Decryption continues with next file... ");	

				if (PeaProperties.isRescueMode() == true) {
					TestLog.o(EAXMode.class, "Rescue mode: Try to restore the content");
				} else {
					TestLog.o(EAXMode.class, "If want to try to restore the content, run this PEA "
							+ "in rescue mode: java -jar YOUR_PEA.jar -r");
					TestLog.o(EAXMode.class, "Decryption continues with next file... ");
					continue;
				}

			} catch (FileNotFoundException e) {
				errorMessages[fileCounter] = bridge.translate("file_not_found");
				file_not_found = true;
				TestLog.ve(EAXMode.class,  e.toString() + ", file: " + fileNames[fileCounter]);
				closeAfterException(fileNames[fileCounter], f, null, eaxCipher);
				continue;

			} catch (IOException e) {
				errorMessages[fileCounter] = bridge.translate("read_write_failed");
				read_write_failed = true;
				TestLog.ve(EAXMode.class,  e.toString() + ", file: " + fileNames[fileCounter]);
				closeAfterException(fileNames[fileCounter], f, null, eaxCipher);
				continue;
				
			} catch (Exception e) {
				errorMessages[fileCounter] = bridge.translate("unexpected_error");
				if ( ! PeaProperties.isTestMode()) {
					unexpectedError = e.toString() + " - " + e.getMessage();
					unexpected_error = true;
				} else {
					TestLog.e(EAXMode.class, new ExceptionHelper(e).getInfos());				
				}
				closeAfterException(fileNames[fileCounter], f, null, eaxCipher);
				continue;			
			}
			if (eaxCipher != null) {
				try {
					eaxCipher.reset();
				} catch (Exception e1) {
					if ( ! PeaProperties.isTestMode()) {
						unexpected_error = true;
						unexpectedError = e1.toString() + " - " + e1.getMessage();
					} else {
						TestLog.e(EAXMode.class, e1.toString() + " - " + e1.getMessage() + ": " + fileNames[fileCounter]);
						e1.printStackTrace();				
					}
				}
			}
		} // end for files

		//---------------
		// Handle keys:
		// encrypt or fill
		//----------------
		CipherStuff.handleKey(key.getKey(), encryptKeyBySessionKey);
		
		if (showProgressBar == true) {
			//filePanel.closeProgressTask();
			bridge.closeProgressTask(filePanel);
		}
		printErrors("decrypting files"); // print all occurred errors to std.err

		return errorMessages;
	}
	
	//================= Helper methods=====================
	
	// handles InvalidCipherTextException in decryption process (MAC failed):
	// The last part of the file is not written, it must be written here
	private String handleAuthenticationError( RandomAccessFile f,
			String fileName, Object ftp, 
			int processedBytes, byte[] lastOut, 
			byte[] originalNonce, byte[] key) {
		
		String errorMessage = null;
		
		VMBridge bridge = PeaProperties.getVmBridge();
		if (bridge == null) {
			new UnexpectedValueException("VMBridge", "bridge", "is null").printDescription();
		}
		
		// close the progressBar, otherwise the program stops
		if (showProgressBar == true) {
			//ftp.closeProgressTask();
			bridge.closeProgressTask(ftp);
		}
	
		TestLog.ve(EAXMode.class,"!!!!!!!!!!!!!!!!!  Data integrity violated  !!!!!!!!!!!!!!!!!!!");	
		TestLog.ve(EAXMode.class,fileName + ", processed bytes: " + processedBytes);
		TestLog.ve(EAXMode.class,"Authentication failed!\n");
		TestLog.ve(EAXMode.class,"The file " + fileName + " was modified after encryption (processed bytes: " + processedBytes + ").");

		if (bridge != null) {
			PeaProperties.getVmBridge().showUrgentMessage(null, bridge.translate("integrity_check_failed_message") + ": " + fileName + "\n" +
					bridge.translate("data_integrity_violated"));
		}

		if(PeaProperties.isRescueMode()){ // rescue mode: 			
			TestLog.o(EAXMode.class, "Rescue mode: Integrity checks are ignored...");
		} else {			
			TestLog.ve(EAXMode.class, 
					bridge.translate("file") + ": " + fileName + "\n"
							+ bridge.translate("integrity_check_failed_message") + "\n" +
							bridge.translate("data_integrity_violated")); //OK_CANCEL_OPTION, ERROR_MESSAGE
			errorMessage = bridge.translate("data_integrity_violated");

			// progressBar stops the program: stop automatically and initialize with progress value
			if (showProgressBar == true) {
				//ftp.startProgressTask();
				bridge.startProgressTask(ftp);
				//ftp.setProgressValue((int)progress);
				bridge.setProgress(ftp, progress);
			}
			// Write the last part of the file: 
			// seek at the end of the BLOCK LOOP
			TestLog.o(EAXMode.class, "try to write the invalid last block...");
			try {
				f.seek(processedBytes);
				// write the last block of the plaintext
				f.write(lastOut, 0, lastOut.length );
				// truncate the file (there are some zero bytes at the end)
				f.setLength(processedBytes + lastOut.length);
				f.close();
				TestLog.ve(EAXMode.class, "last block written");
				TestLog.ve(EAXMode.class, "Warning: Although the file is still in modified state, \n" 
						+ "this error message will not occur again");
			} catch (IOException e) {
				TestLog.o(EAXMode.class, "Writing of last block failed: " + e.toString());
				//			errorMessage = "### writing of last block failed";
			} catch (Exception e1) {
				if ( ! PeaProperties.isTestMode()) {
					unexpectedError = e1.toString() + " - " + e1.getMessage();
					unexpected_error = true;
				} else {
					TestLog.e(EAXMode.class, e1.toString() + " - " + e1.getMessage());
					e1.printStackTrace();				
				}
			}
		}
		return errorMessage; // always null except for break decryption option
	}
		
	private void setProgressValues(long allSize, int fileNumber) {
		
		progress = 0;
		progressHelper = 0;
		blockProgress = 0;
		
		//TestLog.o(EAXMode.class, "allSize: " + allSize + "  FILE_BLOCK_SIZE: " + FILE_BLOCK_SIZE + "  fileNumber: " + fileNumber);		
		
		// Set the progressPro100 verticalBar for many or large files:
		if (fileNumber > 64 || allSize > FILE_BLOCK_SIZE){
			showProgressBar = true;
		} else {
			showProgressBar = false;
			return;
		}
		
		if (showProgressBar == true) {			
			progressPro100 =  (100.0 / allSize);	
			blockProgress = FILE_BLOCK_SIZE * progressPro100;						
		} else {
			progressPro100 = 0;
		}
		//TestLog.o(EAXMode.class, "progressPro100: " + progressPro100 + ", blockProgress: " + blockProgress);
	}	
	
	// show the progress for one file
	private void showFileProgress(Object filePanel, long fileSizeLong, VMBridge bridge) {
		// show the progress:
		if (showProgressBar == true) {
								
			double partOfProgress = fileSizeLong * progressPro100;
			
			if (partOfProgress > 1) {
				if (fileSizeLong < FILE_BLOCK_SIZE) {
					progress += partOfProgress;
					bridge.setProgress(filePanel, progress);
				} else {
					// show only last block progress:
					// block-wise progress is shown in loop
					progress += (fileSizeLong % FILE_BLOCK_SIZE) * progressPro100;
					bridge.setProgress(filePanel, progress);
				}
			} else {
				progress += partOfProgress;
				if (progress - progressHelper > 1) {
					bridge.setProgress(filePanel, progress);
					progressHelper = (int) progress;
				}
			}
		}
	}
	
	private void showBlockProgress(Object filePanel, VMBridge bridge){
		if (showProgressBar == true) {						
			
			progress += blockProgress;
			if (blockProgress > 1) {
				bridge.setProgress(filePanel, progress);				
			} else {

				if (progress - progressHelper > 5) { // show all steps of 5
					bridge.setProgress(filePanel, progress);
					progressHelper = (int) progress;
				}
			}
		}
	}	

	/**
	 * Attaches pswIdentifier, nonce, salt, 
	 * (alogParam), file identifier to the file. 
	 * 
	 * @param f							the file to be attached
	 * @param pswIdentifierAttachment	password identifier
	 * @param originalNonce				original nonce
	 * @param bridge					VMBridge
	 * @param schemeVersion				the scheme version as int (0, 1,2)
	 * 
	 * @return	null for success, an error message otherwise
	 */
	private String addAllFileAttachments(
			RandomAccessFile f, 
			byte[] pswIdentifierAttachment, 
			byte[] originalNonce, VMBridge bridge, int schemeVersion, 
			String algoParamString){
		String errorMessage = null;

		try {	
			// add pswIdentifier to ciphertext file
			// to check the password before decryption later
			f.seek( f.length() ); // set file pointer to end
			f.write(pswIdentifierAttachment, 0, pswIdentifierAttachment.length);
			f.seek(f.length() );			
			//TestLog.v(this.getClass().getSimpleName(), "File size: " + f.length() + "\n" +
			//		"pswId: " + Converter.bytes2hex(pswIdentifierAttachment) );					
			
			errorMessage = addNonceSaltAlgoFileId(f, originalNonce, schemeVersion, algoParamString);						

		} catch (IOException ioe) {
			TestLog.ve(EAXMode.class, "EAXMode addAllFileAttachmets failed..." + ioe.toString());
			read_write_failed = true;
			errorMessage = bridge.translate("read_write_failed");
			try {
				f.close();
			} catch (IOException e1) {		
				closing_file_failed = true;
				read_write_failed = true;
				errorMessage += " - " + e1.toString();
			}
		} catch (Exception e) {
			if ( ! PeaProperties.isTestMode()) {
				TestLog.ve(EAXMode.class, "EAXMode addAllFileAttachmets failed..." + e.toString());
				unexpected_error = true;
				unexpectedError = e.toString() + " - " + e.getMessage();
			} else {
				TestLog.e(EAXMode.class, new ExceptionHelper(e).getInfos());			
			}
			try {
				f.close();
			} catch (IOException e1) {
				if ( ! PeaProperties.isTestMode()) {
					unexpected_error = true;
					unexpectedError = e1.toString() + " - " + e1.getMessage();
				} else {
					TestLog.ve(EAXMode.class, e1.toString() + " - " + e1.getMessage());
					e1.printStackTrace();				
				}
			}
		}		
		return errorMessage;
	}
	
	// add: 1. nonce, 2. salt, 3, fileIdentifier
	/**
	 * 
	 * 
	 * Attaches nonce, salt, (alogParam), 
	 * file identifier to the file, but not the
	 * password identifier
	 * 
	 * @param f							the file to be attached
	 * @param originalNonce				original nonce
	 * @param peaVersion				the version of the PEA: 0 (key file prop), 1 (old version), 2 (new version)
	 * 
	 * @return	null for success, an error message otherwise	 
	 * @throws IOException
	 * @throws Exception
	 */
	private String addNonceSaltAlgoFileId(
			RandomAccessFile f, 
			byte[] originalNonce, int schemeVersion, String algoParamString)  {
		
		String errorMessage = null;
		try {
			//TestLog.v(this.getClass().getSimpleName(), "File size: " + f.length());
			
		f.seek(f.length() );
		// add nonce to ciphertext file
		Attachments.addNonce(f, originalNonce);

		f.seek(f.length() );

		if (KeyDerivation.getSalt() == null) {
			new UnexpectedValueException("byte[]", "salt", "is null").printDescription();
			errorMessage = PeaProperties.getVmBridge().translate("unexpected_error");
		}

		Attachments.addSalt(f, KeyDerivation.getSalt());
		f.seek(f.length() );
		//TestLog.o(EAXMode.class, "AddNonce..." + AlgoParamHandler.writeString());
		if (schemeVersion == 2 || schemeVersion == 0) {
			Attachments.addAlgoParamString(f, algoParamString);
			f.seek(f.length() );
		}
		
		// add fileIdetifier to check later if this file was encrypted with this archive
		Attachments.addFileIdentifier(f);	
		f.seek(f.length() );
		//TestLog.v(this.getClass().getSimpleName(), "File size: " + f.length() + "\n" +
		//"Nonce: " + Converter.bytes2hex(originalNonce) + "\n" + 
		//"Salt: " + Converter.bytes2hex(KeyDerivation.getSalt()) + "\n");
		} catch (IOException ioe) {
			TestLog.ve(EAXMode.class, "EAXMode addNonceSaltAlgoFileID failed..." + ioe.toString());
			read_write_failed = true;
			errorMessage = PeaProperties.getVmBridge().translate("read_write_failed");
			try {
				f.close();
			} catch (IOException e1) {		
				closing_file_failed = true;
				read_write_failed = true;
				errorMessage += " - " + e1.toString();
			}
		} catch (Exception e) {
			if ( ! PeaProperties.isTestMode()) {
				TestLog.ve(EAXMode.class, "EAXMode addNonceSaltAlgoFileID failed..." + e.toString());
				unexpected_error = true;
				unexpectedError = e.toString() + " - " + e.getMessage();
			} else {
				TestLog.e(EAXMode.class, new ExceptionHelper(e).getInfos());			
			}
			try {
				f.close();
			} catch (IOException e1) {
				if ( ! PeaProperties.isTestMode()) {
					unexpected_error = true;
					unexpectedError = e1.toString() + " - " + e1.getMessage();
				} else {
					TestLog.e(EAXMode.class, e1.toString() + " - " + e1.getMessage());
					e1.printStackTrace();				
				}
			}
		}				
		return errorMessage;
	}
	
	/**
	 * Close RandomAccessFiles and reset the cipher
	 *
	 * @param fileName	the processed file name
	 * @param f			the origin file name
	 * @param f2		the encrypted copy of the file or null
	 * @param eaxCipher	the cipher to reset
	 */
	private void closeAfterException(String fileName, 
			RandomAccessFile f, RandomAccessFile f2,
			EAXBlockCipher eaxCipher) {
		
		if (f != null || f2 != null) {

			try {
				// maybe the exception occurred after opening:
				if (f != null) {
					f.close();
				}
				if (f2 != null) {
					f2.close();
				}
			} catch (IOException e) {
				closing_file_failed = true;
				// reset the cipher
				if (eaxCipher != null) {
					try {
						eaxCipher.reset();
					} catch (Exception e1) {
						if ( ! PeaProperties.isTestMode()) {
							unexpected_error = true;
							unexpectedError = e1.toString() + " - " + e1.getMessage();
						} else {
							TestLog.ve(EAXMode.class, e1.toString() + " - " + e1.getMessage());
							//e1.printStackTrace();				
						}
					}
				}
				return;		
			} catch (NullPointerException npe) {
				
				if ( ! PeaProperties.isTestMode()) {
					closing_file_failed = true;
					unexpected_error = true;
					unexpectedError = "File is probably used by another program";
				} else {
					TestLog.ve(EAXMode.class, npe.toString() + " - " + npe.getMessage());
					//npe.printStackTrace();				
				}
				
				if (eaxCipher != null) {
					try {
						eaxCipher.reset();
					} catch (Exception e1) {
						if ( ! PeaProperties.isTestMode()) {
							unexpected_error = true;
							unexpectedError = e1.toString() + " - " + e1.getMessage();
						} else {
							TestLog.e(EAXMode.class, e1.toString() + " - " + e1.getMessage());
							e1.printStackTrace();	
						}
					}
				}
				return;
			} catch (Exception e) {
				if ( ! PeaProperties.isTestMode()) {
					unexpectedError = e.toString() + " - " + e.getMessage();
					unexpected_error = true;
					closing_file_failed = true;
				} else {
					TestLog.e(EAXMode.class, new ExceptionHelper(e).getInfos());	
				}
				if (eaxCipher != null) {
					try {
						eaxCipher.reset();
					} catch (Exception e1) {
						if ( ! PeaProperties.isTestMode()) {
							unexpectedError = e1.toString() + " - " + e1.getMessage();
							unexpected_error = true;
							closing_file_failed = true;
						} else {
							TestLog.e(EAXMode.class, e1.toString() + " - " + e1.getMessage());
							//e1.printStackTrace();	
						}
					}
				}
				return;
			}
		}
		
		if (eaxCipher != null) {
			try {
				eaxCipher.reset();
			} catch (Exception e) {
				if ( ! PeaProperties.isTestMode()) {
					unexpectedError = e.toString() + " - " + e.getMessage();
					unexpected_error = true;
				} else {
					TestLog.e(EAXMode.class, new ExceptionHelper(e).getInfos());	
				}
			}
		}
	}
	
	/*
	 * Collect errors. Avoid printing errors for each file
	 */
	private void printErrors(String processName){
		
		if (	read_write_failed == true || 
				unexpected_error == true || 
				file_identifier_failed == true || 
				inappropriate_file_length == true || 
				closing_file_failed == true ||
				salt_differs == true ||
				password_failed == true ||
				file_not_found == true) {
		
			StringBuilder sb = new StringBuilder();

			if (read_write_failed == true){
				sb.append("Access denied, can not read or write files.\n");
			}
			if (file_not_found == true){
				sb.append("A file was not found. This file is probably used by another program");
			}			
			if (unexpected_error == true){
				sb.append("An unexpected error occured:\n   " + unexpectedError);
			}
			if (file_identifier_failed == true){
				sb.append("File were not encrypted with this application.\n");
			}
			if (inappropriate_file_length == true){
				sb.append("Files were too short to be encrypted with this application.\n");
			}
			if (closing_file_failed == true){
				sb.append("Closing  files failed.\n");
			}
			if (salt_differs == true){
				sb.append("Different salt occurred compared to salt of first file.\n");
			}
			if (password_failed == true){
				sb.append("Password failed for selected file.\n");
			}
			// print the errors:
			TestLog.ve(EAXMode.class, "The following errors occurred during the process (" + processName + "): " );
			TestLog.ve(EAXMode.class, new String(sb));		
			TestLog.e(getClass(),  "The following errors occurred during the process (" + processName + "): " + new String(sb));
			// reset values:
			read_write_failed = false;
			file_not_found = false;
			unexpected_error = false;
			file_identifier_failed = false;
			inappropriate_file_length = false;
			closing_file_failed = false;
			salt_differs = false;
			password_failed = false;
			unexpectedError = "";
		}
	}	
	
	/**
	 * Encrypt an array of files, used by FilePEA. 
	 * The original file remains in plain text.
	 * 
	 * @param plainFileNames 		array of file names to encrypt and copy
	 * @param encryptedFileNames 	array of target file names (before processing empty, afterwards encrypted copy)
	 * @param keyMaterial			derived material from KDF, contains the key
	 * @param encryptBySessionKey	whether encrypt and store the derived key
	 * 								or zeroize it
	 * @param fileComposer			in JRE: cast to FileComposer, 
	 * 								used to display the progressPro100 of encryption
	 * @return						array of error messages, with the same indexing as fileNames
	 * 								with localized informations
	 */
	public final String[] copyEncryptFiles( String[] plainFileNames, String[] encryptedFileNames, 
			byte[] keyMaterial, boolean encryptBySessionKey, Object fileComposer){
		
		// Attachments: 
		// NO PADDING (EAX mode)
		// 1. MAC size == block size
		// 2. 16 byte pswIdentifier to ciphertext (encrypted _PEA_FACTORY_ID_)
		// 3. 16 byte nonce to ciphertext
		// 4. 32 byte salt to ciphertext
		// 5. 8 byte fileIdentifier to ciphertext				

		//return value: 
		String[] errorMessages = new String[plainFileNames.length];				

		VMBridge bridge = PeaProperties.getVmBridge();
		if (bridge == null) {
			new UnexpectedValueException("VMBridge", "bridge", "is null").printDescription();
		}
		int fileNumber = plainFileNames.length;
//		long allSize = fileComposer.getFileModel().getAllSize();
		long allSize = FileTools.getNumberAndSize(plainFileNames)[1];

		Object filePanel = null;
		if (fileComposer != null) {		
			filePanel = bridge.getProgressWidget(fileComposer);
		} else {
			//new UnexpectedValueException("FileComposer", "fileCompoiser", "is null").printDescription();
		}
		
		// initialize the progress verticalBar if required: 
		progress = 0;
		setProgressValues(allSize, fileNumber);	
		
		//------------------
		// get the key: 
		//------------------
		byte[] keyBytes = CipherStuff.detectKey(keyMaterial);
		if (PeaProperties.isTestMode()){
			Comparator.checkNullVector(keyBytes);
		}
		KeyParameter key = new KeyParameter(keyBytes);
		
		int blockSize = CipherStuff.getCipherAlgo().getBlockSize();	
		int macSizeBytes = blockSize;// maximum MAC size for EAX
		if (macSizeBytes < 16) { // this should not happen
			TestLog.o(EAXMode.class, "Warning: short mac size: " + macSizeBytes);
		}
		int nonceSize = Attachments.getNonceSize();
		
		 EAXBlockCipher eaxCipher = new EAXBlockCipher(CipherStuff.getCipherAlgo());
		 
		 byte[] attachedSalt = null;
			 
		 if (KeyDerivation.getSalt() == null) {

			attachedSalt = new RandomStuff().createRandomBytes(KeyDerivation.getSaltSize());
			//Help.printBytes("new attachedSalt",  attachedSalt);			
			KeyDerivation.setSalt(attachedSalt);// this will set attachedSalt and update the salt		
		 } 
		 String algoParamString = AlgoParamHandler.writeString(false);
		
		//----------------------------------
		// FILE LOOP: ======================	
		for (int fileCounter = 0; fileCounter < plainFileNames.length; fileCounter++) {
			//TestLog.o(EAXMode.class, "copy encrypt" + plainFileNames[fileCounter]);

			RandomAccessFile pF = null; // plain text file (origin)
			RandomAccessFile eF = null; // encrypted file (copy)
			
			File plainFile = new File(plainFileNames[fileCounter]);
			if (! plainFile.canRead() || ! plainFile.canWrite() ){
				errorMessages[fileCounter] = bridge.translate("read_write_failed");
				read_write_failed = true;
				TestLog.e(EAXMode.class, new ExceptionHelper(null).getInfos());
				continue;
			}
			
			File encryptedFile =  FileTools.createEmptyFile(encryptedFileNames[fileCounter]);
			if (encryptedFile == null) {
				errorMessages[fileCounter] = bridge.translate("read_write_failed");
				TestLog.e(EAXMode.class, new ExceptionHelper(null).getInfos());
				read_write_failed = true;
				TestLog.e(EAXMode.class, new ExceptionHelper(null).getInfos());
				continue;	
			}
			try {					
				if (new File( plainFileNames[fileCounter]).isDirectory() ) {
					continue;
				}
				pF = new RandomAccessFile( plainFileNames[fileCounter], "r" );			
				eF = new RandomAccessFile( encryptedFileNames[fileCounter], "rwd" );	

				long fileSizeLong = pF.length();					
				
				// check overflow:
				if (fileSizeLong < 0) {
					TestLog.o(EAXMode.class, "ERROR: this file is too large...");
					pF.close();
					eF.close();
					errorMessages[fileCounter] = bridge.translate("file_too_large");
					TestLog.e(EAXMode.class, new ExceptionHelper(null).getInfos());
					continue;
				}

				// check if file is too large to append attachments
				if (fileSizeLong > (Long.MAX_VALUE - 200)){ // at most 200 for block size 128...
	
					if (filePanel != null) {
						//filePanel.closeProgressTask();
						bridge.closeProgressTask(filePanel);
					}
					// FileTypePanel will not display errorMessages...
					TestLog.e(EAXMode.class, 
							bridge.translate("file") + ": " + plainFileNames[fileCounter] + "\n"
									+ bridge.translate("file_too_large") + " (1)\n"
									+ bridge.translate("file_skipped") + ".");	
	
					errorMessages[fileCounter] = bridge.translate("file_too_large");
					TestLog.e(EAXMode.class, "File is too large: \n" + plainFileNames[fileCounter]);
					TestLog.e(EAXMode.class, new ExceptionHelper(null).getInfos());
					pF.close();
					eF.close();
					continue;
				}
		
				// round down: get block number except last block
				long blockNumber = fileSizeLong / FILE_BLOCK_SIZE;
				if (fileSizeLong % FILE_BLOCK_SIZE == 0 && fileSizeLong != 0) {
					// always process a last block: 
					blockNumber--;
				}
				
				// update the progress verticalBar, depending on file size
				if (filePanel != null) {
					showFileProgress(filePanel, fileSizeLong, bridge);	
				}
				
				if (blockNumber < 0){ // bug, error or overflow
					errorMessages[fileCounter] = "Inappropriate file size";
					TestLog.ve(EAXMode.class, "Inappropriate file size: \n" + plainFileNames[fileCounter]);
					TestLog.e(EAXMode.class, new ExceptionHelper(null).getInfos());
					if (filePanel != null) {
						//filePanel.closeProgressTask();
						bridge.closeProgressTask(filePanel);
					}
					// FileTypePanel will not display errorMessages...
					TestLog.e(EAXMode.class, 
							bridge.translate("file") + ": " + plainFileNames[fileCounter] + "\n"
							+ "~~ Inappropriate file size ~~~\n"
							+ bridge.translate("file_skipped") + ".");	
					// the file was not modified
					pF.close();
					eF.close();
					continue;
				}

				// prepare values for encryption: ================================
				// new nonce for each file
				byte[] nonce = new RandomStuff().createUniqueBytes(nonceSize);

				 // initialize the cipher and the parameters: 
				 eaxCipher = new EAXBlockCipher(CipherStuff.getCipherAlgo());
				 AEADParameters params = new AEADParameters(key, macSizeBytes * 8, nonce, null);//macSize in Bits!
				 eaxCipher.init(true, params);
				 
				 int processedBytes = 0;// number of processed bytes
				 byte[] block = null; // input buffer
				 byte[] out = null;	// output buffer
				 if (blockNumber > 0) {
					 out = new byte[FILE_BLOCK_SIZE];
					 block = new byte[FILE_BLOCK_SIZE];
				 }				 
				 //-----------------------------------------------------
				// BLOCK LOOP: =========================================
				 
				for (long blockCounter = 0; blockCounter < blockNumber; blockCounter++) { // only full blocks					

					// read one block from file and store in block
					pF.seek(blockCounter * FILE_BLOCK_SIZE);
					pF.read(block, 0, FILE_BLOCK_SIZE);					

					// we need the processedBytes value later to write at the right point of the file
					int newProcessedBytes = eaxCipher.processBytes(block, 0, FILE_BLOCK_SIZE, out, 0);
					
					// store the encrypted block in the file:
					eF.seek(processedBytes);
					eF.write(out, 0, FILE_BLOCK_SIZE );		
					
					// update processedBytes
					processedBytes += newProcessedBytes;					

					if (filePanel != null) {
						showBlockProgress(filePanel, bridge);
					}
				} // END BLOCK LOOP

				//------------------------------------------------
				// process the last (maybe only) block: ==========
				
				// get length of last part of plaintext:
				int lastSize = (int) (fileSizeLong - (FILE_BLOCK_SIZE * blockNumber));
				// buffer for last bytes
				byte[] lastBlock = new byte[lastSize];
				// read last bytes
				pF.seek(processedBytes);
				pF.read(lastBlock, 0, lastSize);
				// get required size for last part of ciphertext 
				byte[] lastOut = new byte[eaxCipher.getOutputSize( lastSize)];
				
				// process the last part and get number of new processed bytes
				int lastProcessedBytes = eaxCipher.processBytes(lastBlock, 0, lastSize, lastOut, 0);				
				// finalize the encryption process: This will add the MAC 
				lastProcessedBytes += eaxCipher.doFinal(lastOut, lastProcessedBytes);

				// clear the last plaintext block
				Zeroizer.zero(lastBlock);
				
				// reset the cipher
				eaxCipher.reset();
			
				// write new last bytes
				eF.seek(processedBytes);

				eF.write(lastOut, 0, lastProcessedBytes);
				eF.setLength(processedBytes + lastProcessedBytes);
				int macSize = CipherStuff.getCipherAlgo().getBlockSize();
				if ( eF.length() % FILE_BLOCK_SIZE != lastSize + macSize) {
					new UnexpectedValueException ("long", "ef.length", "is not equal mac size: "+ encryptedFileNames[fileCounter]).printDescription();
					TestLog.e(EAXMode.class, "Last block length: " + eF.length() % FILE_BLOCK_SIZE);
					TestLog.e(EAXMode.class, "last size: " + lastSize + ", mac size: " + macSize);
				} else {
	//				TestLog.o(EAXMode.class, "OK...");
				}

				// create an identifier to check the password
				// this is the encrypted string _PEA_FACTORY_ID_
				byte[] pswIdentifierAttachment = Attachments.createPswIdentifierAttachment(keyBytes, nonce);

				// add the password identifier and the nonce to the file:
				errorMessages[fileCounter] = addAllFileAttachments(eF, 
						pswIdentifierAttachment, nonce, bridge, 2, algoParamString);
				if (errorMessages[fileCounter] != null) {
					try{ 
						pF.close();
						eF.close();
					} catch (Exception e) {
						if ( ! PeaProperties.isTestMode()) {
							unexpected_error = true;
							unexpectedError = e.toString() + " - " + e.getMessage();
						} else {
							TestLog.e(EAXMode.class, new ExceptionHelper(e).getInfos());
						}
					}
					continue;
				}
				pF.close();
				eF.close();
	
			} catch (FileNotFoundException e) {
				errorMessages[fileCounter] = bridge.translate("file_not_found");
				TestLog.e(EAXMode.class, new ExceptionHelper(e).getInfos());
				file_not_found = true;
				//TestLog.e(EAXMode.class, "EAXMode " + e.toString() + ", file: " + fileNames[fileCounter]);
				//TestLog.e(EAXMode.class, "file_probably_used_by_other_program");
				closeAfterException(plainFileNames[fileCounter], pF, eF, eaxCipher);
				continue;

			} catch (IOException e) {
				errorMessages[fileCounter] = bridge.translate("read_write_failed");
				TestLog.e(EAXMode.class, new ExceptionHelper(e).getInfos());
				read_write_failed = true;
				closeAfterException(plainFileNames[fileCounter], pF, eF, eaxCipher);
				continue;
				
			} catch (Exception e) {
				errorMessages[fileCounter] = bridge.translate("unexpected_error");
				if ( ! PeaProperties.isTestMode()) {
					TestLog.e(EAXMode.class, new ExceptionHelper(e).getInfos());
					unexpectedError = e.toString() + " - " + e.getLocalizedMessage();
					unexpected_error = true;
				} else {
					TestLog.e(EAXMode.class, new ExceptionHelper(e).getInfos());
				}
				closeAfterException(plainFileNames[fileCounter], pF, eF, eaxCipher);
				continue;
			}
			//TestLog.o(EAXMode.class, "files encrypted and copied...");
		} // end for		
		
		//-----------------
		// Handle the keys:
		// encrypt or fill
		//-----------------

		CipherStuff.handleKey(key.getKey(), encryptBySessionKey);
		
		if (showProgressBar == true) {
			if (filePanel != null) {
				//filePanel.closeProgressTask();
				bridge.closeProgressTask(filePanel);
			}
		}
		printErrors("encrypting files"); // print all occurred errors to std.err
		
		return errorMessages;
	}
}
