package cologne.eck.all_peas.bridge;

/*
 * Peafactory - Production of Password Encryption Archives
 * Copyright (C) 2020  Axel von dem Bruch
 * 
 * This library is free software; you can redistribute it and/or modify it 
 * under the terms of the GNU General Public License as published 
 * by the Free Software Foundation; either version 2 of the License, 
 * or (at your option) any later version.
 * This library is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
 * or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU General Public License for more details.
 * See:  http://www.gnu.org/licenses/gpl-2.0.html
 * You should have received a copy of the GNU General Public License 
 * along with this library.
 */

import java.io.File;
import java.net.MalformedURLException;
import java.time.LocalDateTime;

import cologne.eck.all_peas.control.Translator;

/**
 * Interface for methods that are used differently in different virtual machines (JRE, ART)
 */

public interface VMBridge {
	
	/**
	 * Size of personal string as additional string
	 * for generation of random values
	 * (IP address, system values)
	 */
	public int PERSONAL_BYTES_SIZE = 16;
	
	/**
	 * Translate a String
	 * 
	 * @param key	the String to translate
	 * 
	 * @return	the translated String
	 */
	public String translate(String key);
	
	/**
	 * Show an urgent message
	 * 
	 * @param owner		owner window or null
	 * @param message	the localized message
	 */
	public void showUrgentMessage(Object owner, String message);
	
	/**
	 * Exit the program with an error: 
	 * Show an error message and probably unencrypted
	 * left files and then exit the program
	 * 
	 * @param ownerComponent		owner to show dialog
	 * @param message				message to show
	 * @param unencryptedFilesLeft	true if there are unencrypted files
	 */
	public void exitWithError(Object ownerComponent, String message, boolean unencryptedFilesLeft);
	
	////////////////////////////////////////////////////////////////////////
	///   Used in EAXMode   ////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////
	/**
	 * Get the progress displaying widget
	 * from an object (JRE: FileTypePanel from FileComposer)
	 * 
	 * @param progressObject	the Object with access to the widget
	 * 
	 * @return	the widget to show the progress (e.g. progress bar)
	 */
	public Object getProgressWidget(Object progressObject);
	
	/**
	 * Get the overall size of files to decrypt/encrypt
	 * 
	 * @param progressObject	the object, that holds the files
	 * 
	 * @return	the overall size in bytes
	 */
	public long getAllSizeForProgress(Object progressObject);
	
	/** 
	 * Start the progress task: Show the widget
	 * 
	 * @param ftp	the widget to show the progress
	 */
	public void startProgressTask(Object ftp);
	
	/**
	 * Set the progress in a widget
	 * 
	 * @param progressWidget	the widget to show the progress	 * 
	 * @param progress	the progress as double
	 */
	public void setProgress(Object progressWidget, double progress);

	/**
	 * Close the progress showing task
	 * 
	 * @param progressWidget	the widget that shows the progress
	 */
	public void closeProgressTask(Object progressWidget);
	
	////////////////////////////////////////////////////////////////////////
	///   Used in RandomStuff   ////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////
	/**
	 * Generate 16 byte from personal properties
	 * 
	 * @return	16 byte from personal properties
	 */
	public byte[] generatePersonalRandomBytes();

	/**
	 * Get additional values from EntropyPool to
	 * generate random values and reseed the EntropyPool
	 * 
	 * @param number	number of long values
	 * 
	 * @return	an byte array of size number * 8
	 */
	public byte[] getAndUpdateEntopyPoolTableValue(int number);
	
	/**
	 * Reseed the entopy pool
	 * 
	 * @param ep	the pool to reseed as Object (cast to EntropyPool)
	 */
	public void reseedEntropyPool(Object ep);
	
	////////////////////////////////////////////////////////////////////////
	///   Used in PeaProperties   //////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////
	
	/**
	 * Set the requested language bundle
	 * 
	 * @param _language		the language e.g. "en", "de", "tr"
	 * @throws MalformedURLException
	 */
	public void setLanguagesBundle(String _language) throws MalformedURLException;
	
	
	////////////////////////////////////////////////////////////////////////
	///   Used in FileTools   //////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////	
    /**
     * Check if files already exists and ask to overwrite
     * 
     * @param fileNames	the file names to check
     * 
     * @return	the file names to write (overwriting confirmed or
     * 			not yet exist)
     */
	public String[] askToOverwriteFiles(String[] fileNames);
	
    /**
     * Check if a file already exists and ask to overwrite
     * 
     * @param file	the file to check
     * 
     * @return	true, if the file should be overwritten or
     * 			if the file does not exist, 
     * 			false if the file should not be written
     */
	public boolean askToOverwriteFile(File file);

	/**
	 * Get the LocalDateTime from a long value
	 *
	 * @param timestamp	the long value
	 *
	 * @return	the LocalDateTime
	 */
	public LocalDateTime long2LocalDateTime(long timestamp);
	
	/**
	 * Create conditions required for testing:
	 * JRE: EntropyPool
	 */
	public void createTestConditions();

	/**
	 * Get the available languages as an array of string
	 * e.g. "en", "tr", "de"
	 * 
	 * @return	the available languages as an array of strings
	 */
	public String[] getAvailableLanguages();
	
	/**
	 * Get the used Translator instance
	 * 
	 * @return	the used Translator instance
	 */
	public Translator getTranslator();
}
