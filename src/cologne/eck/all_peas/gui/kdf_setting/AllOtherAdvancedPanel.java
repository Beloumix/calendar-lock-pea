package cologne.eck.all_peas.gui.kdf_setting;

/*
 * Peafactory - Production of Password Encryption Archives
 * Copyright (C) 2022  Axel von dem Bruch
 * 
 * This library is free software; you can redistribute it and/or modify it 
 * under the terms of the GNU General Public License as published 
 * by the Free Software Foundation; either version 2 of the License, 
 * or (at your option) any later version.
 * This library is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
 * or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU General Public License for more details.
 * See:  http://www.gnu.org/licenses/gpl-2.0.html
 * You should have received a copy of the GNU General Public License 
 * along with this library.
 */

import java.awt.Dimension;
import java.awt.Font;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;

import cologne.eck.all_peas.gui.PeaBorderFactory;
import cologne.eck.peafactory.crypto.kdf.KeyDerivation;
import cologne.eck.tools.TestLog;


@SuppressWarnings("serial")
public class AllOtherAdvancedPanel  extends JPanel {

	// combo boxes for parameters:
	private JComboBox<Integer> memoryList;
	private JComboBox<Integer> timeList;
	private JComboBox<Integer> parallelList;
	
	private KeyDerivation kdf = null; 

	public AllOtherAdvancedPanel(KeyDerivation _kdf) {

		if (_kdf == null) {
			TestLog.ve(getClass(), "Missing key derivation function", 5);
			return;
		} else {
			this.kdf = _kdf;
		}
		//System.out.println("AllOtherAdvanced: " + kdf.getName());

		String kdfName = kdf.getName();
		Integer[] memoryValues = null;
		Integer[] timeValues = null;
		Integer[] parallelValues = null;
		if (kdfName != null) {
			if (kdfName.equals("Scrypt")) {

				memoryValues = new Integer[9];
				int memParam = 1;
				for (int i = 0; i < 9; i++) {
					memParam = memParam * 2;
					memoryValues[i] = memParam;
				}
				memoryList =  new JComboBox<>(memoryValues);
				memoryList.setSelectedIndex(4);					

				// iterations: 2^14 - 2^22 
				timeValues = new Integer[7];
				int timeValue = (int) Math.pow(2, 11);
				for (int i = 0; i < 7; i++) {
					timeValue = timeValue * 2;
					timeValues[i] = timeValue;
				}				
				timeList =  new JComboBox<>(timeValues);
				timeList.setSelectedIndex(2);

				// parallel: 
				parallelValues = new Integer[4];
				parallelValues[0] = 1;
				parallelValues[1] = 2;
				parallelValues[2] = 3;
				parallelValues[3] = 4;			
				parallelList =  new JComboBox<>(parallelValues);
				//parallelList.setPreferredSize(new Dimension(50, 30));
				parallelList.setSelectedIndex(0);

			} else if (kdfName.equals("Bcrypt")) {
				//String[] bcryptValues = { "4", "5", "6", "7", "8", "9", "10" , "11", "12", "13", "14" , "15", "16", "17", "18", "19", "20", "21", "22" , "23", "24", "25", "26" , "27", "28", "29", "30", "31"};
				timeValues = new Integer[27];
				int timeValue = 3;
				for (int i = 0; i < 27; i++) {
					timeValue = timeValue +1;
					timeValues[i] = timeValue;
				}				
				timeList =  new JComboBox<>(timeValues);
				timeList.setSelectedIndex(7);

			} else if (kdfName.equals("PomeloKDF")) {
				// memory cost + time cost must be > 5 and < 25
				memoryValues = new Integer[18];
				int memParam = 0;
				for (int i = 0; i < 18; i++) {						
					memoryValues[i] = memParam;
					memParam = memParam + 1;
				}
				memoryList =  new JComboBox<>(memoryValues);
				memoryList.setSelectedIndex(15);	

				timeValues = new Integer[26];
				int timeValue = 0;
				for (int i = 0; i < 26; i++) {						
					timeValues[i] = timeValue;
					timeValue = timeValue +1;
				}				
				timeList = new JComboBox<>(timeValues);
				timeList.setSelectedIndex(0);					

			} else if (kdfName.equals("Balloon")) {
				TestLog.ve(getClass(), "Balloon is not yet implemented", 5);
			} else {
				TestLog.ve(getClass(), "Invalid KDF: " + kdfName, 5);	
			}
		}			

		JPanel advancedKDFPane = new JPanel();
		advancedKDFPane.setBorder(PeaBorderFactory.createBorder(false));
		advancedKDFPane.setLayout(new BoxLayout(advancedKDFPane, BoxLayout.Y_AXIS));

		JLabel topicLabel = new JLabel("Settings for " + kdf.getName() + ":");
		topicLabel.setPreferredSize(new Dimension(250, 30));

		JLabel tipLabel1 = new JLabel("Settings for this session only");
		tipLabel1.setFont(new Font(Font.SANS_SERIF, Font.PLAIN, 11));
		tipLabel1.setPreferredSize(new Dimension(250, 20));
		advancedKDFPane.add(topicLabel);
		advancedKDFPane.add(tipLabel1);

		if (memoryList != null) {
			memoryList.setMinimumSize(new Dimension(250, 30));
			memoryList.setMaximumSize(new Dimension(250, 30));
			JLabel memoryLabel = new JLabel("Memory cost: ");
			memoryLabel.setLabelFor(memoryList);
			memoryLabel.setPreferredSize(new Dimension(250, 40));
			advancedKDFPane.add(memoryLabel);			
			//JLabel memoryRecommendedLabel = new JLabel("recommended " + kdf.getmCost());
			//memoryRecommendedLabel.setFont(new Font(Font.SANS_SERIF, Font.PLAIN, 11));
			JPanel memoryPanel = new JPanel();
			memoryPanel.add(memoryList);//memoryField);
			//memoryPanel.add(memoryRecommendedLabel);
			advancedKDFPane.add(memoryPanel);
			advancedKDFPane.add(Box.createVerticalStrut(10));
		}			

		if (timeList != null) {
			JPanel timePanel = new JPanel();
			timeList.setMinimumSize(new Dimension(250, 30));
			timeList.setMaximumSize(new Dimension(250, 30));
			JLabel timeLabel = new JLabel("Time cost: ");
			timeLabel.setLabelFor(timeList);
			timeLabel.setPreferredSize(new Dimension(250, 40));
			advancedKDFPane.add(timeLabel);

			//JLabel timeRecommendedLabel = new JLabel("recommended " + kdf.gettCost());
			//timeRecommendedLabel.setFont(new Font(Font.SANS_SERIF, Font.PLAIN, 11));
			timePanel.add(timeList);
			//timePanel.add(timeRecommendedLabel);
			advancedKDFPane.add(timePanel);
			advancedKDFPane.add(Box.createVerticalStrut(10));
		}			

		if (parallelList != null) {
			parallelList.setMinimumSize(new Dimension(250, 30));
			parallelList.setMaximumSize(new Dimension(250, 30));
			JLabel parallelLabel = new JLabel("Parallelism: ");
			parallelLabel.setLabelFor(parallelList);
			parallelLabel.setSize(new Dimension(250, 40));
			advancedKDFPane.add(parallelLabel);		
			//JLabel parallelRecommendedLabel = new JLabel("recommended " + kdf.getArg3());
			//parallelRecommendedLabel.setFont(new Font(Font.SANS_SERIF, Font.PLAIN, 11));
			JPanel parallelPanel = new JPanel();
			parallelPanel.add(parallelList);//parallelField);
			//parallelPanel.add(parallelRecommendedLabel);
			advancedKDFPane.add(parallelPanel);	
		}						
		this.add(advancedKDFPane);
	}

	/*		public static void main(String[] args) {
			// TODO Auto-generated method stub
			JFrame f = new JFrame();
			f.getContentPane().add(new AllOtherAdvancedPanel(new BcryptKDF()));
			f.pack();
			f.setVisible(true);
		} */


	/**
	 * Get the parameters: 
	 * [0] is time, [1] is memory, [2] is parallel
	 * 
	 * @return	the parameters: [0] is time, [1] is memory, [2] is parallel
	 */
	public int[] getParams() {
		int[] params = new int[3];
		if (timeList != null && timeList.getSelectedItem() instanceof Integer) {
			params[0] = (Integer) timeList.getSelectedItem();
		} else {
			params[0] = 0;
		}
		if (memoryList != null && memoryList.getSelectedItem() instanceof Integer) {
			params[1] = (Integer) memoryList.getSelectedItem();
		} else {
			params[1] = 0;
		}
		if (parallelList != null && parallelList.getSelectedItem() instanceof Integer) {
			params[2] = (Integer) parallelList.getSelectedItem();
		} else {
			params[2] = 0;
		}
/*		// Check parameters
		String kdfName = kdf.getName();
		if (kdfName.equals("Scrypt")) {
			
		} else if (kdfName.equals("Bcrypt")) {
		} else if (kdfName.equals("PomeloKDF")) {
		} else {			
		} */

		return params;
	}
}