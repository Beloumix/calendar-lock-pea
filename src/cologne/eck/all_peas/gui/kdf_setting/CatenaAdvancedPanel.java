package cologne.eck.all_peas.gui.kdf_setting;

/*
 * Peafactory - Production of Password Encryption Archives
 * Copyright (C) 2021  Axel von dem Bruch
 * 
 * This library is free software; you can redistribute it and/or modify it 
 * under the terms of the GNU General Public License as published 
 * by the Free Software Foundation; either version 2 of the License, 
 * or (at your option) any later version.
 * This library is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
 * or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU General Public License for more details.
 * See:  http://www.gnu.org/licenses/gpl-2.0.html
 * You should have received a copy of the GNU General Public License 
 * along with this library.
 */

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.KeyEvent;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.ButtonGroup;
import javax.swing.JFormattedTextField;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.text.NumberFormatter;

import cologne.eck.all_peas.gui.PeaBorderFactory;
import cologne.eck.peafactory.crypto.kdf.CatenaKDF;
import cologne.eck.tools.TestLog;

@SuppressWarnings("serial")
public class CatenaAdvancedPanel extends JPanel implements ActionListener {	
	
	private static volatile String instance = "Dragonfly-Full";

	private JFormattedTextField memoryField;
	private JFormattedTextField timeField;
	private JLabel memoryRecommendedLabel;
	private JLabel timeRecommendedLabel;
	
	private JLabel errorLabel;	
	
	public CatenaAdvancedPanel() {		

		JPanel scryptPane = new JPanel();
		scryptPane.setBorder(PeaBorderFactory.createBorder(false));
		scryptPane.setLayout(new BoxLayout(scryptPane, BoxLayout.Y_AXIS));
		
		JLabel scryptLabel = new JLabel("Settings for CATENA:");
		scryptLabel.setPreferredSize(new Dimension(250, 30));
		
		JLabel tipLabel1 = new JLabel("Settings for this session only");
		tipLabel1.setFont(new Font(Font.SANS_SERIF, Font.PLAIN, 11));
		tipLabel1.setPreferredSize(new Dimension(250, 20));
		scryptPane.add(scryptLabel);
		scryptPane.add(tipLabel1);
		
		JLabel instanceLabel = new JLabel("Select an instance of Catena:");
		instanceLabel.setPreferredSize(new Dimension(250, 40));
		scryptPane.add(instanceLabel);		
		
		JLabel instanceRecommendedLabel = new JLabel("recommended: Dragonfly-Full");
		instanceRecommendedLabel.setPreferredSize(new Dimension(250, 20));
		instanceRecommendedLabel.setFont(new Font(Font.SANS_SERIF, Font.PLAIN, 11));
		scryptPane.add(instanceRecommendedLabel);
	    
		JRadioButton dragonflyFullButton = new JRadioButton("Dragonfly-Full");
	    dragonflyFullButton.setMnemonic(KeyEvent.VK_U);
	    dragonflyFullButton.addActionListener(this);//addActionListener(this);
	    dragonflyFullButton.setActionCommand("Dragonfly-Full");
	    scryptPane.add(dragonflyFullButton);
	    
		JRadioButton butterflyFullButton = new JRadioButton("Butterfly-Full");
	    butterflyFullButton.setMnemonic(KeyEvent.VK_U);
	    butterflyFullButton.addActionListener(this);
	    butterflyFullButton.setActionCommand("Butterfly-Full");
	    scryptPane.add(butterflyFullButton);
	    
		JRadioButton dragonflyButton = new JRadioButton("Dragonfly");
	    dragonflyButton.setMnemonic(KeyEvent.VK_B);
	    dragonflyButton.addActionListener(this);
	    dragonflyButton.setActionCommand("Dragonfly");
	    scryptPane.add(dragonflyButton);
		
		JRadioButton butterflyButton = new JRadioButton("Butterfly");
	    butterflyButton.setMnemonic(KeyEvent.VK_B);
	    butterflyButton.addActionListener(this);
	    butterflyButton.setActionCommand("Butterfly");
	    scryptPane.add(butterflyButton);
	    
	    //Group the radio buttons.
	    ButtonGroup group = new ButtonGroup();
	    group.add(dragonflyButton);
	    group.add(dragonflyFullButton);
	    group.add(butterflyButton);
	    group.add(butterflyFullButton);
	    
	    group.setSelected(dragonflyFullButton.getModel(), true);
	    
		scryptPane.add(Box.createVerticalStrut(20));

		JLabel memoryLabel = new JLabel("Memory cost parameter GARLIC: ");
		memoryLabel.setPreferredSize(new Dimension(220, 40));
		scryptPane.add(memoryLabel);			
		NumberFormatter memoryFormatter = new NumberOrEmptyFormatter();
		memoryFormatter.setMaximum(99);
		memoryField = new JFormattedTextField(memoryFormatter);
		memoryLabel.setLabelFor(memoryField);
		memoryField.setText("18");
		memoryField.setColumns(2);
		memoryField.setDragEnabled(true);

		memoryRecommendedLabel = new JLabel("recommended >= 18");
		memoryRecommendedLabel.setFont(new Font(Font.SANS_SERIF, Font.PLAIN, 11));
		JPanel memoryPanel = new JPanel();
		memoryPanel.add(memoryField);
		memoryPanel.add(memoryRecommendedLabel);
		scryptPane.add(memoryPanel);
		
		scryptPane.add(Box.createVerticalStrut(10));

		JLabel timeLabel = new JLabel("Time cost parameter LAMBDA: ");
		timeLabel.setPreferredSize(new Dimension(220, 40));
		scryptPane.add(timeLabel);			
		NumberFormatter timeFormatter = new NumberOrEmptyFormatter();
		timeFormatter.setMaximum(999);
		timeField = new JFormattedTextField(timeFormatter);
		timeLabel.setLabelFor(timeField);
		timeField.setText("2");
		timeField.setColumns(3);
				
		timeRecommendedLabel = new JLabel("recommended 2");
		timeRecommendedLabel.setFont(new Font(Font.SANS_SERIF, Font.PLAIN, 11));
		JPanel timePanel = new JPanel();
		timePanel.add(timeField);
		timePanel.add(timeRecommendedLabel);
		scryptPane.add(timePanel);		
		
		scryptPane.add(Box.createVerticalStrut(10));
		
		errorLabel = new JLabel("");
		errorLabel.setForeground(Color.RED);
		scryptPane.add(errorLabel);
		
		this.add(scryptPane);	
	}
	
	/**
	 * Get the instance of Catena: Dragonfly-Full, Butterfly-FULL, 
	 * Dragonfly, Butterfly
	 * 
	 * @return	the current instance of Catena as String
	 */
	public String getCatenaInstance() {
		return instance;
	}
	
	/**
	 * Get the parameters: 
	 * [0] is time, [1] is memory, [2] is parallel
	 * 
	 * @return	the parameters: [0] is time, [1] is memory, [2] is parallel
	 */
	public int[] getParams() {
		int[] params = new int[3];
		try {
			timeField.commitEdit();
			params[0] = (int) timeField.getValue();
			params[1] = Integer.parseInt(memoryField.getText());
			params[2] = 0;
		} catch (Exception e) {
			TestLog.e(CatenaAdvancedPanel.class, "Can't set parameters for Catena: " + e.getLocalizedMessage());
			TestLog.ve(CatenaAdvancedPanel.class, e.getLocalizedMessage() + " - " + e.toString(), 5);
			return null;//getDefaultParams();
		}
		return params;
	}
	
	/**
	 * Get the default parameters: 
	 * [0] is time, [1] is memory, [2] is parallel
	 * 
	 * @return	the parameters: [0] is time, [1] is memory, [2] is parallel
	 */
	public static int[] getDefaultParams() {
		int[] params = new int[3];
		String versionID = CatenaKDF.getVersionID();
		if ((versionID.equals("Dragonfly-Full")) || (versionID.equals("Dragonfly"))){
			params[0] = 2;
			params[1] = 18;			
			params[2] = 0;
		} else if ((versionID.equals("Butterfly-Full")) || (versionID.equals("Butterfly"))){
			params[0] = 4;
			params[1] = 14;			
			params[2] = 0;
		}
		return params;
	}

	@Override
	public void actionPerformed(ActionEvent ape) {
		instance = ape.getActionCommand();
		int currentGarlic = Integer.parseInt(memoryField.getText());
		int currentLambda = Integer.parseInt(timeField.getText());
		if (currentGarlic > 22 || currentLambda > 4){
			errorLabel.setText("Warning: Long execution time");
		} else {
			errorLabel.setText("");
		}
		
		if (instance.equals("Dragonfly")){
			timeRecommendedLabel.setText("recommended 2"); // lambda: 2
			memoryRecommendedLabel.setText("recommended >= 18"); // garlic			
			timeField.setText("2");
			memoryField.setText("18");
		} else if (instance.equals("Dragonfly-Full")){
			timeRecommendedLabel.setText("recommended 2"); // lambda: 2
			memoryRecommendedLabel.setText("recommended >= 18"); // garlic		
			timeField.setText("2");
			memoryField.setText("18");
		} else if (instance.equals("Butterfly")){
			timeRecommendedLabel.setText("recommended 4"); // lambda: 4
			memoryRecommendedLabel.setText("recommended >= 14"); // garlic		
			timeField.setText("4");
			memoryField.setText("14");
		} else if (instance.equals("Butterfly-Full")){
			timeRecommendedLabel.setText("recommended 4"); // lambda: 4
			memoryRecommendedLabel.setText("recommended >= 14"); // garlic	
			timeField.setText("4");
			memoryField.setText("14");
		}		
	}
}
