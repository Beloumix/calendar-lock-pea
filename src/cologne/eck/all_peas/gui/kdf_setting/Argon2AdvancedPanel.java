package cologne.eck.all_peas.gui.kdf_setting;

/*
 * Peafactory - Production of Password Encryption Archives
 * Copyright (C) 2021  Axel von dem Bruch
 * 
 * This library is free software; you can redistribute it and/or modify it 
 * under the terms of the GNU General Public License as published 
 * by the Free Software Foundation; either version 2 of the License, 
 * or (at your option) any later version.
 * This library is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
 * or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU General Public License for more details.
 * See:  http://www.gnu.org/licenses/gpl-2.0.html
 * You should have received a copy of the GNU General Public License 
 * along with this library.
 */

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.util.Enumeration;

import javax.swing.AbstractButton;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.ButtonGroup;
import javax.swing.JFormattedTextField;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.text.NumberFormatter;

import cologne.eck.all_peas.gui.PeaBorderFactory;
import cologne.eck.tools.TestLog;

@SuppressWarnings("serial")
public class Argon2AdvancedPanel extends JPanel {
	
	private JFormattedTextField memoryField;
	private JFormattedTextField parallelField;
	private JFormattedTextField timeField;
	private JLabel memoryRecommendedLabel;
	private JLabel timeRecommendedLabel;
	private JLabel parallelRecommendedLabel;
	private  ButtonGroup group;
	
	private JLabel errorLabel;

	public Argon2AdvancedPanel() {

		JPanel argonPane = new JPanel();//(JPanel) setting.getContentPane();//new JPanel();
		argonPane.setBorder(PeaBorderFactory.createBorder(false));
		argonPane.setLayout(new BoxLayout(argonPane, BoxLayout.Y_AXIS));
		
		JLabel scryptLabel = new JLabel("Settings for Argon2:");
		scryptLabel.setPreferredSize(new Dimension(250, 30));
		
		JLabel tipLabel1 = new JLabel("Settings for this session only");
		tipLabel1.setFont(new Font(Font.SANS_SERIF, Font.PLAIN, 11));
		tipLabel1.setPreferredSize(new Dimension(250, 20));
		argonPane.add(scryptLabel);
		argonPane.add(tipLabel1);
		
		JLabel instanceLabel = new JLabel("Select an instance of Argon2:");
		instanceLabel.setPreferredSize(new Dimension(250, 40));
		argonPane.add(instanceLabel);		
		
		JLabel instanceRecommendedLabel = new JLabel("recommended: Argon2id");
		instanceRecommendedLabel.setPreferredSize(new Dimension(250, 20));
		instanceRecommendedLabel.setFont(new Font(Font.SANS_SERIF, Font.PLAIN, 11));
		argonPane.add(instanceRecommendedLabel);
	    
		JRadioButton idButton = new JRadioButton("Argon2id");
	    argonPane.add(idButton);
	    
		JRadioButton dButton = new JRadioButton("Argon2d");
	    argonPane.add(dButton);
	    
		JRadioButton iButton = new JRadioButton("Argon2i");
	    argonPane.add(iButton);
	    
	    //Group the radio buttons.
	    group = new ButtonGroup();
	    group.add(iButton);
	    group.add(idButton);
	    group.add(dButton);
	    
	    group.setSelected(idButton.getModel(), true);
	    
		argonPane.add(Box.createVerticalStrut(20));

		NumberFormatter parallelFormatter = new NumberOrEmptyFormatter();//NumberFormat.getIntegerInstance());
		parallelFormatter.setMaximum(99);
		//numberFormatter.setOverwriteMode(true);		
		
		JLabel parallelLabel = new JLabel("Parallelism parameter p (lanes): ");		
		parallelLabel.setPreferredSize(new Dimension(220, 40));
		argonPane.add(parallelLabel);			
		parallelField = new JFormattedTextField(parallelFormatter);
		parallelLabel.setLabelFor(parallelField);
		parallelField.setText("4");
		parallelField.setColumns(2);				
		parallelRecommendedLabel = new JLabel("recommended 4");
		parallelRecommendedLabel.setFont(new Font(Font.SANS_SERIF, Font.PLAIN, 11));
		JPanel parallelPanel = new JPanel();
		parallelPanel.add(parallelField);
		parallelPanel.add(parallelRecommendedLabel);
		argonPane.add(parallelPanel);		
		
		//DecimalFormat df = new DecimalFormat();
		//df.format(value).replace(Character.toString(decSep), "");
		NumberFormatter memoryFormatter = new NumberOrEmptyFormatter();
				//new DecimalFormat());//NumberFormat.getIntegerInstance());
		memoryFormatter.setMaximum(99999);

		JLabel memoryLabel = new JLabel("Memory cost parameter m (MiB): ");
		memoryLabel.setPreferredSize(new Dimension(220, 40));
		argonPane.add(memoryLabel);			
		memoryField = new JFormattedTextField(memoryFormatter);
		memoryLabel.setLabelFor(memoryField);
		memoryField.setText("128");
		memoryField.setColumns(5);
		memoryRecommendedLabel = new JLabel("recommended >= 128");
		memoryRecommendedLabel.setFont(new Font(Font.SANS_SERIF, Font.PLAIN, 11));
		JPanel memoryPanel = new JPanel();
		memoryPanel.add(memoryField);
		memoryPanel.add(memoryRecommendedLabel);
		argonPane.add(memoryPanel);
		
		argonPane.add(Box.createVerticalStrut(10));
		
		NumberFormatter timeFormatter = new NumberOrEmptyFormatter();
		timeFormatter.setMaximum(99);

		JLabel timeLabel = new JLabel("Time cost parameter t: ");		
		timeLabel.setPreferredSize(new Dimension(220, 40));
		argonPane.add(timeLabel);			
		timeField = new JFormattedTextField(timeFormatter);
		timeLabel.setLabelFor(timeField);
		timeField.setText("3");
		timeField.setColumns(2);
		timeRecommendedLabel = new JLabel("recommended 3");
		timeRecommendedLabel.setFont(new Font(Font.SANS_SERIF, Font.PLAIN, 11));
		JPanel timePanel = new JPanel();
		timePanel.add(timeField);
		timePanel.add(timeRecommendedLabel);
		argonPane.add(timePanel);		
		
		argonPane.add(Box.createVerticalStrut(10));
		
		errorLabel = new JLabel("");
		errorLabel.setForeground(Color.RED);
		argonPane.add(errorLabel);
		
		this.add(argonPane);
	}
	
	/**
	 * Get the instance of Argon: id, d, i
	 * 
	 * @return	the current instance of Argon2 as String: id, d, i
	 */
	public String getArgon2Instance() {
		for (Enumeration<AbstractButton> buttons = group.getElements(); buttons.hasMoreElements();) {
			Object obj = buttons.nextElement();
			if (obj instanceof JRadioButton) {
				JRadioButton button = (JRadioButton) obj;
				if (button.isSelected()) {
					return button.getText();
				}
			}
		}
		return null;
	}
	
	/**
	 * Get the parameters: 
	 * [0] is time, [1] is memory, [2] is parallel
	 * 
	 * @return	the parameters: [0] is zime, [1] is memory, [2] is parallel
	 */
	public int[] getParams() {
		int[] params = new int[3];
		try {
			timeField.commitEdit();
			params[0] = (Integer) timeField.getValue();
			// replace "." in memory field
			String memText = memoryField.getText();
			if (memText.contains(".")) {
				memText = memText.replace(".",  "");
			}			
			params[1] = Integer.parseInt(memText);
			parallelField.commitEdit();
			params[2] = (Integer) parallelField.getValue();
		}  catch (Exception e) {
			TestLog.e(Argon2AdvancedPanel.class, "Can't set parameters for Argon2: " + e.getLocalizedMessage()
			+ ": \n" + " + timeField.getValue()" + ", " +  memoryField.getText()  + ", " + parallelField.getValue());
			TestLog.ve(Argon2AdvancedPanel.class, e.getLocalizedMessage() + " - " + e.toString(), 5);
			// default: 128, 3, 4
			return null;//getDefaultParams();
		}
		TestLog.v(Argon2AdvancedPanel.class, "Argon2 params:  m: " + params[0] + ", t: " + params[1] + ", p: " + params[2]);
		return params;
	}
	
	/**
	 * Get the default parameters: 
	 * [0] is time, [1] is memory, [2] is parallel
	 * 
	 * @return	the parameters: [0] is time, [1] is memory, [2] is parallel
	 */
	public static int[] getDefaultParams() {
		int[] params = new int[3];
		// default: 3, 128, 4
		params[0] = 3;
		params[1] = 128;		
		params[2] = 4;
		//TestLog.v(Argon2AdvancedPanel.class, "Argon2 params:  m: " + params[0] + ", t: " + params[1] + ", p: " + params[2]);
		return params;
	}
}
