package cologne.eck.all_peas.gui.kdf_setting;

import java.text.NumberFormat;
import java.text.ParseException;

import javax.swing.text.NumberFormatter;

/*
 * Integer format with minimum 1.
 * "" or null String allowed in JFormattedTextField. 
 * Otherwise users can't delete the existing value in field
 */

@SuppressWarnings("serial")
public class NumberOrEmptyFormatter extends NumberFormatter {
	
	/**
	 * Formatter with integer format, minimum 1, letters not allowed,
	 * "" or null are parsed to 0.
	 */
	public NumberOrEmptyFormatter() { 
		super(NumberFormat.getIntegerInstance());
		this.setValueClass(Integer.class); 
		this.setAllowsInvalid(false); // no letters allowed
		this.setMinimum(1); // no 0 allowed
	}

	
	// Otherwise users can't delete the existing value in field
	@Override
	public Object stringToValue(String text) throws ParseException{
		if (text == null || text.equals("") ) {
			return Integer.valueOf(0);
		} else {
			return super.stringToValue(text);
		}
	}		
} 