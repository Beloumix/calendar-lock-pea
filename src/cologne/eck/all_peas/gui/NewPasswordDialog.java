package cologne.eck.all_peas.gui;

/*
 * Peafactory - Production of Password Encryption Archives
 * Copyright (C) 2015  Axel von dem Bruch
 * 
 * This library is free software; you can redistribute it and/or modify it 
 * under the terms of the GNU General Public License as published 
 * by the Free Software Foundation; either version 2 of the License, 
 * or (at your option) any later version.
 * This library is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
 * or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU General Public License for more details.
 * See:  http://www.gnu.org/licenses/gpl-2.0.html
 * You should have received a copy of the GNU General Public License 
 * along with this library.
 */
/**
 * Dialog to change the password of the pea. 
 */

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Point;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.util.Arrays;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.KeyStroke;
import javax.swing.WindowConstants;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

import cologne.eck.all_peas.data.PeaProperties;
import cologne.eck.all_peas.vm_specific.JREProperties;
import cologne.eck.tools.KeyRandomCollector;
import cologne.eck.tools.MouseRandomCollector;
import cologne.eck.tools.PasswordQualityCheck;
import cologne.eck.tools.TestLog;
import cologne.eck.tools.Zeroizer;


class NewPasswordDialog extends JDialog 
		implements ActionListener, DocumentListener, WindowListener {//KeyListener {	

	private static final long serialVersionUID = 1L;
	
	private static NewPasswordDialog newPswDialog = null;
	
	private static JLabel messageLabel = null;
	
	private static JPasswordField newPasswordField;
	private static JLabel qualityLabel;
	private static JLabel qualityLabelStars;
	private static JPasswordField retypePasswordField;
	
	private JLabel hintLabel;
	
	private static char[] returnPsw;
	
	private static boolean randomCollector = false;

	/**
	 * Show a dialog for a new password
	 * 
	 * @param owner			must be a awt.Window
	 * @param messageString	message to show
	 * @param hint			hint to show
	 * @param location		location
	 */
	private NewPasswordDialog(Window owner, String messageString, String hint, Point location) {
		
		super((Window)owner);
		if (owner == null) {
			TestLog.ve(getClass(), "Missing owner", 5);
		}

			
		this.setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
		this.addWindowListener(this);
		//this.setModal(true);
		this.setModalityType(JDialog.ModalityType.APPLICATION_MODAL);
		this.getRootPane().getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW)
    		.put(KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), "CLOSE_ON_ESCAPE");
		this.getRootPane().getActionMap().put("CLOSE_ON_ESCAPE", new CloseOnEscapeAction() );
				
		int fontSize = PeaProperties.getFontSize();
		
		// show passwordField
		newPswDialog = this;
		newPswDialog.setAlwaysOnTop(true);
		newPswDialog.setModal(true);
		newPswDialog.setIconImage(PswDialogView.getImage() );
		newPswDialog.setFont(new Font(Font.SANS_SERIF, Font.PLAIN, fontSize));
		
		newPswDialog.setLayout(new BoxLayout(newPswDialog.getContentPane(), BoxLayout.Y_AXIS));

		if(randomCollector == true){
			newPswDialog.addMouseMotionListener(new MouseRandomCollector() );
		}
		
		JPanel messageLabelPanel = new JPanel();
		messageLabelPanel.setLayout(new BoxLayout(messageLabelPanel, BoxLayout.X_AXIS));
		
		messageLabel = new JLabel(messageString);
		messageLabel.setFont(new Font(Font.SANS_SERIF, Font.PLAIN, fontSize));
		messageLabel.setPreferredSize(new Dimension(300,30));

		if (messageString.equals(PeaProperties.getVmBridge().translate("initialization"))
				|| messageString.equals(PeaProperties.getVmBridge().translate("change_password"))){
			messageLabel.setForeground(new Color(0, 80, 0));//dark green
		} else {
			messageLabel.setForeground(Color.RED);
		}
		
		messageLabelPanel.add(messageLabel);			
		
		newPswDialog.add(messageLabelPanel);
		
		JPanel pswLabelPanel = new JPanel();
		pswLabelPanel.setLayout(new BoxLayout(pswLabelPanel, BoxLayout.X_AXIS));
		
		JLabel pswLabel = new JLabel(PeaProperties.getVmBridge().translate("enter_new_password"));
		pswLabel.setLabelFor(newPasswordField);
		
		pswLabel.setFont(new Font(Font.SANS_SERIF, Font.PLAIN, fontSize));
		pswLabelPanel.add(pswLabel);	
		
		pswLabelPanel.add(Box.createHorizontalStrut(20));
		
		qualityLabel = new JLabel();
		qualityLabel.setFont(new Font(Font.SERIF, Font.ITALIC, fontSize + 2));
		pswLabelPanel.add(qualityLabel);
		
		pswLabelPanel.add(Box.createHorizontalStrut(5));
		
		qualityLabelStars = new JLabel();
		qualityLabelStars.setOpaque(true);
		qualityLabelStars.getAccessibleContext().
	    setAccessibleDescription(
	    	    "Shows the quality of the typed password as a number of stars.");
		qualityLabelStars.setMaximumSize(new Dimension (300, 10));
		//qualityLabelStars.setBackground(Color.WHITE);
		qualityLabelStars.setFont(new Font(Font.MONOSPACED, Font.PLAIN, fontSize + 4));
		pswLabelPanel.add(qualityLabelStars);
		
		pswLabelPanel.add(Box.createHorizontalGlue());
		
		JButton charTableButton = new JButton(PeaProperties.getVmBridge().translate("char_table"));
		charTableButton.setFont(new Font(Font.SANS_SERIF, Font.PLAIN, fontSize - 2));
		charTableButton.addActionListener(this);
		charTableButton.setActionCommand("charTable1");
		if(randomCollector == true){
			charTableButton.addMouseMotionListener(new MouseRandomCollector() );
		}
		pswLabelPanel.add(charTableButton);
		
		newPswDialog.add(pswLabelPanel);
			
		newPasswordField = new JPasswordField(50);
		newPasswordField.setActionCommand("newPsw");// Enter
		newPasswordField.addActionListener(this);
		if(randomCollector == true){
			newPasswordField.addKeyListener(new KeyRandomCollector() );
		}

		// if the event comes from CharTable or virtual Keyboard
		// two events are received because CharTable must replace
		// the current password (remove and insert)
		newPasswordField.getDocument().addDocumentListener(this);

		newPswDialog.add(newPasswordField);
		
		newPswDialog.add(Box.createVerticalStrut(10));
		
		JPanel pswLabelPanel2 = new JPanel();
		pswLabelPanel2.setLayout(new BoxLayout(pswLabelPanel2, BoxLayout.X_AXIS));
		
		JLabel pswLabel2 = new JLabel(PeaProperties.getVmBridge().translate("retype_password"));
		pswLabel2.setLabelFor(retypePasswordField);
		pswLabel2.setPreferredSize(new Dimension(300,30));
		pswLabel2.setFont(new Font(Font.SANS_SERIF, Font.PLAIN, fontSize));
		pswLabelPanel2.add(pswLabel2);		
		pswLabelPanel2.add(Box.createHorizontalGlue());
		
		JButton charTableButton2 = new JButton(PeaProperties.getVmBridge().translate("char_table"));
		charTableButton2.setFont(new Font(Font.SANS_SERIF, Font.PLAIN, fontSize - 2));
		charTableButton2.addActionListener(this);
		charTableButton2.setActionCommand("charTable2");
		if(randomCollector == true){
			charTableButton2.addMouseMotionListener(new MouseRandomCollector() );
		}
		pswLabelPanel2.add(charTableButton2);
		
		newPswDialog.add(pswLabelPanel2);

		retypePasswordField = new JPasswordField(50);
		retypePasswordField.setActionCommand("retypePsw");// Enter
		retypePasswordField.addActionListener(this);
		if(randomCollector == true){
			retypePasswordField.addKeyListener(new KeyRandomCollector() );
		}
		newPswDialog.add(retypePasswordField);
		
		if (hint != null) {
			JPanel hintPanel = new JPanel();
			hintPanel.setLayout(new BoxLayout(hintPanel, BoxLayout.X_AXIS));
			hintLabel = new JLabel(hint);
			hintLabel.setFont(new Font(Font.SANS_SERIF, Font.PLAIN, fontSize));
			//hintLabel.setPreferredSize(new Dimension(300,30));
			hintPanel.add(hintLabel);
			hintPanel.add(Box.createHorizontalGlue());
			newPswDialog.add(hintPanel);
		}
		
		JPanel keyFileOnlyPanel = new JPanel();
		keyFileOnlyPanel.setLayout(new BoxLayout(keyFileOnlyPanel, BoxLayout.LINE_AXIS));
		JButton keyFileOnlyButton = new JButton(PeaProperties.getVmBridge().translate("use_only_keyfile")); 
		keyFileOnlyButton.addActionListener(this);
		keyFileOnlyButton.setActionCommand("keyFileOnly");
		keyFileOnlyPanel.add(keyFileOnlyButton);
		keyFileOnlyPanel.add(Box.createHorizontalGlue());
		newPswDialog.add(Box.createVerticalStrut(10));
		newPswDialog.add(keyFileOnlyPanel);
		
		JPanel okPanel = new JPanel();
		if(randomCollector == true){
			okPanel.addMouseMotionListener(new MouseRandomCollector() );
		}
		okPanel.setLayout(new BoxLayout(okPanel, BoxLayout.X_AXIS));
		JButton newPswButton = new JButton(PeaProperties.getVmBridge().translate("ok"));
		if(randomCollector == true){
			newPswButton.addMouseMotionListener(new MouseRandomCollector() );
		}
		newPswButton.addActionListener(this);
		newPswButton.setActionCommand("newPsw");// ok-Button
		okPanel.add(Box.createHorizontalGlue());
		okPanel.add(newPswButton);		
		
		newPswDialog.add(okPanel);
		
		JPanel contentPane = (JPanel) newPswDialog.getContentPane();
		contentPane.setBorder(PeaBorderFactory.createBorder(false));
		
		//newPswDialog.setSize( 400, 170);
		newPswDialog.setMinimumSize( new Dimension(400, 170));
		newPswDialog.pack();
		
		
		if (owner != null && owner.isShowing()) {
			this.setLocationRelativeTo((Component) owner);
		} else {
			newPswDialog.setLocation(location);	
		}
		newPswDialog.setVisible(true);		
	}
	
	protected final static NewPasswordDialog getInstance(
			Window owner, String messageString, String hint, Point loc) {
		
		if (owner == null) {
			TestLog.ve(NewPasswordDialog.class,  "Missing owner", 5);
			owner = JREProperties.getMainWindow();
		}

		if ( owner == null || ((Window)owner).isShowing() == false) {
			TestLog.ve(NewPasswordDialog.class,  "Missing owner (" + (owner == null) + ") or not showing", 5);
			// check all opened windows: 
			Window[] windows = Window.getWindows();
			Window showingWindow = null;
			for (Window w : windows) {
				if (w.isVisible() && w.isShowing()) {
					showingWindow = w;
					break;
				}
			}
			if (showingWindow != null) {
				owner = showingWindow;
			}
		}
		if (newPswDialog == null) {
			newPswDialog =  new NewPasswordDialog(owner, messageString, hint, loc);
		} else {
			//
		}
		return newPswDialog;
	}

	/**
	 * Get the typed password. If no password was typed, 
	 * "no password" is returned. 
	 * If null is returned, the password dialog was cancelled.
	 * 
	 * @return	the password
	 */
	public final char[] getDialogInput() {
		if (returnPsw == null) {
			if (PeaProperties.isKeyFileOnlyMode() == true) {
				TestLog.v(NewPasswordDialog.class, "keyfile only mode");
			} else {
				TestLog.v(NewPasswordDialog.class, "Password dialog cancelled...");
			}
		}
		resetDialog();
		return returnPsw;	
	}	
	
	private synchronized void resetDialog() {
		messageLabel.setText("");
		newPswDialog = null;
	}
	
	public void setMessage(String message) {
		// error message: foreground red
		messageLabel.setForeground(Color.RED);
		messageLabel.setText(message);
	}
/*	public void setHint(String hint) {

		hintLabel.setForeground(new Color(0, 80, 0));//dark green
		hintLabel.setText(hint);
		newPswDialog.revalidate();
		newPswDialog.repaint();
	} */	
	
	private final void updatePasswordCheck() {
		char[] pwd = newPasswordField.getPassword();
		if (pwd == null) {
			return;
		}
		int quality = PasswordQualityCheck.checkQuality(pwd);//pswField.getPassword() );
		
		// display at most 24 stars:
		if (quality > 24){
			quality = 24;
		}		
		if (pwd != null){
			Zeroizer.zero(pwd);
		}
		pwd = null;
		char[] q = new char[quality];
		//Arrays.fill(q,  '*');
		Arrays.fill(q,  ' ');
		
		if (quality < 12) {
			qualityLabel.setText(PeaProperties.getVmBridge().translate("weak"));
			qualityLabelStars.setText(new String(q)); 
			//qualityLabelStars.setForeground(Color.RED);
			qualityLabelStars.setBackground(Color.RED);
		} else if (quality < 16) {
			qualityLabel.setText(PeaProperties.getVmBridge().translate("medium"));
			qualityLabelStars.setText(new String(q)); 
			//qualityLabelStars.setForeground(Color.YELLOW);
			qualityLabelStars.setBackground(Color.YELLOW);
		} else {
			qualityLabel.setText(PeaProperties.getVmBridge().translate("strong"));
			qualityLabelStars.setText(new String(q)); 
			//qualityLabelStars.setForeground(Color.GREEN);
			qualityLabelStars.setBackground(Color.GREEN);
		}
	}

	//
	// derive keys from password and set new keys in CryptStuff
	//
	@Override
	public void actionPerformed(ActionEvent ape) {
		
		String com = ape.getActionCommand();
		
		if ( com.startsWith("charTable")) {	
			if(com.equals("charTable1")){
				CharTable table = new CharTable(this, newPasswordField, 0);
				table.setVisible(true);
			} else {
				CharTable table = new CharTable(this, retypePasswordField, 0);
				table.setVisible(true);				
			}
			
		} else if (com.equals("newPsw")) {
			
			// get password 
			char[] newPsw = newPasswordField.getPassword();	
			char[] retypePsw = retypePasswordField.getPassword();			
			
			if (retypePsw.length == 0 && newPsw.length != 0) {
				setMessage(PeaProperties.getVmBridge().translate("must_retype_password"));
				return; 
			}
			
			if (newPsw.length != retypePsw.length) {
				setMessage(PeaProperties.getVmBridge().translate("password_not_equal"));
				newPasswordField.setText("");
				retypePasswordField.setText("");
				qualityLabelStars.setText("");
				return;				
			}
			boolean pswEqual = true;
			for (int i = 0; i < newPsw.length; i++) {
				if (newPsw[i] != retypePsw[i]) {
					pswEqual = false;
				}
			}
			if (pswEqual == false) {
				setMessage(PeaProperties.getVmBridge().translate("password_not_equal"));
				newPasswordField.setText("");
				retypePasswordField.setText("");
				qualityLabelStars.setText("");
				return;				
			}
			
			if (newPsw.length == 0){
				// On some systems the warning message is displayed
				// behind the dialog otherwise: 
				newPswDialog.setAlwaysOnTop(false);
				// allow null-passwords with warning
				JOptionPane.showMessageDialog(newPswDialog,
						PeaProperties.getVmBridge().translate("null_password_chosen"),
					PeaProperties.getVmBridge().translate("warning"),
					JOptionPane.WARNING_MESSAGE);
				newPsw = "no password".toCharArray();
			}
			//returnPsw = newPsw;
			setReturnPsw(newPsw);
			//TestLog.o(XXXX.class, "NewPasswordDialog actioPerformed psw: " + new String(returnPsw) );
			if (retypePsw.length > 0){
				Zeroizer.zero(retypePsw);
			}
			newPasswordField.setText("");		
			this.dispose();
		
		} else if (com.equals("keyFileOnly")) {
			TestLog.v(getClass(), "Keyfile only mode...");
			KeyFileOnlyDialog kfod = new KeyFileOnlyDialog(this);
			kfod.setVisible(true);
			//PeaProperties.setKeyFileOnlyMode(true);
			//setReturnPsw(null);
			//this.dispose();			
			if (kfod.isSuccessfullySet() == false) {
				TestLog.v(getClass(), "Keyfile-only mode was not set");
				return;
			} else {
				TestLog.v(getClass(), "Keyfile-only mode was set");
				setReturnPsw(null);
				this.dispose();				
			}
		}
	}
	
	private static synchronized void setReturnPsw(char[] newPsw) {
		returnPsw = newPsw;
	}
	
/*	public static void close() {
		if (newPswDialog.isVisible() ||  newPswDialog.isShowing() ) {
			newPswDialog.dispose();
		}
	}*/
	
	/**
	 * @param randomCollector the randomCollector to set
	 */
	protected static void setRandomCollector(boolean _randomCollector) {
		NewPasswordDialog.randomCollector = _randomCollector;
	}

	@Override
	public void changedUpdate(DocumentEvent arg0) {}

	@Override
	public void insertUpdate(DocumentEvent arg0) {
		updatePasswordCheck();		
	}

	@Override
	public void removeUpdate(DocumentEvent arg0) {
		updatePasswordCheck();		
	}	

	@Override
	public void windowActivated(WindowEvent arg0) {}

	@Override
	public void windowClosed(WindowEvent arg0) {}

	@Override
	public void windowClosing(WindowEvent arg0) {
		setReturnPsw(null);		
		this.resetDialog();
		this.dispose();
	}

	@Override
	public void windowDeactivated(WindowEvent arg0) {}

	@Override
	public void windowDeiconified(WindowEvent arg0) {}

	@Override
	public void windowIconified(WindowEvent e) {}

	@Override
	public void windowOpened(WindowEvent e) {}
}