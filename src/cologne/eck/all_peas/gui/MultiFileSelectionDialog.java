package cologne.eck.all_peas.gui;

/*
 * PeaFactory - Production of Password Encryption Archives
 * Copyright (C) 2015  Axel von dem Bruch
 * 
 * This library is free software; you can redistribute it and/or modify it 
 * under the terms of the GNU General Public License as published 
 * by the Free Software Foundation; either version 2 of the License, 
 * or (at your option) any later version.
 * This library is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
 * or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU General Public License for more details.
 * See:  http://www.gnu.org/licenses/gpl-2.0.html
 * You should have received a copy of the GNU General Public License 
 * along with this library.
 */

import java.awt.Component;
import java.awt.Dimension;
import java.awt.Point;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.KeyStroke;

import cologne.eck.all_peas.data.PeaProperties;
import cologne.eck.tools.TestLog;

@SuppressWarnings("serial")
public class MultiFileSelectionDialog extends JDialog implements ActionListener, WindowListener {
	
	private JPanel filePanel;
	private ArrayList <String> fileList;

	public MultiFileSelectionDialog (Window parent, Point loc, String areaText,ArrayList <String> origFileNames) {
		
		super(parent);
		
		if (origFileNames == null) {
			TestLog.e(MultiFileSelectionDialog.class, "No file names to show (MultiFileSelectionDialog)");
			return;
		}
		// Use a copy of the list, the list will be probably modified
		ArrayList<String> fileNames = new ArrayList<String>(origFileNames);
		this.setModalityType(JDialog.DEFAULT_MODALITY_TYPE);
		this.setDefaultCloseOperation(JDialog.DO_NOTHING_ON_CLOSE);
		this.addWindowListener(this);
		
		// Avoid keyboard trap: Close with ESC
		this.getRootPane().getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW)
    		.put(KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), "CLOSE_ON_ESCAPE");
		this.getRootPane().getActionMap().put("CLOSE_ON_ESCAPE", new CloseOnEscapeAction() );
		
		JPanel contentPane = (JPanel) this.getContentPane();
		contentPane.setLayout(new BoxLayout(contentPane, BoxLayout.Y_AXIS));
		
		JPanel textPanel = new JPanel();
		textPanel.setLayout(new BoxLayout(textPanel, BoxLayout.X_AXIS));
		JTextArea textArea = new JTextArea();
		textArea.setWrapStyleWord(true);
		textArea.setLineWrap(true);
		textArea.setText(areaText);
		textArea.setEditable(false);
		textPanel.add(textArea);
		textPanel.add(Box.createHorizontalGlue());
		textPanel.setMaximumSize(new Dimension(600,200));
		textPanel.setMinimumSize(new Dimension(200,50));
		contentPane.add(textPanel);

		filePanel = new JPanel();
		filePanel.setLayout(new BoxLayout(filePanel, BoxLayout.Y_AXIS));

		// remove null elements:
		Iterator<String> it = fileNames.iterator();
		while (it.hasNext()) {
			String name = it.next();
			if (name == null) {
				it.remove();
			}
		}
		// alphabetical order: 
		Collections.sort(fileNames);
		int len = fileNames.size();

		for (int i = 0; i < len; i++) {
			if (fileNames.get(i) != null) {
				JCheckBox check = new JCheckBox(fileNames.get(i));
				check.setSelected(true);
				filePanel.add(check);
			} else {
				TestLog.ve(MultiFileSelectionDialog.class, "Null file name found");
			}
		}
		
		JScrollPane scrollPane = new JScrollPane(filePanel);
		scrollPane.setPreferredSize(new Dimension(400, 200));
		contentPane.add(scrollPane);
		
		JPanel okPanel = new JPanel();
		okPanel.setLayout(new BoxLayout(okPanel, BoxLayout.X_AXIS));
		JButton okButton = new JButton(PeaProperties.getVmBridge().translate("ok"));
		okButton.getAccessibleContext().
	    setAccessibleDescription(
	    	    "Clicking this component takes the selected files and closes this dialog.");
		okButton.addActionListener(this);
		okButton.setActionCommand("ok");
		okPanel.add(Box.createHorizontalGlue());
		okPanel.add(okButton);
		contentPane.add(okPanel);
		
		this.pack();
		this.setLocation(loc);
		this.setVisible(true);
	}
	
	@Override
	public void actionPerformed(ActionEvent ape) {
		fileList = new ArrayList<String>();
		String command = ape.getActionCommand();
		if (command.equals("ok")) {

			Component[] components = filePanel.getComponents();
			int len = components.length;
			for (int i = 0; i < len; i++) {
				if (components[i] instanceof JCheckBox) {
					if ( ( (JCheckBox) components[i] ).isSelected() == true) {
						//TestLog.o(XXXX.class, "ImageDisplay add: " +  ((JCheckBox) components[i]).getText() );	
						String fileName =  ((JCheckBox) components[i]).getText();
						if (fileName != null) {
							fileList.add(fileName);
						} else {
							TestLog.ve(MultiFileSelectionDialog.class, "Checkbox without file name");
						}
					}
				}
			}
			this.dispose();
		}
	}

	/**
	 * Get a sorted list of selected file names.
	 * 
	 * @return	the list of selected file names
	 */
	public ArrayList <String> getSelectedFiles() {
		if (fileList != null && fileList.size() > 0) {
			// return sorted file list
			Collections.sort(fileList);
		} else {
			TestLog.ve(MultiFileSelectionDialog.class, "Empty list of selected files...");
		}
		return fileList;
	}


	@Override
	public void windowActivated(WindowEvent arg0) {}
	@Override
	public void windowClosed(WindowEvent arg0) {}
	
	@Override
	public void windowClosing(WindowEvent arg0) {
		fileList = null;		
		this.dispose();
	}
	
	@Override
	public void windowDeactivated(WindowEvent arg0) {}
	@Override
	public void windowDeiconified(WindowEvent arg0) {}
	@Override
	public void windowIconified(WindowEvent arg0) {}
	@Override
	public void windowOpened(WindowEvent arg0) {}
}
