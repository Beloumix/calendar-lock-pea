package cologne.eck.all_peas.gui;

/*
 * PeaFactory - Production of Password Encryption Archives
 * Copyright (C) 2015  Axel von dem Bruch
 * 
 * This library is free software; you can redistribute it and/or modify it 
 * under the terms of the GNU General Public License as published 
 * by the Free Software Foundation; either version 2 of the License, 
 * or (at your option) any later version.
 * This library is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
 * or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU General Public License for more details.
 * See:  http://www.gnu.org/licenses/gpl-2.0.html
 * You should have received a copy of the GNU General Public License 
 * along with this library.
 */

/**
 * This class is currently only used by Image Lock PEA
 */

import java.awt.Component;
import java.awt.Dimension;
import java.awt.Point;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.KeyStroke;

import cologne.eck.tools.TestLog;


@SuppressWarnings("serial")
public class SingleFileSelectionDialog extends JDialog implements ActionListener {
	
	private JPanel filePanel;
	private static String fileName;
	private ButtonGroup group = null;

	public SingleFileSelectionDialog (Window window, Point loc, String areaText, ArrayList <String> origFileNames) {
		super( window);
		if (origFileNames == null) {
			TestLog.ve(getClass(), "is null", 5);
			return;
		}
		// use a copy: list may be modified
		ArrayList<String> fileNames = new ArrayList<String>(origFileNames);
		this.setModalityType(JDialog.DEFAULT_MODALITY_TYPE);
		
		// Avoid keyboard trap: Close with ESC
		this.getRootPane().getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW)
    		.put(KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), "CLOSE_ON_ESCAPE");
		this.getRootPane().getActionMap().put("CLOSE_ON_ESCAPE", new CloseOnEscapeAction() );
		
		JPanel contentPane = (JPanel) this.getContentPane();
		contentPane.setLayout(new BoxLayout(contentPane, BoxLayout.Y_AXIS));
		
		JPanel textPanel = new JPanel();
		textPanel.setLayout(new BoxLayout(textPanel, BoxLayout.X_AXIS));
		JTextArea textArea = new JTextArea();
		textArea.setEditable(false);
		textArea.setText(areaText);
		textPanel.add(textArea);
		textPanel.add(Box.createHorizontalGlue());
		textPanel.setMaximumSize(new Dimension(600,200));
		textPanel.setMinimumSize(new Dimension(200,50));
		contentPane.add(textPanel);

		filePanel = new JPanel();
		filePanel.setLayout(new BoxLayout(filePanel, BoxLayout.Y_AXIS));
		// alphabetical order: 
		// first remove null elements to avoid NullPointerExc.
		// use iterator to avoid ConcurrentModificationException
		Iterator<String> iti = fileNames.iterator();
		while (iti.hasNext()) {
			String s = iti.next();
			if (s == null) {
				iti.remove();
			}
		}
		Collections.sort(fileNames);
		group = new ButtonGroup();
		int len = fileNames.size();
		for (int i = 0; i < len; i++) {
			JRadioButton rb = new JRadioButton(fileNames.get(i));
			rb.setSelected(true);
			group.add(rb);
			filePanel.add(rb);
		}

		JScrollPane scrollPane = new JScrollPane(filePanel);
		scrollPane.setPreferredSize(new Dimension(400, 200));
		contentPane.add(scrollPane);
		
		JPanel okPanel = new JPanel();
		okPanel.setLayout(new BoxLayout(okPanel, BoxLayout.X_AXIS));
		JButton okButton = new JButton("ok");
		okButton.addActionListener(this);
		okButton.setActionCommand("ok");
		okPanel.add(Box.createHorizontalGlue());
		okPanel.add(okButton);
		contentPane.add(okPanel);
		
		this.pack();
		this.setLocation(loc);
		this.setVisible(true);
	}
	
	@Override
	public void actionPerformed(ActionEvent ape) {

		String command = ape.getActionCommand();
		if (command.equals("ok")) {

			Component[] components = filePanel.getComponents();
			int len = components.length;
			for (int i = 0; i < len;i++) {
				if (components[i] instanceof JRadioButton) {
					if (( (JRadioButton)components[i]).isSelected() == true) {
						setFileName( ((JRadioButton) components[i]).getText() );
					}
				}
			}
			this.dispose();
		}
	}
	
	private static synchronized void setFileName(String newFileName) {
		SingleFileSelectionDialog.fileName = newFileName;
	}
	
	public static String getSelectedFileName() {
		
		return fileName;
	}
}
