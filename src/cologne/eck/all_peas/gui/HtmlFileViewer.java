package cologne.eck.all_peas.gui;

/*
 * Peafactory - Production of Password Encryption Archives
 * Copyright (C) 2020  Axel von dem Bruch
 * 
 * This library is free software; you can redistribute it and/or modify it 
 * under the terms of the GNU General Public License as published 
 * by the Free Software Foundation; either version 2 of the License, 
 * or (at your option) any later version.
 * This library is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
 * or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU General Public License for more details.
 * See:  http://www.gnu.org/licenses/gpl-2.0.html
 * You should have received a copy of the GNU General Public License 
 * along with this library.
 */

import java.awt.Dimension;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;


import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JEditorPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.KeyStroke;

import cologne.eck.all_peas.data.PeaProperties;
import cologne.eck.all_peas.vm_specific.JREProperties;
import cologne.eck.tools.ExceptionHelper;
import cologne.eck.tools.TestLog;



@SuppressWarnings("serial")
public class HtmlFileViewer extends JDialog implements ActionListener {
	
	
	private boolean okButtonPressed = false;
	
	/**
	 * Shows a HTML file in a window. Looks for a file in the
	 * current language. If not found, shows tips in English.
	 * If fails, show error message.
	 * 
	 * @param fileNameWithoutLang	the file name without extension html
	 * 					and without language extension
	 * @param loc		the location to show the dialog
	 */
	public HtmlFileViewer(String fileName, Window owner) {
		new HtmlFileViewer(fileName, null, owner);
	}
	
	/**
	 * Shows a HTML file in a window. Looks for a file in the
	 * current language. If not found, shows tips in English.
	 * If fails, show error message.
	 * 
	 * @param fileNameWithoutLang	the file name without extension html
	 * 					and without language extension
	 * @param buttonText	the text of the ok button
	 * @param loc		the owner component
	 */
	public HtmlFileViewer(String fileName, String buttonText, Window owner) {


		super(owner);
		if (owner == null) {
			owner = JREProperties.getMainWindow();
		}
		this.setModalityType(JDialog.ModalityType.APPLICATION_MODAL);
		
		okButtonPressed = false;

		// Avoid keyboard trap: Close with ESC
		this.getRootPane().getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW)
    		.put(KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), "CLOSE_ON_ESCAPE");
		this.getRootPane().getActionMap().put("CLOSE_ON_ESCAPE", new CloseOnEscapeAction() );
		
		
		JPanel contentPane = (JPanel) this.getContentPane();
		contentPane.setLayout(new BoxLayout(contentPane, BoxLayout.PAGE_AXIS));
		
		JEditorPane editorpane= new JEditorPane();
	//	editorpane.setFocusable(false);
		editorpane.setContentType("text/html;charset=UTF-8");
		//editorpane.setContentType("text/html");
		//editorpane.getDocument().putProperty("IgnoreCharsetDirective", Boolean.TRUE);
	    JScrollPane editorScrollPane = new JScrollPane(editorpane);
	    editorScrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
	    
	    String lang = PeaProperties.getLastLanguage();
	    // the file name for the current language:
		String fileNameLang = fileName + "_" + lang + ".html";
		String langFolder = lang + File.separator; // in sub-directory
		// Handle en or default/fallback
		if (lang == null || lang.equals("en")) {
			fileNameLang = fileName + ".html";// English version
			lang = "";
			langFolder = ""; // in resources folder, no sub-folder
		}
		
		URL resUrl = null;
		// check external folder resources for language
		File file = new File(System.getProperty("user.dir") + File.separator 
				+ "resources" + File.separator + langFolder + fileNameLang);

		boolean success = false;
		// 1. Try from external folder resources:
		if (file != null && file.exists()) {
			// get URL from inside jar archive:
			try {
				resUrl = file.toURI().toURL();
			} catch (MalformedURLException e) {
				TestLog.ve(HtmlFileViewer.class, file.getAbsolutePath() + "\n" +  e.getLocalizedMessage(), 5);
			} catch (Exception e) {
				TestLog.ve(HtmlFileViewer.class, file.getAbsolutePath()
						+ "\n" +  e.getLocalizedMessage() + " - " + e.toString(), 5);
			}
			// Set page:
			try {
				editorpane.setPage(resUrl);
				success = true;
			} catch (IOException e) {
				TestLog.ve(HtmlFileViewer.class, file.getAbsolutePath()
						+ "\n" +  e.getLocalizedMessage() + " - " + e.toString(), 5);
			}
		}
		if (success == true) {
			TestLog.v(HtmlFileViewer.class,"Found info file in external folder resources");
		} else { 
			// 2. inside jar in folder resources
			if ( ! langFolder.equals("")) { // not default or en
				langFolder = lang + "/";// Replace File.separator
			}

			// check inside jar for language
			try { // Don't use File.separator
				resUrl = HtmlFileViewer.class.getClassLoader().getResource("resources/" + langFolder + fileNameLang);
				editorpane.setPage(resUrl);
			} catch (MalformedURLException e) {
				TestLog.e(HtmlFileViewer.class, "Can't display guide for first use: " + fileNameLang);
				TestLog.ve(HtmlFileViewer.class, e.getLocalizedMessage(), 5);
				editorpane.setText("<html>" + PeaProperties.getVmBridge().translate("unexpected_error")
						+ "<br><br>" + e.getLocalizedMessage() + "</html>");
			} catch (IOException e) {
				TestLog.v(HtmlFileViewer.class, "Can't find html file " + fileNameLang + " for language " + lang);
				if ( ! lang.equals("")) {
					
					// try default language (English) in jar file
					fileNameLang = fileName + ".html";// English version
					lang = "";
					langFolder = ""; // in resources folder, no sub-folder
					try {
						resUrl = HtmlFileViewer.class.getClassLoader().getResource("resources/" + langFolder + fileNameLang);
						editorpane.setPage(resUrl);
					} catch (IOException e2) {
						TestLog.ve(HtmlFileViewer.class, "Can't find html file " + fileNameLang);
						// test default from folder resources
						file = new File("resources" + File.separator + langFolder + fileNameLang);
						if (file.exists()) {
							try {
								editorpane.setPage(file.toURI().toURL());
							} catch (MalformedURLException e1) {
								TestLog.ve(HtmlFileViewer.class, new ExceptionHelper(e1).getInfos());
								TestLog.ve(HtmlFileViewer.class, "Can't display guide for first use: " + fileNameLang, 5);
								editorpane.setText("<html>" + PeaProperties.getVmBridge().translate("unexpected_error")
										+ "<br><br>" + e1.getLocalizedMessage() + "</html>");
							} catch (IOException e1) {
								TestLog.ve(HtmlFileViewer.class, "Can't find html file " + file.getAbsolutePath());
								editorpane.setText("<html>" + PeaProperties.getVmBridge().translate("unexpected_error")
										+ "<br><br>" + e1.getLocalizedMessage() + "</html>");
							}
						} else {
							TestLog.ve(HtmlFileViewer.class, "Can't find html file " + file.getAbsolutePath());
							editorpane.setText("<html>" + PeaProperties.getVmBridge().translate("unexpected_error")
									+ "<br><br>" + "Missing file: <br> " + fileNameLang + "</html>");
						}
					}
				}
			} catch (Exception e) {
				TestLog.e(HtmlFileViewer.class, "unexpected error: " + e.getLocalizedMessage());
				TestLog.ve(HtmlFileViewer.class, "unexpected error: " + e.getLocalizedMessage() + " - " + e.toString(), 5);
				editorpane.setText("<html>" + PeaProperties.getVmBridge().translate("unexpected_error")
						+ "<br><br>" + e.getLocalizedMessage() + "</html>");
			}
		}
	    editorpane.setEditable(false);	    
	    contentPane.add(editorScrollPane);
	    
	    if (buttonText == null) {
	    	buttonText = PeaProperties.getVmBridge().translate("ok");
	    } 
	    
	    JButton okButton = new JButton(buttonText);
	    okButton.addActionListener(this);
	    okButton.setActionCommand("hideDialog");
	    okButton.getAccessibleContext().
	    setAccessibleDescription(
	    	    "Clicking this component closes this info dialog.");
	    //okButton.setMaximumSize(new Dimension(200, 20));
	    contentPane.add(okButton);
	    if (owner != null && owner.isVisible() && owner.isShowing() ) {
	    	this.setLocation(owner.getLocationOnScreen());
	    } else {
	    	TestLog.ve(HtmlFileViewer.class, "Missing owner ", 5);
	    }
	    this.setSize(new Dimension(500,450));

	    okButton.requestFocus();
	    okButton.requestFocusInWindow();
	    TestLog.v(HtmlFileViewer.class, "Open html file..." + fileName);

	    this.setVisible(true);	
	}


	@Override
	public void actionPerformed(ActionEvent ae) {

		if (ae.getActionCommand().equals("hideDialog")) {
			okButtonPressed = true;
			this.setVisible(false);
			this.dispose();
		}		
	}
	public boolean isOkButtonPressed() {
		return okButtonPressed;
	}
}
