package cologne.eck.all_peas.gui;

import java.awt.Color;

/*
 * PeaFactory - Production of Password Encryption Archives
 * Copyright (C) 2015  Axel von dem Bruch
 * 
 * This library is free software; you can redistribute it and/or modify it 
 * under the terms of the GNU General Public License as published 
 * by the Free Software Foundation; either version 2 of the License, 
 * or (at your option) any later version.
 * This library is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
 * or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU General Public License for more details.
 * See:  http://www.gnu.org/licenses/gpl-2.0.html
 * You should have received a copy of the GNU General Public License 
 * along with this library.
 */
/**
 * Show messages and questions with JOptionPanes
 */

import java.awt.Component;
import java.awt.Container;
import java.awt.Desktop;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.net.URI;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.ButtonGroup;
import javax.swing.Icon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextPane;

import cologne.eck.all_peas.control.PeaControl;
import cologne.eck.all_peas.data.PeaProperties;
import cologne.eck.tools.TestLog;




public class PeaDialog  {
	
	private static Icon icon = null;//new ImageIcon(PswDialogView.getImage());


	// ===== message dialog =====
	
	public static void showExitMessage(Object ownerComponent, String message, boolean unencryptedFilesLeft) {
		// program_bug = An unexpected error occurred during program execution
		// unselected_files_left = There are unselected files left. These files are left unencrypted:
		String title = PeaProperties.getVmBridge().translate("unexpected_error");
		String errorMessage = PeaProperties.getVmBridge().translate("program_bug") + "\n"
				+ message;
		if (unencryptedFilesLeft == true) {
			errorMessage = errorMessage + "\n\n" + PeaProperties.getVmBridge().translate("unselected_files_left");
			if (PeaControl.getDialog() != null && PeaControl.getDialog().getLockFrame() != null
					&& PeaControl.getDialog().getLockFrame() instanceof PeaLockFrame 
					&& ((PeaLockFrame) PeaControl.getDialog().getLockFrame()).getCurrentFileNames() != null) {
				String[] fileNames = ((PeaLockFrame) PeaControl.getDialog().getLockFrame()).getCurrentFileNames();
				for (String f : fileNames) {
					errorMessage = errorMessage + "\n" + f;
				}
			}
		}
		showMultiLineMessage(ownerComponent, errorMessage, title, 0);
		System.exit(1);
	}
	
	/**
	 * Show a message. 
	 * 
	 * @param ownerComponent	the Object will be casted to java.awtComponent
	 * @param message			the message, lines separated by \n
	 * @param title				the title of the message
	 * @param messageType		possible values are:
	 *     						ERROR_MESSAGE		0
     * 	 						INFORMATION_MESSAGE	1
     * 							WARNING_MESSAGE		2
     * 							QUESTION_MESSAGE	3
     * 							PLAIN_MESSAGE 		-1
	 */
	public static void showMessage(Object ownerComponent, 
			Object message, String title, int messageType){
		
		Component owner = null;
		try {
			owner = (Component) ownerComponent;
		} catch (NullPointerException npe) {
			TestLog.e(PeaDialog.class, "Missing PeaMessage owner");			
		} catch (ClassCastException cce) {
			TestLog.e(PeaDialog.class, "Invalid PeaMessage owner");	
		}
		if (message instanceof String) {
			String text = (String) message;
			
			if (text.contains("\n") == false && text.length() > 80 ){
				// avoid long lines, longer than screen size
				JTextArea textArea = new JTextArea(5,80);
				textArea.setText(text);
				textArea.setLineWrap(true);  
				textArea.setWrapStyleWord(true); 
				textArea.setEditable(false);
				JOptionPane.showMessageDialog(owner, textArea, null, -1, icon);
				return;
				
			} else if (text.split("\n").length > 14) {
				// more than 14 lines...	
				
				JTextArea textArea = new JTextArea((String)message);
				JScrollPane scrollPane = new JScrollPane(textArea);  
				textArea.setLineWrap(true);  
				textArea.setWrapStyleWord(true); 
				textArea.setEditable(false);
				
				int screenHeight = (int)PswDialogView.getScreenheight();
				int screenWidth = (int)PswDialogView.getScreenwidth();
				int w, h;
				if (textArea.getPreferredSize().getHeight() > (screenHeight - 200)) {
					h = screenHeight - 200;
				} else {
					h = (int) textArea.getPreferredSize().getHeight();
				}
				if (textArea.getPreferredSize().getWidth() > (screenWidth - 200)) {
					w = screenWidth - 200;
				} else {
					w = (int) textArea.getPreferredSize().getWidth();
				}
				scrollPane.setPreferredSize( new Dimension( w, h) );
				
				JOptionPane.showMessageDialog(owner, scrollPane, title, messageType, icon);
				return;
			}
		}
		JOptionPane.showMessageDialog(owner, message, title, messageType, icon);
	}
	
	/**
	 * Show a message in a dialog with specified size (scroll pane)
	 * 
	 * @param ownerComponent	the Object will be casted to java.awtComponent
	 * @param message			the message, lines separated by \n
	 * @param title				the title of the message
	 * @param messageType		possible values are:
	 *     						ERROR_MESSAGE		0
     * 	 						INFORMATION_MESSAGE	1
     * 							WARNING_MESSAGE		2
     * 							QUESTION_MESSAGE	3
     * 							PLAIN_MESSAGE 		-1
     * @param width		width of dialog to create
     * @param height	height of dialog to create
	 */
	public static void showMessageWithSize(Object ownerComponent, 
			String message, String title, int messageType, int width, int height){
		
		Component owner = null;
		try {
			owner = (Component) ownerComponent;
		} catch (NullPointerException npe) {
			TestLog.e(PeaDialog.class, "Missing PeaMessage owner");			
		} catch (ClassCastException cce) {
			TestLog.e(PeaDialog.class, "Invalid PeaMessage owner");	
		}
		//if (message instanceof String) {
			//if (((String) message).split("\n").length > 14) {
				// more than 14 lines...	
				JTextPane textArea = new JTextPane();
				textArea.setText(message);
				textArea.setEditable(false);
				JScrollPane  readmeScroll = new JScrollPane(textArea);
				readmeScroll.setPreferredSize(new Dimension (width, height));
				/*readme = readmeScroll;
				
				JTextArea textArea = new JTextArea((String)message);
				JScrollPane scrollPane = new JScrollPane(textArea);  
				//textArea.setLineWrap(true);  
				//textArea.setWrapStyleWord(true); 
				textArea.setEditable(false);*/
				
			/*	int screenHeight = (int)PswDialogView.getScreenheight();
				int screenWidth = (int)PswDialogView.getScreenwidth();
				int w, h;
				if (textArea.getPreferredSize().getHeight() > (screenHeight - 200)) {
					h = screenHeight - 200;
				} else {
					h = (int) textArea.getPreferredSize().getHeight();
				}
				if (textArea.getPreferredSize().getWidth() > (screenWidth - 200)) {
					w = screenWidth - 200;
				} else {
					w = (int) textArea.getPreferredSize().getWidth();
				}
				scrollPane.setPreferredSize( new Dimension( w, h) );*/
				
				JOptionPane.showMessageDialog(owner, readmeScroll, title, messageType, icon);
				//return;
			//}
		//}
		//JOptionPane.showMessageDialog(owner, message, title, messageType, icon);
	}

	/**
	 * Show a message with several lines in a text area
	 * without scroll pane. The text can be selected and copied. 
	 * 
	 * @param ownerComponent	the Object will be casted to java.awtComponent
	 * @param message			the message, lines separated by \n
	 * @param title				the title of the message
	 * @param messageType		possible values are:
	 *     						ERROR_MESSAGE		0
     * 	 						INFORMATION_MESSAGE	1
     * 							WARNING_MESSAGE		2
     * 							QUESTION_MESSAGE	3
     * 							PLAIN_MESSAGE 		-1
	 */
	public static void showMultiLineMessage(Object ownerComponent, 
			Object message, String title, int messageType){
		Component owner = null;
		try {
			owner = (Component) ownerComponent;
		} catch (NullPointerException npe) {
			TestLog.e(PeaDialog.class, "Missing PeaMessage owner");			
		} catch (ClassCastException cce) {
			TestLog.e(PeaDialog.class, "Invalid PeaMessage owner");	
		}
		
		JTextArea textArea = new JTextArea();
		textArea.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
		int[] plainVals = PeaProperties.getPlainPeaColor();
		textArea.setBackground(new Color(plainVals[0], plainVals[1], plainVals[2]));
		//textField.setLineWrap(true);
		message = ((String)message).replaceAll("\\\\n", System.getProperty("line.separator"));
		//textArea.setContentType("text/html");
		textArea.setText((String)message);
		textArea.setEditable(false);
		JOptionPane.showMessageDialog(owner, textArea, title, messageType, icon);
	}
	
	/**
	 * Show a message with several lines in a text area
	 * without scroll pane
	 * 
	 * @param ownerComponent	the Object will be casted to java.awtComponent
	 * @param message			the message, lines separated by \n
	 * @param title				the title of the message
	 * @param messageType		possible values are:
	 *     						ERROR_MESSAGE		0
     * 	 						INFORMATION_MESSAGE	1
     * 							WARNING_MESSAGE		2
     * 							QUESTION_MESSAGE	3
     * 							PLAIN_MESSAGE 		-1
	 */
	public static void showMessageWithLink(Object ownerComponent, 
			Object mainMessage, String[][] messagesAndLinks, String title, int messageType){
		Component owner = null;
		try {
			owner = (Component) ownerComponent;
		} catch (NullPointerException npe) {
			TestLog.e(PeaDialog.class, "Missing PeaMessage owner");			
		} catch (ClassCastException cce) {
			TestLog.e(PeaDialog.class, "Invalid PeaMessage owner");	
		}
		
		JPanel panel = new JPanel();
		panel.setLayout(new BoxLayout(panel, BoxLayout.PAGE_AXIS));
		
		JTextArea textArea = new JTextArea();
		textArea.setBorder(null);
		int[] plainVals = PeaProperties.getPlainPeaColor();
		textArea.setBackground(new Color(plainVals[0], plainVals[1], plainVals[2]));
		mainMessage = ((String)mainMessage).replaceAll("\\\\n", System.getProperty("line.separator"));
		textArea.setText((String)mainMessage);
		textArea.setEditable(false);
		textArea.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
		panel.add(textArea);
		panel.add(Box.createVerticalStrut(10));
		
		ActionListener al = new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent ae) {
				String command = ae.getActionCommand();
				URI uri = null;
				try {
					uri = new URI(command);
					Desktop.getDesktop().browse(uri);
				} catch (Exception e) {
					TestLog.ve(PeaDialog.class, e.getMessage());
					Object button = ae.getSource();
					if (button != null && button instanceof JButton) {
						((JButton) button).setEnabled(false);
						// show error on label:
						Container cont = ((JButton) button).getParent();
						if (cont != null && cont instanceof JPanel) {			
							Component label = cont.getComponent(0);
							if (label != null && label instanceof JLabel) {
								label.setForeground(Color.RED);
								((JLabel) label).setText(PeaProperties.getVmBridge().translate("unexpected_error"));
							} else {
								//TestLog.o(PeaDialog.class, "label not found: " + label.getClass().getSimpleName());
							}
						} else {
							//TestLog.o(PeaDialog.class, "container not found");
						}
					} else {
						//TestLog.o(PeaDialog.class, "button not found");
					}
				} 
			}				
		};
		
		for (String[] messageAndLink : messagesAndLinks) {
			JPanel mlPanel = new JPanel();
			mlPanel.setLayout(new BoxLayout(mlPanel, BoxLayout.PAGE_AXIS));
			JLabel label = new JLabel(messageAndLink[0]);
			mlPanel.add(label);
			JButton linkLabel = new JButton();
			linkLabel.setForeground(Color.BLUE.darker());
			//linkLabel.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
			linkLabel.setText(messageAndLink[1]);
			
			linkLabel.addActionListener(al);
			linkLabel.setActionCommand(messageAndLink[1]);
			mlPanel.add(linkLabel);
			panel.add(mlPanel);
		}

		JOptionPane.showMessageDialog(owner, panel, title, messageType, icon);
	}
	
	/**
	 * Show a plain type message without title. 
	 * 
	 * @param ownerComponent	the Object will be casted to java.awtComponent
	 * @param message			the message, lines separated by \n
	 */
	public static void showMessage(Object ownerComponent, 
			Object message){
		
		Component owner = null;
		try {
			owner = (Component) ownerComponent;
		} catch (NullPointerException npe) {
			TestLog.e(PeaDialog.class, "Missing PeaMessage owner");			
		} catch (ClassCastException cce) {
			TestLog.e(PeaDialog.class, "Invalid PeaMessage owner");	
		}
		if (message instanceof String) {
			String text = (String) message;
			
			if (text.contains("\n") == false && text.length() > 80 ){
				
				JTextArea textArea = new JTextArea(5,80);
				textArea.setText(text);
				textArea.setLineWrap(true);  
				textArea.setWrapStyleWord(true); 
				textArea.setEditable(false);
				JOptionPane.showMessageDialog(owner, textArea, null, -1, icon);
				return;
				
			} else if (text.split("\n").length > 14) {
				
				// more than 10 lines...	
				
				JTextArea textArea = new JTextArea((String)message);
				JScrollPane scrollPane = new JScrollPane(textArea);  
				//textArea.setLineWrap(true);  
				//textArea.setWrapStyleWord(true); 
				textArea.setEditable(false);
				
				int screenHeight = (int)PswDialogView.getScreenheight();
				int screenWidth = (int)PswDialogView.getScreenwidth();
				int w, h;
				if (textArea.getPreferredSize().getHeight() > (screenHeight - 200)) {
					h = screenHeight - 200;
				} else {
					h = (int) textArea.getPreferredSize().getHeight() + 50;
				}
				if (textArea.getPreferredSize().getWidth() > (screenWidth - 100)) {
					w = screenWidth - 100;
				} else {
					w = (int) textArea.getPreferredSize().getWidth() + 50;
				}
				scrollPane.setPreferredSize( new Dimension( w, h) );
				
				JOptionPane.showMessageDialog(owner, scrollPane, null, -1, icon);
				return;
			}
		}
		JOptionPane.showMessageDialog(owner, message, null, -1, icon);
	}	
	
	// ===== confirm dialog =====
	
	/**
	 * Show a question dialog. 
	 * 
	 * @param ownerComponent	the Object will be casted to java.awtComponent
	 * @param message			the message, lines separated by \n
	 * @param title				the title of the message
	 * @param optionType	possible values are:
	 * 						DEFAULT_OPTION			-1
     *						YES_NO_OPTION			0
     *						YES_NO_CANCEL_OPTION	1
     *						OK_CANCEL_OPTION 		2			   
	 * @param messageType	possible values are:
	 *     					ERROR_MESSAGE		0
     * 	 					INFORMATION_MESSAGE	1
     * 						WARNING_MESSAGE		2
     * 						QUESTION_MESSAGE	3
     * 						PLAIN_MESSAGE 		-1
     * 
     * @result	YES_OPTION		0
    			NO_OPTION		1
   				CANCEL_OPTION	2
    			OK_OPTION		0
    			CLOSED_OPTION 	-1
	 */
	public static int showQuestion(Object ownerComponent, Object message, String title, int optionType, int messageType){
		
		Component owner = null;
		try {
			owner = (Component) ownerComponent;
		} catch (NullPointerException npe) {
			TestLog.e(PeaDialog.class, "Missing PeaMessage owner");			
		} catch (ClassCastException cce) {
			TestLog.e(PeaDialog.class, "Invalid PeaMessage owner");	
		}
		
		return JOptionPane.showConfirmDialog(owner, message, title, optionType, messageType, icon);
	}
	
	/**
	 * Show a question dialog. 
	 * 
	 * @param ownerComponent	the Object will be casted to java.awtComponent
	 * @param message			the message, lines separated by \n
	 * @param title				the title of the message
	 * @param optionType	possible values are:
	 * 						DEFAULT_OPTION			-1
     *						YES_NO_OPTION			0
     *						YES_NO_CANCEL_OPTION	1
     *						OK_CANCEL_OPTION 		2			   
	 * @param messageType	possible values are:
	 *     					ERROR_MESSAGE		0
     * 	 					INFORMATION_MESSAGE	1
     * 						WARNING_MESSAGE		2
     * 						QUESTION_MESSAGE	3
     * 						PLAIN_MESSAGE 		-1
     * 
     * @result	YES_OPTION		0
    			NO_OPTION		1
   				CANCEL_OPTION	2
    			OK_OPTION		0
    			CLOSED_OPTION 	-1
	 */
	public static int showQuestionWithScollPane(Object ownerComponent, Object message, String title, int optionType, int messageType){
		
		Component owner = null;
		try {
			owner = (Component) ownerComponent;
		} catch (NullPointerException npe) {
			TestLog.e(PeaDialog.class, "Missing PeaMessage owner");			
		} catch (ClassCastException cce) {
			TestLog.e(PeaDialog.class, "Invalid PeaMessage owner");	
		}
		
		JTextArea textArea = new JTextArea((String)message);
		JScrollPane scrollPane = new JScrollPane(textArea);  
		//textArea.setLineWrap(true);  
		//textArea.setWrapStyleWord(true); 
		textArea.setEditable(false);
		
		int screenHeight = (int)PswDialogView.getScreenheight();
		int screenWidth = (int)PswDialogView.getScreenwidth();
		int areaW = (int)textArea.getPreferredSize().getWidth();
		int areaH = (int)textArea.getPreferredSize().getHeight();
		//TestLog.o(XXXX.class, screenHeight + ", " + screenWidth + ", " + areaH + ", " + areaW);
		int w = 500, h = 200;
		if (areaH > (screenHeight - 200)) {
			h = screenHeight - 200;
		} else {
			h = (int) areaH + 50;
		}
		if (areaW > (screenWidth - 200)) {
			w = screenWidth - 200;
		} else {
			w = (int) areaW + 50;
		}
		scrollPane.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		scrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
		scrollPane.setPreferredSize( new Dimension( w, h) );
		
		return JOptionPane.showConfirmDialog(owner, scrollPane, title, optionType, messageType, icon);
	}
	
	/**
	 * Show a plain type question dialog with default options
	 * and without title. 
	 * 
	 * @param ownerComponent	the Object will be casted to java.awtComponent
	 * @param message			the message, lines separated by \n
	 * 
     * 
     * @result	YES_OPTION		0
    			NO_OPTION		1
   				CANCEL_OPTION	2
    			OK_OPTION		0
    			CLOSED_OPTION 	-1
	 */
	public static int showQuestion(Object ownerComponent, Object message){
		
		Component owner = null;
		try {
			owner = (Component) ownerComponent;
		} catch (NullPointerException npe) {
			TestLog.e(PeaDialog.class, "Missing PeaMessage owner");			
		} catch (ClassCastException cce) {
			TestLog.e(PeaDialog.class, "Invalid PeaMessage owner");	
		}		
		return JOptionPane.showConfirmDialog(owner, message, null, -1, -1, icon);
	}
	
	/**
	 * Show a plain type question dialog. 
	 * 
	 * @param ownerComponent	the Object will be casted to java.awtComponent
	 * @param message			the message, lines separated by \n
	 * @param title				the title of the message
	 * @param optionType	possible values are:
	 * 						DEFAULT_OPTION			-1
     *						YES_NO_OPTION			0
     *						YES_NO_CANCEL_OPTION	1
     *						OK_CANCEL_OPTION 		2	
     *
     * 
     * @result	YES_OPTION		0
    			NO_OPTION		1
   				CANCEL_OPTION	2
    			OK_OPTION		0
    			CLOSED_OPTION 	-1		   
	 */
	public static int showQuestion(Object ownerComponent, Object message, String title, int optionType){
		
		Component owner = null;
		try {
			owner = (Component) ownerComponent;
		} catch (NullPointerException npe) {
			TestLog.e(PeaDialog.class, "Missing PeaMessage owner");			
		} catch (ClassCastException cce) {
			TestLog.e(PeaDialog.class, "Invalid PeaMessage owner");	
		}		
		return JOptionPane.showConfirmDialog(owner, message, title, optionType, -1, icon);
	}	
	
	// ===== option dialog =====
	
	/**
	 * Show an option dialog.
	 * 
	 * 
	 * @param ownerComponent	the Object will be casted to java.awtComponent
	 * @param message			the message, lines separated by \n
	 * @param title				the title of the message			   
	 * @param messageType	possible values are:
	 *     					ERROR_MESSAGE		0
     * 	 					INFORMATION_MESSAGE	1
     * 						WARNING_MESSAGE		2
     * 						QUESTION_MESSAGE	3
     * 						PLAIN_MESSAGE 		-1
	 * @param options		options, at least two (Object[] - String[])
	 * @param initialValue	initial value of options, 
	 * 						one Object of the options array e.g.options[0]
	 * 
     * 
     * @result	options index or
    			CLOSED_OPTION 	-1
	 */
	public static int showOptionYAxis(Object ownerComponent, String message, String title, int messageType, String[] options, int initialValue){
		
		Component owner = null;
		try {
			owner = (Component) ownerComponent;
		} catch (NullPointerException npe) {
			TestLog.e(PeaDialog.class, "Missing PeaMessage owner");			
		} catch (ClassCastException cce) {
			TestLog.e(PeaDialog.class, "Invalid PeaMessage owner");	
		}			
		
		JPanel p = new JPanel();
		p.setLayout(new BoxLayout(p, BoxLayout.PAGE_AXIS));
		JTextArea textField = new JTextArea();		
		int[] plainVals = PeaProperties.getPlainPeaColor();
		textField.setBorder(//null);
				BorderFactory.createCompoundBorder(BorderFactory.createLineBorder(new Color(plainVals[0], plainVals[1], plainVals[2])),
		                BorderFactory.createEmptyBorder(10, 10, 10,10)));
		
		textField.setBackground(new Color(plainVals[0], plainVals[1], plainVals[2]));
		//textField.setLineWrap(true);
		message = message.replaceAll("\\\\n", System.getProperty("line.separator"));
		textField.setText(message);
		textField.setEditable(false);
		p.add(textField);
		p.add(Box.createVerticalStrut(10));
			
	    ButtonGroup group = new ButtonGroup();
	    
		for (int i = 0; i < options.length; i++) {
			JPanel radioPanel = new JPanel();
			radioPanel.setLayout(new BoxLayout(radioPanel, BoxLayout.LINE_AXIS));
			JRadioButton rb = new JRadioButton(options[i]);
			rb.setFont(new Font(Font.SANS_SERIF, Font.BOLD, PeaProperties.getFontSize() ));
			if (i == initialValue) {
				rb.setSelected(true); // select initial value
			}
//			p.add(rb);
			radioPanel.add(rb);
			radioPanel.add(Box.createHorizontalGlue());
			p.add(radioPanel);
			group.add(rb);
		}

		//JOptionPane.showMessageDialog(owner, p, title, messageType, icon);
		int x = JOptionPane.showConfirmDialog(owner,p, null, -1, messageType, icon);
		if (x == -1 || x == 2) { // cancel, close
			return -1;
		}
		// get selected text index
		int selectedTextIndex = -1;
		// note: this also contains the label at position 0 and Box at 1
		Component[] elements = p.getComponents();
		for (int i = 0; i < elements.length; i++) {
			if (elements[i] instanceof JPanel) {
				Component[] panelComponents = ((JPanel) elements[i]).getComponents();
				if (panelComponents[0] instanceof JRadioButton) {
					if ( ((JRadioButton) panelComponents[0]).isSelected() ){
						selectedTextIndex = i - 2;// the label is on position 0,Box on 1
						break;
					}
				}
			}
		}
		return selectedTextIndex;
	}
	
	/**
	 * Show an option dialog.
	 * 
	 * 
	 * @param ownerComponent	the Object will be casted to java.awtComponent
	 * @param message			the message, lines separated by \n
	 * @param title				the title of the message
	 * @param optionType	possible values are:
	 * 						DEFAULT_OPTION			-1
     *						YES_NO_OPTION			0
     *						YES_NO_CANCEL_OPTION	1
     *						OK_CANCEL_OPTION 		2			   
	 * @param messageType	possible values are:
	 *     					ERROR_MESSAGE		0
     * 	 					INFORMATION_MESSAGE	1
     * 						WARNING_MESSAGE		2
     * 						QUESTION_MESSAGE	3
     * 						PLAIN_MESSAGE 		-1
	 * @param options		options, at least two (Object[] - String[])
	 * @param initialValue	initial value of options, 
	 * 						one Object of the options array e.g.options[0]
	 * 
     * 
     * @result	options index or
    			CLOSED_OPTION 	-1
	 */
	public static int showOptions(Object ownerComponent, Object message, String title, int optionType, int messageType, Object[] options, Object initialValue){
		
		Component owner = null;
		try {
			owner = (Component) ownerComponent;
		} catch (NullPointerException npe) {
			TestLog.e(PeaDialog.class, "Missing PeaMessage owner");			
		} catch (ClassCastException cce) {
			TestLog.e(PeaDialog.class, "Invalid PeaMessage owner");	
		}			
		if (message instanceof String) {
			String text = (String) message;
			
			if (text.contains("\n") == false && text.length() > 80 ){
				// avoid long lines, longer than screen size
				JTextArea textArea = new JTextArea(5,80);
				textArea.setText(text);
				textArea.setLineWrap(true);  
				textArea.setWrapStyleWord(true); 
				textArea.setEditable(false);
				//JOptionPane.showMessageDialog(owner, textArea, null, -1, icon);
				return JOptionPane.showOptionDialog(owner, textArea, title, optionType, messageType, icon, options, initialValue);
			}
		}
		return JOptionPane.showOptionDialog(owner, message, title, optionType, messageType, icon, options, initialValue);
	}
	
	/**
	 * Show an input dialog.
	 * 
	 * 
	 * @param ownerComponent	the Object will be casted to java.awtComponent
	 * @param message			the message, lines separated by \n
	 * @param title				the title of the message
	 * @param messageType	possible values are:
	 *     					ERROR_MESSAGE		0
     * 	 					INFORMATION_MESSAGE	1
     * 						WARNING_MESSAGE		2
     * 						QUESTION_MESSAGE	3
     * 						PLAIN_MESSAGE 		-1 
     * 
     * @result	input as String
	 */
	public static String showInputDialog(Object ownerComponent, Object message, String title, int messageType) {
		
		Component owner = null;
		try {
			owner = (Component) ownerComponent;
		} catch (NullPointerException npe) {
			TestLog.e(PeaDialog.class, "Missing PeaMessage owner");			
		} catch (ClassCastException cce) {
			TestLog.e(PeaDialog.class, "Invalid PeaMessage owner");	
		}
		
		Object obj =  JOptionPane.showInputDialog(owner, message, title, messageType, icon, null, null);
		if (obj == null) {
			return null;
		} else {
			if (obj instanceof String) {
				String result = (String) obj;
				if (result.length() == 0) {
					return null;
				} else {
					return result;
				}
				
			} else {
				TestLog.ve(PeaDialog.class, "Cannot cast input to string..." + obj.toString(), 5);
				return null;
			}
		}
	}
	
	/**
	 * Shows a password field dialog with the label "enter password".
	 * This returns the String "cAnCeLlEd".toCharArray(), if the dialog was cancelled or closed. 
	 * Always check this return value with if Comparator.compare(psw, "cAnCeLlEd".toCharArray())
	 * 
	 * @param ownerComponent	owner Component
	 * @param message			additional message, not required (enter password is shown)
	 * @param title				title of the dialog
	 * @param peaPassword		true if used for PEA (null password is "no password".toCharArray())
	 * 
	 * @return	the password as an array of chars
	 */
	public static char[] showPasswordDialog(Object ownerComponent, 
			String message, String title, boolean peaPassword) {
		//TestLog.v(PeaDialog.class, "New password with text: " + message, 5);
		JPanel panel = new JPanel();
		panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
		
		JPanel messageLabelPanel = new JPanel();
		messageLabelPanel.setLayout(new BoxLayout(messageLabelPanel, BoxLayout.X_AXIS));
		JLabel messageLabel = new JLabel(message);
		messageLabel.setFont(new Font(Font.SANS_SERIF, Font.PLAIN, PeaProperties.getFontSize()));
		messageLabel.setPreferredSize(new Dimension(300,30));
		messageLabelPanel.add(messageLabel);
		messageLabelPanel.add(Box.createHorizontalGlue());
		panel.add(messageLabelPanel);
		panel.add(Box.createVerticalStrut(5));
		
		JPanel pswLabelPanel = new JPanel();
		pswLabelPanel.setLayout(new BoxLayout(pswLabelPanel, BoxLayout.X_AXIS));		
		JLabel pswLabel = new JLabel(PeaProperties.getVmBridge().translate("enter_password"));				
		pswLabel.setFont(new Font(Font.SANS_SERIF, Font.PLAIN, PeaProperties.getFontSize()));
		pswLabelPanel.add(pswLabel);	
		pswLabelPanel.add(Box.createHorizontalGlue());
		panel.add(pswLabelPanel);

		@SuppressWarnings("serial")
		JPasswordField passwordField = new JPasswordField() {
            public void addNotify()
            {
                super.addNotify();
                requestFocus();
                requestFocusInWindow();             
            }
        };
        pswLabel.setLabelFor(passwordField);
		//passwordField.setFocusable(true);
		passwordField.requestFocusInWindow();
		panel.add(passwordField);
		
		int option = JOptionPane.showConfirmDialog((Component)ownerComponent, panel, title,
                JOptionPane.OK_CANCEL_OPTION, JOptionPane.PLAIN_MESSAGE);
		if (option == 0) {
			if (peaPassword == true) {
				if (passwordField.getPassword() == null) {
					return "no password".toCharArray();
				}
			}
			return passwordField.getPassword();
		} else {
			return null;
		}
	}
}