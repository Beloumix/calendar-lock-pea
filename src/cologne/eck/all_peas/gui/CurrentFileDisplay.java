package cologne.eck.all_peas.gui;

/*
 * Peafactory - Production of Password Encryption Archives
 * Copyright (C) 2020  Axel von dem Bruch
 * 
 * This library is free software; you can redistribute it and/or modify it 
 * under the terms of the GNU General Public License as published 
 * by the Free Software Foundation; either version 2 of the License, 
 * or (at your option) any later version.
 * This library is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
 * or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU General Public License for more details.
 * See:  http://www.gnu.org/licenses/gpl-2.0.html
 * You should have received a copy of the GNU General Public License 
 * along with this library.
 */

import java.awt.Component;
import java.awt.Dimension;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JToggleButton;
import javax.swing.KeyStroke;

import cologne.eck.all_peas.cloud.CloudControl;
import cologne.eck.all_peas.data.PeaProperties;
import cologne.eck.tools.Converter;
import cologne.eck.tools.ExceptionHelper;
import cologne.eck.tools.TestLog;

@SuppressWarnings("serial")
public class CurrentFileDisplay extends JDialog implements ActionListener {
	
	
	/**
	 * The panel that holds the file names and check boxes or radio buttons
	 */
	private JPanel filePanel;
	
	/**
	 * The selected files
	 */
	private String[] selectedFileNames;
	
	/**
	 * File names that failed and reasons (messages)
	 */
	private HashSet<String> failedFiles = new HashSet<String>();
	
	/**
	 * Display of current files to select:
	 * Equal file names are grouped together as radio buttons, 
	 * otherwise, file names are shown as check boxes.
	 * Call getSelectedFileNames and getFailedFiles
	 * 
	 * @param provider		the provider to use
	 * @param fileNames 	the file names with path to display (cloud backup files with tmp directory)
	 */	
	public CurrentFileDisplay(Window win, String[] fileNames) {
		
		super(win);
		this.setModal(true);

		this.setIconImage(PswDialogView.getImage());
		this.setMinimumSize(new Dimension(400,400));
		this.setTitle(PeaProperties.getVmBridge().translate("files"));
		
		// Avoid keyboard trap: Close with ESC
		this.getRootPane().getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW)
    		.put(KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), "CLOSE_ON_ESCAPE");
		this.getRootPane().getActionMap().put("CLOSE_ON_ESCAPE", new CloseOnEscapeAction() );
		
		JPanel contentPane = (JPanel) this.getContentPane();
		contentPane.setLayout(new BoxLayout(contentPane, BoxLayout.PAGE_AXIS));
		
		filePanel = new JPanel();
		JScrollPane scroll = new JScrollPane(filePanel);
		scroll.setPreferredSize(new Dimension(400, 400));
		filePanel.setLayout(new BoxLayout(filePanel,BoxLayout.PAGE_AXIS));
		//contentPane.add(Box.createHorizontalStrut(10));
		if (fileNames == null || fileNames.length == 0) {
			// message as label
			contentPane.add(new JLabel(PeaProperties.getVmBridge().translate("no_valid_file_found")));
		} else {
			
			HashMap<String, HashSet<String>> sortedMap = sortByFileNames(fileNames);
			
			for(HashMap.Entry<String, HashSet<String>>  entry: sortedMap.entrySet()){
	//			System.out.print(entry.getKey() + " : " + entry.getValue());
				
				HashSet<String> set = entry.getValue();
				int setSize = set.size();
				if (setSize == 1) {
					String fileNameWithPath = set.iterator().next();
					if (fileNameWithPath.contains(CloudControl.getTmpDir())) {
						fileNameWithPath = CloudControl.getProviderNameFromTmpFile(fileNameWithPath)
								+ ": " + CloudControl.getFileNameFromTmpFile(fileNameWithPath);
					}
					JCheckBox check = new JCheckBox(fileNameWithPath);
					check.getAccessibleContext().
				    setAccessibleDescription(
				    	    "Clicking this component selects this file.");
					filePanel.add(check);
				} else if (setSize > 1) {
					// add RadioButton Group
					ButtonGroup group = new ButtonGroup();
					filePanel.add(Box.createVerticalStrut(5));// small space before group
					for (String fileNameWithPath : set) {
						if (fileNameWithPath.contains(CloudControl.getTmpDir())) {
							fileNameWithPath = CloudControl.getProviderNameFromTmpFile(fileNameWithPath)
									+ ": " + CloudControl.getFileNameFromTmpFile(fileNameWithPath);
						}
						// create RadioButton
						JRadioButton fButton = new JRadioButton(fileNameWithPath);
						fButton.getAccessibleContext().
					    setAccessibleDescription(
					    	    "Clicking this component selects this file.");
						// add to group
						group.add(fButton);
						// add to panel:
						filePanel.add(fButton);
					}
					filePanel.add(Box.createVerticalStrut(5));// small space after group
				}
			}
		}
		contentPane.add(scroll);
		
		JPanel buttonPanel = new JPanel();
		buttonPanel.setLayout(new BoxLayout(buttonPanel,BoxLayout.LINE_AXIS));
		JButton okButton = new JButton("OK");
		okButton.getAccessibleContext().
	    setAccessibleDescription(
	    	    "Clicking this component takes the selection and closes this dialog.");
		okButton.addActionListener(this);
		okButton.setActionCommand("ok");
		buttonPanel.add(okButton);
		buttonPanel.add(Box.createHorizontalGlue());
		contentPane.add(buttonPanel);
		
		this.setLocation(win.getLocationOnScreen());
		this.setVisible(true);
	}

	private HashMap<String, HashSet<String>> sortByFileNames(String[] fileNames){
		// return value
		HashMap<String,  HashSet<String>> fileMap = new HashMap<String, HashSet<String>>();
		failedFiles.clear(); // reset failed files
		for (String fileNameWithPath : fileNames) {
			
			if (fileNameWithPath.contains(": ")) { // cloud file will contain a :
				TestLog.ve(getClass(),"File does not exist: "+ fileNameWithPath, 5);
				failedFiles.add(fileNameWithPath + " - " +  PeaProperties.getVmBridge().translate("format")
						+ ":  file name contains invalid character :");
				continue;
			}
			File f = new File(fileNameWithPath);
			if ( ! f.exists()) {
				TestLog.ve(getClass(),"File does not exist: "+ fileNameWithPath, 5);
				failedFiles.add(fileNameWithPath  + " - " +   PeaProperties.getVmBridge().translate("file_not_found"));
				continue;
			}
			String fileNameOnly = null;
			try {
				fileNameOnly = f.getCanonicalFile().getName();
			} catch (IOException e) {
				TestLog.e(CurrentFileDisplay.class, new ExceptionHelper(e).getInfos());
				failedFiles.add(fileNameWithPath  + " - " +   e.getLocalizedMessage());
				continue;
			}
			//TestLog.o(XXXX.class, "canonical file name: " + fileNameOnly);
			if (fileMap.containsKey(fileNameOnly)) {
				HashSet<String> set = fileMap.get(fileNameOnly);
				set.add(fileNameWithPath);
			} else {
				HashSet<String> set = new HashSet<String>();
				set.add(fileNameWithPath);
				fileMap.put(fileNameOnly, set);
			}		
		}// end for
		return fileMap;		
	}

	@Override
	public void actionPerformed(ActionEvent e) {

		if (e.getActionCommand().equals("ok")) {
			Component[] comps = filePanel.getComponents();
			if (comps == null || comps.length == 0) {
				this.setVisible(false);
				this.dispose();
				return;
			} else {
				int len = comps.length;
				ArrayList<String> list = new ArrayList<String>();
				for (int i = 0; i < len; i++) {
					if (comps[i] instanceof JToggleButton) { // check boxes and radio buttons
						JToggleButton toggle = (JToggleButton) comps[i];
						if (toggle.isSelected()) {
							String text = toggle.getText();							
							if (text == null || text.length() == 0) {							
								TestLog.ve(getClass(),"String is null", 5);
							} else {
								if (text.contains(": ")) { // cloud file
									String[] parts = text.split(": ");
									if (parts.length != 2) {
										TestLog.ve(getClass(),"String[] has invalid size: " + parts.length, 5);
										continue;
									}
									text = CloudControl.getTmpFileNameForCloudFile( parts[1], parts[0], null);
									list.add(text);
								} else {
									// text is file name
									list.add(text);
								}
							}
						}
					}
				}
				if (list.size() > 0) {
					selectedFileNames = list.toArray(new String[list.size()]);
				}
		        //for(int i=0; i<selectedFileNames.length;i++) TestLog.o(XXXX.class, "selected: " + selectedFileNames[i]);;
			}
			this.setVisible(false);
			this.dispose();
		}
	}
	
	/**
	 * Get the selected file names in the default folder
	 * 
	 * @return  the selected file names in the default folder PeaFactory
	 * 			without the default folder name
	 */
	public String[] getSelectedFileNames() {
		return selectedFileNames;
	} 
	
	/**
	 * Get the failed files with (localized) failure reasons 
	 * in one line format: 
	 * file name with path - reason
	 * @return
	 */
	public String[] getFailedFiles() {		
		if (failedFiles.isEmpty()) {
			return null;
		} else {
			return Converter.hashSetToArray(failedFiles);
		}
	}
}
