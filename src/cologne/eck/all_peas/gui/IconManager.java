package cologne.eck.all_peas.gui;

/*
 * PeaFactory - Production of Password Encryption Archives
 * Copyright (C) 2015  Axel von dem Bruch
 * 
 * This library is free software; you can redistribute it and/or modify it 
 * under the terms of the GNU General Public License as published 
 * by the Free Software Foundation; either version 2 of the License, 
 * or (at your option) any later version.
 * This library is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
 * or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU General Public License for more details.
 * See:  http://www.gnu.org/licenses/gpl-2.0.html
 * You should have received a copy of the GNU General Public License 
 * along with this library.
 */
import java.awt.Image;
import java.awt.Toolkit;
import java.io.File;


import javax.swing.ImageIcon;

import cologne.eck.tools.TestLog;

public class IconManager {
	
	/**
	 * Load an ImageIcon for javax swing components
	 * from an image file. The image file can be inside 
	 * the jar archive or in the file system, but must
	 * be in the directory resources.
	 * 
	 * @param imageFileName		the file name of the image file
	 * 							in the directory resources
	 * 							( png, gif,jpg, bmp)
	 * @param description		the description of the icon
	 * @param accessibleName	name of the icon, e.g. for icon-only 
	 * 							buttons
	 * @param accessibleDescription		the description of the icon, 
	 * 									describing the purpose of the object
	 * 
	 * @return	the ImageIcon loaded from the file
	 */
	public static ImageIcon loadIcon(String imageFileName, String description,
			String accessibleName, String accessibleDescription) {
		
		ImageIcon icon = null;
		// first try to load from jar archive
		try {
			icon = new ImageIcon(
					IconManager.class.getClassLoader().getResource("resources/"+ imageFileName));//, 
			//		description);
		} catch (Exception e) {
			// try to load from file system: 
			icon = new ImageIcon("resources" + File.separator + imageFileName);
			// check success:
			if (icon.getImageLoadStatus() == 4){// MediaTracker.ERRORED
				icon = new ImageIcon(Toolkit.getDefaultToolkit().createImage("resources/" + imageFileName));
				// check success:
				if (icon.getImageLoadStatus() == 4){// MediaTracker.ERRORED
					// try to load from jar
					java.net.URL imgURL = IconManager.class.getResource("/resources/"+ imageFileName);
					if (imgURL != null) {
						icon = new ImageIcon(imgURL);
					} else {
						icon = null;
					}
				}
			}
		}
		if (icon == null) {
			TestLog.ve(IconManager.class,"is null", 5);
			return null;
		} else if (icon.getImageLoadStatus() == 4){// MediaTracker.ERRORED
			TestLog.ve(IconManager.class,"ImageIcon s not loaded", 5);
			return null;
		} else {
			icon.setDescription(description);
		}
		if (accessibleName != null) {
			icon.getAccessibleContext().setAccessibleDescription(accessibleName);
		}
		if (accessibleDescription != null) {
			icon.getAccessibleContext().setAccessibleDescription(accessibleDescription);
		}
		return icon;
	}
	
	/**
	 * Load a scaled ImageIcon for javax swing components
	 * from an image file. The image file can be inside 
	 * the jar archive or in the file system, but must
	 * be in the directory resources.
	 * 
	 * @param imageFileName		the file name of the image file
	 * 							in the directory resources
	 * 							( png, gif,jpg, bmp)
	 * @param description		the description of the icon
	 * @param width				the intended width if the icon
	 * @param height			the intended height of the icon
	 * @param accessibleName	name of the icon, e.g. for icon-oly 
	 * 							buttons
	 * @param accessibleDescription		the description of the icon, 
	 * 									describing the purpose of the object
	 * 
	 * @return	the ImageIcon loaded from the file
	 */
	public static ImageIcon loadScaledIcon(String imageFileName, String description, 
			int width, int height,
			String accessibleName, String accessibleDescription) {
		
		ImageIcon icon = null;
		// first try to load from jar archive
		try {
			icon = new ImageIcon(
					IconManager.class.getClassLoader().getResource("resources/"+ imageFileName));//, 
			//		description);
		} catch (Exception e) {
			// try to load from file system: 
			icon = new ImageIcon("resources" + File.separator + imageFileName);
			// check success:
			if (icon.getImageLoadStatus() == 4){// MediaTracker.ERRORED
				icon = new ImageIcon(Toolkit.getDefaultToolkit().createImage("resources/" + imageFileName));
				// check success:
				if (icon.getImageLoadStatus() == 4){// MediaTracker.ERRORED
					// try to load from jar
					java.net.URL imgURL = IconManager.class.getResource("/resources/"+ imageFileName);
					if (imgURL != null) {
						icon = new ImageIcon(imgURL);
					} else {
						icon = null;
					}
				}
			}
		}
		if (icon == null) {
			TestLog.ve(IconManager.class,"is null", 5);
			return null;
		} else if (icon.getImageLoadStatus() == 4){// MediaTracker.ERRORED
			TestLog.ve(IconManager.class, "ImageIcon is not loaded", 5);
			return null;
		} else {
			icon.setDescription(description);
		}
		Image image = icon.getImage().getScaledInstance(width,height,Image.SCALE_DEFAULT);
		icon = new ImageIcon(image);
		if (accessibleName != null) {
			icon.getAccessibleContext().setAccessibleDescription(accessibleName);
		}
		if (accessibleDescription != null) {
			icon.getAccessibleContext().setAccessibleDescription(accessibleDescription);
		}
		return icon;
	}
}
