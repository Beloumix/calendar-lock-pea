package cologne.eck.all_peas.gui;


/*
 * Peafactory - Production of Password Encryption Archives
 * Copyright (C) 2021  Axel von dem Bruch
 * 
 * This library is free software; you can redistribute it and/or modify it 
 * under the terms of the GNU General Public License as published 
 * by the Free Software Foundation; either version 2 of the License, 
 * or (at your option) any later version.
 * This library is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
 * or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU General Public License for more details.
 * See:  http://www.gnu.org/licenses/gpl-2.0.html
 * You should have received a copy of the GNU General Public License 
 * along with this library.
 */

/**
 * Settings for crypto
 */


import java.awt.Window;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;

import javax.swing.Box;
import javax.swing.BoxLayout;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JDialog;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.KeyStroke;

import org.bouncycastle.crypto.BlockCipher;
import org.bouncycastle.crypto.Digest;
import org.bouncycastle.crypto.digests.*;
import org.bouncycastle.crypto.engines.*;

import cologne.eck.all_peas.control.PeaControl;
import cologne.eck.all_peas.data.PeaProperties;
import cologne.eck.all_peas.gui.kdf_setting.*;
import cologne.eck.all_peas.gui.menu.SettingMenu;
import cologne.eck.all_peas.vm_specific.JREProperties;
import cologne.eck.peafactory.crypto.AlgoParamHandler;
import cologne.eck.peafactory.crypto.CipherStuff;
import cologne.eck.peafactory.crypto.HashStuff;
import cologne.eck.peafactory.crypto.kdf.*;
import cologne.eck.tools.PropertyHandler;
import cologne.eck.tools.TestLog;
import settings.PeaSettings;


@SuppressWarnings("serial")
public class CryptoSetter extends JDialog implements ActionListener {	

	// this instance
	private CryptoSetter setting;
	
	private JComboBox<String> cipherCombo;
	private JComboBox<String> hashCombo;
	private JComboBox<String> kdfCombo;	
	
	// show or hide panel for KDF parameters and instances
	private JCheckBox advancedCheck;
	// holder panel for panel for parameters and instances of the KDF
	private JPanel kdfAdvancedPanelHolder;
	// panel for parameters and instances of the KDF
	private JPanel advancedPanel;	

	// show error messages
	private JLabel errorLabel;
	
	// The owner of this dialog: Whether PswDialogView or PeaLockFrame
	private Window owner;
	
	// dimension of combo boxes
	private static final Dimension comboBoxDimension = new Dimension(300, 30);
	
	private SettingMenu setMenu;
	
	/**
	 * Show a dialog to set crypto algorithms and parameters
	 * 
	 * @param _owner			window owner
	 * @param _setMenu		the SettingMenu this dialog is called from or null
	 */
	public CryptoSetter(Window _owner, SettingMenu _setMenu) {
		super(_owner);//PeaFactory.getFrame() );
		this.owner = _owner;
		setting = this;
		setMenu =_setMenu;
		JPanel contentPane = (JPanel) setting.getContentPane();//new JPanel();
		contentPane.setLayout(new BoxLayout(contentPane, BoxLayout.PAGE_AXIS));
		contentPane.setBorder(PeaBorderFactory.createBorder(false));
		
		// Avoid keyboard trap: Close with ESC
		this.getRootPane().getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW)
    		.put(KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), "CLOSE_ON_ESCAPE");
		this.getRootPane().getActionMap().put("CLOSE_ON_ESCAPE", new CloseOnEscapeAction() );
		
		this.setIconImage(PswDialogView.getImage());
		
		String info = PeaProperties.getVmBridge().translate("settings");
		if (this.owner instanceof PswDialogView) {
			info = PeaProperties.getVmBridge().translate("default") + " " + info + ": " 
					+ PeaProperties.getVmBridge().translate("new_files");
		} else {
			info = info + ": " 
					+ PeaProperties.getVmBridge().translate("current_files");
		}
		JLabel infoLabel = new JLabel(info);
		contentPane.add(infoLabel);
		contentPane.add(Box.createVerticalStrut(15));		
		JPanel cipherPanel= new JPanel();
		cipherPanel.setLayout(new BoxLayout(cipherPanel, BoxLayout.LINE_AXIS));
		JLabel cipherLabel = new JLabel(PeaProperties.getVmBridge().translate("cipher"));
		cipherLabel.setLabelFor(cipherCombo);
		cipherPanel.add(Box.createHorizontalGlue());
		cipherPanel.add(cipherLabel);
		cipherPanel.add(Box.createHorizontalStrut(10));
		cipherCombo = new JComboBox<String>();		
		cipherCombo.addItem("Threefish-512");
		cipherCombo.addItem("AES");
		cipherCombo.addItem("Serpent");
		cipherCombo.getAccessibleContext().
	    setAccessibleDescription(
	    	    "Clicking this component selects a cipher for encryption.");
		BlockCipher bc = PeaSettings.getCipherAlgo();
		if (bc != null &&  ! ( (bc instanceof AESEngine) // AESFastEngine is extra class
				|| (bc instanceof ThreefishEngine && bc.getBlockSize() == 64)
				||  (bc instanceof SerpentEngine) ) ) { 
			// extra cipher, set in PeaFactory
			cipherCombo.addItem(bc.getAlgorithmName() );
		}
		cipherCombo.setMinimumSize(comboBoxDimension);
		cipherCombo.setMaximumSize(comboBoxDimension);
		cipherCombo.setPreferredSize(comboBoxDimension);
		BlockCipher currentCipher = CipherStuff.getCipherAlgo();
		if (currentCipher instanceof ThreefishEngine  && currentCipher.getBlockSize() == 64) {
			cipherCombo.setSelectedIndex(0);
		} else if (currentCipher instanceof AESEngine) {
			cipherCombo.setSelectedIndex(1);
		} else if (currentCipher instanceof SerpentEngine) {
			cipherCombo.setSelectedIndex(2);
		} else if (cipherCombo.getItemCount() > 3 && currentCipher.getClass().isInstance( bc.getClass())) { 
			cipherCombo.setSelectedIndex(3);
		} else {
			TestLog.ve(CryptoSetter.class, "Invalid cipher found: " + currentCipher);
		}
		cipherPanel.add(cipherCombo);		
		contentPane.add(cipherPanel);
		contentPane.add(Box.createVerticalStrut(15));		
		
		JPanel hashPanel= new JPanel();
		hashPanel.setLayout(new BoxLayout(hashPanel, BoxLayout.LINE_AXIS));
		JLabel hashLabel = new JLabel(PeaProperties.getVmBridge().translate("hash"));
		hashLabel.setLabelFor(hashCombo);
		hashPanel.add(Box.createHorizontalGlue());
		hashPanel.add(hashLabel);
		hashPanel.add(Box.createHorizontalStrut(10));
		hashCombo = new JComboBox<String>();		
		hashCombo.addItem("Blake2b");
		hashCombo.addItem("SHA-3");
		hashCombo.addItem("SHA-512");
		hashCombo.getAccessibleContext().
	    setAccessibleDescription(
	    	    "Clicking this component selects a hash function that is used for encryption and key derivation.");

		Digest d = PeaSettings.getHashAlgo();
		if ( ! ( (d instanceof Blake2bDigest)
				|| (d instanceof SHA3Digest)
				|| (d instanceof SHA512Digest) ) ) {
			// extra hash function, set in PeaFactory
			hashCombo.addItem(d.getAlgorithmName());
		}
		hashCombo.setMinimumSize(comboBoxDimension);
		hashCombo.setMaximumSize(comboBoxDimension);
		hashCombo.setPreferredSize(comboBoxDimension);
//		cipherCombo.addItem("Serpent");
		Digest currentHash = HashStuff.getHashAlgo();
		if (currentHash instanceof Blake2bDigest) {
			hashCombo.setSelectedIndex(0);
		} else if (currentHash instanceof SHA3Digest) {
			hashCombo.setSelectedIndex(1);
		} else if (currentHash instanceof SHA512Digest) {
			hashCombo.setSelectedIndex(2);
		} else if (hashCombo.getItemCount() > 3 && currentHash.getClass().isInstance(d)) {
			hashCombo.setSelectedIndex(3);
		} else {
			TestLog.ve(CryptoSetter.class, "Invalid hash found: " + currentHash);
		}
		hashPanel.add(hashCombo);
		contentPane.add(hashPanel);
		contentPane.add(Box.createVerticalStrut(15));
		
		JPanel kdfPanel= new JPanel();
		kdfPanel.setLayout(new BoxLayout(kdfPanel, BoxLayout.LINE_AXIS));
		JLabel kdfLabel = new JLabel(PeaProperties.getVmBridge().translate("key_derivation"));
		kdfLabel.setLabelFor(kdfCombo);
		kdfPanel.add(Box.createHorizontalGlue());
		kdfPanel.add(kdfLabel);
		kdfPanel.add(Box.createHorizontalStrut(10));
		kdfCombo = new JComboBox<String>();		
		kdfCombo.addItem("Catena");
		kdfCombo.addItem("Argon2"); 
		kdfCombo.addItem("***" + " " + PeaProperties.getVmBridge().translate("use_only_keyfile"));
		KeyDerivation settingKDF = PeaSettings.getKdfScheme();
		if ( ! (settingKDF instanceof Argon2KDF || settingKDF instanceof CatenaKDF
				|| settingKDF instanceof SingleIterationPBKDF2) ) {
			kdfCombo.addItem(settingKDF.getName());
		}
		kdfCombo.setMinimumSize(comboBoxDimension);
		kdfCombo.setMaximumSize(comboBoxDimension);
		kdfCombo.setPreferredSize(comboBoxDimension);
		kdfCombo.getAccessibleContext().
	    setAccessibleDescription(
	    	    "Clicking this component selects a key derivation function.");
//		cipherCombo.addItem("Serpent");
		KeyDerivation currentKDF = KeyDerivation.getKdf();
		if (currentKDF instanceof CatenaKDF) {
			kdfCombo.setSelectedIndex(0);
		} else if (currentKDF instanceof Argon2KDF) {
			kdfCombo.setSelectedIndex(1);
		} else if (currentKDF instanceof SingleIterationPBKDF2) {
			kdfCombo.setSelectedIndex(2);
		} else {
			TestLog.ve(CryptoSetter.class, "Invalid KDF found: " + currentKDF);
		}
		kdfPanel.add(kdfCombo);
		contentPane.add(kdfPanel);
		contentPane.add(Box.createVerticalStrut(5));

		advancedCheck = new JCheckBox(PeaProperties.getVmBridge().translate("advanced"));
		advancedCheck.setSelected(false);
		advancedCheck.addActionListener(this);
		advancedCheck.setActionCommand("kdf");
		advancedCheck.setMinimumSize(comboBoxDimension);
		advancedCheck.setMaximumSize(comboBoxDimension);
		advancedCheck.setPreferredSize(comboBoxDimension);
		advancedCheck.getAccessibleContext().
	    setAccessibleDescription(
	    	    "Clicking this component opens more options to choose parameters for the key derivation function.");
		contentPane.add(advancedCheck);
		contentPane.add(Box.createVerticalStrut(15));

		kdfAdvancedPanelHolder = new JPanel();
		contentPane.add(kdfAdvancedPanelHolder);
		
		errorLabel = new JLabel();
		errorLabel.setForeground(Color.RED);
		contentPane.add(errorLabel);
		
		JButton scryptOkButton = new JButton("ok");
		scryptOkButton.setActionCommand("newSetting");
		scryptOkButton.addActionListener(this);
		scryptOkButton.getAccessibleContext().
	    setAccessibleDescription(
	    "Clicking this component sets the selection and closes this dialog.");
		contentPane.add(scryptOkButton);
		setting.pack();
		
		setting.setLocationRelativeTo(owner);//.setLocation(MainView.getFrameLocation());
		setting.setVisible(true);
	}

	
/* FOR TESTING	public static void main(String[] args) {
		PeaProperties.setVmBridge(new JREBridge());
		CryptoSetter cs = new CryptoSetter(null);
		cs.setVisible(true);
	} */
	
	/**
	 * Warn the user if parameter is weak or expected execution time is too long. 
	 * Ask the user to cancel. 
	 * 
	 * @param weak	true: weak parameter, false: long execution time
	 * 
	 * @return	true: cancel, false: go on
	 */
	private boolean cancelBecauseOfParameters(boolean weak) {
		// TODO execution_time_warning extreme_time_warning - weak
		// go_on  ignore_and_go_on
		String[] options = { PeaProperties.getVmBridge().translate("cancel"), 
				PeaProperties.getVmBridge().translate("ignore_and_go_on")};
		String message = "";
		if (weak == true) {
			message = PeaProperties.getVmBridge().translate("parameter") 
					+ ": " + PeaProperties.getVmBridge().translate("weak");
		} else {
			message = PeaProperties.getVmBridge().translate("extreme_time_warning");
		}
		int answer = PeaDialog.showOptions(JREProperties.getMainWindow(), 
				message, 
				PeaProperties.getVmBridge().translate("warning"), 
				0, 
				2, 
				options, options[0]);
		if (answer == 1) {
			return false;
		} else { // cancel 1 or close -1
			return true;
		}
	}

	@Override
	public void actionPerformed(ActionEvent ape) {

		String command = ape.getActionCommand();
		boolean setDefaults = false;
		if (owner instanceof PswDialogView) {
			setDefaults = true;
		}

		if (command.equals("newSetting")) {		
			
			TestLog.v(CryptoSetter.class, "Setting algos and parameters...");
			String currentParamStringToReset = AlgoParamHandler.writeString(false);
			String defaultParamStringToReset = AlgoParamHandler.writeString(true);
			if (setDefaults == false) {
				if (owner instanceof PeaLockFrame) {
					// First: save the content
					if (((PeaLockFrame) owner).isContentUnsaved() == true) {
						String saveError = ((PeaLockFrame) owner).saveCurrentContent(); 
						if (saveError != null) {
							TestLog.ve(CryptoSetter.class, "Could not save current file - " + saveError, 5);
							//return;// PeaProperties.getVmBridge().translate("unexpected_error") + " ("+ saveError + ")";
							PeaDialog.showMessage(PeaControl.getDialog().getLockFrame(), 
									PeaProperties.getVmBridge().translate("failed") + ": " + PeaProperties.getVmBridge().translate("save")
									+ "\n" + saveError, 
									PeaProperties.getVmBridge().translate("unexpected_error"), 0);
							return;
						} else {
							((PeaLockFrame) owner).markContentIsSaved();
						}
					}
					// store previous algoParams:
					// set the the previous algorithm-parameter string
					if (JREProperties.getMainWindow() instanceof PeaLockFrame) {
						((PeaLockFrame) JREProperties.getMainWindow()).storeValuesBeforeChange();
					}
				}
			}
			BlockCipher cipher = null;
			Digest digest = null;
			int[] params = null;
			String appendix = null;
			String kdfString = null;
			// parameters
			int m = 0, t = 0, p = 0;
			try {
				String cipherString = (String) cipherCombo.getSelectedItem();
				if (cipherString.equals("Threefish-512")) {
					cipher = new ThreefishEngine(512);
				} else if (cipherString.equals("AES")) {
					cipher = new AESEngine();
				} else if (cipherString.equals("Serpent")) {
					cipher = new SerpentEngine();
				} else if (cipherString.equals(PeaSettings.getCipherAlgo().getAlgorithmName())) {
					cipher = PeaSettings.getCipherAlgo();
				}
				if (cipher == null) {
					errorLabel.setText("Missing cipher");
					TestLog.e(CryptoSetter.class, "Missing cipher");
					TestLog.ve(CryptoSetter.class, "Missing  cipher ", 5);
					pack();
					return;
				}
				String digestString = (String) hashCombo.getSelectedItem();
				if (digestString.equals("Blake2b")) {
					digest = new Blake2bDigest();
				} else if (digestString.equals("SHA-3")) {
					digest = new SHA3Digest();
				} else if (digestString.equals("SHA-512")) {
					digest = new SHA512Digest();
				} else if (digestString.equals(PeaSettings.getHashAlgo().getAlgorithmName())) {
					digest = PeaSettings.getHashAlgo();
				}
				if (digest == null) {
					errorLabel.setText("Missing hash");
					TestLog.e(CryptoSetter.class, "Missing hash");
					TestLog.ve(CryptoSetter.class, "Missing  hash", 5);
					pack();
					return;
				}
				// Get the key derivation function and the instance/appendix
				// d, i, id for Argon2 or Drangonfly-FULL... for Catena
				// and the parameters
				// Do not set here
				kdfString = (String) kdfCombo.getSelectedItem();
				if (kdfString.equals("Catena")) {

					if (advancedPanel != null) {
						TestLog.v(CryptoSetter.class, "Advanced settings for... ");
						if (advancedPanel instanceof CatenaAdvancedPanel) {
							TestLog.v(CryptoSetter.class, "... Catena");
							appendix = ((CatenaAdvancedPanel) advancedPanel).getCatenaInstance();
							params = ((CatenaAdvancedPanel)advancedPanel).getParams();
							if (params == null) {
								errorLabel.setText("invalid cost parameters");
								TestLog.e(CryptoSetter.class, "Invalid KDF parameters");
								pack();
								return;
							}
							boolean cancelSetting = false;
							if (params[1] < 12 || params[0] < 2) {
								cancelSetting = cancelBecauseOfParameters(true);
							} else if (params[1] > 22 || params[0] > 6) {
								cancelSetting = cancelBecauseOfParameters(false);
							}
							if (cancelSetting == true) {
								return;
							}
						} else {
							TestLog.ve(CryptoSetter.class, "Invalid panel for Catena", 5);
							errorLabel.setText("Unexpected error (panel)");
							this.pack();
							return;
						}
					} else {
						params = CatenaAdvancedPanel.getDefaultParams();
						appendix = CatenaKDF.getVersionID();
					}

				} else if (kdfString.equals("Argon2")) {

					if (advancedPanel != null) {
						TestLog.v(CryptoSetter.class, "Advanced settings for... ");
						if (advancedPanel instanceof Argon2AdvancedPanel) {
							TestLog.v(CryptoSetter.class, "... Argon2 ");
							String argonInstance = ((Argon2AdvancedPanel) advancedPanel).getArgon2Instance();
							params = ((Argon2AdvancedPanel)advancedPanel).getParams();
							if (params == null) {
								errorLabel.setText("invalid cost parameters");
								TestLog.e(CryptoSetter.class, "Invalid KDF parameters");
								pack();
								return;
							}
							boolean cancelSetting = false;
							if (params[1] < 64 || params[0] < 2) {
								cancelSetting = cancelBecauseOfParameters(true);
							} else if (params[1] > 1024 || params[0] > 8) {
								cancelSetting = cancelBecauseOfParameters(false);
							}
							if (cancelSetting == true) {
								return;
							}
							appendix = argonInstance.substring("Argon2".length(), argonInstance.length());
						} else {
							TestLog.ve(CryptoSetter.class, "Invalid panel for Argon2", 5);
							errorLabel.setText("Unexpected error (panel)");
							this.pack();
							return;
						}
					} else {
						params = Argon2AdvancedPanel.getDefaultParams();
						int type = Argon2KDF.getType();
						if (type == 0) {
							appendix = "d";
						} else if (type == 1) {
							appendix = "i";
						} else if (type == 2) {
							appendix = "id";
						} else {
							TestLog.ve(CryptoSetter.class, "Invalid Argon2 appendix: " + type, 5);
							TestLog.ve(CryptoSetter.class, "Set to default instance id... ");
							appendix = "id";
						}
					}
					
				} else if (kdfString.startsWith("***")){//.equals(PeaProperties.getVmBridge().translate("use_only_keyfile"))){
				
					// parameters
					m = 0;
					t = 0;
					p = 0;
					params = new int[3];
					appendix = null;
					
				} else if (kdfString.equals(PeaSettings.getKdfScheme().getName())) {
					
					if (advancedPanel != null) {
						params = ((AllOtherAdvancedPanel)advancedPanel).getParams();

						if (params == null) {
							errorLabel.setText("invalid cost parameters");
							TestLog.e(CryptoSetter.class, "Invalid KDF parameters");
							pack();
							return;
						}
				/*		boolean success = PeaSettings.getKdfScheme().setParameters(params);
						if (success == false) {
							errorLabel.setText("Invalid parameters");
							return;
						}*/

					} else {
						// Get default values from PeaSettings
						params = new int[3];
						params[0] = PeaSettings.getIterations();
						params[1] = PeaSettings.getMemory();						
						params[2] = PeaSettings.getParallelization();
					}
				}
				if (params == null || params.length < 3) {
					errorLabel.setText("unexpected error");
					TestLog.e(CryptoSetter.class, "Null or too short KDF parameters", 5);
					pack();
					return;
				}

				t = params[0];
				m = params[1];				
				p = params[2];			

				if (m < 0 || t < 0 || p < 0) {
					errorLabel.setText("invalid cost parameters");
					TestLog.e(CryptoSetter.class, "Invalid KDF parameters");
					TestLog.ve(CryptoSetter.class, "Invalid KDF parameters: " + m + ", " + t + ", " + p, 5);
					pack();
					return;
				}
				if (appendix == null && ( kdfString.equals("Catena") || kdfString.equals("Argon2") ) ) {
					errorLabel.setText("invalid KDF instance");
					TestLog.e(CryptoSetter.class, "Invalid KDF instance");
					TestLog.ve(CryptoSetter.class, "Invalid KDF instance ", 5);
					pack();
					return;
				}
			} catch (Exception e) {
				TestLog.e(CryptoSetter.class, PeaProperties.getVmBridge().translate("unexpected_error") + 
						e.getLocalizedMessage() + " - " + e.toString());
				TestLog.ve(CryptoSetter.class, e.toString(), 5);
				PeaDialog.showMessage(PeaControl.getDialog().getLockFrame(), 
						PeaProperties.getVmBridge().translate("failed") + ": " + PeaProperties.getVmBridge().translate("settings")
						+ "\n" + e.getLocalizedMessage(), 
						PeaProperties.getVmBridge().translate("unexpected_error"), 0);
				setting.dispose();
				return;
			}

			try {
				// Set the KDF: instance, appendix, parameters
				if (setDefaults == true) {

					// set the default cipher
					PeaProperties.setDefaultCipher(cipher);
					// set the default hash
					PeaProperties.setDefaultDigest(digest);

					// set as default value
					PeaProperties.setDefaultTCost(t);
					if (kdfString.equals("Argon2")) {
						PeaProperties.setDefaultMCost(m * 1024);// Argon2 uses KiB, used here: MiB
					} else if (kdfString.equals("Catena")) {
						PeaProperties.setDefaultMCost(m );
					} else {// extra KDF
						PeaProperties.setDefaultMCost(m );
					}
					PeaProperties.setDefaultParallel(p);
					if (kdfString.equals("Argon2")) {
						// set this KDF with new parameters as KDF to use	
						PeaProperties.setDefaultKDFInstanceAppendix(appendix);
						PeaProperties.setDefaultKDF(new Argon2KDF());					
					} else if (kdfString.equals("Catena")) {
						// set this KDF with new parameters as KDF to use	
						PeaProperties.setDefaultKDFInstanceAppendix(appendix);
						PeaProperties.setDefaultKDF(new CatenaKDF());				
					} else if (kdfString.startsWith("***")) {//  .equals(PeaProperties.getVmBridge().translate("use_only_keyfile"))){
						PeaProperties.setDefaultKDF(new SingleIterationPBKDF2());	
						//TestLog.e(getClass(), "XXXXXXXXXXXXX setDefalt keyfile-only mode");
					} else if (kdfString.equals(PeaSettings.getKdfScheme().getName())) {
						PeaProperties.setDefaultKDF(PeaSettings.getKdfScheme());	
					}
					// create algorithm parameter string and set properties
					String defaultAlgos = AlgoParamHandler.writeString(true);
					PeaProperties.setDefaultAlgoParamString(defaultAlgos);
					TestLog.o(CryptoSetter.class, "New default algorithms and parameters: " + AlgoParamHandler.getReadableString(defaultAlgos, false));
					new PropertyHandler().setOneProperty("default_algo_param", defaultAlgos);

				} else {

					// Set the current cipher
					CipherStuff.setCipherAlgo(cipher);
					// Set the current hash
					HashStuff.setHashAlgo(digest);

					// not default: Current KDF
					if (kdfString.equals("Argon2")) {
						Argon2KDF.setIterations(t);
						Argon2KDF.setMemoryCost(m * 1024);// Argon2 uses KiB, used here: MiB
						Argon2KDF.setLanes(p);
						// set KDF with new parameters as KDF to use
						if (appendix.equals("id")){
							Argon2KDF.setType(2);
						} else if (appendix.equals("d")){
							Argon2KDF.setType(0);
						} else if (appendix.equals("i")){
							Argon2KDF.setType(1);
						}
						KeyDerivation.setKdf( new Argon2KDF() );
						PeaProperties.setKeyFileOnlyMode(false);
						PeaProperties.setDirectStart(false);
						
					}  else if (kdfString.equals("Catena")) {
						CatenaKDF.setVersionID(appendix);
						if (appendix.equals("Dragonfly-Full") || appendix.equals("Dragonfly")){
							CatenaKDF.setGarlicDragonfly(m);
							CatenaKDF.setLambdaDragonfly(t);
						} else if (appendix.equals("Butterfly-Full") || appendix.equals("Butterfly")){
							CatenaKDF.setGarlicButterfly(m);
							CatenaKDF.setLambdaButterfly(t);
						}
						// set KDF with new parameters as KDF to use
						KeyDerivation.setKdf( new CatenaKDF() );
						PeaProperties.setKeyFileOnlyMode(false);
						PeaProperties.setDirectStart(false);
						
					} else if (kdfString.startsWith("***")) {//  .equals(PeaProperties.getVmBridge().translate("use_only_keyfile"))){
						KeyDerivation.setKdf( new SingleIterationPBKDF2());	
						PeaProperties.setKeyFileOnlyMode(true);
						
					}   else if (kdfString.equals(PeaSettings.getKdfScheme().getName())) {				
						// set KDF from PeaSettings
						boolean setParamSuccess = PeaSettings.getKdfScheme().setParameters(params);
						if (setParamSuccess == false) {
							TestLog.e(getClass(), "Unable to set parameters for " + kdfString, 5);
						}
						KeyDerivation.setKdf( PeaSettings.getKdfScheme() );	
						TestLog.v(CryptoSetter.class, "Set current extra KDF: " + kdfString + " ,t=" + params[0] + ",m=" + params[1] + ",p=" + params[2]);
						PeaProperties.setKeyFileOnlyMode(false);
						PeaProperties.setDirectStart(false);
					} 
					
					///////////////////
					// apply:
					///////////////////
					if (PeaControl.getDialog().getLockFrame() instanceof PeaLockFrame) {
						String[] fileNamesToApply =((PeaLockFrame) owner).chooseFileNamesToApply();
						String errorMessage = ((PeaLockFrame) PeaControl.getDialog().getLockFrame()).changePasswordOrApplyChanges(
								(PeaLockFrame) PeaControl.getDialog().getLockFrame(), fileNamesToApply);
						if (errorMessage != null) {
							TestLog.ve(CryptoSetter.class, "Error occurred when changing algos or  parameters", 5);
							PeaDialog.showMessage(PeaControl.getDialog().getLockFrame(), 
									errorMessage, 
									PeaProperties.getVmBridge().translate("error"), 0);
							TestLog.v(getClass(), "Reset to previous algorithms...");
							AlgoParamHandler.readString(currentParamStringToReset, false);
						}
					} else {
						TestLog.ve(CryptoSetter.class, "Error occurred when changing KDF parameters: Invalid main frame", 5);
						PeaDialog.showMessage(PeaControl.getDialog().getLockFrame(), 
								PeaProperties.getVmBridge().translate("unexpected_error") + "\n"
										+ "Could not set algorithms and parameters \n"
										+ "(Invalid main frame)", 
										PeaProperties.getVmBridge().translate("error"), 0);

					}
				}
			} catch (Exception e) {
				TestLog.e(CryptoSetter.class, PeaProperties.getVmBridge().translate("unexpected_error") + 
						e.getLocalizedMessage() + " - " + e.toString());
				TestLog.ve(CryptoSetter.class, e.toString(), 5);
				PeaDialog.showMessage(PeaControl.getDialog().getLockFrame(), 
						PeaProperties.getVmBridge().translate("failed") + ": " + PeaProperties.getVmBridge().translate("settings")
						+ "\n" + e.getLocalizedMessage(), 
						PeaProperties.getVmBridge().translate("unexpected_error"), 0);
				TestLog.v(getClass(), "Reset to previous algorithms...");
				AlgoParamHandler.readString(currentParamStringToReset, false);
				AlgoParamHandler.readString(defaultParamStringToReset, true);
			}

			setting.dispose();
			if (setMenu != null) {
				setMenu.setVisibility();
			}
			
		} else if (command.equals("kdf")) {	
			if (advancedCheck.isSelected() == true) {
				kdfCombo.setEnabled(false);
				// check if selected KDF setting is shown
				if (kdfCombo.getSelectedItem().equals("Argon2")) {
					kdfAdvancedPanelHolder.removeAll();
					advancedPanel = new Argon2AdvancedPanel();
					kdfAdvancedPanelHolder.add(advancedPanel);
					this.pack();
				} else if (kdfCombo.getSelectedItem().equals("Catena")) {
					kdfAdvancedPanelHolder.removeAll();
					advancedPanel = new CatenaAdvancedPanel();
					kdfAdvancedPanelHolder.add(advancedPanel);
					this.pack();
				} else if (kdfCombo.getSelectedItem().equals(PeaSettings.getKdfScheme().getName())) {
					kdfAdvancedPanelHolder.removeAll();
					advancedPanel = new AllOtherAdvancedPanel(PeaSettings.getKdfScheme());
					kdfAdvancedPanelHolder.add(advancedPanel);
					this.pack();
				}
			} else {
				kdfCombo.setEnabled(true);
				kdfAdvancedPanelHolder.removeAll();
				advancedPanel = null;
				this.pack();
			}	
		}
	}	
}
