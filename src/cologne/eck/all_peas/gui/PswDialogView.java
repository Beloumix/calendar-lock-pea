package cologne.eck.all_peas.gui;

/*
 * Peafactory - Production of Password Encryption Archives
 * Copyright (C) 2015  Axel von dem Bruch
 * 
 * This library is free software; you can redistribute it and/or modify it 
 * under the terms of the GNU General Public License as published 
 * by the Free Software Foundation; either version 2 of the License, 
 * or (at your option) any later version.
 * This library is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
 * or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU General Public License for more details.
 * See:  http://www.gnu.org/licenses/gpl-2.0.html
 * You should have received a copy of the GNU General Public License 
 * along with this library.
 */

/**
 * View of the pea dialog to type the password. 
 */

import java.awt.*;
import java.awt.event.*;
import java.util.ArrayList;
import java.util.Hashtable;

import javax.imageio.ImageIO;
import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.plaf.FontUIResource;

import cologne.eck.all_peas.cloud.CloudPropertyHandler;
import cologne.eck.all_peas.cloud.gui.CloudMenu;
/* TODO
import cologne.eck.all_peas.cloud.StorageProvider;
import cologne.eck.all_peas.cloud.gui.CloudFileManager;
import cologne.eck.all_peas.cloud.provider.DropboxProvider; */
import cologne.eck.all_peas.control.PeaControl;
import cologne.eck.all_peas.data.AttachmentHandler;
import cologne.eck.all_peas.data.Attachments;
import cologne.eck.all_peas.data.PeaProperties;
import cologne.eck.all_peas.files.FilePanelHolder;
import cologne.eck.all_peas.files.FileTypePanel;
import cologne.eck.all_peas.gui.menu.HelpMenu;
import cologne.eck.all_peas.gui.menu.PeaMenuBar;
import cologne.eck.all_peas.gui.menu.SettingMenu;
import cologne.eck.all_peas.gui.menu.ViewMenu;
import cologne.eck.all_peas.vm_specific.JREProperties;
import cologne.eck.peafactory.crypto.AlgoParamHandler;
import cologne.eck.peafactory.crypto.CipherStuff;
import cologne.eck.tools.EntropyPool;
import cologne.eck.tools.KeyRandomCollector;
import cologne.eck.tools.MouseRandomCollector;
import cologne.eck.tools.TestLog;
import cologne.eck.tools.Zeroizer;
import settings.PeaSettings;



/*
 * View of EditorControl, CalendarControl, FileControl,
 * ImageControl 
 */


@SuppressWarnings("serial")
public class PswDialogView extends JDialog implements WindowListener,
ActionListener, KeyListener {

	// screen: 
	private static final Dimension screenSize = GraphicsEnvironment.getLocalGraphicsEnvironment().getMaximumWindowBounds().getSize();
	//private static final Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
	private static final double screenWidth = screenSize.getWidth();
	private static final double screenHeight = screenSize.getHeight();

	private static int numberOfScreens = 0;

	// label above password field (if undefined: type password...)
	private static JLabel pswLabel;
	// password field
	private static JPasswordField pswField;
	// Button to start decryption
	private static JButton okButton;

	private static PeaMenuBar menuBar;

	// static instance
	private static PswDialogView dialog;

	private static Hashtable<Component, ArrayList<Character>> mnemoTable = new Hashtable<Component, ArrayList<Character>>();

	// display warnings and error messages
	private static JLabel errorMessageLabel;

	// thread while typing the password - still not used
	private WhileTypingThread t;

	// pea image icon for frames
	private static Image image;

	// to check if decryption was startet by ok-button
	private static boolean started = false;

	// if started first time (no password, no content)
	// can be set manually in menu
	private static boolean initializing = false; 
	
	//private static JCheckBox directStartCheck;
	
	private static JPanel directStartPanel;

	private PswDialogView() {
		
		TestLog.v(getClass(), "Create password dialog");

		GraphicsEnvironment ge = GraphicsEnvironment.getLocalGraphicsEnvironment();
		if (ge != null) {
			GraphicsDevice[] gd = ge.getScreenDevices();
			numberOfScreens =  gd.length;
		} 

		// start the EntropyPool:
		EntropyPool.getInstance().updateThread(System.nanoTime());

		dialog = this;
		this.setTitle(PeaSettings.getJarFileName() );
		this.setName("EncryptedDialog");

		//this.setBackground(peaColor);
		image = IconManager.loadIcon("pea-lock.png", 
				PeaProperties.getVmBridge().translate("pea_with_keyhole"), 
				null, 
				PeaProperties.getVmBridge().translate("logo_of_pea") ).getImage();

		this.setIconImage(image);

		this.addWindowListener(this);

		this.addMouseMotionListener(new MouseRandomCollector() );

		JPanel topPanel = (JPanel) this.getContentPane();

		topPanel.setBorder(PeaBorderFactory.createBorder(true));//threeBorder);

		topPanel.addMouseMotionListener(new MouseRandomCollector() );
		topPanel.setLayout(new BoxLayout(topPanel, BoxLayout.Y_AXIS));
		int[] cipherColor = PeaProperties.getCipherPeaColor();
		topPanel.setBackground(new Color(cipherColor[0], cipherColor[1], cipherColor[2]));
		
		// Before loading file display: cleanup cloud properties
		if ( ( ! PeaProperties.getFileType().equals("file"))) {
			// cleanup cloud properties: delete empty sub sets:
			CloudPropertyHandler.cleanupProperties();
		}

		loadComponents(topPanel, true);
		// reset Mnemonics:
		mnemoTable = new Hashtable<Component, ArrayList<Character>>();

		this.setLocation(new Point(JREProperties.getFrameLocation()[0], JREProperties.getFrameLocation()[1]));
		//FilePanelHolder.updateWindow();
		pack();

	}

	public final static PswDialogView getInstance() {
		if (dialog == null) {
			dialog = new PswDialogView();
		} else {
			//return null
		}
		return dialog;
	}

	/**
	 * Dispose the current instance, create new instance and
	 * set visible
	 */
	public final static void resetLanguage() {

		Point p = dialog.getLocationOnScreen();
		FilePanelHolder.resetLanguage();
		dialog.dispose();
		dialog = new PswDialogView();
		if (p != null) {
			dialog.setLocation(p);
		}
		dialog.setVisible(true);
		JREProperties.setMainWindow(dialog);

		// this is only required for Calendar Lock PEA (own language bundle)
		PeaControl.getDialog().setLanguage(PeaProperties.getLastLanguage());
	}
	
	/**
	 * Remove a mnemonic from a list. This should be called,
	 * when a button disappears
	 * 
	 * @param parent	parent component that hold the list of mnemonics
	 * @param c	the menmonic to remove
	 */
	public void removeMnemonic(Component parent, char c) {
		if (parent == null || c == '\0') {
			TestLog.ve(PswDialogView.class, "Missing parent component or character", 5);
			return;
		}
		if (mnemoTable.containsKey(parent)) {
			ArrayList<Character> list = mnemoTable.get(parent);
			boolean removeResult = list.remove( (Character) c);
			if (removeResult == false) {
				TestLog.ve(getClass(), "Menmonic did not contain character to remove: " + c);
			} else {
				//TestLog.ve(getClass(), "Menmonic removed: " + c);
			}
				
		}
	}

	/**
	 * Get the next free mnemonic of a text. 
	 * 
	 * @param parent	the component to proof
	 * @param text		the text to get the menmo from
	 * 
	 * @return	the next free mnemonic
	 */
	public char getMnemo(Component parent, String text) {
		if (parent == null || text == null) {
			TestLog.ve(PswDialogView.class, "Missing parent component or text", 5);
			return '\0';
		}
		if (mnemoTable.containsKey(parent)) {
			ArrayList<Character> list = mnemoTable.get(parent);
			int indexInText = 0;
			char c = text.charAt(indexInText);
			int textLen = text.length();
			while (indexInText < textLen ) {
				if(list.contains(c) == false) {
					list.add(c);
//TestLog.o(getClass(), list.size() + ", Mnemo: " + c);
					return c;
				} else {
					indexInText++;
					c = text.charAt(indexInText);
				}
			}
			list.add(c);
//TestLog.o(getClass(), list.size() + ", Mnemo: " + c);
			return c;
		} else { // new menu / menuBar
			ArrayList<Character> list = new ArrayList<Character>();
			list.add(text.charAt(0));
//TestLog.o(getClass(), list.size() + ", Mnemo: " + text.charAt(0));
			mnemoTable.put(parent, list);
			return text.charAt(0);
		}
	}

	/**
	 * Show error messages on the message label of the password dialog
	 * 
	 * @param topic		the text to show
	 * @param error		true for error (red font color), false otherwise
	 */
	public final void displayErrorMessages(String topic, boolean error) {
		setMessage(topic + ":\n", error, false);
		setMessage(CipherStuff.getErrorMessage(), error, true);
	}

	@Override
	public void keyPressed(KeyEvent kpe) {
		if (kpe.getKeyChar() == KeyEvent.VK_ENTER && initializing == false){
			// ok button
			okButton.doClick();

		} else if (kpe.isControlDown()) {
			if (errorMessageLabel.getText().equals(PeaProperties.getVmBridge().translate("password_failed"))){
				errorMessageLabel.setText(" ");
			}
			if (kpe.getKeyCode() == KeyEvent.VK_T ) {
				CharTable table = new CharTable(this, pswField,0);
				table.setVisible(true);
			} else if (kpe.getKeyCode() == KeyEvent.VK_K) {
				Keyboard keyboard = new Keyboard(this, pswField);
				keyboard.setVisible(true);
			} else if (kpe.getKeyCode() == KeyEvent.VK_F1 ) {
				CharTable table = new CharTable(this, pswField, 0);
				table.setVisible(true);
			}else if (kpe.getKeyCode() == KeyEvent.VK_F2 ) {
				CharTable table = new CharTable(this, pswField, 1);
				table.setVisible(true);
			}else if (kpe.getKeyCode() == KeyEvent.VK_F3 ) {
				CharTable table = new CharTable(this, pswField, 2);
				table.setVisible(true);
			}else if (kpe.getKeyCode() == KeyEvent.VK_F4 ) {
				CharTable table = new CharTable(this, pswField, 3);
				table.setVisible(true);
			}else if (kpe.getKeyCode() == KeyEvent.VK_F5 ) {
				CharTable table = new CharTable(this, pswField, 4);
				table.setVisible(true);
			}else if (kpe.getKeyCode() == KeyEvent.VK_F6 ) {
				CharTable table = new CharTable(this, pswField, 5);
				table.setVisible(true);
			}else if (kpe.getKeyCode() == KeyEvent.VK_F7 ) {
				CharTable table = new CharTable(this, pswField, 6);
				table.setVisible(true);
			}else if (kpe.getKeyCode() == KeyEvent.VK_F8 ) {
				CharTable table = new CharTable(this, pswField, 7);
				table.setVisible(true);
			}else if (kpe.getKeyCode() == KeyEvent.VK_F9 ) {
				CharTable table = new CharTable(this, pswField, 8);
				table.setVisible(true);
			}else if (kpe.getKeyCode() == KeyEvent.VK_F10 ) {
				CharTable table = new CharTable(this, pswField, 9);
				table.setVisible(true);
			}else if (kpe.getKeyCode() == KeyEvent.VK_F11 ) {
				CharTable table = new CharTable(this, pswField, 10);
				table.setVisible(true);
			}else if (kpe.getKeyCode() == KeyEvent.VK_F12 ) {
				CharTable table = new CharTable(this, pswField, 11);
				table.setVisible(true);
			}
		} 
	}

	@Override
	public void keyReleased(KeyEvent arg0) {}
	@Override
	public void keyTyped(KeyEvent arg0) {}

	@Override
	public void actionPerformed(ActionEvent ape) {
		String command = ape.getActionCommand();
		//TestLog.o(PswDialogView.class, "command: " + command);
		if(command.equals("open")){
			//TestLog.o(PswDialogView.class, "open files");		
			PeaControl.getDialog().openEncryptedFiles();

		} else if (command.equals("init")){			
			initialize();

		} else if (command.equals("quit")){
			TestLog.o(PswDialogView.class, "Exit");
			System.exit(0);

		} else if(command.equals("keyboard")){

			// clear error messages from previous password errors
			if (errorMessageLabel.getText().equals(PeaProperties.getVmBridge().translate("password_failed"))){
				errorMessageLabel.setText(" ");
			}
			Keyboard keyboard = new Keyboard(this, pswField);
			keyboard.setVisible(true);
			//PeaSettings.getKeyboard().setVisible(true);

		} else if (command.equals("charTable1")){
			// clear error messages from previous password errors
			if (errorMessageLabel.getText().equals(PeaProperties.getVmBridge().translate("password_failed"))){
				errorMessageLabel.setText(" ");
			}
			CharTable table = new CharTable(this, pswField,0);
			table.setVisible(true);
			
		} else if (command.equals("directStart")){
			Object obj = ape.getSource(); 
			if (obj instanceof JCheckBox) {
				JCheckBox directStartCheck = (JCheckBox) obj;
				if (directStartCheck.isSelected() == true) {
					PeaProperties.setDirectStart(true);
					directStartCheck.setText(PeaProperties.getVmBridge().translate("disable_direct_start"));					
					TestLog.v(getClass(), "Direct start mode disabled");
				} else {
					PeaProperties.setDirectStart(false);
					directStartCheck.setText(PeaProperties.getVmBridge().translate("enable_direct_start"));
					TestLog.v(getClass(), "Direct start mode enabled");
				}
				//directStartCheck.repaint();
			}

		} else if (command.equals("ok")){

			// disable okButton
			okButton.setEnabled(false);

			// reset error message
			errorMessageLabel.setText("\t   ");
			
			try {
				t.join(1);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			try { // always reset the cursor				
				this.setCursor(CursorManager.getWaitCursor());//Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
				//TestLog.v(PswDialogView.class, "start execution", 5);
				PeaControl.getDialog().startExecution(false);//rememberPath);
			} finally {
				// always reset the cursor:
				this.setCursor(CursorManager.getDefaultCursor());
			}
		} else {
			TestLog.e(PswDialogView.class, "Invalid command: " + command);
		}
	}	

	@Override
	public void windowActivated(WindowEvent arg0) {}
	@Override
	public void windowClosed(WindowEvent arg0) { // dispose
		this.dispose();
	}

	@Override
	public void windowClosing(WindowEvent arg0) { // x

		if (started == true) {
			return;
		} else {
			// if attacker has access to memory but not to file
			if (Attachments.getNonce() != null) {
				Zeroizer.zero(Attachments.getNonce());
			}
			if (PeaControl.getDialog() != null) {
				PeaControl.getDialog().clearSecretValues();
			}
			System.exit(0);
		}	
	}

	@Override
	public void windowDeactivated(WindowEvent arg0) {}
	@Override
	public void windowDeiconified(WindowEvent arg0) {}
	@Override
	public void windowIconified(WindowEvent arg0) {}

	@Override
	public void windowOpened(WindowEvent arg0) {

		// do some expensive computations while user tries to remember the password
		t = this.new WhileTypingThread();
		t.setPriority( Thread.MAX_PRIORITY );// 10
		t.start();
		if (initializing == false) {
			pswField.requestFocus();
		}
	}


	//=======================================
	// Helper Functions

	/**
	 * Get the number of currently used screens
	 * 
	 * @return	the number of screens
	 */
	public static int getNumberOfScreens() {
		return numberOfScreens;
	}	

	public final static void setUI(boolean encrypted) {
		Color mainColor;
		if (encrypted == true) {
			int[] ciphVals = PeaProperties.getCipherPeaColor();
			mainColor = new Color(ciphVals[0], ciphVals[1], ciphVals[2]);
			//mainColor = PeaProperties.getCipherPeaColor();//cipherPeaColor;
		} else {
			//mainColor = PeaProperties.getPlainPeaColor();//plainPeaColor;
			int[] plainVals = PeaProperties.getPlainPeaColor();
			mainColor = new Color(plainVals[0], plainVals[1], plainVals[2]);
		}	
		try {
			// for backward compatibility: In Java 6 Nimbus package was renamed
			UIManager.LookAndFeelInfo[] infos = UIManager.getInstalledLookAndFeels();
			int len = infos.length;
			for (int i = 0; i < len; i++) {
				if ("Nimbus".equals(infos[i].getName())) {
					UIManager.setLookAndFeel(infos[i].getClassName());

					int[] ciphVals = PeaProperties.getCipherPeaColor();
					Color darkerCiphColor = new Color(ciphVals[0], ciphVals[1], ciphVals[2]).darker();
					// Set the primary colors:
					UIManager.put("control", mainColor);//peaColor);
					UIManager.put("info", new Color(242,242,189));
					UIManager.put("nimbusAlertYellow", new Color(255,220,35));
					UIManager.put("nimbusBase", darkerCiphColor);//Color.GRAY);//new Color(PeaProperties.getCipherPeaColor().getRed()+ 25, PeaProperties.getCipherPeaColor().getGreen()+ 25, PeaProperties.getCipherPeaColor().getBlue()+ 25));// .darker());//new Color(76, 127, 80));//new Color(255,255,255));// new Color(51, 102, 55));
					UIManager.put("nimbusDisabledText", new Color(142,143,145));
					UIManager.put("nimbusFocus", darkerCiphColor.darker());//new Color(102, 127, 104));//Color.RED);// new Color(114, 229, 124));
					UIManager.put("nimbusGreen", new Color(157, 178, 158));
					UIManager.put("nimbusInfoBlue", new Color(33, 165, 44));
					UIManager.put("nimbusLightBackground", new Color(255,255,255));
					UIManager.put("nimbusOrange", new Color(191,98,4));//(148, 178, 0));
					UIManager.put("nimbusRed", new Color(169,46,34));
					UIManager.put("nimbusSelectedText", new Color(255,255,255));
					UIManager.put("nimbusSelectionBackground", darkerCiphColor);//new Color(76, 153, 82));//(74, 165, 82));
					UIManager.put("text", new Color(0,0,0));

					// correction:
					UIManager.put("MenuItem.opaque", true); // peaColor background
					UIManager.put("Panel.opaque", true); // peaColor background

					//		UIManager.put("MenuBar.background", new Color(221, 249, 222));
					//		UIManager.put("Menu.background", Color.LIGHT_GRAY);//new Color(221, 249, 222));
					//UIManager.put("Button[Default+Focused+Pressed].backgroundPainter", UIManager.get("Button[Focused+Pressed].backgroundPainter") );

					//UIManager.put("Button.contentMargins", new Insets(0,0,0,0));

					UIManager.put("defaultFont",
							new Font(Font.SANS_SERIF, Font.PLAIN, PeaProperties.getFontSize()));
					// set font size (UIManager)
					FontUIResource f = new FontUIResource(Font.SANS_SERIF, Font.PLAIN, PeaProperties.getFontSize());			
					java.util.Enumeration<Object> keys = UIManager.getDefaults().keys();
					while (keys.hasMoreElements())  {
						Object key = keys.nextElement();
						Object value = UIManager.get(key);
						if (value instanceof javax.swing.plaf.FontUIResource) {
							UIManager.put(key, f);
						}
					}
					break;
				}
			}		

		} catch (Exception e) {
			TestLog.e(PswDialogView.class, "Look&Feel Nimbus first try failed");

			try {
				// works for debian: package name for Nimbus was changed
				// Bug JDK-6647622
				UIManager.setLookAndFeel("com.sun.java.swing.plaf.nimbus.NimbusLookAndFeel");
				TestLog.e(PswDialogView.class, "Package name for Nimbus renamed, start with new packege name...");
				UIManager.put("control", PeaProperties.getPeaColor());//peaColor);
			} catch (Exception e2) {


				TestLog.e(PswDialogView.class, "Setting of Look&Feel failed: \n"
						+ UIManager.getSystemLookAndFeelClassName() + "\n"
						+ e.toString() + ",  " + e.getMessage());
				// set Metal Look&Feel
				try {
					UIManager.setLookAndFeel("javax.swing.plaf.metal.MetalLookAndFeel");
				} catch (Exception e1) {
					TestLog.e(PswDialogView.class, "Setting of Look&Feel failed: \n"
							+ UIManager.getSystemLookAndFeelClassName() + "\n"
							+ e.toString() + ",  " + e.getMessage());
				}
			}
		}		
	}

	private void loadComponents(JPanel topPanel, boolean updateFiles) {
		if (PeaProperties.isCommandLineMode() == true) {
			return;
		}

		Font font = new Font(Font.SANS_SERIF, Font.PLAIN, PeaProperties.getFontSize());
		int[] cipherColor = PeaProperties.getCipherPeaColor();

		JPanel menuPanel = new JPanel();
		menuPanel.setLayout(new BoxLayout(menuPanel, BoxLayout.X_AXIS));
		menuPanel.setFont(font);
		menuPanel.setMinimumSize(new Dimension(300, 30));
		menuPanel.setMaximumSize(new Dimension((int)getScreenwidth(), 30));
		menuPanel.setBackground(new Color(cipherColor[0], cipherColor[1], cipherColor[2]));

		menuBar = new PeaMenuBar();

		JMenu menu = new JMenu(PeaProperties.getVmBridge().translate("file"));
		menu.getAccessibleContext().
		setAccessibleDescription(
				"Clicking this component opens a menu for file operations: open, new,quit.");
		//menu.setRolloverEnabled(true);
		menu.setFont(font);
		menu.setMnemonic(getMnemo(menuBar, PeaProperties.getVmBridge().translate("file")));
		menuBar.add(menu);

		JMenuItem openItem = new JMenuItem(PeaProperties.getVmBridge().translate("open_encrypted_file"));
		openItem.getAccessibleContext().
		setAccessibleDescription(
				"Clicking this component opens a dialog to open an encrypted file.");
		openItem.setActionCommand("open");
		openItem.setFont(font);
		//openItem.setMnemonic(getMnemo(menu, PeaProperties.getVmBridge().translate("open_encrypted_file")));
		openItem.addActionListener(dialog);
		menu.add(openItem);

		JMenuItem initItem = new JMenuItem(PeaProperties.getVmBridge().translate("init_new_file"));
		initItem.getAccessibleContext().
		setAccessibleDescription(
				"Clicking this component opens dialogs to set a password and a file name.");
		initItem.setActionCommand("init");
		initItem.setFont(font);
		//initItem.setMnemonic(getMnemo(menu, PeaProperties.getVmBridge().translate("init_new_file")));
		initItem.addActionListener(dialog);
		menu.add(initItem);		

		JMenuItem quitItem = new JMenuItem(PeaProperties.getVmBridge().translate("quit"));//"Quit");
		quitItem.getAccessibleContext().
		setAccessibleDescription(
				"Clicking this component exits the prgram.");
		quitItem.setActionCommand("quit");
		quitItem.setFont(font);
		//quitItem.setMnemonic(getMnemo(menu, PeaProperties.getVmBridge().translate("quit")));
		quitItem.addActionListener(dialog);
		menu.add(quitItem);		


		JMenu extrasMenu = new JMenu(PeaProperties.getVmBridge().translate("extras"));
		extrasMenu.getAccessibleContext().
		setAccessibleDescription(
				"Clicking this component opens a menu for extras: keyboard and character table.");
		extrasMenu.setRolloverEnabled(true);
		extrasMenu.setFont(font);
		extrasMenu.setMnemonic(getMnemo(menuBar, PeaProperties.getVmBridge().translate("extras")));
		menuBar.add(extrasMenu);

		JMenuItem charTableItem = new JMenuItem(PeaProperties.getVmBridge().translate("char_table") + " (CTRL + T) / (CTRL + F1...)");
		charTableItem.getAccessibleContext().
		setAccessibleDescription(
				"Clicking this component opens a table of characters to choose.");
		charTableItem.setActionCommand("charTable1");
		charTableItem.setFont(font);
		//charTableItem.setMnemonic(getMnemo(extrasMenu, PeaProperties.getVmBridge().translate("char_table")));
		charTableItem.addActionListener(dialog);
		extrasMenu.add(charTableItem);

		JMenuItem keyboardItem = new JMenuItem(PeaProperties.getVmBridge().translate("keyboard") + " (CTRL + K)");
		keyboardItem.getAccessibleContext().
		setAccessibleDescription(
				"Clicking this component opens a virtual keyboard.");
		keyboardItem.setActionCommand("keyboard");
		keyboardItem.setFont(font);
		//keyboardItem.setMnemonic(getMnemo(extrasMenu, PeaProperties.getVmBridge().translate("keyboard")));
		keyboardItem.addActionListener(dialog);
		extrasMenu.add(keyboardItem);

		JMenu viewMenu = new ViewMenu(FilePanelHolder.getInstance(), (FileTypePanel)JREProperties.getTypePanel(), 
				getMnemo(menuBar, PeaProperties.getVmBridge().translate("view")), false);// plainModus false
		viewMenu.getAccessibleContext().
		setAccessibleDescription(
				"Clicking this component opens a menu to configure the view of the files.");
		viewMenu.setRolloverEnabled(true);
		//viewMenu.setMnemonic(getMnemo(menuBar, PeaProperties.getVmBridge().translate("view")));
		menuBar.add(viewMenu);

		JMenu settingMenu = new SettingMenu(true, getMnemo(menuBar, PeaProperties.getVmBridge().translate("settings")));// encrypted mode
		settingMenu.setRolloverEnabled(true);
		//extrasMenu.setMnemonic(getMnemo(menuBar, PeaProperties.getVmBridge().translate("settings")));
		//settingMenu.setMnemonic(getMnemo(menuBar, PeaProperties.getVmBridge().translate("settings")));
		menuBar.add(settingMenu);


		JMenu cloudMenu = new CloudMenu(getMnemo(menuBar, PeaProperties.getVmBridge().translate("cloud")));
		cloudMenu.setRolloverEnabled(true);
		//cloudMenu.setMnemonic(getMnemo(menuBar, PeaProperties.getVmBridge().translate("cloud")));
		menuBar.add(cloudMenu);

		JMenu helpMenu = new HelpMenu(getMnemo(menuBar, PeaProperties.getVmBridge().translate("help")));
		helpMenu.setRolloverEnabled(true);
		//helpMenu.setMnemonic(getMnemo(menuBar, PeaProperties.getVmBridge().translate("help")));
		menuBar.add(helpMenu);

		menuPanel.add(menuBar);
		menuPanel.add(Box.createHorizontalGlue());
		topPanel.add(menuPanel);

		topPanel.add(Box.createVerticalStrut(5));

		JPanel errorMessagePanel = new JPanel();
		errorMessagePanel.setLayout(new BoxLayout(errorMessagePanel, BoxLayout.X_AXIS));	
		errorMessagePanel.setBackground(new Color(cipherColor[0], cipherColor[1], cipherColor[2]));
		errorMessageLabel = new JLabel("                                             ");
		errorMessageLabel.setForeground(Color.RED);
		errorMessageLabel.setPreferredSize(new Dimension(300, 20));
		errorMessageLabel.setMinimumSize(new Dimension(300, 20));
		errorMessageLabel.setMaximumSize(new Dimension(300, 20));
		errorMessagePanel.add(errorMessageLabel);
		errorMessagePanel.add(Box.createHorizontalGlue());
		topPanel.add(errorMessagePanel);
		topPanel.add(Box.createVerticalStrut(5));

		if (PeaProperties.isTestMode()) { // test mode
			JLabel rescueLabel = new JLabel("TEST MODE");
			topPanel.add(rescueLabel);

			if (PeaProperties.getFileType().equals("image")) { // test mode

				String names[] = ImageIO.getReaderFormatNames();
				StringBuilder builder = new StringBuilder();
				for (int i = 0; i < names.length; ++i) {
					builder.append("\"" + names[i] + "\",");
					//TestLog.o(PswDialogView.class, names[i]);
				}
				//TestLog.o(PswDialogView.class, builder.substring(0, builder.lastIndexOf(",")));
				TestLog.v(PswDialogView.class, "Supported image formats: " +  builder.substring(0, builder.lastIndexOf(",")));// delete the last ','

			}
		} else if (PeaProperties.isRescueMode()) { // rescue mode
			JLabel rescueLabel = new JLabel("*** RESCUE MODE ***" );
			rescueLabel.setForeground(Color.RED);
			topPanel.add(rescueLabel);
		}

		FilePanelHolder ftpHolder = FilePanelHolder.getInstance();
		ftpHolder.addMouseMotionListener(new MouseRandomCollector() );
		topPanel.add(Box.createVerticalStrut(10));
		topPanel.add(ftpHolder);

		topPanel.add(Box.createVerticalStrut(5));

		
		directStartPanel = new JPanel(); // TODO hide
		directStartPanel.setLayout(new BoxLayout(directStartPanel, BoxLayout.LINE_AXIS));
		JCheckBox directStartCheck = new JCheckBox(PeaProperties.getVmBridge().translate("enable_direct_start"));
		directStartCheck.setSelected(false);
		directStartCheck.addActionListener(this);
		directStartCheck.setActionCommand("directStart");
		directStartPanel.add(directStartCheck);
		directStartPanel.add(Box.createHorizontalGlue());
		topPanel.add(directStartPanel);
		// only show if keyfile-only files are selected
		if (PeaProperties.isKeyFileOnlyMode() == true) {
			directStartPanel.setVisible(true);
		} else {
			directStartPanel.setVisible(false);
		}


		JPanel pswLabelPanel = new JPanel();
		pswLabelPanel.setBackground(new Color(cipherColor[0], cipherColor[1], cipherColor[2]));
		pswLabelPanel.addMouseMotionListener(new MouseRandomCollector() );
		pswLabelPanel.setLayout(new BoxLayout(pswLabelPanel, BoxLayout.X_AXIS));
		topPanel.add(Box.createVerticalStrut(10));
		topPanel.add(pswLabelPanel);

		if (PeaSettings.getLabelText() == null) {
			pswLabel = new JLabel( PeaProperties.getVmBridge().translate("enter_password") );
		} else {
			pswLabel = new JLabel( PeaSettings.getLabelText() );
		}
		pswLabel.setPreferredSize(new Dimension( 460, 20));
		pswLabel.setLabelFor(pswField);
		pswLabelPanel.add(pswLabel);
		pswLabelPanel.add(Box.createHorizontalGlue());

		JPanel pswPanel = new JPanel();
		pswPanel.setLayout(new BoxLayout(pswPanel, BoxLayout.X_AXIS));
		pswPanel.setPreferredSize(new Dimension(400, 30));
		pswPanel.setMinimumSize(new Dimension(200, 30));
		pswPanel.setMaximumSize(new Dimension((int) screenWidth, 60)); 
		pswPanel.setBackground(new Color(cipherColor[0], cipherColor[1], cipherColor[2]));

		pswField = new JPasswordField();
		pswField.setBackground(new Color(231, 231, 231) );
		pswField.setPreferredSize(new Dimension(400, 30));
		pswField.setMinimumSize(new Dimension(200, 30));
		pswField.setMaximumSize(new Dimension((int) screenWidth, 60)); 
		pswField.addKeyListener(dialog);
		pswField.addKeyListener(new KeyRandomCollector() );
		pswPanel.add(pswField);

		JButton eyeButton = new JButton();
		ImageIcon eyeIcon = IconManager.loadIcon("eye.png", "eye",  "eye", "hide or show the password");
		if (eyeIcon != null) {
			eyeButton.setIcon(eyeIcon);
		} else {
			eyeButton.setText("see");
		}
		eyeButton.addChangeListener(new ChangeListener() {
			// accessibility: show password also using the keyboard
			@Override
			public void stateChanged(ChangeEvent cl) {
				if (cl.getSource() instanceof JButton) {
					JButton b = (JButton) cl.getSource();
					if (b.getModel().isPressed()) {
						pswField.setEchoChar((char) 0);
					} else {
						pswField.setEchoChar('*');
					}
				}					
			}				
		});

		// to remote the spacing between the image and button's borders
		eyeButton.setMargin(new Insets(0, 0, 0, 0));
		// to remove the border
		eyeButton.setBorder(null);
		pswPanel.add(Box.createHorizontalStrut(10));
		pswPanel.add(eyeButton);
		pswPanel.add(Box.createHorizontalStrut(10));

		topPanel.add(pswPanel);		
	

		JPanel okButtonPanel = new JPanel();
		okButtonPanel.setLayout(new BoxLayout(okButtonPanel, BoxLayout.X_AXIS));

		okButton = new JButton(PeaProperties.getVmBridge().translate("ok"));
		Font exFont = okButton.getFont(); // set larger font size
		okButton.setFont(new Font(exFont.getFamily(), exFont.getStyle(), exFont.getSize() + 4));
		//okButton.setFont(new Font(exFont.getFamily(), Font.BOLD, exFont.getSize() + 2));
		//okButton.setHorizontalAlignment(SwingConstants.RIGHT);//setAlignmentX(RIGHT_ALIGNMENT);
		okButton.setFocusPainted(true);
		okButton.setRolloverEnabled(true);
		okButton.getAccessibleContext().
		setAccessibleDescription(
				"Clicking this component decrypts and opens the selected files.");
		okButton.setPreferredSize(new Dimension( 400, 40));
		okButton.setMinimumSize(new Dimension( 200, 30));
		okButton.setMaximumSize(new Dimension( (int) screenWidth, 100));
		okButton.setActionCommand("ok");
		okButton.addActionListener(dialog);
		okButton.setBorder(PeaBorderFactory.getButtonBorder());
		// if there is no password, this might be the only 
		// chance to start EntropyPool
		okButton.addMouseMotionListener(new MouseRandomCollector() );
		okButtonPanel.add(okButton);	    

		topPanel.add(okButtonPanel);	
	}


	/**
	 * Initialize the PEA with a new password, new salt and new content
	 */
	private final static void initialize(){

		initializing = true;

		NewPasswordDialog.setRandomCollector(true);
		String hint = null;
		if (PeaProperties.getFileType().equals("file")){
			hint = PeaProperties.getVmBridge().translate("hint_initialization");
		}
		NewPasswordDialog newPswDialog = NewPasswordDialog.getInstance(
				dialog, PeaProperties.getVmBridge().translate("initialization"),
				hint, dialog.getLocation());
		char[] newPsw =newPswDialog.getDialogInput();
		if (newPsw == null) {
			if (PeaProperties.isKeyFileOnlyMode() == false) {
			TestLog.v(PswDialogView.class, "New password cancelled");
			initializing = false;
			return;
			} else {
				TestLog.v(PswDialogView.class, "Continue without password...");
			}
		} else {
			PeaControl.setInitializedPassword( newPsw);
		}


		NewPasswordDialog.setRandomCollector(false);

		String initError = PeaControl.getDialog().initializeNewFiles();
		if (initError != null && PeaProperties.getFileType().equals("image")) { // TODO other PEAs check (no double dialogs)
			PeaDialog.showMessage(JREProperties.getMainWindow(), 
					initError, PeaProperties.getVmBridge().translate("error"), 0);
			initializing = false;
			PeaControl.setInitializedPassword( null);
			return;
		}

		//		dialog.setVisible(false);
		if (PeaProperties.getFileType().equals("calendar")) {
			// new calendar is set
			//			return;
		}

		PeaControl.getDialog().startExecution(false);
	}	

	//===============================================
	// Getter & Setter

	public final static char[] getPassword() {
		return pswField.getPassword();
	}

	public final static void clearPassword() {
		if (pswField != null) {
			pswField.setText("");
		}
	}

	/**
	 * Set a message text to show in PswDialogView messageLabel
	 * 
	 * @param message	the message to show
	 * @param error		true if message is an error message (font color red)
	 * 					false: font color blue
	 * @param translate	use message text as language bundle string (translate)
	 */
	public final static void setMessage(String message, boolean error, boolean translate) {
		
		if (PeaProperties.isCommandLineMode() == true) {
			if (error == true) {
				TestLog.e(PswDialogView.class, message);
			} else {
				TestLog.o(PswDialogView.class, message);
			}
			return;
		}
		//TestLog.o(PswDialogView.class, "Messages shown in PswDialogView: "+message);
		if(errorMessageLabel == null && PeaProperties.isTestMode() == true) {
			TestLog.v(PswDialogView.class, "No messages shown in PswDialogView");
			return; // while testing...
		}
		if (error == true) {
			errorMessageLabel.setForeground(new Color(153, 0, 0));
		} else {
			errorMessageLabel.setForeground(new Color(0, 0, 153));
		}

		if (message != null) {

			if (translate == false) {//if (message.startsWith("###")){
				errorMessageLabel.setText(message);
				//TestLog.o(PswDialogView.class, "PswDialogView - setMessage: " + message);
			} else {
				try {
					String i18nMessage = PeaProperties.getVmBridge().translate(message);
					errorMessageLabel.setText(i18nMessage);
				} catch (Exception e) {
					errorMessageLabel.setText(message);
				}
			}	
		}
		errorMessageLabel.paintImmediately(errorMessageLabel.getVisibleRect());
		// after message: enable the button again
		//okButton.setEnabled(true);
	}


	/**
	 * Show a message dialog with links and email, related to this PEA
	 * 
	 * @param websiteFolder		folder with leading slash after the language,
	 * 							that is "/html/xxx_pea.html"
	 * @param downloadLink		full link to download the source code
	 */
	public final static void showInfoDialog(Window parent){

		String websiteFolder = PeaControl.getDialog().getWebsite();
		String downloadLink = PeaControl.getDialog().getSourceLink();

		String lang = System.getProperty("user.language"); // en, de
		if (lang != "de" && lang != "en") {
			lang = "en";
		}
		String infoSite = "http://eck.cologne/peafactory/" + lang + websiteFolder;

		JTextPane textPane = new JTextPane();
		textPane.setEditable(false);
		textPane.setText(
				PeaProperties.getVmBridge().translate("find_web_infos") + ": \n" 
						+ "     " + infoSite + "\n\n" 
						+ PeaProperties.getVmBridge().translate("general_infos") + ": \n" 
						+ "   http://eck.cologne/peafactory/" + lang + "/index.html\n\n" 
						+ PeaProperties.getVmBridge().translate("find_source_code") + ": \n" 
						+ "     " + downloadLink + "\n\n" 
						+ PeaProperties.getVmBridge().translate("contact") + ": \n" 
						+ "   peafactory@eck.cologne\n"
				);
		
		JOptionPane.showMessageDialog(parent, //PswDialogView.getView(),
				textPane,
	/*			PeaProperties.getVmBridge().translate("find_web_infos") + ": \n" 
				+ "     " + infoSite + "\n\n" 
				+ PeaProperties.getVmBridge().translate("general_infos") + ": \n" 
				+ "   http://eck.cologne/peafactory/" + lang + "/index.html\n\n" 
				+ PeaProperties.getVmBridge().translate("find_source_code") + ": \n" 
				+ "     " + downloadLink + "\n\n" 
				+ PeaProperties.getVmBridge().translate("contact") + ": \n" 
				+ "   peafactory@eck.cologne\n",								*/	
				PeaProperties.getVmBridge().translate("help"), 									   
				JOptionPane.INFORMATION_MESSAGE);	
	}

	/**
	 * Show a dialog with informations about the properties of the current PEA
	 * 
	 * @param peaType		the type or name of the PEA
	 * @param extraLine		an optional extra line for PEA dependent properties
	 */
	public final static void showAboutDialog(String peaType, String extraLine, Window parent) {


		String algoParams = null;
		String titleString = null;
		String keyFileProp = PeaProperties.getVmBridge().translate("with") + " "
				+ PeaProperties.getVmBridge().translate("key_file_property") + "\n";
		//String x = PeaProperties.getVmBridge().translate("with") + " ";
		if (PeaSettings.getKeyFileProperty() == false){
			keyFileProp = "";
			//x = PeaProperties.getVmBridge().translate("without") + " ";
		}
		if (extraLine == null){
			extraLine = " ";
		}

		if (parent instanceof PswDialogView) {
			String defaultAlgoParams = PeaProperties.getDefaultAlgoParamString();
			algoParams = AlgoParamHandler.getReadableString(defaultAlgoParams, true);
			titleString = PeaProperties.getVmBridge().translate("new_files");
			// show defaults
		} else {
			// show current algorithms
			if (PeaProperties.getAlgoParamString() == null) {
				TestLog.e(PswDialogView.class, "Missing current algorithm parameter string -> set from current algorithms...", 5);
				PeaProperties.setAlgoParamString(AlgoParamHandler.writeString(false).getBytes(AttachmentHandler.getASCIICharset()));
			}
			String currentAlgoParams = new String(PeaProperties.getAlgoParamString(), AttachmentHandler.getASCIICharset());
			algoParams = AlgoParamHandler.getReadableString(currentAlgoParams, true);
			titleString = PeaProperties.getVmBridge().translate("infos");
		}


		JTextPane textPane = new JTextPane();
		textPane.setEditable(false);
		textPane.setText(
				peaType + " - " + PeaProperties.getVmBridge().translate("part_of") + " PeaFactory\n" 
						+ PeaProperties.getVmBridge().translate("version") + ": " + PeaControl.getDialog().getVersion() + " (" + PeaControl.getDialog().getYearOfPublication() + ")\n" +
						keyFileProp + 
						extraLine + "\n"
						+ PeaProperties.getVmBridge().translate("used_primitives") + "\n"
						+ algoParams
				);

		PeaDialog.showMessage(parent, 
				textPane,
						titleString, 1);
	}

	/**
	 * Enable the button to start decryption. 
	 * This button was disabled when clicked.
	 */
	public static void enableOkButton() {
		if (okButton != null) {
			okButton.setEnabled(true);
		} else { // in command line mode OK
			if (PeaProperties.isCommandLineMode() == false) {
				TestLog.ve(PswDialogView.class, "Missing instance", 5);
			}
		}
	}

	public final WhileTypingThread getThread() {
		return t;
	}

	public final static Image getImage() {
		return image;
	}

	public final static PswDialogView getView() {
		return dialog;
	}

	public final static void setStarted(boolean _started) {
		started = _started;
	}

	public final static void showHideDirectStartPanel(boolean show) {
		directStartPanel.setVisible(show); // TODO TEST
	}
	
	/**
	 * @return the initializing
	 */
	public static boolean isInitializing() {
		return initializing;
	}

	/**
	 * @param initializing the initializing to set
	 */
	public static void setInitializing(boolean initialize) {
		PswDialogView.initializing = initialize;
	}

	/**
	 * @return the screenwidth
	 */
	public static double getScreenwidth() {

		return screenWidth;
	}

	/**
	 * @return the screenheight
	 */
	public static double getScreenheight() {

		return screenHeight;
	}



	//================================================
	// inner class
	public class WhileTypingThread extends Thread {
		// some pre-computations while typing the password
		@Override
		public void run() {		
			if (PeaControl.getDialog() != null) {
				PeaControl.getDialog().preComputeInThread();
			}
		}
	}
}