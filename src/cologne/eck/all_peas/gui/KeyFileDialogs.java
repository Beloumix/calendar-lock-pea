package cologne.eck.all_peas.gui;

import java.awt.Window;

/*
 * PeaFactory - Production of Password Encryption Archives
 * Copyright (C) 2021  Axel von dem Bruch
 * 
 * This library is free software; you can redistribute it and/or modify it 
 * under the terms of the GNU General Public License as published 
 * by the Free Software Foundation; either version 2 of the License, 
 * or (at your option) any later version.
 * This library is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
 * or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU General Public License for more details.
 * See:  http://www.gnu.org/licenses/gpl-2.0.html
 * You should have received a copy of the GNU General Public License 
 * along with this library.
 */

import java.io.File;
import java.io.IOException;

import cologne.eck.all_peas.control.CommandLineHandler;
import cologne.eck.all_peas.data.PeaProperties;
import cologne.eck.all_peas.vm_specific.JREProperties;
import cologne.eck.peafactory.crypto.CipherStuff;
import cologne.eck.peafactory.crypto.HashStuff;
import cologne.eck.peafactory.crypto.KeyFileHandler;
import cologne.eck.tools.Converter;
import cologne.eck.tools.PropertyHandler;
import cologne.eck.tools.TestLog;
import cologne.eck.tools.Zeroizer;


public class KeyFileDialogs {
	
	//private static String probablyNewDefaultKeyFileName = null;
	
	private static File newChosenKeyFile = null;
	
	private static boolean newChosenDefaultKeyFile = false;
	
	/**
	 * For manual tests....
	 * @param args
	 */
/*	public static void main(String[] args) {		
		PeaProperties.setVmBridge(new JREBridge());
		JREProperties.setTypePanel(new FileTypePanel(0, 0, true, false, 
				new FileComposer(
						new FileModel(true, AttachmentHandler.getVersion2ID(), new byte[KeyDerivation.getSaltSize()])), args));
		HashStuff.setHashAlgo(new Blake2bDigest());
		EntropyPool pool = EntropyPool.getInstance();
		pool.updateThread(1);		
		int createSuccess = setKeyFileStatus(true);
		TestLog.o(KeyFileDialogs.class, "Created key file: " + createSuccess);
	}*/
	/**
	 * Apply the key file (default or user chosen)
	 * 
	 * @param keyMaterial	the key material from KDF
	 * 
	 * @return	hash of keyMaterial and key file hash (HKDF)
	 */
	public static byte[] applyKeyFile(byte[] keyMaterial) {

		if (keyMaterial == null) {
			TestLog.e(KeyFileDialogs.class, "Missing keyMaterial...");		
			TestLog.ve(KeyFileDialogs.class, "Missing keyMaterial...", 5);		
			PeaDialog.showMessage(JREProperties.getMainWindow(), 
					PeaProperties.getVmBridge().translate("unexpected_error"), 
					PeaProperties.getVmBridge().translate("error"), 0); 
			return null;
		}

		byte[] keyFileHash = null;

		// Always use the file from command line mode if available, even when default keyfile was set
		if ((PeaProperties.isCommandLineMode() == true) && CommandLineHandler.getCommandLineKeyFileName() != null) {
			
			TestLog.v(KeyFileDialogs.class, "Command line mode: " +  PeaProperties.isCommandLineMode() + " with keyfile:" + CommandLineHandler.getCommandLineKeyFileName());	
			File keyFile = new File(CommandLineHandler.getCommandLineKeyFileName());
			// generate a hash from this file:		 
			keyFileHash = getChosenKeyFileHash(keyFile);	
		} else {
			int keyFileFlag = PeaProperties.getKeyFileFlag();
	/*		int keyFileFlag = -1;
			if (JREProperties.getMainWindow() == null && PeaProperties.isCommandLineMode() == true) {
				keyFileFlag = PeaProperties.getKeyFileFlag();	
			} else if (JREProperties.getMainWindow() instanceof PswDialogView) {
				// default
				keyFileFlag = PeaProperties.getKeyFileFlag();			
			} else if (JREProperties.getMainWindow() instanceof PeaLockFrame) {
				// open file
				keyFileFlag = PeaProperties.getKeyFileFlag();
			} else {
				TestLog.e(KeyFileDialogs.class, "Invalid main window");
				TestLog.ve(KeyFileDialogs.class, "Invalid main window", 5);		
				PeaDialog.showMessage(JREProperties.getMainWindow(), 
						PeaProperties.getVmBridge().translate("unexpected_error"), 
						PeaProperties.getVmBridge().translate("error"), 0);
				return null;
			}		*/
			TestLog.v(KeyFileDialogs.class, "keyFileFlag:" + keyFileFlag);	


			// Get the key file hash
			if (keyFileFlag == 0) {
				return keyMaterial;
			} else if (keyFileFlag == 1) {
				// use default
				keyFileHash = getDefaultKeyFileHash();
			} else if (keyFileFlag == 2) {
				// choose manually
				keyFileHash = getChosenKeyFileHash(null);				
			}
		}

		if (keyFileHash == null) { // probably cancelled user chosen key file
			TestLog.e(KeyFileDialogs.class, "Missing key file hash");		
			return null;
		}		
		// Use HKDF 
		byte[] result =  HashStuff.generateHKDFBytes(keyMaterial, 
				keyFileHash, 
				CipherStuff.getKeySize() ); // size 
		Zeroizer.zero(keyMaterial);
		Zeroizer.zero(keyFileHash);
		return result;
	}

	/**
	 * Ask for kind of key file, 
	 * Create or choose a new or no key file. 
	 * If this is a default key file, store in properties. 
	 * 
	 * @param changeDefault	true: change default key file properties
	 * 
	 * @return	-1 for cancel or error, 0 for no key file, 1 for default, 2 for manual chosen
	 */
	public static int setKeyFileStatus(boolean changeDefault) {
		
		String[] options = { 
				PeaProperties.getVmBridge().translate("gen_key_file_automatically_opened"),
				PeaProperties.getVmBridge().translate("no_key_file"),
				PeaProperties.getVmBridge().translate("gen_key_file_manually_opened"),
				PeaProperties.getVmBridge().translate("choose_key_file_manually_opened"),
				PeaProperties.getVmBridge().translate("choose_key_file_automatically_opened")
				};

		int chosenType = PeaDialog.showOptionYAxis(JREProperties.getMainWindow(), 
				PeaProperties.getVmBridge().translate("choose_key_file_kind") + "\n" +
				PeaProperties.getVmBridge().translate("default_key_file_info"),
						PeaProperties.getVmBridge().translate("key_file"), 3, options, 0);
		TestLog.o(KeyFileDialogs.class, "Chosen option: " + chosenType);		
		
		boolean createKeyFile = false;
		int keyFileFlag = -1;
		if (chosenType == -1 ) { // cancel
			return -1;
		} else if (chosenType == 1) { // no key file
			keyFileFlag = 0;
			createKeyFile = false;
		} else if (chosenType == 0) { // generate default
			keyFileFlag = 1;
			createKeyFile = true;
		} else if (chosenType == 2 ) { // select manually
			keyFileFlag = 2;
			createKeyFile = true;
		} else if (chosenType == 3 ) { // select manually
			keyFileFlag = 2;
			createKeyFile = false;
		} else if (chosenType == 4 ) { // select manually for default
			if (PeaProperties.getDefaultKeyFileName() != null) {
				PeaDialog.showMessage(JREProperties.getMainWindow(), 
						JREProperties.getVmBridge().translate("default_key_file_selected"),
						//+ "\n " + JREProperties.getVmBridge().translate("ignore_and_go_on") + "?", // ignore_and_go_on
						JREProperties.getVmBridge().translate("error"), 
						0);
				return -1;
			} else {
				keyFileFlag = 1;
				createKeyFile = false;
			}
		} else {
			return -1;
		}
		
		// Set the flags and properties for no key file
		if (keyFileFlag == 0) {// no key file
			if (changeDefault == true) {
				PeaProperties.setDefaultKeyFileFlag(0); // default 
				PeaProperties.setDefaultKeyFileName(null);
			} else {
				PeaProperties.setKeyFileFlag(0); // actual 
			}
			return 0;
		}		
		File keyFile = null;
		if (createKeyFile == true) {
			keyFile = generateKeyFile(null);
			if (keyFile == null) {
				return -1;
			}
		} else {			
			keyFile = manuallyChooseNewKeyFile();
			TestLog.v(KeyFileDialogs.class, "Chosen key file... ");
			if (keyFile == null) {			
				return -1;
			}
		}		
		// Set key file flags and properties:
		if (keyFileFlag == 1) {
			TestLog.v(KeyFileDialogs.class, "Set default key file...");
			PeaProperties.setDefaultKeyFileName(keyFile.getAbsolutePath());
			// store in pea.properties
			String propError = new PropertyHandler().setOneProperty("key_file", keyFile.getAbsolutePath());
			if (propError != null) {
				// no property file e.g. first start -> set file
				new PropertyHandler().updateAllPropValues();
				propError = new PropertyHandler().setOneProperty("key_file", keyFile.getAbsolutePath());
				if (propError != null) {
					TestLog.ve(KeyFileDialogs.class, "Could not store default key file in poperties: " + propError, 5);
					PeaDialog.showMessage(JREProperties.getMainWindow(), 
							PeaProperties.getVmBridge().translate("operation_failed") + "\n"
									+ propError + "\n\n"
									+ PeaProperties.getVmBridge().translate("select_file_when_open_files"), 
									PeaProperties.getVmBridge().translate("unexpected_error"), 0);
				}
			}
			PeaDialog.showMessage(JREProperties.getMainWindow(), 					
					PeaProperties.getVmBridge().translate("key_file_must_be_present") + ": \n\n" 
			+ keyFile.getAbsolutePath() + "\n\n"
			+ PeaProperties.getVmBridge().translate("never_modify_or_delete") ); 
			
			if (changeDefault == true) {
				PeaProperties.setDefaultKeyFileFlag(1); // default 
			} else {
				PeaProperties.setKeyFileFlag(1); //actual file
			}

		} else if (keyFileFlag == 2) {
			PeaDialog.showMessage(JREProperties.getMainWindow(), 					
					PeaProperties.getVmBridge().translate("select_file_when_open_files") + ": \n\n" 
							+ keyFile.getAbsolutePath()  + "\n\n"
							+ PeaProperties.getVmBridge().translate("never_modify_or_delete") ); 
			// manual selection
			if (changeDefault == true) {
				PeaProperties.setDefaultKeyFileFlag(2); // default 
			} else {
				PeaProperties.setKeyFileFlag(2); //actual file
			}
		}  else {
			return -1;
		}		
		return keyFileFlag;
	}
	
	public static File generateKeyFile(Window owner) {
		if (owner == null) {
			owner = JREProperties.getMainWindow();
		}
		int rInt = PeaProperties.getRandom().nextInt();
		int randomInt = 123456789;
		if (rInt != Integer.MIN_VALUE) { // returns negative value
			randomInt = Math.abs( rInt );
		} else {
			randomInt = Math.abs( rInt + 1 );
		}
		String randomString =  Converter.bytes2hex( Converter.int2bytesBE(randomInt) ); //Integer.toString(randomInt);
		// 2. Select file name and directory 
		File defaultDir = new File(System.getProperty("user.dir"));
		File resDir  = new File(System.getProperty("user.dir") + File.separator 
				+ "resources");
		if (resDir.exists() && resDir.isDirectory()) {
			defaultDir = resDir;
		} else {
			// create directory
			boolean resDirSuccess = resDir.mkdirs();
			if (resDirSuccess == true) {
				defaultDir = resDir;
				new PropertyHandler().updateAllPropValues();
			}
		}
		PeaFileChooser pfc = new PeaFileChooser(owner); //JREProperties.getMainWindow());
		pfc.setCurrentDirectory(defaultDir);
		pfc.customizeFileChooser(null, 
				PeaProperties.getVmBridge().translate("select_folder_for_key_file"), 
				true);
		pfc.setFileSelectionMode(PeaFileChooser.FILES_ONLY);
		pfc.setSelectedFile(new File(randomString ));
		int selectionResult = pfc.showOpenDialog(owner);//JREProperties.getMainWindow());
		if (selectionResult != PeaFileChooser.APPROVE_OPTION) {
			return null;
		}
		File keyFile = pfc.getSelectedFile();	
		if (keyFile == null) {			
			return null;
		}
		TestLog.v(KeyFileDialogs.class, keyFile.getAbsolutePath());

		if (keyFile.exists()) {
			PeaDialog.showMessage(owner,//JREProperties.getMainWindow(), 
					PeaProperties.getVmBridge().translate("file_already_exists")
					+ "\n" + 
					keyFile.getAbsolutePath() +"\n" + 
							PeaProperties.getVmBridge().translate("select_new_file"), 
							PeaProperties.getVmBridge().translate("error"), 0);
			return null;
		}

		// 3. Create empty file
		if (keyFile.getParentFile() != null) {
			keyFile.getParentFile().mkdirs(); 
		}
		try {
			keyFile.createNewFile();
		} catch (IOException e) {
			TestLog.e(KeyFileDialogs.class, e.getLocalizedMessage());
			PeaDialog.showMessage(owner,//JREProperties.getMainWindow(), 
					e.getLocalizedMessage(), 
					PeaProperties.getVmBridge().translate("error"), 0);
			return null;
		}
		String error = KeyFileHandler.createKeyFile(keyFile);
		if (error != null) {
			PeaDialog.showMessage(owner, //JREProperties.getMainWindow(), 
					PeaProperties.getVmBridge().translate("operation_failed") + "\n" + error, 
					PeaProperties.getVmBridge().translate("error") + ": " + PeaProperties.getVmBridge().translate("key_file"), 0);
			return null;
		}		
		return keyFile;
	}	
	
	/**
	 * Choose any file as key file. 
	 * This will check the size and the entropy of the file
	 * and warn if this is not appropriate
	 * 
	 * @return	the user chosen key file
	 */
	public static File manuallyChooseNewKeyFile() {
		TestLog.v(KeyFileDialogs.class,  "Manually choose key file...");
		PeaFileChooser pfc = new PeaFileChooser(JREProperties.getMainWindow());
		pfc.setFileSelectionMode(PeaFileChooser.FILES_ONLY);
		pfc.showOpenDialog(JREProperties.getMainWindow());
		File keyFile = pfc.getSelectedFile();
		
		if (keyFile == null) {
			return null;
		}
		
		long keyFileSize = keyFile.length();
		// Check the file
		if (keyFileSize > 1024 * 1024 * 16) { // 16 MB
			//  warn performance			
			int largeWarning = PeaDialog.showQuestion(JREProperties.getMainWindow(), 
					PeaProperties.getVmBridge().translate("warning") + "\n\n" +
					keyFile.getAbsolutePath() + "\n " + PeaProperties.getVmBridge().translate("size") + ": " + keyFile.length() + "\n "
					+ PeaProperties.getVmBridge().translate("size_warning"), 
					PeaProperties.getVmBridge().translate("warning"), 2);
			if (largeWarning != 0) {
				return null;
			}
		}
		if (keyFileSize < 1024) {
			// check if this is a generated key file:
			boolean isHexInt = false;
			if (keyFileSize == KeyFileHandler.getKeyFileSize()) {		
				if (keyFile.getName().contains(".")) {
					TestLog.v(KeyFileDialogs.class, "This seems to be not a generated key file: " + keyFile.getName());
				} else {
					try { 
						Integer.parseInt(keyFile.getName(), 16);
						isHexInt = true;
					} catch (Exception e ) {
						TestLog.v(KeyFileDialogs.class, "This seems to be not a generated key file: " + keyFile.getName());
					}
				}
			}
			if (isHexInt == false) {
				// warn small size
				int smallWarning = PeaDialog.showQuestion(JREProperties.getMainWindow(), 
						PeaProperties.getVmBridge().translate("warning") + "\n\n  " +
								keyFile.getAbsolutePath() + "\n  " + PeaProperties.getVmBridge().translate("size") + ": " + keyFile.length() + "\n"
								+ "  " + PeaProperties.getVmBridge().translate("should_be") + " > " + 1024
								+ "\n\n" + PeaProperties.getVmBridge().translate("continue") + "?",  
								PeaProperties.getVmBridge().translate("warning"), 2);
				if (smallWarning != 0) {
					return null;
				}
			}
		}
		TestLog.v(KeyFileDialogs.class, "Set manually chosen key file to check...");
		newChosenKeyFile = keyFile;
		return keyFile;
	} 
	
	/**
	 * Get the Blake2b hash of default key file
	 * 
	 * @return	the hash of the key file
	 */
	public static byte[] getDefaultKeyFileHash() {
		String keyFileName = PeaProperties.getDefaultKeyFileName();
		if (keyFileName == null) {
			TestLog.ve(KeyFileDialogs.class, "Missing key file in properties " , 5);
			PeaDialog.showMessage(JREProperties.getMainWindow(), 
					PeaProperties.getVmBridge().translate("key_file") + " " + PeaProperties.getDefaultKeyFileName() + "\n"
					+ PeaProperties.getVmBridge().translate("file_not_found") +"\n\n"
					+ PeaProperties.getVmBridge().translate("choose_key_file"),
					PeaProperties.getVmBridge().translate("error"), 0);
			
			PeaFileChooser pfc = new PeaFileChooser(JREProperties.getMainWindow());
			pfc.setFileSelectionMode(PeaFileChooser.FILES_ONLY);
			pfc.customizeFileChooser(null, 
					PeaProperties.getVmBridge().translate("choose_key_file"),  
					true);
			pfc.showOpenDialog(JREProperties.getMainWindow());
			File keyFile = pfc.getSelectedFile();
			if (keyFile != null) {
				PeaProperties.setDefaultKeyFileName(keyFile.getAbsolutePath());
				// register to reset if this keyfile fails
				newChosenDefaultKeyFile = true;
				return KeyFileHandler.readAndHashKeyFile(keyFile);
			} else {
				TestLog.v(KeyFileDialogs.class, "Opening key file cancelled...");
				return null;
			}
			//keyFileName = keyFile.getAbsolutePath();
		}
		File keyFile = new File(keyFileName );
		if (keyFile.exists() == false) {
			TestLog.ve(KeyFileDialogs.class, "Key file does not exist: " + keyFileName, 5);
			PeaDialog.showMessage(JREProperties.getMainWindow(), 
					PeaProperties.getVmBridge().translate("key_file") + " " + PeaProperties.getDefaultKeyFileName() + "\n"
					+ PeaProperties.getVmBridge().translate("file_not_found") +"\n\n"
					+ PeaProperties.getVmBridge().translate("choose_key_file"),
					PeaProperties.getVmBridge().translate("error"), 0);
			
			PeaFileChooser pfc = new PeaFileChooser(JREProperties.getMainWindow());
			pfc.setFileSelectionMode(PeaFileChooser.FILES_ONLY);
			pfc.customizeFileChooser(null, 
					PeaProperties.getVmBridge().translate("choose_key_file"),  
					true);
			pfc.showOpenDialog(JREProperties.getMainWindow());
			keyFile = pfc.getSelectedFile();
			
			if (keyFile != null) {
				if (keyFile.canRead() == true) {
					PeaProperties.setDefaultKeyFileName(keyFile.getAbsolutePath());
					//probablyNewDefaultKeyFileName = keyFile.getAbsolutePath();
					// register to reset if this keyfile fails
					newChosenDefaultKeyFile = true;
					return KeyFileHandler.readAndHashKeyFile(keyFile);
				} else {
					TestLog.v(KeyFileDialogs.class, "Opening key file cancelled...");
					return null;
				}
			} else {
				TestLog.v(KeyFileDialogs.class, "Opening key file cancelled...");
				return null;
			}
		}		
		if (keyFile.canRead() == false) {
			TestLog.ve(KeyFileDialogs.class, "Key file does not exist: " + keyFileName, 5);
			PeaDialog.showMessage(JREProperties.getMainWindow(), 
					PeaProperties.getVmBridge().translate("key_file") + " " + PeaProperties.getDefaultKeyFileName() + "\n"
					+ PeaProperties.getVmBridge().translate("no_access") ,
					PeaProperties.getVmBridge().translate("error"), 0);
			return null;
		}
		return KeyFileHandler.readAndHashKeyFile(keyFile);
	}
	
	/**
	 * Get the Blake2b hash of a manually chosen file
	 * 
	 * @param	the keyfile (command line mode) or null (file chooser)
	 * 
	 * @return	the hash of the key file
	 */
	private static byte[] getChosenKeyFileHash(File kf) {
		
		File keyFile =  null;
		if (kf == null) {
			PeaFileChooser pfc = new PeaFileChooser(JREProperties.getMainWindow());
			pfc.setFileSelectionMode(PeaFileChooser.FILES_ONLY);
			pfc.customizeFileChooser(null, 
					PeaProperties.getVmBridge().translate("choose_key_file"),  
					true);
			pfc.showOpenDialog(JREProperties.getMainWindow());
			keyFile = pfc.getSelectedFile();
		} else {
			keyFile = kf;
		}
		if (keyFile != null) {
			if (newChosenKeyFile != null) {
				// check, if this is the chosen file:
				if ( keyFile.equals(newChosenKeyFile)) {
					// reset the newChosenKeyFile
					TestLog.v(KeyFileDialogs.class, "Chosen key file is OK");
					newChosenKeyFile = null;
				} else {
					TestLog.e(KeyFileDialogs.class, "Opening key file is not the previously selected one...");
					return null;
				}
			}
			return KeyFileHandler.readAndHashKeyFile(keyFile);
		} else {
			TestLog.v(KeyFileDialogs.class, "Opening key file cancelled...");
			return null;
		}
	}

	/**
	 * @return the newChosenDefaultKeyFile
	 */
	public static boolean isNewChosenDefaultKeyFile() {
		return newChosenDefaultKeyFile;
	}

	/**
	 * @param newChosenDefaultKeyFile the newChosenDefaultKeyFile to set
	 */
	public static void setNewChosenDefaultKeyFile(boolean newChosenDefaultKeyFile) {
		KeyFileDialogs.newChosenDefaultKeyFile = newChosenDefaultKeyFile;
	}
	
	/**
	 * Get the probably new default key file name. 
	 * This value is set, when a default key file is missing
	 * and a new default key file was chosen. 
	 * 
	 * @return	the probably new key file name
	 */
	/*public static String getProbablyNewDefaultKeyFileName() {
		return KeyFileDialogs.probablyNewDefaultKeyFileName;
	}*/
	
	/**
	 * Reset the probably new default key file name. 
	 * This value is set, when a default key file is missing
	 * and a new default key file was chosen. 
	 */
	/*public static void resetProbablyNewDefaultKeyFileName() {
		KeyFileDialogs.probablyNewDefaultKeyFileName = null;
	}*/
}
