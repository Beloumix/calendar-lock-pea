package cologne.eck.all_peas.gui;

import java.awt.Component;
import java.awt.Window;
import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;
import javax.swing.SwingUtilities;

import cologne.eck.all_peas.vm_specific.JREProperties;



/*
	Use with:
	
		this.getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW)
        .put(KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), "CLOSE_ON_ESCAPE");
		this.getActionMap().put("CLOSE_ON_ESCAPE", new CloseOnEscapeAction() );
		
		or:
		
		this.getRootPane().getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW)
        .put(KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), "CLOSE_ON_ESCAPE");
		this.getRootPane().getActionMap().put("CLOSE_ON_ESCAPE", new CloseOnEscapeAction() );
 */

@SuppressWarnings("serial")
public class CloseOnEscapeAction extends AbstractAction {

/*	public CloseOnEscapeAction() {}
	public CloseOnEscapeAction(String arg0) {}
	public CloseOnEscapeAction(String arg0, Icon arg1) {}*/

	@Override
	public void actionPerformed(ActionEvent ae) {
		Object source = ae.getSource();
		if (source instanceof java.awt.Component) {
			// If other dialog is open except main window, set focus on this, 
			// because dialog was probably opened from this dialog e.g. DatePicker from EventSetter
			SwingUtilities.getWindowAncestor( (Component) ae.getSource() ).dispose();
			Window[] wins = Window.getWindows();
			for (Window win : wins) {
				if (win.isVisible() && win.isFocusable() && ( ! win.equals(JREProperties.getMainWindow()))){
					win.requestFocus();
					return;
				}
			}
			Window w = JREProperties.getMainWindow();
			if (w != null && w.isVisible() == true) { //displayable, focusable, visible
				w.requestFocus();
			}
		}
	}
}
