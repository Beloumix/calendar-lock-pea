package cologne.eck.all_peas.gui;

import java.awt.Color;
import java.awt.Font;
import java.awt.Point;
import java.awt.Window;

import javax.swing.JFrame;
import javax.swing.JTextArea;
import javax.swing.border.EmptyBorder;

import cologne.eck.all_peas.data.PeaProperties;
import cologne.eck.all_peas.vm_specific.JREProperties;
import cologne.eck.tools.TestLog;

@SuppressWarnings("serial")
public class Toaster extends JFrame {
	
	private Toaster(String text) {
		setDefaultCloseOperation(EXIT_ON_CLOSE);
        setUndecorated(true);
        setFocusableWindowState(false);
        JTextArea area = new JTextArea();
        area.setFont(new Font(Font.SANS_SERIF, Font.PLAIN, PeaProperties.getFontSize()));
        area.setText(text);
        area.setBorder(new EmptyBorder(7,7,7,7));
        this.add(area);
		this.setBackground(new Color(215, 215, 215, 190));
		area.setBackground(new Color(0, 0, 0, 0));
        this.pack();
	}
	/**
	 * Show a toast message in the middle of the owner window
	 * for some seconds
	 * 
	 * @param owner		owner window
	 * @param text		text to show
	 * @param seconds	delay in seconds
	 */
	public static void toast(Window owner, String text, int seconds) {
		if (text == null || seconds <= 0) {
			return;
		}
		if (owner == null || owner.isVisible() == false) {
			owner = JREProperties.getMainWindow();
		}
		Toaster t = new Toaster(text);
		// set in the middle of the owner
		int ownerWidth = owner.getWidth();
		int ownerHeight = owner.getHeight();
		int toastWidth = (int) t.getPreferredSize().getWidth();
		int toastHeight = (int) t.getPreferredSize().getHeight();
		int x = 0;
		if (ownerWidth > toastWidth) {
			x = (int) (owner.getX() + Math.abs((owner.getWidth() - t.getPreferredSize().getWidth()) / 2));
		} else {
			x = owner.getX();
		}
		int y = 0;
		if (ownerHeight > toastHeight) {
			y = (int) (owner.getY() + Math.abs((owner.getHeight() - t.getPreferredSize().getHeight()) / 2));
		} else {
			y = owner.getY();
		}
		Point loc = new Point(x,y);
		t.setLocation(loc);
		t.setVisible(true);
		
		new Thread(){
            public void run() {
                try {
                    Thread.sleep(seconds * 1000);
                    t.dispose();
                } catch (InterruptedException e) {
                    TestLog.e(Toaster.class, e.getLocalizedMessage());
                }
            }
        }.start();
	}
}
