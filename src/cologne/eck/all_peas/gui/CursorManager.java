package cologne.eck.all_peas.gui;

/*
 * PeaFactory - Production of Password Encryption Archives
 * Copyright (C) 2015  Axel von dem Bruch
 * 
 * This library is free software; you can redistribute it and/or modify it 
 * under the terms of the GNU General Public License as published 
 * by the Free Software Foundation; either version 2 of the License, 
 * or (at your option) any later version.
 * This library is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
 * or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU General Public License for more details.
 * See:  http://www.gnu.org/licenses/gpl-2.0.html
 * You should have received a copy of the GNU General Public License 
 * along with this library.
 */
import java.awt.Cursor;
import java.awt.Image;
import java.awt.Point;
import java.awt.Toolkit;
import java.io.File;
import java.net.URL;

import javax.swing.ImageIcon;

import cologne.eck.tools.TestLog;



public class CursorManager {

	  private static final Cursor DEFAULT_CURSOR = 
	    Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR);
	  private static Cursor waitCursor = null;
	  
	  /**
	   * Get the wait cursor (hour glass) to indicate that
	   * a process is working
	   * Window win = SwingUtilities.getWindowAncestor(this).setCursor(CursorManager.getWaitCursor());
	   * try 
	   * 	if (win != null)
	   * 	win.setCursor(CursorManager.getWaitCursor());
	   * finally 
	   *	if (win != null) 
	   *	win.setCursor(CursorManager.getDefaultCursor());
	   * 
	   * @return	the cursor (hour glass)
	   */
	  public static Cursor getWaitCursor(){
		  if (waitCursor == null) {
			  
			  Image cursorImage = null;
			  try {
					// get resource from jar
					URL cursorUrl = CursorManager.class.getResource("/resources/wait_cursor.png");
					// get image from resource
					cursorImage = new ImageIcon(cursorUrl).getImage();					
				} catch (Exception e) {
					// for PeaFactory
					// get image from file
					cursorImage = new ImageIcon("resources" + File.separator + "wait_cursor.png").getImage();	
				}
				try{
					// create cursor
					waitCursor = Toolkit.getDefaultToolkit().createCustomCursor(cursorImage,
							// this does not work:
		               // new Point((int)win.getLocation().getX(),(int) win.getLocation().getY()),
		                new Point(0,0),
		                "wait_cursor");
				} catch (Exception e) {
					TestLog.e(CursorManager.class, "CursorManager: " 
							+ e.toString() + ", " + e.getMessage() 
							+ "\nset default wait cursor");
					// set predefined wait cursor instead
					waitCursor = Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR);
				}
		  }
		  return waitCursor;
	  }

	/**
	 * @return the defaultCursor
	 */
	public static Cursor getDefaultCursor() {
		return DEFAULT_CURSOR;
	} 
}