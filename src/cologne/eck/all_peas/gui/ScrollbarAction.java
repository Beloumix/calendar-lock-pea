package cologne.eck.all_peas.gui;


import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;

import javax.swing.AbstractAction;
import javax.swing.BoundedRangeModel;
import javax.swing.JComponent;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.KeyStroke;

import cologne.eck.tools.TestLog;


@SuppressWarnings("serial")
public class ScrollbarAction extends AbstractAction {

	/*
	 * 	Use like:
	 * 
 	// page up and page down buttons:
	KeyStroke pageup = KeyStroke.getKeyStroke(KeyEvent.VK_PAGE_UP, 0, false);
	KeyStroke pagedown = KeyStroke.getKeyStroke(KeyEvent.VK_PAGE_DOWN, 0, false);
	// register:
	contentPane.registerKeyboardAction(
			new ScrollbarAction("up", scrollPane.getVerticalScrollBar().getModel()), "up", pageup, JComponent.WHEN_IN_FOCUSED_WINDOW); 
	contentPane.registerKeyboardAction(
			new ScrollbarAction("down", scrollPane.getVerticalScrollBar().getModel()), "down", pagedown, JComponent.WHEN_IN_FOCUSED_WINDOW); 

	// up, down, left, right buttons:
	KeyStroke up = KeyStroke.getKeyStroke(KeyEvent.VK_UP, 0, false);
	KeyStroke down = KeyStroke.getKeyStroke(KeyEvent.VK_DOWN, 0, false);
	KeyStroke left = KeyStroke.getKeyStroke(KeyEvent.VK_LEFT, 0, false);
	KeyStroke right = KeyStroke.getKeyStroke(KeyEvent.VK_RIGHT, 0, false);
	// register:	
	contentPane.registerKeyboardAction(
			new ScrollbarAction("up", scrollPane.getVerticalScrollBar().getModel()), "up", up, JComponent.WHEN_IN_FOCUSED_WINDOW); 
	contentPane.registerKeyboardAction(
			new ScrollbarAction("down", scrollPane.getVerticalScrollBar().getModel()), "down", down, JComponent.WHEN_IN_FOCUSED_WINDOW); 
	contentPane.registerKeyboardAction(
			new ScrollbarAction("left", scrollPane.getHorizontalScrollBar().getModel()), "left", left, JComponent.WHEN_IN_FOCUSED_WINDOW); 
	contentPane.registerKeyboardAction(
			new ScrollbarAction("right", scrollPane.getHorizontalScrollBar().getModel()), "right", right, JComponent.WHEN_IN_FOCUSED_WINDOW); 
	 */

	// the model of the JScrollBar
	private BoundedRangeModel scrollBarModel;

	// higher values result in faster scrolling:
	private static final int scrollbarValue = 25;
	
	/**
	 * ScrollBar action to scroll with keys
	 * 
	 * @param direction	the directions as String: up, down, (page up, page down) left, right
	 * @param model	the model of the JScrollBar ( scrollBar.getModel() )
	 */
	private ScrollbarAction(String direction, BoundedRangeModel model) {
		super(direction);
		this.scrollBarModel = model;
	}

	/**
	 * Register left, right, up, down keys for keyboard access of a JScrollPane
	 * 
	 * @param panel			the JPanel e.g. the content pane or the JREProperties.getMainWindow()
	 * @param scrollPane	the JScrollPane to access
	 * @param pageKeysOnly	register only page up/down, not up/down keys, always register left/right
	 */
	public static void getScrollPaneKeyboardAccess(JPanel panel, JScrollPane scrollPane, boolean pageKeysOnly) {
		if (panel == null) {
			TestLog.ve(ScrollbarAction.class, "Missing panel", 5);
			return;
		}
		if (scrollPane == null) {
			TestLog.ve(ScrollbarAction.class, "Missing scroll pane", 5);
			return;
		}
		//	register keys for keyboard access:
		KeyStroke up = null;
		KeyStroke down = null;
		if (pageKeysOnly == false) {
			up = KeyStroke.getKeyStroke(KeyEvent.VK_UP, 0, false);
			down = KeyStroke.getKeyStroke(KeyEvent.VK_DOWN, 0, false);
		} else {
			up = KeyStroke.getKeyStroke(KeyEvent.VK_PAGE_UP, 0, false);
			down = KeyStroke.getKeyStroke(KeyEvent.VK_PAGE_DOWN, 0, false);
		}
		KeyStroke left = KeyStroke.getKeyStroke(KeyEvent.VK_LEFT, 0, false);
		KeyStroke right = KeyStroke.getKeyStroke(KeyEvent.VK_RIGHT, 0, false);
		if (scrollPane.getVerticalScrollBar() != null && scrollPane.getVerticalScrollBar().getModel() != null) {
			panel.registerKeyboardAction(
					new ScrollbarAction("up", scrollPane.getVerticalScrollBar().getModel()), "up", up, JComponent.WHEN_IN_FOCUSED_WINDOW); 
			panel.registerKeyboardAction(
					new ScrollbarAction("down", scrollPane.getVerticalScrollBar().getModel()), "down", down, JComponent.WHEN_IN_FOCUSED_WINDOW);
		}
		if (scrollPane.getHorizontalScrollBar() != null && scrollPane.getHorizontalScrollBar().getModel() != null) {
			panel.registerKeyboardAction(
					new ScrollbarAction("left", scrollPane.getHorizontalScrollBar().getModel()), "left", left, JComponent.WHEN_IN_FOCUSED_WINDOW); 
			panel.registerKeyboardAction(
					new ScrollbarAction("right", scrollPane.getHorizontalScrollBar().getModel()), "right", right, JComponent.WHEN_IN_FOCUSED_WINDOW);
		}
	}
	
	@Override
	public void actionPerformed(ActionEvent ae) {
		String direction = getValue(AbstractAction.NAME).toString();
		if (direction != null && scrollBarModel != null) {
			int value = scrollBarModel.getValue();
			if (direction.equals("up")) {
				value -= scrollbarValue;
				scrollBarModel.setValue(value);
			} else if (direction.equals("down")) {
				value += scrollbarValue;
				scrollBarModel.setValue(value);
			} else if (direction.equals("left")) {
				value -= scrollbarValue;
				scrollBarModel.setValue(value);
			} else if (direction.equals("right")) {
				value += scrollbarValue;
				scrollBarModel.setValue(value);
			}
		} else {
			TestLog.ve(getClass(), "Missing direction or model", 5);
		}
	}
}