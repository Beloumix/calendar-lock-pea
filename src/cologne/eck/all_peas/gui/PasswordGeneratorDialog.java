package cologne.eck.all_peas.gui;

/*
 * Peafactory - Production of Password Encryption Archives
 * Copyright (C) 2015  Axel von dem Bruch
 * 
 * This library is free software; you can redistribute it and/or modify it 
 * under the terms of the GNU General Public License as published 
 * by the Free Software Foundation; either version 2 of the License, 
 * or (at your option) any later version.
 * This library is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
 * or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU General Public License for more details.
 * See:  http://www.gnu.org/licenses/gpl-2.0.html
 * You should have received a copy of the GNU General Public License 
 * along with this library.
 */

/**
 * Dialog to create random passwords.
 */

//import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Toolkit;
import java.awt.Window;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.StringSelection;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.JTextField;
//import javax.swing.border.LineBorder;
import javax.swing.KeyStroke;

import cologne.eck.all_peas.data.PeaProperties;
import cologne.eck.all_peas.bridge.VMBridge;
import cologne.eck.peafactory.crypto.RandomStuff;


@SuppressWarnings("serial")
public final class PasswordGeneratorDialog extends JDialog 
		implements ActionListener, WindowListener {
	
	private JTextField randomField;
	
	public PasswordGeneratorDialog (Window owner) {		
		
		super(owner);		
		VMBridge bridge = PeaProperties.getVmBridge();

		this.setTitle(bridge.translate("create_random_password"));//"create new random password");
		this.setAlwaysOnTop(true);
		this.setDefaultCloseOperation(JDialog.DO_NOTHING_ON_CLOSE);
		this.addWindowListener(this);

		// Avoid keyboard trap: Close with ESC
		this.getRootPane().getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW)
    		.put(KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), "CLOSE_ON_ESCAPE");
		this.getRootPane().getActionMap().put("CLOSE_ON_ESCAPE", new CloseOnEscapeAction() );
	
		JPanel randomPane = (JPanel) this.getContentPane();
		randomPane.setBorder(PeaBorderFactory.createBorder(false));//.setBorder(new LineBorder(Color.GRAY,2));
		randomPane.setLayout(new BoxLayout(randomPane, BoxLayout.Y_AXIS));
	
		JPanel displayPanel = new JPanel();
		displayPanel.setLayout(new BoxLayout(displayPanel, BoxLayout.Y_AXIS));
	
		randomField = new JTextField();
		randomField.setFont(new Font(Font.SERIF, Font.PLAIN, 14));
		randomField.setPreferredSize(new Dimension(450, 30));
		displayPanel.add(randomField);
	
		JButton randomButton = new JButton(bridge.translate("create_password"));//"new password");
		randomButton.setActionCommand("createRandomPassword");
		randomButton.addActionListener(this);
		displayPanel.add(randomButton);
		
		randomPane.add(displayPanel);
		
		JPanel copyPanel = new JPanel();
		copyPanel.setLayout(new BoxLayout(copyPanel, BoxLayout.X_AXIS));
		JButton copyButton = new JButton(bridge.translate("copy"));
		copyButton.addActionListener(this);
		copyButton.setActionCommand("copy");
		copyPanel.add(Box.createHorizontalGlue());
		copyPanel.add(copyButton);
	
		randomPane.add(copyPanel);
		
		if (owner != null) {
			this.setLocation((int)owner.getLocationOnScreen().getX() + 50, 
					(int)owner.getLocationOnScreen().getY() + 50);
		} else {
			this.setLocation(100,100);
		}
		this.setResizable(false);
		this.pack();
	}
	
	@Override
	public void actionPerformed(ActionEvent ape) {
		String command = ape.getActionCommand();
		if (command.equals("createRandomPassword")) {
			// creates a random password with security properties of at least a 256 bit key: 50 characters
			char[] rand = new RandomStuff().createNewRandomChars();
			randomField.setText(new String(rand));
		} else if (command.equals("copy")) {
			StringSelection stringSelection = new StringSelection (randomField.getText());
			Clipboard clpbrd = Toolkit.getDefaultToolkit ().getSystemClipboard ();
			clpbrd.setContents (stringSelection, null);
			randomField.setText("");
		}
	}
/*	public static void main(String[] args){
		PasswordGeneratorDialog p = new PasswordGeneratorDialog(null);
		p.setVisible(true);
	}*/

	@Override
	public void windowActivated(WindowEvent arg0) {}
	@Override
	public void windowClosed(WindowEvent arg0) {}
	
	@Override
	public void windowClosing(WindowEvent arg0) {
		// clear the field:
		randomField.setText("");
		this.dispose();
	}
	
	@Override
	public void windowDeactivated(WindowEvent arg0) {}
	@Override
	public void windowDeiconified(WindowEvent arg0) {}
	@Override
	public void windowIconified(WindowEvent arg0) {}
	@Override
	public void windowOpened(WindowEvent arg0) {} 	
	
}
