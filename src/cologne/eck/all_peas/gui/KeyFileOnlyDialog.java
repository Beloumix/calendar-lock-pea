package cologne.eck.all_peas.gui;

import java.awt.Color;

/*
 * Peafactory - Production of Password Encryption Archives
 * Copyright (C) 2020  Axel von dem Bruch
 * 
 * This library is free software; you can redistribute it and/or modify it 
 * under the terms of the GNU General Public License as published 
 * by the Free Software Foundation; either version 2 of the License, 
 * or (at your option) any later version.
 * This library is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
 * or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU General Public License for more details.
 * See:  http://www.gnu.org/licenses/gpl-2.0.html
 * You should have received a copy of the GNU General Public License 
 * along with this library.
 */

import java.awt.Dimension;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.io.File;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JEditorPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.JTextArea;
import javax.swing.JCheckBox;
import javax.swing.KeyStroke;
import javax.swing.border.LineBorder;

import cologne.eck.all_peas.data.AttachmentHandler;
import cologne.eck.all_peas.data.PeaProperties;
//import cologne.eck.all_peas.vm_specific.JREProperties;
import cologne.eck.peafactory.crypto.AlgoParamHandler;
import cologne.eck.peafactory.crypto.kdf.KeyDerivation;
import cologne.eck.peafactory.crypto.kdf.SingleIterationPBKDF2;
import cologne.eck.tools.PropertyHandler;
import cologne.eck.tools.TestLog;



@SuppressWarnings("serial")
public class KeyFileOnlyDialog extends JDialog implements ActionListener {
	
	private JTextField keyFileField;
	private JCheckBox directCheck;
	private boolean successfullySet = false;
	

	
	/**
	 * Shows a dialog to set a keyfile instead of a password.
	 * Check success with isSuccessfullySet()
	 * 
	 * @param owner		the component owner
	 */
	public KeyFileOnlyDialog(Window owner) {//Point loc) {

		super(owner);
		this.setModalityType(JDialog.ModalityType.APPLICATION_MODAL);
	//	this.setAlwaysOnTop(true);
		
		//successfullySet = false;

		// Avoid keyboard trap: Close with ESC
		this.getRootPane().getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW)
    		.put(KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), "CLOSE_ON_ESCAPE");
		this.getRootPane().getActionMap().put("CLOSE_ON_ESCAPE", new CloseOnEscapeAction() );
		
		
		JPanel contentPane = (JPanel) this.getContentPane();
		contentPane.setLayout(new BoxLayout(contentPane, BoxLayout.PAGE_AXIS));
		
		JEditorPane editorpane= new JEditorPane();
		editorpane.setContentType("text/html;charset=UTF-8");
		String htmlText = "<H1>"
				+ PeaProperties.getVmBridge().translate("key_file_only_mode")
				+ "</H1><p>"
				+ PeaProperties.getVmBridge().translate("key_file_instead_password")
				+ "</p><p>"
				+ PeaProperties.getVmBridge().translate("local_protection_omitted")
				+ "</p><p>"
				+ PeaProperties.getVmBridge().translate("no_password_anyone_access")
				+ PeaProperties.getVmBridge().translate("recommended_backup")
				+ "</p>";
		editorpane.setText(htmlText);
	    editorpane.setEditable(false);	    
	    contentPane.add(editorpane);
	    contentPane.add(Box.createVerticalStrut(10));
	    
	    // choose file
	    String keyFile = PeaProperties.getDefaultKeyFileName();
	    if (keyFile == null) {
	    	JPanel fileChooserPanel = new JPanel();
	    	fileChooserPanel.setLayout(new BoxLayout(fileChooserPanel, BoxLayout.LINE_AXIS));
	    	JButton keyFileChooser = new JButton(PeaProperties.getVmBridge().translate("select_folder_for_key_file"));
	    	keyFileChooser.addActionListener(this);
	    	keyFileChooser.setActionCommand("chooseDirectory");
	    	fileChooserPanel.add(keyFileChooser);
	    	fileChooserPanel.add(Box.createHorizontalGlue());
	    	contentPane.add(fileChooserPanel);	    
	    }
	    
	    keyFileField = new JTextField(40);
	    keyFileField.setEditable(false);
	    keyFileField.setMaximumSize(new Dimension(1000, 30));
	    if (keyFile != null) {
	    	keyFileField.setText(keyFile);
	    }
	    contentPane.add(keyFileField);
	    //contentPane.add(Box.createVerticalStrut(5));
	    
	    JPanel keyFileInfoPanel = new JPanel();
	    keyFileInfoPanel.setLayout(new BoxLayout(keyFileInfoPanel, BoxLayout.LINE_AXIS));
	    JTextArea keyFileInfo = new JTextArea();
	    keyFileInfo.setWrapStyleWord(true);
	    keyFileInfo.setLineWrap(true);
	    keyFileInfo.setText(PeaProperties.getVmBridge().translate("key_file_must_be_present") + ". \n" 
	    		+ PeaProperties.getVmBridge().translate("never_modify_or_delete"));
	    keyFileInfoPanel.add(Box.createHorizontalStrut(25));
	    keyFileInfoPanel.add(keyFileInfo);
	    contentPane.add(keyFileInfoPanel);
	    contentPane.add(Box.createVerticalStrut(10));
	    
	    JPanel checkPanel = new JPanel();
	    checkPanel.setLayout(new BoxLayout(checkPanel, BoxLayout.LINE_AXIS));
	    directCheck = new JCheckBox(PeaProperties.getVmBridge().translate("start_without_selection")); 
	    if (PeaProperties.isDirectStart() == true) {
	    	directCheck.setSelected(true);
	    } else {
	    	directCheck.setSelected(false);
	    }
	    checkPanel.add(directCheck);
	    checkPanel.add(Box.createHorizontalGlue());
	    contentPane.add(checkPanel);
	    
	    JPanel directInfoPanel = new JPanel();
	    directInfoPanel.setLayout(new BoxLayout(directInfoPanel, BoxLayout.LINE_AXIS));
	    JTextArea directInfo = new JTextArea();
	    directInfo.setWrapStyleWord(true);
	    directInfo.setLineWrap(true);
	    directInfo.setText(PeaProperties.getVmBridge().translate("start_with_last_used_files")
				//+ " \n The contents are displayed immediately, but the files cannot be selected. "
				+ " \n" + PeaProperties.getVmBridge().translate("change_in_settings") );
	    directInfoPanel.add(Box.createHorizontalStrut(25));
	    directInfoPanel.add(directInfo);
	    contentPane.add(directInfoPanel);
	    contentPane.add(Box.createVerticalStrut(10));
	    
	    JButton okButton = new JButton(PeaProperties.getVmBridge().translate("ok"));
	    okButton.addActionListener(this);
	    okButton.setActionCommand("OK");
	    okButton.getAccessibleContext().
	    setAccessibleDescription(
	    	    "Clicking this component closes this dialog and sets the keyfile-only mode.");
	    //okButton.setMaximumSize(new Dimension(200, 20));
	    contentPane.add(okButton);
	    if (owner != null && owner.isVisible() && owner.isShowing() ) {
	    	this.setLocation(owner.getLocationOnScreen());
	    } else {
	    	TestLog.ve(KeyFileOnlyDialog.class, "Missing owner ", 5);
	    }
	   this.setSize(new Dimension(500,600));
	  //  this.pack();

	    okButton.requestFocus();
	    okButton.requestFocusInWindow();
	    TestLog.v(KeyFileOnlyDialog.class, "Open keyfile-only dialog...");

	    //this.setVisible(true);	
	}

	@Override
	public void actionPerformed(ActionEvent ae) {
		String command = ae.getActionCommand();
		if (command.equals("OK")) {
			String keyFileName = keyFileField.getText();
			if (keyFileName == null || keyFileName.trim().length() == 0) {
				keyFileField.setBorder(new LineBorder(Color.RED));
				return;
			} else {
				if ( !  new File(keyFileName).exists()) {
					PeaDialog.showMessage(this, 
							PeaProperties.getVmBridge().translate("key_file") + ": " 
					+ PeaProperties.getVmBridge().translate("file_not_found"), 
					PeaProperties.getVmBridge().translate("error"), 0);
					keyFileField.setBorder(new LineBorder(Color.RED));
					return;
				}
			}
			// set properties
			// Check if properties file exists (first start):
		/*	if ( ! new File(System.getProperty("user.dir") + File.separator 
					+ "resources").exists()) {
				boolean dirSuccess = new File(System.getProperty("user.dir") + File.separator 
						+ "resources").mkdirs();
				if (dirSuccess == false) {
					PeaDialog.showMessage(this, "Could not create directory "
							+ System.getProperty("user.dir") + File.separator 
							+ "resources", 
							PeaProperties.getVmBridge().translate("unexpected_error"), 0);
					return;
				}
			}		*/	
			if ( ! new File(System.getProperty("user.dir") + File.separator 
					+ "resources" + File.separator + "pea.properties").exists()) {
				// create property file and directory resources if required:
				String error = new PropertyHandler().updateAllPropValues();
				if (error != null) {
					PeaDialog.showMessage(this, 
							PeaProperties.getVmBridge().translate("unexpected_error") + ": " 
					+ error, 
					PeaProperties.getVmBridge().translate("error"), 0);
					return;
				}
			}
			
			String prop1Error = new PropertyHandler().setOneProperty("key_file", keyFileName);
			if (prop1Error != null) {
				TestLog.ve(KeyFileDialogs.class, "Could not set poperties: " + prop1Error, 5);
				PeaDialog.showMessage(this, //JREProperties.getMainWindow(), 
						PeaProperties.getVmBridge().translate("operation_failed") + "\n"
								+ prop1Error, 
								PeaProperties.getVmBridge().translate("unexpected_error"), 0);
				return;
			} 
			String prop2Error = new PropertyHandler().setOneProperty("keyfile_only_mode", "TRUE");
			if (prop2Error != null) {
				TestLog.ve(KeyFileDialogs.class, "Could not set keyfile-only mode poperty: " + prop2Error, 5);
				PeaDialog.showMessage(this, //JREProperties.getMainWindow(), 
						PeaProperties.getVmBridge().translate("operation_failed") + "\n"
								+ prop2Error + "\n\n"
								+ PeaProperties.getVmBridge().translate("select_file_when_open_files"), 
								PeaProperties.getVmBridge().translate("unexpected_error"), 0);
			} 
			// set session properties:
			PeaProperties.setKeyFileOnlyMode(true);
			PeaProperties.setDefaultKeyFileName(keyFileName);
			PeaProperties.setDefaultKeyFileFlag(1); // default 
			PeaProperties.setKeyFileFlag(1);					
			KeyDerivation.setKdf(new SingleIterationPBKDF2());
			PeaProperties.setDefaultKDF(new SingleIterationPBKDF2());
			PeaProperties.setAlgoParamString(
					AlgoParamHandler.writeString(false).getBytes(AttachmentHandler.getASCIICharset()));
			PeaProperties.setDefaultAlgoParamString(
					AlgoParamHandler.writeString(true));
			
			if (directCheck.isSelected()) {				
				 new PropertyHandler().setOneProperty("direct_start", "TRUE");
				PeaProperties.setDirectStart(true);
				TestLog.v(getClass(), "Set direct start");
			}			
			successfullySet = true;
			//NewPasswordDialog.close();
			this.setVisible(false);
			this.dispose();
			
		} else if (command.equals("chooseDirectory")) {
			File keyFile = KeyFileDialogs.generateKeyFile(this);
			if (keyFile != null) {
				keyFileField.setText(keyFile.getAbsolutePath());
			}
		}
	}
	
	/**
	 * Check if keyfile-only mode was set successfully. 
	 * 
	 * @return	true: keyfile-only mode was set successfully
	 */
	public boolean isSuccessfullySet() {
		return successfullySet;
	}
}
