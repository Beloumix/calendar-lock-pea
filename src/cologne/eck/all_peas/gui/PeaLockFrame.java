package cologne.eck.all_peas.gui;



/*
 * Peafactory - Production of Password Encryption Archives
 * Copyright (C) 2020  Axel von dem Bruch
 * 
 * This library is free software; you can redistribute it and/or modify it 
 * under the terms of the GNU General Public License as published 
 * by the Free Software Foundation; either version 2 of the License, 
 * or (at your option) any later version.
 * This library is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
 * or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU General Public License for more details.
 * See:  http://www.gnu.org/licenses/gpl-2.0.html
 * You should have received a copy of the GNU General Public License 
 * along with this library.
 */

import java.awt.Window;
import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;

import javax.swing.JFrame;
import javax.swing.filechooser.FileFilter;

import org.bouncycastle.crypto.BlockCipher;
import org.bouncycastle.crypto.Digest;

import cologne.eck.all_peas.cloud.CloudControl;
import cologne.eck.all_peas.cloud.CloudPropertyHandler;
import cologne.eck.all_peas.cloud.StorageProvider;
import cologne.eck.all_peas.cloud.gui.WebDavLogin;
import cologne.eck.all_peas.control.ContentUtilities;
import cologne.eck.all_peas.control.PeaControl;
import cologne.eck.all_peas.data.AttachmentHandler;
import cologne.eck.all_peas.data.Attachments;
import cologne.eck.all_peas.data.PeaProperties;
import cologne.eck.all_peas.files.CloudFilePanel;
import cologne.eck.all_peas.files.FileComposer;
import cologne.eck.all_peas.files.FileModel;
import cologne.eck.all_peas.files.FileTypePanel;
import cologne.eck.all_peas.vm_specific.JREProperties;
import cologne.eck.peafactory.crypto.AlgoParamHandler;
import cologne.eck.peafactory.crypto.CipherStuff;
import cologne.eck.peafactory.crypto.HashStuff;
import cologne.eck.peafactory.crypto.RandomStuff;
import cologne.eck.peafactory.crypto.kdf.KeyDerivation;
import cologne.eck.peafactory.crypto.kdf.SingleIterationPBKDF2;
import cologne.eck.tools.Converter;
import cologne.eck.tools.ReadResources;
import cologne.eck.tools.TestLog;
import cologne.eck.tools.Zeroizer;


@SuppressWarnings("serial")
public abstract class PeaLockFrame extends JFrame {
	
	// this is required, when a new sub-set in cloud.properties is to be set: providerName - password
	private HashMap <String, String>  providerPswMap = new HashMap<String, String>();
	
	// the key before changes were applied: This is used for re-encryption
	private byte[] previousKey = null;
	// the algorithm-parameter string before changes were applied
	private static byte[] algoParamsBeforeChange = PeaProperties.getAlgoParamString();
	// this may be needed for re-encryption
	private BlockCipher previousCipher = CipherStuff.getCipherAlgo();
	private Digest previousHash = HashStuff.getHashAlgo();
	
	/**
	 * Store all passwords for opened cloud providers
	 * to reset password property in cloud.properties
	 * and current algorithms and parameters
	 * 
	 */
	public void storeValuesBeforeChange() {
		if (JREProperties.getMainWindow() instanceof PeaLockFrame) {	
			
			// set the the previous algorithm-parameter string
			algoParamsBeforeChange = PeaProperties.getAlgoParamString();
			if (algoParamsBeforeChange != null) {
				TestLog.v(CryptoSetter.class, "Store previous algoParam: " + new String(algoParamsBeforeChange));
			} else {
				TestLog.v(CryptoSetter.class, "Store default previous algoParam: " + new String(AlgoParamHandler.getV1Bytes()));
				algoParamsBeforeChange = AlgoParamHandler.getV1Bytes();
			}
			// set the previous cipher and hash algorithm:
			previousCipher = AlgoParamHandler.getCipherFromString( new String( algoParamsBeforeChange, AttachmentHandler.getASCIICharset()));
			previousHash = AlgoParamHandler.getHashFromString(new String( algoParamsBeforeChange, AttachmentHandler.getASCIICharset()));
			
			// set the previous cloud provider passwords
			Collection<StorageProvider> providers = CloudControl.getOpenProviders();
			if (providers != null && providers.isEmpty() == false
					&& ( ! PeaProperties.getFileType().equals("file")) ) {// there are no cloud.properties in File Lock PEA
				for (StorageProvider p : providers) {
					String pName = p.getProviderName();

					//TestLog.v(PeaLockFrame.class,"Store login for " + pName + " - " + new String(PeaProperties.getAlgoParamString()));
					String pPassword =  CloudPropertyHandler.getPlainPasswordFromProvider(pName, 
							Converter.bytes2hex(KeyDerivation.getSalt()), 
							Converter.bytes2hex(PeaProperties.getAlgoParamString()));
					if (pName == null) {
						pName = new String(Converter.bytes2chars(WebDavLogin.getTemporaryStoredPassword(pName))) ;
					}
					if (pPassword != null) {
						providerPswMap.put(pName, pPassword);
					} else {
						TestLog.ve(PeaLockFrame.class, "Could not temporarly store password for " + pName, 5);
					}
				}
			} else {
				TestLog.v(PeaLockFrame.class, "No cloud provider is open"); 
			}
		}
	}
	
	/**
	 * Ask for further setting and if not:
	 * Change the password for current used files 
	 * or apply changes in algorithms and parameters:
	 * Ask for a new password, ask for other (remaining) files, 
	 * derive the key, encrypt the files
	 * 
	 * @param owner: PswDialogView or the lock frame
	 * @param fileNamesToChange	the file names to apply changes or null
	 * 							(apply to files from getCurrentFileNames() )
	 * 
	 * @return	null for success, a localized error message to show otherwise
	 */
	public String changePasswordOrApplyChanges(Window owner, String[] fileNamesToApply) {
		
		if (owner == null){
			TestLog.ve(getClass(), "Missing owner", 5);
			owner = JREProperties.getMainWindow();
		}

		if (owner instanceof PswDialogView) {
			// although already done in SettingMenu and KDFSettings...
			String newDefaultString = AlgoParamHandler.writeString(true);
			PeaProperties.setDefaultAlgoParamString(newDefaultString);
			return null;

		} else if (owner instanceof PeaLockFrame) {

			// First: save the content
			if (isContentUnsaved() == true) {
				String saveError = saveCurrentContent(); 
				if (saveError != null) {
					TestLog.e(PeaLockFrame.class, "Could not save current file - " + saveError);
					// clear sensible values:
					if (previousKey != null) {
						Zeroizer.zero(previousKey);	
					}
					if (providerPswMap != null && providerPswMap.isEmpty() == false) {
						providerPswMap.clear();
					}
					return PeaProperties.getVmBridge().translate("unexpected_error") + " ("+ saveError + ")";
				} else {
					markContentIsSaved();
				}
			}
			// set the previous key
			previousKey = CipherStuff.getSessionKeyCrypt().getKey();

			String currentAlgos = AlgoParamHandler.writeString(false);
			PeaProperties.setAlgoParamString(currentAlgos.getBytes(AttachmentHandler.getASCIICharset()));
			AlgoParamHandler.readString(currentAlgos, false);

			boolean contentWasSaved = false;
			char[] newPsw = null;
			if (KeyDerivation.getKdf() instanceof SingleIterationPBKDF2) {
				// keyfile-only mode
				TestLog.v(getClass(), "Keyfile only mode...");
				KeyFileOnlyDialog kfod = new KeyFileOnlyDialog(JREProperties.getMainWindow());
				kfod.setVisible(true);
				if (kfod.isSuccessfullySet() == false) {
					return null;
				} else {
					TestLog.v(getClass(), "Keyfile-only mode was set");
					newPsw = null;		
				}
			} else {				

				NewPasswordDialog newPswDialog = NewPasswordDialog.getInstance(
						owner, PeaProperties.getVmBridge().translate("change_password"), null,
						owner.getLocation());
				newPsw = newPswDialog.getDialogInput();
				if (newPsw == null) {
					TestLog.v(PeaLockFrame.class, "No password change");
					// clear sensible values:
					if (previousKey != null) {
						Zeroizer.zero(previousKey);	
					}
					if (providerPswMap != null && providerPswMap.isEmpty() == false) {
						providerPswMap.clear();
					}
					return null;
				}
			}
			//==============================================
			// new Nonce for every new password
			byte[] nonce = new RandomStuff().createRandomBytes(Attachments.getNonceSize());
			Attachments.setNonce(nonce);
			//==============================================

			try {
				setMessage("... " + PeaProperties.getVmBridge().translate("change_password") + " " +
						PeaProperties.getVmBridge().translate("in_progress")  + "... ", false);
			} finally {

				byte[] keyMaterial = PeaControl.deriveKeyFromPsw(newPsw);
				if (keyMaterial == null) {
					TestLog.ve(PeaLockFrame.class, "keyMaterial is null", 5);
					// clear sensible values:
					if (previousKey != null) {
						Zeroizer.zero(previousKey);	
					}
					if (providerPswMap != null && providerPswMap.isEmpty() == false) {
						providerPswMap.clear();
					}
					return JREProperties.getVmBridge().translate("failed") + ": KDF" + "\n" 
					+ PeaControl.getDerivationError();				
				}

				if (newPsw != null){
					Zeroizer.zero(newPsw);
				} 
				if (CipherStuff.getSessionKeyCrypt() == null) {
					TestLog.ve(PeaLockFrame.class, "CipherStuff.getSessionKeyCrypt() is null", 5);
					// clear sensible values:
					if (previousKey != null) {
						Zeroizer.zero(previousKey);	
					}
					if (providerPswMap != null && providerPswMap.isEmpty() == false) {
						providerPswMap.clear();
					}
					return PeaProperties.getVmBridge().translate("unexpected_error") + " (SessionKeyCrypt missing)";
				}
				CipherStuff.getSessionKeyCrypt().clearKeys();
				//CipherStuff.resetSessionKeyCrypt();
				CipherStuff.getSessionKeyCrypt().storeKey(keyMaterial);
				setMessage("", false);
				//PeaProperties.getOsBridge().translate("done"));

				// set properties files for new scheme
				if (owner instanceof PeaLockFrame) {
					// File Lock PEA does not open files from cloud, only upload and download
					if ( ! PeaProperties.getFileType().equals("file")) {
						String[] currentFileNames = null;
						if (fileNamesToApply != null) {
							currentFileNames = fileNamesToApply;
						} else {
							currentFileNames = ((PeaLockFrame) owner).getCurrentFileNames();
						}
						if (currentFileNames != null) {
							/*	if (PeaProperties.getFileType().equals("image")
							 * || PeaProperties.getFileType().equals("contact") 
									|| PeaProperties.getFileType().equals("text file") ) { //  Notebook
								This would cause two problems:
									1. unselected, opened files in this session can con be opened any more
									2. cloud passwords are change, if only the password is changed and
									   unselected cloud files can't be opened at next start
								MultiFileSelectionDialog mfs = new MultiFileSelectionDialog(JREProperties.getMainWindow(), 
										JREProperties.getMainWindow().getLocationOnScreen(), 
										PeaProperties.getVmBridge().translate("choose"),
										Converter.arrayToArrayList(currentFileNames));
								ArrayList<String> chosenFileNames = mfs.getSelectedFiles();
								if (chosenFileNames == null || chosenFileNames.size() == 0) {
									TestLog.v(PeaLockFrame.class, "No file names chosen...");
									// clear sensible values:
									if (previousKey != null) {
										Zeroizer.zero(previousKey);	
									}
									if (providerPswMap != null && providerPswMap.isEmpty() == false) {
										providerPswMap.clear();
									}
									return PeaProperties.getVmBridge().translate("file_not_found");
								}
								currentFileNames = Converter.arrayListToArray(chosenFileNames);

							} */
							// re-encrypt files: Large files are stored in plaintext on disk to re-encrypt
							// inform the user and ask to accept the risk:
							boolean warn = false;
							long warningLen = 1024*1024*256;// 256 MB
							boolean acceptRisk = false;
							for (String s : currentFileNames) {
								if (s != null) {
									File f = new File(s);
									if (f.exists() && f.isFile() && f.canRead()) {
										if (f.length() > warningLen) {
											warn = true;
											break;
										}
									}
								}
							}
							if (warn == true) {
								int warnResult = PeaDialog.showQuestion(owner, 
										PeaProperties.getVmBridge().translate("file_too_large") +"\n"
												+ "Can not re-encrypt in RAM. \n"
												+ "Re-encrypt the files on disk?\n"
												+ "  (Accept the risk that files are shortly unencrypted)", 
												PeaProperties.getVmBridge().translate("warning"), 0);// yes/no
								if (warnResult == 0) {
									acceptRisk = true;
								}
							}
							//save all files
							if (previousKey == null || keyMaterial == null) {
								TestLog.ve(PeaLockFrame.class, "Key is null", 5);
								// clear sensible values:
								if (previousKey != null) {
									Zeroizer.zero(previousKey);	
								}
								if (providerPswMap != null && providerPswMap.isEmpty() == false) {
									providerPswMap.clear();
								}
								return PeaProperties.getVmBridge().translate("unexpected_error") + " (null key)";
							}
							TestLog.v(PeaLockFrame.class, "Re-encrypt " + currentFileNames.length + " files with new key...");

							String reencryptError = ContentUtilities.reencrypt(currentFileNames, Arrays.copyOf(previousKey, previousKey.length), 
									CipherStuff.getSessionKeyCrypt().getKey(), 
									previousCipher, CipherStuff.getCipherAlgo(), previousHash, HashStuff.getHashAlgo(), acceptRisk);
							if (reencryptError != null) {
								PeaDialog.showMultiLineMessage(owner, reencryptError, 
										PeaProperties.getVmBridge().translate("error"), 0);
								TestLog.ve(PeaLockFrame.class, "Reencryption failed");

								// clear sensible values:
								if (previousKey != null) {
									Zeroizer.zero(previousKey);	
								}
								if (providerPswMap != null && providerPswMap.isEmpty() == false) {
									providerPswMap.clear();
								}
								return PeaProperties.getVmBridge().translate("unexpected_error") + " (reencryption failed)";

								// TODO return and set old algoParamString
							}
							//							}

	// TODO File Lock PEA						// =================== Handle the cloud files: properties=========================
							String newSaltAsHex = Converter.bytes2hex(KeyDerivation.getSalt());
							String newAlgoParamAsHex = Converter.bytes2hex(PeaProperties.getAlgoParamString());
							if (newSaltAsHex != null && newAlgoParamAsHex != null) {

								// for these providers the password in properties was changed:
								ArrayList<String> changedProviders = new ArrayList<String>();

								// check if key file, algorithms or parameters were changed:
								boolean algoParamsWereChanged = false;
								if ( ! Arrays.equals(algoParamsBeforeChange, PeaProperties.getAlgoParamString() )){
									algoParamsWereChanged = true;
								}
								// avoid null password, because then the files are not stored in properties.
								// if the password fails, a password dialog appears
								String plainPsw = "placeholder";
								// iterate over cloud files:
								for (String openedFileName : currentFileNames) {
									if (openedFileName.startsWith(CloudControl.getTmpDir())) {									

										String providerName = CloudControl.getProviderNameFromTmpFile(openedFileName);
										String fileNameToMove = CloudControl.getFileNameFromTmpFile(openedFileName);
										if (providerName != null && fileNameToMove != null) {

											if (algoParamsWereChanged == true) { // must move files in properties
												// 1. remove the file from the current set
												int previosSubSetIndex = CloudPropertyHandler.removeFileFromAnyProviderSet(providerName, fileNameToMove, null);
												if (previosSubSetIndex >= 0 ) { // file was removed
													TestLog.v(PeaLockFrame.class, "Removed from properties: " + fileNameToMove);
												} else {
													TestLog.v(PeaLockFrame.class, "Not found in properties: " + fileNameToMove);
												}

												// 2. add file to a probably new sub-set and change the cloud password
												int extToAdd = CloudPropertyHandler.getSubSetExtension(providerName, newSaltAsHex, newAlgoParamAsHex, null);
												String[] fileNamesToAdd = {fileNameToMove};
												if (extToAdd >= 0) { // exists already												
													String addError = CloudPropertyHandler.addFileNamesToProperties(providerName, fileNamesToAdd, 
															newSaltAsHex, newAlgoParamAsHex);
													if (addError != null) {
														TestLog.e(PeaLockFrame.class, addError);
													}

												} else {
													// create new set, get password (this is done only once, next file will be added)
													plainPsw = providerPswMap.get(providerName);
													if (plainPsw != null) {
														int newExt = CloudPropertyHandler.createSubSet(providerName, newSaltAsHex, newAlgoParamAsHex, 
																plainPsw, null);

														if (newExt == -1) {// failed
															TestLog.e(PeaLockFrame.class, "Could not create new property set for " + providerName);
															TestLog.ve(PeaLockFrame.class, "Could not create new property set for " + providerName, 5);													
														} else { // success
															// mark, that this password must not be changed any more
															changedProviders.add(providerName);

															// add removed file name to new set
															String addError = CloudPropertyHandler.addFileNamesToProperties(providerName, fileNamesToAdd, newSaltAsHex, newAlgoParamAsHex);
															if (addError != null) {//failed
																TestLog.e(PeaLockFrame.class, "Could not add file " + fileNameToMove + " to " + providerName);
															}
														}
													}
												}
											} else {
												// key file,algorithms and parameters were not changed, only the password...
												TestLog.v(PeaLockFrame.class, "Key file,algorithms and parameters were not chenged...");
											}

											// change the password in cloud.properties for the current scheme:
											plainPsw = providerPswMap.get(providerName);
											if ( ! changedProviders.contains(providerName) ) {// only once, not for every file
												String storeError = CloudPropertyHandler.storeEncryptedPasswordFromProvider(
														CloudPropertyHandler.encryptPassword(plainPsw), providerName, 
														newSaltAsHex, newAlgoParamAsHex, null);
												if (storeError != null) {
													TestLog.e(PeaLockFrame.class, "Could not change cloud password in properties");
												} else {
													TestLog.v(PeaLockFrame.class, providerName +  " - Cloud password in properties changed for " + " " + fileNameToMove);
													// mark, that for this provider the password was already changed:
													changedProviders.add(providerName);
												}
											}

										} else {
											TestLog.ve(PeaLockFrame.class, "provider/file name string is null", 5);
											//return PeaProperties.getVmBridge().translate("unexpected_error") + " (missing provider/file name)";
										}
									}
								}
							} else {
								TestLog.ve(PeaLockFrame.class, "Salt or algorithm-parameter string is null", 5);
								// clear sensible values:
								if (previousKey != null) {
									Zeroizer.zero(previousKey);	
								}
								if (providerPswMap != null && providerPswMap.isEmpty() == false) {
									providerPswMap.clear();
								}
								return PeaProperties.getVmBridge().translate("unexpected_error") + " (missing salt/algo-string)";
							}
						}
						PeaControl.setAvailableFileNames(Converter.arrayToArrayList(currentFileNames));
						// set new FileTypePanel, CloudFilePanel...
						FileComposer newFc = new FileComposer(new FileModel(true, PeaProperties.getAlgoParamString(), KeyDerivation.getSalt()));

						FileTypePanel newFtp = new FileTypePanel(100,100, true, false, newFc, this);					

						// set new panel:
						JREProperties.setTypePanel(newFtp);
						CloudFilePanel newCfp = new CloudFilePanel(owner, newFtp, Converter.bytes2hex(KeyDerivation.getSalt()), 
								Converter.bytes2hex(PeaProperties.getAlgoParamString()));

						// set CloudFileModel
						newFtp.setCloudFilePanel(newCfp);
						if (currentFileNames != null) {
							for (String newFileName : currentFileNames) {
								if (newFileName.contains(CloudControl.getTmpDir())) {
									String providerName = CloudControl.getProviderNameFromTmpFile(newFileName);
									String usedFileName = CloudControl.getFileNameFromTmpFile(newFileName);
									// add to used files
									newCfp.getCloudControl().addUsedFileNameToProvider(providerName, usedFileName);
								}
							}						
						} else {
							TestLog.ve(PeaLockFrame.class, "Missing current file names", 5);
						}

						// Now save the content with the new key
						String saveError = saveCurrentContent(); // TODO implement for all PEAs except Notebook and Image (is already encrypted)
						if (saveError != null) {
							TestLog.e(PeaLockFrame.class, "Could not save current file - " + saveError);
							TestLog.ve(PeaLockFrame.class, "Could not save current file - " + saveError, 5);
						} else {
							contentWasSaved = true;
							TestLog.o(PeaLockFrame.class, "Content was saved");
						}
					} else {// not File Lock PEA
						// check if there are cloud properties: 
						// Then ask to change password
						boolean askToChangeCloudPasswords = false;
						String suggestion = null;
						// always ask to change if there are open providers:
						Collection <StorageProvider> openProvs = CloudControl.getOpenProviders();
						if (openProvs != null && openProvs.isEmpty() == false) {
							StringBuilder openProvBuilder = new StringBuilder();
							for (StorageProvider sp : openProvs) {
								openProvBuilder.append(sp.getProviderName() + "\n");
							}
							askToChangeCloudPasswords = true;
							suggestion = PeaProperties.getVmBridge().translate("set_cloud_password")
									+ " (" + PeaProperties.getVmBridge().translate("next_open_program") + ")" + ":\n" + new String(openProvBuilder)
									+ "\n\n" + PeaProperties.getVmBridge().translate("menu") + ": " + PeaProperties.getVmBridge().translate("cloud")
									+ "\n  ->  " + PeaProperties.getVmBridge().translate("modify_cloud_provider");
						}
						if (askToChangeCloudPasswords == false) {
							// check cloud properties:
							String[] propProviders = CloudPropertyHandler.getProviderNames();
							if (propProviders != null && propProviders.length > 0) {
								StringBuilder propProvsBuilder = new StringBuilder();
								for (String pp : propProviders) {
									propProvsBuilder.append(pp + "\n");
								}
								askToChangeCloudPasswords = true;
								suggestion = PeaProperties.getVmBridge().translate("set_cloud_password") + ":\n" + new String(propProvsBuilder)
										+ "\n\n" + PeaProperties.getVmBridge().translate("menu") + ": " + PeaProperties.getVmBridge().translate("cloud")
										+ "\n  ->  " + PeaProperties.getVmBridge().translate("modify_cloud_provider");
							}
						}
						if (askToChangeCloudPasswords == true && suggestion != null) {
							PeaDialog.showMessage(owner,suggestion);
						}
					}
				}
				// clear sensible values:
				if (previousKey != null) {
					Zeroizer.zero(previousKey);	
				}
				if (providerPswMap != null && providerPswMap.isEmpty() == false) {
					providerPswMap.clear();
				}
			}
			if (contentWasSaved == false) {
				// the content is not re-encrypted:
				markUnsavedContentChanges();
			} else {
				markContentIsSaved();
			}		
			return null;
		} else {
			TestLog.ve(PeaLockFrame.class, "Invalid main frame", 5);
			return JREProperties.getVmBridge().translate("unexpected_error");
		}
	}
	
	/**
	 * Open a file that is encrypted with the current key, 
	 * check algorithms and password identifier
	 * 
	 * @param	the FileFilter to use or null e.g. new FileNameExtensionFilter("iCalendar", "ics")
	 * 
	 * @return the successfully checked file or null
	 */
	public File openFile(FileFilter filter) {

		try {
			PeaFileChooser pfc = new PeaFileChooser(this);
			pfc.customizeFileChooser(PeaProperties.getVmBridge().translate("open_file"), 
					PeaProperties.getVmBridge().translate("open_encrypted_file"), true);

			pfc.setFileFilter(filter);
			int chooserResult = pfc.showOpenDialog(this);
			if (chooserResult != PeaFileChooser.APPROVE_OPTION) {
				return null;
			}
			File file = pfc.getSelectedFile();
			if (file != null && file.canRead() && file.isFile()) {				
				// Read calendars from file
				// get content of file: 
				byte[] cipherText = ReadResources.readExternFile(file.getAbsolutePath());

				// check algorithms, salt, file identifier
				if (AttachmentHandler.validateSaltAlgosFileID(cipherText) == false) {
					PeaDialog.showMessage(this, 
							PeaProperties.getVmBridge().translate("file_inappropriate") +"\n" + file.getAbsolutePath(),
							PeaProperties.getVmBridge().translate("error"), 0);
					return null;
				}				
				// check password identifier
				String error = PeaControl.checkPassword(cipherText, CipherStuff.getSessionKeyCrypt().getKey());
				if (error != null) {
					PeaDialog.showMessage(this, 
							PeaProperties.getVmBridge().translate("password_failed") +":" + file.getAbsolutePath(),
							PeaProperties.getVmBridge().translate("error"), 0);
					return null;
				}				
				return file;

			} else {
				PeaDialog.showMessage(this, 
						PeaProperties.getVmBridge().translate("file_inappropriate") +"\n" + ((file == null) ? "" : file.getAbsolutePath()),
						PeaProperties.getVmBridge().translate("error"), 0);
				return null;
			}

		} catch (Exception e){
			PeaDialog.showMessage(this, 
					PeaProperties.getVmBridge().translate("operation_failed") 
					+ "\n" + e.getLocalizedMessage(),
					PeaProperties.getVmBridge().translate("error"), 0);
			return null;
		}
	}

	
	/**
	 * Change the password. The selection of files to apply
	 * depends on the PEA type. 
	 * 
	 * @return	true for success
	 */
	public abstract boolean changePassword();
	
	/**
	 * Decrypt the content of a file and load the decrypted
	 * content to the current view.
	 * 
	 * @param fileName	the name of the file to decrypt and load
	 * 
	 * @return an error message in the form "file name - error", or null (success)
	 */
	public abstract String decryptAndLoadFileToView(String fileName);
	
	/**
	 * Choose file names to apply changes (password, algorithms changes).
	 * 
	 * @return	the file names to apply changes to
	 */
	public abstract String[] chooseFileNamesToApply();
	
	/**
	 * Get the associated file names with path of the
	 * currently opened content (Tab in LockFrameEditor (one file + backup files), 
	 * image in LockFrameImage (one file), 
	 * calendar files in LockFrameContact (several files))
	 * 
	 * @return	the file names which is associated with 
	 * 			the currently shown content or null if there is no
	 */
	public abstract String[] getCurrentFileNames();
	
	/**
	 * Get all available file names from last session to open. 
	 * 
	 * @return 	all used file names
	 */
	public abstract ArrayList<String> getUsedFileNames();
	/**
	 * Set all available file names to open on next start. 
	 * 
	 * @param		all used file names
	 */
	//public abstract void setUsedFileNames( ArrayList<String> _usedFileNames);
	
	
	/**
	 * Set the associated file names with path of the
	 * currently opened content (Tab in LockFrameEditor (one file), 
	 * image in LockFrameImage (one file), 
	 * calendar files in LockFrameContact (several files))
	 * 
	 * @param fileNamesToSet	the file names to set
	 */
	public abstract void setCurrentFileNames(String[] fileNamesToSet);

	/**
	 * Set a message in the PeaLockFrame. 
	 * This will be shown on the bottom of the frame. 
	 * Errors are shown in color red. 
	 * 
	 * @param string	the message
	 * @param isError	true, if the message is an error
	 */
	public abstract void setMessage(String string, boolean isError);

	/**
	 * Check if the currently open content 
	 * was modified and not saved manually and ask 
	 * to save the content in this case. 
	 * 
	 * @return	true if content is saved if required,
	 * 			false if unsaved content remains or
	 * 			no unsaved changes exist
	 */
	public abstract boolean checkUnsavedContentChanges();	
	
	/**
	 * Mark that there are content changes that have not been saved yet.
	 */
	public abstract void markUnsavedContentChanges();
	
	/**
	 * Mark that all current content was saved
	 */
	public abstract void markContentIsSaved();
	
	/**
	 * Check if current content was not yet saved.
	 * This does not ask to save.
	 * 
	 * @return true if there are unsaved changes
	 */
	public abstract boolean isContentUnsaved();	
	
	/**
	 * Update the PEA specific model if there is one
	 * with files that are uploaded to the cloud
	 * 
	 * @param files			the file that are uploaded to the cloud
	 * @param providerName	the provider name, the files were uploaded to
	 * 
	 */
	public abstract void updateModelWithCloudFiles(File[] files, String providerName);
	
	/**
	 * Store the current content in a temporary file, that
	 * is used as local backup of a cloud file:
	 *   System.getProperty("java.io.tmpdir") / providerName / fileName
	 *   
	 * @param providerName		the name of the cloud provider
	 * @param fileNameToStore	the file name to store without path
	 * 
	 * @return	an error message, if an error occurred or null
	 */
	public abstract String saveCurrentContentInCloudTempFile(String providerName, String fileNameToStore);
	
	/**
	 * Saves the content of all files from getCurrentFileName(). 
	 * This is performed after the password was changed. 
	 * 
	 * @return null for success, an error message otherwise
	 */
	public abstract String saveCurrentContent();
	
	/**
	 * Dispose the frame and create a new one.
	 * E.g. to change the language of the current window
	 * 
	 */
	public abstract void resetFrame();

}
