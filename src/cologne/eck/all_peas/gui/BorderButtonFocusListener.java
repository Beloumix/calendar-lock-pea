package cologne.eck.all_peas.gui;

import java.awt.Color;
import java.awt.Insets;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;

import javax.swing.AbstractButton;
import javax.swing.border.Border;
import javax.swing.border.CompoundBorder;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;

/**
 * Buttons with border do not show the focus in Nimbus L&F.
 * This class should be registered as FocusListener to show the focus. 
 * Currently works on CompoundBorder, EmptyBorder and LineBorder. 
 */

public class BorderButtonFocusListener implements FocusListener {
	
	private static BorderButtonFocusListener bbfl = null;
	private static final int DIFF = 2;
	
	private LineBorder changeLineBorder(LineBorder lb, boolean thicker) {
		int thickness = lb.getThickness();
		Color lineColor = lb.getLineColor();
		boolean rounded = lb.getRoundedCorners();
		if (thicker == true) {
			return new LineBorder(lineColor, thickness + DIFF, rounded);
		} else {
			return new LineBorder(lineColor, thickness - DIFF, rounded);	
		}
	}
	
	private EmptyBorder changeEmptyBorder(EmptyBorder eb, boolean thicker) {
		
		Insets insets = eb.getBorderInsets();
		// int top, int left, int bottom, int right
		if (thicker == true) {
			return new EmptyBorder(insets.top + DIFF, insets.left + DIFF,insets.bottom + DIFF, insets.right + DIFF);
		} else {
			return new EmptyBorder(insets.top - DIFF, insets.left - DIFF,insets.bottom - DIFF, insets.right - DIFF);
		}
	}

	@Override
	public void focusGained(FocusEvent e) {
		if (e.getSource() instanceof AbstractButton) {
			AbstractButton b = (AbstractButton) e.getSource();
			if (b.getBorder() != null ) {
				if (b.getBorder() instanceof LineBorder) {
					b.setBorder(changeLineBorder((LineBorder) b.getBorder(), true));
					//	b.paint(b.getGraphics());
					
				} else if (b.getBorder() instanceof EmptyBorder) {
					b.setBorder(changeEmptyBorder((EmptyBorder) b.getBorder(), true));
					
				} else if (b.getBorder() instanceof CompoundBorder) { // EventButton
					// shows focus when border is changed:
					CompoundBorder cb = (CompoundBorder) b.getBorder();
					Border insideBorder = cb.getInsideBorder();
					Border outsideBorder = cb.getOutsideBorder();
					b.setBorder(new CompoundBorder(insideBorder, outsideBorder));
					//		b.repaint();
					//b.paint(b.getGraphics());
				}
			}
		}		
	}

	@Override
	public void focusLost(FocusEvent e) {
		if (e.getSource() instanceof AbstractButton) {
			AbstractButton b = (AbstractButton) e.getSource();
			if (b.getBorder() != null ) {
				//Container parent = b.getParent();
				if (b.getBorder() instanceof LineBorder) {
					b.setBorder(changeLineBorder((LineBorder) b.getBorder(), false));
					//b.repaint();
					//b.paint(b.getGraphics());
				} else if (b.getBorder() instanceof EmptyBorder) {
					b.setBorder(changeEmptyBorder((EmptyBorder) b.getBorder(), false));
				}
			} 
		}		
	}
	/**
	 * Get the instance of listener to show the focus on 
	 * buttons with border. 
	 * 
	 * @return	the FocusListener to show the focus
	 */
	public static BorderButtonFocusListener getInstance() {
		if (bbfl == null) {
			bbfl = new BorderButtonFocusListener();
		}
		return bbfl;
	}
}
