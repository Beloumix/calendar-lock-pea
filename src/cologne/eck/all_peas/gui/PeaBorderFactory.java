package cologne.eck.all_peas.gui;

/*
 * PeaFactory - Production of Password Encryption Archives
 * Copyright (C) 2015  Axel von dem Bruch
 * 
 * This library is free software; you can redistribute it and/or modify it 
 * under the terms of the GNU General Public License as published 
 * by the Free Software Foundation; either version 2 of the License, 
 * or (at your option) any later version.
 * This library is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
 * or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU General Public License for more details.
 * See:  http://www.gnu.org/licenses/gpl-2.0.html
 * You should have received a copy of the GNU General Public License 
 * along with this library.
 */
import java.awt.Color;

import javax.swing.BorderFactory;
import javax.swing.border.Border;
import javax.swing.border.CompoundBorder;
import javax.swing.border.EtchedBorder;

import cologne.eck.all_peas.data.PeaProperties;


public class PeaBorderFactory {
	

	private static Border buttonBorder = BorderFactory.createCompoundBorder(
			BorderFactory.createCompoundBorder(
					BorderFactory.createEmptyBorder(2,2,2,2),// show the focus...
					BorderFactory.createEtchedBorder(EtchedBorder.RAISED)
					),
			BorderFactory.createEmptyBorder(3, 8, 3, 8)
			 );

	/**
	 * Create the default pea border, a CounpoundBorder with three borders
	 * (dark, bright, peaColor) for frames and dialogs. 
	 * 
	 * @param withMenu	true, if the border is used for a window with menu 
	 * 					(top and left borders are thinner then)
	 * @return			the CompoundBorder with three borders
	 */
	public static CompoundBorder createBorder(boolean withMenu) {
		
		CompoundBorder threeBorder;
		int[] ciphVals = PeaProperties.getCipherPeaColor();
		Color ciphColor = new Color(ciphVals[0], ciphVals[1], ciphVals[2]);
		Color darkColor = ciphColor.darker();
		int[] plainVals = PeaProperties.getPlainPeaColor();
		Color plainColor = new Color(plainVals[0], plainVals[1], plainVals[2]);
		
		if (withMenu == true) {
			
			Border compoundBorder = BorderFactory.createCompoundBorder(
					BorderFactory.createMatteBorder(1,1,1,1, darkColor),//new Color(160, 178, 162)),
					BorderFactory.createMatteBorder(3,3,3,3, plainColor)//new Color(242, 255, 243)) 
							);
			threeBorder = BorderFactory.createCompoundBorder(
					compoundBorder,
					BorderFactory.createMatteBorder(3,3,3,3, ciphColor)//PswDialogView.getPeaColor())
							);
		} else {
			Border compoundBorder = BorderFactory.createCompoundBorder(
					BorderFactory.createMatteBorder(1,1,1,1, darkColor),//new Color(160, 178, 162)),
					BorderFactory.createMatteBorder(3,3,3,3, plainColor)//new Color(242, 255, 243)) 
							);
			int[] colorVals = PeaProperties.getPeaColor();
			threeBorder = BorderFactory.createCompoundBorder(
					compoundBorder,					
					BorderFactory.createMatteBorder(3,3,3,3, new Color(colorVals[0], colorVals[1], colorVals[2]))//PswDialogView.getPeaColor())
							);			
		}
		return threeBorder;
	}
	
	public static Border getButtonBorder() {
		return buttonBorder;
	}
}