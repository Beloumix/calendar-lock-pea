package cologne.eck.all_peas.gui;

/*
 * PeaFactory - Production of Password Encryption Archives
 * Copyright (C) 2015  Axel von dem Bruch
 * 
 * This library is free software; you can redistribute it and/or modify it 
 * under the terms of the GNU General Public License as published 
 * by the Free Software Foundation; either version 2 of the License, 
 * or (at your option) any later version.
 * This library is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
 * or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU General Public License for more details.
 * See:  http://www.gnu.org/licenses/gpl-2.0.html
 * You should have received a copy of the GNU General Public License 
 * along with this library.
 */

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JCheckBox;
import javax.swing.JFileChooser;
//import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;

import cologne.eck.all_peas.data.PeaProperties;
import cologne.eck.tools.TestLog;

/**
 * 
 *
 * Customized JFileChooser: location is location of parent, not the middle of the screen,
 * icon is not java default icon but PEA icon. 
 * Static method to customize FileChooser (Label...). 
 */

@SuppressWarnings("serial")
public class PeaFileChooser extends JFileChooser implements ActionListener{	

	private static File lastFile;

	public PeaFileChooser (Component comp){
		
		if (lastFile != null) {
			this.setCurrentDirectory(lastFile);
			//this.setSelectedFile(lastFile);
		}
	}
	
	public PeaFileChooser (Object window) { 
		Component comp = null;
		if (window instanceof Component) {
			comp = (Component) window;
		} else {
			TestLog.ve(getClass(), "Missing window owner", 5);
		}
		if (lastFile != null) {
			this.setCurrentDirectory(lastFile);
		}
		if (comp != null && comp.isVisible()) {
			this.setLocation(comp.getLocationOnScreen());
		} else {
			TestLog.ve(getClass(), "Missing location of owner " + (comp != null) , 5);
		}
	}

    /**
     * Adds a title and message on top of the file chooser.
     * Note: This may not work for all Look&Feels. 
     * 
     * @param titleText			the text of the title to set
     * @param instructionText	the text of the message to set on top of the file chooser
     * @param chooser			the file chooser to customize
     */
	public void customizeFileChooser(String titleText, String instructionText, 
			boolean hiddenFilesOption) {
	    // More than customizing the first child panel is error prone
	    // because different Look&Feels build different JFileChoosers. 
	    // For example GTK+  does not work with customization of other panels
		
		if (titleText != null) {
			this.setDialogTitle(titleText);
		}
		
		BorderLayout layout = null;
		try {
			layout = (BorderLayout) this.getLayout();
		} catch (Exception e) {
			// this should not happen
			TestLog.e(PeaFileChooser.class, "Missing BorderLayout for JFileChooser");
			return;
		}

		// get the components of the chooser
		Component[]  comps = this.getComponents();
		// get the component of Page_Start if available
		Component pageStartComp = null;
		Object pageStartConstraints = null;

		int compsLen = comps.length;
		for (int i = 0; i < compsLen; i++) {
			Object constraints = layout.getConstraints( comps[i]);
			//TestLog.o(XXXX.class, i + " constr: " + constraints + ",  " + comps[i].getClass());
			if (constraints.equals(BorderLayout.NORTH)
					//|| constraints.equals(BorderLayout.PAGE_START)
					|| constraints.equals(BorderLayout.BEFORE_FIRST_LINE)){ 
				pageStartComp = comps[i];
				pageStartConstraints = constraints;
				break;
			} 
		}				
		if (instructionText != null) {
			// build a new panel, add the message 
			// and add the existing panel at page start:
			JPanel northPanel = new JPanel();
			northPanel.setLayout(new BoxLayout(northPanel, BoxLayout.Y_AXIS));
			
			JPanel messagePanel = new JPanel();			
			messagePanel.setLayout(new BoxLayout(messagePanel, BoxLayout.X_AXIS));
			
			JTextArea messageLabel = new JTextArea(instructionText);
			messageLabel.setEditable(false);
			messageLabel.setFont(new Font(Font.SANS_SERIF, Font.ITALIC, PeaProperties.getFontSize() + 2));
			
			messagePanel.add(messageLabel);
			messagePanel.add(Box.createHorizontalStrut(10));
			
			northPanel.add(messagePanel);
			northPanel.add(Box.createVerticalStrut(20));

			if (pageStartComp != null){ 
				northPanel.add(pageStartComp);
				this.add(northPanel, pageStartConstraints);
			} else {
				// Look&Feel Motif
				this.add(northPanel, BorderLayout.PAGE_START);
			}
		}
		if (hiddenFilesOption == true) {
			// Add check box for hidden files:
			JCheckBox hideCheck = new JCheckBox("<html>" + PeaProperties.getVmBridge().translate("show") 
					+ "<br/>" + PeaProperties.getVmBridge().translate("hidden") 
					+ "<br/>" + PeaProperties.getVmBridge().translate("files") + "?</html>");
			hideCheck.addActionListener(this);
			hideCheck.setActionCommand("hide");
			hideCheck.getAccessibleContext().
		    setAccessibleDescription(
		    	    "Clicking this component causes showing or hiding of hidden files.");
			this.setAccessory(hideCheck);
		}
		
		if (lastFile != null) {
			//this.setSelectedFile(lastFile);
			this.setCurrentDirectory(lastFile);// if last file is file: parent folder
		}
		this.invalidate();
		this.validate();
		this.repaint();
	}
	
	/**
	 * Remember the selected file to display next time:
	 */
	@Override
    public void approveSelection() {
        //lastFile = getSelectedFile();
        setLastFile(getSelectedFile());
        super.approveSelection();
    }
	
	/**
	 * Same like JFileChoosers method, parent will
	 * be casted to Component.
	 * 
	 * @param parent	the parent component of this file chooser
	 * 
	 * @return	0 = JFileChooser.APPROVE_OPTION, 
	 * 			1 = JFileChooser.CANCEL_OPTION, 
	 * 			-1 = JFileChooser.ERROR_OPTION if an error occurs or the dialog is dismissed 
	 */
    public int showOpenDialog(Object parent) {
        return super.showOpenDialog((Component) parent);
    }

	@Override
	public void actionPerformed(ActionEvent ape) {
		String command = ape.getActionCommand();
		if (command.equals("hide")){
			JCheckBox hideCheck = (JCheckBox) ape.getSource();
			if (hideCheck.isSelected() == true) {
				this.setFileHidingEnabled(false);
			} else {
				this.setFileHidingEnabled(true);
			}
		}		
	}
	
	private static synchronized void setLastFile(File newLastFile) {
		PeaFileChooser.lastFile = newLastFile;
	}
}
