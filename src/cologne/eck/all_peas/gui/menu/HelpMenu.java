package cologne.eck.all_peas.gui.menu;

/*
 * PeaFactory - Production of Password Encryption Archives
 * Copyright (C) 2015  Axel von dem Bruch
 * 
 * This library is free software; you can redistribute it and/or modify it 
 * under the terms of the GNU General Public License as published 
 * by the Free Software Foundation; either version 2 of the License, 
 * or (at your option) any later version.
 * This library is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
 * or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU General Public License for more details.
 * See:  http://www.gnu.org/licenses/gpl-2.0.html
 * You should have received a copy of the GNU General Public License 
 * along with this library.
 */
import java.awt.Font;
import java.awt.Point;
import java.awt.Toolkit;
import java.awt.Window;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.StringSelection;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;

import cologne.eck.all_peas.control.PeaControl;
import cologne.eck.all_peas.control.UpdateChecker;
import cologne.eck.all_peas.data.Attachments;
import cologne.eck.all_peas.data.PeaProperties;
import cologne.eck.all_peas.gui.HtmlFileViewer;
import cologne.eck.all_peas.gui.PeaDialog;
import cologne.eck.all_peas.gui.PswDialogView;
import cologne.eck.all_peas.vm_specific.JREProperties;
import cologne.eck.peafactory.crypto.CipherStuff;
import cologne.eck.peafactory.crypto.HashStuff;
import cologne.eck.peafactory.crypto.kdf.KeyDerivation;
import cologne.eck.tools.Comparator;
import cologne.eck.tools.Converter;
import cologne.eck.tools.ReadResources;
import cologne.eck.tools.TestLog;
import settings.PeaSettings;



@SuppressWarnings("serial")
public class HelpMenu extends JMenu implements ActionListener {
	

	/**
	 * Help menu for menu bar
	 * 
	 * @param mnemo	character to use a smnemonic
	 */
	public HelpMenu( char mnemo) {//Window _owner) {
		
		//PswDialogView pdv = PswDialogView.getInstance();
		
		Font font = new Font(Font.SANS_SERIF, Font.PLAIN, PeaProperties.getFontSize());
		this.setFont(font);

		this.setText(PeaProperties.getVmBridge().translate("help"));
		this.setMnemonic(mnemo);//PswDialogView.getInstance().getMnemo(this, PeaProperties.getVmBridge().translate("help")));
		this.getAccessibleContext().
	    setAccessibleDescription(
	    	    "Clicking this component opens a menu to get help and further informations.");
		
		JMenuItem instructionItem = new JMenuItem(PeaProperties.getVmBridge().translate("instruction"));
		instructionItem.getAccessibleContext().
	    setAccessibleDescription(
	    	    "Clicking this component displays a short instruction text.");
		instructionItem.setActionCommand("instruction");
		instructionItem.setFont(font);
		//instructionItem.setMnemonic(pdv.getMnemo(this, PeaProperties.getVmBridge().translate("instruction")));
		instructionItem.addActionListener(this);
		this.add(instructionItem);
		
		JMenuItem cloudInstructionItem = new JMenuItem(PeaProperties.getVmBridge().translate("infos")
				+ ": " + PeaProperties.getVmBridge().translate("cloud"));
		cloudInstructionItem.getAccessibleContext().
	    setAccessibleDescription(
	    	    "Clicking this component displays a short instruction text for the cloud access.");
		cloudInstructionItem.setActionCommand("cloudInstruction");
		cloudInstructionItem.setFont(font);
		//cloudInstructionItem.setMnemonic(pdv.getMnemo(this, PeaProperties.getVmBridge().translate("cloud")));
		cloudInstructionItem.addActionListener(this);
		this.add(cloudInstructionItem);
		
		JMenuItem webItem = new JMenuItem(PeaProperties.getVmBridge().translate("web"));
		webItem.getAccessibleContext().
	    setAccessibleDescription(
	    	    "Clicking this component displays addresses to find further informations in the web.");
		webItem.setActionCommand("web");
		webItem.setFont(font);
		//webItem.setMnemonic(pdv.getMnemo(this, PeaProperties.getVmBridge().translate("web")));
		webItem.addActionListener(this);
		this.add(webItem);
		
		if (PeaControl.isPlainTextMode() == false) {//JREProperties.getMainWindow() instanceof PswDialogView) {
			JMenuItem rememberFileItem = new JMenuItem(PeaProperties.getVmBridge().translate("edit_remembered_files"));
			rememberFileItem.getAccessibleContext().
			setAccessibleDescription(
					"Clicking this component opens a dialog to help editing remembered files.");
			rememberFileItem.setActionCommand("remembered-files");
			rememberFileItem.setFont(font);
			//rememberFileItem.setMnemonic(pdv.getMnemo(this, PeaProperties.getVmBridge().translate("edit_remembered_files")));
			rememberFileItem.addActionListener(this);
			this.add(rememberFileItem);				
		}
		
		JMenuItem pathFileItem = new JMenuItem(PeaProperties.getVmBridge().translate("path_file"));
		pathFileItem.getAccessibleContext().
	    setAccessibleDescription(
	    	    "Clicking this component opens a dialog to show and modify file to remember when the app opens.");
		pathFileItem.setActionCommand("pathFile");
		pathFileItem.setFont(font);
		//pathFileItem.setMnemonic(pdv.getMnemo(this, PeaProperties.getVmBridge().translate("path_file")));
		pathFileItem.addActionListener(this);
		this.add(pathFileItem);		


		JMenuItem aboutItem = new JMenuItem(PeaProperties.getVmBridge().translate("about"));
		aboutItem.getAccessibleContext().
	    setAccessibleDescription(
	    	    "Clicking this component shows infos about this program.");
		aboutItem.setActionCommand("about");
		aboutItem.setFont(font);
		//aboutItem.setMnemonic(pdv.getMnemo(this, PeaProperties.getVmBridge().translate("about")));
		aboutItem.addActionListener(this);
		this.add(aboutItem);
		

		JMenuItem readmeItem = new JMenuItem("README");
		readmeItem.getAccessibleContext().
	    setAccessibleDescription(
	    	    "Clicking this component shows the README infos.");
		readmeItem.setActionCommand("readme");
		readmeItem.setFont(font);
		//aboutItem.setMnemonic(pdv.getMnemo(this, PeaProperties.getVmBridge().translate("about")));
		readmeItem.addActionListener(this);
		this.add(readmeItem);

		if (PeaSettings.getKeyFileProperty() == true
				&& (PeaControl.isPlainTextMode() == true) ) { // only in lock frame
			JMenuItem keyPropItem = new JMenuItem(PeaProperties.getVmBridge().translate("show_key_prop"));
			keyPropItem.getAccessibleContext().
		    setAccessibleDescription(
		    	    "Clicking this component shows the key of the program as hexadecimal value after a password entry.");
			keyPropItem.setActionCommand("keyProp");
			keyPropItem.setFont(font);
			//keyPropItem.setMnemonic(pdv.getMnemo(this, PeaProperties.getVmBridge().translate("show_key_prop")));
			keyPropItem.addActionListener(this);
			this.add(keyPropItem);
		} 
	
		JMenuItem checkUpdateItem = new JMenuItem(PeaProperties.getVmBridge().translate("check_update"));
		checkUpdateItem.getAccessibleContext().
	    setAccessibleDescription(
	    	    "Clicking this component shows infos about available updates of the program.");
		checkUpdateItem.setActionCommand("checkUpdate");
		checkUpdateItem.setFont(font);
		//checkUpdateItem.setMnemonic(pdv.getMnemo(this, PeaProperties.getVmBridge().translate("check_update")));
		checkUpdateItem.addActionListener(this);
		this.add(checkUpdateItem);
	}

	
	@Override
	public void actionPerformed(ActionEvent ae) {
		String command = ae.getActionCommand();
		Window owner = JREProperties.getMainWindow();

		//TestLog.o(HelpMenu.class, "command: " + command);
		if (command.equals("instruction")){
			HtmlFileViewer hfv = new HtmlFileViewer("first_time_guide", owner);
			hfv.requestFocus();
			
		} else if (command.equals("cloudInstruction")){
			HtmlFileViewer hfv = new HtmlFileViewer("cloud_guide", owner);
			hfv.requestFocus();

		} else if (command.equals("web")){
			
			PswDialogView.showInfoDialog(owner);//parentWindow);

		} else if (command.equals("remembered-files")){
			
			PeaDialog.showMessage(this, PeaProperties.getVmBridge().translate("edit_remembered_files_text"), null, -1);
			
		} else if (command.equals("pathFile")){

			Point point = SwingUtilities.getWindowAncestor(this).getLocationOnScreen();
			PathFileSetting ps = PathFileSetting.getInstance(SwingUtilities.getWindowAncestor(this), point);
			if (ps.isVisible() == false) {
				ps.setVisible(true);
			}

		} else if (command.equals("checkUpdate")) {
			TestLog.o(HelpMenu.class, "check for updates...");
			
			UpdateChecker.checkUpdates(this, true);
			
		} else if (command.equals("about")) {
			String peaName = null;
			if (PeaProperties.getFileType().equals("image")){
				peaName = "Image Lock PEA";
			} else if (PeaProperties.getFileType().equals("file")){
				peaName = "File Lock PEA";
			} else if (PeaProperties.getFileType().equals("text file")){
				peaName = "Notebook PEA";
			} else if (PeaProperties.getFileType().equals("calendar")) {
				peaName = "Calendar Lock PEA";
			} else if (PeaProperties.getFileType().equals("contact")) {
				peaName = "Contact PEA";
			}
			PswDialogView.showAboutDialog(peaName, null, SwingUtilities.getWindowAncestor(this));

		} else if (command.equals("readme")) {
			//Object readme = null;
			byte[] readmeBytes = ReadResources.getResourceFromJAR("README.txt");
			if (readmeBytes != null) {
				String messageToShow = new String(readmeBytes, PeaProperties.getCharset());
				PeaDialog.showMessageWithSize(SwingUtilities.getWindowAncestor(this), 
						messageToShow, "README", -1, 400, 400);
			} else {
				String extraMessage = "";
				if (ReadResources.getLastErrorMessage() != null) {
					extraMessage = ReadResources.getLastErrorMessage();
				}
				PeaDialog.showMessage(SwingUtilities.getWindowAncestor(this), 
						PeaProperties.getVmBridge().translate("read/write failed")
						+ "\n" + extraMessage, 
						PeaProperties.getVmBridge().translate("error"), 0);
			}
			
		} else if (command.equals("keyProp")) {
			// password dialog
			char[] psw = PeaDialog.showPasswordDialog(this, null, 
					PeaProperties.getVmBridge().translate("key_file_property"), true);
			if (psw == null ) {
				return;
			} else if (psw.length == 0) {
				psw = "no password".toCharArray();
			} else if (Comparator.compare(psw, "cAnCeLlEd".toCharArray())) {
				TestLog.v(getClass(), "Process cancelled");
				return;
			}
			byte[] pswInputBytes = Converter.chars2bytes(psw); 
			// derive key and compare
			byte[] pswHash = HashStuff.generateHKDFBytes(pswInputBytes, 
					Attachments.getProgramRandomBytes(), // if no key file property: this is a fixed string
					64); // size of pswHash: 64 bytes
			byte[] keyMaterial = KeyDerivation.deriveKey(pswHash);
			if (keyMaterial == null) {
				PeaDialog.showMessage(JREProperties.getMainWindow(), PeaProperties.getVmBridge().translate("unexpected_error"));
				TestLog.e(HelpMenu.class, "Key derivation failed");
				return;
			} 
			boolean fits = Comparator.compare(CipherStuff.getSessionKeyCrypt().getKey(), 
					keyMaterial);
			if (fits == false) {
				PeaDialog.showMessage(JREProperties.getMainWindow(), PeaProperties.getVmBridge().translate("password_failed"));
				TestLog.e(HelpMenu.class, "Wrong password");
				return;
			}
			// get value and convert in hex
			String secret = Converter.bytes2hex(Attachments.getProgramRandomBytes());
			// TODO QR code (zxing.jar)
			JPanel keyPanel = new JPanel();
			keyPanel.setLayout(new BoxLayout(keyPanel, BoxLayout.PAGE_AXIS));
			JTextField textField = new JTextField();//Attachments.getProgramRandomBytesSize() * 2);
			//textField.setBorder(null);
			//textArea.setBackground(PeaProperties.getPlainPeaColor());
			textField.setText(secret);
			textField.setEditable(false);
			keyPanel.add(textField);
			JButton keyButton = new JButton(PeaProperties.getVmBridge().translate("copy"));
			keyButton.addActionListener(this);
			keyButton.setActionCommand("copyKey");
			keyPanel.add(keyButton);
			
			JOptionPane.showMessageDialog(this, 
					keyPanel, 
					PeaProperties.getVmBridge().translate("key_file_property"), 1, 
					new ImageIcon(PswDialogView.getImage()));
		} else if (command.equals("copyKey")) {
			JTextField textField = null;
			if (ae.getSource() instanceof JButton) {
				if (((JButton)ae.getSource()).getParent() instanceof JPanel){
					if (((JPanel)((JButton)ae.getSource()).getParent()).getComponent(0) instanceof JTextField) {
						textField = (JTextField) ((JPanel)((JButton)ae.getSource()).getParent()).getComponent(0);
					} else {
						TestLog.ve(getClass(), "Invalid source", 5);
					}
				} else {
					TestLog.ve(getClass(), "Invalid source", 5);
				}
			} else {
				TestLog.ve(getClass(), "Invalid source", 5);
			}
			if (textField != null) {
				StringSelection selection = new StringSelection(textField.getText());
				Clipboard clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
				clipboard.setContents(selection, null);
				textField.setText("");
			}
		} else {
			TestLog.o(HelpMenu.class, "Invalid command in help menu");
		}
	}
}
