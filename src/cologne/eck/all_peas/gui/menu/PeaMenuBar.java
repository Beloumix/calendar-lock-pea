package cologne.eck.all_peas.gui.menu;

import java.awt.Color;

/*
 * PeaFactory - Production of Password Encryption Archives
 * Copyright (C) 2015  Axel von dem Bruch
 * 
 * This library is free software; you can redistribute it and/or modify it 
 * under the terms of the GNU General Public License as published 
 * by the Free Software Foundation; either version 2 of the License, 
 * or (at your option) any later version.
 * This library is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
 * or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU General Public License for more details.
 * See:  http://www.gnu.org/licenses/gpl-2.0.html
 * You should have received a copy of the GNU General Public License 
 * along with this library.
 */

import java.awt.Font;

import javax.swing.BoxLayout;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JPopupMenu;
import javax.swing.border.Border;

import cologne.eck.all_peas.data.PeaProperties;
import cologne.eck.all_peas.gui.PeaBorderFactory;


@SuppressWarnings("serial")
public class PeaMenuBar extends JMenuBar {
	
	private Border compoundBorder = PeaBorderFactory.createBorder(true);//BorderFactory.createCompoundBorder(
	
	private Border currentBorder = compoundBorder;
	
	public PeaMenuBar(){
		
		this.setLayout(new BoxLayout(this, BoxLayout.LINE_AXIS));

		this.setBorder(currentBorder);
		this.setBorderPainted(true);
		Font font = new Font(Font.SANS_SERIF, Font.PLAIN, PeaProperties.getFontSize());
		this.setFont(font);
		
		int[] plainVals = PeaProperties.getPlainPeaColor();
		this.setBackground(new Color(plainVals[0], plainVals[1], plainVals[2]));
		//this.setPreferredSize(new Dimension((int)PswDialogView.getScreenwidth(), 40));
	}
	
	@Override
	public JMenu add(JMenu c) {
		Font font = new Font(Font.SANS_SERIF, Font.PLAIN, PeaProperties.getFontSize());
		c.setFont(font);
		c.setOpaque(true);		
		//c.setRolloverEnabled(true);
		JPopupMenu popup = c.getPopupMenu();
		int[] plainVals = PeaProperties.getPlainPeaColor();
		c.setBackground(new Color(plainVals[0], plainVals[1], plainVals[2]));

		popup.setBorder(currentBorder);
		return super.add(c);		
	}
	
	
	public void setNewBorderX( Border newBorder) {
		currentBorder = newBorder;
	}
}
