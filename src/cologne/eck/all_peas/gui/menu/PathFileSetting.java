package cologne.eck.all_peas.gui.menu;

/*
 * Peafactory - Production of Password Encryption Archives
 * Copyright (C) 2015  Axel von dem Bruch
 * 
 * This library is free software; you can redistribute it and/or modify it 
 * under the terms of the GNU General Public License as published 
 * by the Free Software Foundation; either version 2 of the License, 
 * or (at your option) any later version.
 * This library is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
 * or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU General Public License for more details.
 * See:  http://www.gnu.org/licenses/gpl-2.0.html
 * You should have received a copy of the GNU General Public License 
 * along with this library.
 */

/**
 * Settings for the password hashing scheme Pomelo. 
 */

import java.awt.Dimension;
//import java.awt.Font;
import java.awt.Point;
import java.awt.Window;
import java.awt.event.KeyEvent;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;


import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.KeyStroke;

import cologne.eck.all_peas.control.PathFileManager;
import cologne.eck.all_peas.data.PeaProperties;
import cologne.eck.all_peas.gui.CloseOnEscapeAction;
import cologne.eck.all_peas.gui.PswDialogView;
import cologne.eck.tools.ReadResources;
import cologne.eck.tools.WriteResources;


@SuppressWarnings("serial")
class PathFileSetting extends JDialog implements ActionListener {
	
	private static PathFileSetting pathFileSetting;
	
	private JTextArea pathArea;
	
	private PathFileSetting(Window parent, Point loc) {

		super(parent);		
		pathFileSetting = this;
		pathFileSetting.setAlwaysOnTop(true);		
		
		this.setTitle(PeaProperties.getVmBridge().translate("path_file_setting"));				
		this.setIconImage(PswDialogView.getImage());
		
		this.getRootPane().getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW)
        	.put(KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), "CLOSE_ON_ESCAPE");
		this.getRootPane().getActionMap().put("CLOSE_ON_ESCAPE", new CloseOnEscapeAction() );
		
		JPanel pane = (JPanel) pathFileSetting.getContentPane();//new JPanel();
		pane.setLayout(new BoxLayout(pane, BoxLayout.Y_AXIS));
//		pane.setBorder(PeaBorderFactory.createBorder(false));//.setBorder(new EmptyBorder(5,5,5,5));
		
		JTextArea infoArea = new JTextArea();
		infoArea.setEditable(false);
		infoArea.setLineWrap(true);
		infoArea.setWrapStyleWord(true);
		infoArea.setText(PeaProperties.getVmBridge().translate("path_file_info_1") 
				+ "\n      " 
//				+ System.getProperty("user.dir") + java.io.File.separator
				+ PathFileManager.getPathFileName()
		+ "\n\n" + PeaProperties.getVmBridge().translate("path_file_info_2") + "\n");

		pane.add(infoArea);
		pane.add(Box.createVerticalStrut(10));
		
		pathArea = new JTextArea();
		String content = " ";
		if (new File(PathFileManager.getPathFileName()).exists()
				&& new File(PathFileManager.getPathFileName()).length() > 0){
			content = new String(
				ReadResources.readExternFile(PathFileManager.getPathFileName()), PeaProperties.getCharset());
			//TestLog.o(XXXX.class, content);
		}

		pathArea.setText(content);
		
		JScrollPane scrollPane = new JScrollPane(pathArea);
		scrollPane.setPreferredSize(new Dimension (400, 200));
		
		pane.add(scrollPane);
				
		String okString = PeaProperties.getVmBridge().translate("ok");
		JButton okButton = new JButton(okString);
		okButton.setMnemonic(okString.charAt(0));
		okButton.setActionCommand("newSetting");
		okButton.addActionListener(this);
		pane.add(okButton);
		pathFileSetting.pack();
//		this.getParent().getLocationOnScreen();
		pathFileSetting.setLocation(loc);
		pathFileSetting.setVisible(true);
	}
	
	protected static PathFileSetting getInstance(Window parent, Point p) {
		if (pathFileSetting == null) {
			return new PathFileSetting(parent, p);
		} else {
			return pathFileSetting;
		}
	}

	@Override
	public void actionPerformed(ActionEvent ape) {

		String command = ape.getActionCommand();
		
		if (command.equals("newSetting")) {
			// leading or trailing whitespace may cause bugs
			String areaText = pathArea.getText();
			if ((new File(PathFileManager.getPathFileName()).exists() == false)
					&& areaText.equals(" ")) {
				pathFileSetting.dispose();
				pathFileSetting = null;
				return;
			}
			
			String newPathFiles = "";
			String[] lines = areaText.split("\n");
			int len = lines.length;
			if (len > 0) {
				for (int i = 0; i < len-1; i++){
					newPathFiles += lines[i].trim() + "\n"; // delete whitespace
				}
				// last line without "\n":
				newPathFiles += lines[len-1].trim();
			}
			// write String without leading and trailing whitespace:
			WriteResources.writeText(newPathFiles, PathFileManager.getPathFileName());			
			
			pathFileSetting.dispose();
			pathFileSetting = null;
		}
	}
}
