package cologne.eck.all_peas.gui.menu;


/*
 * PeaFactory - Production of Password Encryption Archives
 * Copyright (C) 2020  Axel von dem Bruch
 * 
 * This library is free software; you can redistribute it and/or modify it 
 * under the terms of the GNU General Public License as published 
 * by the Free Software Foundation; either version 2 of the License, 
 * or (at your option) any later version.
 * This library is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
 * or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU General Public License for more details.
 * See:  http://www.gnu.org/licenses/gpl-2.0.html
 * You should have received a copy of the GNU General Public License 
 * along with this library.
 */
import java.awt.Font;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.net.MalformedURLException;
import java.util.ArrayList;

import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.SwingUtilities;

import cologne.eck.all_peas.cloud.CloudControl;
import cologne.eck.all_peas.control.PeaControl;
import cologne.eck.all_peas.data.PeaProperties;
import cologne.eck.all_peas.gui.CryptoSetter;
import cologne.eck.all_peas.gui.KeyFileDialogs;
import cologne.eck.all_peas.gui.PeaDialog;
import cologne.eck.all_peas.gui.PeaLockFrame;
import cologne.eck.all_peas.gui.PswDialogView;
import cologne.eck.all_peas.vm_specific.JREProperties;
import cologne.eck.peafactory.crypto.AlgoParamHandler;
import cologne.eck.peafactory.crypto.kdf.KeyDerivation;
import cologne.eck.peafactory.crypto.kdf.SingleIterationPBKDF2;
import cologne.eck.tools.Converter;
import cologne.eck.tools.PropertyHandler;
//import cologne.eck.peafactory.gui.kdf_settings.Argon2Setting;
//import cologne.eck.peafactory.gui.kdf_settings.CatenaSetting;
import cologne.eck.tools.TestLog;
import cologne.eck.tools.WebTools;


@SuppressWarnings("serial")
public class SettingMenu extends JMenu implements ActionListener {
	
	private JMenuItem directStartItem;
	private JMenuItem keyFileItem;
	private JMenuItem changePswItem;
	
	
	/**
	 * Menu for settings: language, security/crypto, key file and password
	 * 
	 * @param encryptedMode	true: show in password dialog (encrypted mode),
	 * 						false: show in PeaLockFrame (decrypted mode)
	 * @param	the character to use as mnemonic
	 */
	public SettingMenu(boolean encryptedMode, char mnemo){//Window _owner) {
		
		//this.addActionListener(this);
		//this.setActionCommand("MENU");

		Font font = new Font(Font.SANS_SERIF, Font.PLAIN, PeaProperties.getFontSize());
		this.setFont(font);

		this.setText(PeaProperties.getVmBridge().translate("settings"));
		this.setMnemonic(mnemo);//PswDialogView.getInstance().getMnemo(this, PeaProperties.getVmBridge().translate("settings")));
		this.getAccessibleContext().
	    setAccessibleDescription(
	    	    "Clicking this component opens a menu for settings.");
		
		String[] langs = PeaProperties.getVmBridge().getAvailableLanguages();
		StringBuilder builder = new StringBuilder();
		for(String s : langs) {
		    builder.append(s + " ");
		}
		TestLog.v(SettingMenu.class, "Available languges: " + builder.toString());
		//PswDialogView pdv = PswDialogView.getInstance();
		
		JMenu langMenu = new JMenu(PeaProperties.getVmBridge().translate("language"));
		langMenu.getAccessibleContext().
	    setAccessibleDescription(
	    	    "Clicking this component displays a list of available languages.");
		langMenu.setFont(font);
		//langMenu.setMnemonic(pdv.getMnemo(this, PeaProperties.getVmBridge().translate("language")));
		this.add(langMenu);
		
		for (String lang : langs) {
			JMenuItem langItem = new JMenuItem(lang);
			langItem.getAccessibleContext().
		    setAccessibleDescription(
		    	    "Clicking this component changes the laguage to this language.");
			langItem.setFont(font);
			langItem.addActionListener(this);
			if (lang != null && lang.length() > 1) {
				//langItem.setMnemonic(pdv.getMnemo(langMenu, lang));
			}
			langItem.setActionCommand("LANG" + lang);			
			langMenu.add(langItem);
		}
		JMenuItem installLangItem = new JMenuItem(PeaProperties.getVmBridge().translate("install") + " " +
				PeaProperties.getVmBridge().translate("language"));
		installLangItem.getAccessibleContext().
	    setAccessibleDescription(
	    	    "Clicking this component opens a selection to install a new language.");
		installLangItem.setFont(font);
		installLangItem.addActionListener(this);
		//installLangItem.setMnemonic(pdv.getMnemo(langMenu, installLangItem.getText()));

		installLangItem.setActionCommand("installLang");			
		langMenu.add(installLangItem); 		


		directStartItem = new JMenuItem();
		if (PeaProperties.isDirectStart() == true) {
			directStartItem.setText(PeaProperties.getVmBridge().translate("disable_direct_start"));
		} else {
			directStartItem.setText(PeaProperties.getVmBridge().translate("enable_direct_start"));
		}
		directStartItem.getAccessibleContext().
		setAccessibleDescription(
				"Clicking this component enables or disables the direct start mode.");
		directStartItem.setActionCommand("directStart");
		directStartItem.setFont(font);
		//keyFileItem.setMnemonic(pdv.getMnemo(this, PeaProperties.getVmBridge().translate("key_file")));
		directStartItem.addActionListener(this);
		this.add(directStartItem);
		if (PeaProperties.isKeyFileOnlyMode() == true ) { // show only when keyfile-only mode
			directStartItem.setVisible(true);
		} else {
			directStartItem.setVisible(false);
		}
		
		JMenuItem cryptoItem = new JMenuItem(PeaProperties.getVmBridge().translate("security_settings"));
		cryptoItem.getAccessibleContext().
	    setAccessibleDescription(
	    	    "Clicking this component opens a dialog to set cryptographic algorithms and parameters.");
		cryptoItem.setActionCommand("securitySettings");
		cryptoItem.setFont(font);
		//cryptoItem.setMnemonic(pdv.getMnemo(this, PeaProperties.getVmBridge().translate("security_settings")));
		cryptoItem.addActionListener(this);
		this.add(cryptoItem);


		keyFileItem = new JMenuItem(PeaProperties.getVmBridge().translate("key_file"));
		keyFileItem.getAccessibleContext().
		setAccessibleDescription(
				"Clicking this component opens a dialog to create a key file.");
		keyFileItem.setActionCommand("keyFile");
		keyFileItem.setFont(font);
		//keyFileItem.setMnemonic(pdv.getMnemo(this, PeaProperties.getVmBridge().translate("key_file")));
		keyFileItem.addActionListener(this);
		this.add(keyFileItem);
		if (PeaProperties.isKeyFileOnlyMode() == false ) { // do not show in keyfile-only mode
			keyFileItem.setVisible(true);
		} else {
			keyFileItem.setVisible(false);
		}


		if (encryptedMode == false) { // only for PeaLockFrame

			changePswItem = new JMenuItem(PeaProperties.getVmBridge().translate("change_password"));//CalendarSetting.calTranslate("instruction"));
			changePswItem.getAccessibleContext().
			setAccessibleDescription(
					"Clicking this component opens a dialog to set a new password.");
			changePswItem.setActionCommand("changePsw");
			changePswItem.setFont(font);
			//changePswItem.setMnemonic(pdv.getMnemo(this, PeaProperties.getVmBridge().translate("change_password")));
			changePswItem.addActionListener(this);
			this.add(changePswItem);
			if (PeaProperties.isKeyFileOnlyMode() == false ) { // do not show in keyfile-only mode
				changePswItem.setVisible(true);
			} else {
				changePswItem.setVisible(false);
			}
		}
	}
	
	private void apply() {

		Window owner = JREProperties.getMainWindow();
		//TestLog.o(SettingMenu.class, "apply...");

		if (owner == null ) {
			TestLog.ve(SettingMenu.class, "Missing owner window", 5);
		}
		if (owner != null && owner instanceof PswDialogView) {
			// password was not yet typed
			String defaultAlgos = AlgoParamHandler.writeString(true);
			PeaProperties.setDefaultAlgoParamString(defaultAlgos);
			TestLog.o(SettingMenu.class, "New default algorithms and parameters: " + AlgoParamHandler.getReadableString(defaultAlgos, false));
			new PropertyHandler().setOneProperty("default_algo_param", defaultAlgos);

		} else {
			if (owner instanceof PeaLockFrame) {
				// do not set PeaProperties, this is done later
				// ask to apply changes or make further settings
				// TODO MultiFileSelection for Notebook, Image
				((PeaLockFrame) owner).storeValuesBeforeChange();
				String[] fileNamesToApply =((PeaLockFrame) owner).chooseFileNamesToApply();
				String errorMessage = ((PeaLockFrame) owner).changePasswordOrApplyChanges(owner, fileNamesToApply);
				if (errorMessage != null) {
					PeaDialog.showMessage(owner, 
							errorMessage, 
							PeaProperties.getVmBridge().translate("error"), 0);
				}
			} else {
				TestLog.ve(SettingMenu.class, "Invalid class of owner", 5);
			}
		}
	}

	@Override
	public void actionPerformed(ActionEvent ae) {

		String command = ae.getActionCommand();
		if (command.startsWith("LANG")) {

			String lang = command.substring(4);
			TestLog.v(SettingMenu.class, "Change language to " + lang);

			try {
				PeaProperties.getVmBridge().setLanguagesBundle(lang);
				PeaProperties.setLastLanguage(lang);
				if (JREProperties.getMainWindow() instanceof PswDialogView) {					
					PswDialogView.resetLanguage();
				} else if (JREProperties.getMainWindow() instanceof PeaLockFrame) {
					((PeaLockFrame) JREProperties.getMainWindow()).resetFrame();
				}
			} catch (MalformedURLException e) {
				TestLog.e(SettingMenu.class, e.getLocalizedMessage());
				TestLog.ve(SettingMenu.class, e.getLocalizedMessage() + " - " + e.toString(), 5);
			}

			new PropertyHandler().setOneProperty("language", lang);

		} else if (command.equals("installLang")) {
			
			installLanguages();

		} else if (command.equals("directStart")) {

			boolean directStart = PeaProperties.isDirectStart();
			if (directStart == true) {
				PeaProperties.setDirectStart(false);
				directStartItem.setText(PeaProperties.getVmBridge().translate("enable_direct_start"));
				TestLog.v(getClass(), "Direct start mode disabled");
			} else {
				PeaProperties.setDirectStart(true);
				directStartItem.setText(PeaProperties.getVmBridge().translate("disable_direct_start"));
				TestLog.v(getClass(), "Direct start mode enabled");
			}

		} else if (command.equals("securitySettings")) {

			CryptoSetter cs = new CryptoSetter(JREProperties.getMainWindow(), this);
			cs.setVisible(true);

		} else if (command.equals("keyFile")) { // crypto setting: must be applied
			// Not visible in keyfile-only mode
			if (PeaProperties.isKeyFileOnlyMode() == true) {
				TestLog.ve(getClass(), "Keyfile  setting, but is in keyfile-only mode", 5);
				return;
			}

			if (PeaProperties.getFileType().equals("calendar") || PeaProperties.getFileType().equals("contact")
					|| PeaProperties.getFileType().equals("text file")) { 
				Object lockFrame = PeaControl.getDialog().getLockFrame();
				if (lockFrame != null && lockFrame instanceof PeaLockFrame) {
					if ( ((PeaLockFrame) lockFrame).isContentUnsaved() == true) {
						// ask to save:
						if ( ((PeaLockFrame) lockFrame).checkUnsavedContentChanges() == true) {
							// content was saved
						} else {
							PeaDialog.showMessage(lockFrame, 
									PeaProperties.getVmBridge().translate("failed") + ": " 
											+ PeaProperties.getVmBridge().translate("settings")
											+ "\n(" + PeaProperties.getVmBridge().translate("unsaved_modifications") + ")");
							return;
						}
					} else {
						// all content was saved						
					}
				}
			} 

			if (CloudControl.getNumberOfRunningCloudProcesses() > 0) {
				// TODO only cancel if same file is to be uploaded twice
				// register uploaded files in CloudControl
				PeaDialog.showMessage(this, PeaProperties.getVmBridge().translate("cloud_process_warning"));
				return;
			}

			boolean setDefault = false;
			if (JREProperties.getMainWindow() instanceof PswDialogView) {
				setDefault = true;
				if (PeaProperties.getDefaultKeyFileName() != null) {
					int applyKeyFile = PeaDialog.showQuestion(SwingUtilities.getWindowAncestor(this), 
							PeaProperties.getVmBridge().translate("default_key_file_selected") + ": \n"
									+ PeaProperties.getDefaultKeyFileName() + "\n\n"
									+ PeaProperties.getVmBridge().translate("use_this_file"),  
									null, 0);
					if (applyKeyFile == 0) { // yes
						//createKeyFile = false;

						// apply key file to default files
						PeaProperties.setDefaultKeyFileFlag(1); // default 
						apply();
						return;
					} else if (applyKeyFile == 1) { // No
						setNonDefaultKeyFile(true);
						return;

					} else {
						return;
					}
				}

			} else {  // PeaLockFrame:

				if (PeaProperties.getDefaultKeyFileName() != null) {
					int applyKeyFile = PeaDialog.showQuestion(SwingUtilities.getWindowAncestor(this), 
							PeaProperties.getVmBridge().translate("default_key_file_selected") + ": \n"
									+ PeaProperties.getDefaultKeyFileName() + "\n\n"
									+ PeaProperties.getVmBridge().translate("use_this_file"), 
									null, 0);
					if (applyKeyFile == 0) { // yes
						// apply key file to current files
						PeaProperties.setKeyFileFlag(1); // default 
						apply();
						return;
					} else if (applyKeyFile == -1 || applyKeyFile == 2) { // close or cancel
						return;
					} else if (applyKeyFile == 1) { // No
						setNonDefaultKeyFile(false);
						return;
					}
				}
			}

			TestLog.o(SettingMenu.class, "Create key file...");

			int keyFileType  = KeyFileDialogs.setKeyFileStatus(setDefault);
			if (keyFileType == -1) {
				return;
			} else {	
				TestLog.o(SettingMenu.class, PeaProperties.getVmBridge().translate("key_file") + ": " +
						PeaProperties.getVmBridge().translate("success"));
				apply();
				return;
			}
		} else if (command.equals("changePsw")) { // crypto setting: must be applied
			TestLog.v(SettingMenu.class, "Change pasword...");
			if ( JREProperties.getMainWindow() != null 
					&& JREProperties.getMainWindow() instanceof PeaLockFrame) {
				// store cloud passwords to change in properties
				((PeaLockFrame) JREProperties.getMainWindow()).storeValuesBeforeChange();
				boolean changeSuccess = ((PeaLockFrame) JREProperties.getMainWindow()).changePassword();
				if (changeSuccess == false) {
					PeaDialog.showMessage(JREProperties.getMainWindow(), 
							PeaProperties.getVmBridge().translate("failed") + ": \n" +
									PeaProperties.getVmBridge().translate("change_password"), 
							PeaProperties.getVmBridge().translate("error"), 0);
				}
				
			} else {
				TestLog.ve(SettingMenu.class, PeaProperties.getVmBridge().translate("unexpected_error") + ": " +
						"Invalid main window", 5);
			}
		} else {
			TestLog.o(SettingMenu.class, "Invalid command in help menu: " + command);
			return;
		}
	}
	/**
	 * Show a dialog to install languages from the web
	 * 
	 * @return	true, if a language was installed, false otherwise
	 */
	public static boolean installLanguages() {

		boolean hasConnection = WebTools.checkConnection();
		if (hasConnection == false) {
			PeaDialog.showMessage(JREProperties.getMainWindow(), PeaProperties.getVmBridge().translate("bad_web_connection"), 
					PeaProperties.getVmBridge().translate("error"), 0);
			return false;
		}
		ArrayList<String> installedLangs = Converter.arrayToArrayList( PeaProperties.getVmBridge().getAvailableLanguages());

		// get available languages from web
		String langsInWeb = null;
		try {													
			// Before peafactory-0.4.4-alpha
			//langsInWeb = WebTools.readPropertyFromWeb("https://eck.cologne/peafactory/src/lang/lang.properties", "all_pea_langs");
			
			// new extra bundles for Image Lock PEA and Notebook PEA - bundle for allPEAs was changed
			// from peafactory-0.4.4-alpha changed to folder language, not lang
			langsInWeb = WebTools.readPropertyFromWeb("https://eck.cologne/peafactory/src/language/lang.properties", "all_pea_langs");
		} catch (Exception e) {
			TestLog.e(SettingMenu.class, e.getLocalizedMessage());
			TestLog.ve(SettingMenu.class, e.getLocalizedMessage() + " - " + e.toString(), 5);
			PeaDialog.showMessage(JREProperties.getMainWindow(), e.getLocalizedMessage(), 
					PeaProperties.getVmBridge().translate("error"), 0);
			return false;
		}			
		if (langsInWeb != null && langsInWeb.trim().length() > 1) {
			String[] langs = langsInWeb.split(",");
			// show only new languages:
			ArrayList<String> langList = new ArrayList<String>();
			for (String l : langs) {
				if ( ! installedLangs.contains(l)) {
					langList.add(l);
				}
				TestLog.v(SettingMenu.class, "Language from web: " + l);
			}
			if (langList != null) {
				langs = Converter.arrayListToArray(langList);
			}
			if (langs.length > 0) {
				int langIndex = PeaDialog.showOptionYAxis(JREProperties.getMainWindow(), 
						PeaProperties.getVmBridge().translate("language") + ": ", 
						null, 3, langs, 0);
				if (langIndex == -1) {// cancel
					return false;
				} else {
					TestLog.v(SettingMenu.class, PeaProperties.getVmBridge().translate("install") + ": " 
							+ PeaProperties.getVmBridge().translate("language") + " " + langs[langIndex]); 
					// Remove annotation if exists (e.g. experimental):
					if (langs[langIndex].contains(" ")) {
						// remove everything after space:
						langs[langIndex] = langs[langIndex].substring(0, langs[langIndex].indexOf(" "));
					}
					return downloadLanguageFiles(langs[langIndex]);
				}
			} else {
				TestLog.ve(SettingMenu.class, "No new languages found...");
				PeaDialog.showMessage(JREProperties.getMainWindow(), PeaProperties.getVmBridge().translate("no_new_language"), 
						PeaProperties.getVmBridge().translate("error"), 0);
				return false;
			}
		} else {
			TestLog.ve(SettingMenu.class,"No language in web available...");
			PeaDialog.showMessage(JREProperties.getMainWindow(), PeaProperties.getVmBridge().translate("no_new_language"),
					PeaProperties.getVmBridge().translate("error"), 0);
			return false;
		}					
		//return true;
	}
	
	/**
	 * Download language files and set language if 
	 * download was successful. 
	 * 
	 * @param language	the language to download
	 * 
	 * @return	true for download success
	 */
	public static boolean downloadLanguageFiles(String language) {
		// Download file in folder resources
		try {
			// before peafactory 0.1.4.4:
			//String urlFolder = "https://eck.cologne/peafactory/src/lang/" + langs[langIndex];//TODO

			// from peafactory.0.1.4.4
			String urlFolder = "https://eck.cologne/peafactory/src/language/" + language;//TODO
			String urlFile =   "PeaLanguagesBundle_" + language + ".properties";
			String localFolder = System.getProperty("user.dir") + File.separator + "resources" + File.separator + language + File.separator;
			long byteNumber = WebTools.downloadFile(urlFolder + "/" + urlFile, localFolder + File.separator + urlFile);
			//System.getProperty("user.dir") + File.separator + "resources" + File.separator + "PeaLanguagesBundle_" + langs[langIndex] + ".properties");
			if (byteNumber == 0) {
				TestLog.e(SettingMenu.class, "Download failed");
				TestLog.ve(SettingMenu.class, "Download failed", 5);
				PeaDialog.showMessage(JREProperties.getMainWindow(), 
						PeaProperties.getVmBridge().translate("download_failed")
						+ "\n" + urlFile, 
						PeaProperties.getVmBridge().translate("error"), 0);
				return false;
			}
			if ( ! PeaProperties.getFileType().equals("file")) { // all PEAs except File Lock PEA
				// Specific language bundles:
				String bundleFirstPart = "";
				if (PeaProperties.getFileType().equals("calendar")) { 
					bundleFirstPart = "Calendar";
				} else if (PeaProperties.getFileType().equals("contact")) { 
					bundleFirstPart = "Contact";
				} else if (PeaProperties.getFileType().equals("image")) { 
					bundleFirstPart = "Image";
				} else if (PeaProperties.getFileType().equals("text file")) { 
					bundleFirstPart = "Note";
				} else {
					TestLog.ve(SettingMenu.class, "Unknown PEA type for language bundles: " + PeaProperties.getFileType(), 5);
					return false;
				}
				//if (PeaProperties.getFileType().equals("calendar")) { 
					urlFile =   bundleFirstPart + "LanguageBundle_" + language + ".properties";
					localFolder = System.getProperty("user.dir") + File.separator + "resources" + File.separator + language + File.separator;
					byteNumber = WebTools.downloadFile(urlFolder + "/" + urlFile, localFolder + File.separator + urlFile);
					//System.getProperty("user.dir") + File.separator + "resources" + File.separator + "PeaLanguagesBundle_" + langs[langIndex] + ".properties");
					if (byteNumber == 0) {
						TestLog.e(SettingMenu.class, "Download failed");
						TestLog.ve(SettingMenu.class, "Download failed", 5);
						PeaDialog.showMessage(JREProperties.getMainWindow(), 
								PeaProperties.getVmBridge().translate("download_failed")
								+ "\n" + urlFile, 
								PeaProperties.getVmBridge().translate("error"), 0);
						//return;
					}
				//}
			}
			urlFile =   "first_time_guide_" + language + ".html";
			byteNumber = WebTools.downloadFile(urlFolder + "/" + urlFile, localFolder + File.separator + urlFile);
			//System.getProperty("user.dir") + File.separator + "resources" + File.separator + "PeaLanguagesBundle_" + langs[langIndex] + ".properties");
			if (byteNumber == 0) {
				TestLog.e(SettingMenu.class, "Download failed");
				TestLog.ve(SettingMenu.class, "Download failed", 5);
				PeaDialog.showMessage(JREProperties.getMainWindow(), 
						PeaProperties.getVmBridge().translate("download_failed")
						+ "\n" + urlFile, 
						PeaProperties.getVmBridge().translate("error"), 0);
				//return;
			}
			urlFile =   "cloud_guide_" + language + ".html";
			byteNumber = WebTools.downloadFile(urlFolder + "/" + urlFile, localFolder + File.separator + urlFile);
			//System.getProperty("user.dir") + File.separator + "resources" + File.separator + "PeaLanguagesBundle_" + langs[langIndex] + ".properties");
			if (byteNumber == 0) {
				TestLog.e(SettingMenu.class, "Download failed");
				TestLog.ve(SettingMenu.class, "Download failed", 5);
				PeaDialog.showMessage(JREProperties.getMainWindow(), 
						PeaProperties.getVmBridge().translate("download_failed")
						+ "\n" + urlFile, 
						PeaProperties.getVmBridge().translate("error"), 0);
				//return;
			}
		} catch (Exception e) {
			TestLog.e(SettingMenu.class, e.getLocalizedMessage());
			TestLog.ve(SettingMenu.class, e.getLocalizedMessage() + " - " + e.toString(), 5);
			PeaDialog.showMessage(JREProperties.getMainWindow(), 
					language + ": " + PeaProperties.getVmBridge().translate("download_failed") + "\n" 
					+ e.getLocalizedMessage(), 
					PeaProperties.getVmBridge().translate("error"), 0);
			return false;
		}
		// update setting menu 
		try {
			PeaProperties.getVmBridge().setLanguagesBundle(language);

			if (JREProperties.getMainWindow() instanceof PswDialogView) {
				PswDialogView.resetLanguage();
			} else if (JREProperties.getMainWindow() instanceof PeaLockFrame) {
				((PeaLockFrame) JREProperties.getMainWindow()).resetFrame();
			}
		} catch (Exception e) {
			TestLog.e(SettingMenu.class, e.getLocalizedMessage());
			TestLog.ve(SettingMenu.class, e.getLocalizedMessage() + " - " + e.toString(), 5);
			PeaDialog.showMessage(JREProperties.getMainWindow(), e.getLocalizedMessage() + "\n"
					+ PeaProperties.getVmBridge().translate("language") + ": " + language, 
					PeaProperties.getVmBridge().translate("error"), 0);
		}
		PeaProperties.setLastLanguage(language);
		new PropertyHandler().setOneProperty("language", language);
		return true;
	}

	public void setVisibility() {
		if (JREProperties.getMainWindow() instanceof PeaLockFrame) { // only for PeaLockFrame
			if (PeaProperties.isKeyFileOnlyMode() == true
					&& KeyDerivation.getKdf() instanceof SingleIterationPBKDF2) {
				keyFileItem.setVisible(false);
				changePswItem.setVisible(false);
				directStartItem.setVisible(true);
				boolean directStart = PeaProperties.isDirectStart();
				if (directStart == true) {
					directStartItem.setText(PeaProperties.getVmBridge().translate("disable_direct_start"));
				} else {
					directStartItem.setText(PeaProperties.getVmBridge().translate("enable_direct_start"));
				}
			} else {
				keyFileItem.setVisible(true);
				changePswItem.setVisible(true);
				directStartItem.setVisible(false);
			}
		}
	}
	
	/**
	 * Set a key file status except a new default key file.
	 * 
	 * @param defaultProperties	true: set default key file flag
	 */
	private void setNonDefaultKeyFile(boolean defaultProperties) {
		// choose a key file
		String[] options = { 			
				PeaProperties.getVmBridge().translate("no_key_file"),
				PeaProperties.getVmBridge().translate("gen_key_file_manually_opened"), 
				PeaProperties.getVmBridge().translate("choose_key_file_manually_opened")//,
				//PeaProperties.getVmBridge().translate("choose_key_file_automatically_opened")
		};
		int keyFileType = PeaDialog.showOptionYAxis(JREProperties.getMainWindow(), 
				PeaProperties.getVmBridge().translate("choose_key_file_kind")+ "\n",
				//	+ PeaProperties.getVmBridge().translate("default_key_file_info"), 
				PeaProperties.getVmBridge().translate("key_file"), 3, options, 0);
		TestLog.v(SettingMenu.class, "Option: " + keyFileType);		
		File keyFile = null;
		if (keyFileType == 0) { // no key file
			if (defaultProperties == true) {
				PeaProperties.setDefaultKeyFileFlag(0);
			} else {
				PeaProperties.setKeyFileFlag(0);
			}
			apply();
			return;

		} else if (keyFileType == 2 || keyFileType == 3) { // manually choose

			keyFile = KeyFileDialogs.manuallyChooseNewKeyFile();
			if (keyFile == null) {
				return;
			} 
		} else if (keyFileType == 1) { // create
			keyFile = KeyFileDialogs.generateKeyFile(null);
			if (keyFile == null) {
				return;
			} 
		} else {
			return;
		}
		if (defaultProperties == true) {
			PeaProperties.setDefaultKeyFileFlag(2);

		} else {
			PeaProperties.setKeyFileFlag(2);

		}
		PeaDialog.showMessage(JREProperties.getMainWindow(), 					
				PeaProperties.getVmBridge().translate("select_file_when_open_files") + ": \n\n" 
						+ keyFile.getAbsolutePath()  + "\n\n"
						+ PeaProperties.getVmBridge().translate("never_modify_or_delete") ); 
		apply();
		return;
	}
}
