package cologne.eck.all_peas.gui.menu;

/*
 * PeaFactory - Production of Password Encryption Archives
 * Copyright (C) 2015  Axel von dem Bruch
 * 
 * This library is free software; you can redistribute it and/or modify it 
 * under the terms of the GNU General Public License as published 
 * by the Free Software Foundation; either version 2 of the License, 
 * or (at your option) any later version.
 * This library is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
 * or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU General Public License for more details.
 * See:  http://www.gnu.org/licenses/gpl-2.0.html
 * You should have received a copy of the GNU General Public License 
 * along with this library.
 */

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JColorChooser;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.plaf.FontUIResource;

import cologne.eck.all_peas.data.PeaProperties;
import cologne.eck.all_peas.files.FileComposer;
import cologne.eck.all_peas.files.FilePanelHolder;
import cologne.eck.all_peas.files.FileTypePanel;
import cologne.eck.all_peas.gui.PswDialogView;
import cologne.eck.tools.TestLog;
import cologne.eck.tools.UnexpectedValueException;



@SuppressWarnings("serial")
public class ViewMenu extends JMenu implements ActionListener {
	
	private FileTypePanel ftp;
	private FilePanelHolder fph;

	/**
	 * Menu to set the view (font size, color...)
	 * 
	 * @param _fph	the FilePanelHolder - must be present if plainModus is false
	 * @param _ftp	the FileTypePanel - must be present if plainModus is true
	 * @param menmo	character to use as mnemonic
	 * @param plainModus	true: files are plaintext, false: encrypted
	 */
	public ViewMenu(FilePanelHolder _fph, FileTypePanel _ftp, char mnemo,
			boolean plainModus) {
		
		Font font = new Font(Font.SANS_SERIF, Font.PLAIN, PeaProperties.getFontSize());
		//PswDialogView pdvX = PswDialogView.getInstance();
		
		if (plainModus == false && _fph == null) {
			new UnexpectedValueException("FilePanelHolder", "_fph","is null").printDescription();
		}
		if (plainModus == true && _ftp == null){
			new UnexpectedValueException("FileTypePanel", "_ftp","is null").printDescription();
		}
		this.ftp = _ftp;
		this.fph = _fph;
		this.setFont(font);
		this.setText(PeaProperties.getVmBridge().translate("view"));
		this.setMnemonic(mnemo);//PswDialogView.getInstance().getMnemo(this, PeaProperties.getVmBridge().translate("view")));
		
		JMenuItem treeViewItem = new JMenuItem(PeaProperties.getVmBridge().translate("tree_view"));
		treeViewItem.setActionCommand("treeView");
		treeViewItem.setFont(font);
		treeViewItem.setToolTipText(PeaProperties.getVmBridge().translate("tooltip_tree_view"));
		//treeViewItem.setMnemonic(pdv.getMnemo(this, PeaProperties.getVmBridge().translate("tree_view")));
		treeViewItem.addActionListener(this);
		this.add(treeViewItem);
		
		JMenuItem fullNameItem = new JMenuItem(PeaProperties.getVmBridge().translate("full_name"));
		fullNameItem.getAccessibleContext().
	    setAccessibleDescription(
	    	    "Clicking this component files with the full path.");
		fullNameItem.setActionCommand("fullName");
		fullNameItem.setFont(font);
		//fullNameItem.setMnemonic(pdv.getMnemo(this, PeaProperties.getVmBridge().translate("full_name")));
		fullNameItem.addActionListener(this);
		this.add(fullNameItem);

		if (plainModus == false){
			JMenuItem checkAgainItem = new JMenuItem(PeaProperties.getVmBridge().translate("refresh"));
			checkAgainItem.setActionCommand("refresh");
			checkAgainItem.setToolTipText(PeaProperties.getVmBridge().translate("tooltip_refresh"));
			checkAgainItem.setFont(font);
			//checkAgainItem.setMnemonic(pdv.getMnemo(this, PeaProperties.getVmBridge().translate("refresh")));
			checkAgainItem.addActionListener(this);
			this.add(checkAgainItem);		
		}

		JMenuItem cleanupItem = new JMenuItem(PeaProperties.getVmBridge().translate("cleanup"));
		cleanupItem.setActionCommand("cleanup");
		cleanupItem.setFont(font);
		cleanupItem.setToolTipText(PeaProperties.getVmBridge().translate("tooltip_cleanup"));
		//cleanupItem.setMnemonic(pdv.getMnemo(this, PeaProperties.getVmBridge().translate("cleanup")));		
		cleanupItem.addActionListener(this);
		this.add(cleanupItem);
		
		JMenuItem increaseFontSizeItem = new JMenuItem(PeaProperties.getVmBridge().translate("enlarge_font"));//PeaProperties.getVmBridge().translate("cleanup"));
		increaseFontSizeItem.setActionCommand("increaseFontSize");
		increaseFontSizeItem.getAccessibleContext().
	    setAccessibleDescription(
	    	    "Clicking this component increases the font size for this program.");
		increaseFontSizeItem.setFont(font);
		//increaseFontSizeItem.setMnemonic(pdv.getMnemo(this, PeaProperties.getVmBridge().translate("enlarge_font")));		
		increaseFontSizeItem.addActionListener(this);
		this.add(increaseFontSizeItem);
		
		JMenuItem decreaseFontSizeItem = new JMenuItem(PeaProperties.getVmBridge().translate("reduce_font"));//PeaProperties.getVmBridge().translate("cleanup"));
		decreaseFontSizeItem.setActionCommand("decreaseFontSize");
		decreaseFontSizeItem.getAccessibleContext().
	    setAccessibleDescription(
	    	    "Clicking this component decreases the font size for this program.");
		decreaseFontSizeItem.setFont(font);
		//decreaseFontSizeItem.setMnemonic(pdv.getMnemo(this, PeaProperties.getVmBridge().translate("reduce_font")));		
		decreaseFontSizeItem.addActionListener(this);
		this.add(decreaseFontSizeItem);
		
		JMenuItem colorItem = new JMenuItem(PeaProperties.getVmBridge().translate("background_color"));//PeaProperties.getVmBridge().translate("cleanup"));
		colorItem.setActionCommand("setColor");
		colorItem.getAccessibleContext().
	    setAccessibleDescription(
	    	    "Clicking this component opens a color chooser to set the background color for this program.");
		colorItem.setFont(font);
		//colorItem.setMnemonic(pdv.getMnemo(this, PeaProperties.getVmBridge().translate("background_color")));		
		colorItem.addActionListener(this);
		this.add(colorItem);
	}
	
	@Override
	public void actionPerformed(ActionEvent ae) {
		String command = ae.getActionCommand();
		FileComposer fc = null;
		if (ftp != null) {
			fc = ftp.getFileComposer();
		}		
		if (command.equals("increaseFontSize")) {			
			// change font size
			int oldFontSize = PeaProperties.getFontSize();
			int newFontSize = oldFontSize;
			if (oldFontSize < 18) {
				newFontSize = oldFontSize + 2;
				PeaProperties.setFontSize( newFontSize);
			} 		    
			// set font size (UIManager)
			FontUIResource f = new FontUIResource(Font.SANS_SERIF, Font.PLAIN, newFontSize);			
			java.util.Enumeration<Object> keys = UIManager.getDefaults().keys();
		    while (keys.hasMoreElements())  {
		        Object key = keys.nextElement();
		        Object value = UIManager.get(key);
		        if (value instanceof javax.swing.plaf.FontUIResource) {
		            UIManager.put(key, f);
		        }
		    }

		    UIManager.getLookAndFeelDefaults()
	        .put("defaultFont", new Font(Font.SANS_SERIF, Font.PLAIN, newFontSize));

			if (SwingUtilities.getWindowAncestor(this) instanceof JFrame){ // LockFrame: plain text mode
				PswDialogView.setUI(false);
			} else if (SwingUtilities.getWindowAncestor(this) instanceof JDialog){ // Password dialog: encrypted mode
				PswDialogView.setUI(true);
			} else {
				new UnexpectedValueException("window ancgestor", "Window",
						"is not JDialog or JFrame: " + SwingUtilities.getWindowAncestor(this).toString()).printDescription();
			}
			Window win = SwingUtilities.windowForComponent(this);
			SwingUtilities.updateComponentTreeUI(win);
			win.pack();
			win.setVisible(true);
		    		    
		} else if (command.equals("decreaseFontSize")) {
			
			// change font size
			int oldFontSize = PeaProperties.getFontSize();
			int newFontSize = oldFontSize;
			if (oldFontSize > 10) {
				newFontSize = oldFontSize - 2;
			PeaProperties.setFontSize( newFontSize);
			} 
			
			// set font size (UIManager)
			FontUIResource f = new FontUIResource(Font.SANS_SERIF, Font.PLAIN, newFontSize);			
			java.util.Enumeration<Object> keys = UIManager.getDefaults().keys();
		    while (keys.hasMoreElements())  {
		        Object key = keys.nextElement();
		        Object value = UIManager.get(key);
		        if (value instanceof javax.swing.plaf.FontUIResource) {
		            UIManager.put(key, f);
		        }
		    }  
		    // resizes currently shown items, labels...
		    UIManager.getLookAndFeelDefaults()
	        .put("defaultFont", new Font(Font.SANS_SERIF, Font.PLAIN, newFontSize));
		    
		    // update (current) view
		    SwingUtilities.updateComponentTreeUI(PswDialogView.getView());		        
			if (SwingUtilities.getWindowAncestor(this) instanceof JFrame){ // LockFrame: plain text mode
				PswDialogView.setUI(false);
			} else if (SwingUtilities.getWindowAncestor(this) instanceof JDialog){ // Password dialog: encrypted mode
				PswDialogView.setUI(true);
			} else {
				new UnexpectedValueException("window ancgestor", "Window",
						"is not JDialog or JFrame: " + SwingUtilities.getWindowAncestor(this).toString()).printDescription();
			}
			Window win = SwingUtilities.windowForComponent(this);
			SwingUtilities.updateComponentTreeUI(win);
			win.pack();
			win.setVisible(true);
		    
		} else if (command.equals("treeView")){
			if (fph == null && ftp != null) {
				// add zip buttons as empty strings:
				String[] newTexts = fc.showFileHierarchy(ftp.getValidLocalFileNames(false, false));
				if (newTexts == null) {
					return;
				}
				// set the new texts as check box texts
				ftp.updateCheckBoxTexts(newTexts);
				ftp.updateWindow();
			} else if (fph != null) {
				ArrayList<FileTypePanel> ftpList = FilePanelHolder.getFileTypePanelList();
				for (FileTypePanel f : ftpList) {
					FileComposer fcs = f.getFileComposer();
					// add zip buttons as empty strings:
					String[] newTexts = fcs.showFileHierarchy(f.getValidLocalFileNames(false, false));
					if (newTexts == null) {
						return;
					}
					// set the new texts as check box texts
					f.updateCheckBoxTexts(newTexts);
					f.updateWindow();
				}
			}

		} else if (command.equals("fullName")){
			if (fph == null && ftp != null) {
				ftp.showFullFileNames();
				ftp.updateWindow();
			} else if (fph != null) {
				fph.showFullFileNames();
				FilePanelHolder.updateWindow();
			}

		} else if (command.equals("refresh")){
			if (fph == null && ftp != null) {
				fc.updateFiles(true);
				ftp.getFileComposer().getFileModel().calculateNumberAndSize(ftp.getValidLocalFileNames(true, false));
				ftp.updateNumberAndSize();
				ftp.updateWindow();
			} else if (fph != null) {
				ArrayList<FileTypePanel> ftpList = FilePanelHolder.getFileTypePanelList();
				for (FileTypePanel f : ftpList) {
					FileComposer fcs = f.getFileComposer();
					fcs.updateFiles(true);
					f.getFileComposer().getFileModel().calculateNumberAndSize(f.getValidLocalFileNames(true, false));
					f.updateNumberAndSize();
				}
				FilePanelHolder.updateWindow();
			}

		} else if (command.equals("cleanup")) {
			if (ftp != null && ftp.getFileComposer().isPlainModus() == true) {
				String[] unselectedFiles = ftp.getUnselectedValidFileNames();
				if (unselectedFiles != null) {
					StringBuilder unselectedFilesBuilder = new StringBuilder();
					for (int i = 0; i < unselectedFiles.length; i++) {
						unselectedFilesBuilder.append(unselectedFiles[i]);
						unselectedFilesBuilder.append("\n");
					}
					String unselectedFilesString = new String(unselectedFilesBuilder);
					// display a warning and an option to break
					int result = JOptionPane.showConfirmDialog(ftp,
							PeaProperties.getVmBridge().translate("leave_files_unencrypted")
							+ "\n" + unselectedFilesString,
							PeaProperties.getVmBridge().translate("warning"),
							JOptionPane.WARNING_MESSAGE,
							JOptionPane.YES_NO_OPTION);
					if (result != 0) {	//yes = 0, 
						return;
					} 			
				}
			}
			if (fph == null && ftp != null) {
			ftp.cleanupView();
			ftp.getFileComposer().getFileModel().cleanupMap();
			ftp.getFileComposer().getFileModel().calculateNumberAndSize(ftp.getValidLocalFileNames(true, false));
			ftp.updateNumberAndSize();
			ftp.updateWindow();
			} else if (fph != null) {
				ArrayList<FileTypePanel> ftpList = FilePanelHolder.getFileTypePanelList();
				for (FileTypePanel f : ftpList) {
					//FileComposer fc = f.getFileComposer();
					f.cleanupView();
					f.getFileComposer().getFileModel().cleanupMap();
					f.getFileComposer().getFileModel().calculateNumberAndSize(f.getValidLocalFileNames(true, false));
					f.updateNumberAndSize();
					f.updateWindow();
				}
			}
			
		} else if (command.equals("setColor")) {

			int[] colorVals = PeaProperties.getPeaColor();
			Color peaCol = new Color(colorVals[0], colorVals[1], colorVals[2]);
			Color color = JColorChooser.showDialog(this,PeaProperties.getVmBridge().translate("background_color"), peaCol);
			if (color == null) {
				return;
			}
			// check darkness:
			int darkness = ((30 * color.getRed() +
		            59 * color.getGreen() +
		            11 * color.getBlue()) / 100);
			while (darkness < 210) { // make brighter
				color = color.brighter();
				int newDarkness = ((30 * color.getRed() +
		            59 * color.getGreen() +
		            11 * color.getBlue()) / 100);
				if (newDarkness == darkness) { // this occurs if colors are 0
					int r = color.getRed();
					int g = color.getGreen();
					int b = color.getBlue();
					if ( r < 50) {
						r = r + 50;
					}
					if (g < 50) {
						g = g + 50;
					}
					if (b < 50) {
						b = b + 50;
					}
					color = new Color(r,g,b);
					darkness = ((30 * color.getRed() +
				            59 * color.getGreen() +
				            11 * color.getBlue()) / 100);
				} else {
					darkness = newDarkness;
				}
			}
			Window win = SwingUtilities.windowForComponent(this);
			Dimension dim = win.getSize();

			int[] newCol = {color.getRed(), color.getGreen(), color.getBlue()};
			PeaProperties.setPeaColor(newCol);
			if (SwingUtilities.getWindowAncestor(this) instanceof JFrame){ // LockFrame: plain text mode
				PswDialogView.setUI(false);
			} else if (SwingUtilities.getWindowAncestor(this) instanceof JDialog){ // Password dialog: encrypted mode
				PswDialogView.setUI(true);
			} else {
				new UnexpectedValueException("window anchestor", "Window",
						"is not JDialog or JFrame: " + SwingUtilities.getWindowAncestor(this).toString()).printDescription();
			}
			SwingUtilities.updateComponentTreeUI(win);
			PswDialogView.getView().validate();
			PswDialogView.getView().repaint();
			//win.pack();
			win.setVisible(true);
			win.setSize(dim);
			
		} else {
			TestLog.o(ViewMenu.class, "Invalid command in help menu");
		}
	}
}
