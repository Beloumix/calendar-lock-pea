/**
 * Classes used by all PEAs: Calendar Lock PEA, 
 * File Lock PEA, Notebook PEA, Image Lock PEA,
 * not for PEA creation
 */
/**
 * @author axl
 *
 */
package cologne.eck.all_peas;