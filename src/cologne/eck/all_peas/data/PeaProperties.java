package cologne.eck.all_peas.data;

//////////////////////////////////////////////////////
// Change in Android
//import java.awt.Color;
//import java.awt.Dimension;
//import java.awt.Point;
//import java.util.ResourceBundle;

import cologne.eck.all_peas.bridge.VMBridge;
import cologne.eck.peafactory.crypto.AlgoParamHandler;
import cologne.eck.peafactory.crypto.AuthenticatedEncryption;
import cologne.eck.peafactory.crypto.EAXMode;
import cologne.eck.peafactory.crypto.kdf.CatenaKDF;
import cologne.eck.peafactory.crypto.kdf.KeyDerivation;
import cologne.eck.tools.TestLog;
import cologne.eck.tools.UnexpectedValueException;

import java.nio.charset.Charset;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.Locale;
import java.util.Random;

import org.bouncycastle.crypto.BlockCipher;
import org.bouncycastle.crypto.Digest;
import org.bouncycastle.crypto.digests.*;
import org.bouncycastle.crypto.engines.*;



/*
 * PeaFactory - Production of Password Encryption Archives
 * Copyright (C) 2015  Axel von dem Bruch
 * 
 * This library is free software; you can redistribute it and/or modify it 
 * under the terms of the GNU General Public License as published 
 * by the Free Software Foundation; either version 2 of the License, 
 * or (at your option) any later version.
 * This library is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
 * or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU General Public License for more details.
 * See:  http://www.gnu.org/licenses/gpl-2.0.html
 * You should have received a copy of the GNU General Public License 
 * along with this library.
 */

public class PeaProperties {
	
	private final static int PEA_VERSION = 2;// old version: 1

	/**
	 * The type of the PEA.
	 * Possible values are: 
	 * "image", "file", "text file" (Notebook PEA), "text" (simple Editor), "calendar", "contact"
	 */
	private static String fileType; // set in daughter class
	
	private static String versionInfo;
	
	private static boolean commandLineMode = false;
	
	
	private static String workingMode = ""; // rescue mode or test mode
	//private static boolean workingModeWasSet = false;// for short way instead of String comparison
//	private static boolean testMode = false;
	
	// remember decision: 
	private static boolean overwriteExistingFiles = false;
	
	
	// check automatically updates: 0 = not yet decided, -1 = do not check, 1 = check
	private static int checkUpdates = 0;
	
	// Settings for all PEAs:
	/**
	 * The used font size in menus and dialogs
	 */
	private static int fontSize = 12;
	
	
	////////////////////////////////////////
	/// Change in Android
	/**
	 * background colors
	 */
	//private static Color peaColor = new Color(218, 242, 220);//Farbton 125,Saettigung 10, Wert 95
	private static int[] peaColor = {237, 255, 242}; // +7, +5, +7 since peafactory-0.4-beta
	// #edfff2	rgb(237, 255, 242)	hsl(137, 100%, 96%)

	private static String lastLanguage = null;
	
	private static boolean installLanguageOptionShown = false;
	
	private static Locale locale = Locale.getDefault();

	
	private static VMBridge vmBridge;
	
	/**
	 * Dropbox access token for authorization
	 */
	private static String dropboxAccessToken;
	/**
    api - main Dropbox API server host name
	 */
	//private static String dropboxApi;
	/**
    content - Dropbox API content server host name
	 */
	//private static String dropboxContent;
	/**
    web - Dropbox web server host name
	 */
	//private static String dropboxWeb;
	/**
    notify - Dropbox notification server host name
	 */
	//private static String dropboxNotify;
	
	/**
	 * The default character set, used whenever a string
	 * is converted to an array of bytes
	 */
	private static final Charset UTF_8 = Charset.forName("UTF-8");
	
	/**
	 * Last used salt (stored in pea.properties)
	 */
	private static byte[] lastSalt;
	
	/**
	 * Used algorithms and parameters
	 */
	private static byte[] algoParamString;
	
	private static int keyFileFlag = 0; // 0 no key file, 1 default key file, 2 user chosen key file


	/**
	 * Default algorithms and parameters: Used for new initialized files only
	 */
	private static String defaultAlgoParamString = AlgoParamHandler.getV1String();

	private static int defaultKeyFileFlag = 0; // 0 no key file, 1 default key file, 2 user chosen key file
	
	private static KeyDerivation defaultKDF = new CatenaKDF();
	
	private static String defaultKDFInstanceAppendix = "Dragonfly-Full";
	
	private static int defaultMCost = 18;
	
	private static int defaultTCost = 2;
	
	private static int defaultParallel = 0;
	
	private static BlockCipher defaultCipher = new ThreefishEngine(512);
	
	private static AuthenticatedEncryption defaultMode = new EAXMode();
	
	private static Digest defaultDigest = new Blake2bDigest();
	
	private static String defaultKeyFileName;
	
	private static final Random RANDOM = new Random();
	
	private static final SecureRandom SECURE_RANDOM = new SecureRandom();
	
	// Keyfile only, no password: one iteration PBKDF" instead of a key derivation function
	private static boolean keyFileOnlyMode = false;
	// Start directly without showing files in password dialog
	private static boolean directStart = false;
	// Files that are used before closing - to start automatically next on next start
	private static ArrayList<String> lastUsedFiles = null;
	
	
	/**
	 * Use isTestMode or isRescueMode instead!!
	 * The working mode of the PEA: 
	 * -t test mode
	 * -r rescue mode
	 * 
	 * @return the workingMode
	 */
/*	public static String getWorkingMode() {
		return workingMode;
	}*/
	/**
	 * Check if PEA runs in test mode
	 * 
	 * @return	true if PEA runs in test mode
	 */
	public static boolean isTestMode() {

		if (workingMode.equals("-t")) {
			return true;
		} else {
			return false;
		}
	}
	/**
	 * Check if PEA runs in rescue mode
	 * 
	 * @return	true if PEA runs in rescue mode
	 */
	public static boolean isRescueMode() {
		return workingMode.equals("-r");
	}
	/**
	 * Set the working mode: 
	 * -t for test mode
	 * -r for rescue mode
	 * 
	 * @param workingMode the workingMode to set
	 */
	public static void setWorkingMode(String workingMode) {
		TestLog.o(PeaProperties.class, "Run in mode: " + workingMode);
		PeaProperties.workingMode = workingMode;
	}
	/**
	 * The character set used in all classes
	 * 
	 * @return the Charset UTF-8
	 */
	public static Charset getCharset() {
		return UTF_8;
	}

	/**
	 * Set a String, that indicates the type of the PEA.
	 * Possible values are: 
	 * "image", "file", "text file" (Notebook PEA), "text" (simple Editor),
	 * "calendar", "contact"
	 * 
	 * @param _fileType
	 */
	public final static void setFileType(String _fileType) {
		fileType = _fileType;
	}
	/**
	 * Get a String, that indicates the type of the PEA
	 * 
	 * @return	a String indicating the type of the PEA. 
	 * 			Possible values are: "image", "file", "text file" (Notebook PEA), "text" (simple Editor),
	 * 			"calendar", "contact"
	 */
	public final static String getFileType() {
		return fileType;
	}

	public static boolean isOverwriteExistingFiles() {
		return overwriteExistingFiles;
	}
	public static void setOverwriteExistingFiles(boolean overwriteExistingFiles) {
		//TestLog.v(PeaProperties.class, "XXXXX "+ overwriteExistingFiles, 5);
		PeaProperties.overwriteExistingFiles = overwriteExistingFiles;
	}
	/**
	 * Check decision to check on start for available updates of this PEA
	 * 
	 * @return	0: no decision; 1: check, -1: do not check
	 */
	public static int getCheckUpdates() {
		return checkUpdates;
	}
	/**
	 * Check on start for available updates of this PEA
	 * 
	 * @param checkUpdates	1: check automatically for updates, -1: do not check,0: no decision
	 */
	public static void setCheckUpdates(int checkUpdates) {
		PeaProperties.checkUpdates = checkUpdates;
	}
	public static int getFontSize() {
		return fontSize;
	}
	public static void setFontSize(int newFontSize) {
		PeaProperties.fontSize = newFontSize;
	}
	


	/**
	 * Return the background color for frames 
	 * containing plain content (brighter than pea color)
	 * as int array red, green, blue
	 * 
	 * @return	color for frames containing plain content 
	 * 			as int[] r,g,b
	 */
	public static int[] getPlainPeaColor() { 
		int r = peaColor[0] + 15;
		if (r > 255) {
			r = 255;
		}
		int g = peaColor[1] + 15;
		if (g > 255) {
			g = 255;
		}
		int b =  peaColor[2] + 15;
		if (b > 255) {
			b = 255;
		}
		int[] color = {r,g,b};
		return color;//Color(r, g, b);//peaColor.brighter();
	}
	/**
	 * Set color of PEA as int array r,g,b
	 * 
	 * @param rgbValues	int[] r,g,b
	 */
	public static void setPeaColor(int[] rgbValues) {
		if (rgbValues.length != 3) {
			new UnexpectedValueException("rgbValue", "int[]", "is invalid").printDescription();
		}
		PeaProperties.peaColor = rgbValues;
	}
	/**
	 * Get Color of PEA: value of red, green, blue
	 * 
	 * @return	use as: new Color(r,g,b)
	 */
	public static int[] getPeaColor() {
		return peaColor;
	}
	/**
	 * Return the background color for frames 
	 * containing encrypted content (darker than pea color)
	 * as int array red, green, blue
	 * 
	 * @return	background color for frames containing encrypted content
	 * 			as int[] r,g,b
	 */
	public static int[] getCipherPeaColor() {
		int r = peaColor[0] - 10;// before PeaFactory-0.4 + 15
		if (r < 0) {
			r = 0;
		}
		int g = peaColor[1] - 10;// before PeaFactory-0.4 + 15
		if (g < 0) {
			g = 0;
		}
		int b =  peaColor[2] - 10;// before PeaFactory-0.4 + 15
		if (b < 0) {
			b = 0;
		}
		int[] color = {r,g,b};
		return color;//Color(r, g, b);//peaColor.brighter();

		//return peaColor.darker();
	}
	public static String getDropboxAccessToken() {
		return dropboxAccessToken;
	}
	public static void setDropboxAccessToken(String accessToken) {
		PeaProperties.dropboxAccessToken = accessToken;
	}

	/**
	 * Get the last used salt as stored in property file
	 * 
	 * @return the salt as an array of bytes
	 */
	public static byte[] getLastSalt() {
		return lastSalt;
	}
	/**
	 * Set the last used salt as stored in property file
	 * 
	 * @param _lastSalt	the salt as an array of bytes
	 */
	public static void setLastSalt(byte[] _lastSalt) {
		PeaProperties.lastSalt = _lastSalt;
	}
	/**
	 * Get the class that implements VM depending methods
	 * 
	 * @return the vmBridge
	 */
	public static VMBridge getVmBridge() {
		return vmBridge;
	}
	/**
	 * Set the class that implements VM depending methods
	 * 
	 * @param _vmBridge the vmBridge to set
	 */
	public static void setVmBridge(VMBridge _vmBridge) {
		PeaProperties.vmBridge = _vmBridge;
	}
	/**
	 * @return the keyFileFlag  (0 no key file, 1 default key file, 2 user chosen key file)
	 */
	public static int getKeyFileFlag() {
		return keyFileFlag;
	}
	/**
	 * @param keyFileFlag the keyFileFlag to set
	 *  (0 no key file, 1 default key file, 2 user chosen key file)
	 */
	public static void setKeyFileFlag(int _keyFileFlag) {
		if (_keyFileFlag < 0 || _keyFileFlag > 2) {
			new UnexpectedValueException("int", "_keyFileFlag", "is invalid: " + _keyFileFlag).printDescription();
		} else {
			PeaProperties.keyFileFlag = _keyFileFlag;
		}
	}
	/**
	 * @return the kdf
	 */
	public static KeyDerivation getDefaultKDF() {
		return defaultKDF;
	}
	/**
	 * @param kdf the kdf to set
	 */
	public static void setDefaultKDF(KeyDerivation kdf) {
		if (kdf == null) {
			new UnexpectedValueException("KeyDerivation", "kdf", "is null").printDescription();
		}
		PeaProperties.defaultKDF = kdf;
	}
	/**
	 * Get the String for the special instance if there is one.
	 * "i", "id", "d" for Argon2, 
	 * "Dragonfly", "Dragonfly-Full", "Butterfly", "Butterfly-Full" for Catena, 
	 * null for Pomelo, Scrypt, Bcrypt
	 * 
	 * @return the defaultKDFInstanceAppendix
	 */
	public static String getDefaultKDFInstanceAppendix() {
		return defaultKDFInstanceAppendix;
	}
	/**
	 * Set the String for the special instance if there is one.
	 * "i", "id", "d" for Argon2, 
	 * "Dragonfly", "Dragonfly-Full", "Butterfly", "Butterfly-Full" for Catena, 
	 * null for Pomelo, Scrypt, Bcrypt
	 * 
	 * @param defaultKDFInstanceAppendix the defaultKDFInstanceAppendix to set
	 */
	public static void setDefaultKDFInstanceAppendix(String defaultKDFInstance) {
		PeaProperties.defaultKDFInstanceAppendix = defaultKDFInstance;
	}
	/**
	 * @return the cipher
	 */
	public static BlockCipher getDefaultCipher() {
		return defaultCipher;
	}
	/**
	 * @param cipher the cipher to set
	 */
	public static void setDefaultCipher(BlockCipher cipher) {
		PeaProperties.defaultCipher = cipher;
	}
	/**
	 * @return the defaultMode
	 */
	public static AuthenticatedEncryption getDefaultMode() {
		return defaultMode;
	}
	/**
	 * @param defaultMode the defaultMode to set
	 */
	public static void setDefaultMode(AuthenticatedEncryption defaultMode) {
		PeaProperties.defaultMode = defaultMode;
	}
	/**
	 * @return the defaultDigest
	 */
	public static Digest getDefaultDigest() {
		return defaultDigest;
	}
	/**
	 * @param defaultDigest the defaultDigest to set
	 */
	public static void setDefaultDigest(Digest defaultDigest) {
		PeaProperties.defaultDigest = defaultDigest;
	}
	/**
	 * @return the defaultMCost
	 */
	public static int getDefaultMCost() {
		return defaultMCost;
	}
	/**
	 * @param defaultMCost the defaultMCost to set
	 */
	public static void setDefaultMCost(int defaultMCost) {
		PeaProperties.defaultMCost = defaultMCost;
	}
	/**
	 * @return the defaultTCost
	 */
	public static int getDefaultTCost() {
		return defaultTCost;
	}
	/**
	 * @param defaultTCost the defaultTCost to set
	 */
	public static void setDefaultTCost(int defaultTCost) {
		PeaProperties.defaultTCost = defaultTCost;
	}
	/**
	 * @return the defaultParallel
	 */
	public static int getDefaultParallel() {
		return defaultParallel;
	}
	/**
	 * @param defaultParallel the defaultParallel to set
	 */
	public final static void setDefaultParallel(int defaultParallel) {
		PeaProperties.defaultParallel = defaultParallel;
	}
	/**
	 * Get the key file flag: 0 no key file, 1 default key file, 2 user chosen key file
	 * 
	 * @return the defaultKeyFileFlag
	 */
	public static int getDefaultKeyFileFlag() {
		return defaultKeyFileFlag;
	}
	/**
	 * Set the key file flag: 0 no key file, 1 default key file, 2 user chosen key file
	 * 
	 * @param defaultKeyFileFlag the defaultKeyFileFlag to set
	 */
	public static void setDefaultKeyFileFlag(int defaultKeyFileFlag) {
		PeaProperties.defaultKeyFileFlag = defaultKeyFileFlag;
	}
	public final static String getDefaultAlgoParamString() {
		return defaultAlgoParamString;
	}
	public final static void setDefaultAlgoParamString(String newDefaultString) {
		defaultAlgoParamString = newDefaultString;
	}
	/**
	 * Get the identifier string for algorithms and parameters
	 * 
	 * @return the algoParamString as byte array
	 */
	public static byte[] getAlgoParamString() {
		return algoParamString;
	}
	/**
	 * Set the identifier string for algorithms and parameters
	 * 
	 * @param algoParamString the algoParamString as byte array
	 */
	public static void setAlgoParamString(byte[] algoParamString) {
		//TestLog.e(PeaProperties.class, "XXXXXXXXXXX-" + new String(algoParamString), 5);
		PeaProperties.algoParamString = algoParamString;
	}
	/**
	 * @return the versionInfo
	 */
	public static String getVersionInfo() {
		return versionInfo;
	}
	/**
	 * @param versionInfo the versionInfo to set
	 */
	public static void setVersionInfo(String versionInfo) {
		PeaProperties.versionInfo = versionInfo;
	}
	/**
	 * @return the commandLineMode
	 */
	public static boolean isCommandLineMode() {
		return commandLineMode;
	}
	/**
	 * @param commandLineMode the commandLineMode to set
	 */
	public static void setCommandLineMode(boolean commandLineMode) {
		PeaProperties.commandLineMode = commandLineMode;
	}
	/** Get the PEA version of the current PEA: 
	 * Version 2: algorithms and parameters to choose.
	 * 
	 * @return	the PEA version
	 */
	public static int getPeaVersion() {
		return PEA_VERSION;
	}
	/**
	 * Get the key file name with path.
	 * This file is only used when the flag is set to 1.
	 * 
	 * @return	the key file name
	 */
	public static String getDefaultKeyFileName() {
		return defaultKeyFileName;
	}
	/**
	 * Set the key file name with path.
	 * This file is only used when the flag is set to 1.
	 * 
	 * @param keyFileName	the key file name with path
	 */
	public static void setDefaultKeyFileName(String _newDefaultKeyFileName) {
		PeaProperties.defaultKeyFileName = _newDefaultKeyFileName;
	}
	/**
	 * Get the last set language
	 * 
	 * @return	language String like "en" "tr"
	 */
	public static String getLastLanguage() {
		return lastLanguage;
	}
	/**
	 * Set the last set language
	 * 
	 * @param lastLanguage	language String like "en" "tr"
	 */
	public static void setLastLanguage(String _lastLanguage) {
		if (_lastLanguage !=  null) {
			PeaProperties.lastLanguage = _lastLanguage;
			PeaProperties.locale = Locale.forLanguageTag(_lastLanguage);
		} else {
			TestLog.ve(PeaProperties.class, "language to set is null", 5);
		}
	}
	public static boolean isInstallLanguageOptionShown() {
		return installLanguageOptionShown;
	}
	/** 
	 * Set if the option to install the current default language was 
	 * shown (to avoid showing several times)
	 * 
	 * @param _installLanguageOptionShown
	 */
	public static void setInstallLanguageOptionShown(boolean _installLanguageOptionShown) {
		PeaProperties.installLanguageOptionShown = _installLanguageOptionShown;
	}
	/**
	 * @return the locale
	 */
	public static Locale getLocale() {
		return locale;
	}

	/**
	 * @return the random
	 */
	public static Random getRandom() {
		return RANDOM;
	}
	/**
	 * @return the secureRandom
	 */
	public static SecureRandom getSecureRandom() {
		return SECURE_RANDOM;
	}
	/**
	 * @return the keyFileOnlyMode
	 */
	public static boolean isKeyFileOnlyMode() {
		return keyFileOnlyMode;
	}
	/**
	 * @param keyFileOnlyMode the keyFileOnlyMode to set
	 */
	public static void setKeyFileOnlyMode(boolean keyFileOnlyMode) {
		PeaProperties.keyFileOnlyMode = keyFileOnlyMode;
	}
	/**
	 * @return the directStart
	 */
	public static boolean isDirectStart() {
		return directStart;
	}
	/**
	 * @param directStart the directStart to set
	 */
	public static void setDirectStart(boolean directStart) {
		PeaProperties.directStart = directStart;
	}
	/**
	 * Get the last used files.
	 * These are the files that were used when the PEA
	 * was closed the last time. 
	 * 
	 * @return the lastUsedFiles
	 */
	public static ArrayList<String> getLastUsedFiles() {
		return lastUsedFiles;
	}
	/**
	 * Set the last used files.
	 * These are the files that are used when the PEA
	 * is closed to open
	 * automatically on next start. 
	 * 
	 * @param lastUsedFiles the lastUsedFiles to set
	 */
	public static void setLastUsedFiles(ArrayList<String> lastUsedFiles) {
		PeaProperties.lastUsedFiles = lastUsedFiles;
	}
	/**
	 * Add a file to the last used files to open
	 * automatically on next start. 
	 * 
	 * @param newFileName
	 * 
	 * @return	true if the list was modified
	 */
/*	public static boolean addLastUsedFile(String newFileName) {
		if (newFileName != null &&  ! PeaProperties.lastUsedFiles.contains(newFileName)) {
			return PeaProperties.lastUsedFiles.add(newFileName);
		} else {
			return false;
		}
	}*/
	/**
	 * Remove a file from the last used files to open
	 * automatically on next start. 
	 * 
	 * @param oldFileName
	 * 
	 * @return	true if the list was modified
	 */
/*	public static boolean removeLastUsedFile(String oldFileName) {
		if (oldFileName != null &&  PeaProperties.lastUsedFiles.contains(oldFileName)) {
			return PeaProperties.lastUsedFiles.remove(oldFileName);
		} else {
			return false;
		}
	}*/
}
