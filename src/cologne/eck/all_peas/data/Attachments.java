package cologne.eck.all_peas.data;

/*
 * Peafactory - Production of Password Encryption Archives
 * Copyright (C) 2015  Axel von dem Bruch
 * 
 * This library is free software; you can redistribute it and/or modify it 
 * under the terms of the GNU General Public License as published 
 * by the Free Software Foundation; either version 2 of the License, 
 * or (at your option) any later version.
 * This library is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
 * or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU General Public License for more details.
 * See:  http://www.gnu.org/licenses/gpl-2.0.html
 * You should have received a copy of the GNU General Public License 
 * along with this library.
 */

/**
 * Attachments for byte blocks and files. 
 */


import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.charset.Charset;
import java.util.Arrays;

import org.bouncycastle.crypto.Digest;
import org.bouncycastle.crypto.InvalidCipherTextException;
import org.bouncycastle.crypto.digests.Blake2bDigest;

import cologne.eck.peafactory.crypto.AlgoParamHandler;
import cologne.eck.peafactory.crypto.CipherStuff;
import cologne.eck.peafactory.crypto.HashStuff;
import cologne.eck.peafactory.crypto.RandomStuff;
import cologne.eck.peafactory.crypto.kdf.KeyDerivation;
import cologne.eck.tools.Comparator;
import cologne.eck.tools.TestLog;
//import cologne.eck.tools.Help;
//import cologne.eck.tools.Converter;
import cologne.eck.tools.UnexpectedValueException;
import cologne.eck.tools.Zeroizer;
import settings.PeaSettings;



public final class Attachments {
	
	// Random values:
	private static byte[] programRandomBytes  = null; // unique for every program/dialog
	private static byte[] nonce = null; // unique for every encrypted content 
	private static byte[] fileIdentifier = null; // unique for every program/dialog

	private final static int FILE_IDENTIFIER_SIZE = 8; // byte
	private final static int PROGRAM_RANDOM_BYTES_SIZE = KeyDerivation.getSaltSize();// at most 129
	private final static int NONCE_SIZE = 16;// byte

	private final static byte[] PSW_IDENTIFIER = "_PEA_FACTORY_ID_".getBytes(Charset.forName("UTF-8"));// 16 byte
	
	//========== Helper Functions: ==============================================
	
	private final static void addBytes(RandomAccessFile f, byte[] supplementBytes) 
			throws IOException {
		f.seek( f.length() ); // set file pointer
		f.write(supplementBytes, 0, supplementBytes.length ); 
	}
	protected final static byte[] getLastBytes(RandomAccessFile f, int resultSize, boolean truncate) 
			throws IOException {
		
		if (resultSize < 0 || f.length() < resultSize) {
			throw new IllegalArgumentException("Attachments getLastBytes invalid value to get last bytes: resultSize " + resultSize + ", length: " + f.length());
		}
			
		byte[] result = new byte[resultSize];

		long fileSize = 0;

		fileSize = f.length();
		if (fileSize - resultSize < 0) {
			throw new IllegalArgumentException( "Attachments getLastBytes invalid file size, file:  " + f.toString() );
		}
		f.seek(fileSize - resultSize); // set file pointer
		f.read(result, 0, resultSize ); // read bytes
		if (truncate == true) {
			cutLastBytes(f,  resultSize); // truncate file
		}
		return result;
	}
	// more secure cut (overwrites truncated bytes)
	private final static void cutLastBytes(RandomAccessFile f, int truncateSize) 
			throws IOException {
		if (truncateSize < 0 || f.length() < truncateSize) {
			throw new IllegalArgumentException("Attachments cutLastBytes invalid value to cut last bytes");
		}
		byte[] nullBytes = new byte[truncateSize];
		f.seek(f.length() - truncateSize); // set file pointer
		f.write(nullBytes, 0, nullBytes.length);
		f.setLength(f.length() - truncateSize);
	}
	
	public final static byte[] addBytes(byte[] previousBytes, byte[] bytesToAdd) {
		
		if(previousBytes == null || bytesToAdd == null) {
			throw new IllegalArgumentException("byte array is null");
		}
		
		byte[] result = new byte[previousBytes.length + bytesToAdd.length];
		System.arraycopy(previousBytes,  0,  
				result,  0,  previousBytes.length);
		System.arraycopy(bytesToAdd,  0,  
				result, previousBytes.length, bytesToAdd.length);		
		Zeroizer.zero(previousBytes);		
		return result;		
	}

	
	//=== psw identifier ======================================================
	
	/**
	 * Get the cipher text of the String "_PEA_FACTORY_ID_" 
	 * 
	 * @param key		the key to use for encryption
	 * @param nonce		the nonce to use for encryption
	 * 
	 * @return			the encrypted String "_PEA_FACTORY_ID_"
	 * @throws IllegalStateException
	 * @throws InvalidCipherTextException
	 */
	public final static byte[] createPswIdentifierAttachment(byte[] key, byte[] nonce) 
			throws IllegalStateException, InvalidCipherTextException {

		Comparator.checkNullVector(key);
		Comparator.checkNullVector(nonce);

		// use the first part of the hash of the nonce as nonce:
		byte[] extraNonce = new byte[nonce.length];
		byte[] hashedNonce = HashStuff.hash(nonce);
		System.arraycopy(hashedNonce,  0,  extraNonce,  0,  extraNonce.length);
		
		byte[] pswIdentifier = Attachments.getPSW_IDENTIFIER();
		
		byte[] pswIdentifierAttachment =  CipherStuff.getCipherMode().encryptBytesNoMAC(	
				pswIdentifier, 
				key, 
				extraNonce);
		
		if (pswIdentifierAttachment == null) {
			new UnexpectedValueException("byte[]", "pswIdentifierAttachment", "failed to create").printDescription();
		}
		return pswIdentifierAttachment;
	}
	/**
	 * Check if the given byte array is the cipher text of the
	 * String "_PEA_FACTORY_ID_"
	 * 
	 * @param pswIdentifierAttachment	the byte array to check
	 * @param key						the key used for decryption
	 * @param nonce						the nonce used for decryption
	 * 
	 * @return							true if the byte array is the
	 * 									encrypted String "_PEA_FACTORY_ID_"
	 */
	public final static boolean checkPswIdentifier(byte[] pswIdentifierAttachment, byte[] key, byte[] nonce) {
		
		if (pswIdentifierAttachment == null) {
			TestLog.e(Attachments.class, "Missing array to check password");
			TestLog.e(Attachments.class, "pswIdentifierAttachment is null", 5);
			return false;
		}
		Comparator.checkNullVector(key);
		Comparator.checkNullVector(nonce);
		
		// create an extra nonce from hash of original nonce: 
		byte[] extraNonce = new byte[nonce.length];
		byte[] hashedNonce = HashStuff.hash(nonce);
		System.arraycopy(hashedNonce,  0,  extraNonce,  0,  extraNonce.length);
		
		byte[] pswIdentifier = Attachments.getPSW_IDENTIFIER();
	
		byte[] encryptedPswIdentifier =  CipherStuff.getCipherMode().encryptBytesNoMAC(	
				pswIdentifier, 
				key, 
				extraNonce);

		boolean equal = Comparator.compare(encryptedPswIdentifier, pswIdentifierAttachment);	
		if (equal == false) {
			TestLog.ve(Attachments.class, "pswIdentifier failed.", 5);//TODO  delete lines \nExpected: " + Converter.bytes2hex(encryptedPswIdentifier)
		}
		return equal;
	}	
	

	//=== file identifier ======================================================
	
	public final static void addFileIdentifier( RandomAccessFile f) 
			throws IOException { 
		
		if( fileIdentifier == null ) {
			throw new IllegalArgumentException ("fileIdentifier null");
		}		
		addBytes(f, fileIdentifier);
	}

	public final static byte[] addFileIdentifier(byte[] cipherBytes) {
		
		if(cipherBytes == null || fileIdentifier == null) {
			TestLog.e(Attachments.class, "Attachments: add fileIdentifier failed: "
					+ "cipherBytes or fileIdentifier null");
			PeaProperties.getVmBridge().exitWithError(null, "Attachments.addFileIdentifier(): Missing cipherBytes or fileIdentifier to add", true);
			return null;
		}

		byte[] result = new byte[cipherBytes.length + fileIdentifier.length];
		System.arraycopy(cipherBytes,  0,  result,  0,  cipherBytes.length);
		System.arraycopy(fileIdentifier,  0,  
				result, cipherBytes.length, fileIdentifier.length);

		Zeroizer.zero(cipherBytes);		
		return result;		
	}

	/**
	 * Check the last bytes of a file if they are
	 * equal to a valid file identifier. If the identifier
	 * does not match,the file is not truncated
	 * 
	 * @param f				the RandomAccessFile in r/w mode
	 * @param truncate		true: truncate the fileIdentifier
	 * 
	 * @return				the version number of the identifier: 
	 * 						-1: invalid, 
	 * 						0: randomly generated, current identifier (key file property), 
	 * 						1: version 1 identifier, 2: version 2 identifier
	 * @throws IOException
	 */
	public final static int checkFileIdentifier( RandomAccessFile f, boolean truncate) 
			throws IOException { 

		if(f.length() < FILE_IDENTIFIER_SIZE ) {
			//TestLog.ve(Attachments.class, "Invalid length of file (too short)", 5);
			return -1;
		}	

		byte[] checkIdentifier = getLastBytes(f, FILE_IDENTIFIER_SIZE, false);
		if (PeaSettings.getKeyFileProperty() == false){
			if (Arrays.equals(checkIdentifier, AttachmentHandler.getVersion1ID()) ) {
				//if (Arrays.equals(checkIdentifier, fileIdentifier)) {
				if (truncate == true) {
					cutLastBytes(f, FILE_IDENTIFIER_SIZE);
				}
				return 1;
			} else if (Arrays.equals(checkIdentifier, AttachmentHandler.getVersion2ID())) {
				if (truncate == true) {
					cutLastBytes(f, FILE_IDENTIFIER_SIZE);
				}
				return 2;
			} else {
				// unknown identifier
				return -1;
			}
		} else { // randomly generated fileIdentifier
			// file identifier was set
			if (Arrays.equals( checkIdentifier, fileIdentifier) ) {
				if (truncate == true) {
					cutLastBytes(f, FILE_IDENTIFIER_SIZE);
				}
				return 0;
			} else {
				return -1;
			}
		}
	}
	
	/**
	 * Checks if an array of bytes contains the file identifier
	 * of version 1 or 2 or the random identifier for a PEA
	 * with key file property. The input is then zeroized.
	 * 
	 * @param input		the byte array to check
	 * @param truncate	true: truncate the file identifier from input
	 * 
	 * @return	the resulting array (truncated or not) or null
	 * 			if array does not contain the file identifier
	 */
	public final static byte[] checkFileIdentifier( byte[] input, boolean truncate) {		
		
		byte[] result = new byte[input.length - FILE_IDENTIFIER_SIZE];	

		if(input.length < FILE_IDENTIFIER_SIZE ) {
			TestLog.e(Attachments.class, "Attachments checkFileIdentifier input too short to contain fileIdentifier");
			return null;
		}	

		byte[] identifierToCheck = new byte[FILE_IDENTIFIER_SIZE];
		if (PeaSettings.getKeyFileProperty() == false){
			System.arraycopy(input, input.length - FILE_IDENTIFIER_SIZE, identifierToCheck, 0, FILE_IDENTIFIER_SIZE);
			//TestLog.v(this.getClass(), "identifierToCheck: " + new String(identifierToCheck));
			byte[] secondVersionwId = AttachmentHandler.getVersion2ID();
			byte[] firstVersionwId = AttachmentHandler.getVersion1ID();
			if (Arrays.equals(identifierToCheck, secondVersionwId) || Arrays.equals(identifierToCheck, firstVersionwId)) {
				if (truncate == true) {

					System.arraycopy(input, 0, result, 0, result.length);
					Zeroizer.zero(input);		
				}
				return result;
			} else {
				//TestLog.ve(Attachments.class, "file identifier failed..." + new String(identifierToCheck) + Converter.bytes2hex(identifierToCheck));
				return null;
			}
		} else {// randomly generated fileIdentifier
			System.arraycopy(input, input.length - FILE_IDENTIFIER_SIZE, identifierToCheck, 0, FILE_IDENTIFIER_SIZE);
			//TestLog.v(this.getClass(), "identifierToCheck: " + new String(identifierToCheck));
			if (Arrays.equals(identifierToCheck, fileIdentifier) ) {
				if (truncate == true) {

					System.arraycopy(input, 0, result, 0, result.length);
					Zeroizer.zero(input);		
				}
				return result;
			} else {
				return null;
			}
		}
	}
	
	/**
	 * Create a random file identifier with HKDF (Blake2b) 
	 * from key file property to use
	 * in PEAs with key file property
	 */
	public final static void generateAndSetFileIdentifier() {
		// Generate from PROGRAM_RANDOM_BYTES with Blake2b
		Digest oldDigest = HashStuff.getHashAlgo();
		HashStuff.setHashAlgo(new Blake2bDigest());
		fileIdentifier = HashStuff.generateHKDFBytes(programRandomBytes, "_PEA_FACTORY_ID_".getBytes(Charset.forName("UTF-8")), FILE_IDENTIFIER_SIZE);
		HashStuff.setHashAlgo(oldDigest);
	}
	
	public final static void setFileIdentifier(byte[] _fileIdentifier) {
		fileIdentifier = _fileIdentifier;
	}
	
	public final static byte[] getFileIdentifier() {

		return fileIdentifier;
	}
	
	//=== NONCE ======================================================
	
	public final static void addNonce( RandomAccessFile f, byte[] nonce) 
			throws IOException { 
		
		if( nonce == null ) {
			throw new IllegalArgumentException ("Nonce null");
		}		
		if (nonce.length != NONCE_SIZE){
			throw new IllegalArgumentException ("Wrong size of nonce : " + nonce.length 
					+ ", must be: " + NONCE_SIZE);
		}
		addBytes(f, nonce);
	}
	
	public final static byte[] addNonce(byte[] cipherBytes, byte[] nonce) {
		
		if(cipherBytes == null || nonce == null) {
			TestLog.e(Attachments.class, "Attachments: addNonce failed: "
					+ "cipherBytes or nonce null");
			PeaProperties.getVmBridge().exitWithError(null, "Attachments.addNonce(): Missing cipherBytes or nonce to add", true);
			return null;
		}
		
		if (nonce.length != NONCE_SIZE){
			throw new IllegalArgumentException ("Wrong size of nonce : " + nonce.length 
					+ ", must be: " + NONCE_SIZE);
		}		
		byte[] result = new byte[cipherBytes.length + NONCE_SIZE];
		System.arraycopy(cipherBytes,  0,  result,  0,  cipherBytes.length);
		System.arraycopy(nonce,  0,  
				result, cipherBytes.length, NONCE_SIZE);		
		Zeroizer.zero(cipherBytes);
		return result;		
	}
	
	public final static byte[] getAndCutNonce( RandomAccessFile f, boolean truncate) 
			throws IOException { 
		if(f.length() < NONCE_SIZE ) {
			throw new IllegalArgumentException("file is too short: " + f.toString() );
		}
		return  getLastBytes(f, NONCE_SIZE, truncate);
	}	
	
	public final static byte[] generateNonce() {
		return new RandomStuff().createUniqueBytes( NONCE_SIZE);
	}
	
	public final static void setNonce(byte[] _nonce
			) { // required in PeaControl
		if (_nonce == null) {
			throw new IllegalArgumentException("Nonce to set null");
		}
		if (_nonce.length != NONCE_SIZE){
			throw new IllegalArgumentException ("Wrong size of nonce : " + nonce.length 
					+ ", must be: " + NONCE_SIZE);
		}
		nonce = _nonce;
	}	
	
	public final static byte[] getNonce() {
		if (nonce == null) {
			//TestLog.e(XXXX.class, "Attachments getCryptIV: cryptIV null");
		}
		return nonce;
	}
	
	public final static byte[] calculateNonce( byte[] input ) {

		byte[] result = new byte[NONCE_SIZE];		
		if (input.length < NONCE_SIZE) {
			TestLog.e(Attachments.class, "Attachments calculateNonce: input too short");
			return null;
		} else {
			System.arraycopy(input, input.length - NONCE_SIZE, result, 0, result.length);
			return result;
		}
	}
	
	public final static byte[] cutNonce(byte[] input) {	
				
		if(input.length < NONCE_SIZE ) {
			TestLog.e(Attachments.class, "input is too short to contain Nonce. ");
			return null;
		} 
		byte[] result = new byte[input.length - NONCE_SIZE];
		System.arraycopy(input, 0, result, 0, result.length);
		Zeroizer.zero(input);
		
		return result;
	}
	
	//===================================================
	// Getter & Setter & Generators
	
	/**
	 * Generate and set an array of bytes with random values
	 * that is used as key file property 
	 * (same size as salt)
	 */
	public final static void generateAndSetProgramRandomBytes() {
		programRandomBytes = new RandomStuff().createRandomBytes( Attachments.getProgramRandomBytesSize() );		
	}
	
	/**
	 * Set an array of bytes with random values
	 * that is used as key file property 
	 * (same size as salt)
	 */
	public final static void setProgramRandomBytes( byte[] _programRandomBytes) {
		if (_programRandomBytes.length != PROGRAM_RANDOM_BYTES_SIZE) {
			new UnexpectedValueException("byte[]", "_programRandomBytes", "has wrong size: "
					+ _programRandomBytes.length + ", but must be " + PROGRAM_RANDOM_BYTES_SIZE).printDescription();
		}
		programRandomBytes = _programRandomBytes;
	}
	
	/**
	 * Get an array of bytes with random values
	 * that is used as key file property 
	 * (same size as salt)
	 * 
	 * @return the bytes that are used for key file property
	 */
	public final static byte[] getProgramRandomBytes() {
		if (programRandomBytes == null) {
			TestLog.e(Attachments.class, "Attachments getProgramRandomBytes: programRandomBytes null");
		}
		return programRandomBytes;
	}
	
	public final static int getProgramRandomBytesSize() {
		return PROGRAM_RANDOM_BYTES_SIZE;
	}
	
	public final static int getPswIdentifierSize() {
		return PSW_IDENTIFIER.length;//PSW_IDENTIFIER_SIZE;
	}
	
	public final static int getFileIdentifierSize() {
		return FILE_IDENTIFIER_SIZE;
	}
	
	public final static int getNonceSize() {
		return NONCE_SIZE;
	}
	
	public static byte[] attachBytes(byte[] sourceBytes, byte[] bytesToAttach) {
		
		if(sourceBytes == null || bytesToAttach == null) {
			if (sourceBytes == null) {
				TestLog.e(Attachments.class, "sourceBytes are null");
			} else {
				TestLog.e(Attachments.class, "bytesToAttach are null");
			}
			throw new IllegalArgumentException ("Attachments: attachedBytes failed");	
			//TestLog.e(XXXX.class, "Attachments: attachedBytes failed");			
			//System.exit(1);
		}
		
		byte[] result = new byte[sourceBytes.length + bytesToAttach.length];
		System.arraycopy(sourceBytes,  0,  result,  0,  sourceBytes.length);
		System.arraycopy(bytesToAttach,  0,  
				result, sourceBytes.length, bytesToAttach.length);
		
		Zeroizer.zero(sourceBytes);
		return result;	
	}	
	
	public final static void addSalt( RandomAccessFile f, byte[] attachedSalt) 
			throws IOException { 		
		if( attachedSalt == null ) {
			throw new IllegalArgumentException ("Attached Salt null");
		}		
		addBytes(f, attachedSalt);
	}
	
	/**
	 * Add the algorithm parameter string to a file
	 * 
	 * @param f					the file as RandomAccessFile
	 * @param algoParamString	the algorithm-parameter string to add
	 * @throws IOException
	 */
	public final static void addAlgoParamString(RandomAccessFile f, String algoParamString) 
			throws IOException {
		if (algoParamString == null ) {
			new UnexpectedValueException("String", "algoParamString", "is null " ).printDescription();
			TestLog.ve(Attachments.class, "Missing algorithm parameter string", 5);
			return;
		}
		if (algoParamString.length() != AlgoParamHandler.getAlgoParamStringSize()) {
			TestLog.ve(Attachments.class, "Aalgorithm parameter string with invalid length:" + algoParamString, 5);
			return;
		}
		addBytes(f, algoParamString.getBytes(AttachmentHandler.getASCIICharset()));
	}
	
	/**
	 * Get and eventually cut the end bytes of a
	 * RandomAccessFile with length of the algorithm
	 * parameter string. 
	 * Note: Other attachments must be truncated before!
	 * 
	 * @param f			the RandomAccessFile
	 * @param truncate	true: truncate the end bytes
	 * 
	 * @return			the end bytes in the length of the 
						algorithm parameter string
	 * @throws IOException
	 */
	public final static byte[] getAndCutAlgoParamStringFromEnd( RandomAccessFile f, boolean truncate) 
			throws IOException { 
		int algoParamSize = AlgoParamHandler.getAlgoParamStringSize();
		if(f.length() < algoParamSize ) {
			throw new IllegalArgumentException("Attachments getAndCutAlgoParamString file is too short: " + f.toString() );
		}
		return  getLastBytes(f, algoParamSize, truncate);
	}
	
	public final static byte[] cutAlgoParamStringFromEnd(byte[] input) {
		int algoParamSize = AlgoParamHandler.getAlgoParamStringSize();
		if(input.length < algoParamSize ) {
			TestLog.e(Attachments.class, "Attachments input is too short to contain algorithm-parameter string. ");
			TestLog.ve(Attachments.class, "input too short to cut algorithm-parameter string",  5);
			return null;
		} 
		byte[] result = new byte[input.length - algoParamSize];
		byte[] algoParam = new byte[algoParamSize];
		System.arraycopy(input, 0, result, 0, result.length);
		System.arraycopy(input, input.length - algoParamSize, algoParam, 0, algoParamSize);
		// check algoParam:
		String error = AlgoParamHandler.checkString(new String(algoParam, AttachmentHandler.getASCIICharset()));
		if (error != null && (Arrays.equals(PeaProperties.getAlgoParamString(), AttachmentHandler.getVersion2ID()))){
				//PeaProperties.getAlgoParamString().equals(AttachmentHandler.getVersion2ID()))){
			TestLog.e(Attachments.class, "Invalid input (algorithm-parameter): " +  error);
			TestLog.ve(Attachments.class, "Invalid algorithm-parameter string from input: " +  error, 5);
			return null;
		}
		Zeroizer.zero(input);		
		return result;
	}

	public final static byte[] cutSalt(byte[] input) {	
		
		if(input.length < PROGRAM_RANDOM_BYTES_SIZE ) {
			TestLog.e(Attachments.class, "Attachments cutSalt input is too short to contain the salt. ");
			return null;
		} 
		byte[] result = new byte[input.length - PROGRAM_RANDOM_BYTES_SIZE];
		System.arraycopy(input, 0, result, 0, result.length);
		Zeroizer.zero(input);
		
		return result;
	}

	/**
	 * Get and eventually cut the end bytes of a
	 * RandomAccessFile with length of salt. 
	 * Note: Other attachments must be truncated before!
	 * 
	 * @param f			the RandomAccessFile
	 * @param truncate	true: truncate the end bytes
	 * 
	 * @return			the end bytes in the length of the salt
	 * @throws IOException
	 */
	public final static byte[] getAndCutSaltFromEnd( RandomAccessFile f, boolean truncate) 
			throws IOException { 
		if(f.length() < PROGRAM_RANDOM_BYTES_SIZE ) {
			throw new IllegalArgumentException("Attachments getAndCutSalt file is too short: " + f.toString() );
		}
		return  getLastBytes(f, PROGRAM_RANDOM_BYTES_SIZE, truncate);
	}
	
	public final static byte[] getEndBytesOfFile( String fileName, int resultLen) 
			throws IOException { 
		
		RandomAccessFile f = new RandomAccessFile( fileName, "r" );		
		if(f.length() < resultLen) {
			TestLog.e(Attachments.class, "Attachments getEndBytesOfFile File with length " + f.length() + " is too short to get " + resultLen + " end bytes");
		} 
		byte[] result = getLastBytes(f, resultLen, false);
		f.close();
		return result;
	}
	
	/**
	 * Get the last bytes of an array of bytes.
	 * The array is not modified.
	 * 
	 * @param input		array to get last bytes from
	 * @param length	length of last bytes
	 * 
	 * @return 	last bytes of an array
	 */
	public final static byte[] getEndOfBytes(byte[] input, int length) {
		if (input.length < length) {
			TestLog.e(Attachments.class, "Invalid size: " + input.length);
			return null;
		}
		byte[] result = new byte[length];
		System.arraycopy(input, input.length - length, result, 0, length);
		return result;
	}
	
	/**
	 * @return the pSW_IDENTIFIER
	 */
	private static final byte[] getPSW_IDENTIFIER() {
		return PSW_IDENTIFIER;
	}
}
