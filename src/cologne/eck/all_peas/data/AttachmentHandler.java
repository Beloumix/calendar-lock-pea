package cologne.eck.all_peas.data;

/*
 * Peafactory - Production of Password Encryption Archives
 * Copyright (C) 2020  Axel von dem Bruch
 * 
 * This library is free software; you can redistribute it and/or modify it 
 * under the terms of the GNU General Public License as published 
 * by the Free Software Foundation; either version 2 of the License, 
 * or (at your option) any later version.
 * This library is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
 * or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU General Public License for more details.
 * See:  http://www.gnu.org/licenses/gpl-2.0.html
 * You should have received a copy of the GNU General Public License 
 * along with this library.
 */

import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

import javax.swing.filechooser.FileNameExtensionFilter;

import cologne.eck.all_peas.cloud.CloudControl;
import cologne.eck.all_peas.cloud.CloudPropertyHandler;
import cologne.eck.all_peas.control.PathFileManager;
import cologne.eck.all_peas.control.PeaControl;
import cologne.eck.all_peas.files.ExecutionTimeObserver;
import cologne.eck.all_peas.files.FilePanelHolder;
import cologne.eck.all_peas.files.InvalidPanel;
import cologne.eck.all_peas.gui.PeaDialog;
import cologne.eck.all_peas.gui.PeaFileChooser;
import cologne.eck.all_peas.gui.PswDialogView;
import cologne.eck.all_peas.vm_specific.JREProperties;
import cologne.eck.peafactory.crypto.AlgoParamHandler;
import cologne.eck.peafactory.crypto.kdf.KeyDerivation;
import cologne.eck.tools.Converter;
import cologne.eck.tools.TestLog;
import cologne.eck.tools.UnexpectedValueException;
import settings.PeaSettings;

public class AttachmentHandler {
	
	private final static Charset usASCII = Charset.forName("US-ASCII");
	private static final byte[] ve1ID = "PFACTORY".getBytes(usASCII);
	private static final byte[] ve2ID = "PEA-VE-2".getBytes(usASCII);
	
	
	/**
	 *  A scheme depends on: 0: versionID, 1: algoParamString or null, 2: salt
	 */
	private static ArrayList<byte[][]> schemeMarkers = new ArrayList<byte[][]>();
	
	/**
	 * last selected file. Used to open FileChooser at this directory
	 */
	private static File lastDir = null;
	
	/*
	 * While reading path file, do not display messages
	 */
	private static boolean isReadingPathFile = false;
	
    /**
     * Display the previously stored file names.
     * Warning for long execution time if these files are too large
     * or too many is disabled, only extreme number and size results
     * in a warning message. 
     * This is called in PeaControl initializeVariables()
     */
	public final static void initFileNames() {
		
		TestLog.v(AttachmentHandler.class, "Initialize file names...");
    	isReadingPathFile = true;

    	// disable warning for number and size
    	ExecutionTimeObserver.setSizeWarning(true);
    	ExecutionTimeObserver.setNumberWarning(true);
    	
    	if (Attachments.getFileIdentifier() == null) { // needs to check
    		Attachments.setFileIdentifier(PeaSettings.getFileIdentifier() );
    		TestLog.v(AttachmentHandler.class, "File identifier: " + Converter.bytes2hex(Attachments.getFileIdentifier())
    		+ " - " + new String(Attachments.getFileIdentifier()) );
    	}
    	//TestLog.v(AttachmentHandler.class, "FileIdentifier set...");
    	// file set in PeaFactory 
    	if ( PeaSettings.getExternalFilePath() != null) {
    		File externalFile = new File(PeaSettings.getExternalFilePath());
    		File[] extFile = {externalFile};
    		distributeFiles(extFile);
    	}

		String[] pathNames = PathFileManager.getFileNamesFromPathFile();// pathString.split("\n");
		//TestLog.v(AttachmentHandler.class, "get path file names...");
		if (pathNames == null) {
			if (PeaProperties.isTestMode()){
				TestLog.v(AttachmentHandler.class, "FileDisplayPanel: empty path file. " );
			}
			//return;
		} else {	
			// show only valid files:
			// there are no file names, so just sort
			ArrayList<String> list = new ArrayList<String>(Arrays.asList(pathNames));
			Collections.sort(list);
			int listSize = list.size();
			File[] fileArray = new File[listSize];
			for (int i = 0; i < listSize; i++) {
				// existence was already tested in PathFileManager
				fileArray[i] = new File(list.get(i));
				//TestLog.v(this.getClass(), "file in path: " + list.get(i));
			}
			boolean added = distributeFiles(fileArray);
			if (added == true) {
				FilePanelHolder.updateFileView();
				FilePanelHolder.updateWindow();
			}
		}	
		if ( ! PeaProperties.getFileType().equals("file")) {
			if (new File(CloudPropertyHandler.getCloudPropertyFileName()).exists() ) {
				TestLog.v(AttachmentHandler.class, "Search for cloud files in properties...");
				// load cloud files from properties
				String[] allProviders = CloudPropertyHandler.getProviderNames();
				if (allProviders != null) {
					boolean changed = false;
					for (String providerName : allProviders) {
						//String[] allSubSetNames = CloudPropertyHandler.getSubSetNames(null, providerName);				
						String[][] allSubSets = CloudPropertyHandler.getSubSets(providerName, null);
						if (allSubSets != null) {
							TestLog.v(AttachmentHandler.class, "Found provider " + providerName + " with " + allSubSets.length + " sub sets");
							for (String[] subSet : allSubSets) {
								String salt = subSet[0];
								if (salt != null) {
									String algoParam = subSet[1];
									if (algoParam ==  null) {
										String error = CloudPropertyHandler.migrateFromVersion1(null, null);
										if (error != null) {
											PeaDialog.showMessage(JREProperties.getMainWindow(), 
													PeaProperties.getVmBridge().translate("old_version") + ":\n\n"

											+ " -> " + PeaProperties.getVmBridge().translate("cloud") + "\n    ->  "
											+ PeaProperties.getVmBridge().translate("modify_cloud_provider") + "\n         ->  "
											+ providerName 
											, null, 0);							
										}
										algoParam = Converter.bytes2hex( AlgoParamHandler.getV1Bytes());
									}
									if (algoParam != null) {
										String filesFromScheme = subSet[3];
										TestLog.v(AttachmentHandler.class, "Found files as comma separated list:\n " + filesFromScheme);
										if (filesFromScheme != null && filesFromScheme.length() > 1) {
											String[] fileNames = filesFromScheme.split(",");
											if (fileNames != null) {
												for (String fileName : fileNames) {
													if (fileName != null) {
														TestLog.v(AttachmentHandler.class, "Try to add file: " + fileName);
														int index = addCloudFile(fileName, providerName, Converter.hex2bytes(salt), Converter.hex2bytes(algoParam));
														if (index == -1) { // failed...
															TestLog.e(AttachmentHandler.class, "Cant add cloud file " + fileName + "from properties for " + providerName);
															TestLog.ve(AttachmentHandler.class, "Adding cloud file failed ", 5);
														} else {
															changed = true;
														}
													} else {
														TestLog.v(AttachmentHandler.class, "Null file name found for " +  providerName);
													}
												}
											} else {
												TestLog.v(AttachmentHandler.class, "No files with algorithm-salt combination found for " +  providerName);
											}
										} else {
											TestLog.v(AttachmentHandler.class, "No files with algorithm-salt combination found for " +  providerName);				
										}
									} else {
										TestLog.e(AttachmentHandler.class, "No algorithm string in properties found for " +  providerName);
										TestLog.ve(AttachmentHandler.class, "No algorithm string in properties found for " +  providerName, 5);
									}
								} else {
									TestLog.e(AttachmentHandler.class, "No salt in properties found for " +  providerName);
									TestLog.ve(AttachmentHandler.class, "No salt in properties found for " +  providerName, 5);
								}
							}
						} else {
							TestLog.ve(AttachmentHandler.class, " Not set found", 5);
						}
					}
					if (changed == true) {
						FilePanelHolder.updateFileView();
						FilePanelHolder.updateWindow();
					}
				}
			}
		}
    	// enable warning for number and size
    	ExecutionTimeObserver.setSizeWarning(false);
    	ExecutionTimeObserver.setNumberWarning(false);
    	isReadingPathFile = false;
	}
	
	/**
	 * Classify to which FileTypePanel the file belongs to. 
	 * Create a new scheme in FileTypePanel if no appropriate
	 * panel was found
	 * 
	 * @param f	the file to classify
	 * @param inDistibution	true: while file distribution in PswDialogView
	 * 		(add InvalidCheckBoxes, set to version 2...), false: no messages and check boxes
	 * 
	 * @return	null if directory or if file is invalid, 
	 * 			at index[0] the salt, 
	 * 			at index [1] the also parameter string (null for v1 or key file property)
	 * 			at index [2] the file identifier (v1 changes to v2 is inDistribution)
	 */
	public static byte[][] getSaltAlgosFileIDFromFile(File f, boolean inDistribution) {		
		
		String fileName = "";
		try {
			fileName = f.getAbsolutePath();
			if ( ! f.exists()) {
				TestLog.v(AttachmentHandler.class, "file does not exist...");
				if (inDistribution == true) {
					// add to invalidPanel with annotation
					InvalidPanel.addInvalidFileCheckBox(fileName, PeaProperties.getVmBridge().translate("invalid_file"));
				}
				return null;
			}
			
		} catch (Exception e) {
			TestLog.v(AttachmentHandler.class, e.getLocalizedMessage());
			if (inDistribution == true) {
				// add to invalidPanel with annotation
				InvalidPanel.addInvalidFileCheckBox(fileName, PeaProperties.getVmBridge().translate("invalid_file"));
			}
			return null;
		}
		byte[] salt = null;
		byte[] algoParam = null;
		
		//TestLog.v(this.getClass(), "classify: " + f.getAbsolutePath());
		// get the end of the file containing fileIdentifier, algoParamString, salt
		byte[] end = null;
		int saltSize = KeyDerivation.getSaltSize(); // 32
		int algoParamSize = AlgoParamHandler.getAlgoParamStringSize(); // 24
		int fileIdSize = Attachments.getFileIdentifierSize();	// 8 (end)
		
		byte[] fileIdentifier = null;
		int endSize = saltSize 	// 32
				+ algoParamSize // 24
				+ fileIdSize;	// 8 (end)
		if (f.isDirectory()) {
			// TODO include children
			TestLog.v(AttachmentHandler.class, "file is directory, no scheme...");
			return null;
		}
		try {	
			RandomAccessFile raf = new RandomAccessFile(f, "rwd" );
			long rafLen = raf.length();
			if (rafLen < fileIdSize) {
				TestLog.ve(AttachmentHandler.class, "Short file: " + fileName + ", length: " + f.length());
				TestLog.e(AttachmentHandler.class, fileName + ": " + PeaProperties.getVmBridge().translate("unsuitable_ciphertext_len"));
				raf.close();
				// add to invalidPanel with annotation
				InvalidPanel.addInvalidFileCheckBox(fileName, PeaProperties.getVmBridge().translate("unsuitable_ciphertext_len") + " - ");
				return null;
			}
			fileIdentifier = Attachments.getLastBytes(raf, fileIdSize, false);
			//TestLog.v(this.getClass(), new String(fileIdentifier) + "=?=" + new String(ve1ID));
			if (Arrays.equals(fileIdentifier, ve1ID) == true) {
				endSize = saltSize + fileIdSize;
			}				
			if (rafLen >= endSize) {
				end = Attachments.getLastBytes(raf, endSize, false);
				raf.close();
			} else {
				// TODO add to invalidPanel with annotation
				// in FileLock PEA in plain modus, this is not an error
				TestLog.ve(AttachmentHandler.class, "Short file: " + fileName + ", length: " + f.length());// 
				TestLog.e(AttachmentHandler.class, fileName + ": " + PeaProperties.getVmBridge().translate("unsuitable_ciphertext_len"));
				raf.close();
				// add to invalidPanel with annotation
				if (inDistribution == true) {
					InvalidPanel.addInvalidFileCheckBox(fileName, PeaProperties.getVmBridge().translate("unsuitable_ciphertext_len") + " - ");
				}
				return null;
			}
		} catch (IOException e) {
			TestLog.ve(AttachmentHandler.class, e.toString() + " - " + e.getLocalizedMessage());
			TestLog.e(AttachmentHandler.class, fileName + ": " + PeaProperties.getVmBridge().translate("read_write_failed"));
			// add to invalidPanel with annotation
			if (inDistribution == true) {
				InvalidPanel.addInvalidFileCheckBox(fileName, PeaProperties.getVmBridge().translate("read_write_failed") + " - ");
			}
			return null;
		} 
		salt = new byte[saltSize];		
		//System.arraycopy(end, saltSize + algoParamSize, fileIdentifier, 0, fileIdSize);
		if (Arrays.equals(fileIdentifier, ve2ID) == true) {
			//TestLog.v(this.getClass(), "new scheme");
			algoParam = new byte[algoParamSize];
			System.arraycopy(end, 0, salt, 0, saltSize);
			System.arraycopy(end, saltSize, algoParam, 0, algoParamSize);
			
			if ((salt.length + algoParam.length + fileIdentifier.length) != end.length) {
				new UnexpectedValueException("int", "lengths", "invalid").printDescription();
			}
		} else if ( Arrays.equals(fileIdentifier, ve1ID) == true) { // old version "PFACTORY"
			TestLog.v(AttachmentHandler.class, "Old version 1 scheme: " + fileName);
			// set salt
			System.arraycopy(end, 0, salt, 0, saltSize);
			// change to new version:
			if (inDistribution == true) {
				fileIdentifier = ve2ID;
				// there is no algoParam String and end is shorter
				algoParam = AlgoParamHandler.getV1Bytes();
			}
		} else {
			// PEA with key file property: 
			TestLog.v(AttachmentHandler.class, "Non-default file identifier: " +fileName);
			if ( Arrays.equals(fileIdentifier, Attachments.getFileIdentifier()) == true) { 
				algoParam = new byte[algoParamSize];
				System.arraycopy(end, 0, salt, 0, saltSize);
				System.arraycopy(end, saltSize, algoParam, 0, algoParamSize);
				if ((salt.length + algoParam.length + fileIdentifier.length) != end.length) {
					new UnexpectedValueException("int", "lengths", "invalid").printDescription();
				}
			} else {
				TestLog.ve(AttachmentHandler.class, "Invalid file identifier: " + fileName);
						//+ "\n" + Converter.bytes2hex(fileIdentifier)
						//+ "\n" + Converter.bytes2hex(Attachments.getFileIdentifier()));
				TestLog.e(AttachmentHandler.class, fileName + ": " + PeaProperties.getVmBridge().translate("not_encrypted_with_this_archive"));
				if (inDistribution == true) {
					// add to invalidPanel with annotation
					InvalidPanel.addInvalidFileCheckBox(fileName, PeaProperties.getVmBridge().translate("not_encrypted_with_this_archive"));
				}
				return null;
			}
		}
		byte[][] result = {salt, algoParam, fileIdentifier};
		if (Arrays.equals(salt, new byte[saltSize])) {
			TestLog.e(AttachmentHandler.class, "No salt was set", 5);
		}
		if (algoParam == null) {
			TestLog.e(AttachmentHandler.class, "No algorithms were set", 5);
		}
		return result;	
	}

	/**
	 * Check if content contains current used file identifier, algorithm-parameters
	 * and salt
	 * 
	 * @param input	the content to check
	 * 
	 * @return	true if file identifier, algorithm-parameters and salt 
	 * 			are equal to currently used values
	 */
	public static boolean validateSaltAlgosFileID(byte[] input) {
		int idSize = Attachments.getFileIdentifierSize();
		byte[] fileIdentifier = new byte[idSize];
		int version = -1;
		System.arraycopy(input,  input.length - idSize, fileIdentifier, 0, idSize);
		if (Arrays.equals( fileIdentifier, AttachmentHandler.getVersion1ID())) {
			version = 1;
		} else if (Arrays.equals(fileIdentifier, AttachmentHandler.getVersion2ID())) {
			version = 2;
		} else if (PeaSettings.getKeyFileProperty() == true) {
			version = 0;
		} else { // invalid
			TestLog.e(AttachmentHandler.class, "Invalid file identifier: "
					+ new String(fileIdentifier, AttachmentHandler.getASCIICharset()), 5);
			return false;
		}
		if ( ! Arrays.equals(fileIdentifier, Attachments.getFileIdentifier())) {
			TestLog.e(AttachmentHandler.class, "Wrong file identifier");
			TestLog.ve(AttachmentHandler.class, "Wrong file identifier, found " 
					+ new String(fileIdentifier, AttachmentHandler.getASCIICharset())
					+ ", but should be: " + new String(Attachments.getFileIdentifier(), AttachmentHandler.getASCIICharset())
					);
			if ( Arrays.equals(AlgoParamHandler.getV1Bytes(), PeaProperties.getAlgoParamString())){
				TestLog.v(AttachmentHandler.class, "Old version, but algorithms match current setting");
			} else {
				return false;
			}
		}
		int saltSize = KeyDerivation.getSaltSize();
		byte[] salt = new byte[saltSize];
		//TestLog.v(this.getClass(), "version: " + version);
		if (version == 2|| version == 0) {
			int algoParamSize = AlgoParamHandler.getAlgoParamStringSize();
			byte[] algoParam = new byte[algoParamSize];
			System.arraycopy(input,  input.length - idSize - algoParamSize, algoParam, 0, algoParamSize);
			if ( ! Arrays.equals(algoParam, PeaProperties.getAlgoParamString())) {
				TestLog.e(AttachmentHandler.class, "Wrong algorithm-parameters");
				TestLog.ve(AttachmentHandler.class, "Wrong algorithm-parameters, found " 
						+ new String(algoParam, AttachmentHandler.getASCIICharset())
						+ ", but should be: " + new String(PeaProperties.getAlgoParamString(), AttachmentHandler.getASCIICharset()), 
						 5);
				return false;
			}
			// check salt:
			System.arraycopy(input,  input.length - idSize - algoParamSize - saltSize, salt, 0, saltSize);
			//TestLog.v(this.getClass(), "inputLen: " + input.length + ", index: " + (input.length - idSize - algoParamSize - saltSize));
		} else {
			System.arraycopy(input,  input.length - idSize - saltSize, salt, 0, saltSize);
			//TestLog.v(this.getClass(), "inputLen: " + input.length + ", index: " + (input.length - idSize - saltSize));
		}		
		if ( ! Arrays.equals(salt, KeyDerivation.getSalt())) {
			TestLog.e(AttachmentHandler.class, "Wrong salt");
			TestLog.ve(AttachmentHandler.class, "Wrong salt, found " 
					+ Converter.bytes2hex(salt)
					+ ", but should be: " + Converter.bytes2hex(KeyDerivation.getSalt()) ,//+"\n"
					//+ 	 new String(salt, AttachmentHandler.getASCIICharset())
					//+ ", but should be: " + new String(KeyDerivation.getSalt(), AttachmentHandler.getASCIICharset()),  
					5);
			return false;
		}
		return true;
	}
	
	/**
	 * Add a cloud file with salt-algorithm combination. 
	 * Create a new scheme in FileTypePanel if no appropriate
	 * panel was found
	 * 
	 * @param fileName		the cloud file name from properties
	 * @param providerName	the provider name
	 * @param salt			salt as byte array
	 * @param algoParam		algorithms as byte array
	 * 
	 * @return	the index in fileSchemes or -1 if an error occurred
	 */
	private static int addCloudFile(String fileName, String providerName, byte[] salt, byte[] algoParam) {
		// check if this scheme is already listed:
		int index = -1;
		for (int i = 0; i < schemeMarkers.size(); i++) {
			byte[][] scheme = schemeMarkers.get(i);
			if ( Arrays.equals(scheme[1], algoParam) == true 
					&& Arrays.equals(scheme[2], salt) == true) {
				//TestLog.v(this.getClass(), "scheme found: " + i);
				index = i;
				break;
			}
		}
		String fileNameWithPath = CloudControl.getTmpFileNameForCloudFile(fileName, providerName, null);
		if (index != -1) { // already listed: add file
			FilePanelHolder.addFile(new File(fileNameWithPath), index);
		} else { // not listed: create a new scheme
			
			String checkError = AlgoParamHandler.checkString(new String(algoParam, usASCII));
			if (checkError != null) {
				TestLog.e(AttachmentHandler.class, "Invalid algorithms or parameters: " + checkError);
				return -1;
			}
			
			byte[][] scheme = {getVersion2ID(), algoParam, salt};
			//TestLog.v(this.getClass(), "new scheme fileId: " + new String(fileIdentifier, usASCII) 
			//+ ", " + new String(algoParam, usASCII) + ", " + new String(salt, usASCII));
			
			// create FileTypePanel
			boolean schemeAdded = FilePanelHolder.addScheme(new File(fileNameWithPath), algoParam, salt);
			
			if (schemeAdded == true) {
				schemeMarkers.add(scheme);// this also adds the file
				index = schemeMarkers.indexOf(scheme);
			} else {
				// add to invalidPanel with annotation
				InvalidPanel.addInvalidFileCheckBox(fileName, PeaProperties.getVmBridge().translate("invalid_file") + " - ");
				index = -1;
			}
			//TestLog.v(this.getClass(), "Added at index " + schemeMarkers.indexOf(scheme));
		}
		//TestLog.v(this.getClass(), f.getAbsolutePath() + " at scheme index " + index);
		return index;		
	}
	
	/**
	 * Classify to which FileTypePanel the file belongs to. 
	 * Create a new scheme in FileTypePanel if no appropriate
	 * panel was found
	 * 
	 * @param f	the file to classify
	 * 
	 * @return	the index in fileSchemes or -1 if file is invalid
	 */
	static int classifyFile(File f) {		
		
		try {
		String fileName = f.getAbsolutePath();
		
		byte[][] saltAndAlgos = getSaltAlgosFileIDFromFile(f, true);
		
		if (saltAndAlgos == null) {
			TestLog.ve(AttachmentHandler.class, "Invalid: " + fileName);
			TestLog.e(AttachmentHandler.class, fileName + ": " + PeaProperties.getVmBridge().translate("invalid_file"));
			return -1;
		}
		byte[] salt = saltAndAlgos[0];
		byte[] algoParam = saltAndAlgos[1];
		byte[] fileIdentifier = saltAndAlgos[2];
		if (salt == null) {
			new UnexpectedValueException("byte[]", "salt", "is null").printDescription();
			return -1;
		}
		if (algoParam == null) {
			new UnexpectedValueException("byte[]", "algoParam", "is null").printDescription();
			return -1;
		}
		if (fileIdentifier == null) {
			new UnexpectedValueException("byte[]", "fileIdentifier", "is null").printDescription();
			return -1;
		}
		if (Arrays.equals(ve1ID, fileIdentifier) || Arrays.equals(ve2ID, fileIdentifier)  ){
			if (PeaSettings.getKeyFileProperty() == true) {
				TestLog.e(AttachmentHandler.class, "Invalid file identifier: " + fileName);
				TestLog.e(AttachmentHandler.class, "This PEA has a key fileproperty and can't open files"
						+ "created from a PEA without this property ");
				InvalidPanel.addInvalidFileCheckBox(fileName, PeaProperties.getVmBridge().translate("failed") + ": " +
						PeaProperties.getVmBridge().translate("key_file_property") + " - ");
				return -1;
			}
		} else {
			// not a default fileID
			if (PeaSettings.getKeyFileProperty() == false) {
				TestLog.e(AttachmentHandler.class, "Invalid file identifier: " + fileName);
				return -1;
			} else { // key file property
				if ( ! Arrays.equals(Attachments.getFileIdentifier(), fileIdentifier)) {
					TestLog.e(AttachmentHandler.class, "Invalid file identifier: " + fileName);
					return -1;
				} else {
					//TestLog.v(AttachmentHandler.class, "valid file identifier (key property): " + f.getAbsoluteFile());
				}
			}
		}
		// check if this scheme is already listed:
		int index = -1;
		for (int i = 0; i < schemeMarkers.size(); i++) {
			byte[][] scheme = schemeMarkers.get(i);
			if ( Arrays.equals(scheme[0], fileIdentifier) == true
					&& Arrays.equals(scheme[1], algoParam) == true 
					&& Arrays.equals(scheme[2], salt) == true) {
				//TestLog.v(this.getClass(), "scheme found: " + i);
				index = i;
				break;
			}
		}
		if (index != -1) { // already listed

			//TestLog.v(this.getClass(), "added to FilePanelHolder: " + f.getAbsolutePath());
			//done in distributeFiles	FilePanelHolder.addFile(f, index);
		} else { // not listed		
			byte[][] scheme = {fileIdentifier, algoParam, salt};
			TestLog.v(AttachmentHandler.class, "new scheme: " + new String(fileIdentifier, usASCII) 
			+ ", " + new String(algoParam, usASCII) + ", " + Converter.bytes2hex(salt) );
			// check algoParam:
			String checkError = AlgoParamHandler.checkString(new String(algoParam, usASCII));
			if (checkError != null) {
				TestLog.e(AttachmentHandler.class, "Invalid algorithms or parameters: " + checkError);
				return -1;
			}
			// create FileTypePanel
			boolean schemeAdded = FilePanelHolder.addScheme(f, algoParam, salt);		
			if (schemeAdded == true) {
				schemeMarkers.add(scheme);

				index = schemeMarkers.indexOf(scheme);
			} else {
				TestLog.ve(AttachmentHandler.class, "Invalid file: " + fileName);
				// add to invalidPanel with annotation
				InvalidPanel.addInvalidFileCheckBox(fileName, PeaProperties.getVmBridge().translate("invalid_file") + " - ");
				index = -1;
			}
			//TestLog.v(this.getClass(), "Added at index " + schemeMarkers.indexOf(scheme));
		}
		//TestLog.v(AttachmentHandler.class, f.getAbsolutePath() + " at scheme index " + index);
		return index;		
		} catch (Exception e) {
			TestLog.ve(AttachmentHandler.class, "Cant classify file: " + f.getAbsolutePath(), 5);	
			TestLog.v(AttachmentHandler.class, "Cant classify file: " + f.getAbsolutePath());	
			return -1;
		}
	}
	
	/**
	 * Classify files and distribute in FileTypePanels
	 * depending on salt, algorithms and parameters
	 * 
	 * @param files	the files to classify
	 * 
	 * @return	true if view should be updated
	 */
	public static boolean distributeFiles(File[] files) {
		
		//TestLog.v(AttachmentHandler.class,"distribute: ");for(File f : files) {System.out.print(f.getAbsolutePath()+", ");}System.out.print("\n");
		boolean fileAdded = false;
		for (File file : files) {
			if (file == null) {
				TestLog.ve(AttachmentHandler.class, "File to distribute is null", 5);
				continue;
			}
			if (file.isDirectory()) {
				TestLog.v(AttachmentHandler.class, "Distribute directory...");
				File[] list = file.listFiles();
				if (list != null ) {
					// Keep value if true:
					boolean previousAdded = false;
					if (fileAdded == true) {
						previousAdded = true;
					}
					fileAdded = distributeFiles(list);
					if (fileAdded == true || previousAdded == true) {
						fileAdded = true;
					}
					//return added;
				}
			} else if (file.isFile()) {
				// check format by extension
				boolean suitableFormat = PeaControl.getDialog().checkFormat(file, true, false, false);
				if (suitableFormat == false) {
					TestLog.ve(AttachmentHandler.class, "Unsuitable format: " + file.getAbsolutePath());
					if (PswDialogView.getView() != null && PswDialogView.getView().isVisible()) {
						PswDialogView.setMessage(PeaProperties.getVmBridge().translate("format") + ": "
								+ PeaProperties.getVmBridge().translate("invalid"), 
								true, false);
					}
					InvalidPanel.addInvalidFileCheckBox(file.getAbsolutePath(), PeaProperties.getVmBridge().translate("unsuitable_format") + " - ");
					continue;
				}
			}
			int previousSchemeMarkerSize = schemeMarkers.size();
			int schemeIndex = classifyFile(file);
			//TestLog.v(this.getClass(), "index: " + schemeIndex);
			if (schemeIndex == -1) {
				// not a valid file
				TestLog.ve(AttachmentHandler.class, "Invalid file: " + file.getAbsolutePath());
				continue;
			}
			// check if a new scheme was added:
			if (previousSchemeMarkerSize < schemeMarkers.size()) {
				// a new scheme was added, files are already added
				fileAdded = true;
			} else {
			// add file to scheme:
				boolean added = FilePanelHolder.addFile(file, schemeIndex);
				if (added == true) {
					fileAdded = true;
				}
			}
		}
		if (fileAdded == true) {			
			FilePanelHolder.updateFileView();
			FilePanelHolder.updateWindow();			
		} 
		//TestLog.v(AttachmentHandler.class, "file added: " + fileAdded);
		return fileAdded;
	}

	/**
	 * Display a file chooser to add new files. 
	 * This classifies the files and calls the appropriate FileComposers
	 * addAction() method.
	 * 
	 * @param window		parent window
	 * @param message		the message to display on top of the FileChooser
	 * @param hiddenFilesCheckBox	true: add a check box to optionally 
	 * 								show hidden files
	 * 
	 * @return 	true if at least one file was added
	 */
	public final static boolean addEncryptedFiles(Object window, String message, 
			boolean hiddenFilesCheckBox, boolean checkAgain) {	
		
		PeaFileChooser chooser = new PeaFileChooser(window);//, locationOnScreen);
		if (lastDir != null){
			chooser.setCurrentDirectory(lastDir);
		}
		String[] extensions = PeaControl.getDialog().getSupportedExtensions();
		String peaType = PeaProperties.getFileType();
		if (peaType.equals("image")) {
			FileNameExtensionFilter filter = new FileNameExtensionFilter("IMAGE FILES", extensions);
			chooser.setFileFilter(filter);
		} else if (peaType.equals("calendar")) {
			FileNameExtensionFilter filter = new FileNameExtensionFilter("iCalendar", extensions);
			chooser.setFileFilter(filter);
		} else if (peaType.equals("text file")) {
			FileNameExtensionFilter filter = new FileNameExtensionFilter("TEXT FILES", extensions);
			chooser.setFileFilter(filter);
		} else if (peaType.equals("contact")) {
			FileNameExtensionFilter filter = new FileNameExtensionFilter("vCard FILES", extensions);
			chooser.setFileFilter(filter);
		} else if (peaType.equals("file")) {
			chooser.setAcceptAllFileFilterUsed(true);
		}
		
		chooser.setFileSelectionMode(PeaFileChooser.FILES_AND_DIRECTORIES);
		chooser.setMultiSelectionEnabled(true);
		chooser.customizeFileChooser(null, message, true);// no title
	    int returnVal = chooser.showOpenDialog(window);
	    if(returnVal == 0) { //FileChooser.APPROVE_OPTION == 0			    	
	    	
	    	lastDir = chooser.getCurrentDirectory();
	    	File[] files = chooser.getSelectedFiles();
	    	if (files == null || files.length == 0) {
	    		TestLog.e(AttachmentHandler.class, "no files selected");
	    		return false;
	    	}	    	
	    	boolean fileAdded = distributeFiles(chooser.getSelectedFiles());	    	
	    	if (fileAdded == true) {
	    		// update view:
	    		FilePanelHolder.updateWindow();
	    		return true;
	    	} else {
	    		return false;
	    	}
	    } else { // cancel or close
	    	return false;
	    }
	}
	
	/**
	 * Get the version of the last used file.
	 * 1 for first version, 2 for second version, 
	 * 0 for PEA with key file property
	 * 
	 * @return the version (1 or 2 or 0)
	 */
/*	public static int getVersion() {
		return version;
	}*/

	/**
	 * Set the version of the last used file.
	 * 1 for first version, 2 for second version, 
	 * 0 for PEA with key file property
	 * 
	 * @param version the version to set (1 or 2 or 0)
	 */
/*	public static void setVersion(int version) {
		AttachmentHandler.version = version;
	}*/

	/**
	 * List of markers of a scheme.
	 * A marker consists of three values: [0] fileIdentifier, 
	 * [1] algorithm-parameter, [2] salt
	 * 
	 * @return	list of scheme markers
	 */
	static ArrayList<byte[][]> getSchemeMarkers(){
		return schemeMarkers;
	}
	
	/**
	 * Get a scheme marker at index:
	 * [0] fileIdentifier, 
	 * [1] algorithm-parameter, [2] salt
	 * 
	 * @param index	the scheme marker ([0] fileIdentifier, 
	 * [1] algorithm-parameter, [2] salt)
	 * @return
	 */
	public static byte[][] getScheme(int index) {
		return schemeMarkers.get(index);
	}
	
	/**
	 * Clear the list of scheme markers
	 */
	static void clearSchemeMarkers() {
		schemeMarkers.clear();
	}

	/**
	 * Get the US ASCII charset
	 * 
	 * @return	US ASCII charset
	 */
	public static final Charset getASCIICharset() {
		return usASCII;
	}
	
	/**
	 * Get the file identifier of version 1 (PFACTOR)
	 * as an array of bytes
	 * 
	 * @return	version 1 file identifier as byte[]
	 */
	public static final byte[] getVersion1ID() {
		return ve1ID;
	}
	
	/**
	 * Get the file identifier of version 2 (PEA-VE-2)
	 * as an array of bytes
	 * 
	 * @return	version 2 file identifier as byte[]
	 */
	public static final byte[] getVersion2ID() {
		return ve2ID;
	}

	/**
	 * Indicates that program is reading the path file. 
	 * While reading the path file, no dialogs should 
	 * appear. 
	 * 
	 * @return	true if path file is currently processing
	 */
	public static boolean isReadingPathFile() {
		return isReadingPathFile;
	}
}
