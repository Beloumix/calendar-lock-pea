package cologne.eck.all_peas.vm_specific;

/*
 * Peafactory - Production of Password Encryption Archives
 * Copyright (C) 2020  Axel von dem Bruch
 * 
 * This library is free software; you can redistribute it and/or modify it 
 * under the terms of the GNU General Public License as published 
 * by the Free Software Foundation; either version 2 of the License, 
 * or (at your option) any later version.
 * This library is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
 * or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU General Public License for more details.
 * See:  http://www.gnu.org/licenses/gpl-2.0.html
 * You should have received a copy of the GNU General Public License 
 * along with this library.
 */

import java.net.InetAddress;
import java.net.MalformedURLException;
import java.net.UnknownHostException;
import java.time.LocalDateTime;
import java.time.ZoneId;

import cologne.eck.all_peas.files.FileComposer;
import cologne.eck.all_peas.files.FileTypePanel;
import cologne.eck.all_peas.gui.PeaDialog;
import cologne.eck.all_peas.gui.PswDialogView;
import cologne.eck.peafactory.crypto.HashStuff;
import cologne.eck.tools.Converter;
import cologne.eck.tools.EntropyPool;
import cologne.eck.tools.TestLog;
import cologne.eck.tools.Zeroizer;
import cologne.eck.all_peas.bridge.VMBridge;
import cologne.eck.all_peas.control.PeaControl;
import cologne.eck.all_peas.control.Translator;
import cologne.eck.all_peas.data.PeaProperties;

import java.awt.Component;
import java.awt.Window;
import java.io.File;
import java.util.ArrayList;
import java.util.Date;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

/**
 * @author axl
 *
 */
public class JREBridge implements VMBridge {
	
	private int entropyPoolCounter = 0;
	
	private Translator translator = null;
	
	public JREBridge() {
		
	}

	@Override
	public String translate(String key) {
		if (translator ==  null) {
			translator = new Translator();
		}
		return translator.translate(key, null);
	}
	
	@Override
	public void showUrgentMessage(Object owner, String message) {
		if (message == null) {			
			TestLog.e(JREBridge.class, "Empty urgent error message");
			TestLog.ve(JREBridge.class, "Empty urgent error message", 5);
			return;
		}
		if (owner == null) {
			owner = JREProperties.getMainWindow();
		}
		if ( ! (owner instanceof Window)) {
			TestLog.ve(JREBridge.class, "Window owner of urgent error message is not a Window", 5);
		}
		Component c = null;
		if (owner instanceof Component) {
			c = (Component) owner;
		}
		JOptionPane.showMessageDialog(c, message,
	               "Error", JOptionPane.ERROR_MESSAGE);
	}

	@Override
	public Object getProgressWidget(Object progressObject) {
		if (progressObject != null) {
			if (progressObject instanceof FileComposer) {
				return ((FileComposer) progressObject).getFileTypePanel();
			} else {
				return null;
			}
		} else {
			return null;
		}
	}

	@Override
	public long getAllSizeForProgress(Object progressObject) {
		if (progressObject instanceof FileComposer) {
			return ((FileComposer) progressObject).getFileModel().getAllSize();
		} else {
			return 0;
		}
	}

	@Override
	public void startProgressTask(Object ftp) {
		if (ftp != null) {
			if (ftp instanceof FileTypePanel) {
				((FileTypePanel) ftp).startProgressTask();
			} else {
			}
		}
	}

	@Override
	public void setProgress(Object progressWidget, double progress) {
		if (progressWidget != null) {
			if (progressWidget instanceof FileTypePanel) {
				((FileTypePanel) progressWidget).setProgressValue((int)progress);
			} else {
			}
		}	
	}

	@Override
	public void closeProgressTask(Object progressWidget) {
		if (progressWidget != null) {
			if (progressWidget instanceof FileTypePanel) {
				((FileTypePanel) progressWidget).closeProgressTask();
			} else {
			}
		}	
	}

	@Override
	public byte[] generatePersonalRandomBytes() {
		byte[] personalBytes = new byte[PERSONAL_BYTES_SIZE];
		int index = 0;

		// ip address of local host: 4 byte
		byte[] ipBytes = new byte[4];
		try {
			InetAddress ia = InetAddress.getLocalHost();
			ipBytes = ia.getAddress();
			if (ipBytes != null) {
				System.arraycopy(ipBytes,  0,  personalBytes, index,  ipBytes.length);
				index += ipBytes.length;
			} else {
				TestLog.e(JREBridge.class, "RandomStuff: personal bytes for random seed: ip adress null");
			}
		} catch (UnknownHostException e) {
			TestLog.e(JREBridge.class, "RandomStuff: " + e.toString() + ": personal bytes for random seed: ip adress failed");
		} catch (NullPointerException e) {
			TestLog.e(JREBridge.class, "RandomStuff: " + e.toString() + ": personal bytes for random seed: ip adress failed");
		}
		
		// fill rest with system properties
		byte[] systemProps = ( System.getProperty("user.name") + System.getProperty("user.dir")
				+ System.getProperty("os.version")  ).getBytes();//+ System.getProperty("sun.boot.class.path")	
		for (int i = 0; i < systemProps.length && index < PERSONAL_BYTES_SIZE; i++) {
			personalBytes[index++] = systemProps[i];
		}			
		return personalBytes;
	}

	@Override
	public byte[] getAndUpdateEntopyPoolTableValue(int number) {
		EntropyPool rc = EntropyPool.getInstance();

		int POOL_SIZE = EntropyPool.getPoolSize();
		
		byte[] result = new byte[number * 8];
		byte[] tableSeed = null;
		for (int i = 0; i < number; i++) {

			// get 8 bytes from one value of the table
			tableSeed = Converter.long2bytesBE(rc.getValue(true));
			entropyPoolCounter++;
			//System.out.print(" " + entropyPoolCounter);
			if (entropyPoolCounter >= POOL_SIZE){
				reseedEntropyPool(rc);
			}
			// store in result:
			System.arraycopy(tableSeed,  0, result, i * 8, tableSeed.length);
		}
		return result;
	}
	
	@Override
	public void reseedEntropyPool(Object ep) {
		//TestLog.o(JREBridge.class, "\nreseed, counter: " + entropyPoolCounter);
		
		byte[] updateBytes = new byte[EntropyPool.getPoolSize() * 8];
		int hashLen = HashStuff.getHashAlgo().getDigestSize();
		// get old value from pool to hash
		byte[] reseedBytes = Converter.longs2bytesBE(((EntropyPool)ep).getValuesToReseed());// 32 bytes			
		// hash values
		byte[] hashedBytes = HashStuff.hash(reseedBytes);
		if (hashLen >= updateBytes.length) {
			System.arraycopy(hashedBytes,  0,  updateBytes,  0,  updateBytes.length);
		} else {
			
			int index = 0;
			while (index < updateBytes.length) {		
				hashedBytes = HashStuff.hash(hashedBytes);
				System.arraycopy(hashedBytes,  0,  updateBytes,  index,  
						(index + hashLen > updateBytes.length) ? (updateBytes.length - index) : hashedBytes.length);
				index += hashLen;
			}
		}
		// convert to long[]
		long[] reseedValues = Converter.bytes2longsLE(updateBytes);
		// reseed the pool
		((EntropyPool)ep).reseedPool(reseedValues);
		Zeroizer.zero(updateBytes);
		Zeroizer.zero(reseedBytes);
		Zeroizer.zero(hashedBytes);
		Zeroizer.zero(reseedValues);
		entropyPoolCounter = 0;
	}
	
	@Override
	public final String[] getAvailableLanguages()  {
		
		if (translator == null) {
			translator = new Translator();
		}		
		return translator.getAvailableLanguages();
	}

	@Override
	public final void setLanguagesBundle(String _language) throws MalformedURLException {
		
		if (translator == null) {
			translator = new Translator();
		}
		translator.setLanguagesBundle(_language);
	}

	@Override
	public String[] askToOverwriteFiles(String[] fileNames) {
		if (fileNames == null) {
			return null;
		}
		ArrayList<String> resultList = new ArrayList<String>();
		
		JPanel panel = new JPanel();
		panel.setLayout(new BoxLayout(panel, BoxLayout.PAGE_AXIS));
		panel.add(new JLabel(PeaProperties.getVmBridge().translate("overwrite_existing_file") +":"));

		boolean existingFileFound = false;
		for (String fileName : fileNames) {
			if (fileName == null || fileName.length() == 0) {
				continue;
			}
			if (new File(fileName).exists() == true) {
				JCheckBox c = new JCheckBox(fileName);
				c.setSelected(true);
				panel.add(c);
				existingFileFound = true;
			} else {
				resultList.add(fileName);
			}
		}
		if (existingFileFound == false) {
			return fileNames;
		}
		Component comp = null;
		if (PswDialogView.getView().isVisible() == true
				&& PswDialogView.getView().isDisplayable()) {
			comp = PswDialogView.getView();
		} else {
			comp = (Component) PeaControl.getDialog().getLockFrame();
		}
		int result = PeaDialog.showQuestion(comp, panel, null, 0);
		if (result == 0) {
			Component[] comps = panel.getComponents();
			for (Component c : comps) {
				if (c instanceof JCheckBox) {
					JCheckBox check = (JCheckBox) c;
					if (check.isSelected() == true) {
						TestLog.o(JREBridge.class, "Overwrite " + check.getText());
						resultList.add(check.getText());
					} else {
						TestLog.o(JREBridge.class, "Do not overwrite " + check.getText());
					}
				}
			}
		} else if (result == 1) { // NO
			// return only files that do not exist
		} else {
			TestLog.e(JREBridge.class, "Choose files to overwrite - Process cancelled...");
			return null;
		}
		if (resultList.isEmpty()) {
			return null;
		}
		return resultList.toArray(new String[0]); 
	}

	@Override
	public boolean askToOverwriteFile(File file) {
		if (file.exists() == true) {
			JPanel panel = new JPanel();
			panel.setLayout(new BoxLayout(panel, BoxLayout.PAGE_AXIS));
			panel.add(new JLabel(PeaProperties.getVmBridge().translate("overwrite_existing_file") +":"));
			panel.add(new JLabel(file.getAbsolutePath()));
			panel.add(Box.createVerticalStrut(5));

			Component comp = null;
			if (PswDialogView.getView().isVisible() == true
					&& PswDialogView.getView().isDisplayable()) {
				comp = PswDialogView.getView();
			} else {
				comp = (Component) PeaControl.getDialog().getLockFrame();
			}
			int result = PeaDialog.showQuestion(comp, panel, null, 0);
			if (result == 0) {
				return true;	
			} else {
				return false;
			}
		} else {
			return true;
		} 
	}

	@Override
	public LocalDateTime long2LocalDateTime(long timestamp) {
        return new Date(timestamp).toInstant()
                .atZone(ZoneId.systemDefault())
                .toLocalDateTime();
	}

	@Override
	public void createTestConditions() {
		EntropyPool.getInstance().updateThread(System.nanoTime());		
		EntropyPool.getInstance().stopCollection();		
		long[] reseedValues = new long[16];
		for (int i = 0; i < reseedValues.length;i++) {
			reseedValues[i] = PeaProperties.getRandom().nextLong();
		}
		EntropyPool.getInstance().reseedPool(reseedValues);
		EntropyPool.getInstance().stopCollection();
		
	}

	@Override
	public void exitWithError(Object ownerComponent, String message, boolean unencryptedFilesLeft) {
		TestLog.ve(this.getClass().getSimpleName(), "========= Program exists because of fatal error: \n"
				+ message + "probably unencrypted files left:" + unencryptedFilesLeft + " \n",
				8);
		TestLog.e(JREBridge.class, "If you want to help to fix this bug, \nplease send this file to \npeafactory@eck.cologne");
		TestLog.o(JREBridge.class, "Program exits with fatal error. See file error_log.txt");

		PeaDialog.showExitMessage(ownerComponent, message + "\n( -> " + PeaProperties.getVmBridge().translate("file") + ":  error_log.txt)", unencryptedFilesLeft);
		System.exit(1);
	}

	@Override
	public Translator getTranslator() {
		if (translator ==  null) {
			translator = new Translator();
		}
		return translator;
	}
}
