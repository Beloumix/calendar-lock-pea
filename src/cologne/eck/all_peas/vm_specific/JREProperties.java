package cologne.eck.all_peas.vm_specific;

/*
 * Peafactory - Production of Password Encryption Archives
 * Copyright (C) 2020  Axel von dem Bruch
 * 
 * This library is free software; you can redistribute it and/or modify it 
 * under the terms of the GNU General Public License as published 
 * by the Free Software Foundation; either version 2 of the License, 
 * or (at your option) any later version.
 * This library is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
 * or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU General Public License for more details.
 * See:  http://www.gnu.org/licenses/gpl-2.0.html
 * You should have received a copy of the GNU General Public License 
 * along with this library.
 */

import java.awt.Window;
import java.net.MalformedURLException;
import java.util.Locale;

import cologne.eck.all_peas.control.PeaControl;
import cologne.eck.all_peas.data.PeaProperties;
import cologne.eck.all_peas.gui.PeaLockFrame;
import cologne.eck.all_peas.gui.PswDialogView;
import cologne.eck.tools.TestLog;
import cologne.eck.tools.UnexpectedValueException;

public class JREProperties extends PeaProperties {
	

	// special panel for PswDialogView, currently only used in PeaProperties.getFileType():
	private static Object typePanel;

	private static Window mainWindow = null;
	
	/**
	 * size and location
	 */
	private static int[] frameDim = {550, 450};// = new Dimension(550,450)
	private static int[] frameLocation = {100, 30};// = new Point(100, 30);

	private static Object languageBundle;	
	
	/**
	 * Get the current main window: PswDialogView
	 * or current instance of PeaLockFrame
	 * 
	 * @return	current main window
	 */
	public static Window getMainWindow() {
		if (mainWindow == null) {
			TestLog.ve(JREProperties.class, "Main window is null", 3);
			if (PeaControl.isPlainTextMode() == false) {
				mainWindow = PswDialogView.getView();
			} else {
				if ( PeaControl.getDialog().getLockFrame() instanceof Window) {
					mainWindow = (Window) PeaControl.getDialog().getLockFrame();
				}
			}
		}
		if (mainWindow == null) {
			Window[] windows = Window.getWindows();
			for (Window w : windows) {
				if (w.isShowing()) {
					if (w instanceof PswDialogView || w instanceof PeaLockFrame) {
						mainWindow = w;
						break;
					}
				}
			}
		}
		if (mainWindow.isShowing() == false) {
			if (PeaProperties.isCommandLineMode() == false) {
				TestLog.ve(JREProperties.class, "Main window " + mainWindow.getClass().getSimpleName() + " is not showing", 5);
			}
		}
		return mainWindow;
	}
	
	/**
	 * Set the current main window: PswDialogView
	 * or current instance of PeaLockFrame
	 * 
	 * @param win	the current main window
	 */
	public static void setMainWindow(Window win) {
		if (win != null) {
			JREProperties.mainWindow = win;
		} else {
			TestLog.ve(JREProperties.class, "Missing main winow", 5);
		}
		//TestLog.v(JREProperties.class, win.getClass().getSimpleName(), 5);
	}
	
	/**
	 * @return the typePanel
	 */
	public static Object getTypePanel() {
		return typePanel;
	}
	
	/**
	 * @param typePanel the typePanel to set
	 */
	public static void setTypePanel(Object typePanel) {
		JREProperties.typePanel = typePanel;
	}
	
	/**
	 * Get the current language bundle: Cast to ResourceBundle.
	 * If no bundle was set before, it is set with default language
	 * 
	 * @return		the current language bundle
	 */
	public final static Object getBundle(){
		if (languageBundle == null) {
			if (PeaProperties.getLastLanguage() != null) {
				try {
					PeaProperties.getVmBridge().setLanguagesBundle(
						PeaProperties.getLastLanguage());// properties language
				} catch (MalformedURLException e) {
					TestLog.e(JREProperties.class, e.toString() + " - " + e.getLocalizedMessage());
					try {
						String langCode = new Locale( System.getProperty("user.language")).getLanguage();
						PeaProperties.getVmBridge().setLanguagesBundle(langCode);// default language
						PeaProperties.setLastLanguage(langCode);
					} catch (MalformedURLException e1) {
						TestLog.e(JREProperties.class, e1.toString() + " - " + e1.getLocalizedMessage());
					}
				}
			} else {
				try {
					String langCode = new Locale( System.getProperty("user.language")).getLanguage();
					PeaProperties.getVmBridge().setLanguagesBundle(langCode);// default language
					PeaProperties.setLastLanguage(langCode);
				} catch (MalformedURLException e) {
					TestLog.e(JREProperties.class, e.toString() + " - " + e.getLocalizedMessage());
				}
			}
		}
		return languageBundle;
	}
	
	/**
	 * Set the languageBundle
	 * @param _bundle
	 */
	public final static void setBundle(Object _bundle) {
		languageBundle = _bundle;
	}
	
	/**
	 * Get frame dimension as width and height values
	 * 
	 * @return	width and height values in int[]
	 */
	public static int[] getFrameDim() {
		return frameDim;
	}
	
	/**
	 * int[] with width and height
	 * 
	 * @param whValues width and height in int[]
	 */
	public static void setFrameDim(int[] whValues) {
		if (whValues.length != 2) {
			new UnexpectedValueException("whValue", "int[]", "is invalid").printDescription();
		}
		JREProperties.frameDim = whValues;
	}
	
	/**
	 * Get location of frame as x/y
	 * 
	 * @return	x,y value of Point
	 */
	public static int[] getFrameLocation() {
		return frameLocation;
	}
	
	/**
	 * Set Location as x,y values
	 * 
	 * @param xyValues	must be instance of awt.Point
	 */
	public static void setFrameLocation(int[] xyValues) {
		if (xyValues.length != 2) {
			new UnexpectedValueException("xyValue", "int[]", "is invalid").printDescription();
		}
		JREProperties.frameLocation = xyValues;
	}
}
