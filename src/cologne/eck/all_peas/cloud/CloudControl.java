package cologne.eck.all_peas.cloud;

/*
 * Peafactory - Production of Password Encryption Archives
 * Copyright (C) 2020  Axel von dem Bruch
 * 
 * This library is free software; you can redistribute it and/or modify it 
 * under the terms of the GNU General Public License as published 
 * by the Free Software Foundation; either version 2 of the License, 
 * or (at your option) any later version.
 * This library is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
 * or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU General Public License for more details.
 * See:  http://www.gnu.org/licenses/gpl-2.0.html
 * You should have received a copy of the GNU General Public License 
 * along with this library.
 */

import java.io.File;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

import cologne.eck.all_peas.control.PeaControl;
import cologne.eck.all_peas.data.PeaProperties;
import cologne.eck.all_peas.files.CloudFilePanel;
import cologne.eck.tools.TestLog;

public class CloudControl {
	
	/**
	 * Static methods for temporary files. 
	 * 
	 * Holds static list of running cloud processes.
	 * 
	 * For every CloudFilePanel/CloudFileModel:
	 * Holds two HashMaps: one for initial files (encrypted files in password dialog)
	 * and one for used files (successfully opened, decrypted and used files).
	 * Answers the questions:
	 * Which cloud backups are used for local files? 
	 * Where to store changes? 
	 */
	private CloudFileModel cloudFileModel;// = new CloudFileModel();
	
	/**
	 * The panel in FileTypePanel to show files in the cloud as check boxes
	 */
	private CloudFilePanel cloudFilePanel;
	
	/**
	 * The default temporary directory, depends on OS:
	 * On Linux and Mac OS there is no trailing File.separator, 
	 * on Windows and Solaris there is one...
	 */
	private static String tmpDir = System.getProperty("java.io.tmpdir");
	
	/**
	 * Running upload processes: Don't exit until these processes are finished
	 * (ask the user)
	 */
	private static ArrayList<Integer> runningCloudProcesses = new ArrayList<Integer>();
	
	/**
	 * Cloud providers, that were successfully authorized in this session
	 * to avoid several authorization processes: 
	 * provider name as used in CloudProperties and the instance to
	 * authorize (for WebDAV: sardine)
	 */
	private static HashMap<String, StorageProvider> openProviders = new HashMap<String, StorageProvider>();
	
	static {
		if ( ! tmpDir.endsWith(File.separator)) { // Linux, Mac OS
			tmpDir = tmpDir + File.separator;
			TestLog.v(CloudControl.class, "Adding file separator to temp directory");
		}
	}
	
	/**
	 * Control class for CloudFilePanel and CloudFileModel
	 * 
	 * @param cfp			the CloudFilePanel
	 * @param salt			the salt as hex String
	 * @param algoParams	the algorithms-parameters as hex String
	 */
	public CloudControl(CloudFilePanel cfp, String salt, String algoParams)  {	
		
		if (salt == null) {
			TestLog.ve(CloudControl.class, "Missing salt", 5);			
		}
		if (algoParams == null) {
			TestLog.ve(CloudControl.class, "Missing algos", 5);			
		}
		//this.cloudFileModel = cfm;
		cloudFileModel = new CloudFileModel(salt, algoParams);
		createInitialCloudFileMap(salt, algoParams);

		this.cloudFilePanel = cfp;
	}


	
	//================= Helper functions: =============================================
	/**
	 * Get the provider name from the cloud file in the
	 * default tmp directory in the form:
	 * System.getProperty("java.io.tmpdir") + File.separator + provider + File.separator + fileName
	 * 
	 * @param tmpFileName	the cloud file stored in tmp folder
	 * 
	 * @return	the cloud provider name or null if this is not a tmp file
	 */
	public static String getProviderNameFromTmpFile(String tmpFileName) {
		if (tmpFileName == null) {
			TestLog.e(CloudControl.class, "Missing file name to get provider from");
			TestLog.ve(CloudControl.class, "Missing file name", 5);
			return null;
		}
		if (tmpFileName.contains(tmpDir) == false) {
			TestLog.e(CloudControl.class, "File name not in tmp directory: " + tmpFileName);
			TestLog.ve(CloudControl.class, "File name does not contain tmp directory", 5);
			return null;
		} else {
			String providerName = null;
			try {
				Path pFile = Paths.get(tmpFileName);
				if (pFile != null) {
					Path pParent = pFile.getParent();
					if (pParent != null) {
						Path pParentFileName = pParent.getFileName();
						if (pParentFileName != null) {
							Path normalizedPath = pParentFileName.normalize();
							if (normalizedPath != null) {
								providerName = normalizedPath.toString();
							} else {
								TestLog.ve(CloudControl.class, "An error occurred for " + tmpFileName, 5);
							}
						} else {
							TestLog.ve(CloudControl.class, "An error occurred for " + tmpFileName, 5);
						}
					} else {
						TestLog.ve(CloudControl.class, "An error occurred for " + tmpFileName, 5);
					}
				} else {
					TestLog.ve(CloudControl.class, "An error occurred for " + tmpFileName, 5);
				}
			} catch (Exception e) {
				TestLog.e(CloudControl.class, e.getLocalizedMessage() + ": " + tmpFileName);
				TestLog.ve(CloudControl.class, e.toString(), 5);
			}
			return providerName;
		}		
	}

	/**
	 * Get the file name from the cloud file in the
	 * default tmp directory in the form:
	 * System.getProperty("java.io.tmpdir") + File.separator + provider + File.separator + fileName
	 * 
	 * @param tmpFileName	the cloud file stored in tmp folder
	 * 
	 * @return	the file name or null
	 */
	public static String getFileNameFromTmpFile(String tmpFileName) {
		if (tmpFileName == null) {
			TestLog.e(CloudControl.class, "Missing file name ");
			TestLog.ve(CloudControl.class, "Missing file name", 5);
			return null;
		}
		String fileName = null;
		try {
			Path p = Paths.get(tmpFileName).getFileName();
			if (p != null) {
				p = p.normalize();
				if (p != null) {
					fileName = p.toString();
				} else {
					TestLog.ve(CloudControl.class, "An error occurred for " + tmpFileName, 5);
				}
			} else {
				TestLog.ve(CloudControl.class, "An error occurred for " + tmpFileName, 5);
			}
		} catch (Exception e) {
			TestLog.e(CloudControl.class, e.getLocalizedMessage() + ": " + tmpFileName);
			TestLog.ve(CloudControl.class, e.toString(), 5);
		}
		return fileName;
	}
	
	/**
	 * Get the name of the backup file in default tmp directory
	 * for a cloud file in the form:
	 * System.getProperty("java.io.tmpdir") + File.separator + provider + File.separator + fileName
	 * If the origin, local file name to upload is given, this is mapped with the tmp file name.
	 * 
	 * @param cloudFileName	the file name from the cloud without path
	 * @param providerName	the name of the provider, the file is from
	 * @param originLocalFileName	the original file name if a file is to be uploaded or null, 
	 * 								this is mapped with the tmp file name if not null
	 * 
	 * @return	the tmp file name for backup
	 */
	public static String getTmpFileNameForCloudFile(String cloudFileName, String providerName, 
			String originLocalFileName) {
		if (cloudFileName == null || providerName == null) {
			TestLog.e(CloudControl.class, "Invalid argument to create tmp file name, for" + providerName);
			TestLog.ve(CloudControl.class, "Invalid argument to create tmp file name, file: "
					+ cloudFileName + ", provider: " + providerName, 5);
			return null;
		}
		if (cloudFileName.contains(tmpDir)) {
			TestLog.ve(CloudControl.class, "cloudFileName contains tmp directory but should "
					+ "be file name only: " + cloudFileName);
			return null;
		}
		if (cloudFileName.contains(File.separator)) {
			TestLog.ve(CloudControl.class, "Filename with path, but should be without: " + cloudFileName, 5);
			cloudFileName = new File(cloudFileName).getName();
		}
		String tmpFileName = tmpDir + providerName + File.separator + cloudFileName;
		if (originLocalFileName != null && ( ! PeaProperties.getFileType().equals("file")) ) {
			if (PeaControl.getDialog().getEncryptedFileTypePanel() != null // does not work for initialization
					&& PeaControl.getDialog().getEncryptedFileTypePanel().getCloudFilePanel() != null) {
				CloudFileModel currentModel = PeaControl.getDialog() 
						.getEncryptedFileTypePanel().getCloudFilePanel().getCloudFileModel();
				currentModel.addToCloudLocalMap(originLocalFileName, tmpFileName);
			}
		}
		return tmpFileName;
	}

	/**
	 * Get the system default temporary directory
	 * System.getProperty("java.io.tmpdir")
	 * with trailing File.separator
	 * 
	 * @return system default temporary directory
	 */
	public static String getTmpDir() {
		return tmpDir;//System.getProperty("java.io.tmpdir");
	}
	
	
	//======================= Functions for initial (encrypted) cloud files: ========================

	/**
	 * Create the initial (unchecked) map of cloud file names by
	 * reading the cloud.properties
	 * @param salt			salt as hex string
	 * @param algoParams	algorithms and parameters as hex string
	 * 
	 * @return	the initial cloud file map, read from cloud.properties
	 */
	private HashMap<String, HashSet<String>> createInitialCloudFileMap(String salt, String algoParams) {

		HashMap<String, HashSet<String>> map = new HashMap<String, HashSet<String>>();	
		String[] providers = CloudPropertyHandler.getProviderNames();
		if (providers != null && providers.length > 0) {
			for (int i = 0; i < providers.length; i++) {
				String commaSeparatedFileNames = CloudPropertyHandler.getFilesFromProvider(providers[i],
						salt, algoParams);
				if (commaSeparatedFileNames != null) {
					String[] fileNames = commaSeparatedFileNames.split(",");
					if (fileNames != null) {
						map.put(providers[i], new HashSet<>(Arrays.asList(fileNames)));
					} else {
						map.put(providers[i], null);
					}
				} else {
					map.put(providers[i], null);
				}
			}	
		}
		return map;
	}
	/**
	 * Get the selected encrypted files from CloudFilePanel
	 * of the given cloud provider
	 * 
	 * @param providerName	the provider name, the files belong to
	 * 
	 * @return	the selected file names of this provider
	 */
	public String[] getSelectedInitialFilesOfProvider(String providerName) {
		if (cloudFilePanel != null) {
			return cloudFilePanel.getSelectedCloudFilesOfProvider(providerName);
		} else {
			return null;
		}
	}
	
	/**
	 * Remove a provider with all files
	 * from model and view
	 * 
	 * @param providerName	the provider to be removed
	 * 
	 * @return	true if provider was removed
	 */
	public boolean removeInitialProvider(String providerName) {
		boolean removed = cloudFileModel.removeInitialProvider(providerName);
		if (removed == true) {
			if (cloudFilePanel != null) {
				cloudFilePanel.removeProvider(providerName);
			} else {
				return false;
			}
		}
		return removed;
	}
	
	/**
	 * Get all initial cloud providers (the providers shown in password dialog)
	 * of this session
	 * 
	 * @return	the opened providers of this session
	 */
	public String[] getInitialProviders() {
		if (cloudFileModel == null) {
			//new UnexpectedValueException("CloudFileModel", "cloudFileModel", "is null").printDescription();
			TestLog.v(this.getClass(), "No cloud providers...");
			return null;
		}
		HashMap<String, HashSet<String>> map = cloudFileModel.getInitialCloudFileMap();
		if (map != null && map.size() > 0) {
			Set<String> set = map.keySet();
			if (set != null) {
				return set.toArray(new String[set.size()]);
			} else {
				return null;
			}
		} else {
			return null;
		}
	}

	/**
	 * Remove an initial (encrypted) file name of a provider. 
	 * If this file name is present, it is removed from the CloudFilePanel.
	 * 
	 * @param providerName	the provider name, the file is stored in
	 * @param fileName		the file name to remove
	 * 
	 * @return	true if, file name was removed, false is it was not present
	 */
	public boolean removeInitialFileOfProvider(String providerName, String fileName) {
		boolean removed = cloudFileModel.removeInitialFileNameFromProvider(providerName, fileName);
		if (removed == true) {
			if (cloudFilePanel != null) {
				cloudFilePanel.removeFileFromPanel(providerName, fileName);
			} else {
				return false;
			}
		}
		return removed;
	}
	
	/**
	 * Add an encrypted initial file name. If not already present
	 * in the set for the provider, a new check box in the 
	 * CloudFilePanel is added
	 * 
	 * @param providerName	the provider name, the file is stored in
	 * @param fileName		the file name to add
	 * @param select		true: select the added check box
	 * 
	 * @return 	true, if file name was not listed and a check box was added, 
	 * 			false if file name was already listed
	 */
/*	public boolean addInitialFileToProviderX(String providerName, String fileName, boolean select) {
		
		String[] fileNameArray = {fileName};
		boolean added = cloudFileModel.addInitialFileNamesToProvider(providerName, fileNameArray);
		if (added == true) {
			if (cloudFilePanel != null) {
				cloudFilePanel.addFileToPanel(providerName, fileName, select);
			} else {
				new UnexpectedValueException("CloudFilePanel", "cloudFilePanel", "is null").printDescription();
			}
		} else {
			if (cloudFilePanel != null) {
				cloudFilePanel.selectFileInPanel(providerName, fileName);
			} else {
				//new UnexpectedValueException("CloudFilePanel", "cloudFilePanel", "is null").printDescription();
			}
		}
		return added;
	}*/
	
	/** Get manually added an already downloaded cloud files
	 * 
	 * @return downloaded cloud file names
	 */
	public static HashSet<String> getDownloadedInitialFileNames(){
		return CloudFileModel.getDownloadedInitialCloudFiles();
	}
	
	/**
	 * Add encrypted initial file names that are already downloaded in tmp.
	 *
	 * @param fileNames		the file names to add
	 * 
	 * @return 	true, if file names were added, 
	 * 			false if file names were already listed
	 */
	public static boolean addDownloadedInitialFileNames(String[] fileNames) {
		TestLog.v(CloudControl.class, "files to add: " + fileNames.length);
		boolean added = CloudFileModel.addDownloadedInitialFileNamesToProvider(fileNames);
		return added;
	}
	
	/**
	 * Add encrypted initial file names. If not already present
	 * in the set for the provider, new check boxes in the 
	 * CloudFilePanel are added
	 * 
	 * @param providerName	the provider name, the file is stored in
	 * @param fileNames		the file names to add
	 * @param select		true: select the added check box
	 * 
	 * @return 	true, if file names were not listed and check boxes were added, 
	 * 			false if file names were already listed
	 */
	public boolean addInitialFileNamesToProvider(String providerName, String[] fileNames, boolean select) {
		TestLog.v(CloudControl.class, "files to add: " + fileNames.length);
		boolean added = cloudFileModel.addInitialFileNamesToProvider(providerName, fileNames);
		if (added == true) {
			if (cloudFilePanel != null) {
				for (String fileName : fileNames) {
					cloudFilePanel.addFileToPanel(providerName, fileName, select);
				}
				if (this.cloudFilePanel.getFileTypePanel().isCloudFilePanelVisible() == false) {
					this.cloudFilePanel.getFileTypePanel().setCloudFilePanel(this.cloudFilePanel);
					this.cloudFilePanel.getFileTypePanel().validate();
					this.cloudFilePanel.getFileTypePanel().revalidate();
				} else {
					cloudFilePanel.validate();
					cloudFilePanel.revalidate();
				}
			} else {
				TestLog.ve(CloudControl.class, "cloudFilePanel is null");
			}
		} else {
			TestLog.v(CloudControl.class, "no files added...");
			if (cloudFilePanel != null) {
				// select the files:
				for (String fileName : fileNames) {
					cloudFilePanel.selectFileInPanel(providerName, fileName);
				}
			} else {
				//new UnexpectedValueException("CloudFilePanel", "cloudFilePanel", "is null").printDescription();
			}
		}
		return added;
	}
	
	/**
	 * Get the initial map of cloud files (provider name - file names). 
	 * These file are unproven and only loaded from cloud properties
	 * 
	 * @return	the initial (unchecked) cloud file map (provider name - file names)
	 */
	public HashMap<String, HashSet<String>> getInitialCloudFileMap() {
		if (cloudFileModel == null) {
			TestLog.e(CloudControl.class, "Unexpected error when accessing map of cloud files");
			TestLog.ve(CloudControl.class, "Missing CloudFileModel", 5);
			return null;
		}
		return cloudFileModel.getInitialCloudFileMap();
	}

	/**
	 * Add a provider  to map for initial files if not already present
	 * 
	 * @param providerName	The provider name to add
	 * @param fileNames		The files stored in this provider or null
	 * 
	 * @return	true if file name added, false otherwise
	 */
/*	public static boolean addInitialProvider(String providerName, String[] fileNames) {
		// add file names to provider
		return cloudFileModel.addInitialFileNamesToProvider(providerName, fileNames);
	} */

	//======================= Functions for used (decrypted) cloud files: ========================
	/**
	 * Get the StorageProviders used in this session
	 * (successfully opened cloud providers)
	 * 
	 * @return	the used StorageProviders
	 */
	public Set<String> getUsedProvidersAsSet() {
		return  cloudFileModel.getInitialCloudFileMap().keySet();
	}
	
	/**
	 * Get all used (successfully opened) cloud providers
	 * of this session
	 * 
	 * @return	the opened providers of this session
	 */
	public String[] getUsedProviders() {
		if (cloudFileModel == null) {
			TestLog.v(this.getClass(), "No cloud providers...");
			return null;
		}
		HashMap<String, HashSet<String>> map = cloudFileModel.getUsedCloudFileMap();
		if (map != null && map.size() > 0) {
			Set<String> set = map.keySet();
			if (set != null) {
				return set.toArray(new String[set.size()]);
			} else {
				return null;
			}
		} else {
			return null;
		}
	}

	/**
	 * Get the successfully opened and used files of a cloud provider
	 * without the path (tmp/provider)
	 * 
	 * @param providerName	the name of the provider, the file names belong to
	 * 
	 * @return	the file names of this provider without path in tmp directory
	 */
	public String[] getUsedFileNamesOfProvider(String providerName) {
		return cloudFileModel.getUsedFilesOfProvider(providerName);
	}
	
	/**
	 * Set the successfully opened and used files of a cloud provider
	 * without the path (tmp/provider)
	 * 
	 * @param providerName	the provider name
	 * @param usedFileName	the file name without path (tmp/provider)
	 * 
	 * @return	true if set of file names was modified, 
	 * 			false if file name was already present
	 */
	public boolean addUsedFileNameToProvider(String providerName, String usedFileName) {
		return cloudFileModel.addUsedFileToProvider(providerName, usedFileName);
	}
	/**
	 * Remove an used (encrypted) file name of a provider. 
	 * If this file name is present, it is removed from the CloudFilePanel.
	 * 
	 * @param providerName	the provider name, the file is stored in
	 * @param fileName		the file name to remove
	 * 
	 * @return	true if, file name was removed, false is it was not present
	 */
	public boolean removeUsedFileOfProvider(String providerName, String fileName) {
		boolean removed = cloudFileModel.removeUsedFileNameFromProvider(providerName, fileName);
		if (removed == true) {
			if (cloudFilePanel != null) {
				cloudFilePanel.removeFileFromPanel(providerName, fileName);
			} else {
				return false;
			}
		}
		return removed;
	}
	
	/**
	 * Remove an opened cloud provider from model (TODO and view)
	 * 
	 * @param providerName	the provider name to be removed
	 * 
	 * @return	true if provider was removed
	 */
	public boolean removeUsedProvider(String providerName) {
		boolean removed = cloudFileModel.removeUsedProvider(providerName);
		if (removed == true) {
			//TODO update view
			// in lockFrame
		}
		return removed;
	}
	
	//========================= Getter & Setter ===============================================
	/**
	 * @return the cloudFilePanel
	 */
/*	public static CloudFilePanel getCloudFilePanel() {
		return cloudFilePanel;
	} */
	
	/**
	 * @param _cloudFilePanel the cloudFilePanel to set
	 */
	public void setCloudFilePanel(CloudFilePanel _cloudFilePanel) {
		this.cloudFilePanel = _cloudFilePanel;
	}
	/**
	 * Get the model for cloud files 
	 * 
	 * @return	the CloudFileModel
	 */
	public CloudFileModel getCloudFileModel() {
		return cloudFileModel;
	}

	/**
	 * Set the model for cloud files 
	 * 
	 * @param	the CloudFileModel
	 */
/*	public void setCloudFileModel(CloudFileModel _cloudFileModel) {
		if (_cloudFileModel == null) {
			TestLog.ve(CloudControl.class, "CloudFileModel to set is null",  5);
		}
		this.cloudFileModel = _cloudFileModel;
	} */

	/**
	 * Remove an integer from running cloud process list
	 * 
	 * @param processIndex	the integer (randomly generated)
	 * 
	 * @return	true if integer was removed
	 */
	public static boolean removeRunningCloudProcesses(int processIndex) {
		return  runningCloudProcesses.remove(Integer.valueOf(processIndex));
	}
	
	/**
	 * Add an integer to mark a running cloud process
	 * 
	 * @param processIndex	an integer (randomly generated)
	 * 
	 * @return	true, if integer was added
	 */
	public static boolean addRunningCloudProcesses(int processIndex) {
		return runningCloudProcesses.add(processIndex);
	}
	
	/**
	 * Get the number of running cloud processes
	 * 
	 * @return	 the number of running cloud processes
	 */
	public static int getNumberOfRunningCloudProcesses() {
		return runningCloudProcesses.size();
	}
	
	/**
	 * Get the successfully initialized cloud provider or null
	 * 
	 * @param providerName	the name of the provider as used in CloudProperties
	 * 
	 * @return	the opened provider, that need no authorization
	 * 			(for WebDAV sardine instance) or null
	 */
	public static StorageProvider getOpenProvider(String providerName) {
		return openProviders.get(providerName);
	}
	
	/**
	 * Add a provider after successfully executed authorization
	 * 
	 * @param providerName	the name of the provider as used in CloudProperties
	 * @param sp			the instance of authorization (for WebDAV sardine instance)
	 * 
	 * @return	true if the provider was already added, false otherwise
	 */
	public static boolean addOpenProvider(String providerName, StorageProvider sp) {
		if (providerName == null || sp == null) {
			TestLog.e(CloudControl.class, "Missing provider: " + providerName);
			TestLog.ve(CloudControl.class, "Missing provider: " + providerName, 5);
			return true;
		}
		StorageProvider oldP = openProviders.put(providerName, sp);
		if (oldP == null) {
			return false;
		} else {
			return true;
		}
	}
	
	/**
	 * Remove a provider after failed authorization
	 * 
	 * @param providerName	the name of the provider as used in CloudProperties
	 * 
	 * @return	true if the provider was removed, false otherwise
	 */
	public static boolean removeOpenProvider(String providerName) {

		StorageProvider oldP = openProviders.remove(providerName);
		if (oldP == null) {
			return false;
		} else {
			return true;
		}
	}
	
	/**
	 * Get all StorageProviders that were successfully opened in this session
	 * 
	 * @return	all opened StorageProviders as Collection
	 */
	public static Collection<StorageProvider> getOpenProviders() {
		return openProviders.values();
	}
}
