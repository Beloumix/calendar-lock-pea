package cologne.eck.all_peas.cloud.gui;

/*
 * Peafactory - Production of Password Encryption Archives
 * Copyright (C) 2020  Axel von dem Bruch
 * 
 * This library is free software; you can redistribute it and/or modify it 
 * under the terms of the GNU General Public License as published 
 * by the Free Software Foundation; either version 2 of the License, 
 * or (at your option) any later version.
 * This library is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
 * or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU General Public License for more details.
 * See:  http://www.gnu.org/licenses/gpl-2.0.html
 * You should have received a copy of the GNU General Public License 
 * along with this library.
 */

import java.awt.Component;
import java.awt.Dimension;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.util.ArrayList;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.KeyStroke;

import cologne.eck.all_peas.cloud.StorageProvider;
import cologne.eck.all_peas.data.PeaProperties;
import cologne.eck.all_peas.gui.CloseOnEscapeAction;
import cologne.eck.all_peas.gui.PswDialogView;
import cologne.eck.tools.TestLog;

@SuppressWarnings("serial")
public class CloudFileManager extends JDialog implements ActionListener {
	
	
	/**
	 * The panel that holds the file names and check boxes
	 */
	private JPanel filePanel;
	
	private StorageProvider provider;
	
	/**
	 * The selected files
	 */
	private String[] selectedFileNames;
	
	/**
	 * Display of files and folders in the PeaFactory folder
	 * of the cloud provider in a modal dialog
	 * 
	 * @param win			the owner window
	 * @param provider		the provider to use
	 * @param fileNames 	the file names to display
	 */	
	public CloudFileManager(Window win, StorageProvider _provider, String[] fileNames) {
		
		super(win);
		this.setModal(true);

		this.setIconImage(PswDialogView.getImage());
		this.setMinimumSize(new Dimension(400,400));
		this.provider = _provider;
		this.setTitle(provider.getProviderName() + ": " + PeaProperties.getVmBridge().translate("files"));
		
		JPanel contentPane = (JPanel) this.getContentPane();
		contentPane.setLayout(new BoxLayout(contentPane, BoxLayout.PAGE_AXIS));
		
		// avoid keyboard trap: close with escape
		this.getRootPane().getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW)
        	.put(KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), "CLOSE_ON_ESCAPE");
		this.getRootPane().getActionMap().put("CLOSE_ON_ESCAPE", new CloseOnEscapeAction() );
		
		filePanel = new JPanel();
		JScrollPane scroll = new JScrollPane(filePanel);
		scroll.setPreferredSize(new Dimension(400, 400));
		filePanel.setLayout(new BoxLayout(filePanel,BoxLayout.PAGE_AXIS));
		//contentPane.add(Box.createHorizontalStrut(10));
		if (fileNames == null || fileNames.length == 0) {
			// message as label
			contentPane.add(new JLabel(PeaProperties.getVmBridge().translate("no_valid_file_found")));
		} else {
			int len = fileNames.length;
			for (int i = 0; i < len; i++) {
				if (fileNames[i] != null && fileNames[i].length() > 0) {
					JCheckBox check = new JCheckBox(fileNames[i]);
					filePanel.add(check);
				} else {
					TestLog.ve(getClass(), fileNames[i] + "is null", 5);
				}
			}
		}
		contentPane.add(scroll);
		
		JPanel buttonPanel = new JPanel();
		buttonPanel.setLayout(new BoxLayout(buttonPanel,BoxLayout.LINE_AXIS));
		JButton okButton = new JButton("OK");
		okButton.addActionListener(this);
		okButton.setActionCommand("ok");
		buttonPanel.add(okButton);
		buttonPanel.add(Box.createHorizontalGlue());
		contentPane.add(buttonPanel);
		
		this.setLocation(win.getLocationOnScreen());
	}

	@Override
	public void actionPerformed(ActionEvent e) {

		if (e.getActionCommand().equals("ok")) {
			Component[] comps = filePanel.getComponents();
			if (comps == null || comps.length == 0) {
				this.setVisible(false);
				this.dispose();
				return;
			} else {
				int len = comps.length;
				ArrayList<String> list = new ArrayList<String>();
				for (int i = 0; i < len; i++) {
					if (comps[i] instanceof JCheckBox) {
						JCheckBox check = (JCheckBox) comps[i];
						if (check.isSelected()) {
							String text = check.getText();
							if (text != null && text.length() > 0) {							
								list.add(text);
							}
						}
					}
				}
				if (list.size() > 0) {
					selectedFileNames = list.toArray(new String[list.size()]);
				}
		        //for(int i=0; i<selectedFileNames.length;i++) TestLog.v(this.getClass(), "selected: " + selectedFileNames[i]);;
			}
			this.setVisible(false);
			this.dispose();
		}
	}
	
	/**
	 * Get the selected file names in the default folder
	 * 
	 * @return  the selected file names in the default folder PeaFactory
	 * 			without the default folder name
	 */
	public String[] getSelectedFileNames() {
		return selectedFileNames;
	}
}
