package cologne.eck.all_peas.cloud.gui;

/*
 * Peafactory - Production of Password Encryption Archives
 * Copyright (C) 2020  Axel von dem Bruch
 * 
 * This library is free software; you can redistribute it and/or modify it 
 * under the terms of the GNU General Public License as published 
 * by the Free Software Foundation; either version 2 of the License, 
 * or (at your option) any later version.
 * This library is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
 * or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU General Public License for more details.
 * See:  http://www.gnu.org/licenses/gpl-2.0.html
 * You should have received a copy of the GNU General Public License 
 * along with this library.
 */

import java.awt.Color;
import java.awt.Component;
import java.awt.Desktop;
import java.awt.Font;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.KeyEvent;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JPopupMenu;
import javax.swing.JRadioButton;
import javax.swing.JSeparator;
import javax.swing.JTextField;
import javax.swing.KeyStroke;
import javax.swing.SwingConstants;
import javax.swing.SwingWorker;
import javax.swing.WindowConstants;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.text.DefaultEditorKit;

import org.bouncycastle.crypto.BlockCipher;

import cologne.eck.all_peas.cloud.CloudControl;
import cologne.eck.all_peas.cloud.CloudPropertyHandler;
import cologne.eck.all_peas.cloud.StorageProvider;
//import cologne.eck.all_peas.cloud.StorageProvider;
import cologne.eck.all_peas.cloud.provider.WebDavProvider;
import cologne.eck.all_peas.control.PeaControl;
import cologne.eck.all_peas.data.PeaProperties;
import cologne.eck.all_peas.gui.CloseOnEscapeAction;
import cologne.eck.all_peas.gui.CursorManager;
import cologne.eck.all_peas.gui.IconManager;
import cologne.eck.all_peas.gui.PeaDialog;
//import cologne.eck.all_peas.gui.PeaDialog;
import cologne.eck.all_peas.gui.PswDialogView;
import cologne.eck.all_peas.vm_specific.JREProperties;
import cologne.eck.peafactory.crypto.CipherStuff;
import cologne.eck.peafactory.crypto.kdf.KeyDerivation;
import cologne.eck.all_peas.gui.PeaLockFrame;
import cologne.eck.tools.Comparator;
import cologne.eck.tools.Converter;
import cologne.eck.tools.TestLog;
import cologne.eck.tools.Zeroizer;


@SuppressWarnings("serial")
public class WebDavLogin extends JDialog 
implements ActionListener, DocumentListener, FocusListener, WindowListener {

	private WebDavLogin login;

	/**
	 * SwingWorker for authorization process
	 */
	private SwingWorker<String, Void> sw;
	/**
	 * Possible values: 0 success, 1 error, -1 close
	 */
	private static int result = 0;

	/**
	 * the providers with passwords stored from open dialog to store
	 * later as encrypted property
	 */
	private static HashMap <String, byte[]>  providerPswMap = new HashMap<String, byte[]>();
	/**
	 * The cipher used for encryption and decryption of the cloud passwords
	 */
	private static BlockCipher usedCipher = null;
	/**
	 * the temporary key to xor with the stored password
	 */
	private static byte[] tempKey = null;
	private WebDavProvider provider;
	private Component owner;

	private JLabel titleLabel;
	private String title;

	// the combo box for cloud providers
	private JComboBox<String> combo;

	// info and link to create an account:
	private JPanel websitePanel;

	// checkbox for manual input
	private JCheckBox manualCheck;

	// panel that holds serverField
	private JPanel serverNamePanel;
	// contains the server address to construct a WebDAV base path for some cloud software (Nextcloud, Owncloud, Seafile)
	// this is not the WebDAV base path to use!
	private JTextField serverField;
	// set notes for the server address
	private JLabel serverNoteLabel;

	// the WebDAV base path of the cloud provider
	private JTextField webDavBaseField;
	// notes for the WebDAV base path
	private JLabel webDavNoteLabel;
	// one more help text for webDavBaseField
	private JLabel webDavHelpLabel;

	// the name of the provider
	private JTextField providerField;

	// server software: Nextcloud,Owncloud, Seafile
	private JPanel radioButtonPanel1;
	private JPanel radioButtonPanel2;

	private JPanel webDavBasePanel;

	// The user name to login
	private JTextField userNameField;
	// notes for the user name
	private JLabel userNameNoteLabel;

	// the password to login
	private JPasswordField passwordField;
	// save the password or not
	//private JCheckBox passwordCheck;
	private JLabel passwordNoteLabel;

	private JPopupMenu cutCopyPasteMenu;

	// the cloud software as String: Nextcloud, Owncloud, Seafile, Unknown
	private String providerSoftware = "Unknown";

	// the font used for buttons, labels...
	private static Font font = new Font(Font.SANS_SERIF, Font.PLAIN, 12);
	// the border to display an error
	private static LineBorder errorBorder = new LineBorder( Color.RED, 2);


	// list of cloud providers and their WebDAV base paths
	private static final LinkedHashMap<String, List<String>> providerPaths = new LinkedHashMap<String, List<String>>() {{

		put(PeaProperties.getVmBridge().translate("choose") + "...", null);
		put("   Open Source: ", null);
		put("The Good Cloud (Nextcloud)", Arrays.asList("https://use02.thegood.cloud/remote.php/webdav", "https://thegood.cloud/"));
		// CloudFiles: Runs with Nextcloud path with mail address only
		// TODO put("CloudFiles (Nextcloud)", Arrays.asList("https://free.files.cnow.at/remote.php/dav/files/" + "MAIL_ADDRESS", "https://free.files.cnow.at"));
		put("oCloud (Owncloud)", Arrays.asList("https://***YOUR_CLOUD_NAME***.oCloud.de/owncloud/remote.php/webdav", "https://ocloud.de/"));
		put("OpsData (Nextcloud)",  Arrays.asList("https://cloud01.opsdata.ch/remote.php/webdav", "https://opsone.ch/en/managed-applications/nextcloud"));
		// OwnCube runs with Nextcloud path with user name only
		// TODO	put("OwnCube (Nextcloud)",  Arrays.asList("https://login.owncube.cloud/remote.php/dav/files/" + "USER_NAME", "https://owncube.com/"));
		put("OwnDrive (Nextcloud)", Arrays.asList("https://my.owndrive.com/remote.php/webdav", "https://owndrive.com/"));
		put("Tab.Digital (Nextcloud)",  Arrays.asList("https://c.tab.digital/remote.php/webdav", "https://cloud.tab.digital/"));
		put("Wint.global (Owncloud)", Arrays.asList("https://ocloud.global/remote.php/webdav", "https://wint.global/ocloud/"));
		put("Wölkli (Nextcloud)",  Arrays.asList("https://cloud.woelkli.com/remote.php/webdav", "https://woelkli.com/en"));
		put( "______________________", null);
		put("1&1 (DSL)", Arrays.asList("https://sd2dav.1und1.de"));
		put("1&1 (Webhosting)", Arrays.asList("https://webdav.office.1und1.de"));
		put("4shared", Arrays.asList("https://webdav.4shared.com/"));
		put("CloudMe", Arrays.asList("https://webdav.cloudme.com/***YOUR_USER_NAME***"));
		put("Cubby", Arrays.asList("https://webdav.cubby.com"));
		put("DriveOnWeb", Arrays.asList("https://storage.driveonweb.de/probdav"));		
		put("Freenet", Arrays.asList("https://webmail.freenet.de/webdav"));
		put("GMX", Arrays.asList("https://webdav.mc.gmx.net"));		
		put("HiDrive", Arrays.asList("https://webdav.hidrive.strato.com"));
		put("IDrive", Arrays.asList("https://dav.idrivesync.com"));
		put("MagentaCLOUD", Arrays.asList("https://webdav.magentacloud.de"));
		put("Mail.Ru", Arrays.asList("https://webdav.cloud.mail.ru"));
		put("Mailbox.org", Arrays.asList("https://dav.mailbox.org/servlet/webdav.infostore/"));
		put("Memopal", Arrays.asList("https://dav.memopal.com/"));
		put("MyDrive", Arrays.asList("https://webdav.mydrive.ch"));			
		put("OpenDrive", Arrays.asList("https://webdav.opendrive.com/"));		
		put("pCloud", Arrays.asList("https://webdav.pcloud.com"));
		put("PowerFolder", Arrays.asList("https://my.powerfolder.com/webdav"));		
		put("STACK", Arrays.asList("https://{username}.stackstorage.com/remote.php/webdav"));		
		put("Storage Made Easy (EU)", Arrays.asList("https://webdaveu.storagemadeeasy.com"));
		put("Storage Made Easy (US)", Arrays.asList("https://webdav.storagemadeeasy.com"));		
		put("Strato HiDrive", Arrays.asList("https://webdav.hidrive.strato.com"));
		put("Syncwerk", Arrays.asList("https://app.syncwerk.com/seafdav"));		
		put("WEB.DE", Arrays.asList("https://webdav.smartdrive.web.de"));
		put("Yandex.Disk", Arrays.asList("https://webdav.yandex.com"));

	}};


	/**
	 * Login into a WebDavProvider in a modal dialog
	 * and set properties
	 * 
	 * @param _owner		the window owner
	 * @param _title		the localized title: add, modify or null
	 * @param _provider		the provider or null
	 * @param _providerName		the provider name or null
	 * @param _userName		the user name or null
	 * @param _password		the password as char array or null
	 * @param _webDavBasePath	the WebDAV base path or null
	 */
	public WebDavLogin(Component _owner, String _title, WebDavProvider _provider, 
			String _providerName, String _userName, char[] _password, String _webDavBasePath) {

		login = this;
		this.owner = _owner;
		if (_provider == null) {
			//this.provider = new WebDavProvider(_providerName);
			StorageProvider sp = CloudControl.getOpenProvider(_providerName);
			if (sp == null) {
				sp = new WebDavProvider(_providerName);
			}
			if (sp instanceof WebDavProvider) {
				this.provider = (WebDavProvider) sp;
			}
		} else {
			this.provider = _provider;
		}	
		this.setModal(true);
		this.setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
		this.addWindowListener(this);
		
		// avoid keyboard trap: close with escape
		this.getRootPane().getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW)
        	.put(KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), "CLOSE_ON_ESCAPE");
		this.getRootPane().getActionMap().put("CLOSE_ON_ESCAPE", new CloseOnEscapeAction() );

		this.setIconImage(PswDialogView.getImage());
		boolean modifyModus = false;
		this.title = _title;
		if (title == null) {
			if (_providerName == null) {
				this.setTitle(PeaProperties.getVmBridge().translate("add_cloud_provider"));
			} else {
				this.setTitle(_providerName);
			}
		} else {
			this.setTitle(title);//PeaProperties.getVmBridge().translate("add_cloud_provider"));
			if (title.equals(PeaProperties.getVmBridge().translate("modify_cloud_provider"))) {
				modifyModus = true;
			}
		}

		JPanel contentPane = (JPanel) this.getContentPane();
		contentPane.setLayout(new BoxLayout(contentPane, BoxLayout.PAGE_AXIS));
		contentPane.setBorder(new EmptyBorder(10,10,10,10));

		JPanel titleLabelPanel = new JPanel();
		titleLabelPanel.setLayout(new BoxLayout(titleLabelPanel, BoxLayout.LINE_AXIS));
		titleLabel = new JLabel(PeaProperties.getVmBridge().translate("need_existing_account"));
		titleLabelPanel.add(titleLabel);
		titleLabelPanel.add(Box.createHorizontalGlue());

		contentPane.add(titleLabelPanel);

		websitePanel = new JPanel();
		contentPane.add(websitePanel);		

		contentPane.add(Box.createVerticalStrut(10));

		if (modifyModus == false){
			JPanel providerComboBoxPanel = new JPanel();
			providerComboBoxPanel.setLayout(new BoxLayout(providerComboBoxPanel, BoxLayout.LINE_AXIS));
			JLabel providerComboBoxLabel = new JLabel(PeaProperties.getVmBridge().translate("choose_provider"));
			providerComboBoxLabel.setLabelFor(combo);
			providerComboBoxLabel.setFont(font);
			String[] provNames = getProviderNames();
			combo = new JComboBox<String>(provNames);
			combo.getAccessibleContext().
		    setAccessibleDescription(
		    	    "Clicking this component selects a cloud provider.");
			combo.addActionListener(this);
			combo.setActionCommand("PROVIDER");
			// popup with SPACE key
			combo.getInputMap().put(KeyStroke.getKeyStroke(KeyEvent.VK_SPACE, 0),
	                "showPopup");
			combo.getActionMap().put("showPopup",
	                new AbstractAction() {
	                    public void actionPerformed(ActionEvent e) {
	                        combo.showPopup();
	                    }
			});
			
			providerComboBoxPanel.add(Box.createHorizontalGlue());
			providerComboBoxPanel.add(providerComboBoxLabel);
			providerComboBoxPanel.add(combo);
			providerComboBoxPanel.add(Box.createHorizontalGlue());
			contentPane.add(providerComboBoxPanel);
		}	
		JPanel manuallyPanel = new JPanel();
		manuallyPanel.setLayout(new BoxLayout(manuallyPanel, BoxLayout.LINE_AXIS));
		manualCheck = new JCheckBox(PeaProperties.getVmBridge().translate("enter_manually")); 
		manualCheck.setSelected(false);
		manualCheck.addActionListener(this);
		manualCheck.setActionCommand("manual");
		manualCheck.setFont(font);
		manuallyPanel.add(manualCheck);
		manuallyPanel.add(Box.createHorizontalGlue());
		contentPane.add(manuallyPanel);
		contentPane.add(Box.createVerticalStrut(10));

		// cut-copy-paste functionality for text fields:
		cutCopyPasteMenu = new JPopupMenu();
		Action cut = new DefaultEditorKit.CutAction();
		cut.putValue(Action.NAME, PeaProperties.getVmBridge().translate("cut"));
		cut.putValue(Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke("control X"));
		cutCopyPasteMenu.add( cut );
		Action copy = new DefaultEditorKit.CopyAction();
		copy.putValue(Action.NAME, PeaProperties.getVmBridge().translate("copy"));
		copy.putValue(Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke("control C"));
		cutCopyPasteMenu.add( copy );
		Action paste = new DefaultEditorKit.PasteAction();
		paste.putValue(Action.NAME, PeaProperties.getVmBridge().translate("paste"));
		paste.putValue(Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke("control V"));
		cutCopyPasteMenu.add( paste );
		// for password field
		JPopupMenu pasteOnly = new JPopupMenu();
		pasteOnly.add(paste);

		JPanel providerNamePanel = new JPanel();
		providerNamePanel.setLayout(new BoxLayout(providerNamePanel, BoxLayout.PAGE_AXIS));
		JLabel providerNameLabel = new JLabel(PeaProperties.getVmBridge().translate("enter_provider_name"));
		providerNameLabel.setLabelFor(providerField);
		providerNameLabel.setFont(font);
		JPanel providerLabelPanel = new JPanel();
		providerLabelPanel.setLayout(new BoxLayout(providerLabelPanel, BoxLayout.LINE_AXIS));
		providerLabelPanel.add(providerNameLabel);
		providerLabelPanel.add(Box.createHorizontalGlue());
		providerNamePanel.add(providerLabelPanel);
		providerField = new JTextField();
		if (_providerName != null) {
			providerField.setText(_providerName);
		} else {
			if (provider != null && provider.getProviderName() != null) {
				providerField.setText(provider.getProviderName());
			} else {
				providerField.setBorder(errorBorder);
			}
		}
		providerField.getDocument().putProperty("owner", providerField);
		providerField.getDocument().addDocumentListener(this);
		providerField.setComponentPopupMenu( cutCopyPasteMenu );
		JPanel providerFieldPanel = new JPanel();
		providerFieldPanel.setLayout((new BoxLayout(providerFieldPanel, BoxLayout.LINE_AXIS)));
		providerFieldPanel.add(providerField);
		providerFieldPanel.add(Box.createHorizontalGlue());
		providerNamePanel.add(providerFieldPanel);
		providerField.setToolTipText(PeaProperties.getVmBridge().translate("user_defined"));		
		contentPane.add(providerNamePanel);
		contentPane.add(Box.createVerticalStrut(10));
		if (modifyModus == true) {
			providerField.setEditable(false);
		}		

		radioButtonPanel1 = new JPanel();
		radioButtonPanel1.setLayout(new BoxLayout(radioButtonPanel1, BoxLayout.LINE_AXIS));
		JLabel providerSoftwareLabel = new JLabel(PeaProperties.getVmBridge().translate("server_runs_with"));
		providerSoftwareLabel.setFont(font);
		radioButtonPanel1.add(providerSoftwareLabel);

		JRadioButton unknownButton = new JRadioButton(PeaProperties.getVmBridge().translate("no_idea"));
		unknownButton.setFont(font);
		unknownButton.setActionCommand("Unknown");
		unknownButton.setSelected(true);

		JRadioButton nextcloudButton = new JRadioButton("Nextcloud");
		nextcloudButton.setFont(font);
		nextcloudButton.setMnemonic(KeyEvent.VK_N);
		nextcloudButton.setActionCommand("Nextcloud");

		JRadioButton owncloudButton = new JRadioButton("Owncloud");
		owncloudButton.setFont(font);
		owncloudButton.setMnemonic(KeyEvent.VK_O);
		owncloudButton.setActionCommand("Owncloud");

		JRadioButton seafileButton = new JRadioButton("Seafile");
		seafileButton.setFont(font);
		seafileButton.setMnemonic(KeyEvent.VK_S);
		seafileButton.setActionCommand("Seafile");

		//Group the radio buttons.
		ButtonGroup group = new ButtonGroup();
		group.add(nextcloudButton);
		group.add(owncloudButton);
		group.add(seafileButton);
		group.add(unknownButton);

		//Register a listener for the radio buttons.
		nextcloudButton.addActionListener(this);
		owncloudButton.addActionListener(this);
		seafileButton.addActionListener(this);
		unknownButton.addActionListener(this);

		radioButtonPanel2 = new JPanel();
		radioButtonPanel2.setLayout(new BoxLayout(radioButtonPanel2, BoxLayout.LINE_AXIS));
		radioButtonPanel1.add(unknownButton);
		radioButtonPanel1.add(Box.createHorizontalGlue());
		radioButtonPanel2.add(Box.createHorizontalStrut(20));
		radioButtonPanel2.add(nextcloudButton);
		radioButtonPanel2.add(owncloudButton);
		radioButtonPanel2.add(seafileButton);
		radioButtonPanel2.add(Box.createHorizontalGlue());
		contentPane.add(radioButtonPanel1);
		contentPane.add(radioButtonPanel2);
		contentPane.add(Box.createVerticalStrut(20));		

		JPanel userNamePanel = new JPanel();
		userNamePanel.setLayout(new BoxLayout(userNamePanel, BoxLayout.PAGE_AXIS));
		JLabel userNameLabel = new JLabel(PeaProperties.getVmBridge().translate("enter_user_name"));
		userNameLabel.setLabelFor(userNameField);
		userNameLabel.setFont(font);
		JPanel userNameLabelPanel = new JPanel();
		userNameLabelPanel.setLayout(new BoxLayout(userNameLabelPanel, BoxLayout.LINE_AXIS));
		userNameLabelPanel.add(userNameLabel);
		userNameLabelPanel.add(Box.createHorizontalGlue());
		userNamePanel.add(userNameLabelPanel);
		userNameField = new JTextField();
		if (_userName != null) {
			userNameField.setText(_userName);
		} else {
			userNameField.setBorder(errorBorder);
		}
		userNameField.getDocument().putProperty("owner", userNameField);
		userNameField.getDocument().addDocumentListener(this);
		userNameField.setComponentPopupMenu( cutCopyPasteMenu );
		JPanel userNameFieldPanel = new JPanel();
		userNameFieldPanel.setLayout((new BoxLayout(userNameFieldPanel, BoxLayout.LINE_AXIS)));
		userNameFieldPanel.add(userNameField);
		userNameFieldPanel.add(Box.createHorizontalGlue());
		userNamePanel.add(userNameFieldPanel);
		userNameField.setToolTipText(PeaProperties.getVmBridge().translate("try_user_name_or_mail_address"));
		userNameNoteLabel = new JLabel("");
		userNameNoteLabel.setFont(font);
		JPanel userNameNoteLabelPanel = new JPanel();
		userNameNoteLabelPanel.setLayout(new BoxLayout(userNameNoteLabelPanel, BoxLayout.LINE_AXIS));
		userNameNoteLabelPanel.add(userNameNoteLabel);
		userNameNoteLabelPanel.add(Box.createHorizontalGlue());
		userNamePanel.add(userNameNoteLabelPanel);
		contentPane.add(userNamePanel);
		contentPane.add(Box.createVerticalStrut(20));

		JPanel passwordPanel = new JPanel();
		passwordPanel.setLayout(new BoxLayout(passwordPanel, BoxLayout.PAGE_AXIS));
		JLabel passwordLabel = new JLabel(PeaProperties.getVmBridge().translate("enter_cloud_password") );
		passwordLabel.setLabelFor(passwordField);
		passwordLabel.setFont(new Font(Font.SANS_SERIF, Font.PLAIN, 12));
		JPanel passwordLabelPanel = new JPanel();
		passwordLabelPanel.setLayout(new BoxLayout(passwordLabelPanel, BoxLayout.LINE_AXIS));
		passwordLabelPanel.add(passwordLabel);
		passwordLabelPanel.add(Box.createHorizontalGlue());
		passwordPanel.add(passwordLabelPanel);
		JPanel passwordFieldPanel = new JPanel();
		passwordFieldPanel.setLayout(new BoxLayout(passwordFieldPanel, BoxLayout.LINE_AXIS));
		passwordField = new JPasswordField();
		if (_password != null) {
			passwordField.setText(new String(_password));
		} else {
			passwordField.setBorder(errorBorder);
		}
		passwordField.getDocument().putProperty("owner", passwordField);
		passwordField.getDocument().addDocumentListener(this);
		passwordField.setComponentPopupMenu( pasteOnly );
		passwordFieldPanel.add(passwordField);
		JCheckBox passwordHideCheck = new JCheckBox();
		passwordHideCheck.addActionListener(this);
		passwordHideCheck.setActionCommand("passwordHide");
		passwordFieldPanel.add(passwordHideCheck);
		JLabel eyeLabel = new JLabel();
		eyeLabel.setIcon(IconManager.loadIcon("eye.png", "eye",  "eye", "hide or show the password"));
		passwordFieldPanel.add(eyeLabel);
		passwordFieldPanel.add(Box.createHorizontalGlue());
		passwordPanel.add(passwordFieldPanel);

		passwordNoteLabel = new JLabel(PeaProperties.getVmBridge().translate("is_stored_encrypted"));
		passwordNoteLabel.setFont(new Font(Font.SANS_SERIF, Font.PLAIN, 12));
		passwordPanel.add(passwordNoteLabel);

		contentPane.add(passwordPanel);
		contentPane.add(Box.createVerticalStrut(20));		

		webDavBasePanel = new JPanel();
		webDavBasePanel.setLayout(new BoxLayout(webDavBasePanel, BoxLayout.PAGE_AXIS));
		JPanel webDavBaseLabelPanel = new JPanel();
		webDavBaseLabelPanel.setLayout(new BoxLayout(webDavBaseLabelPanel, BoxLayout.LINE_AXIS));
		JLabel webDavBaseLabel = new JLabel(PeaProperties.getVmBridge().translate("enter_web_dav_path"));
		webDavBaseLabel.setLabelFor(webDavBasePanel);
		webDavBaseLabel.setFont(new Font(Font.SANS_SERIF, Font.PLAIN, 12));
		JButton helpButton = new JButton(PeaProperties.getVmBridge().translate("help"));
		helpButton.setFont(font);
		helpButton.addActionListener(this);
		helpButton.setActionCommand("help");
		webDavBaseLabelPanel.add(webDavBaseLabel);
		webDavBaseLabelPanel.add(Box.createHorizontalGlue());
		webDavBaseLabelPanel.add(helpButton);
		webDavBasePanel.add(webDavBaseLabelPanel);
		webDavBaseField = new JTextField();
		if (_webDavBasePath != null) {
			webDavBaseField.setText(_webDavBasePath);
		} else {
			webDavBaseField.setText("https://webdav.example.org");
			webDavBaseField.setForeground(Color.GRAY);
		}
		JPanel webDavNoteLabelPanel = new JPanel();
		webDavNoteLabelPanel.setLayout(new BoxLayout(webDavNoteLabelPanel, BoxLayout.LINE_AXIS));
		webDavNoteLabel = new JLabel("");//Note: Generally the server publishes the base path");
		webDavNoteLabel.setFont(new Font(Font.SANS_SERIF, Font.ITALIC, 12));
		webDavNoteLabelPanel.add(webDavNoteLabel);
		webDavNoteLabelPanel.add(Box.createHorizontalGlue());
		webDavBaseField.getDocument().addDocumentListener(this);
		webDavBaseField.setComponentPopupMenu( cutCopyPasteMenu );
		webDavBaseField.getDocument().putProperty("owner", webDavBaseField);
		webDavBaseField.addFocusListener(this);

		JPanel webDavBaseFieldPanel = new JPanel();
		webDavBaseFieldPanel.setLayout(new BoxLayout(webDavBaseFieldPanel, BoxLayout.LINE_AXIS));
		webDavBaseFieldPanel.add(webDavBaseField);
		webDavBaseFieldPanel.add(Box.createHorizontalGlue());
		webDavBasePanel.add(webDavBaseFieldPanel);
		webDavBaseField.setToolTipText(PeaProperties.getVmBridge().translate("provider_publish_web_dav_path"));
		webDavBasePanel.add(webDavNoteLabelPanel);
		webDavBasePanel.add(Box.createVerticalStrut(10));

		JPanel webDavHelpLabelPanel = new JPanel();
		webDavHelpLabelPanel.setLayout(new BoxLayout(webDavHelpLabelPanel, BoxLayout.LINE_AXIS));
		webDavHelpLabel = new JLabel("");//Note: Generally the server publishes the base path");
		webDavHelpLabel.setFont(new Font(Font.SANS_SERIF, Font.ITALIC, 12));
		webDavHelpLabelPanel.add(webDavHelpLabel);
		webDavHelpLabelPanel.add(Box.createHorizontalGlue());
		webDavBasePanel.add(webDavHelpLabelPanel);

		contentPane.add(webDavBasePanel);

		serverNamePanel = new JPanel();
		serverNamePanel.setLayout(new BoxLayout(serverNamePanel, BoxLayout.PAGE_AXIS));	
		//webDavBaseLabel.setAlignmentX(RIGHT_ALIGNMENT);
		webDavBasePanel.add(serverNamePanel);

		JPanel buttonPanel = new JPanel();
		buttonPanel.setLayout(new BoxLayout(buttonPanel,BoxLayout.LINE_AXIS));
		JButton okButton = new JButton("OK");
		okButton.addActionListener(this);
		okButton.setActionCommand("ok");
		buttonPanel.add(Box.createHorizontalGlue());
		buttonPanel.add(okButton);
		contentPane.add(buttonPanel);

		radioButtonPanel1.setVisible(false);
		radioButtonPanel2.setVisible(false);
		webDavBasePanel.setVisible(false);

		this.pack();
		this.setLocationRelativeTo(_owner);
	}

	/**
	 * The WebDavLogin must have been created before calling this function.
	 * WebDavLogin should always be called by this method
	 * to get a resulting value. If the authorization succeeds, the login
	 * parameters are stored in cloud.properties. The password is then stored
	 * encrypted whether in cloud.properties or temporary stored. 
	 * 
	 * @return	result of login process: 
	 * 			-1 cancelled, 0 success, 1 failed
	 */
	public int showLoginDialog() {
		// Close all existing error dialogs:
		Window[] windows = Window.getWindows();
		for (Window window : windows) {
			if (window instanceof JDialog) {
				JDialog dialog = (JDialog) window;
				if (dialog.getContentPane().getComponentCount() > 0
						&& dialog.getContentPane().getComponent(0) instanceof JOptionPane){
					dialog.dispose();
				}
			}
		}
		if (login == null) {
			TestLog.ve(WebDavLogin.class, "Missing login dialog", 5);
		} else {
			login.setVisible(true);
		}		
		return result;
	}

	/**
	 * Get all available provider names
	 * 
	 * @return	provider names of WebDAV providers
	 */
	private static String[] getProviderNames() {
		return providerPaths.keySet().toArray(new String[providerPaths.size()]);
	}

	/**
	 * Get the WebDAV base path of a provider
	 * 
	 * @param providerName	the name of the provider
	 * 
	 * @return	the WebDAV base path of this provider
	 */
	private static String getBasePath(String providerName) {
		List<String> l = providerPaths.get(providerName);
		if (l != null && l.size() > 0) {
			return providerPaths.get(providerName).get(0);
		} else {
			TestLog.ve(WebDavLogin.class, "Invalid list of provider " + providerName);
			return null;
		}
	}

	/**
	 * Get the web site to create an account of a provider
	 * 
	 * @param providerName	the name of the provider
	 * 
	 * @return	the web site of this provider
	 */
	private static String getWebsite(String providerName) {
		List<String> l = providerPaths.get(providerName);
		if (l == null) {
			TestLog.ve(WebDavLogin.class, "Invalid list of provider " + providerName);
			return null;
		}
		int listSize = l.size();
		if (listSize == 0) {
			TestLog.ve(WebDavLogin.class, "Empty list of provider " + providerName);
			return null;
		} else if (listSize == 1) { // closed source providers
			return null;
		} else if (listSize > 1) {
			return l.get(1);
		} else {
			TestLog.ve(WebDavLogin.class, "Invalid list of provider " + providerName);
			return null;
		}
	}

	/**
	 * Get all cloud login password from PEA open dialog
	 * as password properties in the cloud property file. 
	 * The password are encrypted by a temporary key and must
	 * be encrypted by the session key, derived from the
	 * PEA password
	 * 
	 * @return	a HashMap with providerName and decrypted keys
	 * 			as byte array
	 */
	public static HashMap<String, byte[]> getPasswordsFromNewProviderDialog() {
		if (providerPswMap.isEmpty()) {
			return null;
		}
		if (tempKey == null) { // this should never happen
			TestLog.e(WebDavLogin.class,  "is null",5);
			return null;
		} else {
			// check for program bug
			if (PeaProperties.isTestMode()){
				Comparator.checkNullVector(tempKey);
			}
		}
		HashMap <String, byte[]>  plainProviderPswMap = new HashMap<String, byte[]>();
		// iterate over map and set all stored passwords as properties with session key
		// get current cipher to reset later
		BlockCipher lastCipher = CipherStuff.getCipherAlgo();
		if (usedCipher != null) {
			CipherStuff.setCipherAlgo(usedCipher);
			for (Iterator<Map.Entry<String, byte[]>> entries = providerPswMap.entrySet().iterator(); entries.hasNext(); ) {
				Map.Entry<String, byte[]> entry = entries.next();
				String provider = entry.getKey();
				byte[] pswBytes = entry.getValue();
				// decrypt by stored temporary key
				byte[] plainPswBytes = CipherStuff.processCTR(false, pswBytes, tempKey, false);
				plainProviderPswMap.put(provider, plainPswBytes);
			}
			// reset cipher:
			CipherStuff.setCipherAlgo(lastCipher);
		} else {
			TestLog.e(WebDavLogin.class, "Missing block cipher for decryption", 5);
		}
		return plainProviderPswMap;
	}

	/**
	 * Get a manually added and temporary stored password
	 * for a provider
	 * 
	 * @param providerName	the name of the provider
	 * 
	 * @return	the plain text password as array of bytes or null
	 */
	public static byte[] getTemporaryStoredPassword(String providerName) {
		if (providerPswMap.isEmpty()) {
			return null;
		}
		if (providerName == null) {
			TestLog.e(WebDavLogin.class, "Unexpected error");
			TestLog.ve(WebDavLogin.class, "Missing provider name", 5);
			return null;
		}
		byte[] encryptedPswBytes = providerPswMap.get(providerName);
		if (encryptedPswBytes != null) {
			// get current cipher to reset later
			BlockCipher lastCipher = CipherStuff.getCipherAlgo();
			// avoid exitWithError:
			if (CipherStuff.getKeySize() == tempKey.length) {
				byte[] plainPswBytes = CipherStuff.processCTR(false, encryptedPswBytes, tempKey, false);
				// reset cipher:
				CipherStuff.setCipherAlgo(lastCipher);
				return plainPswBytes;
			} else {
				TestLog.e(WebDavLogin.class, "Invalid key size of temporary key");
				TestLog.ve(WebDavLogin.class, "Invalid key size of temporary key for provider " + providerName, 8);
				return null;
			}
		} else {
			TestLog.v(WebDavLogin.class, "No temporary stored password for " + providerName);
			return null;
		}		
	}

	/**
	 * Store a manually typed password for a provider in encrypted form. 
	 * The password is zeroized.
	 * 
	 * @param providerName		the name of the provider
	 * @param plainPswBytes		the plain password as byte array
	 * 
	 * @return	true if there was no password for this provider before, 
	 * 			false if an already existed password was overwritten
	 */
	public static synchronized boolean storePassword(String providerName, byte[] plainPswBytes) {
		// get current cipher to reset later
		BlockCipher lastCipher = CipherStuff.getCipherAlgo();
		if (usedCipher == null) {
			// cipher may change:
			usedCipher = CipherStuff.getCipherAlgo();
		} else {
			// set this cipher:
			CipherStuff.setCipherAlgo(usedCipher);
		}
		if (tempKey == null) {
			tempKey = new byte[CipherStuff.getKeySize()];//.getCipherAlgo().getBlockSize()];
			PeaProperties.getSecureRandom().nextBytes(tempKey);
		}						
		byte[] pswBytes = CipherStuff.processCTR(true, plainPswBytes, tempKey, false);
		Zeroizer.zero(plainPswBytes);
		// reset cipher:
		CipherStuff.setCipherAlgo(lastCipher);		
		TestLog.v(WebDavLogin.class, "Store manually typed password in encrypted form for " + providerName);
		byte[] previousKey = providerPswMap.put(providerName, pswBytes);
		if (previousKey == null) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * Get the last used WebDavProvider
	 * 
	 * @return	the last used provider
	 */
	public StorageProvider getProvider() {
		return provider;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		String command = e.getActionCommand();
		if (command.equals("ok")) {

			sw = new WebDavLoginWorker();
			sw.execute();

		} else if (command.equals("manual")) {
			if (manualCheck.isSelected()) {			
				radioButtonPanel1.setVisible(true);
				radioButtonPanel2.setVisible(true);
				webDavBasePanel.setVisible(true);
				this.pack();
			} else {
				radioButtonPanel1.setVisible(false);
				radioButtonPanel2.setVisible(false);
				webDavBasePanel.setVisible(false);
				this.pack();
			}

		} else if (command.equals("passwordHide")) {

			JCheckBox check = (JCheckBox) e.getSource();
			if (check.isSelected() == true) {
				passwordField.setEchoChar((char) 0);
			} else {
				passwordField.setEchoChar('*');
			}
		} else if (command.equals("help")) {
			if (serverNamePanel.getComponentCount() < 2) {
				serverNamePanel.setBorder(new EmptyBorder(0, 20, 0,20));
				webDavNoteLabel.setText(PeaProperties.getVmBridge().translate("provider_publish_web_dav_path"));
				//webDavNoteLabel.setForeground(Color.RED);

				if (providerSoftware.equals("Unknown")) { // no further help available...
					this.pack();
					return;
				} else {

					serverNamePanel.setLayout(new BoxLayout(serverNamePanel, BoxLayout.PAGE_AXIS));
					webDavHelpLabel.setText(PeaProperties.getVmBridge().translate("not_found"));
					JLabel serverLabel = new JLabel(PeaProperties.getVmBridge().translate("enter_cloud_server_address") );
					serverLabel.setFont(new Font(Font.SANS_SERIF, Font.PLAIN, 12));
					JPanel serverLabelPanel = new JPanel();
					serverLabelPanel.setLayout(new BoxLayout(serverLabelPanel, BoxLayout.LINE_AXIS));
					serverLabelPanel.add(Box.createHorizontalStrut(20));
					serverLabelPanel.add(serverLabel);
					serverLabelPanel.add(Box.createHorizontalGlue());
					serverNamePanel.add(serverLabelPanel);
					serverField = new JTextField();
					serverField.setText("https//:example.org");
					serverField.setForeground(Color.GRAY);
					serverField.getDocument().putProperty("owner", serverField);
					serverField.getDocument().addDocumentListener(this);
					serverField.setComponentPopupMenu( cutCopyPasteMenu );
					serverField.addFocusListener(this);
					JPanel serverFieldPanel = new JPanel();
					serverFieldPanel.setLayout(new BoxLayout(serverFieldPanel, BoxLayout.LINE_AXIS));
					serverFieldPanel.add(Box.createHorizontalStrut(20));
					serverFieldPanel.add(serverField);
					serverFieldPanel.add(Box.createHorizontalGlue());
					serverNamePanel.add(serverFieldPanel);
					serverNoteLabel = new JLabel("");
					JPanel serverNotePanel = new JPanel();
					serverNotePanel.setLayout(new BoxLayout(serverNotePanel, BoxLayout.LINE_AXIS));
					serverNotePanel.add(Box.createHorizontalStrut(20));
					serverNotePanel.add(serverNoteLabel);
					serverNotePanel.add(Box.createHorizontalGlue());
					serverNamePanel.add(serverNotePanel);
					serverNamePanel.add(Box.createVerticalStrut(20)); 				
					this.pack();
				}				
			}

		} else if (command.startsWith("LINK")){

			command = command.substring(4, command.length());
			URI uri = null;
			try {
				uri = new URI(command);
			} catch (URISyntaxException e1) {
				TestLog.e(WebDavLogin.class, "Invalid URI: " + command);
				return;
			}
			if (Desktop.isDesktopSupported() == true) {
				Desktop d = Desktop.getDesktop();
				if (d == null) {
					TestLog.e(WebDavLogin.class, "Invalid Desktop... ");
					return;
				} else {
					try {
						d.browse(uri);
					} catch (IOException e1) {
						TestLog.e(WebDavLogin.class, e1.getLocalizedMessage());
						return;
					}
				}			

			} else {
				TestLog.e(WebDavLogin.class, "Desktop is not supported... ");
				return;
			}

		} else if (command.equals("PROVIDER")) {
			radioButtonPanel1.setVisible(false);
			radioButtonPanel2.setVisible(false);
			webDavBasePanel.setVisible(false);

			String provName = (String) combo.getSelectedItem();

			websitePanel.removeAll();
			if (this.title.equals(PeaProperties.getVmBridge().translate("add_cloud_provider"))) {

				// show link to web site to create an account: 
				if (provName != null && getWebsite(provName) != null) {

					websitePanel.setLayout(new BoxLayout(websitePanel, BoxLayout.PAGE_AXIS));
					JPanel sitePanel = new JPanel();

					JLabel createAccountLabel = new JLabel(PeaProperties.getVmBridge().translate("go_to") );
					sitePanel.add(createAccountLabel);
					//websitePanel.add(createAccountLabel);
					JButton linkButton = new JButton(PeaProperties.getVmBridge().translate("web"));
					linkButton.addActionListener(this);
					linkButton.setActionCommand("LINK" + getWebsite(provName));
					sitePanel.add(linkButton);
					websitePanel.add(sitePanel);
					JTextField linkField = new JTextField();
					//linkField.setBackground(websitePanel.getBackground());
					linkField.setText(getWebsite(provName));
					linkField.setEditable(false);
					websitePanel.add(linkField);		
					websitePanel.add(Box.createVerticalStrut(5));
					websitePanel.add(new JSeparator(SwingConstants.HORIZONTAL));
					websitePanel.add(Box.createVerticalStrut(15));
				}
			}
			this.pack();		

			if (providerPaths.get(provName) == null) {

			} else {
				//TestLog.v(WebDavLogin.class, "provider name: " + provName);
				webDavBaseField.setText(getBasePath(provName));
			}
		} else if (command.equals("Unknown")) {
			providerSoftware = "Unknown";
		} else if (command.equals("Nextcloud")) {
			providerSoftware = "Nextcloud";
		} else if (command.equals("Owncloud")) {
			providerSoftware = "Owncloud";
		} else if (command.equals("Seafile")) {
			providerSoftware = "Seafile";
		}
	}

	@Override
	public void changedUpdate(DocumentEvent cue) {	}

	private void handleDocumentEvents(DocumentEvent iue) {
		// Check serverField and webDavBaseField for valid URL
		// Set webDavBaseField if serverField contains a valid URL
		JTextField comp = (JTextField) iue.getDocument().getProperty("owner");
		if (comp.equals(serverField)) {
			//TestLog.v(WebDavLogin.class, "serverField");
			if ( ! serverField.getText().isEmpty() ) {
				String text = serverField.getText();
				try {
					@SuppressWarnings("unused")
					URL url = new URL(serverField.getText());
				} catch (MalformedURLException e1) {
					//TestLog.e(WebDavLogin.class, "Not a valid URL: " + text);
					serverNoteLabel.setText(PeaProperties.getVmBridge().translate("enter_valid_web_address"));
					serverNoteLabel.setForeground(Color.RED);
					serverField.setBorder(errorBorder);
					this.pack();
					return;
				}
				serverNoteLabel.setText("");
				serverField.setBorder(null);
				if (text.contains(".") ) {
					// check for index page and remove
					//String basePart = text.substring(0, text.lastIndexOf('.'));
					//TestLog.v(WebDavLogin.class, "basePart: " + basePart);
					if (text.endsWith(".html") || text.endsWith(".htm") 
							|| text.endsWith(".shtml") || text.endsWith(".shtm")
							|| text.endsWith(".php") || text.endsWith(".asp")
							|| text.endsWith(".jsp")) {
						text = text.substring(0, text.lastIndexOf("/"));
						//TestLog.v(WebDavLogin.class, "text: " + text);
					}
				}
				text = text.trim();
				if (! text.endsWith("/")) {
					text = text + "/";
				}
				// Scheme for Nextcloud: requires user name
				// https://{host}/{path}/remote.php/dav/files/{username}
				// Most Nextcloud providers work also with Owncloud path
				// TODO providers such as OwnCube only run with dav/files/{username} 
				// TODO providers such as CloudFiles run only with  dav/files/{mail-address}
				if (providerSoftware.equals("Owncloud")
						|| providerSoftware.equals("Nextcloud")) { 
					// https://{host}/{path}/remote.php/webdav
					String webDavBasePath = text.trim() + "remote.php/webdav";
					webDavBaseField.setText(webDavBasePath);
					webDavBaseField.setForeground(Color.BLACK);
					webDavBaseField.setBorder(null);
					webDavNoteLabel.setText("");

				} else if (providerSoftware.equals("Seafile")) {
					// https://{host}/{path}/seafdav
					String webDavBasePath = text.trim() + "seafdav";
					webDavBaseField.setText(webDavBasePath);
					webDavBaseField.setForeground(Color.BLACK);
					webDavBaseField.setBorder(null);
					webDavNoteLabel.setText("");

				}
			} else { // serverField empty
				if (webDavBaseField.getText().isEmpty()) {
					serverField.setBorder(errorBorder);
					webDavBaseField.setBorder(errorBorder);
				}
			}
			//this.pack();
		} else if (comp.equals(webDavBaseField)) {
			//TestLog.v(WebDavLogin.class, "webDavBaseField");
			if ( ! webDavBaseField.getText().isEmpty() ) {
				String text = webDavBaseField.getText();
				try {
					@SuppressWarnings("unused")
					URL url = new URL(text);
				} catch (MalformedURLException e1) {
					//TestLog.e(WebDavLogin.class, "Not a valid URL: " + text);
					webDavNoteLabel.setText(PeaProperties.getVmBridge().translate("enter_valid_web_address"));
					webDavNoteLabel.setForeground(Color.RED);
					webDavBaseField.setBorder(errorBorder);
					return;
				}
				webDavBaseField.setBorder(null);
				webDavBaseField.setForeground(Color.BLACK);
				webDavNoteLabel.setText("");
				// Set the provider name if not present
				if (! text.equals("https://webdav.example.org")) {
					String provName = text.substring(text.indexOf("://") + 3);
					if (provName.startsWith("webdav") || provName.startsWith("dav")
							|| provName.startsWith("cloud")
							|| provName.startsWith(userNameField.getText().toLowerCase())){
						if (provName.contains(".")) {
							provName = provName.substring(provName.indexOf(".") + 1);
						}
					}
					if (provName.contains("/")) {
						provName = provName.substring(0, provName.indexOf("/"));
					}
					if (provName.contains(".")) {
						provName = provName.substring(0, provName.indexOf("."));
					}
					if (provName.contains("_")) { // underscore is used in properties
						provName = provName.replace('_', '-');							
					}
					//TestLog.v(WebDavLogin.class, "Set provider name " + provName);
					providerField.setText(provName);
				}
				if (serverField != null) {
					serverField.setBorder(null);
					serverNoteLabel.setText("");
				}
			}
		} else if (comp.equals(userNameField)) {

			if ( ! userNameField.getText().isEmpty() ) {

				userNameField.setBorder(null);
				userNameNoteLabel.setText("");
				// construct Nextcloud base path...
				if (providerSoftware.equals("Nextcloud") 
						&& serverField != null 
						&& ! serverField.getText().isEmpty() // Nextcloud
						&& serverField.getBorder() == null) { // valid URL
					// construct the WebDAV base path for Nextcloud:
					String userName = userNameField.getText();
					String text = serverField.getText();
					if (text.contains(".") ) {
						// check for index page and remove
						String basePart = text.substring(0, text.lastIndexOf('.'));
						//TestLog.v(WebDavLogin.class, "basePart: " + basePart);
						if (basePart.endsWith("index")  
								|| basePart.endsWith("home") || basePart.endsWith("default")) {
							text = text.substring(0, text.lastIndexOf("/"));
							//TestLog.v(WebDavLogin.class, "text: " + text);
						}
					}
					if (! text.endsWith("/")) {
						text = text + "/";
					}
					String webDavBasePath = text.trim() + "remote.php/dav/files/" + userName.trim();
					webDavBaseField.setText(webDavBasePath);
					webDavBaseField.setForeground(Color.BLACK);
					webDavBaseField.setBorder(null);
					webDavNoteLabel.setText("");
					this.pack();
				}
			} else {
				userNameField.setBorder(errorBorder);
			}
		} else if (comp.equals(passwordField)) {
			if ( passwordField.getPassword().length != 0 ) {
				passwordField.setBorder(null);
			} else {
				passwordField.setBorder(errorBorder);
			}
		} else if (comp.equals(providerField)) {
			if (! providerField.getText().isEmpty() ) {
				providerField.setBorder(null);
			} else {
				providerField.setBorder(errorBorder);
			}
		}
	}

	@Override
	public void insertUpdate(DocumentEvent iue) {
		handleDocumentEvents(iue);
	}

	@Override
	public void removeUpdate(DocumentEvent rue) {
		handleDocumentEvents(rue);
	}

	/**
	 * Handle the placeholder text and color: 
	 * Remove the placeholder text and set color to black
	 */
	@Override
	public void focusGained(FocusEvent fge) {
		// Set or reset the placeholder text
		if (fge.getComponent().equals(serverField)) {
			if (serverField.getText().equals("https//:example.org")) {
				serverField.setText("");
				serverField.setForeground(Color.BLACK);
			}
		} else if (fge.getComponent().equals(webDavBaseField)) {
			if (webDavBaseField.getText().equals("https://webdav.example.org")) {
				webDavBaseField.setText("");
				webDavBaseField.setForeground(Color.BLACK);
			}
		}
	}

	/**
	 * Handle the placeholder text and color: 
	 * Set the placeholder text and set color to gray
	 */
	@Override
	public void focusLost(FocusEvent fle) {	}

	/**
	 * Inner class for cloud operations
	 */
	private class WebDavLoginWorker extends SwingWorker<String, Void> {

		@Override
		protected String doInBackground() throws Exception { 

			try {
				login.setCursor(CursorManager.getWaitCursor());

				userNameField.setBorder(null);
				userNameNoteLabel.setText("");
				providerField.setBorder(null);
				passwordField.setBorder(null);
				webDavBaseField.setBorder(null);
				if (serverField != null) {
					serverField.setBorder(null);
				}			
				boolean error = false;
				if (providerField.getText().equals("")) {
					providerField.setBorder(errorBorder);
					error = true;
				}
				if (userNameField.getText().equals("")) {
					userNameField.setBorder(errorBorder);
					error = true;
				}
				if (passwordField.getPassword().length == 0) {
					passwordField.setBorder(errorBorder);
					error = true;
				}
				if (webDavBaseField.getText().equals("") ||
						webDavBaseField.getText().equals("https://webdav.example.org")) {
					webDavBaseField.setBorder(errorBorder);
					error = true;
				} else {
					try {
						@SuppressWarnings("unused")
						URL url = new URL(webDavBaseField.getText());
					} catch (MalformedURLException e1) {
						TestLog.e(WebDavLogin.class, "Invalid WebDAV pasth: " + webDavBaseField.getText());
						webDavBaseField.setBorder(errorBorder);
						webDavNoteLabel.setText(PeaProperties.getVmBridge().translate("provider_publish_web_dav_path"));
						webDavNoteLabel.setForeground(Color.RED);
						error = true;
					}
				}			
				if (error == false) {
					String providerName = providerField.getText();
					if (providerName.contains("_")) { // underscore is used in properties
						providerName = providerName.replace('_', '-');							
					}
					provider.setProviderName(providerName);
					provider.setWebDavBasePath(webDavBaseField.getText() 
							+ (webDavBaseField.getText().endsWith("/")? "" : "/"));
					String authError = null;

					if (title.equals(PeaProperties.getVmBridge().translate("modify_cloud_provider"))) {
						// do not use opened provider
						authError = provider.updateAuthorization(
								userNameField.getText(), passwordField.getPassword(), null);
					} else {
						// do not open new provider, if provider was already opened
						authError = provider.authorize(
								userNameField.getText(), passwordField.getPassword(), null);
					}
					if (authError == null) {
						if (JREProperties.getMainWindow() instanceof PeaLockFrame) { // PeaLockFrame
							((PeaLockFrame)PeaControl.getDialog().getLockFrame()).setMessage(PeaProperties.getVmBridge().translate("successfully_authorized"), false);
							//Properties props = new Properties();
							String propError = CloudPropertyHandler.setNewProvider(provider.getProviderName(), userNameField.getText(), webDavBaseField.getText());
							if (propError != null) {
								TestLog.e(WebDavLogin.class, "Could not save login values for: " + provider.getProviderName());
								TestLog.ve(WebDavLogin.class, propError, 5);
								return PeaProperties.getVmBridge().translate("unexpected_error") + "\n" + propError;
							} else {
								((PeaLockFrame)PeaControl.getDialog().getLockFrame())
								.setMessage(PeaProperties.getVmBridge().translate("provider_added") 
										+ ": " + provider.getProviderName(), false);
							}
							char[] psw = passwordField.getPassword();
							passwordField.setText("");
							// check if sub set properties exist
							String saltAsHex = Converter.bytes2hex(KeyDerivation.getSalt());
							String algoParam = Converter.bytes2hex(PeaProperties.getAlgoParamString());
							// the extension/marker for the sub set (0 for default set), 
							// or -1 (provider with this salt/algos does not exist), or -2 (unexpected error)
							int ext = CloudPropertyHandler.getSubSetExtension(providerName, 
									saltAsHex, 
									algoParam, null);
							//TestLog.v(WebDavLogin.class, "ext: " + ext);
							if (ext >= 0) { // provider subset exists: store new password
								if (CipherStuff.getSessionKeyCrypt().getKey() != null) {
									CloudPropertyHandler.storeEncryptedPasswordFromProvider(
											CloudPropertyHandler.encryptPassword(new String(psw)), providerName, saltAsHex, algoParam, null);
									Zeroizer.zero(psw);
								} else {
									WebDavLogin.storePassword(providerName, Converter.chars2bytes(psw));
								}
							} else if (ext == -1 || ext == -2) { // sub set does not exist or error: create subset
								if (ext == -2) {
									TestLog.ve(WebDavLogin.class, "Error occurred when check for scheme, try to add...");
								}
								int x = CloudPropertyHandler.createSubSet(providerName, 
										Converter.bytes2hex(KeyDerivation.getSalt()), 
										Converter.bytes2hex(PeaProperties.getAlgoParamString()),
										new String(psw), 
										null);
								Zeroizer.zero(psw);
								if (x == -1) {// failed
									TestLog.e(this.getClass(),  "Could not create sub set for provider " + providerName);
								} else {
									((PeaLockFrame)PeaControl.getDialog().getLockFrame())
									.setMessage(PeaProperties.getVmBridge().translate("provider_added") 
											+ ": " + provider.getProviderName(), false);
								}
							} 

						} else { // PswDialogView open or add

							// keep the password: create a temp key if not already created
							char[] psw = passwordField.getPassword();
							passwordField.setText("");
							byte[] plainPswBytes = Converter.chars2bytes(psw);	
							if (tempKey == null) {
								tempKey = new byte[CipherStuff.getKeySize()];//.getCipherAlgo().getBlockSize()];
								PeaProperties.getSecureRandom().nextBytes(tempKey);
							}						
							WebDavLogin.storePassword(providerName, plainPswBytes);
							// store properties:
							String propError = CloudPropertyHandler.setNewProvider( 
									providerName,
									userNameField.getText(), 
									//null,//CloudPropertyHandler.encryptPassword(psw),
									webDavBaseField.getText());							
							if (propError != null) {
								TestLog.e(WebDavLogin.class, "Could not save login values for: " + provider.getProviderName());
								TestLog.ve(getClass(), propError);
								PeaDialog.showMessage(owner, 
										PeaProperties.getVmBridge().translate("unexpected_error")
										+ "\n" + "couldn't set cloud properties...", 
										PeaProperties.getVmBridge().translate("error"), 0);
							}

							// last:
							Zeroizer.zero(psw);
						}
						result = 0;
						login.setVisible(false);
						login.dispose();
						return null;
					} else {
						//WebDavLogin.lastAuthSuccess = false;
						TestLog.e(WebDavLogin.class, "Authorization failed...");
						result = 1;
						if (authError.contains("timeout")) {
							PeaDialog.showMessage(this, PeaProperties.getVmBridge().translate("timeout"), null, 0);
							//String errorMessage = authError;

						} else if (authError.contains("405")) { // WebDAV base path error
							webDavBaseField.setBorder(errorBorder);
						} else if (authError.contains("401")) { // user name or password
							userNameField.setBorder(errorBorder);
							passwordField.setBorder(errorBorder);
						} else {
							webDavBaseField.setBorder(errorBorder);
							userNameField.setBorder(errorBorder);
							passwordField.setBorder(errorBorder);
						}
						((PeaLockFrame)PeaControl.getDialog().getLockFrame())
						.setMessage(authError 
								+ ": " + provider.getProviderName(), true);						
						//PeaDialog.showMessage(owner, provider.getErrorMessages(), PeaProperties.getVmBridge().translate("authorization_failed"), 0);
						return PeaProperties.getVmBridge().translate("authorization_failed");
					}
				}
				return null;
			} finally {
				// reset the wait cursor
				login.setCursor(CursorManager.getDefaultCursor());
			}
		}				

		@Override
		protected void done() { 
			//String result = get();
		}
	}

	@Override
	public void windowActivated(WindowEvent arg0) {}

	@Override
	public void windowClosed(WindowEvent e) {}

	@Override
	public void windowClosing(WindowEvent e) {
		TestLog.v(WebDavLogin.class, "closing");
		if (sw != null && sw.getState().equals(SwingWorker.StateValue.STARTED)) {
			sw.cancel(true);
		}
		login.setVisible(false);
		login.dispose();
		result = -1;
	}
	@Override
	public void windowDeactivated(WindowEvent e) {}
	@Override
	public void windowDeiconified(WindowEvent e) {}
	@Override
	public void windowIconified(WindowEvent e) {}
	@Override
	public void windowOpened(WindowEvent e) {}
}