package cologne.eck.all_peas.cloud.gui;

/*
 * PeaFactory - Production of Password Encryption Archives
 * Copyright (C) 2020  Axel von dem Bruch
 * 
 * This library is free software; you can redistribute it and/or modify it 
 * under the terms of the GNU General Public License as published 
 * by the Free Software Foundation; either version 2 of the License, 
 * or (at your option) any later version.
 * This library is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
 * or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU General Public License for more details.
 * See:  http://www.gnu.org/licenses/gpl-2.0.html
 * You should have received a copy of the GNU General Public License 
 * along with this library.
 */

import java.awt.Font;
import java.awt.Window;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.lang.invoke.MethodHandles;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;

import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.SwingUtilities;

import cologne.eck.all_peas.cloud.CloudControl;
import cologne.eck.all_peas.cloud.CloudPropertyHandler;
import cologne.eck.all_peas.cloud.StorageProvider;
import cologne.eck.all_peas.cloud.provider.WebDavProvider;
import cologne.eck.all_peas.control.PeaControl;
import cologne.eck.all_peas.data.AttachmentHandler;
import cologne.eck.all_peas.data.PeaProperties;
import cologne.eck.all_peas.files.CloudFilePanel;
import cologne.eck.all_peas.files.ExecutionTimeObserver;
import cologne.eck.all_peas.files.FileComposer;
import cologne.eck.all_peas.files.FileModel;
import cologne.eck.all_peas.files.FilePanelHolder;
import cologne.eck.all_peas.files.FileTypePanel;
import cologne.eck.all_peas.gui.CurrentFileDisplay;
import cologne.eck.all_peas.gui.CursorManager;
import cologne.eck.all_peas.gui.HtmlFileViewer;
import cologne.eck.all_peas.gui.MultiFileSelectionDialog;
import cologne.eck.all_peas.gui.PeaDialog;
import cologne.eck.all_peas.gui.PeaFileChooser;
import cologne.eck.all_peas.gui.PeaLockFrame;
import cologne.eck.all_peas.gui.PswDialogView;
import cologne.eck.all_peas.vm_specific.JREProperties;
import cologne.eck.peafactory.crypto.CipherStuff;
import cologne.eck.peafactory.crypto.kdf.KeyDerivation;
import cologne.eck.tools.Comparator;
import cologne.eck.tools.Converter;
import cologne.eck.tools.ExceptionHelper;
import cologne.eck.tools.FileTools;
import cologne.eck.tools.TestLog;
import cologne.eck.tools.Zeroizer;

@SuppressWarnings("serial")
public class CloudMenu extends JMenu implements ActionListener {

	private Font font;

	private PeaLockFrame lockFrame;

	private JMenu saveMenu;// to fire programmatically sva action

	// ask to overwrite existing cloud files (Image Lock PEA)
	private boolean askToOverwriteCloudFile = true; // ask only once
	// overwrite existing cloud files (Image LockPEA)
	private boolean overwriteCloudFile = false;

	/**
	 * Menu for cloud actions
	 * 
	 * @param mnemo	the character to use as menmonic
	 */
	public CloudMenu(char mnemo) {

		font = new Font(Font.SANS_SERIF, Font.PLAIN, PeaProperties.getFontSize());
		this.setFont(font);
		this.setText(PeaProperties.getVmBridge().translate("cloud"));
		this.setMnemonic(mnemo);//PeaProperties.getVmBridge().translate("cloud").charAt(0));

		addItems();
	}

	private void addItems() {
		String[] providers = CloudPropertyHandler.getProviderNames();
		if (providers != null) {
			// save and update menu only for lockFrame
			if (PeaControl.isPlainTextMode() == true) {// JREProperties.getMainWindow() instanceof PeaLockFrame... is not yet created
				JMenu downloadMenu = new JMenu(PeaProperties.getVmBridge().translate("download_from_cloud"));
				downloadMenu.setFont(font);
				downloadMenu.setToolTipText(PeaProperties.getVmBridge().translate("tooltip_cloud_download"));
				//downloadMenu.setMnemonic('D');
				downloadMenu.addActionListener(this);
				this.add(downloadMenu);
				int len = providers.length;
				for (int i = 0; i < len; i++) {
					JMenuItem downloadProviderItem = new JMenuItem(providers[i]);
					downloadProviderItem.getAccessibleContext().
					setAccessibleDescription(
							"Download files from " + providers[i]);
					downloadProviderItem.setActionCommand("download_" + providers[i]);
					downloadProviderItem.setFont(font);
					//webItem.setMnemonic(KeyEvent.VK_I);
					//downloadProviderItem.setMnemonic(providers[i].charAt(0));
					downloadProviderItem.addActionListener(this);
					downloadMenu.add(downloadProviderItem);
				}
				saveMenu = new JMenu(PeaProperties.getVmBridge().translate("save_in_cloud"));
				saveMenu.setFont(font);
				//saveMenu.setMnemonic('S');
				saveMenu.addActionListener(this);
				this.add(saveMenu);

				for (int i = 0; i < len; i++) {
					JMenuItem updateProviderItem = new JMenuItem(providers[i]);
					updateProviderItem.getAccessibleContext().
					setAccessibleDescription(
							"Saves current file in " + providers[i]);
					updateProviderItem.setActionCommand("save_" + providers[i]);
					updateProviderItem.setFont(font);
					//updateProviderItem.setMnemonic(providers[i].charAt(0));
					updateProviderItem.addActionListener(this);
					saveMenu.add(updateProviderItem);
				}
			} else {//if ( JREProperties.getMainWindow() instanceof PswDialogView) {

				JMenu openMenu = new JMenu(PeaProperties.getVmBridge().translate("open_file"));
				openMenu.setFont(font);
				//openMenu.setMnemonic('O');
				openMenu.addActionListener(this);				
				this.add(openMenu);
				int len = providers.length;
				for (int i = 0; i < len; i++) {
					JMenuItem openProviderItem = new JMenuItem(providers[i]);
					openProviderItem.getAccessibleContext().
					setAccessibleDescription(
							"Look for files in " + providers[i]);
					openProviderItem.setActionCommand("open_" + providers[i]);
					openProviderItem.setFont(font);
					//openProviderItem.setMnemonic(providers[i].charAt(0));
					openProviderItem.addActionListener(this);
					openMenu.add(openProviderItem);
				}
			} 
		}

		JMenuItem createNewItem = new JMenuItem(PeaProperties.getVmBridge().translate("add_cloud_provider"));
		createNewItem.getAccessibleContext().
		setAccessibleDescription(
				"Create a new cloud access for one provider.");
		createNewItem.setActionCommand("addProvider");
		createNewItem.setFont(font);
		createNewItem.addActionListener(this);
		this.add(createNewItem);

		if (providers != null) {
			
			JMenu modifyMenu = new JMenu(PeaProperties.getVmBridge().translate("modify_cloud_provider"));
			modifyMenu.getAccessibleContext().
			setAccessibleDescription(
					"Modify existing provider.");
			modifyMenu.setFont(font);
			int len = providers.length;
			for (int i = 0; i < len; i++) {

				JMenuItem modifyItem = new JMenuItem(providers[i]);
				modifyItem.getAccessibleContext().
				setAccessibleDescription(
						"Modify provider " + providers[i]);
				modifyItem.setActionCommand("modify_" + providers[i]);
				modifyItem.setFont(font);
				//modifyItem.setMnemonic(providers[i].charAt(0));
				modifyItem.addActionListener(this);
				modifyMenu.add(modifyItem);
			}
			this.add(modifyMenu);

			JMenu deleteMenu = new JMenu(PeaProperties.getVmBridge().translate("delete_cloud_provider"));
			deleteMenu.getAccessibleContext().
			setAccessibleDescription(
					"Delete existing provider.");
			deleteMenu.setFont(font);
			for (int i = 0; i < len; i++) {
				JMenuItem deleteItem = new JMenuItem(providers[i]);
				deleteItem.getAccessibleContext().
				setAccessibleDescription(
						"Delete provider " + providers[i]);
				deleteItem.setActionCommand("delete_" + providers[i]);
				deleteItem.setFont(font);
				//deleteItem.setMnemonic(providers[i].charAt(0));				
				deleteItem.addActionListener(this);
				deleteMenu.add(deleteItem);
			}
			this.add(deleteMenu);
		}
	}

	/**
	 * Rebuild the CloudMenu.
	 * This should be called whenever aprovider was added or deleted
	 */
	private void updateView() {
		this.removeAll();
		this.addItems();
		this.getParent().validate();
		this.getParent().repaint();
	}

	/**
	 * Display a message on the main window (password dialog or lock frame)
	 * 
	 * @param owner		the owner: PswDialogView or PeaLockFrame
	 * @param message	the message to show
	 * @param isError	true: is error (font color red)
	 */
	protected static void displayMessage(Component owner, String message, boolean isError) {
		PeaLockFrame lockFrame = null;
		if (owner instanceof PeaLockFrame) {
			lockFrame = (PeaLockFrame) owner;
			lockFrame.setMessage(message, isError);
		} else if (owner instanceof PswDialogView) {
			PswDialogView.setMessage(message, isError, false);
		} else {
			TestLog.ve(CloudMenu.class, "Component is not instance of PeaLockFrame or PswDialogView");
		}
	}

	/**
	 * Download  files from the cloud, when the key was not yet derived 
	 * (while password dialog is shown)
	 * 
	 * @param prov					the cloud provider
	 * @param selectedFileNames		the file names to download
	 */
	private static void downloadInPasswordDialog(StorageProvider prov, String[] selectedFileNames) {
		// download the files in the chosen directory:
		displayMessage(JREProperties.getMainWindow(), PeaProperties.getVmBridge().translate("download_from_cloud") + "...", false);
		String downloadError = prov.downloadFiles(selectedFileNames, CloudControl.getTmpDir()
				+ prov.getProviderName());

		if (downloadError != null) {
			if (JREProperties.getMainWindow() instanceof PswDialogView &&
					downloadError.equals(PeaProperties.getVmBridge().translate("file_not_found") + " - " + prov.getProviderName())) {
				TestLog.v(CloudMenu.class, "New provider without Peafactory folder...");
			} else {
				PeaDialog.showMessage(JREProperties.getMainWindow(), downloadError, 
					PeaProperties.getVmBridge().translate("error"), 0);
			}
		}
		if (selectedFileNames == null) {
			TestLog.e(CloudMenu.class, "No file names downloaded");
			return;
		}
		// distribute and add to panel:
		int filesLen = selectedFileNames.length;
		File[] files = new File[filesLen];
		String[] selectedFileNamesWithPath = new String[filesLen];
		for (int i = 0; i < filesLen; i++) {		
			String fn = selectedFileNames[i];
			if (fn != null) {
				selectedFileNamesWithPath[i] = CloudControl.getTmpFileNameForCloudFile(fn, prov.getProviderName(), null);
				files[i] = new File(selectedFileNamesWithPath[i]);
			} else {
				TestLog.ve(CloudMenu.class, "Found null file name at index " + i);
			}
		}
		// register that these files are already downloaded:
		CloudControl.addDownloadedInitialFileNames(selectedFileNamesWithPath);
		boolean fileAdded = AttachmentHandler.distributeFiles(files);
		if (fileAdded == true) {
			// update view:
			FilePanelHolder.updateWindow();
		}
	}

	/**
	 * Show login dialog for provider, fill with properties, 
	 * set properties, store password
	 * 
	 * @param rootWindow	PswDialogView or PeaLockFrame
	 * @param title			the title to show
	 * @param provider		the name of the provider
	 * 
	 * @return	true if properties were set, false otherwise (cancelled or error)
	 */
	private boolean showLoginDialogAndSetProperties(Window rootWindow, String title, String provider) {
		String userName = null;
		String webDavPath = null;
		String[] params = CloudPropertyHandler.getProviderLoginParameters(provider);
		if (params != null && params.length > 2) {
			userName = params [0];
			webDavPath = params[2];
		}
		char[] psw = null;
		if (! rootWindow.getName().equals("EncryptedDialog") && ! PeaProperties.getFileType().equals("file")) {// PeaLockFrame

			if (PeaProperties.getAlgoParamString() == null || KeyDerivation.getSalt() == null || provider == null) {
				TestLog.ve(CloudMenu.class, "Missing value for " + provider, 5);
				return false;
			}
			// get password from properties
			String pswString = CloudPropertyHandler.getPlainPasswordFromProvider(provider,
					Converter.bytes2hex(KeyDerivation.getSalt()), 
					Converter.bytes2hex(PeaProperties.getAlgoParamString()));
			if (pswString != null) {
				psw = pswString.toCharArray();
			} else {
				TestLog.ve(CloudMenu.class, "Can't get passwordfrom properties: " + provider, 5);
			}
		} else if (rootWindow.getName().equals("EncryptedDialog")) { // PswDialogView
			// get temporary encrypted password
			HashMap<String, byte[]> map = WebDavLogin.getPasswordsFromNewProviderDialog();
			if (map != null) {
				psw = Converter.bytes2chars(WebDavLogin.getTemporaryStoredPassword(provider));
			}
		}
		// show cloud login dialog
		WebDavLogin login = new WebDavLogin(
				rootWindow, 
				title,
				null, 
				provider, 
				userName, //params[0], 
				psw, 
				webDavPath);//params[2]);
		int result = login.showLoginDialog();
		if (result == 0) {
			TestLog.v(this.getClass(), provider + " successfully modified");
	/*		if (! (JREProperties.getMainWindow() instanceof PswDialogView)  && ! PeaProperties.getFileType().equals("file")) {// PeaLockFrame
				if (psw == null) {
					// was probably set in WebDavLogin:
					psw = CloudPropertyHandler.getPlainPasswordFromProvider(provider,
							Converter.bytes2hex(KeyDerivation.getSalt()), 
							Converter.bytes2hex(PeaProperties.getAlgoParamString())).toCharArray();
					if (psw == null) {
						// stored as manually typed password?
						psw = Converter.bytes2chars(WebDavLogin.getTemporaryStoredPassword(provider));
						if (psw == null) {
							// show password dialog
							psw = PeaDialog.showPasswordDialog(JREProperties.getMainWindow(), provider + ": " + PeaProperties.getVmBridge().translate("enter_cloud_password"), provider, false);
						}
						if (psw != null) {
							if (CipherStuff.getSessionKeyCrypt().getKey() != null) {
								CloudPropertyHandler.storeEncryptedPasswordFromProvider(
										CloudPropertyHandler.encryptPassword(new String(psw)), 
										provider, Converter.bytes2hex(KeyDerivation.getSalt()), 
										Converter.bytes2hex(PeaProperties.getAlgoParamString()), null);	
							} else {
								WebDavLogin.storePassword(provider, Converter.chars2bytes(psw));
							}
						} else {
							TestLog.ve(this.getClass(), provider + " missing password", 5);
						}
					} 
				}
				String pswString = null;
				if (psw != null) {
					pswString = new String(psw);
				}
				CloudPropertyHandler.createSubSet(provider,
						Converter.bytes2hex(KeyDerivation.getSalt()), 
						Converter.bytes2hex(PeaProperties.getAlgoParamString()),
						pswString, null);
			} else {
				// password is stored in WebDavLogin
			}*/
			return true;
		} else {
			if (result == 1) {
				// failed
				TestLog.e(CloudMenu.class, "Could not set properties for " + provider);
				TestLog.ve(CloudMenu.class, "Error occurred for " + provider, 5);
			} else { // cancel/closed
				TestLog.v(CloudMenu.class, "Properties for " + provider + "  not set.");
			}
			return false;
		}
	}

	@Override
	public void actionPerformed(ActionEvent ae) {
		String command = ae.getActionCommand();

		Object lockObject = PeaControl.getDialog().getLockFrame();
		if (lockObject != null) {
			if (lockObject instanceof PeaLockFrame) {
				lockFrame = (PeaLockFrame) lockObject;
			}
		} 
		// Reset message label
		if (lockFrame != null) {
			lockFrame.setMessage("", false);
		} else {
			PswDialogView.setMessage("", false,false);
		}
		Window rootWindow = JREProperties.getMainWindow();

		if (command.equals("addProvider")) {			
			TestLog.v(CloudMenu.class, "Add provider...");
			if (new File (CloudPropertyHandler.getCloudPropertyFileName()).exists() == false) {
				TestLog.v(CloudMenu.class, "Show cloud guide...");
				HtmlFileViewer hfv = new HtmlFileViewer("cloud_guide", SwingUtilities.getWindowAncestor(this));
				hfv.requestFocus();
			} 
			WebDavLogin login = new WebDavLogin( // open a blank WebDavLogin dialog
					rootWindow, 
					PeaProperties.getVmBridge().translate("add_cloud_provider"),
					null, 
					null, 
					null, 
					null, 
					null);
			int result = login.showLoginDialog();
			if (result == 0) {
				if (rootWindow instanceof PswDialogView) {// PswDialogView
					// open:
					StorageProvider prov = login.getProvider();
					displayMessage(rootWindow, PeaProperties.getVmBridge().translate("connect_cloud") + "...", false);
					String[] fileNames = prov.getCloudFiles(PeaControl.getDialog().getSupportedExtensions());
					//	( (WebDavProvider) prov).setWebDavBasePath(params[2]);
					CloudFileManager cfm = new CloudFileManager(rootWindow, prov, fileNames);
					cfm.setVisible(true);
					String[] selectedFileNames = cfm.getSelectedFileNames();
					if (PeaProperties.getFileType().equals("file")) {
						// others than other PEAs, cloud files are stored in a chosen directory

						if (selectedFileNames == null) {
							displayMessage(rootWindow, "", false);
							return;
						}
						// choose a local directory to store the files from cloud:
						// open file chooser to select a directory to store file
						PeaFileChooser pfc = new PeaFileChooser(this);
						pfc.customizeFileChooser(PeaProperties.getVmBridge().translate("save"), 
								PeaProperties.getVmBridge().translate("select_folder_to_store_cloud_files"), false);
						pfc.setApproveButtonText(PeaProperties.getVmBridge().translate("select"));
						pfc.setMultiSelectionEnabled(false);
						pfc.setFileSelectionMode(PeaFileChooser.DIRECTORIES_ONLY);
						int chooserResult = pfc.showOpenDialog(this);
						if (chooserResult != PeaFileChooser.APPROVE_OPTION) {
							return;
						}
						// get selected directory
						File directory = pfc.getSelectedFile();
						if (directory == null) {
							TestLog.e(MethodHandles.lookup().lookupClass(), "No directory selected to store files from cloud...");
							displayMessage(rootWindow, "", false);
							return;
						}
						// create an array of file names with path (to ask to overwrite)
						String[] selectedFileNamesWithPath = new String[selectedFileNames.length];
						for (int i = 0; i < selectedFileNames.length; i++) {
							selectedFileNamesWithPath[i] = directory.getAbsoluteFile() + File.separator + selectedFileNames[i];
						}
						// ask to overwrite, if file exist:
						selectedFileNamesWithPath = FileTools.askToOverwriteFiles(selectedFileNamesWithPath);

						if (selectedFileNamesWithPath == null) {
							TestLog.e(CloudMenu.class, "No files to open left...");		
							displayMessage(rootWindow, "", false);
							return;
						}
						// create array of file names without path (to download)
						selectedFileNames = new String[selectedFileNamesWithPath.length];
						for (int i = 0; i < selectedFileNamesWithPath.length; i++) {
							selectedFileNames[i] = new File(selectedFileNamesWithPath[i]).getName();
						}
						// download the files in the chosen directory:
						displayMessage(rootWindow, PeaProperties.getVmBridge().translate("download_from_cloud") + "...", false);
						String downloadError = prov.downloadFiles(selectedFileNames, directory.getAbsolutePath());
						if (downloadError != null) {
							PeaDialog.showMessage(rootWindow, downloadError, 
									PeaProperties.getVmBridge().translate("error"), 0);
						}
						// distribute and add to panel:
						int filesLen = selectedFileNames.length;
						File[] files = new File[filesLen];
						for (int i = 0; i < filesLen; i++) {		
							String fn = selectedFileNames[i];
							if (fn != null) {							
								files[i] = new File(selectedFileNamesWithPath[i]);
							} else {
								TestLog.ve(CloudMenu.class, "Found null file name at index " + i);
							}
						}
						boolean fileAdded = AttachmentHandler.distributeFiles(files);
						if (fileAdded == true) {
							// update view:
							FilePanelHolder.updateWindow();
						}

					} else { // Notebook PEA, Calendar Lock PEA, Image Lock PEA

						downloadInPasswordDialog(prov, selectedFileNames);
					}
				} else { // PeaLockFrame
					// TODO check: everything done in WebDavLogin???
				}

				this.updateView();
			} else {
				if (result == 1) {
					TestLog.e(CloudMenu.class, "Login failed...");
				} else if (result == -1) {
					TestLog.v(CloudMenu.class, "Login cancelled...");
				}
			}
			displayMessage(rootWindow, "", false);

		} else if (command.startsWith("modify_")) {		
			String provider = command.substring("modify_".length(), command.length());
			//TestLog.v(this.getClass(), "Modify " + provider);
			boolean success = showLoginDialogAndSetProperties(rootWindow, PeaProperties.getVmBridge().translate("modify_cloud_provider"), provider);
			if (success == true) {
				TestLog.v(CloudMenu.class, "properties set for " + provider);
			} else {
				TestLog.v(CloudMenu.class, "Remove provider from opened provider: " + provider);
				CloudControl.removeOpenProvider(provider);
			}

		} else if (command.startsWith("delete_")) {
			String provider = command.substring("delete_".length(), command.length());
			//TestLog.v(this.getClass(), "Delete " + provider);
			int result = PeaDialog.showQuestion(this.getRootPane(), 
					PeaProperties.getVmBridge().translate("really_delete")
					+ "\n" + provider + "?");
			if (result == 0) {
				CloudPropertyHandler.deleteProviderParameters(provider);
				//  remove files from CloudFilePanel and model if in PswDialogView
				if (lockObject == null) {
					ArrayList<FileTypePanel> ftpList = FilePanelHolder.getFileTypePanelList();
					if (ftpList != null &&  ! ftpList.isEmpty()) {
						for (FileTypePanel ftp : ftpList) {
							if (ftp != null && ftp.getCloudFilePanel() != null
									&& ftp.getCloudFilePanel().getCloudControl() != null) {
								ftp.getCloudFilePanel().getCloudControl().removeInitialProvider(provider);
							} else {
								TestLog.ve(CloudMenu.class, "Missing instance...", 5);
								if (ftp == null) {
									TestLog.ve(CloudMenu.class, "ftp missing");
								} else {
									if (ftp.getCloudFilePanel() == null) {
										TestLog.ve(CloudMenu.class, "Cloud filepanel missing");
									}
								}
								displayMessage(rootWindow, PeaProperties.getVmBridge().translate("unexpected_error"), true);
							}
						}
					}
				} else {
					((FileTypePanel)JREProperties.getTypePanel()).getCloudFilePanel().getCloudControl().removeUsedProvider(provider);
				}
				this.updateView();
				// update view:
				//FilePanelHolder.updateFileView();
				FilePanelHolder.updateWindow();
			}			
		} else if (command.startsWith("open_")) {
			/*
			 * This only used in PswDialogView: download an encrypted file from a provider, 
			 * distribute depending on salt and algorithms/parameters
			 * and store in tmp directory or chosen directory (File Lock PEA)
			 */
			// Open a provider manually, when it was not opened automatically
			// ("open_" + providers[i])
			String provider = command.substring("open_".length(), command.length());
			TestLog.v(this.getClass().getSimpleName(), "Open: " + provider);			

			String[] params = CloudPropertyHandler.getProviderLoginParameters(provider);
			StorageProvider prov = null;

			if (params[0] == null || params[2] == null) { // user name, WebDAV path - this should not happen...
				TestLog.e(CloudMenu.class, "Missing user name or WebDAV base path", 5);
				boolean success = showLoginDialogAndSetProperties(rootWindow, PeaProperties.getVmBridge().translate("modify_cloud_provider"), provider);
				if (success == true) {
					TestLog.v(CloudMenu.class, "properties set for " + provider);
				}
			}
			// get the password: Check if it was stored from adding a new provider
			byte[] pswBytes = null;
			char[] psw = null;
			if (WebDavLogin.getPasswordsFromNewProviderDialog() != null) {
				pswBytes = WebDavLogin.getPasswordsFromNewProviderDialog().get(provider);
			}
			if (pswBytes != null) {
				psw = Converter.bytes2chars(WebDavLogin.getPasswordsFromNewProviderDialog().get(provider));
			} else {
				psw = PeaDialog.showPasswordDialog(this, provider + ": " + PeaProperties.getVmBridge().translate("enter_cloud_password"), 
						PeaProperties.getVmBridge().translate("password"), false);
				if (Comparator.compare(psw, "cAnCeLlEd".toCharArray())) {
					TestLog.v(getClass(), "Process cancelled");
					return;
				}
			}
			//TestLog.v(this.getClass().getSimpleName(), "Connecting to: " + provider);
			displayMessage(rootWindow, PeaProperties.getVmBridge().translate("connect_cloud"), false);

			StorageProvider sp = CloudControl.getOpenProvider(provider);
			if (sp == null) {
				sp = new WebDavProvider(provider);
			}
			if (sp instanceof WebDavProvider) {
				prov = (WebDavProvider) sp;
			} else {
				TestLog.ve(CloudMenu.class, "Invalid provider: " + provider, 5);
				displayMessage(rootWindow, PeaProperties.getVmBridge().translate("invalid") + ": " + provider, true);
				return;
			}
			//prov = 	 new WebDavProvider(provider);
			( (WebDavProvider) prov).setWebDavBasePath(params[2]);
			byte[] plainPswBytes = Converter.chars2bytes(psw);
			String error = prov.authorize(params[0], psw, null);
			if (error == null) {
				// store password encrypted:
				WebDavLogin.storePassword(provider, plainPswBytes);
				Zeroizer.zero(plainPswBytes);
				// get file names:
				String[] fileNames = prov.getCloudFiles(PeaControl.getDialog().getSupportedExtensions());
				( (WebDavProvider) prov).setWebDavBasePath(params[2]);
				CloudFileManager cfm = new CloudFileManager(rootWindow, prov, fileNames);

				cfm.setVisible(true);
				String[] selectedFileNames = cfm.getSelectedFileNames();
				if (selectedFileNames != null && selectedFileNames.length > 0) {
					TestLog.v(this.getClass().getSimpleName(), "Number of selected files in cloud: " + selectedFileNames.length);
				}
				if (selectedFileNames != null) {
					// create an array of file names with path (to ask to overwrite)
					String[] selectedFileNamesWithPath = new String[selectedFileNames.length];
					File directory = null;
					if (PeaProperties.getFileType().equals("file")) {
						// download the encrypted files and add to FileTypePanel

						// choose a local directory to store the files from cloud:
						// open file chooser to select a directory to store file
						PeaFileChooser pfc = new PeaFileChooser(this);
						pfc.customizeFileChooser(PeaProperties.getVmBridge().translate("save"), 
								PeaProperties.getVmBridge().translate("select_folder_to_store_cloud_files"), false);
						pfc.setApproveButtonText(PeaProperties.getVmBridge().translate("select"));
						pfc.setMultiSelectionEnabled(false);
						pfc.setFileSelectionMode(PeaFileChooser.DIRECTORIES_ONLY);
						pfc.showOpenDialog(this);
						// get selected directory
						directory = pfc.getSelectedFile();
						if (directory == null) {
							TestLog.e(CloudMenu.class, "No directory selected to store files from cloud...");
							displayMessage(rootWindow, "", false);
							return;
						}

						for (int i = 0; i < selectedFileNames.length; i++) {
							selectedFileNamesWithPath[i] = directory.getAbsoluteFile() + File.separator + selectedFileNames[i];
						}
						// ask to overwrite, if file exist:
						selectedFileNamesWithPath = FileTools.askToOverwriteFiles(selectedFileNamesWithPath);
					} else { // all other PEAs except File Lock PEA
						// create tmp directory
						directory = new File(CloudControl.getTmpDir() + provider);
						if ( ! directory.exists()){
							directory.mkdirs();
						}
						for (int i = 0; i < selectedFileNames.length; i++) {
							selectedFileNamesWithPath[i] = CloudControl.getTmpFileNameForCloudFile(selectedFileNames[i], 
									provider, null);
							//directory.getAbsoluteFile() + File.separator + selectedFileNames[i];
						}
					}

					if (selectedFileNamesWithPath == null) {
						TestLog.e(CloudMenu.class, "No files to open left...");		
						displayMessage(rootWindow, PeaProperties.getVmBridge().translate("no_valid_file_found"), true);
						return;
					}
					// create array of file names without path (to download)

					TestLog.v(this.getClass().getSimpleName(), "Number of files to download: " + selectedFileNames.length);
					// download the files in the chosen directory:
					displayMessage(rootWindow, PeaProperties.getVmBridge().translate("download_from_cloud"), false);
					String downloadError = prov.downloadFiles(selectedFileNames, directory.getAbsolutePath());
					if (downloadError != null) {
						PeaDialog.showMessage(rootWindow, downloadError, 
								PeaProperties.getVmBridge().translate("error"), 0);
					} else {
						String dfn = "";
						for (String s : selectedFileNames) dfn += s + "\n";
						TestLog.v(CloudMenu.class, "Downloaded: \n" + dfn);
					}
					// distribute and add to panel:
					int filesLen = selectedFileNames.length;
					File[] files = new File[filesLen];
					// possible errors, maybe not all files are still present
					for (int i = 0; i < filesLen; i++) {
						files[i] = new File(selectedFileNamesWithPath[i]);
					}
					// register that these files are already downloaded:
					if (! PeaProperties.getFileType().equals("file")) {
						CloudControl.addDownloadedInitialFileNames(selectedFileNamesWithPath);
					}
					boolean fileAdded = AttachmentHandler.distributeFiles(files);
					if (fileAdded == true) {
						// update view:
						FilePanelHolder.updateWindow();
						displayMessage(rootWindow, "", false);
					} else {
						TestLog.ve(CloudMenu.class, "No file was added");
						PeaDialog.showMessage(this, PeaProperties.getVmBridge().translate("failed") + ": " 			
								+ PeaProperties.getVmBridge().translate("download")
								+ "\n" + PeaProperties.getVmBridge().translate("invalid_file"), 
								PeaProperties.getVmBridge().translate("error"), 0);
					}
				}
			} else {
				Zeroizer.zero(plainPswBytes);
				PeaDialog.showMessage(this, PeaProperties.getVmBridge().translate("authorization_failed")
						+ "\n" + error, PeaProperties.getVmBridge().translate("error"), 0);
			}
			displayMessage(rootWindow, "", false);
			
			

		} else if (command.startsWith("download_")) { // = open in lockFrame
			/*
			 * This is used in a PeaLockFrame to download and decrypt file (load from...)
			 * and store in tmp directory or a chosen directory (File Lock PEA)
			 */			

			String directoryName = null;

			if (PeaProperties.getFileType().equals("file")) {
				// download the encrypted files and add to FileTypePanel
				// choose a local directory to store the files from cloud:
				// open file chooser to select a directory to store file
				PeaFileChooser pfc = new PeaFileChooser(this);
				pfc.customizeFileChooser(PeaProperties.getVmBridge().translate("save"), 
						PeaProperties.getVmBridge().translate("select_folder_to_store_cloud_files"), false);
				pfc.setApproveButtonText(PeaProperties.getVmBridge().translate("select"));
				pfc.setMultiSelectionEnabled(false);
				pfc.setFileSelectionMode(PeaFileChooser.DIRECTORIES_ONLY);
				pfc.showOpenDialog(this);
				// get selected directory
				File directory = pfc.getSelectedFile();
				if (directory == null) {
					TestLog.e(CloudMenu.class, "No directory selected to store files from cloud...");
					return;
				}
				directoryName = directory.getAbsolutePath();
			}

			String providerName = command.substring("download_".length(), command.length());
			TestLog.v(CloudMenu.class, "Download from " + providerName + "...");

			String[] params =  CloudPropertyHandler.getProviderLoginParameters(providerName);
			if (params == null) {
				TestLog.ve(CloudMenu.class, "Missing parameters for " + providerName, 5);
				PeaDialog.showMessage(JREProperties.getMainWindow(), 
						JREProperties.getVmBridge().translate("failed") + "\n"
								+ " -> " + JREProperties.getVmBridge().translate("modify_cloud_provider"), 
								JREProperties.getVmBridge().translate("unexpected_error"), 0);
				return;				
			}
			String pswString = null;
			if (JREProperties.getMainWindow() instanceof PeaLockFrame) {
				pswString = CloudPropertyHandler.getPlainPasswordFromProvider(providerName, 
						Converter.bytes2hex(KeyDerivation.getSalt()), 
						Converter.bytes2hex(PeaProperties.getAlgoParamString()));

				if (params[0] == null || params[2] == null ) {// no user or no WebDav path
					TestLog.ve(CloudMenu.class, "Missing parameter for " + providerName + "...", 5);
					// set new parameters along with password and store as properties
					boolean success = showLoginDialogAndSetProperties(rootWindow, PeaProperties.getVmBridge().translate("modify_cloud_provider"), providerName);
					if (success == true) {
						TestLog.v(CloudMenu.class, "properties set for " + providerName);
					} else {
						TestLog.e(CloudMenu.class, "could not set properties set for " + providerName);
						TestLog.ve(CloudMenu.class, "could not set properties set for " + providerName, 5);
						return;
					}
				} 
			}
			if (pswString == null) { // only password not set
				byte[] pswBytes = WebDavLogin.getTemporaryStoredPassword(providerName);
				if (pswBytes != null) {
					pswString = new String(pswBytes, PeaProperties.getCharset());
				} else {
					TestLog.v(CloudMenu.class, "Temporarly stored password not found...");
					char[] psw = PeaDialog.showPasswordDialog(this, providerName + ": " + PeaProperties.getVmBridge().translate("enter_cloud_password"), providerName, false);
					if (psw != null) {
						if (Comparator.compare(psw, "cAnCeLlEd".toCharArray())) {
							TestLog.v(getClass(), "Process cancelled");
							return;
						}
						pswString = new String(psw);
					} else { 
						TestLog.e(CloudMenu.class, "could not get password for " + providerName);
						TestLog.ve(CloudMenu.class, "could not get password for " + providerName, 5);
						return;
					}
				}
				if (pswString != null ) { // store password
					if (JREProperties.getMainWindow() instanceof PeaLockFrame){
						if (CipherStuff.getSessionKeyCrypt().getKey() != null) {
							CloudPropertyHandler.storeEncryptedPasswordFromProvider( 
									CloudPropertyHandler.encryptPassword(pswString), 
									providerName, Converter.bytes2hex(KeyDerivation.getSalt()), 
									Converter.bytes2hex(PeaProperties.getAlgoParamString()), null);		
						} else {
							WebDavLogin.storePassword(providerName, pswBytes);
						}
					} else { // PswDialogView
						WebDavLogin.storePassword(providerName, pswBytes);
					}
				}
			}
			StorageProvider sp = CloudControl.getOpenProvider(providerName);
			if (sp == null) {
				sp = new WebDavProvider(providerName);
			}
			WebDavProvider prov = null;
			if (sp instanceof WebDavProvider) {
				prov = (WebDavProvider) sp;
			} else {
				TestLog.v(CloudMenu.class, "is not a WebDAV provider", 5);
				return;
			}
			( (WebDavProvider) prov).setWebDavBasePath(params[2]);
			try {

				JREProperties.getMainWindow().setCursor(CursorManager.getWaitCursor());
				getRootPane().getParent().setCursor(CursorManager.getWaitCursor());

				if (pswString != null) {
					String error = prov.authorize(params[0], pswString.toCharArray(), null);
					String[] fileNamesToDownload = null;
					if (error == null) {
						fileNamesToDownload = prov.getCloudFiles(PeaControl.getDialog().getSupportedExtensions());
						//( (WebDavProvider) prov).setWebDavBasePath(params[2]);
						CloudFileManager cfm = new CloudFileManager(rootWindow, prov, fileNamesToDownload);
						cfm.setVisible(true);
						JREProperties.getMainWindow().setCursor(CursorManager.getDefaultCursor());
						getRootPane().getParent().setCursor(CursorManager.getDefaultCursor());
						String[] selectedFileNames = cfm.getSelectedFileNames();
						if (selectedFileNames == null ) {
							return;
						}

						if ( ! PeaProperties.getFileType().equals("file")) {

							if (JREProperties.getMainWindow() instanceof PeaLockFrame) {
								//check if files already opened:
								// This becomes a conflict in Editor, Image and File, not in Calendar because of merging
								if ( JREProperties.getTypePanel() == null) {
									// set new FileTypePanel, CloudFilePanel...
									FileComposer newFc = new FileComposer(new FileModel(false, PeaProperties.getAlgoParamString(), KeyDerivation.getSalt()));
									FileTypePanel ftp = new FileTypePanel(100,100, true, false, newFc, JREProperties.getMainWindow());
									// set new panel:
									JREProperties.setTypePanel(ftp);
									CloudFilePanel newCfp = new CloudFilePanel(JREProperties.getMainWindow(), ftp, Converter.bytes2hex(KeyDerivation.getSalt()), 
											Converter.bytes2hex(PeaProperties.getAlgoParamString()));
									// set CloudFilePanel
									ftp.setCloudFilePanel(newCfp);
								}

								HashMap<String, HashSet<String>> map = ((FileTypePanel)JREProperties.getTypePanel()).getCloudFilePanel().getCloudControl().getCloudFileModel().getUsedCloudFileMap();
								if (map.containsKey(providerName)) {
									HashSet<String> set = map.get(providerName);
									StringBuilder builder = new StringBuilder();
									ArrayList<String> filesToAdd = new ArrayList<String>(); // add
									for (String s : selectedFileNames) {
										//s = CloudControl.getTmpFileNameForCloudFile(s, provider, null);
										if (set.contains(s)) {
											builder.append(s + "\n");
										} else {
											filesToAdd.add(s);
											// add to model
											((FileTypePanel)JREProperties.getTypePanel()).getCloudFilePanel().getCloudControl().getCloudFileModel().addUsedFileToProvider(providerName, new File(s).getName());
										}								
									}
									if (builder.length() > 0) {
										PeaDialog.showMessage(rootWindow, 
												providerName + ":\n"
														+ PeaProperties.getVmBridge().translate("file_already_open")
														+ "\n   " + new String(builder), 
														PeaProperties.getVmBridge().translate("error"), 0);
										TestLog.v(this.getClass(), "Already open: " + new String(builder));

										if (filesToAdd.size() == 0) {
											JREProperties.getMainWindow().setCursor(CursorManager.getDefaultCursor());
											getRootPane().getParent().setCursor(CursorManager.getDefaultCursor());
											return;
										} else {
											selectedFileNames = Converter.arrayListToArray(filesToAdd);
										}
									}
								}
							} else {
								downloadInPasswordDialog(sp, selectedFileNames);
								return;
							}

						} else {// File Lock PEA
							// check duplicates and ask to overwrite
							String[] fileNamesToCreate = new String[selectedFileNames.length];
							if (directoryName != null) {
								for (int i = 0; i < selectedFileNames.length; i++) {
									fileNamesToCreate[i] = directoryName + File.separator + selectedFileNames[i];
								}
								fileNamesToCreate = FileTools.askToOverwriteFiles(fileNamesToCreate);
								if (fileNamesToCreate == null) {
									JREProperties.getMainWindow().setCursor(CursorManager.getDefaultCursor());
									getRootPane().getParent().setCursor(CursorManager.getDefaultCursor());
									return;
								}
								// Remove the directory
								int prePart = (directoryName + File.separator).length();
								for (String fn : fileNamesToCreate) {
									fn = fn.substring(prePart);
								}
								selectedFileNames = fileNamesToCreate;
							} else {
								TestLog.v(CloudMenu.class, "is null", 5);;
								PeaDialog.showMessage(this, PeaProperties.getVmBridge().translate("unexpected_error") + ": (missing directory name)"
										+ "\n" + error, PeaProperties.getVmBridge().translate("error"), 0);
								JREProperties.getMainWindow().setCursor(CursorManager.getDefaultCursor());
								getRootPane().getParent().setCursor(CursorManager.getDefaultCursor());
								return;
							}
						}
						DownloadWorker dw = new DownloadWorker(lockFrame, providerName, 
								selectedFileNames, directoryName);
						dw.execute();

					} else {
						PeaDialog.showMessage(this, PeaProperties.getVmBridge().translate("authorization_failed")
								+ "\n" + error, PeaProperties.getVmBridge().translate("error"), 0);
					}
				} 
			} finally {
				JREProperties.getMainWindow().setCursor(CursorManager.getDefaultCursor());
				getRootPane().getParent().setCursor(CursorManager.getDefaultCursor());
			}

		} else if (command.startsWith("save_")) { // UPLOAD
			String providerName = command.substring("save_".length(), command.length());
			
			if ( ((PeaLockFrame)PeaControl.getDialog().getLockFrame()).isContentUnsaved() == true) {
				int saveResult = PeaDialog.showQuestion(this, PeaProperties.getVmBridge().translate("save_modifications"));
				if (saveResult == 0) {
					((PeaLockFrame)PeaControl.getDialog().getLockFrame()).saveCurrentContent();
				}
			}

			// get current used file names:
			String[] currentFileNamesWithPath = null;
			if (PeaControl.getDialog().getLockFrame() instanceof PeaLockFrame) {
				PeaLockFrame lf = (PeaLockFrame)PeaControl.getDialog().getLockFrame();
				currentFileNamesWithPath = lf.getCurrentFileNames();//lockFrame.getCurrentFileNames();
				//for(String s : currentFileNamesWithPath) {TestLog.v(this.getClass(), "file name to save: " + s);TestLog.v(this.getClass(), "Len: " + new File(s).length());}
			}  else {
				TestLog.v(CloudMenu.class, "frame is not instance of PeaLockFrame", 5);

			}	
			
			if (PeaProperties.getFileType().equals("text file") 
					|| PeaProperties.getFileType().equals("image")) { // TODO other PEAs???
				MultiFileSelectionDialog mfs = new MultiFileSelectionDialog(JREProperties.getMainWindow(), 
						JREProperties.getMainWindow().getLocationOnScreen(), 
						PeaProperties.getVmBridge().translate("choose"),
						Converter.arrayToArrayList(currentFileNamesWithPath));
				ArrayList<String> chosenFileNames = mfs.getSelectedFiles();
				if (chosenFileNames == null || chosenFileNames.size() == 0) {
					TestLog.v(CloudMenu.class, "No file names chosen...");
					return;
				}
				currentFileNamesWithPath = Converter.arrayListToArray(chosenFileNames);
			}

			// check if password is present: Set if not
			if( CloudPropertyHandler.getPlainPasswordFromProvider(providerName, 
					Converter.bytes2hex(KeyDerivation.getSalt()), 
					Converter.bytes2hex(PeaProperties.getAlgoParamString())) == null) {
				// show password dialog
				char[] psw = PeaDialog.showPasswordDialog(rootWindow, 
						providerName + ": " + PeaProperties.getVmBridge().translate("enter_cloud_password"), providerName, false);
				if (psw != null) {
					if (Comparator.compare(psw, "cAnCeLlEd".toCharArray())) {
						TestLog.v(getClass(), "Process cancelled");
						return;
					}
					if (CipherStuff.getSessionKeyCrypt().getKey() != null) {
						String error = CloudPropertyHandler.storeEncryptedPasswordFromProvider(
								CloudPropertyHandler.encryptPassword(new String(psw)), providerName, 
								Converter.bytes2hex(KeyDerivation.getSalt()), 
								Converter.bytes2hex(PeaProperties.getAlgoParamString()), null);
						if (error != null) {
							TestLog.e(CloudMenu.class, "unexpected error: could not set password for " + providerName);
							TestLog.ve(CloudMenu.class, "Error occurred for " + providerName, 5);
							return;
						}
					} else {
						WebDavLogin.storePassword(providerName, Converter.chars2bytes(psw));
					}
				} else {
					TestLog.e(CloudMenu.class, "unexpected error: missing password for " + providerName);
					TestLog.ve(CloudMenu.class, "Missing password for " + providerName, 5);
					return;
				}
			}

			if (PeaProperties.getFileType().equals("text file") 
					|| PeaProperties.getFileType().equals("image")) { // TODO other PEAs???
				// check if file already exists and ask to overwrite
				StorageProvider sp = CloudControl.getOpenProvider(providerName);
				if (sp == null) {
					sp = new WebDavProvider(providerName);
				}
				WebDavProvider prov = null;
				if (sp instanceof WebDavProvider) {
					prov = (WebDavProvider) sp;
				} else {
					TestLog.v(CloudMenu.class, "StorageProvider is not a WebDAV provider", 5);
					return;
				}
				// authorize to check if file already exists:
				if (CloudControl.getOpenProvider(providerName) == null) {
					String[] params = CloudPropertyHandler.getProviderLoginParameters(providerName);
					char[] psw = CloudPropertyHandler.getPlainPasswordFromProvider(providerName, 
							Converter.bytes2hex(KeyDerivation.getSalt()), 
							Converter.bytes2hex(PeaProperties.getAlgoParamString())).toCharArray();
					if (psw == null) {
						TestLog.ve(CloudMenu.class, "psw from properties is null - password missing...", 5);
						return ;
					}	
					prov.setWebDavBasePath(params[2]);
					String authError = prov.authorize(params[0], psw, null);
					if (authError != null) {

					}
				}

				ArrayList<String> listToKeep = new ArrayList<String>();
				// check if file exist and ask to overwrite:
				for (String s : currentFileNamesWithPath) {
					String fileNameToCheck = new File(s).getName();
					String fileExist = prov.fileExists(fileNameToCheck);// null if exist
					if (fileExist == null) {// file already exists
						if (askToOverwriteCloudFile == true) {
							int overwriteResult = PeaDialog.showQuestion(JREProperties.getMainWindow(), 
									PeaProperties.getVmBridge().translate("overwrite_existing_file")
									+ "\n (" + PeaProperties.getVmBridge().translate("decision_this_session") + ")",
									//+ "\n (" + PeaProperties.getVmBridge().translate("remember_decision")+")", 
									null, 0, 2);
							if (overwriteResult == 0) { // overwrite
								askToOverwriteCloudFile = false;
								overwriteCloudFile = true;
							} 
						}
						if (overwriteCloudFile == true) {
							listToKeep.add(s);
						} else {
							TestLog.v(CloudMenu.class, "Skip: " + s);
						}

					} else {// file does not exist
						listToKeep.add(s);
					}
				}
				currentFileNamesWithPath = Converter.arrayListToArray(listToKeep);

				// single file: not need to display selectable files
			} else { // Calendar Lock PEA or File Lock PEA
				// Ask user to choose files from current used files, get selected files:
				// do not ask if there is only one file
				if (currentFileNamesWithPath.length > 1) {
					CurrentFileDisplay cfd = new CurrentFileDisplay(rootWindow, currentFileNamesWithPath);		
					// display failed files to upload
					currentFileNamesWithPath = cfd.getSelectedFileNames();
					if (cfd.getFailedFiles() != null && cfd.getFailedFiles().length > 0) {
						String message = PeaProperties.getVmBridge().translate("open_file") 
								+ " " + PeaProperties.getVmBridge().translate("failed") + "\n\n";
						for (String fileAndReason : cfd.getFailedFiles()) {
							message += fileAndReason + "\n";
						}
						PeaDialog.showMessage(rootWindow, message + "\n(failed files)", 
								PeaProperties.getVmBridge().translate("error"), 0);
					}
				}
				if (currentFileNamesWithPath == null) {
					PeaDialog.showMessage(rootWindow, PeaProperties.getVmBridge().translate("no_valid_file_found"), 
							PeaProperties.getVmBridge().translate("error"), 0);
					return;
				} else {
					// check number and warn if required:
					long[] numberAndSize = FileTools.getNumberAndSize(currentFileNamesWithPath);
					if (numberAndSize[0] > 100 && numberAndSize[0] <= 300) {// File number
						boolean goOn = ExecutionTimeObserver.showWarningAndBreakOption(rootWindow, 
								PeaProperties.getVmBridge().translate("number_of_files") + " " + numberAndSize[0] + "\n" 
										+ PeaProperties.getVmBridge().translate("execution_time_warning"),
										false);
						if (goOn == false) {
							TestLog.e(CloudMenu.class, "Upload process cancelled...");
							return;
						}
					} else if (numberAndSize[0] > 300) {// File number)
						boolean goOn = ExecutionTimeObserver.showWarningAndBreakOption(rootWindow, 
								PeaProperties.getVmBridge().translate("number_of_files") + " " + numberAndSize[0] + "\n" 
										+ PeaProperties.getVmBridge().translate("extreme_time_warning"),
										true);
						if (goOn == false) {
							TestLog.e(CloudMenu.class, "Upload process cancelled...");
							return;
						}
					}
					// check size and warn if required:
					if (numberAndSize[1] > 1024 * 1024 * 2 && numberAndSize[1] <= 1024 * 1024 * 16) { // 1 MB
						boolean goOn = ExecutionTimeObserver.showWarningAndBreakOption(rootWindow, 
								PeaProperties.getVmBridge().translate("overall_size") + " " + numberAndSize[1] + "\n" 
										+ PeaProperties.getVmBridge().translate("execution_time_warning"),
										false);
						if (goOn == false) {
							TestLog.e(CloudMenu.class, "Upload process cancelled...");
							return;
						}
					} else if (numberAndSize[1] > 1024 * 1024 * 16) {// File size
						boolean goOn = ExecutionTimeObserver.showWarningAndBreakOption(rootWindow, 
								PeaProperties.getVmBridge().translate("overall_size") + " " + numberAndSize[1] + "\n" 
										+ PeaProperties.getVmBridge().translate("extreme_time_warning"),
										true);
						if (goOn == false) {
							TestLog.e(CloudMenu.class, "Upload process cancelled...");
							return;
						}
					}
				}
			}
			// Warning for long execution time
			long[] numberAndSize = FileTools.getNumberAndSize(currentFileNamesWithPath);
			if (numberAndSize[1] > 1024 * 1024 ) {// 1 MB
				String addition = "";
				if (numberAndSize[1] > 1024*1024*10) { // 16 MB
					addition = PeaProperties.getVmBridge().translate("extreme_time_warning") + "!!!\n\n";
				}
				//int timeResult = PeaDialog.showQuestion(JREProperties.getMainWindow(), "This may take time. Continue?");
				int timeResult = PeaDialog.showQuestion(JREProperties.getMainWindow(), 
						PeaProperties.getVmBridge().translate("program_not_suited_for_upload") + "\n" +
								addition + PeaProperties.getVmBridge().translate("size_warning"),
								PeaProperties.getVmBridge().translate("warning"), 0, 2);
				if (timeResult != 0) {

					return;
				}
			}			
			// create the file array to upload: 
			// copy files to tmp folder to upload
			// (File Lock PEA: copy to encrypt only the copy of the files,
			// not the files in FileTypePanel)
			int fileLen = currentFileNamesWithPath.length;
			ArrayList<File> filesToUploadList = new ArrayList<File>();

			String copyError = "";

			if (PeaProperties.getFileType().equals("file")) {

				// create the target file names
				String[] targetFileNames = new String[currentFileNamesWithPath.length];
				for (int i = 0; i < currentFileNamesWithPath.length; i++) {
					try {
						targetFileNames[i] = CloudControl.getTmpFileNameForCloudFile(
								new File(currentFileNamesWithPath[i]).getCanonicalFile().getName(), 
								providerName, currentFileNamesWithPath[i]);
					} catch (IOException e) {
						TestLog.e(CloudMenu.class, new ExceptionHelper(e).getInfos());
						// null files are handled in cipher
					}
				}

				// copy-encrypt the files:
				try {
					// set the wait cursor: 
					lockFrame.setCursor(CursorManager.getWaitCursor());
					FileComposer fc = null;
					if (JREProperties.getTypePanel() != null && JREProperties.getTypePanel() instanceof FileTypePanel) {
						fc = ((FileTypePanel)JREProperties.getTypePanel()).getFileComposer();
					} else {
						TestLog.v(CloudMenu.class, "is not FileTypePanel", 5);
					}
					String[] errorMessages = CipherStuff.getCipherMode().copyEncryptFiles(
							currentFileNamesWithPath, targetFileNames, 
							CipherStuff.getSessionKeyCrypt().getKey(), true, 
							fc);
					// collect the messages and failed files (index of errorMessages)
					for (int i = 0; i < errorMessages.length; i++) {
						if (errorMessages[i] != null && errorMessages.length > 0) {
							copyError +=  errorMessages[i] + " - " + currentFileNamesWithPath[i] + "\n";
						} else {
							filesToUploadList.add(new File(targetFileNames[i]));
						}
					}	

				} finally {
					lockFrame.setCursor(CursorManager.getDefaultCursor());
				}
				TestLog.v(this.getClass(), "file(s) encrypted");

			} else { // all other PEAs except File Lock PEA

				for (int i = 0; i < fileLen; i++) {
					File origFile = new File(currentFileNamesWithPath[i]);
					// Create a copy in tmp directory
					File destFile = null;
					try {
						destFile = new File(CloudControl.getTmpFileNameForCloudFile(
								origFile.getName(), providerName, origFile.getAbsolutePath()));
					} catch (Exception e) {
						TestLog.ve(CloudMenu.class, "Cant create destFile from " + 
								origFile.getAbsolutePath()+ " for cloud " +  providerName, 5);
						TestLog.e(CloudMenu.class, new ExceptionHelper(e).getInfos());
						copyError += origFile.getAbsolutePath() + ": "  + e.getLocalizedMessage() + "\n";
					}
					if (destFile != null) {
						if (origFile.getAbsolutePath().equals(destFile.getAbsolutePath())) {
							// do not copy the file itself
							TestLog.e(CloudMenu.class, "Break copy file to itself: " + origFile.getAbsolutePath());
							continue;
						}
						String e = FileTools.copyFiles(origFile, destFile);
						if (e != null) {
							copyError += e + "\n";
						} else {
							filesToUploadList.add(destFile);	
						}
					}
				}

			}
			// display failed files:
			if (copyError.length() > 0) {
				PeaDialog.showMessage(rootWindow, copyError + ("copy)"), 
						PeaProperties.getVmBridge().translate("error"), 0);
			}

			// upload the files
			if (filesToUploadList != null && filesToUploadList.size() > 0) {

				// upload the files
				File[] fileArray = filesToUploadList.toArray(new File[0]);

				boolean markUnsaved = false;
				if (PeaControl.getDialog() != null 
						&& PeaControl.getDialog().getLockFrame() != null 
						&& PeaControl.getDialog().getLockFrame() instanceof PeaLockFrame) {
					markUnsaved = ((PeaLockFrame) PeaControl.getDialog().getLockFrame()).isContentUnsaved();
				}
				new UploadWorker(JREProperties.getMainWindow(), 
						providerName, 
						null, 
						fileArray, 
						null, markUnsaved).execute();		
			} else {
				TestLog.v(CloudMenu.class, "No file to upload...");
			}
		} else {
			TestLog.v(this.getClass(), "Invalid command in cloud menu");
		}
	}
}
