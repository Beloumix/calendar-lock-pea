package cologne.eck.all_peas.cloud.gui;

/*
 * Peafactory - Production of Password Encryption Archives
 * Copyright (C) 2020  Axel von dem Bruch
 * 
 * This library is free software; you can redistribute it and/or modify it 
 * under the terms of the GNU General Public License as published 
 * by the Free Software Foundation; either version 2 of the License, 
 * or (at your option) any later version.
 * This library is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
 * or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU General Public License for more details.
 * See:  http://www.gnu.org/licenses/gpl-2.0.html
 * You should have received a copy of the GNU General Public License 
 * along with this library.
 */

import java.awt.Window;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ThreadLocalRandom;
import java.util.concurrent.TimeUnit;

import javax.swing.SwingWorker;

import cologne.eck.all_peas.cloud.CloudControl;
import cologne.eck.all_peas.cloud.CloudPropertyHandler;
import cologne.eck.all_peas.cloud.StorageProvider;
import cologne.eck.all_peas.cloud.provider.WebDavProvider;
import cologne.eck.all_peas.control.PeaControl;
import cologne.eck.all_peas.data.Attachments;
import cologne.eck.all_peas.data.PeaProperties;
import cologne.eck.all_peas.files.FileTypePanel;
import cologne.eck.all_peas.gui.PeaDialog;
import cologne.eck.all_peas.gui.PeaLockFrame;
import cologne.eck.all_peas.vm_specific.JREProperties;
import cologne.eck.peafactory.crypto.kdf.KeyDerivation;
import cologne.eck.tools.Converter;
import cologne.eck.tools.TestLog;
import cologne.eck.tools.UnexpectedValueException;

public class UploadWorker extends SwingWorker<Boolean, String> {
	
	private Window owner;// owner component: PeaLockFrame (or PswDialogView)
	private String providerName;// name of the cloud provider to use
	private String cloudFolder;
	private File[] files;// files to save in the cloud
	private HashMap<String, byte[]> map;
	private boolean markContentUnsaved = false;
	
	private String providerError;// error message from provider to display
	
	private int processInt;
	
	private boolean isRunning = false;
	
	/**
	 * Save files in the cloud in a SwingWorker thread, 
	 * display messages for success, show an
	 * error dialog for failure
	 * 
	 * @param _owner		the owner to display messages
	 * @param _providerName	the provider to store the files
	 * @param _cloudFolder	the folder of the cloud to store or null
	 * @param _files		the files (in tmp with absolute path) to store or null if map is used
	 * @param _map			the map <String, byte[]> (File name, content)
	 * 						or null files are used - only use, when starting the PEA,
	 * 						not, when a file is uploaded in the session
	 * @param _markContentUnsaved		true if the content should be mark unsaved when upload fails
	 */
	public UploadWorker(Window _owner, String _providerName, String _cloudFolder, File[] _files, HashMap<String, byte[]> _map,
			boolean _markContentUnsaved) {
		
		this.owner = _owner;
		this.providerName = _providerName;
		this.files = _files;
		this.map = _map;
		this.cloudFolder = _cloudFolder;
		this.markContentUnsaved = _markContentUnsaved;
	}

	@Override
	protected Boolean doInBackground() throws Exception {
		
		TestLog.v(UploadWorker.class, "Upload to " + providerName + "...");
		providerError = null;
		processInt = ThreadLocalRandom.current().nextInt();
		CloudControl.addRunningCloudProcesses(processInt);		
		isRunning = true;

		publish(providerName + ": " + PeaProperties.getVmBridge().translate("connect_cloud"));

		StorageProvider provider = null;
		if (CloudControl.getOpenProvider(providerName) == null) {

			// Try to authorize:
			String[] params = CloudPropertyHandler.getProviderLoginParameters(providerName);

			if (params[2] != null) { // WebDAV base path
				StorageProvider sp = CloudControl.getOpenProvider(providerName);
				if (sp == null) {
					sp = new WebDavProvider(providerName);
				}
				if (sp instanceof WebDavProvider) {
					provider = (WebDavProvider) sp;
				} else {
					new UnexpectedValueException("StorageProvider","sp","is not a WebDAV provider").printDescription();
					providerError = providerName + ": " + PeaProperties.getVmBridge().translate("unexpected_error") + " - not a WebDAV provider";
					isRunning = false;
					return false;
				}
				( (WebDavProvider) provider).setWebDavBasePath(params[2]);
			} else {
				TestLog.e(UploadWorker.class, "No WebDAV base path in properties for " + providerName);
				TestLog.ve(UploadWorker.class, "for cloud user: " + params[0], 5);
				//TODO Dropbox...
				//new UnexpectedValueException("String", "params[2]", "is null - WebDAV provider").printDescription();
				providerError = PeaProperties.getVmBridge().translate("try_again") + "\n" 
						+ PeaProperties.getVmBridge().translate("unexpected_error") + " - " + PeaProperties.getVmBridge().translate("upload_failed") 
						+ "\n (WebDAV provider is null)";
				isRunning = false;
				return false;
			}
			char[] psw = CloudPropertyHandler.getPlainPasswordFromProvider(providerName, 
					Converter.bytes2hex(KeyDerivation.getSalt()), 
					Converter.bytes2hex(PeaProperties.getAlgoParamString())).toCharArray();
			if (psw == null) {
				TestLog.e(UploadWorker.class, "Can't get password for provider " + providerName + " - Can upload files", 5);
				providerError = providerName + ": " + PeaProperties.getVmBridge().translate("unexpected_error") + " - password missing";
				isRunning = false;
				return false;
			}		

			String authError = provider.authorize(params[0], psw, null);
			if (authError != null) {
				TestLog.e(UploadWorker.class, providerName + ": authorization failed");
				providerError =  authError;
				isRunning = false;
				return false;
			} else {
				// store the password in properties:
				TestLog.v(UploadWorker.class, "Store password encrypted in properties for " + providerName + "...");
				String storePasswordError = CloudPropertyHandler.storeEncryptedPasswordFromProvider(
						CloudPropertyHandler.encryptPassword(new String(psw)),
						providerName, 
						Converter.bytes2hex(KeyDerivation.getSalt()), 
						Converter.bytes2hex(PeaProperties.getAlgoParamString()), null);
				if (storePasswordError != null) {
					TestLog.ve(UploadWorker.class, "Could not store password for " + providerName + "\n" + storePasswordError, 5);
				}
			}
		} else {
			StorageProvider sp = CloudControl.getOpenProvider(providerName);
			if (sp == null) {
				sp = new WebDavProvider(providerName);
			}
			if (sp instanceof WebDavProvider) {
				provider = (WebDavProvider) sp;
			} else {
				new UnexpectedValueException("StorageProvider","sp","is not a WebDAV provider").printDescription();
				providerError = providerName + ": " + PeaProperties.getVmBridge().translate("unexpected_error") + " - not a WebDAV provider";
				isRunning = false;
				return false;
			}
		}
		TestLog.v(UploadWorker.class, "Successfully authorized");
		publish(providerName + ": " + PeaProperties.getVmBridge().translate("successfully_authorized"));
		
		ScheduledExecutorService executor =
			    Executors.newSingleThreadScheduledExecutor();

			Runnable periodicTask = new Runnable() {
				String workingChar1 = "/";
				String workingChar2 = "—";//END OF GUARDED AREA (U+0097)
				String workingChar3 = "\\";
				String workingChar = workingChar1;
			    public void run() {
			    	if (isRunning == true) {
			    		if (workingChar.equals(workingChar1)) {
			    			workingChar = workingChar2;
			    		} else if (workingChar.equals(workingChar2)) {
			    			workingChar = workingChar3;
			    		} else {
			    			workingChar = workingChar1;
			    		}
			    		// Invoke method(s) to do the work
			    		publish(providerName + ": " + PeaProperties.getVmBridge().translate("save_in_cloud") + " " + workingChar);
			    		//doPeriodicWork();
			    	} else {
			    		//publish("");
			    		executor.shutdown();
			    	}
			    }
			};
			executor.scheduleAtFixedRate(periodicTask, 1000, 250, TimeUnit.MILLISECONDS);		
		
		// Try to upload:
		String uploadError = null;
		if (files != null) {
			// check if encrypted
			if (isEncrypted(files) == false) {
				providerError = PeaProperties.getVmBridge().translate("unexpected_error") + " (invalid files)";
						//"Files to upload are not encrypted... Break upload process";
				TestLog.ve(UploadWorker.class, "Files to upload are not encrypted... Break upload process", 5);
				isRunning = false;
				return false;
			}
			uploadError = provider.uploadFiles(files, cloudFolder);
		} else if (map != null) {
			// check if encrypted
			if (isEncrypted(map) == false) {
				TestLog.e(UploadWorker.class, "Is not encrypted...");
				providerError = "Plain text files...";
				isRunning = false;
				return false;
			}
			uploadError = provider.uploadContent(map, cloudFolder);
			
		} else {
			new UnexpectedValueException("File[] and HashMap", "files and map", "is null").printDescription();
		}

		if (uploadError !=  null) {
			// status code: 423, reason phrase: Unexpected response (423 Locked)
			// source or destination resource of a method is locked
			TestLog.e(UploadWorker.class, uploadError);
			providerError = uploadError;
			isRunning = false;
			return false;
		} else {
			if ( ! PeaProperties.getFileType().equals("file")) {

				int filesLen = files.length;
				for (int i = 0; i < filesLen; i++) {
					boolean added = ((FileTypePanel)JREProperties.getTypePanel()).getCloudFilePanel().getCloudControl().addUsedFileNameToProvider(providerName, files[i].getName());
					if (added == true) {
						TestLog.v(this.getClass(), "Was added to used files: " + files[i].getName());
					} else {
						TestLog.v(this.getClass(), "Wasn't added to used files: " + files[i].getName());
					}
				}			
			}
			isRunning = false;
			return true;
		}
	}
	
	/**
	 * Check if a file is encrypted by checking the fileIdentifier
	 * 
	 * @param files	the files to check
	 * 
	 * @return	true if all files are encrypted, false otherwise
	 */
	private boolean isEncrypted(File[] files) {
		RandomAccessFile raf = null;
		for (File f : files) {
			try {
				if (f.length() == 0) {
					TestLog.ve(UploadWorker.class, "Empty file when check if encrypted " + f.getAbsolutePath() );
				}
				raf = new RandomAccessFile( f, "r" );
			} catch (FileNotFoundException e) {
				TestLog.ve(UploadWorker.class, "Missing file to check if encrypted " + f.getAbsolutePath() + " - " + e.getLocalizedMessage());
			} catch (Exception e){
				TestLog.ve(UploadWorker.class, e.toString() + " by checking if encrypted " + f.getAbsolutePath() + " - " + e.getLocalizedMessage());
			}
			
			try {
				if (Attachments.checkFileIdentifier(raf,false) < 0){
					return false;
				}
			} catch (IOException e) {
				TestLog.ve(UploadWorker.class, e.toString() + " by checking file identifier " + f.getAbsolutePath() + " - " + e.getLocalizedMessage());
			} catch (Exception e) {
				TestLog.ve(UploadWorker.class, e.toString() + " by checking file identifier " + f.getAbsolutePath() + " - " + e.getLocalizedMessage());
			}
		}
		return true;
	}
	
	/**
	 * Check if the content in the map is encrypted by checking the fileIdentifier
	 * 
	 * @param map 	the map with contents to check
	 * 
	 * @return	true if all contents are encrypted, false otherwise
	 */
	private boolean isEncrypted(HashMap<String, byte[]> map) {

		Collection<byte[]> col = map.values();
		int FILE_IDENTIFIER_SIZE = Attachments.getFileIdentifierSize();
		for (byte[] bytes : col) {			
			if(bytes.length < FILE_IDENTIFIER_SIZE ) {
				return false;
			}
			byte[] identifierToCheck = new byte[FILE_IDENTIFIER_SIZE];
			System.arraycopy(bytes, bytes.length - FILE_IDENTIFIER_SIZE, identifierToCheck, 0, FILE_IDENTIFIER_SIZE);
			if ( ! Arrays.equals(identifierToCheck, Attachments.getFileIdentifier())) {
				return false;
			}
		}
		return true;
	}
	
    @Override
    protected void process(List <String> infos) {
    	String info = infos.get(infos.size() - 1);
    	CloudMenu.displayMessage(owner, info, false); // not error
    }

	@Override
	protected void done() { 
		isRunning = false;
		try {
			if (providerError == null || providerError.length() == 0) {
				String fileNumber = "0";
				if (files != null && files.length > 0) {
					fileNumber = "" + files.length;				
				} else {
					fileNumber = "" + map.size();
				}
				TestLog.o(this.getClass(), fileNumber + " files successfully uploaded in " + providerName);

				if (files != null) {
					if (PeaControl.getDialog().getLockFrame() instanceof PeaLockFrame) {
						((PeaLockFrame) PeaControl.getDialog().getLockFrame()).updateModelWithCloudFiles(files, providerName);
					}				
				}
				Thread.sleep(100);// other wise, this message is not always shown because of / \ | thread
				CloudMenu.displayMessage(owner,providerName + ": " 
				+  PeaProperties.getVmBridge().translate("successfully_uploaded") + " (" + fileNumber + ")", false);
				
			} else { // error occurred
				if (markContentUnsaved == true) {
					if (PeaControl.getDialog().getLockFrame() != null && PeaControl.getDialog().getLockFrame() instanceof PeaLockFrame) {
						((PeaLockFrame) PeaControl.getDialog().getLockFrame()).markUnsavedContentChanges();
					} else {
						TestLog.ve(UploadWorker.class, "Missing or invalid lock frame", 5);
					}
				}
				PeaDialog.showMessage(owner, 
						providerError, 
						PeaProperties.getVmBridge().translate("error"), 0);
				CloudMenu.displayMessage(owner, providerName + ": " + PeaProperties.getVmBridge().translate("upload_failed"), true);
			}
		} catch (Exception e) {
			TestLog.ve(this.getClass(), e.toString() + " - " + e.getLocalizedMessage(), 5);
		} finally { // always remove process: 
			CloudControl.removeRunningCloudProcesses(processInt);
		}
	}
}
