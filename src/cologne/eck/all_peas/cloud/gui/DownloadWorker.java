package cologne.eck.all_peas.cloud.gui;

/*
 * Peafactory - Production of Password Encryption Archives
 * Copyright (C) 2020  Axel von dem Bruch
 * 
 * This library is free software; you can redistribute it and/or modify it 
 * under the terms of the GNU General Public License as published 
 * by the Free Software Foundation; either version 2 of the License, 
 * or (at your option) any later version.
 * This library is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
 * or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU General Public License for more details.
 * See:  http://www.gnu.org/licenses/gpl-2.0.html
 * You should have received a copy of the GNU General Public License 
 * along with this library.
 */

import java.awt.Window;
import java.io.File;
import java.lang.invoke.MethodHandles;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import javax.swing.SwingWorker;

import cologne.eck.all_peas.cloud.CloudControl;
import cologne.eck.all_peas.cloud.CloudPropertyHandler;
import cologne.eck.all_peas.cloud.StorageProvider;
import cologne.eck.all_peas.cloud.provider.WebDavProvider;
import cologne.eck.all_peas.control.PeaControl;
import cologne.eck.all_peas.data.AttachmentHandler;
import cologne.eck.all_peas.data.Attachments;
import cologne.eck.all_peas.data.PeaProperties;
import cologne.eck.all_peas.files.FileTypePanel;
import cologne.eck.all_peas.gui.PeaDialog;
import cologne.eck.all_peas.gui.PeaLockFrame;
import cologne.eck.all_peas.vm_specific.JREProperties;
import cologne.eck.peafactory.crypto.AlgoParamHandler;
import cologne.eck.peafactory.crypto.CipherStuff;
import cologne.eck.peafactory.crypto.kdf.KeyDerivation;
import cologne.eck.tools.Comparator;
import cologne.eck.tools.Converter;
import cologne.eck.tools.ExceptionHelper;
import cologne.eck.tools.TestLog;
import cologne.eck.tools.UnexpectedValueException;


class DownloadWorker extends SwingWorker<Boolean, String> {
	
	private Window owner;// owner component: PeaLockFrame (or PswDialogView)
	private String providerName;// name of the cloud provider to use
	private String[] fileNames;// file names to download from the cloud
	private String directoryName; // the directory to store the downloaded files
	
	private ArrayList<String> successFileNames = new ArrayList<String>();
	
	private StringBuilder errorBuilder; // builder to collect errors
	
	private boolean isRunning = false;
	
	private String providerError;// error message to display
	// File names that do not exist in the cloud:
	//private ArrayList<String> missingFileNames = new ArrayList<String>();
	// File names for which their download failed
	//private ArrayList<String> failedFileNames = new ArrayList<String>();
	
	/**
	 * Download files from the cloud in a SwingWorker thread, 
	 * checks salt, file identifier, decrypt, 
	 * display messages for success, show an 
	 * error dialog for failure
	 * (Used in CloudMenu (in PeaLockFrame only) in download action
	 * 
	 * @param _owner		the owner to display messages
	 * @param _providerName	the provider to store the files
	 * @param _fileNames	the names of the files to update
	 * @param _directoryName the directory to store the files (File Lock PEA)
	 * 						or null for tmp directory (all other PEAs)
	 */
	protected DownloadWorker(Window _owner, String _providerName, 
			String[] _fileNames, String _directoryName) {
		
		this.owner = _owner;
		if (this.owner == null) {
			this.owner = JREProperties.getMainWindow();
		}
		this.providerName = _providerName;
		this.fileNames = _fileNames;
		if (_directoryName != null) {
			// only absolute paths are allowed
			if (Paths.get(_directoryName).isAbsolute()) {
				this.directoryName = _directoryName;
			} else {
				new UnexpectedValueException("String", "_directoryName", "is not absolute path").printDescription();
			}
		}
		//TestLog.v(this.getClass(), "Download " + providerName);for(String s:_fileNames)TestLog.v(this.getClass(), s);
	}

	@Override
	protected Boolean doInBackground() throws Exception {		

		publish(providerName + ": " + PeaProperties.getVmBridge().translate("connect_cloud"));
		TestLog.v(this.getClass(), providerName + ": " + PeaProperties.getVmBridge().translate("connect_cloud"));
		
		isRunning = true;
		// Try to authorize:
		String[] params = CloudPropertyHandler.getProviderLoginParameters(providerName);
		if (params == null) {
			TestLog.e(DownloadWorker.class, "Missing login parameters for " + providerName, 5);
			providerError = providerName + ": " + PeaProperties.getVmBridge().translate("unexpected_error") + " - login parameters missing";
			return false;
		}
		
		StorageProvider provider = null;
		if (params[2] != null) { // WebDAV base path
			StorageProvider sp = CloudControl.getOpenProvider(providerName);
			if (sp == null) {
				 sp = new WebDavProvider(providerName);
			}
			if (sp instanceof WebDavProvider) {
				provider = (WebDavProvider) sp;
			} else {
				new UnexpectedValueException("StorageProvider","sp","is not a WebDAV provider").printDescription();
				providerError = providerName + ": " + PeaProperties.getVmBridge().translate("unexpected_error") + " - not a WebDAV provider";
				return false;
			}
			( (WebDavProvider) provider).setWebDavBasePath(params[2]);
			//TestLog.v(this.getClass(), "WebdavbasePath: " + ( (WebDavProvider) p).getWebDavBasePath());
		} else {
			//TODO Dropbox...
			new UnexpectedValueException("String", "params[2]", "is null - not a WebDAV provider...").printDescription();
			providerError = providerName + ": " + PeaProperties.getVmBridge().translate("unexpected_error") + " - not a WebDAV provider";
			return false;
		}
		char[] psw = null;
		if (owner instanceof PeaLockFrame) {
			String pswString = CloudPropertyHandler.getPlainPasswordFromProvider(providerName, 
					Converter.bytes2hex(KeyDerivation.getSalt()), 
					Converter.bytes2hex(PeaProperties.getAlgoParamString()));
			if (pswString != null) {
				psw = pswString.toCharArray();
			}
		} else {
			psw = Converter.bytes2chars(WebDavLogin.getTemporaryStoredPassword(providerName));
		}
		if (psw == null) {
			TestLog.ve(DownloadWorker.class, "Missing password for " + providerName, 5);
			// get password from password dialog:
			psw = PeaDialog.showPasswordDialog(JREProperties.getMainWindow(), providerName + ": " + PeaProperties.getVmBridge().translate("enter_cloud_password"), providerName, false);
			if (psw != null) {
				if (Comparator.compare(psw, "cAnCeLlEd".toCharArray())) {
					providerError = providerName + ": Password input cancelled";
					TestLog.v(getClass(), "Process cancelled");
					return false;
				}
				String pswString = new String(psw);
				if (pswString != null && owner instanceof PeaLockFrame) { // store password
					if (CipherStuff.getSessionKeyCrypt().getKey() != null) {						
						CloudPropertyHandler.storeEncryptedPasswordFromProvider( 
								CloudPropertyHandler.encryptPassword(pswString), 
								providerName, Converter.bytes2hex(KeyDerivation.getSalt()), 
								Converter.bytes2hex(PeaProperties.getAlgoParamString()), null);		
					} else {
						// store for this session to set later
						WebDavLogin.storePassword(providerName, Converter.chars2bytes(psw));
					}
				} else {
					// store for this session to set later
					WebDavLogin.storePassword(providerName, Converter.chars2bytes(psw));
				}
			} else { 
				TestLog.e(DownloadWorker.class, "could not get password for " + providerName);
				TestLog.ve(DownloadWorker.class, "could not get password for " + providerName, 5);
				providerError = providerName + ": " + PeaProperties.getVmBridge().translate("unexpected_error") + " - password missing";
				return false;
			}
		}

		String authError = provider.authorize(params[0], psw, null);
		if (authError != null) {
			TestLog.e(MethodHandles.lookup().lookupClass(), providerName + ": authorization failed");
			providerError =  authError;// reason
			return false;
		} else {
			publish(providerName + ": " + PeaProperties.getVmBridge().translate("successfully_authorized"));
		}		
		
		ScheduledExecutorService executor =
			    Executors.newSingleThreadScheduledExecutor();

			Runnable periodicTask = new Runnable() {
				String workingChar1 = "/";
				String workingChar2 = "—";//END OF GUARDED AREA (U+0097)
				String workingChar3 = "\\";
				String workingChar = workingChar1;
			    public void run() {
			    	if (isRunning == true) {
			    		if (workingChar.equals(workingChar1)) {
			    			workingChar = workingChar2;
			    		} else if (workingChar.equals(workingChar2)) {
			    			workingChar = workingChar3;
			    		} else {
			    			workingChar = workingChar1;
			    		}
			    		// Invoke method(s) to do the work
			    		publish(providerName + ": " + PeaProperties.getVmBridge().translate("download_from_cloud") + " " + workingChar);
			    		//doPeriodicWork();
			    	} else {
			    		executor.shutdown();
			    	}
			    }
			};
			executor.scheduleAtFixedRate(periodicTask, 1000, 250, TimeUnit.MILLISECONDS);
		
		int fileNamesLen = fileNames.length;
		// cut temp dir and provider name from all files:
		String[] newFileNames = new String[fileNamesLen];
		//String tmpDir = System.getProperty("java.io.tmpdir");
		for (int i = 0; i < fileNamesLen; i++) {
			String fileName = fileNames[i];
			if (fileName == null) {
				new UnexpectedValueException("String", "fileName", "is null...").printDescription();
				continue;
			}
			if (fileNames[i].contains(File.separator)) {
				newFileNames[i] = fileName.substring(fileName.lastIndexOf(File.separator) + 1);
			} else {
				newFileNames[i] = fileName;
			}
		}
		fileNames = newFileNames;
		if (fileNames == null) {
			new UnexpectedValueException("String[]", "fileNames", "is null...").printDescription();
			return false;
		}
		if (this.directoryName == null) { // all PEAs except File Lock PEA
			this.directoryName = System.getProperty("java.io.tmpdir") 
					+ File.separator + providerName;
		}

		// Download files from cloud
		String downloadError = provider.downloadFiles(fileNames, 
				this.directoryName);
		if (downloadError != null) {
			TestLog.e(MethodHandles.lookup().lookupClass(), "Download failed for provider " + providerName + "\n" + downloadError);
			providerError = downloadError; // list of failed files with reason
			// check if all files failed:
			String[] lines = downloadError.split("\n");
			if (lines.length - 1 == fileNamesLen) {
				TestLog.e(MethodHandles.lookup().lookupClass(), "All files failed for " + providerName);
				return false;
			} else {
				// some files were successfully downloaded, continue...
			}
		} else {
			TestLog.v(this.getClass(), "Download success: " + providerName);
			publish(providerName + ": " + PeaProperties.getVmBridge().translate("successfully_downloaded"));
		}

		// check salt for all files and add errors to providerError
		byte[] salt = KeyDerivation.getSalt();
		byte[] algoParam = PeaProperties.getAlgoParamString();
		byte[] fileIdentifier = Attachments.getFileIdentifier();
		if (salt == null || algoParam == null || fileIdentifier == null) {
			new UnexpectedValueException("byte[]", "salt/algoParam/fileId", "is null)").printDescription();
			providerError = PeaProperties.getVmBridge().translate("unexpected_error");
			return false;
		}
		errorBuilder = new StringBuilder();
		for (int i = 0; i < fileNamesLen; i++) {
			String downloadedFileName = null;
			if (PeaProperties.getFileType().equals("file")) { // not in tmp directory but in chosen one
				downloadedFileName = directoryName + File.separator + fileNames[i];
			} else {
				downloadedFileName = CloudControl.getTmpFileNameForCloudFile(fileNames[i], providerName, null);
			}
			// check the salt and algorithms and parameters for every file
			File f = new File(downloadedFileName);
			byte[][] fileAttachments = AttachmentHandler.getSaltAlgosFileIDFromFile(f, false);
			// TODO key file property
			if (Arrays.equals(fileIdentifier, fileAttachments[2])
					// version 1 file with old fileIdentifier
					|| (Arrays.equals(fileAttachments[2], AttachmentHandler.getVersion1ID()))) {
				
				if (Arrays.equals(salt, fileAttachments[0])) {
					if (Arrays.equals(algoParam, fileAttachments[1])) {
						// OK
						successFileNames.add(downloadedFileName);
					} else {
						// check old version
						if (Arrays.equals(algoParam, AlgoParamHandler.getV1Bytes())
								&& fileAttachments[1] == null) {
							// OK
							successFileNames.add(downloadedFileName);
						} else {
							// wrong algorithm parameter string
							TestLog.e(MethodHandles.lookup().lookupClass(), "Algorithms or parameters differ for " + fileNames[i] +" of "  + providerName
									+ ": " + new String(algoParam) + " differs from " + new String(fileAttachments[1]));
							errorBuilder.append("\n" + fileNames[i] + " - " 							
							+ PeaProperties.getVmBridge().translate("algorithm_parameter") 
							+ " " + PeaProperties.getVmBridge().translate("invalid"));	
							// remove from properties:
							String propError = CloudPropertyHandler.removeFileOfProvider(providerName, fileNames[i], 
									Converter.bytes2hex(salt), Converter.bytes2hex(algoParam), null);
							if (propError != null) {
								TestLog.ve(DownloadWorker.class, "Couldn't remove file " + fileNames[i] + " - " + propError);
							}
							continue;	
						}
					}
					
				} else {
					TestLog.e(MethodHandles.lookup().lookupClass(), "Salt differs for " + fileNames[i] +" of "  + providerName);
					TestLog.ve(DownloadWorker.class, "found salt in " +fileNames[i] + ": " + Converter.bytes2hex(fileAttachments[0]));
					TestLog.ve(DownloadWorker.class, "used salt: " + Converter.bytes2hex(salt));
					
					errorBuilder.append("\n" + fileNames[i] + " - " + PeaProperties.getVmBridge().translate("salt_differs_from_first_selected_file") );
					// remove from properties:
					TestLog.ve(DownloadWorker.class, "Try to remove from properties - this might fail...");
					String propError = CloudPropertyHandler.removeFileOfProvider(providerName, fileNames[i], 
							Converter.bytes2hex(salt), Converter.bytes2hex(algoParam), null);
					if (propError != null) {
						TestLog.ve(DownloadWorker.class, "Couldn't remove file " + fileNames[i] + " - " + propError);
					}
					continue;	
				}
				
			} else {
				TestLog.e(MethodHandles.lookup().lookupClass(), "File identifier failed for " + fileNames[i] +" of "  + providerName);
				errorBuilder.append("\n" + fileNames[i] + " - " 
						+ PeaProperties.getVmBridge().translate("not_encrypted_with_this_archive"));
				// remove from properties:
				String propError = CloudPropertyHandler.removeFileOfProvider(providerName, fileNames[i], 
						Converter.bytes2hex(salt), Converter.bytes2hex(algoParam), null);
				if (propError != null) {
					TestLog.ve(DownloadWorker.class, "Couldn't remove file " + fileNames[i] + " - " + propError);
				}
				continue;
			}
		} 
		//add to properties, salt and scheme are checked
		// get file names only:
		int successLen = successFileNames.size();
		String[] fileNamesForProperties = new String[successLen];
		for (int i = 0; i < successLen; i++) {
			fileNamesForProperties[i] = CloudControl.getFileNameFromTmpFile(successFileNames.get(i));
		}

		String addError = CloudPropertyHandler.addFileNamesToProperties(providerName, fileNamesForProperties, 
				Converter.bytes2hex(KeyDerivation.getSalt()), Converter.bytes2hex(PeaProperties.getAlgoParamString()));
		if (addError != null) {
			TestLog.e(DownloadWorker.class, "Error occurred when storing file names in properties: " + addError);
		}
		if (errorBuilder.length() > 0) {
			if (providerError != null && providerError.length() > 0) {
				providerError = providerError + errorBuilder;
			} else {				
				providerError = PeaProperties.getVmBridge().translate("error_in_decryption_process") + "\n" + errorBuilder;
			}
		} 			
		if (providerError != null && providerError.length() > 0) {
			return false;
		} else {
			return true;
		}
	}
	
    @Override
    protected void process(List <String> infos) {
    	String info = infos.get(infos.size() - 1);
    	CloudMenu.displayMessage(owner, info, false); // not error
    }

	@Override
	protected void done() { 
		
		isRunning = false;
		boolean updateSuccess = false;
		try {
			updateSuccess = get();
		} catch (InterruptedException e) {
			TestLog.e(MethodHandles.lookup().lookupClass(), "Interruption of cloud connection " + providerName);
			TestLog.ve(getClass(), new ExceptionHelper(e).getInfos());
			CloudMenu.displayMessage(owner, PeaProperties.getVmBridge().translate("authorization_failed")  + " (interruption)", true);
			PeaDialog.showMessage(owner, 
					PeaProperties.getVmBridge().translate("download_failed")  + " (interruption)"
					+ "\n" + providerError, 
					PeaProperties.getVmBridge().translate("error"), 0);
		} catch (ExecutionException e) {
			TestLog.e(MethodHandles.lookup().lookupClass(), "Execution exception of download process in " + providerName);
			TestLog.ve(getClass(), new ExceptionHelper(e).getInfos());
			CloudMenu.displayMessage(owner, PeaProperties.getVmBridge().translate("download_failed")  + " (excution)", true);
			if (providerError == null) {
				if (errorBuilder != null) {
					providerError = new String(errorBuilder);
				}
			}
			if (providerError != null) {
				TestLog.ve(MethodHandles.lookup().lookupClass(), "Error: " + providerError);
				PeaDialog.showMessage(owner, 
						PeaProperties.getVmBridge().translate("download_failed")  + " (excution)"
								+ "\n" + providerError, 
								PeaProperties.getVmBridge().translate("error"), 0);
				TestLog.ve(DownloadWorker.class, "ExecutionException " + e.getLocalizedMessage(), 5);
			}
		}		
		if (updateSuccess == true) {
			CloudMenu.displayMessage(owner, PeaProperties.getVmBridge().translate("successfully_downloaded"), false);
		} 
		// Decrypt the files and load to PEA:
		StringBuilder errorBuilder = new StringBuilder();
		for (String successFile : successFileNames) {
			if (PeaControl.getDialog().getLockFrame() instanceof PeaLockFrame) {
				String decryptError = ((PeaLockFrame) PeaControl.getDialog().getLockFrame()).decryptAndLoadFileToView(successFile);
				if (decryptError != null) {
					TestLog.e(MethodHandles.lookup().lookupClass(), "Decryption error for " + successFile +" of "  + providerName);
					errorBuilder.append("\n" + decryptError);
					continue;
				} else {
					TestLog.v(this.getClass(), "successfully loaded " + successFile + " from " + providerName);
					// add to CloudFileModel
					if ( ! PeaProperties.getFileType().equals("file")) {
						((FileTypePanel)JREProperties.getTypePanel()).getCloudFilePanel().getCloudControl().getCloudFileModel().addUsedFileToProvider(
							CloudControl.getProviderNameFromTmpFile(successFile), 
							CloudControl.getFileNameFromTmpFile(successFile));
					}
				}
			}
		}
		
		if (providerError != null && providerError.length() > 0) {
			providerError = providerError + errorBuilder;// no extra head line
		} else {
			if (errorBuilder != null && errorBuilder.length() > 0) {
				providerError = providerName  + PeaProperties.getVmBridge().translate("error_in_decryption_process") + ":\n" // head line
					+ new String(errorBuilder);
			}
		}
		if (providerError != null && providerError.length() > 0) {
			PeaDialog.showMessage(owner, providerError, PeaProperties.getVmBridge().translate("download_failed"), 0);
		}
	}
}
