package cologne.eck.all_peas.cloud;

import java.util.ArrayList;

/*
 * Peafactory - Production of Password Encryption Archives
 * Copyright (C) 2020  Axel von dem Bruch
 * 
 * This library is free software; you can redistribute it and/or modify it 
 * under the terms of the GNU General Public License as published 
 * by the Free Software Foundation; either version 2 of the License, 
 * or (at your option) any later version.
 * This library is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
 * or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU General Public License for more details.
 * See:  http://www.gnu.org/licenses/gpl-2.0.html
 * You should have received a copy of the GNU General Public License 
 * along with this library.
 */

import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;

import cologne.eck.all_peas.data.PeaProperties;
import cologne.eck.tools.TestLog;



public class CloudFileModel {
	
	private String salt;
	private String algoParams;
	
	/**
	 * Holds two HashMaps: one for initial files (encrypted files in password dialog)
	 * and one for used files (successfully opened and used files): in Calendar Lock PEA decrypted, 
	 * in Notebook PEA encrypted as Tab (password not checked)
	 */
	CloudFileModel(String _salt, String _algoParams) {
		this.setSalt(_salt);
		this.setAlgoParams(_algoParams);
		if (PeaProperties.getFileType().equals("file")) {
//			new UnexpectedValueException("Method", "", "falsely used for File Lock PEA").printDescription();	
		}
	}
	
	/** File names of all providers for all schemes and salts
	 * manually opened from cloud, downloaded in tmp 
	 */
	private static HashSet<String> downloadedInitialCloudFiles = new HashSet<String>();
	
	/** provider name and file names of provider
	 * unproven, loaded from cloud.properties
	 */
	private HashMap<String, HashSet<String>> initialCloudFileMap = new HashMap<String, HashSet<String>>();
	
	/** opened provider name and selected and checked file names of provider
	 * In Calendar Lock PEA: decrypted, in Notebook PEA: encrypted (password not checked)
	 */
	private HashMap<String, HashSet<String>> usedCloudFileMap = new HashMap<String, HashSet<String>>();
	
	/**
	 * Maps all uploaded cloud file name with their local origins 
	 */
	private HashMap<String, String> cloudLocalMap = new HashMap<String, String>();
	
	
	//================= initial files (encrypted, in FileTypePanel in PswDialogView) ===============
	
	/**
	 * Add file names to one provider in the password dialog with encrypted files.
	 * If the provider does not yet exist, a new map entry
	 * is created, otherwise the file names are added to the
	 * existing file names. Providers with no file names are allowed.
	 * 
	 * @param providerName	the provider name to add the file names
	 * @param fileNames		the file names to add
	 * 
	 * @return	true, if file names were added
	 */
	protected static boolean addDownloadedInitialFileNamesToProvider(String[] fileNames) {
		if (PeaProperties.getFileType().equals("file")) {
			TestLog.ve(CloudFileModel.class, "Method falsely used for File Lock PEA", 5);	
			return false;
		}
		if (fileNames == null) {
			TestLog.ve(CloudFileModel.class, "No file names ", 5);
			return false;
		}
		ArrayList<String> list = new ArrayList<String>();
		String tempDir = CloudControl.getTmpDir();
		for (String s : fileNames) {
			if (s == null) {
				TestLog.ve(CloudFileModel.class, "Null file name ");
				continue;
			}
			if ( ! s.startsWith(tempDir)) {
				TestLog.ve(CloudFileModel.class, "Not a cloud file: " + s, 5);
				continue;
			}
			list.add(s);
		}
		boolean changed = downloadedInitialCloudFiles.addAll(list);
		if (changed == false) {
			TestLog.v(CloudFileModel.class, "No file was added to initial cloud files...");
		}
		return changed;
	}
	
	/**
	 * Get the downloaded initial set of cloud files 
	 * of all providers for all schemes and salts.
	 * These files are already downloaded in tmp directory
	 * 
	 * @return	the initial cloud file set
	 */
	protected static HashSet<String> getDownloadedInitialCloudFiles() {
		if (PeaProperties.getFileType().equals("file")) {
			TestLog.ve(CloudFileModel.class, "Method falsely used for File Lock PEA", 5);	
		}
		return downloadedInitialCloudFiles;
	}

	/**
	 * Add file names to one provider in the password dialog with encrypted files.
	 * If the provider does not yet exist, a new map entry
	 * is created, otherwise the file names are added to the
	 * existing file names. Providers with no file names are allowed.
	 * 
	 * @param providerName	the provider name to add the file names
	 * @param fileNames		the file names to add
	 * 
	 * @return	true, if file names were added
	 */
	protected boolean addInitialFileNamesToProvider(String providerName, String[] fileNames) {
		if (PeaProperties.getFileType().equals("file")) {
			TestLog.ve(CloudFileModel.class, "Method falsely used for File Lock PEA", 5);
		}
		if (initialCloudFileMap.containsKey(providerName)){
			if (fileNames == null || fileNames.length == 0) {
				return false;
			}
			HashSet<String> existingFileNames = initialCloudFileMap.get(providerName);
			if (existingFileNames == null || existingFileNames.size() == 0) {
				initialCloudFileMap.put(providerName, new HashSet<>(Arrays.asList(fileNames)));
				if (fileNames == null || fileNames.length == 0) {
					return false;
				} else {
					return true;
				}
			} else {
				HashSet<String> set = initialCloudFileMap.get(providerName);
				int oldSetLen = set.size();
				int filesLen = fileNames.length;
				for (int i = 0; i < filesLen; i++) {
					 set.add(fileNames[i]);
				}
				initialCloudFileMap.put(providerName, set);
				if (set.size() > oldSetLen) {
					return true;
				} else {
					return false;
				}
			}
		} else {			
			if (fileNames == null || fileNames.length == 0) {
				return false;
			} else {
				initialCloudFileMap.put(providerName, new HashSet<>(Arrays.asList(fileNames)));
				return true;
			}
		}		
	}
	
	/**
	 * Remove a file name from the list of initial (encrypted) file
	 * names of a cloud provider 
	 * 
	 * @param providerName	the provider name, the file is to be removed
	 * @param fileName		the file name to remove
	 * 
	 * @return true, if file name was removed, false if it was not present
	 */
	protected boolean removeInitialFileNameFromProvider(String providerName, String fileName) {
		if (PeaProperties.getFileType().equals("file")) {
			TestLog.ve(CloudFileModel.class, "Method falsely used for File Lock PEA", 5);	
		}
		if (initialCloudFileMap.containsKey(providerName)){
			
			if (fileName == null) {
				return false;
			}
			HashSet<String> existingFileNames = initialCloudFileMap.get(providerName);
			if (existingFileNames == null || existingFileNames.size() == 0) {
				return false;

			} else {
				HashSet<String> set = initialCloudFileMap.get(providerName);
				boolean removed = set.remove(fileName);
				initialCloudFileMap.put(providerName, set);
				return removed;
			}
		} else {			
			return false;
		}	
	}
	
	/**
	 * Remove a provider with all files from 
	 * the list of initial (encrypted) file
	 * 
	 * @param providerName	the provider name to be removed
	 * 
	 * @return true, if provider was removed, false if it was not present
	 */
	protected boolean removeInitialProvider(String providerName) {
		if (PeaProperties.getFileType().equals("file")) {
			TestLog.ve(CloudFileModel.class, "Method falsely used for File Lock PEA", 5);
		}
		if (initialCloudFileMap.containsKey(providerName)){
			initialCloudFileMap.remove(providerName);
			return true;
		} else {			
			return false;
		}	
	}

	/**
	 * Get the initial map of cloud files (provider name - file names). 
	 * These file are unproven and only loaded from cloud properties
	 * 
	 * @return	the initial (unchecked) cloud file map (provider name - file names)
	 */
	protected HashMap<String, HashSet<String>> getInitialCloudFileMap() {
		if (PeaProperties.getFileType().equals("file")) {
			TestLog.ve(CloudFileModel.class, "Method falsely used for File Lock PEA", 5);	
		}
		return initialCloudFileMap;
	}

	/**
	 * Set the initial map of cloud files (provider name - file names). 
	 * These file are unproven and only loaded from cloud properties
	 * 
	 * @param initialCloudFileMap	the initial (unchecked) cloud file map (provider name - file names)
	 */
/*	protected void setInitialCloudFileMap(HashMap<String, HashSet<String>> _initialCloudFileMap) {
		if (PeaProperties.getFileType().equals("file")) {
			new UnexpectedValueException("Method", "", "falsely used for File Lock PEA").printDescription();	
		}
		this.initialCloudFileMap = _initialCloudFileMap;
	}*/	
	
	//================= used files (successfully opened in cloud, downloaded in tmp directory) ===============
	
	/**
	 * Get the map of opened cloud files (provider name - file names). 
	 * 
	 * @return 	usedCloudFileMap	the map of opened cloud files (provider name - file names)
	 * 			without tmp path
	 */
	public HashMap<String, HashSet<String>> getUsedCloudFileMap() {
		if (PeaProperties.getFileType().equals("file")) {
			TestLog.ve(CloudFileModel.class, "Method falsely used for File Lock PEA", 5);
		}
		return usedCloudFileMap;
	}

	/**
	 * Set the map of opened cloud files (provider name - file names) without tmp path
	 * 
	 * @param usedCloudFileMap	the map of opened cloud files without tmp path
	 * 							(provider name - file names)
	 */
/*	public void setUsedCloudFileMap(HashMap<String, HashSet<String>> _usedCloudFileMap) {
		this.usedCloudFileMap = _usedCloudFileMap;
	} */
	
	/**
	 * Get all opened file names of one provider without tmp path
	 * 
	 * @param providerName	the name of the provider
	 * 
	 * @return	all used file names of this provider without tmp path
	 */
	protected String[] getUsedFilesOfProvider(String providerName) {
		if (PeaProperties.getFileType().equals("file")) {
			TestLog.ve(CloudFileModel.class, "Method falsely used for File Lock PEA", 5);	
		}
		HashSet<String> set = this.usedCloudFileMap.get(providerName);
		if (set != null) {
			return set.toArray(new String[set.size()]);
		} else {
			return null;
		}
	}
	
	/**
	 * Add a file name to the list of used file names of a cloud provider
	 * 
	 * @param providerName	the provider name, the file name is added to
	 * @param usedFileName	the file name to add without tmp path
	 * 
	 * @return	true if file name was added, false if already present
	 */
	public boolean addUsedFileToProvider(String providerName, String usedFileName) {
		if (PeaProperties.getFileType().equals("file")) {
			TestLog.ve(CloudFileModel.class, "Method falsely used for File Lock PEA", 5);
		}
		if (providerName == null) {
			TestLog.e(CloudFileModel.class, "Missing provider ");
			return false;
		}
		if (usedFileName == null) {
			TestLog.e(CloudFileModel.class, "Missing file name to add to " + providerName);
			return false;
		}
		
		// add to properties.
		String[] filesToAdd = {usedFileName};
		String error = CloudPropertyHandler.addFileNamesToProperties(providerName, filesToAdd, salt, algoParams);
		if (error != null) {
			TestLog.e(CloudFileModel.class, "Could not store file names in properties for " + providerName + ": " + error);
			TestLog.ve(CloudFileModel.class, error, 5);
		}
		
		if (usedCloudFileMap.get(providerName) == null) {
			HashSet<String> set = new HashSet<String>();
			set.add(usedFileName);
			usedCloudFileMap.put(providerName, set);			
			return true;
		}
		return usedCloudFileMap.get(providerName).add(usedFileName);
	}
	
	
	
	/**
	 * Remove a file name from the list of used (encrypted) file
	 * names of a cloud provider 
	 * 
	 * @param providerName	the provider name, the file is to be removed
	 * @param fileName		the file name to remove
	 * 
	 * @return true, if file name was removed, false if it was not present
	 */
	protected boolean removeUsedFileNameFromProvider(String providerName, String fileName) {
		if (PeaProperties.getFileType().equals("file")) {
			TestLog.ve(CloudFileModel.class, "Method falsely used for File Lock PEA", 5);	
		}
		if (usedCloudFileMap.containsKey(providerName)){
			
			if (fileName == null) {
				return false;
			}
			HashSet<String> existingFileNames = usedCloudFileMap.get(providerName);
			if (existingFileNames == null || existingFileNames.size() == 0) {
				return false;

			} else {
				HashSet<String> set = usedCloudFileMap.get(providerName);
				boolean removed = set.remove(fileName);
				usedCloudFileMap.put(providerName, set);
				return removed;
			}
		} else {			
			return false;
		}	
	}
	
	/**
	 * Remove a file from set of used file names and from properties
	 * 
	 * @param providerName		provider name
	 * @param fileNameToRemove	the file name to remove without the tmp path
	 * @return
	 */
/*	public boolean removeUsedFileFromProvider(String providerName, String fileNameToRemove) {
		if (PeaProperties.getFileType().equals("file")) {
			new UnexpectedValueException("Method", "", "falsely used for File Lock PEA").printDescription();	
		}
		// remove from properties.
		CloudPropertyHandler.removeFileOfProvider(providerName, fileNameToRemove);//.setSingleProperty(providerName + "_files", );
		
		if (usedCloudFileMap.get(providerName) != null) {
			HashSet<String> set = usedCloudFileMap.get(providerName);
			boolean removed = set.remove(fileNameToRemove);
			usedCloudFileMap.put(providerName, set);			
			return removed;
		} else {
			return false;
		}
	} */
	
	/**
	 * Remove a provider with all files from model
	 * 
	 * @param providerName	the provider name to be removed
	 * 
	 * @return	true if provider was removed
	 */
	protected boolean removeUsedProvider(String providerName) {
		if (PeaProperties.getFileType().equals("file")) {
			TestLog.ve(CloudFileModel.class, "Method falsely used for File Lock PEA", 5);
		}
		 HashSet<String> removedSet =  usedCloudFileMap.remove(providerName);
		 if (removedSet == null) {
			 return false;
		 } else {
			 return true;
		 }
	}
	
	/**
	 * Set the origin local file for a cloud file 
	 * 
	 * @param cloudFileName		the local backup of the uploaded cloud file in the form
	 * 							System.getProperty("java.io.tmpdir") + File.separator + provider + File.separator + fileName
	 * @param originLocalName	the origin local file name, that was uploaded
	 */
	protected void addToCloudLocalMap(String cloudFileName, String originLocalName) {
		if (PeaProperties.getFileType().equals("file")) {
			TestLog.ve(CloudFileModel.class, "Method falsely used for File Lock PEA", 5);	
		}
		cloudLocalMap.put(cloudFileName, originLocalName);
	}
	
	/**
	 * Get the local origin of an uploaded cloud file
	 * 
	 * @param cloudFileName	the uploaded cloud file in the form
	 * 						System.getProperty("java.io.tmpdir") + File.separator + provider + File.separator + fileName

	 * @return	the local origin of the cloud file with absolute path
	 */
	public String getLocalOrigin( String cloudFileName) {
		if (PeaProperties.getFileType().equals("file")) {
			TestLog.ve(CloudFileModel.class, "Method falsely used for File Lock PEA", 5);	
		}
		return cloudLocalMap.get(cloudFileName);
	}
	
	/**
	 * Get the salt as hex string 
	 * 
	 * @return	the salt as hex string 
	 */
	public String getSalt() {
		return salt;
	}

	/**
	 * Set the salt 
	 * @param salt the salt to set as String
	 */
	public void setSalt(String salt) {
		this.salt = salt;
	}
	
	/**
	 * Get the algorithm-parameter string as hex string 
	 * 
	 * @return	the algorithm-parameter string as hex string 
	 */
	public String getAlgoParams() {
		return algoParams;
	}

	/**
	 * Set the String with algorithms and parameters 
	 * 
	 * @param algoParams  String with algorithms and parameters 
	 */
	public void setAlgoParams(String algoParams) {
		this.algoParams = algoParams;
	}
}
