package cologne.eck.all_peas.cloud;

/*
 * Peafactory - Production of Password Encryption Archives
 * Copyright (C) 2020  Axel von dem Bruch
 * 
 * This library is free software; you can redistribute it and/or modify it 
 * under the terms of the GNU General Public License as published 
 * by the Free Software Foundation; either version 2 of the License, 
 * or (at your option) any later version.
 * This library is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
 * or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU General Public License for more details.
 * See:  http://www.gnu.org/licenses/gpl-2.0.html
 * You should have received a copy of the GNU General Public License 
 * along with this library.
 */

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Properties;
import java.util.Set;

import org.bouncycastle.crypto.BlockCipher;

import cologne.eck.all_peas.data.PeaProperties;
import cologne.eck.peafactory.crypto.AlgoParamHandler;
import cologne.eck.peafactory.crypto.CipherStuff;
import cologne.eck.tools.Comparator;
import cologne.eck.tools.Converter;
import cologne.eck.tools.ExceptionHelper;
import cologne.eck.tools.TestLog;


/**
 * 
 * @author ax
 *
 * Properties for cloud access: 
 * Keys start with the provider name + underscore - e.g. woelkli_
 * possible appendices for login are: 
 * user, password, webdav, accesstoken, otherparam - e.g. woekli_user
 * 
 * Sub sets for a provider are defined by the salt and the algorithm-parameter string, 
 * they are marked by an integer extension of the provider name - e.g. woelkli_2_
 * Possible appendices for sub sets are:
 * salt, algoparam, password, files  -  e.g. woekli_2_algoparam
 * Passwords are stored encrypted as hexadecimal strings by the derived key. 
 * Files are stored in a comma separated list without whitespace - e.g. 
 * global_1_files=Cal5-6.ics,Cal7.ics
 */
public class CloudPropertyHandler {

	/*
	 * The property file for all cloud properties
	 */
	private static final String propertyFileName = System.getProperty("user.dir") + File.separator 
			+ "resources" + File.separator + "cloud.properties";



	/**
	 * Open InputStream, load Properties, close stream
	 * 
	 * @return	the loaded Properties
	 */
	private static Properties loadProperties() {
		FileInputStream inputStream = null;

		Properties prop = new Properties();
		File file = new File(propertyFileName);
		if ( ! file.exists()) {
			file.getParentFile().mkdirs();
			try {
				TestLog.v(CloudPropertyHandler.class, "Create cloud property file...");
				file.createNewFile();
			} catch (IOException e) {
				TestLog.e(CloudPropertyHandler.class, "Could not create: " + propertyFileName);;
				return null;
			}
		}
		try {
			inputStream = new FileInputStream(file);
			prop.load(inputStream);
			if (inputStream != null) {
				inputStream.close();
			}
		} catch (IOException e) {
			TestLog.e(CloudPropertyHandler.class, "Access to property file failed...");
			TestLog.ve(CloudPropertyHandler.class, new ExceptionHelper(e).getInfos());
			return null;
		}
		return prop;
	}
	/**
	 * Store the new properties and close the output stream
	 * 
	 * @param prop			the properties to store
	 * 
	 * @return	a localized error message or null for success
	 */
	private static String storeProperties(Properties prop) {

		// create a property file if not already exists
		File file = new File(propertyFileName);
		if (! file.exists()) {
			// create directory
			file.getParentFile().mkdirs();
			// create file
			try {
				file.createNewFile();
				TestLog.v(CloudPropertyHandler.class, "Create property file");
			} catch (IOException e) {
				TestLog.e(CloudPropertyHandler.class, "Can't create file: " + propertyFileName);
				TestLog.ve(CloudPropertyHandler.class, new ExceptionHelper(e).getInfos());
				return PeaProperties.getVmBridge().translate("unexpected_error") + ": " + e.getLocalizedMessage();
			}
		} 
		// Set the new properties
		FileOutputStream outputStream = null;
		try {
			outputStream = new FileOutputStream(file);

			prop.store(outputStream, "Cloud properties for all PEAs");
			if (outputStream != null) {
				outputStream.close();
			}
		} catch (FileNotFoundException e) {
			TestLog.e(CloudPropertyHandler.class, new ExceptionHelper(e).getInfos());
			return PeaProperties.getVmBridge().translate("unexpected_error") + ": " + e.getLocalizedMessage();
		} catch (IOException e) {
			TestLog.e(CloudPropertyHandler.class, new ExceptionHelper(e).getInfos());
			return PeaProperties.getVmBridge().translate("unexpected_error") + ": " + e.getLocalizedMessage();
		}
		return null;
	}

	/**
	 * Set the login properties for one provider. Existing providers are overwritten.
	 * The current used salt and algorithm-parameter string is also set for this provider. 
	 * The file names and password must be set extra along with salt and algorithms. 
	 * 
	 * @param providerName	the user defined name of the provider
	 * @param userName		the user name
	 * @param webDavBasePath	the WebDav base path of the provider
	 * 
	 * @return	error message or null for success
	 */
	public static String setNewProvider(String providerName, String userName, //String password, 
			String webDavBasePath) {

		// set properties for all PEAs
		Properties prop = loadProperties();

		// check if providerName already exists:
		if (prop.getProperty(providerName + "_user") != null) {
			TestLog.v(CloudPropertyHandler.class, "provider to set already exists: " + providerName);
		}
		prop.put(providerName + "_user", userName);

		// check if base path contains peaDirectory and cut
		prop.put(providerName + "_webdav", webDavBasePath);

		// Set the new properties
		return storeProperties(prop);
	}

	/**
	 * Get all saved login parameters for one cloud provider:
	 * user name, password, WebDAV base path or an empty array of strings
	 * and required other parameters e.g. access tokens or null.
	 * This method will never return null. 
	 * 
	 * @param providerName	the provider, to get the parameters for
	 * 
	 * @return	the parameters as an array of strings where
	 * 			index 0 is the user name or null
	 * 			index 1 is always null (for password) and must be set extra
	 * 			index 2 is the WebDAV base path or null, this marks
	 * 					the provides as WebDAV provider
	 * 			index 3 is other required parameter e.g. access token
	 * 			index 4 is one more required parameter
	 */
	public static String[] getProviderLoginParameters(String providerName) {

		String[] params = new String[5];				
		Properties prop = loadProperties();
		if (prop == null) {
			return null;
		}
		Set<Object> keys = prop.keySet();
		for(Object k:keys){
			String key = (String) k;
			if (key.startsWith(providerName + "_")) {
				String keyParam = key.substring(key.indexOf('_') + 1);
				if (keyParam.equals("user")) {
					params[0]= prop.getProperty(key);
				} else if (keyParam.equals("webdav")) {
					params[2]= prop.getProperty(key);
				} else if (keyParam.equals("accesstoken")) {
					params[3]= prop.getProperty(key);
				} else if (keyParam.equals("otherparam")) {
					params[4]= prop.getProperty(key);
				}
			}
		}
		return params;
	}

	/**
	 * Get a single property value (like user, password..) for a given provider. 
	 * Note: password is returned in hex encoded, encrypted form.
	 * 
	 * @param providerName	the name of the provider
	 * @param parameterName	the name of the property: valid values are:
	 * 						user, webdav, access_token, other_param. 
	 * 						The password must be accessed by getPlainPasswordFromProvider
	 * 
	 * @return	the value as string or null if an error occurred
	 */
	public static String getSingleLoginProperty(String providerName, String parameterName) {

		if (providerName == null) {
			TestLog.ve(CloudPropertyHandler.class, "providererName is null", 5);
			return null;
		}
		if (parameterName == null) {
			TestLog.ve(CloudPropertyHandler.class, "parameterName is null", 5);
			return null;
		}
		// check for valid parameter:
		if (! (    parameterName.equals("user") // || parameterName.equals("password") 
				|| parameterName.equals("webdav") 
				|| parameterName.equals("accesstoken") || parameterName.equals("otherparam")  ) ) {
			TestLog.ve(CloudPropertyHandler.class, "invalid parameter", 5);
			return null;
		}

		Properties prop = loadProperties();//new Properties();
		if (prop == null) {
			return "Access to property file failed...";
		}

		Set<Object> keys = prop.keySet();
		for(Object k:keys){
			String key = (String) k;
			if (key.startsWith(providerName + "_")) {
				String keyParam = key.substring(key.indexOf('_') + 1);
				if (keyParam.equals(parameterName)) {
					return prop.getProperty(key);
				} 
			}
		}
		TestLog.e(CloudPropertyHandler.class, "Parameter " + parameterName + " for " + providerName + " not found in properties");
		return null;
	}

	/**
	 * Delete all saved login parameters for one cloud provider:
	 * user name, password, WebDAV base path 
	 * 
	 * @param providerName	the provider, to get the parameters for
	 * 
	 * @return	true for success
	 */
	public static boolean deleteProviderParameters(String providerName) {

		Properties prop = loadProperties();

		if (prop == null || prop.isEmpty()) {
			TestLog.ve(CloudPropertyHandler.class, "properties null", 5);
			return false;
		}
		Set<Object> keySet = prop.keySet();
		// avoid java.util.ConcurrentModificationException: convert in array
		Object[] keys = keySet.toArray(new Object[0]);
		for (Object key : keys) {
			if (((String) key).startsWith(providerName + "_")) {
				prop.remove(key);
			}
		}
		// Set the new properties
		String error = storeProperties(prop);
		if (error == null) {
			return true;
		} else {
			TestLog.e(CloudPropertyHandler.class, "Couldn't delete provider... " + providerName);
			return false;
		}
	}

	/**
	 * Remove files from any sub set of a provider
	 * 
	 * @param providerName	the name of the provider
	 * @param fileNames		the file names to remove
	 * @param prop			the Properties or null
	 * 
	 * @return	an error message or null for success
	 */
	static String removeFiles(String providerName, 
			String[] fileNames, Properties prop) {
		// load properties
		if (prop == null) {
			prop = loadProperties();
			if (prop == null) {
				TestLog.e(CloudPropertyHandler.class, "Can not load properties...");
				return "unexpected_error";	
			}
		}	 
		// find the sub set of the files
		String[] subNames = getSubSetNames(prop, providerName);
		// search every file in every sub set and remove if present
		for (String fileName : fileNames) {
			for (String subSet : subNames) {
				String existingFiles = prop.getProperty( subSet + "_files");
				if (existingFiles != null && existingFiles.length() > 0) {
					if (existingFiles.contains(fileName)) {// this is not safe, e.g. the file "yz" is found if file "xyz.txt" is present
						existingFiles = removeFileNameFromCommaSeparatedList(existingFiles, fileName);
						prop.setProperty(subSet + "_files", existingFiles);
					}
				}
			}
		}
		// store properties
		String error = storeProperties(prop);
		if (error != null) {
			return "unexpected_error";	
		}
		return null;
	}

	/**
	 * Get all provider names, that are stored in the property file
	 * 
	 * @return	the user defines provider names as strings or null for failure
	 */
	public static String[] getProviderNames() {
		if (new File(propertyFileName).exists()) {
			ArrayList<String> providerList = new ArrayList<String>();

			Properties prop = loadProperties();

			Set<Object> keys = prop.keySet();
			for(Object k:keys){
				String key = (String) k;
				if (key.endsWith("_user")) {
					providerList.add(key.substring(0, key.lastIndexOf("_user")));
					//TestLog.v(this.getClass(), "Provider added: " + key.substring(0, key.lastIndexOf("_user")) );
				}
			}			
			if (providerList.isEmpty()) {
				return null;
			} else {
				String[] stockArr = new String[providerList.size()];
				return providerList.toArray(stockArr);
			}
		} else {
			TestLog.v(CloudPropertyHandler.class, "cloud property file does not yet exist...");
			return null;
		}
	}

	/**
	 * Create a new sub set of salt/algo-parameters for a provider, 
	 * e.g. woelkli_3_salt, woelkli_3_algoparam, woelkli_3_files. 
	 * The login properties of this provider must have been set before.
	 * 
	 * @param providerName	the name of the provider
	 * @param saltAsHex		the salt as hex String
	 * @param algoParam		the algorithm-parameter String as hex string
	 * @param password		the plain text password (will be stored encrypted)
	 * @param prop			the Properties or null
	 * 
	 * @return	the extension (0 for default), or -1 if failed
	 */
	public static int createSubSet(String providerName, String saltAsHex, String algoParam,
			String psw, Properties prop) {

		if (providerName == null || saltAsHex == null || algoParam == null) {
			TestLog.e(CloudPropertyHandler.class, "Missing value - Cant set sub set in properties " + providerName);
			TestLog.e(CloudPropertyHandler.class, "Missing value: " + (providerName != null)+ (saltAsHex != null)+ (algoParam != null), 5);
			return -1;
		}
		if (psw == null) {
			TestLog.e(CloudPropertyHandler.class, "Missing password - Cant set sub set in properties");
			TestLog.e(CloudPropertyHandler.class, "Missing password", 5);
			return -1;
		}

		if (prop == null) {
			prop = loadProperties();
			if (prop == null) {
				TestLog.e(CloudPropertyHandler.class, "Can not load properties...");
				return -1;	
			}
		}	 
		TestLog.v(CloudPropertyHandler.class, "Create new sub set for " + providerName + "...");
		// check if login parameters exist:
		String user = getSingleLoginProperty(providerName, "user");
		if (user == null) {
			TestLog.e(CloudPropertyHandler.class, "Missing login parameters for provider " + providerName);
			return -1;
		}
		// check if this scheme  already exist:
		int ext = getSubSetExtension(providerName, saltAsHex, algoParam, prop);
		int newExtension = -2;
		//the extension/marker for the sub set (0 for default set), or -1 (provider with this salt/algos does not exist), or -2 (unexpected error)
		if (ext == -1) { // does not yet exist

			// test if default set exists
			if (prop.getProperty(providerName + "_salt") == null) { // default does not exist
				// set default:
				prop.setProperty(providerName + "_salt", saltAsHex);
				prop.setProperty(providerName + "_algoparam", algoParam);
				prop.setProperty(providerName + "_password", encryptPassword(psw));			
				prop.setProperty(providerName + "_files", "");
				newExtension = 0;
				TestLog.v(CloudPropertyHandler.class, "Create new default property sub set for " + providerName);
			} else {
				// check next int to use:
				int extension = 1;
				while (prop.getProperty(providerName + "_" + extension + "_salt") != null) {
					extension++;
				}
				prop.setProperty(providerName + "_" + extension + "_salt", saltAsHex);
				prop.setProperty(providerName + "_" + extension + "_algoparam", algoParam);
				prop.setProperty(providerName + "_" + extension+ "_password", encryptPassword(psw));		
				prop.setProperty(providerName + "_" + extension + "_files", "");
				newExtension = extension;
				TestLog.v(CloudPropertyHandler.class, "Create new property sub set for " + providerName + "_" + extension);
			}
		} else if (ext == 0) {
			// default scheme, that already exists
			// set default: only password and files may changed
			Object oldProp = prop.setProperty(providerName + "_password", encryptPassword(psw));		
			if (oldProp != null) {
				TestLog.v(CloudPropertyHandler.class, "Replace cloud password for " + providerName);
			}
			prop.setProperty(providerName + "_files", "");
			newExtension = 0;
		} else if (ext > 0) {
			// set sub set: only password and files may changed
			Object oldProp = prop.setProperty(providerName + "_" + ext + "_password", encryptPassword(psw));
			if (oldProp != null) {
				TestLog.v(CloudPropertyHandler.class, "Replace cloud poassword for " + providerName + "_" + ext, 5);
			}
			prop.setProperty(providerName + "_" + ext + "_files", "");
			newExtension = ext;
		} else { // failed
			ext = -1;
		}
		String error = storeProperties(prop);
		if (error != null) {
			return -1;
		}
		return newExtension;
	}

	/**
	 * Remove a sub set (salt/algorithm-parameters) of a provider
	 * 
	 * @param providerName	the provider name
	 * @param saltAsHex		the salt as hex String
	 * @param algoParam		the algorithm-parameter String as hex string
	 * @param prop			the Properties or null
	 * 
	 * @return	null for success, an error message otherwise
	 */
	static String removeSubSet(String providerName, String saltAsHex, 
			String algoParam, Properties prop) {
		if (prop == null) {
			prop = loadProperties();
			if (prop == null) {
				TestLog.e(CloudPropertyHandler.class, "Can not load properties...");
				return "unexpected_error";	
			}
		}	 
		int extension = getSubSetExtension(providerName, saltAsHex, algoParam,prop);
		if (extension == 0) {
			prop.remove(providerName + "_salt");
			prop.remove(providerName + "_algoparam");
			prop.remove(providerName + "_password");
			prop.remove(providerName + "_files");
			String error = storeProperties(prop);
			if (error != null) {
				return "unexpected_error";	
			}
			return null;

		} else if (extension < 0) {
			return "unexpected_error";
		} else {
			prop.remove(providerName + "_" + extension + "_salt");
			prop.remove(providerName + "_" + extension + "_algoparam");
			prop.remove(providerName + "_" + extension + "_password");
			prop.remove(providerName + "_" + extension + "_files");
			String error = storeProperties(prop);
			if (error != null) {
				return "unexpected_error";	
			}
			return null;
		}
	}

	/**
	 * Get all sub-set of provider 
	 * ( = sets with same salt and algoParamString)
	 * 
	 * @param providerName	the provider name
	 * @param extension		the extension of the sub set (0 for default)
	 * @param prop	Properties or null
	 * 
	 * @return	the sub-sets: each set consists of 5 values: 
	 * hex encoded salt [0], algo_param [1], the encrypted password [2], 
	 * comma separated files [3], the sub set name with extension [4]
	 */
	public static String[] getSubSet(String providerName, int extension, Properties props) {
		if (extension < 0) {
			TestLog.ve(CloudPropertyHandler.class, "Invalid extension: " + extension, 5);
			return null;
		}
		if (props == null) {
			props = loadProperties();
			if (props == null) {
				TestLog.e(CloudPropertyHandler.class, "Can not load properties...");
				return null;	
			}
		}
		String setName = null;
		String[] set = new String[5];
		if (extension == 0) {
			setName = providerName;
		} else if (extension > 0) {
			setName = providerName + "_" + extension;
		} 
		set[0] = props.getProperty(setName + "_salt");
		set[1] = props.getProperty(setName + "_algoparam");
		set[2] = props.getProperty(setName + "_password");
		set[3] = props.getProperty(setName + "_files");
		set[4] = setName;
		return set;
	}

	/**
	 * Get all sub-sets of one provider 
	 * ( = sets with same salt and algoParamString)
	 * 
	 * @param providerName	the provider name
	 * @param prop	Properties or null
	 * 
	 * @return	the sub-sets: each set consists of 5 values: 
	 * hex encoded salt [0], algo_param [1], the encrypted password [2], 
	 * comma separated files [3], the sub set name with extension [4]
	 * The order is the same as in getSubSetNamess()
	 */
	public static String[][] getSubSets(String providerName, Properties props) {

		if (props == null) {
			props = loadProperties();
			if (props == null) {
				TestLog.e(CloudPropertyHandler.class, "Can not load properties...");
				return null;	
			}
		}	
		if (providerName == null) {
			TestLog.e(CloudPropertyHandler.class, "No sub set for null provider...");
			return null;
		}
		// get sub-sets
		String[] subSets = getSubSetNames(props, providerName);
		if (subSets == null) { 
			TestLog.v(CloudPropertyHandler.class, "Provider does not yet exist: " + providerName);
			//TestLog.ve(CloudPropertyHandler.class, "Provider does not exist: " + providerName, 5);
			return null;
		}
		int subLen = subSets.length;
		// create return value
		String[][] sets = new String[subLen][5];

		// get parameters of sets
		for (int i = 0; i < subLen; i++) {
			String setName = subSets[i];
			String saltProp = props.getProperty(setName + "_salt");
			if (saltProp == null) {
				TestLog.ve(CloudPropertyHandler.class, "Missing salt for set");
			} else {
				sets[i][0] = saltProp;
			}
			String algoProp = props.getProperty(setName + "_algoparam");
			if (algoProp == null) {
				// check old version
				if (setName.equals(providerName) && subLen == 1) { // default set
					if (saltProp != null) { // salt but no algoparam - seems to be version 1
						String error = migrateFromVersion1(props, providerName);
						if (error == null) {
							algoProp = Converter.bytes2hex(AlgoParamHandler.getV1Bytes());							
						}
					}
				}
				if (algoProp == null) {
					TestLog.ve(CloudPropertyHandler.class, "Missing algo for set");
				}

			} else {
				sets[i][1] = algoProp;
			}			
			sets[i][2] = props.getProperty(setName + "_password");
			sets[i][3] = props.getProperty(setName + "_files");
			sets[i][4] = setName;
		}
		return sets;		
	}

	/**
	 * Get the marker for a sub set of a provider
	 * e.g. the marker/extension 2 for woelkli_2_files. 
	 * Check always if this method returns "unexpected_error"
	 * 
	 * @param providerName	the name of the provider
	 * @param saltAsHex		the salt as hex String
	 * @param algoParam		the algorithm-parameters String as hex string
	 * @param prop			the loaded properties or null
	 * 
	 * @return	the extension/marker for the sub set (0 for default set), 
	 * 			or -1 (provider with this salt/algos does not exist), or -2 (unexpected error)
	 * 			
	 */
	public static int getSubSetExtension(String providerName, 
			String saltAsHex, String algoParam, Properties prop) {
		try {
			if (prop == null) {
				prop = loadProperties();
				if (prop == null) {
					TestLog.e(CloudPropertyHandler.class, "Can not load properties...");
					return -2;	
				}
			}	
			if (saltAsHex == null) {
				TestLog.e(CloudPropertyHandler.class, "Missing salt...");
			}
			if (algoParam == null) {
				TestLog.e(CloudPropertyHandler.class, "Missing algo param...");
			}
			String[][] sets = getSubSets(providerName, prop);
			if (sets == null) {
				return -1;
			}
			for (String[] sub : sets) {
				if (sub[0] != null && sub[0].equals(saltAsHex)) {
					if(sub[1] == null) {
						TestLog.ve(CloudPropertyHandler.class, "Missing algoParam", 3);// migration???
						// seems to be old version 1
						String error = migrateFromVersion1(prop, providerName);
						if (error == null) {
							sub[1] = Converter.bytes2hex(AlgoParamHandler.getV1Bytes());
						} else {
							// unexpected error
							return -2;
						}
					} 
					if (sub[1].equals(algoParam)) {
						String setName = sub[4];
						if (setName.equals(providerName)){
							return 0; // default
						} else {
							String ext = setName.substring(setName.indexOf('_') + 1, // without underscore
									setName.length());
							//TestLog.v(CloudPropertyHandler.class, "Found set extension: " + ext);
							if ( ! ext.matches("-?\\d+")) { // not numeric
								TestLog.ve(CloudPropertyHandler.class, "Sub set extension is not numeric: " + ext 
										+ " - extracted from " + setName);
								return -2;
							} else {
								return Integer.parseInt(ext);
							}
						}
					}			
				}
			}
			// no matching sub set found:
			return -1;
		} catch (Exception e) {
			TestLog.ve(CloudPropertyHandler.class, e.getLocalizedMessage() + " - " + e.toString(), 8);
			return -2;
		}
	}

	/**
	 * Get the sub-set names (different salt or algos)
	 * for a given provider e.g. woelkli, woekli_1, woelkli_2
	 * 
	 * @param props			the loaded Properties or null
	 * @param providerName	the provider name
	 * 
	 * @return	the sub-set names of this provider without trailing underscore
	 * e.g. woelkli_1 or woelkli (default)
	 */
	static String[] getSubSetNames(Properties props, String providerName) {
		if (props == null) {
			props = loadProperties();
		}
		// find algo_param
		Set<Object> keys = props.keySet();
		// list with matching algoParam key of this provider
		ArrayList<String> subSets = new ArrayList<String>();
		for(Object k:keys){
			String key = (String) k;
			if (key.startsWith(providerName)) {
				if (key.endsWith("_salt")) {
					subSets.add(key.substring(0, key.length() - "_salt".length()));
				} else {
					//System.out.println("XXX not added "+key);
				}
			}
		}
		if (subSets.isEmpty()) { 
			//TestLog.ve(CloudPropertyHandler.class, 
			//		"No matching key found for provider " + providerName);
			return null;
		} else {
			return Converter.arrayListToArray(subSets);
		}
	}

	/**
	 * Add file names to the comma separated list of file names
	 * of one cloud provider if not already present. 
	 * (Single file names can be added by setSingleProperty)
	 * 
	 * @param provider	the cloud provider, the files are stored
	 * @param fileNamesToAdd  the file names to add to the properties
	 * @param salt	the salt as hex String
	 * @param algoParam	the algorithm parameter String as hex
	 * 
	 * @return	an error message if occurred or null for success
	 */
	public static String addFileNamesToProperties(String providerName, String[] fileNamesToAdd,
			String salt, String algoParam) {

		if (fileNamesToAdd == null) {
			TestLog.e(CloudPropertyHandler.class, "No files to add for " + providerName);
			return PeaProperties.getVmBridge().translate("no_valid_file_found");
		} 
		//TestLog.ve(CloudPropertyHandler.class, "Add cloud files to " + providerName + " for "+new String(Converter.hex2bytes(algoParam)));
		Properties prop = loadProperties();

		int subSetExtension = getSubSetExtension(providerName, salt, algoParam, prop);
		//TestLog.v(CloudPropertyHandler.class, "Sub set extension: " + subSetExtension);	

		String keyWithEx = null;// the key to set properties e.g. woelkli_1_files
		if (subSetExtension == 0) { // default
			keyWithEx = providerName + "_" + "files";
			//TestLog.v(CloudPropertyHandler.class, "Create default subset with algo " + new String(Converter.hex2bytes(algoParam)),5 );			
		} else if (subSetExtension == -1) { // not exist
			String pswString = getPlainPasswordFromProvider(providerName, salt, algoParam);
			if (pswString == null) {
				TestLog.ve(CloudPropertyHandler.class, "Missing password for provider " + providerName, 5);
				return "Missing password for provider " + providerName;
			}
			int ext = createSubSet(providerName, salt, algoParam, 
					pswString, prop);
			if (ext == 0) { // default
				keyWithEx = providerName + "_" + "files";
			} else if (ext > 0) { // existing extension
				keyWithEx = providerName + "_" + ext + "_files";
			} else { // failed
				TestLog.ve(CloudPropertyHandler.class, "Failed to find sub-set", 5);
				return PeaProperties.getVmBridge().translate("unexpected_error");
			}
		} else if (subSetExtension == -2) { // failed
			return PeaProperties.getVmBridge().translate("unexpected_error");
		} else {
			// non default sub set extension: 
			keyWithEx = providerName + "_" + subSetExtension + "_" + "files";
		}
		// get existing files to add new file:
		String existingFilesAsString =  prop.getProperty(keyWithEx);

		int usedLen = fileNamesToAdd.length;

		if (existingFilesAsString != null && existingFilesAsString.length() > 0) {							
			for (int i = 0; i < usedLen; i++) {
				String usedFile = fileNamesToAdd[i];
				if (usedFile.contains(",") || usedFile.contains("\n")) {
					TestLog.ve(CloudPropertyHandler.class, "contains invalid characters",5);
					return "invalid character in file name: " + usedFile;
				}
				if (usedFile.contains(CloudControl.getTmpDir())){
					TestLog.ve(CloudPropertyHandler.class, "contains tmp dir", 5);
				}
				if ( ! existingFilesAsString.contains( usedFile) ) { // not in existingFiles
					TestLog.v(CloudPropertyHandler.class, "Add file to properties: " + usedFile);
					existingFilesAsString += "," + usedFile;
				} else {
					//TestLog.v(CloudPropertyHandler.class, "properties already contains file: " + usedFile);
				}
			}
		} else { // no files
			TestLog.v(CloudPropertyHandler.class, "Create new sub-set in properties for " + providerName);
			if (fileNamesToAdd[0] != null) {
				StringBuilder sb = new StringBuilder();
				sb.append(fileNamesToAdd[0]); // without leading comma
				for (int i = 1; i < usedLen; i++) {
					sb.append("," + fileNamesToAdd[i]);
				}
				existingFilesAsString = new String(sb);
			}
		}
		// set property:
		if (existingFilesAsString != null && existingFilesAsString.length() > 0) {
			prop.setProperty(keyWithEx, existingFilesAsString);
		} else {
			TestLog.e(CloudPropertyHandler.class, "No files found for " + providerName);
			TestLog.ve(CloudPropertyHandler.class, "is null or empty", 5);
			return PeaProperties.getVmBridge().translate("unexpected_error");
		}
		// Set the new properties
		return storeProperties(prop);
	}

	/**
	 * Remove a file name from the properties files of
	 * a cloud provider if exists. 
	 * 
	 * @param providerName	the provider name, the file is stored in properties
	 * @param fileNameToRemove		the file name to remove without path
	 * @param saltAsHex		the salt as hex String
	 * @param algoParam		the algorithm-parameters String as hex string
	 * @param prop			the loaded properties or null
	 * 
	 * @return	an error message or null for success
	 */
	public static String removeFileOfProvider(String providerName, String fileNameToRemove,
			String saltAsHex, String algoParam, Properties prop) {

		if (prop == null) {
			prop = loadProperties();
			if (prop == null) {
				TestLog.e(CloudPropertyHandler.class, "Can not load properties...");
				return "unexpected_error";	
			}
		}	 
		String existingFilesAsString = null;
		// if full path with tmp dir: remove tmp dir
		if (fileNameToRemove.contains(CloudControl.getTmpDir())) {
			TestLog.ve(CloudPropertyHandler.class, "contains full path with tmp directory", 5);
			fileNameToRemove = CloudControl.getFileNameFromTmpFile(fileNameToRemove);
		}
		int subSetExtension = getSubSetExtension(providerName, saltAsHex, algoParam, prop);

		String keyToFind = null;
		if (subSetExtension ==  0) { // no sub set
			keyToFind = providerName + "_" + "files";
		} else if (subSetExtension < 0) {
			TestLog.e(CloudPropertyHandler.class, "Can find file to remove: " + fileNameToRemove);
			return "unexpected_error";	
		} else {
			keyToFind = providerName + "_" + subSetExtension + "_" + "files";
		}
		existingFilesAsString = prop.getProperty(keyToFind);
		if (existingFilesAsString == null || existingFilesAsString.length() == 0) {
			TestLog.e(CloudPropertyHandler.class, "Couldn't remove file " + fileNameToRemove + " from properties of " + providerName);
			TestLog.ve(CloudPropertyHandler.class, "keyToFind: " + keyToFind + ", subSetExtension: " + subSetExtension, 5);
			return PeaProperties.getVmBridge().translate("file_not_found") + ": "  + providerName + " - " + fileNameToRemove;
		}
		// remove file:
		existingFilesAsString = removeFileNameFromCommaSeparatedList(existingFilesAsString, fileNameToRemove);

		// set property:
		if (existingFilesAsString == null || existingFilesAsString.length() == 0) {
			TestLog.v(CloudPropertyHandler.class, "Warning: No file left for this sub set: "+ providerName + " " + subSetExtension);
			existingFilesAsString = "";
		}
		prop.setProperty(keyToFind, existingFilesAsString);	

		// Set the new properties
		return storeProperties(prop);	
	}

	/**
	 * Remove file name from properties of any sub set of a provider. 
	 * 
	 * @param providerName		the name of the provider
	 * @param fileNameToRemove	the file name without path/tmp
	 * @param prop				the Properties or null
	 * 
	 * @return	the index of the sub set, the file was removed from, -1 if file did not exist, -2 for error
	 */
	public static int removeFileFromAnyProviderSet(String providerName, String fileNameToRemove, Properties prop) {
		// TODO test case
		if (prop == null) {
			prop = loadProperties();
			if (prop == null) {
				TestLog.e(CloudPropertyHandler.class, "Can not load properties...");
				//return "unexpected_error";
				return -2;
			}
		}	 
		// if full path with tmp dir: remove tmp dir
		if (fileNameToRemove.contains(CloudControl.getTmpDir())) {
			TestLog.ve(CloudPropertyHandler.class, "contains full path with tmp directory", 5);
			fileNameToRemove = CloudControl.getFileNameFromTmpFile(fileNameToRemove);
		}
		String[][] allSubSets = getSubSets(providerName, prop);
		for (String[] subSet : allSubSets) {
			String commaSeparatedList = subSet[3];
			if (commaSeparatedList.contains(fileNameToRemove)) { // this is not sure, maybe file name is part of other file name
				String removeError = removeFileOfProvider(providerName, fileNameToRemove, subSet[0], subSet[1], prop);
				if (removeError != null) {
					TestLog.ve(CloudPropertyHandler.class, removeError, 5);
				} else {
					//return subSet[4].substring
					if (subSet[4].equals(providerName)) {
						return 0;
					} else if (subSet[4].contains("_")){
						String ext = subSet[4].substring(subSet[4].indexOf('_') + 1, subSet[4].length());
						TestLog.v(CloudPropertyHandler.class, "Found set extension: " + ext);
						if ( ! ext.matches("-?\\d+")) { // not numeric
							TestLog.ve(CloudPropertyHandler.class, "Sub set extension is not numeric: " + ext 
									+ " - extracted from " + subSet[4]);
							return -2;
						} else {
							return Integer.parseInt(ext);
						}
					} else {
						TestLog.e(CloudPropertyHandler.class, "Invalid property: " + subSet[4], 5);
						TestLog.ve(CloudPropertyHandler.class, "Invalid sub set: " + subSet[4], 5);
						return -2;
					}
				}
			}
		}
		TestLog.v(CloudPropertyHandler.class, PeaProperties.getVmBridge().translate("file_not_found") + ": " + fileNameToRemove);
		return -1;
	}

	/**
	 * Get the files that are stored as properties for a provider, salt and algos
	 * 
	 * @param providerName	the provider name
	 * @param salt			the salt as hex String
	 * @param algoParam		the algorithm parameter String as hex string
	 * 
	 * @return	the file names as comma separated list or null
	 */
	static String getFilesFromProvider(String providerName, String salt, String algoParam) {

		if (salt == null) {
			TestLog.ve(CloudPropertyHandler.class, "Missing salt...", 5);
			return null;
		}
		if (algoParam == null) {
			TestLog.ve(CloudPropertyHandler.class, "Missing algos...", 5);
			return null;
		}
		Properties prop = loadProperties();//new Properties();
		if (prop == null) {
			TestLog.e(CloudPropertyHandler.class, "Access to property file failed...");
			return null;
		}
		String provoderWithExtension = null;
		int ext = getSubSetExtension(providerName, salt, algoParam, prop);
		if (ext == 0) {
			provoderWithExtension = providerName;
		} else if (ext == -1) {
			TestLog.v(CloudPropertyHandler.class, "No such provider with this salt/algos: " + providerName);
			// check old version???
			String propSalt = (String) prop.get(providerName + "_salt");
			if (	propSalt != null 
					&& propSalt.equals(salt)
					&& prop.get(providerName + "_algoparam") == null) { // seems to be old version 1
				String error = migrateFromVersion1(prop, providerName);
				if (error == null) {
					return getFilesFromProvider(providerName, salt, Converter.bytes2hex(AlgoParamHandler.getV1Bytes()));
				}
			}
			return null;
		} else if (ext == -2) {
			TestLog.e(CloudPropertyHandler.class, "unexpected error: Can't get file from provider " + providerName);
			return null;
		} else {
			provoderWithExtension = providerName + "_" + ext;

		}
		return prop.getProperty(provoderWithExtension + "_files");
	}

	/**
	 * Reset (and overwrite) the files in properties. 
	 * The key must not exist already.
	 * Old property is overwritten, new property is stored.
	 * 
	 * @param propKey	the provider name to reset the files
	 * @param propValue	the file names to set as comma-separated String
	 * @param salt	salt as hex String
	 * @param algoParam	the algorithm parameter String as hex string
	 * @param prop	Properties or null
	 * 
	 * @return a single line localized error message to display or null for success
	 */
	public static String resetFileNames(String providerName, String fileNamesAsString, 
			String salt, String algoParam, Properties prop) {

		if (prop == null) {
			prop = loadProperties();
			if (prop == null) {
				return PeaProperties.getVmBridge().translate("unexpected_error") + ": No property file";
			} 
		}
		String providerWithSubSet = null;
		int subset = getSubSetExtension(providerName, salt, algoParam, prop);
		if (subset == 0) {// default
			providerWithSubSet = providerName;
		} else if (subset < 0) { // no sub set
			TestLog.e(CloudPropertyHandler.class, "Reset files for provider " + providerName + " failed.");
			return PeaProperties.getVmBridge().translate("unexpected_error");
		} else {
			providerWithSubSet = providerName + "_" + subset;
		}

		if (providerName != null) { // add file name

			Object oldValue = prop.setProperty(providerWithSubSet + "_files", fileNamesAsString);
			if (oldValue != null) {
				TestLog.v(CloudPropertyHandler.class, providerWithSubSet + ": files property overwritten...");				
			} else {
				TestLog.v(CloudPropertyHandler.class, providerWithSubSet + ": set file property...");
			}
		}
		// Set the new properties
		return storeProperties(prop);	
	}

	/**
	 * Get the hex encoded and encrypted password for a provider
	 * 
	 * @param providerName	provider name
	 * @param salt			salt as hex string
	 * @param algoParam		algorithm-parameters as hex string
	 * 
	 * @return	the plain password as string
	 */
	public static String getPlainPasswordFromProvider(String providerName, String salt, String algoParam) {
		Properties prop = loadProperties();//new Properties();
		if (prop == null) {
			TestLog.e(CloudPropertyHandler.class, "Access to property file failed...");
			return null;
		}
		String provoderWithExtension = null;
		int ext = getSubSetExtension(providerName, salt, algoParam, prop);
		if (ext == 0) {
			provoderWithExtension = providerName;
		} else if (ext < 0) {
			TestLog.v(CloudPropertyHandler.class, "Can't get password for provider " + providerName + " - sub set does not exist");
			return null;
		} else {
			provoderWithExtension = providerName + "_" + ext;

		}
		String encryptedPsw =  prop.getProperty(provoderWithExtension + "_password");
		if (encryptedPsw == null) {
			TestLog.e(CloudPropertyHandler.class, "unexpected error: Can't decrypt password from provider " + providerName);
			return null;
		}
		return decryptPassword(encryptedPsw, null, null);
	}

	/**
	 * Store an hex-encrypted password for a provider
	 * 
	 * @param hexEncryptedPsw	encrypted password as hex string
	 * @param providerName	provider name
	 * @param salt			salt as hex string
	 * @param algoParam		algorithm-parameters as hex string
	 * @param prop			the Properties or null
	 * 
	 * @return	null for success, an error message otherwise
	 */
	public static String storeEncryptedPasswordFromProvider(String hexEncryptedPsw,
			String providerName, String salt, String algoParam, Properties prop) {
		
		if (PeaProperties.isTestMode() == true) {
			if (Comparator.checkNullVector(Converter.hex2bytes(hexEncryptedPsw)) == true) {
				TestLog.e(CloudPropertyHandler.class, "Null vector: hex encrypted password", 8);
			}
		}
		
		if (providerName == null || salt == null || algoParam == null) {
			TestLog.e(CloudPropertyHandler.class, "unexpected error: required value is null");
			TestLog.ve(CloudPropertyHandler.class, "unexpected error", 5);
			return PeaProperties.getVmBridge().translate("unexpected_error");
		}
		if (hexEncryptedPsw == null) {
			TestLog.e(CloudPropertyHandler.class, "unexpected error: psw is null");
			TestLog.ve(CloudPropertyHandler.class, "unexpected error", 5);
			return PeaProperties.getVmBridge().translate("unexpected_error");
		}
		if (prop == null) {
			prop = loadProperties();
			if (prop == null) {
				return PeaProperties.getVmBridge().translate("unexpected_error");	
			}
		}		
		String propKey = null;
		int ext = getSubSetExtension(providerName, salt, algoParam, prop);
		//TestLog.v(CloudPropertyHandler.class,"ext: " + ext);
		if (ext == 0) {
			propKey = providerName + "_password";
		} else if (ext == -1) {
			TestLog.v(CloudPropertyHandler.class, "unexpected error: Can't find provider with this salt/algos: " + providerName);
			int newExt = createSubSet(providerName, salt, algoParam, 
					decryptPassword(hexEncryptedPsw, null, null), prop);
			if (newExt == -1) {
				TestLog.e(CloudPropertyHandler.class, "unexpected error: Can't store password for " + providerName);
				TestLog.ve(CloudPropertyHandler.class, "Can't store password for " + providerName, 5);
				return "Can't store password for " + providerName;
			} else {
				return null;
			}
		} else if (ext == -2) {
			TestLog.e(CloudPropertyHandler.class, "unexpected error for provider " + providerName);
			return PeaProperties.getVmBridge().translate("unexpected_error");
		} else {
			propKey = providerName + "_" + ext + "_password";		
			//TestLog.v(CloudPropertyHandler.class,"propKey: " + propKey);
		}
		if (PeaProperties.isTestMode() == true) {
			if (Comparator.checkNullVector(Converter.hex2bytes(hexEncryptedPsw)) == true) {
				TestLog.e(CloudPropertyHandler.class, "Null vector: hex encrypted password", 8);
			}
		}
		Object oldProp = prop.put(propKey, hexEncryptedPsw);
		if (oldProp != null) {
			TestLog.v(CloudPropertyHandler.class, "Replace password for cloud " + providerName);
		}
		return storeProperties(prop);
	}

	/**
	 * Encrypt the password: 1. convert in bytes, 
	 * 2. encrypt by session key, 3. convert in hex string
	 * 
	 * @param psw	the password as String
	 * 
	 * @return	the encrypted password as hex string 
	 * 			or null if an error occurred
	 */
	public static String encryptPassword(String psw) {
		if (psw == null || psw.length() == 0) {
			TestLog.ve(CloudPropertyHandler.class, "is null", 5);
			return null;
		}
		if (PeaProperties.isTestMode() == true) {
			if (Comparator.checkNullVector(psw.toCharArray()) == true) {
				TestLog.e(CloudPropertyHandler.class, "Null vector: hex encrypted password", 8);
			}
		}
		return encryptPasswordFromBytes(psw.getBytes(PeaProperties.getCharset()));
	}

	/**
	 * Encrypt the password: 1. convert in bytes, 
	 * 2. encrypt by session key, 3. convert in hex string
	 * 
	 * @param psw	the password as bytes
	 * 
	 * @return	the encrypted password as hex string 
	 * 			or null if an error occurred
	 */
	public static String encryptPasswordFromBytes(byte[] pswBytes) {
		if (pswBytes == null ) {
			TestLog.ve(CloudPropertyHandler.class, "is null", 5);
			return null;
		}
		if (PeaProperties.isTestMode() == true) {
			if (Comparator.checkNullVector(pswBytes) == true) {
				TestLog.e(CloudPropertyHandler.class, "Null vector: hex encrypted password", 8);
			}
		}
		if (CipherStuff.getSessionKeyCrypt().getKey() == null) {
			TestLog.ve(CloudPropertyHandler.class, "is null", 5);
			return null;
		}

		byte[] encryptedPsw = CipherStuff.processCTR(true, // for encryption
				pswBytes,  // psw as bytes, is zeroized
				CipherStuff.getSessionKeyCrypt().getKey(), // use stored key
				false);// do not zeroize stored key
		//Zeroizer.zero(psw);
		if (encryptedPsw ==  null) {
			TestLog.e(CloudPropertyHandler.class, "encryption of password failed..." );
			TestLog.ve(CloudPropertyHandler.class, "is null", 5);
			return null;
		}
		return Converter.bytes2hex(encryptedPsw);
	}

	/**
	 * Decrypt the hex encoded and encrypted password:
	 * 1. convert to bytes, 2. decrypt by session key, 
	 * 3. convert in string
	 * 
	 * @param hexEncryptedPsw	the hex-encoded, encrypted Password
	 * @param keyToUse		the key to use or null (stored key)
	 * @param cipherToUse	the cipher to use or null (current cipher)
	 * 
	 * @return	the password as string
	 */
	public static String decryptPassword(String hexEncryptedPsw, byte[] keyToUse, BlockCipher cipherToUse) {
		if (hexEncryptedPsw == null) {
			TestLog.e(CloudPropertyHandler.class, "Missing encrypted password...");
			return null;
		}
		if ( (CipherStuff.getSessionKeyCrypt() == null || CipherStuff.getSessionKeyCrypt().getKey() == null)
				&& keyToUse == null) {
			// Not yet initialized: in PswDialogView
			return null;
		}
		if (keyToUse == null) {
			// use stored key:
			keyToUse = CipherStuff.getSessionKeyCrypt().getKey();
		}
		byte[] plainPsw = null;
		if (cipherToUse != null) {
			BlockCipher cipherToReset = CipherStuff.getCipherAlgo();
			CipherStuff.setCipherAlgo(cipherToUse);
			if (CipherStuff.getKeySize() != keyToUse.length) {
				TestLog.e(CloudPropertyHandler.class, "Invalid key size..", 5);
				return null;
			}
			plainPsw = CipherStuff.processCTR(false, //  decryption
					Converter.hex2bytes(hexEncryptedPsw), // password
					keyToUse, // use session key
					false); // don't zeroize key
			CipherStuff.setCipherAlgo(cipherToReset);
		} else {
			if (CipherStuff.getKeySize() != keyToUse.length) {
				TestLog.e(CloudPropertyHandler.class, "Invalid key size..", 5);
				return null;
			}
			plainPsw = CipherStuff.processCTR(false, //  decryption
					Converter.hex2bytes(hexEncryptedPsw), // password
					keyToUse, // use session key
					false); // don't zeroize key
		}
		if (PeaProperties.isTestMode() == true) {
			if (Comparator.checkNullVector(plainPsw) == true) {
				TestLog.e(CloudPropertyHandler.class, "Null vector: hex encrypted password", 8);
			}
		}
		return new String(plainPsw, PeaProperties.getCharset());
	}

	/**
	 * Remove one file from a comma separated list of files. 
	 * The properties are not changed. 
	 * 
	 * @param commaSeparatedList	the comma separated list as stored in properties
	 * @param fileNameToRemove		the file name to remove from this list
	 * 
	 * @return	the new list, unchanged (did not contain the file) 
	 * 			or the list without the file name to remove 
	 * 			or null if an error occurred (no list exists)
	 */
	private static String removeFileNameFromCommaSeparatedList
	(String commaSeparatedList, String fileNameToRemove) {
		if (commaSeparatedList != null && commaSeparatedList.length() > 0) {			

			String[] existingFiles = commaSeparatedList.split(",");
			ArrayList<String> existingList = Converter.arrayToArrayList(existingFiles);
			if (existingList.contains(fileNameToRemove)) {
				existingList.remove(fileNameToRemove);

				StringBuilder sb = new StringBuilder();
				for (String s : existingList) {
					sb.append(s + ",");
				}
				// remove last comma
				if (sb.length() > 0) {
					sb.setLength(sb.length() - 1);
					//sb.deleteCharAt(sb.length()-1);
				}
				return new String(sb);
			} else {
				TestLog.e(CloudPropertyHandler.class, "File not found in properties: " +fileNameToRemove);
				return commaSeparatedList;
			}
		} else {
			TestLog.e(CloudPropertyHandler.class, "Couldn't remove file " + fileNameToRemove);
			TestLog.ve(CloudPropertyHandler.class, "is null", 5);
			return null;//PeaProperties.getVmBridge().translate("file_not_found") + ": "  + providerName + " - " + fileNameToRemove;
		}
	}

	/**
	 * Delete all sub sets without any file name in properties,
	 * delete all providers without sub sets
	 */
	public static void cleanupProperties() {
		if (new File(propertyFileName).exists()) {
			String[] allProviders = getProviderNames();
			if (allProviders != null) {
				Properties prop = loadProperties();
				if (prop == null) {
					TestLog.e(CloudPropertyHandler.class,PeaProperties.getVmBridge().translate("unexpected_error"));
					return;
				}
				// check all sub sets
				for (String providerName : allProviders) {
					String[][] subSets = getSubSets(providerName, prop);

					if (subSets != null) {
						for (String[] subSet : subSets) {
							if (subSet[3] == null || subSet[3].length() == 0) { // there are no files -> delete sub set						
								String setName = subSet[4];
								TestLog.v(CloudPropertyHandler.class, "Delete sub set " + setName);
								if (setName != null) {
									prop.remove(setName + "_salt");
									prop.remove(setName + "_algoparam");
									prop.remove(setName + "_password");
									prop.remove(setName + "_files");
								} else {
									TestLog.e(CloudPropertyHandler.class, "Couldn't delete sub set of " + providerName);
								}
							} else {
								//
							}
						}
					} else {

					}
					// check if any sub set is left:
					if (getSubSetNames(prop, providerName) == null) {
						// delete provider:
						TestLog.v(CloudPropertyHandler.class, "Delete provider " + providerName);
						boolean deleteSuccess = deleteProviderParameters(providerName);
						if (deleteSuccess == false) {
							TestLog.e(CloudPropertyHandler.class, "Couldn't delete " + providerName);
						}
					}
				}
				String storeError = storeProperties(prop);
				if (storeError != null) {
					TestLog.e(CloudPropertyHandler.class, "Couldn't store properties ");
				}
			} else {
				TestLog.v(CloudPropertyHandler.class, "No providers found in properties ");
			}
		}
	}

	/**
	 * Convert a PEA version1 property file into the new version. 
	 * 
	 * @param prop	Properties or null
	 * @param provName	the name of the provider to migrate or null (check all providers)
	 * 
	 * @return	error message or null
	 */
	public static String migrateFromVersion1(Properties prop, String provName) {

		TestLog.v(CloudPropertyHandler.class, "Migrate cloud properties from version 1 for " + ((provName == null) ? "all providers" : provName));

		if (prop == null) {
			prop = loadProperties();
			if (prop == null) {
				return PeaProperties.getVmBridge().translate("unexpected_error");	
			}
		}	
		try {
			// check if this file is really version 1:
			boolean isVersion1 = true;
			String[] providerNames = null;
			if (provName != null) {
				String[] pn = {provName};
				providerNames = pn;
			} else {
				providerNames = getProviderNames();
			}
			if (providerNames != null) {

				for (String providerName : providerNames) {

					if(providerName != null) {
						// there must be no sub set:
						// this methods checks the salt
						String[] subSetNames = getSubSetNames(prop, providerName);
						if (subSetNames != null) {
							if (subSetNames.length > 1) {
								isVersion1 = false;
								TestLog.e(CloudPropertyHandler.class, "sub set found for " + providerName);
								break;
							} else {
								if (subSetNames[0] != null) {
									if ( ! subSetNames[0].equals(providerName)) {
										TestLog.e(CloudPropertyHandler.class, "sub set name found for " + providerName);
										isVersion1 = false;
										break;
									}
								} else {
									TestLog.ve(CloudPropertyHandler.class, "Sub set name is null", 5);
								}
							}
							// there is no algoparam property in version 1
							if (prop.getProperty(providerName + "_algoparam") != null) {
								TestLog.e(CloudPropertyHandler.class, "algoparam property found for " + providerName);
								isVersion1 = false;
								break;
							}
						} else {
							TestLog.ve(CloudPropertyHandler.class, "No sub set names found", 5);
						}
					} else {
						TestLog.ve(CloudPropertyHandler.class, "Found null provider name for migration from version 1 ", 5);
					}
				}
				if (isVersion1 == false) {
					TestLog.e(CloudPropertyHandler.class,  "Properties are not from PEA version 1...");
					return null;
				}
				// add the algoparam property for all providers
				String algoAsHex = Converter.bytes2hex(AlgoParamHandler.getV1Bytes());
				for (String providerName : providerNames) {
					prop.setProperty(providerName + "_algoparam" ,  algoAsHex);
					TestLog.v(CloudPropertyHandler.class, "Migrated cloud properties from version 1 for " + providerName);
				}
				String storeError = storeProperties(prop);
				if (storeError != null) {
					TestLog.e(CloudPropertyHandler.class, "Couldn't store properties ");
					return storeError;
				} else {
					TestLog.o(CloudPropertyHandler.class, "Cloud properties converted from PEA version 1 to new version.");
				}
			}

		} catch (Exception e) {
			TestLog.e(CloudPropertyHandler.class, "Error occurred when migrating from version 1 to version 2 ");
			TestLog.ve(CloudPropertyHandler.class, e.getLocalizedMessage() + " - " + e.toString(), 5);
			return PeaProperties.getVmBridge().translate("unexpected_error") + "\n" +
			e.getLocalizedMessage() + "\n" + 
			"\n Sorry, you have to add your cloud providers again. ";
		}
		return null;
	}

	/**
	 * Get the file name where cloud properties are stored
	 * 
	 * @return	the name of the cloud property file
	 */
	public static String getCloudPropertyFileName() {
		return propertyFileName;
	}
}
