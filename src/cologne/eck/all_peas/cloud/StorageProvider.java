package cologne.eck.all_peas.cloud;

/*
 * Peafactory - Production of Password Encryption Archives
 * Copyright (C) 2020  Axel von dem Bruch
 * 
 * This library is free software; you can redistribute it and/or modify it 
 * under the terms of the GNU General Public License as published 
 * by the Free Software Foundation; either version 2 of the License, 
 * or (at your option) any later version.
 * This library is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
 * or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU General Public License for more details.
 * See:  http://www.gnu.org/licenses/gpl-2.0.html
 * You should have received a copy of the GNU General Public License 
 * along with this library.
 */

import java.io.File;
import java.util.HashMap;

public interface StorageProvider {
	
	/**
	 * The directory to use with this application
	 */
	public final static String peaDirectory = "PeaFactory";
	
	/**
	 * Run through the authorization process
	 * of the provider. If authorization is successful, the
	 * WebDAV base path is set with trailing peaDirectory, 
	 * otherwise an error message is returned
	 * 
	 * @param userName the user name as String
	 * @param password	the password as chars
	 * @param otherParams	other necessary parameter if required or null
	 * 
	 * @return	an error message to display or null for success
	 */
	String authorize(String userName, char[] password, String[] otherParams);
	
	/**
	 * Get all files that are stored in the 
	 * default folder of this account.
	 * All files must contain the cloud provider path. 
	 * This is used to display all files and folder
	 * in the CloudFileManager
	 * 
	 * @param extensionFilters	extensions without . like rft, ics, png
	 * 							to include only files with this extension
	 * 							or null (include all files)
	 * 
	 * @return	all files in the default folder with given extensions
	 * 			of this account, without default folder, 
	 * 			but with cloud provider path
	 */
	public String[] getCloudFiles(String[] extensionFilters);
	
	/**
	 * Open a file in the cloud. 
	 * If the file is locked, a message is returned
	 *  
	 * @param cloudFileName		the name of the file to open (without path to server)
	 * @param storedFileName	the path to store the file without the name
	 * 
	 * @return	null for success, an error message otherwise
	 */
	public String openFile(String cloudFileName, String storedFilePath);
	
	/**
	 * Download files from the cloud (directory PeaFactory) 
	 * to a local directory. An error message lists all failed files
	 * with failure header (first line) and file names with reasons 
	 * (file name - reason) line by line
	 * 
	 * @param fileNames		the file names to download
	 * @param targetDir		the name of the directory to store the files
	 * 						without trailing or leading /
	 * 
	 * @return 	null for complete success, an error message otherwise with the structure:
	 * 			first line: header failure message and 
	 * 			file name and reason, separated by - line by line (file name - reason)
	 */
	String downloadFiles(String[] fileNames, String targetDir);
	
	/**
	 * Upload files to the cloud and store in the directory PeaFactory
	 * with their file names (without path)
	 * 
	 * @param files		the files to upload
	 * @param folder	the folder on the server to store the files or null
	 * 					without leading or trailing /
	 * 
	 * @return 	null for complete success, an error message otherwise with the structure:
	 * 			first line: header failure message and 
	 * 			file name and reason, separated by - line by line (file name - reason)
	 */
	String uploadFiles(File[] files, String folder);
	
	/**
	 * Upload contents to the cloud and store in files in the directory PeaFactory
	 * 
	 * @param map		a map <String, byte[]> (file name, file content)
	 * @param folder	the folder to upload or null (in PeaFactory)
	 * 
	 * @return 	null for complete success, an error message otherwise with the structure:
	 * 			first line: header failure message and 
	 * 			file name and reason, separated by - line by line (file name - reason)
	 */
	String uploadContent(HashMap<String, byte[]> map, String folder);
	
	/**
	 * Get the date-time of the last modification 
	 * of the file on the server as time stamp
	 * 
	 * @param fileName	the file to get the last modification date-time
	 * 
	 * @return	time stamp as long or -1 if function failed
	 */
	long lastModified(String fileName);
	
	/**
	 * Create a directory on the server if this directory does not exist
	 * 
	 * @param dirName	the name of the directory (not the URL!) 
	 * 					without leading or trailing /
	 * 
	 * @return	null for success or if the directory already exists, 
	 * 			otherwise an error message 
	 */
	String createDirectory(String dirName);
	
	/**
	 * Check if a file exists on the server.
	 * This does not work for directories!
	 * 
	 * @param fileName	the name of the file 
	 * 					eventually with folder (e.g. Folder/file.txt)
	 * 
	 * @return	null if the file exists, an error message if not or an error occurred
	 */
	String fileExists(String fileName);
	
	/**
	 * Deletes a file in the cloud.
	 * 
	 * @param fileName	the file name (cloud folders if necessary)
	 * 
	 * @return	null if the file was deleted, an error message if an error occurred
	 */
	String deleteFile(String fileName);
	
	/**
	 * Lock the read-write access to the file.
	 * This method should be called whenever a file 
	 * is loaded from the cloud to be modified.
	 * 
	 * @param fileName	the name of the file to lock
	 * 
	 * @return	null for success, an error message if an error occurred
	 */
	String lockFileX(String fileName);
	
	/**
	 * Unlock the read-write access to the file. 
	 * This method should be called after a session for 
	 * all files that were locked. 
	 * 
	 * @param fileName	the name of the file to unlock
	 * 
	 * @return	null for success, an error message if an error occurred
	 */
	String unLockFileX(String fileName);
	
	
	/**
	 * Unlock all files that were locked in this session.
	 * This method should be called after every session. 
	 * 
	 * @return	null for complete success, every failed file name otherwise
	 */
	String unlockAllX();
	
	/**
	 * Close the current cloud session
	 * 
	 * @return	null for success, error message otherwise
	 */
	String closeSession();
	
	/**
	 * Get informations from and about the cloud provider
	 * for this application and the user account
	 */
	String getProviderInfos();
	
	/**
	 *  Get the name of the cloud provider
	 *  
	 * @return	the name of the cloud provider as String
	 */
	public String getProviderName();
	
	
	/**
	 * Set the name of the cloud provider.
	 * This name is also used in the property file. 
	 * 
	 * @param name	the name of the provider without whitespace
	 */
	public void setProviderName(String name);
	
	/**
	 * The default path used to upload and download
	 * files from this application
	 * 
	 * @return the default path as String
	 */
	public String getDefaultPath();
}
