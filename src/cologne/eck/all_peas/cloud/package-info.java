/**
 * Classes used for general cloud access.
 * Currently only the WebDAV protocol is supported.
 */
/**
 * @author axl
 *
 */
package cologne.eck.all_peas.cloud;