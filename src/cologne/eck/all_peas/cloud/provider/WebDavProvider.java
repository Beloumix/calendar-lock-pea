package cologne.eck.all_peas.cloud.provider;

/*
 * Peafactory - Production of Password Encryption Archives
 * Copyright (C) 2020  Axel von dem Bruch
 * 
 * This library is free software; you can redistribute it and/or modify it 
 * under the terms of the GNU General Public License as published 
 * by the Free Software Foundation; either version 2 of the License, 
 * or (at your option) any later version.
 * This library is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
 * or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU General Public License for more details.
 * See:  http://www.gnu.org/licenses/gpl-2.0.html
 * You should have received a copy of the GNU General Public License 
 * along with this library.
 */

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Files;
//import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import javax.xml.namespace.QName;

import com.thegrizzlylabs.sardineandroid.DavResource;
import com.thegrizzlylabs.sardineandroid.Sardine;
import com.thegrizzlylabs.sardineandroid.impl.OkHttpSardine;
import com.thegrizzlylabs.sardineandroid.impl.SardineException;
import com.thegrizzlylabs.sardineandroid.util.SardineUtil;

import cologne.eck.all_peas.cloud.CloudControl;
import cologne.eck.all_peas.cloud.CloudPropertyHandler;
import cologne.eck.all_peas.cloud.StorageProvider;
import cologne.eck.all_peas.data.PeaProperties;
import cologne.eck.tools.ExceptionHelper;
import cologne.eck.tools.TestLog;
import cologne.eck.tools.WebTools;




public class WebDavProvider implements StorageProvider {
	
	/**
	 * The base path of the provider used for WebDAV without peaDirectory.
	 * This is stored in the property file.
	 * This should never be used for operations like copy, create...
	 */
	private String webDavBasePath;
	
	/**
	 * The base path of the provider used for WebDAV with peaDirectory
	 * and trailing /
	 * This should be used for all operations
	 */
	private String cloudPath;
	/**
	 * The user defined provider name. 
	 * This is also used to store login parameters in property file
	 */
	private String providerName;
	
    /**
     * The sardine instance
     */
	private Sardine sardine;
	
	/**
	 * The user name for cloud login.
	 * This is stored in the property file.
	 */
	private String userName;
	/**
	 * The password for cloud logion.
	 * This is stored encrypted in the property file
	 */
	private char[] userPassword;
	
	/**
	 * Timeout for upload & download in seconds
	 */
	private int uploadDownloadTimeout = 12;
	/**
	 * Timeout for cloud authorization in seconds
	 */
	private int authorizeTimeout = 20;
	/**
	 * Message set in download per file
	 */
	private String downloadErrorMessage = null;
	/**
	 * This list contains all files that were locked in this session.
	 * These file names may contain the directories in the cloud. 
	 * All file names start with a ~ before the name (not the directory). 
	 */
	//private ArrayList<String> lockedFiles = new ArrayList<String>();
	
	/**
	 * Error messages that occur in functions of this class
	 */
	//private StringBuilder XerrorMessage = null;
	
	// token to unlock 
	//private String lockToken;
	
	
	/**
	 * Property for last modification date of file content
	 */
	private static final QName qn = new QName(SardineUtil.CUSTOM_NAMESPACE_URI, "LASTMOD", SardineUtil.CUSTOM_NAMESPACE_PREFIX);


	/**
	 * The cloud provider that supports WebDAV
	 * 
	 * @param _providerName	the user defined name of the provider
	 */
	public WebDavProvider(String _providerName) {//, Component _owner) {
		this.providerName = _providerName;
		//this.owner = _owner;
	}
	
	//=====================================================================================
	
	
    /**
     * Get the user name:
     * 1. look if it was set manually
     * 2. try to read from resource file
     * 3. open input dialog
     * The user name can be in most cases the mail address,
     * (Nextcloud, OpenDrive, pCloud, GMX...)
     * but sometimes the user name (Yandex). 
     * 
     * @return	the user name or null
     */
	public String getUserName() {
		if (userName == null) {
			String providerName = getProviderName();
			Properties p = new Properties();
			try {
				p.load(new FileInputStream("pea.properties"));
			} catch (FileNotFoundException e) {
				TestLog.e(WebDavProvider.class, "No property file found, when reading user name for " + providerName);
			} catch (IOException e) {
				TestLog.e(WebDavProvider.class, "No access to pea.properties, when reading user name for " + providerName);
			}
			userName =  p.getProperty(providerName + "_" + "user");
		}
		return userName;
	}
    
    /**
     * Get the password for authorization:
     * 1. Check if it was set manually
     * 2. Try to read the encrypted and hex-encoded 
     *    password from property file (key is providerName_password) 
     *    and decrypt and decode it
     * 
     * @return	the decrypted password or null
     */
	public char[] getUserPassword() {
		if (userPassword == null) {
			String providerName = getProviderName();
			Properties p = new Properties();
			try {
				p.load(new FileInputStream("pea.properties"));
			} catch (FileNotFoundException e) {
				TestLog.e(WebDavProvider.class, "No property file found, when reading encrypted pawwsord for " + providerName);
				//return null;
			} catch (IOException e) {
				TestLog.e(WebDavProvider.class, "No access to pea.properties, when reading encrypted pawwsord for " + providerName);
				//return null;
			}
			// this is a hex-encoded, encrypted string
			String hexEncryptedPsw =  p.getProperty(providerName + "_" + "password");       
			userPassword = CloudPropertyHandler.decryptPassword(hexEncryptedPsw, null, null).toCharArray();
		} 
		return userPassword;
	}

    
    /**
     * Create a directory on the server in the peaDirectory 
     * if it does not exist (checks existence before)
     * 
     * @param directoryName	the name of the directory without 
     * 			WebDAV base path and without peaDirectory
     * 
     * @return	null for success, false: errorMessage 
     */
	@Override    
    public String createDirectory(String directoryName) {

    	String urlString = cloudPath
				+ directoryName;
    	String errorReason = dirExists(directoryName);
    	if (errorReason == null) {
    		TestLog.e(WebDavProvider.class, "Dir exists: " + directoryName);
    		return null;
    	} else {
    		if (errorReason.contains("404")){ // just missing directory
    			try {
    				//TestLog.e(WebDavProvider.class, "Try to create directory " + directoryName + "...");
    				sardine.createDirectory(urlString);
    			} catch (Exception e1) {
    				TestLog.e(WebDavProvider.class, "Create directory failed...\n" + urlString + " in " + cloudPath);
    				handleErrors(e1, PeaProperties.getVmBridge().translate("error_occurred_during") + ": " + PeaProperties.getVmBridge().translate("open_directory") ,
    						directoryName);
    				//TestLog.e(WebDavProvider.class, "urlString : " + urlString);
    				return PeaProperties.getVmBridge().translate("error_occurred_during") + ": " + PeaProperties.getVmBridge().translate("open_directory") + ":\n " + directoryName
    						+ "\n" + e1.getLocalizedMessage();
    			}
    			TestLog.e(WebDavProvider.class, "Directory created: " + directoryName);
    			return null;
    		} else {
    			TestLog.e(WebDavProvider.class, "Could not create directory " + directoryName);
    			return PeaProperties.getVmBridge().translate("error_occurred_during") + ": " + PeaProperties.getVmBridge().translate("open_directory") + ":\n " + directoryName
    					+ "\n" + errorReason;
    		}
    	}
    }
    
    /**
     * Move or rename a file on the cloud server and 
     * overwrite existing file
     * 
     * @param origin	the file name to move or rename
     * @param target	the new file
     * 
     * @return	null for success, failure: errorMessage 
     */
/*    public String moveOrRename(String origin, String target) {
    	
    	String originURL = cloudPath + origin;
    	String targetURL = cloudPath + target;
    	//TestLog.e(WebDavProvider.class, "origin: " + originURL);
    	//TestLog.e(WebDavProvider.class, "target: " + targetURL);
    	
    	if (fileExists(origin) != null) {
			handleErrors(null, PeaProperties.getVmBridge().translate("error") + ": \n" + PeaProperties.getVmBridge().translate("rename")
			+ "\n", originURL);
			return PeaProperties.getVmBridge().translate("rename") + " - " 
			+ PeaProperties.getVmBridge().translate("missing") + ": " + origin;
    	}
		// A file or folder can be moved by sending a MOVE request to the file or folder and specifying the destination in the Destination header as full url.
		// The overwrite behavior of the move can be controlled by setting the Overwrite head to T or F to enable or disable overwriting respectively.
	    // This moves a file from one location to another on the remote server. It assumes you want to overwrite all files.
	    try {
			sardine.move(originURL, //"https://eck.cologne/klaut/remote.php/dav/files/Beloumix/PeaFactory/z.txt", 
					targetURL, true);// overwrite			
		} catch (IOException e) {			
			handleErrors(e, PeaProperties.getVmBridge().translate("error_occurred_during") +  ": " + PeaProperties.getVmBridge().translate("rename")
					+ "\n" + originURL + "\n-> ", targetURL);
			return PeaProperties.getVmBridge().translate("error_occurred_during") +  ": " + PeaProperties.getVmBridge().translate("rename")
			+ "\n" + originURL + "\n-> " + targetURL;
		} // "https://eck.cologne/klaut/remote.php/dav/files/Beloumix/PeaFactory/Sub-Folder/z.txtxx");
	    return null;
    } */
    
    /**
     * Delete a file or a folder from the server
     * 
     * @param urlString	URL of the file or folder
     * 
     * @return	null for success, error message is set therwise
     */
/*    public String deleteFileOrFolder(String urlString) {
		
		// A file or folder can be created by sending a DELETE request to the file or folder.
	    try {
			sardine.delete(urlString);
		} catch (IOException e) {
			handleErrors(e, PeaProperties.getVmBridge().translate("error_occurred_during") +  ": " + PeaProperties.getVmBridge().translate("delete"), urlString, true);
			return false;
		} catch (Exception e) {
			handleErrors(e, PeaProperties.getVmBridge().translate("error_occurred_during") +  ": " + PeaProperties.getVmBridge().translate("delete"), urlString, true);
			return false;
		}
	    return true;
    }*/
	

	@Override
	public String deleteFile(String fileName) {
		
		//This uses a HTTP HEAD request to see if a file exists on the remote server.
				if (fileName == null) {
					TestLog.ve(WebDavProvider.class, "is null", 5);
					return PeaProperties.getVmBridge().translate("unexpected_error") + "\nmissing file name";
				}
				if (sardine == null) {
					TestLog.ve(WebDavProvider.class, "is null", 5);
					return PeaProperties.getVmBridge().translate("unexpected_error") + "\nsardine is null";
				}
			    boolean exists = false;	    
				try {
					exists = sardine.exists(cloudPath + fileName);
				} catch (IOException e) {
					TestLog.e(WebDavProvider.class, "File does not exist in " + cloudPath + ": " + fileName);
					handleErrors(e, PeaProperties.getVmBridge().translate("no_access")
							+ "\n" + PeaProperties.getVmBridge().translate("bad_web_connection") + "?", fileName);
					return PeaProperties.getVmBridge().translate("no_access")
							+ "\n" + PeaProperties.getVmBridge().translate("bad_web_connection") + "?";
				} catch (Exception e) {
					TestLog.e(WebDavProvider.class, "Check file exists failed for " + fileName);
					handleErrors(e, PeaProperties.getVmBridge().translate("no_access"), fileName);
					return PeaProperties.getVmBridge().translate("no_access");
				}
				if (exists == false) {
					return PeaProperties.getVmBridge().translate("file_not_found");
				}
		// A file or folder can be created by sending a DELETE request to the file or folder.
	    try {
			sardine.delete(cloudPath + fileName);
		} catch (IOException e) {
			handleErrors(e, PeaProperties.getVmBridge().translate("error_occurred_during") +  ": " + PeaProperties.getVmBridge().translate("delete"), fileName);
			return PeaProperties.getVmBridge().translate("error_occurred_during") +  ": " + PeaProperties.getVmBridge().translate("delete") + " " + fileName;
		} catch (Exception e) {
			handleErrors(e, PeaProperties.getVmBridge().translate("error_occurred_during") +  ": " + PeaProperties.getVmBridge().translate("delete"), fileName);
			return PeaProperties.getVmBridge().translate("error_occurred_during") +  ": " + PeaProperties.getVmBridge().translate("delete") + " " + fileName;
		}
		return null;
	}
    
    /**
     * Copy a file on the cloud server and 
     * overwrite existing file
     * 
     * @param originURL	the file to move or rename
     * @param targetURL	the new file
     * 
     * @return	null for success, false: errorMessage 
     */
 /*   public String copy(String originURL, String targetURL) {
		// A file or folder can be copied by sending a COPY request to the file or folder and specifying the destination in the Destination header as full url.
		// The overwrite behavior of the copy can be controlled by setting the Overwrite head to T or F to enable or disable overwriting respectively.
	    // Sardine: This copies a file from one location to another on the remote server. It assumes you want to overwrite all files.
	    try {
			sardine.copy(originURL, 
					targetURL);
		} catch (IOException e) {
			handleErrors(e, PeaProperties.getVmBridge().translate("error_occurred_during") +  ": " + PeaProperties.getVmBridge().translate("copy")
			+ "\n" + originURL + "\n-> " + targetURL, null, true);
			return false;
		} 
	    return true;
    } */
    
    /**
     * Check if a directory exists in the cloud. This is also 
     * suitable to check the WebDAV base path
     * 
     * @param dirName	the full url with folder name
     * 
     * @return	null for success, 
     * 			the localized reason with error code of failure otherwise
     */
    private String dirExists(String dirName) {

    	try {
			if( sardine.list(dirName, 0, false) == null) {
				return PeaProperties.getVmBridge().translate("unexpected_error");
			}
    	} catch (SardineException e) {
    		if (e.getMessage().contains("timeout")) {
	    		TestLog.e(WebDavProvider.class, "Server timeout for exist request: " + dirName + " - " + e.getLocalizedMessage());
	    		return PeaProperties.getVmBridge().translate("timeout")+ " (timeout)";
    		} else if (e.getMessage().contains("404")) {
	    		TestLog.e(WebDavProvider.class, "Directory does not exist: " + dirName + " (404)");
	    		TestLog.e(getClass(), e.getLocalizedMessage());
	    		return PeaProperties.getVmBridge().translate("missing") + ": " + dirName + " (404)";
			} else if (e.getMessage().contains("400")) {
	    		TestLog.e(WebDavProvider.class, "user name or password failed");
	    		TestLog.e(getClass(), e.getLocalizedMessage());
	    		return PeaProperties.getVmBridge().translate("user_name")
	    				+ " / " + PeaProperties.getVmBridge().translate("password") + " (400)"
	    				+ "\n  " + e.getLocalizedMessage();
			} else if (e.getMessage().contains("401")) {
	    		TestLog.e(WebDavProvider.class, "user name or password failed");
	    		TestLog.e(getClass(), e.getLocalizedMessage());
	    		return PeaProperties.getVmBridge().translate("user_name")
	    				+ " / " + PeaProperties.getVmBridge().translate("password") + " (401)"
	    				+ "\n  " + e.getLocalizedMessage();
			} else if (e.getMessage().contains("405")) {
	    		TestLog.e(WebDavProvider.class, "Wrong WebDAV base path: " + dirName);	
	    		TestLog.e(getClass(), e.getLocalizedMessage());
	    		return PeaProperties.getVmBridge().translate("wrong_webdav_path") + " (405)"
	    				+ "\n  " + e.getLocalizedMessage();
			} else if (e.getMessage().contains("423")) {
	    		TestLog.e(WebDavProvider.class, "WebDAV: The resource that is being accessed is locked: " + dirName);
	    		TestLog.e(getClass(), e.getLocalizedMessage());
	    		return "Locked (423)"
	    				+ "\n  " + e.getLocalizedMessage();
			} else if (e.getMessage().contains("503")) {
	    		TestLog.e(WebDavProvider.class, "Wrong WebDAV base path: " + dirName);
	    		TestLog.ve(WebDavProvider.class, "Error code 503", 5);	
	    		//TestLog.e(getClass(), e.getLocalizedMessage());
	    		return PeaProperties.getVmBridge().translate("wrong_webdav_path") + " (503)"
	    				+ "\n  " + e.getLocalizedMessage();

			} else {
				// unexpected error
				TestLog.e(WebDavProvider.class, e.getLocalizedMessage() + " for " + dirName);	
				TestLog.ve(WebDavProvider.class, e.toString() + " - " + e.getLocalizedMessage() + " for " + dirName, 5);	
				//TestLog.e(getClass(), new ExceptionHelper(e).getInfos());
				return //PeaProperties.getVmBridge().translate("unexpected_error") + "\n  "
						e.getLocalizedMessage();// + "\n  " 
						//+ e.toString();
			}
		} catch (javax.net.ssl.SSLPeerUnverifiedException e) {
    		TestLog.e(WebDavProvider.class, "Wrong WebDAV base path - SSL");	
    		TestLog.e(getClass(), e.getLocalizedMessage() + " - " + e.toString());
    		return PeaProperties.getVmBridge().translate("wrong_webdav_path")
    				+ "\n  " + e.getLocalizedMessage();			
    	} catch (java.net.UnknownHostException e) { // also if connection failed
    		TestLog.e(WebDavProvider.class, "WebDAV base path - Host");	
    		TestLog.e(getClass(), e.getLocalizedMessage() + " - " + e.toString());
    		return PeaProperties.getVmBridge().translate("wrong_webdav_path")
    				+ "\n  " + e.getLocalizedMessage();
    	} catch (Exception e) {
    		TestLog.e(WebDavProvider.class, "Unexpected error...");
			TestLog.e(getClass(), new ExceptionHelper(e).getInfos());
			return PeaProperties.getVmBridge().translate("unexpected_error") + "\n  "
					+ e.getLocalizedMessage() + "\n  " 
					+ e.toString();
    	}
    	return null;
    }
    
    /**
     * Check if a file exists on the server. 
     * This does not work for directories!!!
     * 
     * @param fileName	the file name to check (leading FOLDER/ if in folder)
     * 
     * @return	null if the file exists, 
     * PeaProperties.getVmBridge().translate("file_not_found") if not
     * or an error message, if an error occurs
     */
	@Override    
    public String fileExists(String fileName) {
	    //This uses a HTTP HEAD request to see if a file exists on the remote server.
		if (fileName == null) {
			TestLog.ve(WebDavProvider.class, "is null", 5);
			return PeaProperties.getVmBridge().translate("unexpected_error") + "\nmissing file name";
		}
		if (sardine == null) {
			TestLog.ve(WebDavProvider.class, "is null", 5);
			return PeaProperties.getVmBridge().translate("unexpected_error") + "\nsardine is null";
		}
	    boolean exists = false;	    
		try {
			exists = sardine.exists(cloudPath + fileName);
		} catch (IOException e) {
			TestLog.e(WebDavProvider.class, "File does not exist in " + cloudPath + ": " + fileName);
			handleErrors(e, PeaProperties.getVmBridge().translate("no_access")
					+ "\n" + PeaProperties.getVmBridge().translate("bad_web_connection") + "?", fileName);
			return PeaProperties.getVmBridge().translate("no_access")
					+ "\n" + PeaProperties.getVmBridge().translate("bad_web_connection") + "?";
		} catch (Exception e) {
			TestLog.e(WebDavProvider.class, "Check file exists failed for " + fileName);
			handleErrors(e, PeaProperties.getVmBridge().translate("no_access"), fileName);
			return PeaProperties.getVmBridge().translate("no_access");
		}
		if (exists == true) {
			return null;
		} else {
			return PeaProperties.getVmBridge().translate("file_not_found");
		}
    }	
	
	@Override  
	public long lastModified(String fileName) {
		
		String serverUrl = cloudPath + fileName;
		try {
			if (sardine.exists(serverUrl)  == false) {
				TestLog.e(WebDavProvider.class, "File does not exist: " + fileName);
				return -1;
			}
		} catch (Exception e1) {
			TestLog.e(WebDavProvider.class, "File does not exist: " + fileName);
			return -1;
		}
		// First: Try to get custom property
		// This is the date, the file was modified on the device, not the 
		// modification in the cloud
		// This does not work e.g. for OpenDrive and GMX, but for 
		// Woelki, pCloud, Yandex
		try {
			String dateString =  null;
			Set <QName> set = new HashSet<QName>(); // create a set for query
			set.add(qn);
			/*!!!!!!!!!!!! following failed in ProGuard...*/
			List<DavResource> resources = sardine.list(serverUrl, 0, set);

			// 1. try to get LASTMOD from QName property
			for (DavResource resource : resources) { // there is only one...
			    Map<QName,String> customProps = resource.getCustomPropsNS();
			    dateString = customProps.get(qn);
			    //Map<String,String> customProps2 = resource.getCustomProps();
			    // Use custom properties...
			    //dateString = (String) customProps2.get("LASTMOD");
			}
			if (dateString == null || dateString.equals("")) {
				TestLog.e(WebDavProvider.class, "Get last modification date failed...");
			}
		} catch (Exception e) {
			handleErrors(e, "Failed to get last modification from property ", fileName);
		}
		// Fallback: get last modification in the cloud
		try {
			if (sardine.exists(serverUrl)) {
				List<DavResource> res = sardine.list(serverUrl, 0, true);// 0: one file, true: all properties
				return res.get(0).getModified().getTime();
			} else {
				TestLog.e(getClass(), providerName + ": " + "File to get last modification does not exist: " + serverUrl);
				return -1;
			}
		} catch (Exception e) {
			handleErrors(e, "Failed to get last modification ", fileName);
			return -1;
		}
	}
    
	// TODO 
	// To obtain all the version of a file a normal PROPFIND has to be send to https://cloud.example.com/remote.php/dav/versions/USER/versions/FILEID. This will list the versions for this file.
	// The name is the time stamp of the version.
	// To restore a version all that needs to be done is to move a version the special restore folder at https://cloud.example.com/remote.php/dav/versions/USER/restore
	
	// TODO
	// Uploading large files is always a bit problematic as your connection can be interrupted which will fail your entire upload. Nextcloud has a chunking API where you can upload smaller chunks which will be assembled on the server once they are all uploaded.

    // TODO
	// perform a PROPFIND request to find files in a folder
	
	/**
	 * Handle occurring errors and exceptions
	 * 
	 * @param e				the thrown exception or null
	 * @param message		the message about the failure					
	 * @param fileName		the source of the exception
	 */
	private void handleErrors(Exception e, String message, String fileName) {
		String eMessage = "";
		if (e != null) {
			TestLog.e(getClass(), new ExceptionHelper(e).getInfos());
			eMessage = "\n" + "(" + e.getLocalizedMessage() +")";
		}
		TestLog.e(getClass(), providerName + ": " + eMessage);
	}
	
    /**
     * Set the user password to log in.
     * This is only used for test. 
     * User name should be set in WebDavLogin. 
     * 
     * @param _userName	the user name as String
     */
	public void setUserPasswordX(char[] _userPassword) {
		this.userPassword = _userPassword;
	}
    
    /**
     * Set the user name to log in.
     * This is only used for test. 
     * Password should be set in WebDavLogin. 
     * 
     * @param _userName	the user name as String
     */
	public void setUserNameX(String _userName) {
		this.userName = _userName;
	}

     
    // TODO:
    // search
    // properties of files/folders: size, last modified, content type...
    // access, restore, delete the trash
    // access, restore file versions
    // chunked upload for large files
    
    
	//@Override
	private String authorizeNoTimeout(String _userName, char[] _password, String[] otherParams) {
		TestLog.o(this.getClass(), providerName + " authorize...");

		// check internet connection:
		if (WebTools.checkConnection() == false) {
			TestLog.e(WebDavProvider.class, "No internet connection...");
			return
					providerName + ": " + PeaProperties.getVmBridge().translate("authorization_failed") 
					+ "\n - " + PeaProperties.getVmBridge().translate("bad_web_connection");
		}

		if (_userName != null) {
			this.userName = _userName;
		} else {
			userName = getUserName();
			if (userName == null) {
				TestLog.e(getClass(), providerName + ": " + "Missing user name, authorization cancelled...");
				TestLog.ve(WebDavProvider.class, "is null", 5);
				handleErrors(null, PeaProperties.getVmBridge().translate("authorization_failed") 
						+ " - " + PeaProperties.getVmBridge().translate("user_name")+ " " + PeaProperties.getVmBridge().translate("missing"), providerName);
				return
						providerName + ": " + PeaProperties.getVmBridge().translate("authorization_failed") 
						+ " - " + PeaProperties.getVmBridge().translate("user_name")+ " " 
						+ PeaProperties.getVmBridge().translate("missing") + 
						"\n -> " + PeaProperties.getVmBridge().translate("cloud") + " -> " + PeaProperties.getVmBridge().translate("modify_cloud_provider");
			}
		}
		// Check webDavBasePath:
		if (otherParams != null) {
			this.webDavBasePath = otherParams[0];
		}

		if (this.webDavBasePath == null) {
			TestLog.e(WebDavProvider.class, "Missing WebDAV base path in authorization process. Try to set from properies...");
			this.webDavBasePath = getWebDavBasePath();
			if (this.webDavBasePath == null) {
				TestLog.e(WebDavProvider.class, "Couldn't set WebDAV base path for " + providerName);
				return
						providerName + " - " + PeaProperties.getVmBridge().translate("authorization_failed") + ":\n "
						+ PeaProperties.getVmBridge().translate("wrong_webdav_path")
						+  " (" + PeaProperties.getVmBridge().translate("missing") +")"+ 
						"\n -> " + PeaProperties.getVmBridge().translate("cloud") + " -> " + PeaProperties.getVmBridge().translate("modify_cloud_provider");
			} 
		}
		if (! this.webDavBasePath.endsWith("/")) {
			this.webDavBasePath = this.webDavBasePath + "/";
		}

		//TestLog.e(WebDavProvider.class, "Auth webDavBase: " + this.webDavBasePath);
		try {
			// check password
			char[] psw = _password;			
			String userPasswordString = null;
			if (psw == null) {
				TestLog.e(getClass(), providerName + ": " + "Authorize with null password...");
				psw = getUserPassword();
				if (psw == null) {
					TestLog.e(getClass(), providerName + ": " + "Missing password, authorization cancelled...");
					TestLog.ve(WebDavProvider.class, "is null", 5);
					handleErrors(null, PeaProperties.getVmBridge().translate("authorization_failed") 
							+ " - " + PeaProperties.getVmBridge().translate("password")+ " " + PeaProperties.getVmBridge().translate("missing"), providerName);
					return 
							providerName + ": " + PeaProperties.getVmBridge().translate("authorization_failed") 
							+ " - " + PeaProperties.getVmBridge().translate("password")+ " " + PeaProperties.getVmBridge().translate("missing")+ 
							"\n -> " + PeaProperties.getVmBridge().translate("cloud") + " -> " + PeaProperties.getVmBridge().translate("modify_cloud_provider");
				}
			} else { // psw is not null
				userPasswordString = new String(psw);	
				//Zeroizer.zero(psw); - is used to store in properties later...
			}
			sardine = new OkHttpSardine();// SardineFactory.begin();     
			sardine.setCredentials(userName, userPasswordString );

		} catch (Exception e) {
			TestLog.e(getClass(), providerName + ": " + "Authorization failed...");
			handleErrors(e, PeaProperties.getVmBridge().translate("authorization_failed") 
					+ " (" + PeaProperties.getVmBridge().translate("user_name") + "/" 
					+ PeaProperties.getVmBridge().translate("password")+ ")", userName);
			return
					providerName + ": " + PeaProperties.getVmBridge().translate("authorization_failed") 
					+ " (" + PeaProperties.getVmBridge().translate("user_name") + "/" 
					+ PeaProperties.getVmBridge().translate("password")+ ")"+ 
					"\n -> " + PeaProperties.getVmBridge().translate("cloud") + " -> " + PeaProperties.getVmBridge().translate("modify_cloud_provider");
		}
		String errorReason = dirExists(this.webDavBasePath);
		if (errorReason != null) {
			TestLog.e(WebDavProvider.class, "Authorization failed. WebDAV base path does not exist: " + this.webDavBasePath);
			return 
					providerName + ": " + PeaProperties.getVmBridge().translate("authorization_failed") + ":\n "
					+  "(" + errorReason +")"+ 
					"\n -> " + PeaProperties.getVmBridge().translate("cloud") + " -> " + PeaProperties.getVmBridge().translate("modify_cloud_provider") + "\n\n";
		}
		try {
			// create if not already exist
			if (dirExists(this.webDavBasePath + peaDirectory) != null) {
				sardine.createDirectory(this.webDavBasePath + peaDirectory );
			}
		} catch (IOException e) {
			String message = e.getMessage();
			if (message == null) {
				TestLog.e(getClass(), providerName + ": " + "Create directory failed...\n" + peaDirectory + " in " + this.webDavBasePath);
				handleErrors(e, PeaProperties.getVmBridge().translate("error_occurred_during") + ": " + PeaProperties.getVmBridge().translate("open_directory") ,
						peaDirectory);
				return
						providerName + ": " + PeaProperties.getVmBridge().translate("authorization_failed") 
						+ ":\n" + e.getLocalizedMessage() + "\n(" + e.toString() + ")";
			} else if (message.contains("405")) {
				// e.g. create directory but parent folder is missing
				handleErrors(e, PeaProperties.getVmBridge().translate("error_occurred_during") + ": create directory",
						peaDirectory);
				return
						providerName + ": " + PeaProperties.getVmBridge().translate("authorization_failed") + ":\n" + 
						PeaProperties.getVmBridge().translate("wrong_webdav_path") + "\n" 
						+ e.getLocalizedMessage() + " (405)";

			} else if (message.contains("409")) { // Conflict
				// com.github.sardine.impl.SardineException: status code: 409, reason phrase: Unexpected response (409 Conflict)
				handleErrors(e, PeaProperties.getVmBridge().translate(providerName + ": " + "error_occurred_during") + ": create directory",
						peaDirectory);
				return 
						providerName + ": " + PeaProperties.getVmBridge().translate("authorization_failed") 
						+ ":\n" + e.getLocalizedMessage();
			} else if (message.contains("400")) { // Unexpected response (400 Bad request)
				handleErrors(e, PeaProperties.getVmBridge().translate(providerName + ": " + "error_occurred_during") + ": create directory",
						peaDirectory);
				return 
						providerName + ": " + PeaProperties.getVmBridge().translate("authorization_failed") + ":\n "
						+  PeaProperties.getVmBridge().translate("wrong_webdav_path") 
						+ " (status 400)";
			} else if (message.contains("301")) { //moved permanently, may still work
				TestLog.e(WebDavProvider.class, "providerName + \": \" + Create directory failed...\n" + peaDirectory + " in " + this.webDavBasePath);
				handleErrors(e, PeaProperties.getVmBridge().translate("error_occurred_during") + ": " + PeaProperties.getVmBridge().translate("open_directory") ,
						peaDirectory);
				return 
						providerName + ": " 
						+ ":\n" + e.getLocalizedMessage() + "\n(" + e.toString() + ")";
			} else {
				TestLog.e(WebDavProvider.class, "providerName + \": \" + Create directory failed...\n" + peaDirectory + " in " + this.webDavBasePath);
				handleErrors(e, PeaProperties.getVmBridge().translate("error_occurred_during") + ": " + PeaProperties.getVmBridge().translate("open_directory") ,
						peaDirectory);
				return 
						providerName + ": " + PeaProperties.getVmBridge().translate("authorization_failed") 
						+ ":\n" + e.getLocalizedMessage() + "\n(" + e.toString() + ")";
			} 
		} catch (Exception e) {
			TestLog.e(WebDavProvider.class, "Create directory failed...\n" + peaDirectory + " in " + this.webDavBasePath);
			handleErrors(e, PeaProperties.getVmBridge().translate("error_occurred_during") + ": " + PeaProperties.getVmBridge().translate("open_directory") ,
					peaDirectory);
			return
					PeaProperties.getVmBridge().translate("authorization_failed") 
					+ ":\n" + e.getLocalizedMessage() + "\n(" + e.toString() + ")";
		}
		this.cloudPath = this.webDavBasePath + peaDirectory + "/";
		
		//Help.printStack(WebDavProvider.class, "=====================begin() + setCredentials()", 25, true);
		//TestLog.e(WebDavProvider.class, "Directory " + peaDirectory + " successfully created");
		return null;
	}

	@Override
	public String[] getCloudFiles(String[] extensionFilters) {

		List<DavResource> resources = null;
		if (sardine == null) {
			TestLog.e(getClass(), providerName + " not authorized...");
			TestLog.ve(getClass(), "Sardine instance is null", 5);
			//new UnexpectedValueException("Sardine", "sardine", "is null").printDescription();
			return null;
		}
		String urlString = cloudPath;
		if (urlString == null) {
			TestLog.e(WebDavProvider.class, "Missing cloud path");
			TestLog.ve(getClass(), "URL String is null", 5);
			//new UnexpectedValueException("String", "urlString", "is null").printDescription();
		}

		try { // /remote.php/dav

			// perform a PROPFIND request to find files in a folder
			// contents of a folder can be listed by sending a PROPFIND request to the folder.
			resources = sardine.list(urlString);
		} catch (IOException e2) {
			handleErrors(e2, "Failed to list files on cloud server ", urlString);
		}
		if (resources == null || resources.size() == 0) {
			TestLog.e(WebDavProvider.class, "No files...");
			return null;
		}
		int resourcesLen = resources.size();
		int extensionsLen = 0;
		if (extensionFilters != null) {
			extensionsLen = extensionFilters.length;
		}
		ArrayList<String> fileList = new ArrayList<String>();
		
		//String[] fileNames = new String[len];
		for (int i = 0; i < resourcesLen; i++) {
			DavResource res = resources.get(i);
			String fileName = res.getPath();// + File.separator + res.getName();
			if (extensionFilters == null) {
				// include the file name
				int indexPF = fileName.indexOf(peaDirectory);
				if (indexPF >= 0  && fileName.length() >= indexPF + 11) {
					String fileNameToAdd = fileName.substring(indexPF + 11);
					if (fileNameToAdd != null && fileNameToAdd.length() > 0) {
						fileList.add(fileNameToAdd);
					}
				} 
			} else {
				// check all extensions:
				for (int j = 0; j < extensionsLen; j++) {
					if (fileName.endsWith("." + extensionFilters[j])) {
						// include the file name
						int indexPF = fileName.indexOf(peaDirectory);
						if (indexPF >= 0  && fileName.length() >= indexPF + 11) {
							String fileNameToAdd = fileName.substring(indexPF + 11);
							if (fileNameToAdd != null && fileNameToAdd.length() > 0) {
								fileList.add(fileNameToAdd);
							}
						} 
						break;// do not check other extensions
					}
				}         
			}
		}
		return fileList.toArray(new String[0]);
	}
	
	@Override
	public String openFile(String cloudFileName, String storedFilePath) {
		TestLog.ve(WebDavProvider.class, "not implemented", 5);
	return "ERROR";}
	
	@Override
	public String downloadFiles(String[] fileNames, String targetDir) {

		StringBuilder errorMessage = new StringBuilder();
		
		if (fileNames == null || fileNames.length == 0) {
			if (sardine == null) {
				TestLog.ve(getClass(), "Sardine instance is null");
				return null;
			} else {
				TestLog.e(WebDavProvider.class, "Missing or empty file to download...");
				TestLog.ve(WebDavProvider.class, "No file to download for " + providerName, 5);
				//new UnexpectedValueException("String[]", "fileNames", "is null").printDescription();
				return PeaProperties.getVmBridge().translate("file_not_found") + " - " + providerName;
			}
		}		
		int len = fileNames.length;
		int failuedFileseCount = 0;
		for (int i = 0; i < len; i++) {
			if (fileNames[i] == null) { 		
				TestLog.ve(WebDavProvider.class, "is null", 5);
				errorMessage.append("\n" + PeaProperties.getVmBridge().translate("file_not_found") + " (index " + i + ")");
				continue;
			}		
			InputStream is;
			try {
				if ( sardine == null) {
					TestLog.ve(WebDavProvider.class, "is null", 5);
					errorMessage.append("\n" + PeaProperties.getVmBridge().translate("unexpected_error") + " (sardine missing)");
					break;
				}
				is = getWithTimeout(fileNames[i], sardine);				

				if (downloadErrorMessage != null) {//TODO test
					failuedFileseCount ++;
					errorMessage.append("\n" + fileNames[i] + " - " + downloadErrorMessage );
					if (is != null) {
						is.close();
					}
					fileNames[i] = null;
					downloadErrorMessage = null;
					continue;
				} else {
					if (is == null) {
						failuedFileseCount ++;
						errorMessage.append("\n" + fileNames[i] + " - " + PeaProperties.getVmBridge().translate("unexpected_error") );
						fileNames[i] = null;
						continue;
					}
				}

				// is.available() does not work
				File targetFile = null;
				if (targetDir.endsWith(File.separator)) {
					targetFile = new File(targetDir + fileNames[i]);
				} else {
					targetFile = new File(targetDir + File.separator + fileNames[i]);
				}
				if (targetFile.exists() == false) {
					// create file
					targetFile.getParentFile().mkdirs(); 
					boolean created = targetFile.createNewFile();
					if (created == false) {
						TestLog.ve(WebDavProvider.class, "file was not created", 5);
						errorMessage.append("\n" + PeaProperties.getVmBridge().translate("unexpected_error") + " (file creation)");
						continue;
					}
				} else {
					TestLog.v(WebDavProvider.class, "File is overwritten: " + targetFile);
				}
				
				OutputStream os = new FileOutputStream(targetFile);
				byte[] buffer = new byte[2048];
				int bytesRead;
				int byteSum = 0;
				while((bytesRead = is.read(buffer)) !=-1){
                    os.write(buffer, 0, bytesRead);
                    byteSum += bytesRead;
				}
				if (byteSum == 0) { // empty
					TestLog.e(WebDavProvider.class, "Empty file: " + fileNames[i]);
					errorMessage.append("\n" + fileNames[i] + " - " +
					PeaProperties.getVmBridge().translate("empty_file"));
					failuedFileseCount ++;
					continue;
				}
				if (os != null) {
					os.flush();
					os.close();
				}
				if (is != null) {
					//is.close();
				}
			} catch (IOException e) {
				TestLog.e(WebDavProvider.class, "Download failed: " + fileNames[i] + " in " + targetDir);
				handleErrors(e, PeaProperties.getVmBridge().translate("download_failed"), fileNames[i]);
				failuedFileseCount ++;
				errorMessage.append("\n" + fileNames[i] + " - " + e.getLocalizedMessage());
			} catch (Exception e) {
				TestLog.e(WebDavProvider.class, "Download failed: " + fileNames[i] + " in " + targetDir);
				handleErrors(e, PeaProperties.getVmBridge().translate("download_failed"), fileNames[i]);
				failuedFileseCount ++;
				errorMessage.append("\n" + fileNames[i] + " - " + e.getLocalizedMessage());
			}
		}		
		if (failuedFileseCount > 0) {
			String allFiles = "";
			if (failuedFileseCount == fileNames.length) {
				TestLog.e(WebDavProvider.class, "All file were invalid...");
				allFiles = " (all files)";
			}
			TestLog.ve(WebDavProvider.class, providerName + ": " + PeaProperties.getVmBridge().translate("download_failed") + allFiles + ": "// first line
					+ errorMessage, 3);
			return providerName + ": " + PeaProperties.getVmBridge().translate("download_failed") + allFiles + ": "// first line
					+ errorMessage; // line by line: file name - reason
		} else {
			TestLog.v(WebDavProvider.class, "All files successfully downloaded");
		}
		return null;
	}
	
	/**
	 * Upload content as files in the folder of the cloud
	 * 
	 * @param map		the map <String, byte[]> (File names, content)
	 * @param folder	the folder to store the files or null
	 * 
	 * @return	null for success, an error message to show
	 * 			in a dialog about the failure otherwise
	 */
	@SuppressWarnings("deprecation")
	@Override
	public String uploadContent(HashMap<String, byte[]> map, String folder) { // TODO lastmod of content
		
		StringBuilder errorMessage = new StringBuilder();
		
		if (map == null || map.size() == 0) {
			TestLog.e(WebDavProvider.class, "No content to upload...");
			TestLog.ve(WebDavProvider.class, "is null", 5);
			return providerName + ": " + PeaProperties.getVmBridge().translate("file_not_found");
		}		
		//ArrayList<String> failedFileNames = new ArrayList<String>();
		int failedFilesCount = 0;
		Iterator<Map.Entry<String, byte[]>> it = map.entrySet().iterator();
		//for (int i = 0; i < len; i++) {
		 while (it.hasNext()) {
		        Map.Entry<String, byte[]> pair = (Map.Entry<String, byte[]>)it.next();
			// A file can be uploading by sending a PUT request to the file and sending the raw file contents as the request body.
			// check if content exists 
			String fileName = pair.getKey();
			byte[] content = pair.getValue();
			if (content == null || content.length == 0) {
				TestLog.e(WebDavProvider.class, "Empty content to upload - " + fileName);
				handleErrors(null, "Content to upload does not exist ", fileName);
				errorMessage.append("\n" + providerName + ": " + fileName + " - " + PeaProperties.getVmBridge().translate("can_not_find"));
				failedFilesCount++;
				continue;
			}
			if (fileName == null || fileName.length() == 0) {
				TestLog.e(WebDavProvider.class, "Missing file name to upload");
				handleErrors(null, "Missing file name to upload ", "");
				errorMessage.append("\n" + providerName + ": " + "File name - " + PeaProperties.getVmBridge().translate("missing"));
				failedFilesCount++;
				continue;
			}
			String urlString = null;
			if (folder == null) {
				urlString = cloudPath + fileName;
			} else {
				urlString = cloudPath + folder + "/" + fileName;
			}
			//TestLog.e(WebDavProvider.class, "urlString: " + urlString);
			try {
				if ( sardine.exists(urlString)) {
					TestLog.v(this.getClass(), providerName + ": " + "Replace existing file " + fileName + "...");
				} else {
					TestLog.v(this.getClass(), providerName + ": " + "Upload new file " + fileName + "...");
				}
			} catch (SardineException e) {
				if (e.getMessage().contains("423")) { // Locked
					TestLog.e(WebDavProvider.class, providerName + ": " + "Upload content - file exist: " + providerName + ", " + fileName + ": ");
					TestLog.e(WebDavProvider.class, providerName + ": " + new ExceptionHelper(e).getInfos());
				} else {
					TestLog.e(WebDavProvider.class, "Upload content - file exist: " + providerName + ", " + fileName + ": ");
					TestLog.e(getClass(), providerName + ": " + new ExceptionHelper(e).getInfos());;
				}
			} catch (Exception e1) {
				TestLog.e(WebDavProvider.class, "Upload content - file exist: " + providerName + ", " + fileName + ": ");
				TestLog.e(WebDavProvider.class, providerName + ": " + new ExceptionHelper(e1).getInfos());
			}
			
			// Read the content:
			if (content.length >= Integer.MAX_VALUE) {
				TestLog.e(WebDavProvider.class, "Content to upload is too long");
				errorMessage.append("\n" + providerName + ": " + fileName + " - " + PeaProperties.getVmBridge().translate("file_too_large"));
				failedFilesCount++;
				continue;
			}
			try {

				String contentType = "application/octet-stream";

				//TestLog.e(WebDavProvider.class, "Upload: "  + urlString + "\n--" + contentType + "\n--" + fileName);
				// For example GMX need the content type of the file...
				sardine.put(urlString, 
						content, //new FileInputStream(files[i]), does not work
						contentType );
				TestLog.v(WebDavProvider.class, "Uploaded: " + fileName);	
			} catch (IOException e) {
				String msg = e.getMessage();
				if (msg.contains("443") && msg.contains("failed to respond")) {
					TestLog.e(WebDavProvider.class, new ExceptionHelper(e).getInfos() + 
							"... but it should have been uploaded.");
				} else if (msg.contains("423") && msg.contains("Error contacting")) {
					TestLog.e(WebDavProvider.class, new ExceptionHelper(e).getInfos() + 
							"... but it should have been uploaded.");
				} else {
					TestLog.e(WebDavProvider.class, e.getLocalizedMessage() + " - " + e.toString());
					TestLog.ve(WebDavProvider.class, e.getLocalizedMessage() + " - " + e.toString(), 5);
					errorMessage.append("\n" + providerName + ": " + fileName + " - " + e.getLocalizedMessage());
					failedFilesCount++;
					handleErrors(e, providerName + ": " + "Failed to upload (put)", fileName);
					continue;
				}
			} catch (Exception e) {
				TestLog.e(WebDavProvider.class, providerName + ": " + "Unexpected exception for " + fileName + ": " + e.toString());
				TestLog.ve(WebDavProvider.class, e.getLocalizedMessage() + " - " + e.toString(), 5);
				errorMessage.append("\n" + providerName + ": " + fileName + " - " + e.getLocalizedMessage());
				failedFilesCount++;
				handleErrors(e, "Failed to upload (put)", fileName);		
				continue;
			}
			
			// try to set lastModification:
			// Set custom properties on a resource:
			Map<QName,String> addProps = new HashMap<QName,String>(1);
			addProps.put(qn,  "" + System.currentTimeMillis());//files[i].lastModified());		
			try {
				sardine.patch(urlString, addProps);
			} catch (Exception e) {
				//TestLog.e(WebDavProvider.class, "Unexpected exception for " + files[i].getName() + ": " + e.toString());
				handleErrors(e, providerName + ": " + "Failed to set last modification property ", fileName);
				HashMap<String, String> sMap = new HashMap<String, String>();
				sMap.put("LASTMOD", "" + System.currentTimeMillis());
				try {
					sardine.setCustomProps(urlString, sMap, null);
				} catch (IOException e1) {
					TestLog.e(WebDavProvider.class, "Setting LASTMOD failed again...");
				} catch (Exception e1) {
					TestLog.e(WebDavProvider.class, "Setting LASTMOD failed again - " + e1.getLocalizedMessage());
				}
				TestLog.e(WebDavProvider.class, "Successfully set LASTMOD...");
			}			
		}
		if (failedFilesCount > 0 && errorMessage.length() > 0) {
			String allFiles = "";
			if (failedFilesCount == map.size()) {
				TestLog.e(getClass(), providerName + ": " + "All contents were invalid...");
				allFiles = " (all contents)";
			}
			return providerName + ": " + PeaProperties.getVmBridge().translate("upload_failed") + allFiles +": " // first line
			+ errorMessage; // line by line (file - reason)
		} else {
			return null;
		}
	}

	@SuppressWarnings("deprecation")
	@Override
	public String uploadFiles(File[] files, String folder) {
		
		StringBuilder errorMessage = new StringBuilder();
		
		if (files == null || files.length == 0) {
			TestLog.e(WebDavProvider.class, "No file to upload...");
			TestLog.ve(WebDavProvider.class, "is null", 5);
			return providerName + ": " + PeaProperties.getVmBridge().translate("file_not_found");
		}		
		int len = files.length;
		int failedFilesCount = 0;
		for (int i = 0; i < len; i++) {
			// A file can be uploading by sending a PUT request to the file and sending the raw file contents as the request body.			
			// check if file exists and is not empty
			if (! files[i].exists()) {
				TestLog.e(WebDavProvider.class, "File to upload does not exist: " + files[i].getAbsolutePath());
				//handleErrors(null, providerName + ": " + "File to upload does not exist ", files[i].getAbsolutePath());
				TestLog.ve(WebDavProvider.class, "does not exist: " + files[i].getAbsolutePath(), 5);
				errorMessage.append("\n" + providerName + ": " + files[i].getAbsolutePath() + " - " + PeaProperties.getVmBridge().translate("can_not_find"));
				failedFilesCount++;
				continue;
			}
			if (files[i].length() == 0) {
				TestLog.e(WebDavProvider.class, "Empty file to upload: " + files[i].getAbsolutePath());
				handleErrors(null, providerName + ": " + "File to upload is empty ", files[i].getAbsolutePath());
				errorMessage.append("\n"  + providerName + ": " +  files[i].getAbsolutePath() + " - " + PeaProperties.getVmBridge().translate("empty_file"));
				failedFilesCount++;
				continue;
			}
			String urlString = null;
			if (folder == null) {
				urlString = cloudPath + files[i].getName();
			} else {
				urlString = cloudPath + folder + "/" + files[i].getName();
			}
			//TestLog.e(WebDavProvider.class, "urlString: " + urlString);
			try {
				if ( sardine.exists(urlString) ) {
					TestLog.o(this.getClass(), providerName + ": " + "Replace existing file " + files[i].getName() + "...");
				} else {
					TestLog.o(this.getClass(), providerName + ": " + "Upload new file " + files[i].getName() + "...");
				}
			} catch (SardineException e) {
				if (e.getMessage().contains("423")) { // Locked
					TestLog.e(WebDavProvider.class, "Upload files - exist: " + providerName + ", " + files[i].getAbsolutePath() + ": ");
					TestLog.e(WebDavProvider.class, providerName + ": " + new ExceptionHelper(e).getInfos());
				} else {
					TestLog.e(WebDavProvider.class, "Upload files - exist: " + providerName + ", " + files[i].getAbsolutePath() + ": ");
					TestLog.e(getClass(), providerName + ": " + new ExceptionHelper(e).getInfos());
					//continue;
				}
			} catch (Exception e1) {
				TestLog.e(WebDavProvider.class, "Upload files - exist: " + providerName + ", " + files[i].getAbsolutePath() + ": ");
				TestLog.e(WebDavProvider.class, providerName + ": " + new ExceptionHelper(e1).getInfos());
				//continue;
			}
			
			// Read the content:
			if (files[i].length() >= Integer.MAX_VALUE) {
				TestLog.e(WebDavProvider.class, "File to upload is too long: " + files[i].getAbsolutePath());
				errorMessage.append("\n" + providerName + ": " + files[i].getAbsolutePath() + " - " + PeaProperties.getVmBridge().translate("file_too_large"));
				failedFilesCount++;
				continue;
			}
			
			try {
				String contentType = Files.probeContentType( files[i].toPath());
				if (contentType == null || contentType.equals("")) {
					contentType = "application/octet-stream";
				}
				//TestLog.v(this.getClass(), urlString + "\n--" + contentType + "\n--" + files[i].getAbsolutePath());
				// For example GMX need the content type of the file...
				if (files[i].length() > 1024*1024*2) { // 2MB
					TestLog.v(WebDavProvider.class, "Large file: "  + files[i].getName() + " - " + files[i].length());					
					//sardine.put(urlString, new FileInputStream(files[i]));
				}
				sardine.put(urlString,  // TODO stream
						files[i],
						contentType );
				TestLog.v(this.getClass(), providerName + ": " + "Uploaded: " + files[i].getAbsolutePath());
			} catch (FileNotFoundException e) {
				TestLog.e(WebDavProvider.class, "File to upload no found: " + files[i].getAbsolutePath());
				errorMessage.append("\n" + providerName + ": " + files[i].getAbsolutePath() + " - " + e.getLocalizedMessage());
				failedFilesCount++;
				handleErrors(e, providerName + ": " + "Failed to upload ", files[i].getAbsolutePath());			
				continue;
			} catch (IOException e) {
				if (e.getMessage().contains("423") && e.getMessage().contains("Error contacting")) {
					TestLog.e(WebDavProvider.class, new ExceptionHelper(e).getInfos() + 
							"... but it should have been uploaded.");
				} else {
					TestLog.e(getClass(), files[i].getAbsolutePath() +" - " + new ExceptionHelper(e).getInfos());
					errorMessage.append("\n" + providerName + ": " + files[i].getAbsolutePath() + " - " + e.getLocalizedMessage());
					failedFilesCount++;
					handleErrors(e, providerName + ": " + "Failed to upload ", files[i].getAbsolutePath());
					continue;
				}
			} catch (Exception e) {
				TestLog.e(WebDavProvider.class, "Unexpected exception for " + files[i].getName() + ": " + e.toString());
				errorMessage.append("\n" + providerName + ": " + files[i].getAbsolutePath() + " - " + e.getLocalizedMessage() + ", " + e.toString());
				failedFilesCount++;
				handleErrors(e, providerName + ": " + "Failed to upload ", files[i].getAbsolutePath());			
				continue;
			}
			
			// try to set lastModification:
			// Set custom properties on a resource:
			Map<QName,String> addProps = new HashMap<QName,String>(1);
			addProps.put(qn,  "" + files[i].lastModified());		
			try {
				sardine.patch(urlString, addProps);
			} catch (Exception e) {
				//TestLog.e(WebDavProvider.class, "Unexpected exception for " + files[i].getName() + ": " + e.toString());
				handleErrors(e, providerName + ": " + "Failed to set last modification property ", files[i].getAbsolutePath());
				HashMap<String, String> sMap = new HashMap<String, String>();
				sMap.put("LASTMOD", "" + files[i].lastModified());
				try {
					sardine.setCustomProps(urlString, sMap, null);
				} catch (IOException e1) {
					TestLog.e(WebDavProvider.class, "Setting LASTMOD failed again...");
				} catch (Exception e1) {
					TestLog.e(WebDavProvider.class, "Setting LASTMOD failed again - " + e1.getLocalizedMessage());
				}
				TestLog.e(WebDavProvider.class, "Successfully set LASTMOD...");
			}			
		}
		if (failedFilesCount > 0 && errorMessage.length() > 0) {
			String allFiles = "";
			if (failedFilesCount == files.length) {
				TestLog.e(WebDavProvider.class, "All file were invalid...");
				allFiles = " (all files)";
			}
			return providerName + ": " + PeaProperties.getVmBridge().translate("upload_failed") + allFiles +": " // first line
			+ errorMessage; // line by line (file - reason)
		} else {
			return null;
		}
	}
	
	@Override
	public String unlockAllX() {		
		TestLog.ve(WebDavProvider.class, "not implemented", 5);
	return "ERROR";}
	/*	boolean success = true;		
		int len = lockedFiles.size();
		for (int i = 0; i < len; i++) {
			success = unLockFileX(lockedFiles.get(i));					
			if (success == false) {
				TestLog.e(WebDavProvider.class, "Unlock failed for " + lockedFiles.get(i));
				if (errorMessage != null) {
					errorMessage.append("Unlock failed for " + lockedFiles.get(i));
				} else {
					errorMessage = new StringBuilder();
					errorMessage.append("Unlock failed for " + lockedFiles.get(i) + "\n");
				}
			}
		}		
		lockedFiles.clear();
		return success;
	}*/
	
	@Override
	public String unLockFileX(String fileName) {
		TestLog.ve(WebDavProvider.class, "not implemented", 5);
	return "ERROR";}
/*		// the file name contains ~ (leading of file name, not directory)
		// remove leading ~
		String target = null;
		if (fileName.contains("/")) { // with folder
			target = fileName.substring(0, fileName.lastIndexOf("/") + 1)
					+ fileName.substring(fileName.lastIndexOf("/")+ 2, fileName.length());
		} else { // just file name
			target = fileName.substring(1, fileName.length());
		}

		boolean success = moveOrRename(fileName, target);
		if (success == false) {
			handleErrors(null, PeaProperties.getVmBridge().translate("error")
					+ "\nUnlock: Couldn't rename file ", target);
			PeaDialog.showMessage(owner, 
					"Couldn't rename " + target, PeaProperties.getVmBridge().translate("error"), 0);

			return false;
		}
		if (lockedFiles != null) {
			// If the list does not contain the element, it is unchanged
			lockedFiles.remove(fileName);
		}
/*		try {
			sardine.unlock(getWebDavBasePath() + fileName, lockToken);
		} catch (Exception e) {
			// TODO language
			handleErrors(e, "Failed to lock ", fileName,false);
		} */
/*		return true;
	}*/
	
	@Override
	public String lockFileX(String fileName) {
		TestLog.ve(WebDavProvider.class, "not implemented", 5);
	return "ERROR";}
/*		// add a leading ~ to the file name, not the directory
		String target = null;
		if (fileName.contains("/")) { // with folder
			target = fileName.substring(0, fileName.lastIndexOf("/") + 1)
					+ "~" 
					+ fileName.substring(fileName.lastIndexOf("/")+ 1, fileName.length());
		} else { // file name cobtains no directory
			target = "~" + fileName;
		}
		boolean success = moveOrRename(fileName, target);
		if (success == false) {
			handleErrors(null, PeaProperties.getVmBridge().translate("error")
					+ "\nCouldn't rename file to mark in process ", fileName);
			PeaDialog.showMessage(owner, 
					"Couldn't rename file to mark in process  " + fileName, PeaProperties.getVmBridge().translate("error"), 0);

			return false;
		}
		// add the target file name to the list of locked files
		lockedFiles.add(target);
	/*	try { // write lock only
			lockToken = sardine.lock(getWebDavBasePath() + fileName);
		} catch (Exception e) {
			// TODO language
			handleErrors(e, "Failed to unlock ", fileName,false);
		} */
/*		return true;
	}*/
	
	@Override
	public String closeSession() {
		try {
//			unlockAll();
			if (sardine != null) {
//				sardine.disablePreemptiveAuthentication();
				sardine.setCredentials("", "");
				if (sardine != null) {
					//sardine.shutdown();
				}
			}
		} catch (Exception e) {
			handleErrors(e, "Failed to close session ", "");
			return "Failed to close session";
		}
		return null;
	}
	
	@Override
	public String getProviderName() {
		return providerName;
	}

	@Override
	public void setProviderName(String name) {
		this.providerName = name;		
	}
	
    /**
     * Get the base path of the server without peaDirectory
     * The folder PEAFactory must be added afterwards
     * 
     * @return	the base path of the server
     */
	public String getWebDavBasePath() {
		if (this.webDavBasePath != null) {
			if (! this.webDavBasePath.endsWith("/")) {
				this.webDavBasePath = this.webDavBasePath + "/";
			}
		} else {
			TestLog.e(WebDavProvider.class, "Try to get WebDAV base path from properties...");
			String providerName = getProviderName();
			if (providerName == null) {
				TestLog.e(WebDavProvider.class, "Missing provider name...\nCan't get WebDAV base path...");
				TestLog.ve(WebDavProvider.class, "is null", 5);
				return null;
			}
			this.webDavBasePath = CloudPropertyHandler.getSingleLoginProperty(providerName, "webdav");

			if (this.webDavBasePath != null) {
				if (! this.webDavBasePath.endsWith("/")) {
					this.webDavBasePath = this.webDavBasePath + "/";
				}
			} else {
				TestLog.ve(WebDavProvider.class, "is null", 5);
				//PeaDialog.showMessage(owner, "WebDAV base path is zero", 
				//		PeaProperties.getVmBridge().translate("error"), 0);
				return null;
			}			
		}
		return this.webDavBasePath;
	}
	
	/**
	 * Set the WebDAV base path of the server.
	 * This must be an URL with http...
	 * This method will always add a "/" if not present.
	 * 
	 * @param _newBasePath	the WebDAV base path to set
	 */
	public void setWebDavBasePath(String _newBasePath) {
		if (_newBasePath == null || _newBasePath.equals("")) {
			TestLog.ve(WebDavProvider.class, "is null", 5);;
			return;
		}
		if (_newBasePath != null &&  ! _newBasePath.endsWith("/")) {
			_newBasePath = _newBasePath + "/";
		}
		this.webDavBasePath = _newBasePath;
	}

	@Override
	public String getProviderInfos() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getDefaultPath() {
		return cloudPath;
	}
	
	private InputStream getWithTimeout(String fileName, Sardine dSardine) {
		ExecutorService executor = Executors.newCachedThreadPool();
		Callable<InputStream> task = new Callable<InputStream>() {
			public InputStream call() {
				InputStream is = null;
				try {
					if (downloadErrorMessage == null) {
						is = dSardine.get(cloudPath + fileName);
					}
				} catch (SardineException e) {
					if (e.getMessage().contains("404")) { // not found
						TestLog.e(WebDavProvider.class, "Download - Not found: " + fileName);
						handleErrors(e, PeaProperties.getVmBridge().translate("download_failed"), fileName);
						downloadErrorMessage =  PeaProperties.getVmBridge().translate("file_not_found");//e.getLocalizedMessage());
					} else {
						TestLog.e(WebDavProvider.class, "Download failed (1): " + fileName);
						handleErrors(e, PeaProperties.getVmBridge().translate("download_failed"), fileName);
						if (downloadErrorMessage == null) {
							downloadErrorMessage =  PeaProperties.getVmBridge().translate("download_failed") + " " + e.getLocalizedMessage();
						} else {
							downloadErrorMessage = downloadErrorMessage + ", " + e.getLocalizedMessage();
						}
					}

				} catch (IOException e) {
					TestLog.e(WebDavProvider.class, "Download failed (3): " + fileName);
					handleErrors(e, PeaProperties.getVmBridge().translate("download_failed"), fileName);
					if (downloadErrorMessage == null) {
						downloadErrorMessage =  e.getLocalizedMessage();
					} else {
						downloadErrorMessage = downloadErrorMessage + ", " + e.getLocalizedMessage();
					}
				} catch (Exception e) {
					TestLog.e(WebDavProvider.class, "Download failed (4): " + fileName);
					handleErrors(e, PeaProperties.getVmBridge().translate("download_failed"), fileName);
					if (downloadErrorMessage == null) {
						downloadErrorMessage =  PeaProperties.getVmBridge().translate("download_failed") + ", " + e.getLocalizedMessage();
					} else {
						downloadErrorMessage = downloadErrorMessage +  ", " + e.getLocalizedMessage();
					}
				}
				return is;
			}
		};
		Future<InputStream> future = executor.submit(task);
		InputStream result = null;
		try {
			result = future.get(uploadDownloadTimeout, TimeUnit.SECONDS); 
		} catch (TimeoutException ex) {
			TestLog.e(WebDavProvider.class, "Timeout: " + providerName + " download: "+ fileName);
			// handle the timeout
			if (downloadErrorMessage == null) {
				downloadErrorMessage =  PeaProperties.getVmBridge().translate("download_failed") + " (timeout)";
			} else {
				downloadErrorMessage = downloadErrorMessage + " (timeout)";
			}
		} catch (InterruptedException e) {
			TestLog.e(WebDavProvider.class, "Interrupted: " + providerName + " download: "+ fileName);
			// handle the interrupts
			if (downloadErrorMessage == null) {
				downloadErrorMessage =  PeaProperties.getVmBridge().translate("download_failed") + " (interrupted)";
			} else {
				downloadErrorMessage = downloadErrorMessage + " (interrupted)";
			}
		} catch (ExecutionException e) {
			TestLog.e(WebDavProvider.class, "Execution failed: " + providerName + " download: "+ fileName);
			// handle other exceptions
			if (downloadErrorMessage == null) {
				downloadErrorMessage =  PeaProperties.getVmBridge().translate("download_failed") + " (execution)";
			} else {
				downloadErrorMessage = downloadErrorMessage + " (execution)";
			}
		} finally {
			future.cancel(true); // may or may not desire this
		}
		return result;
	}
	
	public String updateAuthorization(String _userName, char[] _password, String[] otherParams) {

		ExecutorService executor = Executors.newCachedThreadPool();
		Callable<String> task = new Callable<String>() {
			public String call() {
				String result = authorizeNoTimeout(_userName,  _password, otherParams);
				//if(result != null)new UnexpectedValueException("","","").printDescription();
				if (result == null) {
					// successfully executed authorization
					// overwrites existing provider
					CloudControl.addOpenProvider(providerName, WebDavProvider.this);
				}
				return result;
			}
		};
		Future<String> future = executor.submit(task);
		try {
			String result = future.get(authorizeTimeout, TimeUnit.SECONDS); 
			return result;
		} catch (TimeoutException ex) {
			TestLog.e(WebDavProvider.class, "Timeout: " + providerName + " authorization");
			// handle the timeout
			return 
					PeaProperties.getVmBridge().translate("timeout") +"\n"
					+ providerName + " - " + PeaProperties.getVmBridge().translate("authorization_failed");
		} catch (InterruptedException e) {
			TestLog.e(WebDavProvider.class, "Interrupted: " + providerName + " authorization");
			// handle the interrupts
			return providerName + " - " + PeaProperties.getVmBridge().translate("authorization_failed") + " (interrupted)";
		} catch (ExecutionException e) {
			TestLog.e(WebDavProvider.class, "Execution failed: " + providerName + " authorization");
			TestLog.v(this.getClass(), new ExceptionHelper(e).getInfos());
			// handle other exceptions
			return providerName + " - " + PeaProperties.getVmBridge().translate("authorization_failed") + " (execution)";
		} finally {
			future.cancel(true); // may or may not desire this
		}
	}
	
	@Override
	public String authorize(String _userName, char[] _password, String[] otherParams) {
		if (CloudControl.getOpenProvider(providerName) != null) {
			// was already opened and authorized
			return null;
		}
		ExecutorService executor = Executors.newCachedThreadPool();
		Callable<String> task = new Callable<String>() {
			public String call() {
				String result = authorizeNoTimeout(_userName,  _password, otherParams);
				//if(result != null)new UnexpectedValueException("","","").printDescription();
				if (result == null) {
					// successfully executed authorization
					CloudControl.addOpenProvider(providerName, WebDavProvider.this);
				}
				return result;
			}
		};
		Future<String> future = executor.submit(task);
		try {
			String result = future.get(authorizeTimeout, TimeUnit.SECONDS); 
			return result;
		} catch (TimeoutException ex) {
			TestLog.e(WebDavProvider.class, "Timeout: " + providerName + " authorization");
			// handle the timeout
			return 
					PeaProperties.getVmBridge().translate("timeout") +"\n"
					+ providerName + " - " + PeaProperties.getVmBridge().translate("authorization_failed");
		} catch (InterruptedException e) {
			TestLog.e(WebDavProvider.class, "Interrupted: " + providerName + " authorization");
			// handle the interrupts
			return providerName + " - " + PeaProperties.getVmBridge().translate("authorization_failed") + " (interrupted)";
		} catch (ExecutionException e) {
			TestLog.e(WebDavProvider.class, "Execution failed: " + providerName + " authorization");
			TestLog.e(getClass(), new ExceptionHelper(e).getInfos());
			// handle other exceptions
			return providerName + " - " + PeaProperties.getVmBridge().translate("authorization_failed") + " (execution)";
		} finally {
			future.cancel(true); // may or may not desire this
		}
	}
}
