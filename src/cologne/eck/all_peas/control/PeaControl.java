package cologne.eck.all_peas.control;


import java.awt.Window;

/*
 * Peafactory - Production of Password Encryption Archives
 * Copyright (C) 2015  Axel von dem Bruch
 * 
 * This library is free software; you can redistribute it and/or modify it 
 * under the terms of the GNU General Public License as published 
 * by the Free Software Foundation; either version 2 of the License, 
 * or (at your option) any later version.
 * This library is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
 * or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU General Public License for more details.
 * See:  http://www.gnu.org/licenses/gpl-2.0.html
 * You should have received a copy of the GNU General Public License 
 * along with this library.
 */

/**
 * This is the base class for all Password Encrypted Archives (peas).
 * This is the parent class of all PswDialog* classes. 
 */


//import java.awt.Window;
import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Locale;
import java.util.Properties;

import cologne.eck.all_peas.data.AttachmentHandler;
import cologne.eck.all_peas.data.Attachments;
import cologne.eck.all_peas.data.PeaProperties;
import cologne.eck.all_peas.files.FilePanelHolder;
import cologne.eck.all_peas.files.FileTypePanel;
import cologne.eck.all_peas.gui.HtmlFileViewer;
import cologne.eck.all_peas.gui.KeyFileDialogs;
import cologne.eck.all_peas.gui.PeaDialog;
import cologne.eck.all_peas.gui.PeaLockFrame;
import cologne.eck.all_peas.gui.PswDialogView;
import cologne.eck.all_peas.gui.Toaster;
import cologne.eck.all_peas.gui.menu.SettingMenu;
import cologne.eck.all_peas.vm_specific.JREBridge;
import cologne.eck.all_peas.vm_specific.JREProperties;
import cologne.eck.peafactory.crypto.AlgoParamHandler;
import cologne.eck.peafactory.crypto.CipherStuff;
import cologne.eck.peafactory.crypto.HashStuff;
import cologne.eck.peafactory.crypto.RandomStuff;
import cologne.eck.peafactory.crypto.kdf.Argon2KDF;
import cologne.eck.peafactory.crypto.kdf.CatenaKDF;
import cologne.eck.peafactory.crypto.kdf.KeyDerivation;
import cologne.eck.tools.Comparator;
import cologne.eck.tools.Converter;
import cologne.eck.tools.EntropyPool;
import cologne.eck.tools.PropertyHandler;
import cologne.eck.tools.TestLog;
import cologne.eck.tools.UnexpectedValueException;
import cologne.eck.tools.WebTools;
import cologne.eck.tools.Zeroizer;
import settings.*;



public abstract class PeaControl { 

	
	// for Image Lock PEA and Notebook PEA: the chosen file names to decrypt
	private static ArrayList<String> availableFileNames = new ArrayList<String>();	
	
	private static PeaControl dialog;
	
	private static char[] initializedPassword = null;
	
	// the current main window: lockFrame or PswDialogView
	private static boolean plainTextMode = false;//Window mainWindow = null;
	
	private static String derivationError = null;
	
	private static boolean isWindowsOS = false;
	
	//============================================================
	// --- abstract Methods:
	//


	// pre computation while windowOpening
	// this is nowhere used yet, but maybe used later 
	/**
	 * Used for pre-computations while the user types the password
	 */
	public abstract void preComputeInThread();
	//
	// before exit: clear secret values
	/**
	 * Overwriting secret value before closing the program
	 */
	public abstract void clearSecretValues();
	/**
	 * Start the decryption.
	 * This method should be called in EDT.
	 */
	public abstract void startDecryption();	
	
	// used in menu of PswDialogView:
	/**
	 * Display a file chooser to initialize (encrypt)
	 * new files. This method should be called in EDT.
	 * 
	 * @return null for success, an error message to show otherwise
	 * ( not yet implemented in Notebook, Calendar, File)
	 */
	public abstract String initializeNewFiles();		
	
	// used in menu of PswDialogView:
	/**
	 * Display a file chooser to open
	 * previously encrypted files. This method should be called in EDT.
	 */
	public abstract void openEncryptedFiles();	
	
	// currently only used in PeaProperties.getFileType():
	/**
	 * Get the chosen file names to decrypt
	 * 
	 * @return	an array of Strings of the selected file names
	 */
	public abstract String[] getSelectedFileNames();
	
	/**
	 * Get the name of the used PEA as String
	 * in one word e.g. File-Lock-Pea
	 * 
	 * @return the name of the PEA in one word
	 */
	public abstract String getPeaName();
	
	/**
	 * Get the version of the used PEA as String
	 * 
	 * @return the version as String
	 */
	public abstract String getVersion();
	
	/**
	 * Get the year of the publication of the used PEA
	 * 
	 * @return	year of publication as String
	 */
	public abstract String getYearOfPublication();
	
	/**
	 * Get the specific website of the used PEA, that is the 
	 * web address following the language, for example "/html/file_pea.html"
	 * 
	 * @return	the website as String
	 */
	public abstract String getWebsite();
	
	/**
	 * Get the link with the source code of the used PEA
	 * 
	 * @return	the link as String
	 */
	public abstract String getSourceLink();
	
	/**
	 * Get the frame with the decrypted content
	 * 
	 * @return	the frame of the opened PEA
	 */
	public abstract Object getLockFrame();
	
	/**
	 * Set the frame with the decrypted content
	 * 
	 * @param _lockFrame	the frame of the opened PEA
	 */
	public abstract void setLockFrame(PeaLockFrame _lockFrame);	
	
	/**
	 * Get the supported file extensions for this PEA
	 * without leading . in lower case to open encrypted files
	 * (e.g. ics for calendar, rtf for notes,
	 * png, jpg, jepg, bmp, gif for image). If this method returns null, 
	 * all extensions are to be opened
	 * 
	 * @return	the supported file extensions for this PEA without leading . 
	 * 			or null (open all types of files - FileLock PEA)
	 */
	public abstract String[] getSupportedExtensions();
	
	/**
	 * Check if the given file is in the expected format 
	 * for the specific PEA: For Notebook PEA: txt or rtf,
	 * for Image Lock PEA: png, jpg, bmp,gif, 
	 * for File Lock PEA: nothing to check.
	 * 
	 * @param file				the file to check
	 * @param extensionCheck		check only the file name extension,
	 * 							this is also possible for encrypted files
	 * @param middleCheck
	 * @param expensiveCheck
	 * @return
	 */
	public abstract boolean checkFormat(File file, boolean extensionCheck, boolean middleCheck, boolean expensiveCheck);

	// only used in Notebook PEA:
	/**
	 * Get currently used plain text
	 * 
	 * @return	the current plain text
	 */
	protected abstract byte[] getPlainBytes();

	/**
	 * Set all properties for this specific PEA from property file
	 * @param prop	properties for this specific PEA
	 * @param inputStream 	the InputStream to read the properties
	 * 
	 * @return	errorMessage or null
	 */
	public abstract String setPeaPropValues(Properties prop, InputStream inputStream);
	
	/**
	 * Update all properties for this specific PEA from current instance
	 * 
	 * @param prop	properties for this specific PEA
	 * @param outputStream 	the OutputStream to write the properties
	 * 
	 * @return	errorMessage or null
	 */
	public abstract String updatePeaPropValues(Properties prop);//, OutputStream outputStream);
	
	/**
	 * Set the language for the instance
	 * 
	 * @param languageShortName	e.g. "en", "tr"...
	 */
	public abstract void setLanguage(String languageShortName);
	
	/**
	 * Get languages that can be installed for this PEA
	 * as 2-letter strings. This does not include the six per default
	 * installed languages de, en, es, fr, ru, tr
	 * 
	 * @return Array of languages as 2-letter strings
	 */
	public abstract String[] getSupportedLanguages();
	
	
	/**
	 * Get the name of the property file for the specific PEA
	 * without directory (e.g. "file.properties")
	 * 
	 * @return	the name of the property file for the specific PEA
	 * 			without the directory
	 */
	public abstract String getPeaPropFileName();
	
	//============================================================
	
	/**
	 * Handles first commands after start: read and set properties, 
	 * direct start mode, command line mode, test mode.
	 * This is not used in File Lock PEA.
	 * 
	 * @param args	arguments from main
	 * 
	 * @return	true if command line mode, false: GUI
	 */
	public final static boolean handleMainCommands(String[] args) {
		String error = new PropertyHandler().readAndSetAllPropValues();
		if (error != null) {
			TestLog.e(PeaControl.class, "Error from properties: " + error  );
		}

		if (PeaProperties.isDirectStart() == true) {
			args = CommandLineHandler.getDirectStartArgs(args);
		}
		
		if(args != null && args.length > 0) {
			//PeaProperties.setWorkingMode(args[0]);
			boolean runCommands = CommandLineHandler.handleArgs(args);
			TestLog.v(PeaControl.class, "Run in command line mode: " + runCommands);
			if (runCommands == false) {
				PeaProperties.setCommandLineMode(false);
			}
		}	
		return PeaProperties.isCommandLineMode();
	}
	
	/**
	 * First commands in EventDispatchThread: 
	 * Handle language and first_time_guide
	 */
	public final static void startEDT() {
		boolean firstTime = false;
		if (new File("resources" + File.separator + "pea.properties").exists() == false) { // first time	
			firstTime = true;
		}
		// ask to install user default language if available
		if (PeaProperties.isInstallLanguageOptionShown() == false) { // ask only once	
			Locale defLocale = Locale.getDefault();
			String defLang = defLocale.getLanguage();
			// check if user language is already installed:
			boolean langInstalled = false;				
			String[] avLangList = PeaProperties.getVmBridge().getAvailableLanguages();				
			for (String al : avLangList) { // some languages changed...
				if (defLang.equals(al) || defLang.equals(new Locale(al).getLanguage())){
					langInstalled = true;
					break;
				}				
			}
			if (langInstalled == false) {
				// check is user language is supported from web:
				boolean langSupported = false;
				String[] supportedLanguages = PeaControl.getDialog().getSupportedLanguages();
				for (String sLang : supportedLanguages) {
					if (defLang.equals(sLang) || defLang.equals(new Locale(sLang).getLanguage())) {
						langSupported = true;
						break;
					}
				}
				if (langSupported == true) {

					if (PeaProperties.getLastLanguage().equals("en") // is set to en
							&& ( ! defLocale.getLanguage().equals("en")) ) { // user lang is not en
						if (WebTools.checkConnection() == true) {
							String[] options = { defLocale.getDisplayLanguage(defLocale), "English" };
							int installLang = PeaDialog.showOptions(JREProperties.getMainWindow(), "",//PeaProperties.getVmBridge().translate("language"), 
									PeaProperties.getVmBridge().translate("language"), 
									-1, 3, options, options[0]);

							if (installLang == 0) { // install user default language
								boolean installed = SettingMenu.downloadLanguageFiles(Locale.getDefault().getLanguage());    
								//Translator.showLanguageInstallOption(Locale.getDefault());
								if (installed == true) {
									PeaProperties.setInstallLanguageOptionShown(true);
									Toaster.toast(JREProperties.getMainWindow(), "Do you prefer English instead?"
									+ "\n\n Change in menu:\n\n"
									+ "   " + PeaProperties.getVmBridge().translate("settings") + "\n"
									+ "      " + PeaProperties.getVmBridge().translate("language") + "\n"
									+ "         en", 5);
									// new language resets the dialog view
								/*	if (JREProperties.getMainWindow() != null && JREProperties.getMainWindow() instanceof PswDialogView) {
										dialogView = (PswDialogView) JREProperties.getMainWindow();
									}*/
								}
							} else if (installLang == 1) {
								PeaProperties.setInstallLanguageOptionShown(true);
							}
						}
					}
				}
			}
		}
		if (firstTime == true ) {//&& ! PeaProperties.getLastLanguage().equals("en")) {
			HtmlFileViewer hfv = new HtmlFileViewer("first_time_guide", JREProperties.getMainWindow());
			hfv.requestFocus();
		}

	}

	
	/**
	 * Start the execution of the PEA: stop the collection of entropy, 
	 * add file to path file, start decryption process
	 * 
	 * @param rememberPath		remember the selected file names or not
	 */
	public final void startExecution(boolean rememberPath) {
		
		// print some informations to stdout:
		System.out.println(dialog.getPeaName() + " " + dialog.getVersion());
		System.out.println(//System.getProperty("os.name") + "\n" +
				"Using Java:" + System.getProperty("java.version") );//+ "\n"
				//+ "PEA type: " + PeaProperties.getFileType() + ", version: " + PeaControl.getDialog().getVersion() );//VERSION);
		System.err.println (KeyDerivation.getKdf().getName() + ", " 
				+ CipherStuff.getCipherAlgo().getAlgorithmName() 
				+ ", " + HashStuff.getHashAlgo().getAlgorithmName());
		
		PswDialogView.setStarted(true);

		EntropyPool.getInstance().stopCollection();

		if (PswDialogView.isInitializing() == false) {
			
			if (PeaProperties.isCommandLineMode() == false) {

				FileTypePanel ftp = (FileTypePanel)JREProperties.getTypePanel();
				// set Model:
				if (ftp == null || (! PeaProperties.getFileType().equals("file") && ftp.getCloudFilePanel() == null) ) {
					// check if first start
					if ( new File(System.getProperty("user.dir") + File.separator + "resources" + File.separator + "pea.propertries").exists()) {
						TestLog.e(PeaControl.class, "Unexpected error when accessing cloud files");
						TestLog.ve(PeaControl.class, "Missing CloudFilePanel", 5);
						PswDialogView.setMessage(PeaProperties.getVmBridge().translate("unexpected_error"), true, false);
						PswDialogView.enableOkButton();
						return;
					} else {
						TestLog.v(PeaControl.class, "First start...");
					}
				}
				if (ftp != null &&  ftp.getFileComposer().getFileModel().checkForValidFile() == false){
					// check for cloud file
					if ( ( ! PeaProperties.getFileType().equals("file")) // there is no cloud file panel in File Lock PEA
							&& ftp.getCloudFilePanel().getComponentCount() > 0) {
						// get and decrypt the cloud files in XControl	
						//TestLog.o(PeaControl.class, "cloud file found");			
					} else {
						PswDialogView.setMessage(PeaProperties.getVmBridge().translate("no_valid_file_selected"), true, false);
						PswDialogView.enableOkButton();
						return;
					}
				}
				// remember this file?
				if (rememberPath == true ) {
					String[] newNames = PeaControl.getDialog().getSelectedFileNames();
					if (newNames != null) {
						PathFileManager.addFileNamesToPathFile( newNames );
					} else {
						TestLog.e(PeaControl.class, "PeaControl.startExecution: files to remember are null");
					}
				}
				//TestLog.v(PeaControl.class, "PeaControl.startExecution",5);
				PeaControl.getDialog().startDecryption();
			} else {
				PeaControl.getDialog().startDecryption();
			}
		} else { //  initializing
			if (PeaSettings.getKeyFileProperty() == false) {
				Attachments.setFileIdentifier(AttachmentHandler.getVersion2ID());
				PeaProperties.setAlgoParamString(
						PeaProperties.getDefaultAlgoParamString().getBytes(AttachmentHandler.getASCIICharset()));
			} else {
				Attachments.setFileIdentifier(PeaSettings.getFileIdentifier());
				PeaProperties.setAlgoParamString( // TODO algo param string from PeaSettings
						PeaProperties.getDefaultAlgoParamString().getBytes(AttachmentHandler.getASCIICharset()));
			}
			PeaControl.getDialog().startDecryption();
		} 
		PswDialogView.setStarted(false);
	}
	
	/**
	 * Derive the key from the password. Performs an initial
	 * hash and derives the key using the selected key derivation function.
	 * The salt must be set before
	 * 
	 * @param pswInputChars		the password
	 * 
	 * @return					the derived key in required size. If this method returns null, 
	 * 							the value derivationError is set
	 */
	public final static byte[] deriveKeyFromPsw(char[] pswInputChars) { 
		
		derivationError = null;

		if (pswInputChars == null) {
			if (PeaProperties.isKeyFileOnlyMode() == false) {
				pswInputChars = "no password".toCharArray();
			}
		} else if (pswInputChars.length == 0) {
			pswInputChars = "no password".toCharArray();
		}
		// TODO direct start
		byte[] pswInputBytes = null;
		if (PeaProperties.isKeyFileOnlyMode() == false) {
			pswInputBytes = Converter.chars2bytes(pswInputChars); 
		} else {
			// set content of keyfile as password
			// TODO Test
			pswInputBytes = KeyFileDialogs.getDefaultKeyFileHash();
		}
			if (pswInputBytes == null) {
				TestLog.ve(PeaControl.class, "Key derivation error");
				TestLog.ve(PeaControl.class, "Error converting char[] to byte[]", 5);
				derivationError = PeaProperties.getVmBridge().translate("unexpected_error") + " (conversion)";
				return null;
			}

			if (pswInputBytes.length > 0)  {
				Zeroizer.zero(pswInputChars);
			}
			// prevent using a zero password:
			pswInputChars = null;
		

		// set algorithms and parameters for hash, KDF and key size
		if (plainTextMode == false) {//getMainWindow() instanceof PswDialogView) {
			
			if ( PswDialogView.isInitializing() == false) {
				if (PeaProperties.isCommandLineMode() == false) {
					if (JREProperties.getMainWindow() instanceof PswDialogView) {
						// use scheme from FilePanelHolder
						if (FilePanelHolder.getSelectedModel() != null) {
							String schemeToUse = new String(FilePanelHolder.getSelectedModel().getSchemeMarker(), 
									AttachmentHandler.getASCIICharset());
							TestLog.v(PeaControl.class, "scheme to use: " + schemeToUse);
							AlgoParamHandler.readString(schemeToUse, false);
						} else {
							TestLog.e(PeaControl.class, "No files in FilePanelHolder?");
							TestLog.ve(PeaControl.class, "No scheme found in FilePanelHolder", 5);
						}
					} else if (JREProperties.getMainWindow() instanceof PeaLockFrame) {
						String schemeToUse = new String( PeaProperties.getAlgoParamString(), AttachmentHandler.getASCIICharset());
						TestLog.v(PeaControl.class, "scheme to use: " + schemeToUse);
						AlgoParamHandler.readString(schemeToUse, false);						
					}
				} else {
					// scheme was set in CommandLineHandler from first file
			/*		String defaultScheme= PeaProperties.getDefaultAlgoParamString();
					TestLog.v(PeaControl.class, "default scheme: " + defaultScheme);
					AlgoParamHandler.readString(defaultScheme, false); */
				}
			} else {
				// use default algos and parameters
				String defaultScheme= PeaProperties.getDefaultAlgoParamString();
				TestLog.v(PeaControl.class, "default scheme: " + defaultScheme);
				AlgoParamHandler.readString(defaultScheme, false);
			}
		}
		byte[] salt = KeyDerivation.getSalt();
		if (salt == null) {
			if ( PswDialogView.isInitializing() == false) {
				TestLog.ve(PeaControl.class, "No salt found although not initializing", 5);
			}
			TestLog.v(PeaControl.class, "Create new salt...");
			salt =  new RandomStuff().createRandomBytes(KeyDerivation.getSaltSize());
			KeyDerivation.setSalt(salt);
		}
		String keyFileInfo = "";
		int keyFileFlag = PeaProperties.getKeyFileFlag();
		if (keyFileFlag == 1) { // default
			keyFileInfo = PeaProperties.getVmBridge().translate("with") 
					+ " " + PeaProperties.getVmBridge().translate("default")
					+ " " + PeaProperties.getVmBridge().translate("key_file");
		} else if (keyFileFlag == 2) { // chosen
			keyFileInfo = PeaProperties.getVmBridge().translate("with") 
					//+ " " + PeaProperties.getVmBridge().translate("default")
					+ " " + PeaProperties.getVmBridge().translate("key_file");
		}
		TestLog.o(PeaControl.class, "Derive key: "
					+ KeyDerivation.getKdf().getInfoString() + " \n " 		
					+ HashStuff.getHashAlgo().getAlgorithmName() + " - " 
					+ CipherStuff.getCipherAlgo().getAlgorithmName() + "\n"
					+ keyFileInfo);
		TestLog.v(PeaControl.class, "Salt: " + Converter.bytes2hex(KeyDerivation.getSalt()));

		//=============
		//  derive key: 
		//	
		// 1. initial hash, reasons: 
		// - Bcrypt limited password length, 
		// - password remains n RAM in Bcrypt and Scrypt
		// - key file property
		if (PeaProperties.isTestMode()){
			Comparator.checkNullVector(pswInputBytes);
		}
		byte[] pswHash = HashStuff.generateHKDFBytes(pswInputBytes, 
				Attachments.getProgramRandomBytes(), // if no key file property: this is a fixed string
				64); // size of pswHash: 64 bytes
		Zeroizer.zero(pswInputBytes);
		if (PeaProperties.isTestMode()){
			Comparator.checkNullVector(pswHash);
		}
		// 2. derive key from selected KDF: 
		if (KeyDerivation.getKdf() == null) {
			new UnexpectedValueException("KeyDerivation", "getKdf()", "is null").printDescription();
			derivationError = PeaProperties.getVmBridge().translate("unexpected_error") + " (missing KDF)";
			return null;
		}
		if (pswHash == null) {
			new UnexpectedValueException("byte[]", "pswHash", "is null").printDescription();
			derivationError = PeaProperties.getVmBridge().translate("unexpected_error") + " (password hash)";
			return null;
		}
		byte[] keyMaterial = null;
		try {
			keyMaterial = KeyDerivation.deriveKey(pswHash);	
		}  catch (OutOfMemoryError e) {
			if (PswDialogView.getView().isShowing()) {
			TestLog.e(PeaControl.class,   
					"Your system doesn't offer enough memory..." +
					"\n" +	e.getLocalizedMessage() +
					"\nThe key derivation requires more memory than your system provides."
					+ "\n  You can try to run java with more memory by typing: "
					+ "\n    java -Xmx1024M -jar YOUR_PEA.jar", 5);
			derivationError = "Your system doesn't offer enough memory..." +
					"\n" +	e.getLocalizedMessage() +
					"\nThe key derivation requires more memory than your system provides."
					+ "\n  You can try to run java with more memory by typing: "
					+ "\n    java -Xmx1024M -jar YOUR_PEA.jar";
			PeaDialog.showMessage(JREProperties.getMainWindow(), e.getLocalizedMessage()+"\n"
					+ "Your system doesn't offer enough memory...", 
					PeaProperties.getVmBridge().translate("error"), 0);
			return null;
			} else {
				TestLog.e(PeaControl.class, 
						"Your system doesn't offer enough memory..." +
						"\n" +	e.getLocalizedMessage() +
						"\nThe key derivation requires more memory than your system provides."
						+ "\n  Set the memory parameter of the key derivation function to a lower value:"
						+ "\n    Menu -> Setting -> crypto -> key derivation -> parameter setting", 5);
				derivationError = "Your system doesn't offer enough memory..." +
						"\n" +	e.getLocalizedMessage() +
						"\nThe key derivation requires more memory than your system provides."
						+ "\n  Set the memory parameter of the key derivation function to a lower value:"
						+ "\n    Menu -> Setting -> crypto -> key derivation -> parameter setting";
				PeaDialog.showMessage(JREProperties.getMainWindow(), e.getLocalizedMessage()+"\n"
						+ "Your system doesn't offer enough memory...", 
						PeaProperties.getVmBridge().translate("error"), 0);
				return null;
			}
		} catch (Exception e) {
			TestLog.e(PeaControl.class, e.getMessage(), 5);
			PeaDialog.showMessage(JREProperties.getMainWindow(), e.getLocalizedMessage()+"\n"
					+ "(" + e.toString() + ")", 
					PeaProperties.getVmBridge().translate("error"), 0);
			return null;
		}
		if (keyMaterial == null) {
			derivationError = PeaProperties.getVmBridge().translate("unexpected_error") + " (KDF or key file)";
		}
		Zeroizer.zero(pswHash);		
		return keyMaterial;
	}

	/**
	 * Get password, set algorithms and parameters, 
	 * and derive the key. 
	 * The salt must be set before. 
	 * 
	 * @return	the key as an array of bytes
	 */
	protected final byte[] getKeyMaterial() {

		//-------------------------------------------------------------------------------
		// Check if t is alive, if so: wait...
		if (PswDialogView.getView().getThread() != null) {
			if (PswDialogView.getView().getThread().isAlive()) { // this should never happen... 

				TestLog.e(PeaControl.class, "PeaControl: WhileTypingThread still alive!");
				try {
					PswDialogView.getView().getThread().join();
				} catch (InterruptedException e) {
					if (PeaProperties.isTestMode()){
						TestLog.e(PeaControl.class, "PeaControl: Joining thread interrupted");
						e.printStackTrace();
					}
				}
			}		
		}
	
		//---------------------------------------------------------------------
		// get password 
		char[] pswInputChars = null;
		if (PswDialogView.isInitializing() == false){
			if (PeaProperties.isCommandLineMode() == false){
				pswInputChars = PswDialogView.getPassword();	
			} else {
				pswInputChars = CommandLineHandler.getPassword();
			}
		} else {// initializing
			
			if (PeaProperties.getFileType().equals("text file") || PeaProperties.getFileType().equals("image")){
				pswInputChars = Arrays.copyOf(initializedPassword, initializedPassword.length);
			} else {
				pswInputChars = initializedPassword;
			}			
		}

		// set algorithms and parameters:
		if (PswDialogView.isInitializing() == true) {
			// get and set default algorithms and parameters
			AlgoParamHandler.readString(PeaProperties.getDefaultAlgoParamString(), false);
		} else {
			if (PeaProperties.isCommandLineMode() == false) {
				// get and set currently selected algorithms and parameters
				if (FilePanelHolder.getSelectedModel() == null ||
						FilePanelHolder.getSelectedModel().getSchemeMarker() == null) {
					TestLog.ve(PeaControl.class, "No algorithm-parameter scheme found", 5);
					return null;
				}
				AlgoParamHandler.readString(
						new String(FilePanelHolder.getSelectedModel().getSchemeMarker(), AttachmentHandler.getASCIICharset()), 
						false);
				if (FilePanelHolder.getSelectedModel().getSalt() == null) {
					TestLog.ve(PeaControl.class, "No salt found", 5);
					return null;
				}
				KeyDerivation.setSalt(FilePanelHolder.getSelectedModel().getSalt());
			} else {
				// scheme was set in CommandLineHandler from first file
				//AlgoParamHandler.readString(PeaProperties.getDefaultAlgoParamString(), false);
			}
		}
		
		byte[] keyMaterial = deriveKeyFromPsw(pswInputChars);	
		
		if (PeaProperties.getFileType().equals("image") || PeaProperties.getFileType().equals("text file")){
			// do not zeroize
			// the password is needed to decrypt the image or 
			// to decrypt the file (text file)
		} else {
			Zeroizer.zero(initializedPassword);
			initializedPassword = null;
		}
	
		if (keyMaterial == null) { // probably key file dialog cancelled
			TestLog.e(PeaControl.class, "Key derivation failed" );
			PswDialogView.getView().displayErrorMessages("Key derivation failed.", true);
			if (PeaControl.getDerivationError() != null) {
				PeaDialog.showMessage(JREProperties.getMainWindow(), 							
						PeaControl.getDerivationError(), 
						PeaProperties.getVmBridge().translate("error"), 
						0);//JOptionPane.ERROR_MESSAGE 
			}
			PswDialogView.enableOkButton();
		}		
		return keyMaterial;
	}

	/**
	 * Initialize required values for execution: hash, cipher, 
	 * KDF, KDF parameters, programRandomBytes, salt, file identifier
	 * from PeaSettings 
	 */
	public final static void initializeVariables() {	
		
		String os = System.getProperty("os.name");
		if (os.startsWith("Windows")) {
			setWindowsOS(true);
		}
		PeaProperties.setVmBridge(new JREBridge());
		HashStuff.setHashAlgo(PeaSettings.getHashAlgo() );
		PeaProperties.setDefaultDigest(PeaSettings.getHashAlgo());
		CipherStuff.setCipherAlgo(PeaSettings.getCipherAlgo() );
		PeaProperties.setDefaultCipher(PeaSettings.getCipherAlgo() );
		KeyDerivation.setKdf( PeaSettings.getKdfScheme() );
		PeaProperties.setDefaultKDF( PeaSettings.getKdfScheme());
		// set default KDF parameters:
		String versionString = PeaSettings.getVersionString();
		KeyDerivation.setVersionString(versionString);
		if (versionString.startsWith("Dragonfly") || versionString.startsWith("Butterfly")) {
			CatenaKDF.setVersionID(versionString);
			PeaProperties.setDefaultKDFInstanceAppendix(versionString);
		} else if (versionString.startsWith("Argon")) {
			if (versionString.equals("Argon2d")) {
				Argon2KDF.setType(0);
				PeaProperties.setDefaultKDFInstanceAppendix("d");
			} else if (versionString.equals("Argon2i")) {
				Argon2KDF.setType(1);
				PeaProperties.setDefaultKDFInstanceAppendix("i");
			} else if (versionString.equals("Argon2id")) {
				Argon2KDF.setType(2);
				PeaProperties.setDefaultKDFInstanceAppendix("id");
			} 
		}
		PeaProperties.setDefaultMCost(PeaSettings.getMemory());
		PeaProperties.setDefaultTCost(PeaSettings.getIterations());
		PeaProperties.setDefaultParallel(PeaSettings.getParallelization());
		// Calculate algoParamString and set as default:
		String defaultAlgoParamString = AlgoParamHandler.writeString(true);
		PeaProperties.setDefaultAlgoParamString(defaultAlgoParamString);

		Attachments.setProgramRandomBytes(PeaSettings.getProgramRandomBytes() );	
		if (PeaSettings.getKeyFileProperty() == false) {
			Attachments.setFileIdentifier(AttachmentHandler.getVersion2ID());
		} else {
			Attachments.setFileIdentifier(PeaSettings.getFileIdentifier() );
		}		
		if (PeaProperties.isTestMode() == true) {
			try { 
				TestLog.o(PeaControl.class, PeaProperties.getVersionInfo());
				TestLog.o(PeaControl.class, "Operating system: " +  System.getProperty("os.name"));
				TestLog.o(PeaControl.class, "Java version: " +  System.getProperty("java.version"));
				TestLog.o(PeaControl.class, "stderr and stdout are redirected to the file " + TestLog.getLogFileName());
				TestLog.o(PeaControl.class, KeyDerivation.getKdf().getName() + ", " 
						+ CipherStuff.getCipherAlgo().getAlgorithmName() 
						+ ", " + HashStuff.getHashAlgo().getAlgorithmName());
			} catch (Exception e) {
				TestLog.e(PeaControl.class, "Print informations failed: " + e.toString() + ",  " + e.getMessage());
			}
		}
		FileTypePanel ftp = null;
		if (PswDialogView.isInitializing() == false) { // get selected panel
			ftp = FilePanelHolder.getSelectedPanel();
			if (ftp == null) { // initialization: Need FTP and CloudFilePanel for later access
				if (PswDialogView.isInitializing() == false) {// maybe first initialization: there are no files
					//TestLog.v(PeaControl.class, "Set FileTypePanel null");
				}
			}
		} else {
			// Create later from default properties and salt
			// salt is not yet set
		}

		JREProperties.setTypePanel(ftp);
	}
	
	/**
	 * Ask the user for automatically checking updates
	 * and store the answer in properties
	 */
	public void checkUpdates() {
		if (new File("resources" + File.separator + "pea.properties").exists() == true) { // not first time
			if ( PswDialogView.isInitializing() == false ){
				int checkUpdates = PeaProperties.getCheckUpdates();
				if (checkUpdates == 0) {
					// check updates
					int checkUpdateResult = PeaDialog.showQuestion(JREProperties.getMainWindow(), 
							PeaProperties.getVmBridge().translate("automatically_check_updates"), 
							PeaProperties.getVmBridge().translate("check_update"), 0);
					if (checkUpdateResult == 0) { // yes
						PeaProperties.setCheckUpdates(1);
						new PropertyHandler().setOneProperty("check_updates", "" + 1);
					} else if (checkUpdateResult == 1) { // no
						PeaProperties.setCheckUpdates(-1);
						new PropertyHandler().setOneProperty("check_updates", "" + -1);
					} else { // cancel
						PeaProperties.setCheckUpdates(0);
						new PropertyHandler().setOneProperty("check_updates", "" + 0);
					}
				} else if (checkUpdates == 1) {
					UpdateChecker.checkUpdates(JREProperties.getMainWindow(), false);
				} else {
					TestLog.v(PeaControl.class, "No check for updates: " + checkUpdates);
				}
			}
		}
	}
	
	/**
	 * Check a password for a cipher text: 1. compares salt from file with current salt, 
	 * 2. check if the pswIdentifier is the encrypted String "_PEA_FACTORY_ID_". 
	 * The nonce is taken from the cipher text, the cipher text must in the original state,
	 * (not modified: (fileIdentifier, salt and nonce not cut). 
	 * 
	 * @param cipherText	the original cipher text to check
	 * @param key			the key to use for decryption
	 * 
	 * @return				null if the password does not fail, 
	 * 						an error message otherwise
	 */
	public final static String checkPassword(byte[] cipherText, byte[] key) {

		if (cipherText == null) {
			if (PeaProperties.isTestMode()) {
				new UnexpectedValueException("cipherText", "byte[]", "is null").printDescription();
			} 		
			return "Missing ciphertext to check passord...";
		}
		if (key == null) {
			if (PeaProperties.isTestMode()) {
				new UnexpectedValueException("key", "byte[]", "is null").printDescription();
			}
		} else {
			// check for program bug
			if (PeaProperties.isTestMode()){
				Comparator.checkNullVector(key);
			}
		}
		// check the size of the cipher text:
		// TODO algoParam
		if (cipherText == null || cipherText.length < (Attachments.getFileIdentifierSize() 
				+ KeyDerivation.getSaltSize()
					+ Attachments.getNonceSize() 
					+ Attachments.getPswIdentifierSize() ) ){
			return PeaProperties.getVmBridge().translate("unsuitable_ciphertext_len");
		}
		boolean check = AttachmentHandler.validateSaltAlgosFileID(cipherText);
		if (check == false) {
			return PeaProperties.getVmBridge().translate("unexpected_error");
		}
		
		byte[] fileIdentifier = Attachments.getEndOfBytes(cipherText, Attachments.getFileIdentifierSize());
		int version = -1;
		if (Arrays.equals(fileIdentifier, AttachmentHandler.getVersion1ID())) {
			version = 1;
		} else if (Arrays.equals(fileIdentifier, AttachmentHandler.getVersion2ID())) {
			version = 2;
		} else if (PeaSettings.getKeyFileProperty() == true) {
			version = 0;
		} else {
			return PeaProperties.getVmBridge().translate("unexpected_error");
		}
		
		// get pswIdentifier from cipher text:
		byte[] pswIdentifier = new byte[Attachments.getPswIdentifierSize()];
		int pswIdIndex = cipherText.length 
				- Attachments.getFileIdentifierSize() 
				- KeyDerivation.getSaltSize()
				- Attachments.getNonceSize() 
				- Attachments.getPswIdentifierSize();
		if (version == 2 || version == 0) {
			pswIdIndex -= AlgoParamHandler.getAlgoParamStringSize();
		}
		System.arraycopy(cipherText, pswIdIndex, pswIdentifier, 0, Attachments.getPswIdentifierSize());
		byte[] nonce = new byte[ Attachments.getNonceSize()];
		int nonceIndex = cipherText.length 
				- Attachments.getFileIdentifierSize() 					
				- KeyDerivation.getSaltSize()
				- Attachments.getNonceSize();
		if (version == 2 || version == 0) {
			nonceIndex -= AlgoParamHandler.getAlgoParamStringSize();
		}
		System.arraycopy(cipherText, nonceIndex, nonce, 0, Attachments.getNonceSize());
		
		boolean result = Attachments.checkPswIdentifier(pswIdentifier, key, nonce);
		
		if (result == true) {
			return null;
		} else {
			return PeaProperties.getVmBridge().translate("password_failed");
		}
	}
	/**
	 * Sets the focus to an open window. 
	 * If any dialog is open, this will request the focus, 
	 * otherwise the MainWindow will request the focus. 
	 * 
	 * @return	true if the MainWindow request the focus, false if any other or no window request the focus
	 */
	public final static boolean resetFocus() {
		Window[] wins = Window.getWindows();
		for (Window win : wins) {
			if (win.isVisible() && win.isFocusable() && ( ! win.equals(JREProperties.getMainWindow()))){
				win.requestFocus();
				return false;
			}
		}
		Window w = JREProperties.getMainWindow();
		if (w != null && w.isVisible() == true) { //displayable, focusable, visible
			w.requestFocus();
			return true;
		}
		TestLog.ve(PeaControl.class, "Could not find a window to request focus", 5);
		return false;
	}
	
	//==========================================
	// Getter & Setter

	public final static void setDialog(PeaControl _dialog) {
		dialog = _dialog;
	}
	public final static PeaControl getDialog() {
		return dialog;
	}
	
	/**
	 * Set the password from initialization process
	 * to use in key derivation
	 * 
	 * @param psw	the password to set
	 */
	public static void setInitializedPassword(char[] psw){
		initializedPassword = psw;
	}
	
	/**
	 * @return the encryptedFileTypePanel
	 */
	public FileTypePanel getEncryptedFileTypePanel() {
		return (FileTypePanel)JREProperties.getTypePanel();
	}

	/**
	 * Return the file name which can be decrypted with his PEA
	 * 
	 * @return the availableFileNames
	 */
	public static ArrayList<String> getAvailableFileNames() {
		return availableFileNames;
	}
	
	/**
	 * Set the available file names
	 * 
	 * @param newAvailableFileNames the file names to set
	 */
	public static void setAvailableFileNames(ArrayList<String> newAvailableFileNames) {
		availableFileNames = newAvailableFileNames;
	}
	
	/**
	 * Remove a file name from the list of available file names
	 * 
	 * @param availableFileNames the availableFileNames to remove
	 * 
	 * @return	true, if the file was present and was removed
	 * 			false, if the file was not present
	 */
	public static boolean removeAvailableFileName(String availableFileName) {
		
		if (PeaProperties.getFileType().equals("file")){
			return false;
		}

		if (PeaControl.availableFileNames.contains(availableFileName)){
			return PeaControl.availableFileNames.remove(availableFileName);
		} else {
			return false;
		}
	}
	
	/**
	 * @param availableFileNames the availableFileNames to set
	 */
	public static void addAvailableFileName(String availableFileName) {
		
		if (PeaProperties.getFileType().equals("file")){
			return;
		}

		if (PeaControl.availableFileNames.contains(availableFileName)){
//			TestLog.o(PeaControl.class, "file already available: " + availableFileName);
		} else {
			PeaControl.availableFileNames.add(availableFileName);
		}
	}

	
	public static String getDerivationError() {
		return derivationError;
	}

	/**
	 * Check if key was derived and 
	 * decryption was performed
	 * and files are open
	 * 
	 * @return	true if content was decrypted
	 */
	public static boolean isPlainTextMode() {
		return plainTextMode;
	}
	
	/**
	 * Set if key was derived and 
	 * decryption was performed
	 * and files are open. 
	 * This also checks if the is a probably new default key file
	 * available to set in properties
	 * 
	 * @param filesOpen	true if content was decrypted
	 */
	public static void setPlainTextMode(boolean _contentOpen) {
		if (_contentOpen == true) {
			// Set probably new default key file
		/*	String probablyNewDefaultKeyFileName = KeyFileDialogs.getProbablyNewDefaultKeyFileName();
			if (probablyNewDefaultKeyFileName != null) {
				PeaProperties.setDefaultKeyFileName(probablyNewDefaultKeyFileName);
			}*/
			KeyFileDialogs.setNewChosenDefaultKeyFile(false);
		}		
		PeaControl.plainTextMode = _contentOpen;
	}
	
	/**
	 * Check if current system is Windows
	 * 
	 * @return	true if current system is Windows
	 */
	public static boolean isWindowsOS() {
		return isWindowsOS;
	}
	
	/**
	 * Set if current system is Windows
	 * 
	 * @param isWindowsOS	true if current system is Windows
	 */
	private static void setWindowsOS(boolean isWindowsOS) {
		PeaControl.isWindowsOS = isWindowsOS;
	}
}
