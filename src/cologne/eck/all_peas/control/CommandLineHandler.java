package cologne.eck.all_peas.control;


import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;

import javax.swing.JOptionPane;
import javax.swing.JPasswordField;
import javax.swing.SwingUtilities;

import cologne.eck.all_peas.data.AttachmentHandler;
import cologne.eck.all_peas.data.Attachments;
import cologne.eck.all_peas.data.PeaProperties;
import cologne.eck.all_peas.gui.PeaDialog;
import cologne.eck.all_peas.gui.PeaFileChooser;
import cologne.eck.all_peas.vm_specific.JREProperties;
import cologne.eck.peafactory.crypto.AlgoParamHandler;
import cologne.eck.peafactory.crypto.kdf.KeyDerivation;
import cologne.eck.tools.Converter;
import cologne.eck.tools.TestLog;
import settings.PeaSettings;


// --files /home/ax/x3,/home/ax/x1,/home/ax/x2 --passphrase "x" -keyfile "/home/ax/15DB388A"


// TODOs
// no std:out (silent mode)
// encrypt file with default algorithms
// read path file instead of --file

// Problem: EntropyPool not starting...

public class CommandLineHandler {
	
	private static String[] files = null;
	
	private static char[] psw;
	
	private static String commandLineKeyFileName;

	private static final String errorAppendix = "Type \"java -jar YOUR_PEA.jar --help\" to get more information";
	
	// Format is "error: file name"
	private static StringBuilder failedFilesBuilder = new StringBuilder();
	
	private static final String helpText = 
			 "java -jar MY_PEA.jar [-t] [--file file]...\n"
			+ "java -jar MY_PEA.jar [-t] [--file file... [--passphrase passphrase] [--keyfile keyfile]]\n\n"			
			+ "-t			run in test mode, print infos while running\n"
			+ "--files		files with absolute path as comma separated list, \n"
			+ "				use quotes for files that contain spaces.\n"
			+ "             If files are encrypted with different algorithms, parameters or key files, \n"
			+ "             the first file is opened and an error is returned for other files. \n"
			+ "--passphrase	--password	the passphrase to use for decryption, \n"
			+ "			use quotes if the passphrase contains spaces, use \"\" for null password.\n" 	
			+ "			Warning: Use with caution,the passphrase is present without any protection.\n" 
			+ "			If no password is specified, the user will be prompted to type it.\n\n"
			+ "			This is the safer option.\n" 		
			+ "--keyfile	the keyfile to use with absolute path, "
			+ "				\"\" may be used for no keyfile,\n"
			+ "				use quotes for a file that contains spaces.\n"		
			+ "			If a keyfile is required but not specified, it can be selected manually.\n\n"
			+ "	-h --help	display help and exit\n"
			+ "	--version	output version information and exit\n\n"
			+ "Example: \n"
			+ "	java -jar MY_PEA.jar --file /home/file1,/home/file2\n"
			+ "Asks for a passphrase and decrypts and opens file1 and file2";
			
					
	// option passphrase requires file option: 
	// java -jar MY_PEA.jar [-t] [--file files .. [--passphrase passphrase]]
	
	// get passphrase from stdin:
	// java -jar MY_PEA.jar [-t] [--files files]
	
	// java -jar MY_PEA.jar -h --help	display help and exit
	// java -jar MY_PEA.jar -version	output version information and exit
	
	// -r	run in rescue mode (not used yet)
	// -t	run in test mode, print infos while running
	// --files files the files to decrypt and open as commad separated list
	// --passphrase passphrase	enter a passphrase for key derivation


	
	/**
	 * Read and validate the arguments from command line. 
	 * Skip password dialog if the argument contain files. 
	 * 
	 * @param args	arguments from command line
	 * 
	 * @return	false: start password dialog, true: open lock frame directly
	 */
	public static boolean handleArgs(String[] args){

		if (args == null) {
			return false;
		}
		int len = args.length;
        for (int i = 0; i < len; i++) {
            TestLog.v(CommandLineHandler.class, "Argument: " + args[i]);
        }
        
		String firstArg = args[0];
		//TestLog.o(XXXX.class, "firstArg: " + firstArg);
		if ( ! firstArg.startsWith("-")) {
			
			TestLog.e(CommandLineHandler.class, "Invalid parameter " + firstArg + " - must start with a -\n" + errorAppendix);
			System.exit(1);
		}
		if (firstArg.equals("-h") || firstArg.equals("--help")) {
			TestLog.o(CommandLineHandler.class, helpText);
			System.exit(0);
		}
		if (firstArg.equals("--version")) {
			TestLog.o(CommandLineHandler.class, PeaProperties.getVersionInfo());
			System.exit(0);
		}
		
		if (firstArg.equals("-t")) {
			//TestLog.o(XXXX.class, "firstArg test mode: " + firstArg);
			PeaProperties.setWorkingMode("-t");
			TestLog.v(CommandLineHandler.class, "Run in test mode...");
			if (args.length == 1) { // no other arguments
				return false;
			}
		} else if (firstArg.equals("-r")) {
				//TestLog.o(XXXX.class, "firstArg test mode: " + firstArg);
				PeaProperties.setWorkingMode("-r");
				TestLog.v(CommandLineHandler.class, "Run in rescue mode...");				
		} else if (firstArg.startsWith("--files")){
			PeaProperties.setCommandLineMode(true);
			// OK
		} else {
			TestLog.e(CommandLineHandler.class, "Invalid parameter " + firstArg + "\n" + errorAppendix);
			System.exit(1);
		}

		// initialize variables from PeaSettings
		// - algos and parameters will change later depending on the first file
		PeaControl.initializeVariables();		
		// get the files, passphrase, keyfile:
		for (int i = 0; i < len; i++) {
			String arg = args[i];
			if (arg.equals("--files")) { // must be first argument after modes
				// check for missing files:
				if ( (i + 1) >= len) {
					TestLog.e(CommandLineHandler.class, "Invalid parameter " + args[i] + " - missing specified file." + "\n" + errorAppendix);
					System.exit(1);
				}
				i++;
				files = args[i].split(",");
				TestLog.v(CommandLineHandler.class, "files: " + args[i]);
				// files must be the first argument (after modes)
				// process files, set algos and keyfile before --keyfile
				files = processFiles(files);
				if (files == null) {
					TestLog.e(CommandLineHandler.class, "No valid files found." + "\n" + errorAppendix);
					//System.exit(1);
					return false;
				} else {
					
					PeaProperties.setCommandLineMode(true);	
					// list files:
					System.out.println("Open files: ");
					for (String s : files) {
						TestLog.v(CommandLineHandler.class, s);
						System.out.println(s);
					}	
				}
			} else if (arg.equals("--passphrase") || arg.equals("--password")) {
				// check for null password:				
				if ( (i + 1) >= len) {
					TestLog.e(CommandLineHandler.class, "Missing specified passphrase." + "\n" + errorAppendix);
					System.exit(1);
				}
				i++;
				// remove quotes at beginning and end:
				if (args[i] != null) {
					if (args[i].equals("")) {
						psw = new char[0];
					} else {
						args[i] = args[i].replaceAll("^\"|\"$", "");//replace("\"", "");
						psw = args[i].toCharArray();
					}
				} else {
					TestLog.e(CommandLineHandler.class, "Missing specified passphrase." + "\n" + errorAppendix);
				}
			} else if (arg.equals("--keyfile") ) { // keyFileFlag is already set in processFiles
				
				int usedKeyFileFlag = PeaProperties.getKeyFileFlag();
				TestLog.v(CommandLineHandler.class, "Uses keyfile: " + usedKeyFileFlag);
				if (usedKeyFileFlag == 0) { //no keyfile
					String keyFileName = args[i+1];
					if ( ! ( keyFileName == null || keyFileName.equals("")) ) {
						if (PeaProperties.isDirectStart() == false) {
							TestLog.e(CommandLineHandler.class, "Specified keyfile, but is not required..." + "\n" + errorAppendix);
							commandLineKeyFileName = null;
							System.exit(1);
						}
					}
					// no keyfile, next argument is "" or null
					i++;
				} else if (usedKeyFileFlag == 1 || usedKeyFileFlag == 2) { // default keyfile or manually chosen
					String keyFileName = null;
					if ( (i + 1) >= len) {
						TestLog.e(CommandLineHandler.class, "Missing specified keyfile." + "\n" + errorAppendix);
						if (usedKeyFileFlag == 1) { //default keyfile in properties
							keyFileName = PeaProperties.getDefaultKeyFileName();
							if (keyFileName == null || (new File(keyFileName).exists() == false) ) {
								PeaFileChooser pfc = new PeaFileChooser(null);
								pfc.setFileSelectionMode(PeaFileChooser.FILES_ONLY);
								int returnVal = pfc.showOpenDialog(null);
								if (returnVal == PeaFileChooser.APPROVE_OPTION) {
									keyFileName = pfc.getSelectedFile().getAbsolutePath();
									commandLineKeyFileName = keyFileName;
								} else {
									TestLog.e(CommandLineHandler.class, "No keyfile selected." + "\n" + errorAppendix);
									System.exit(1);
									//return false;
								}
							} else {
								commandLineKeyFileName = keyFileName;
							}
						}

					} else { // keyfile specified
						keyFileName = args[i+1];
						if (keyFileName.equals("")) {
							TestLog.e(CommandLineHandler.class, "No keyfile");
							PeaFileChooser pfc = new PeaFileChooser(null);
							pfc.setFileSelectionMode(PeaFileChooser.FILES_ONLY);
							int returnVal = pfc.showOpenDialog(null);
							if (returnVal == PeaFileChooser.APPROVE_OPTION) {
								keyFileName = pfc.getSelectedFile().getAbsolutePath();
								commandLineKeyFileName = keyFileName;
							} else {
								TestLog.e(CommandLineHandler.class, "No keyfile selected." + "\n" + errorAppendix);
								System.exit(1);
							}
						} else {
							TestLog.v(CommandLineHandler.class, "Keyfile: " + keyFileName);
							keyFileName = keyFileName.replaceAll("^\"|\"$", "");//replace("\"", "");
							if (new File(keyFileName).exists()) {
								commandLineKeyFileName = keyFileName;
							} else {
								TestLog.e(CommandLineHandler.class, "Missing keyfile: " + keyFileName);							
							}
						}
					}
					i++;
				}				

			} else if (arg.equals("-t")) {
				// done
				PeaProperties.setWorkingMode("-t");
			} else if (arg.equals("-r")) {
				// done
				PeaProperties.setWorkingMode("-r");
			} else {
				TestLog.e(CommandLineHandler.class, "Invalid parameter " + args[i]  + "\n" + errorAppendix);
				System.exit(1);
			}
		}
				
		// Create a PeaControl instance from PeaSettings:
		PeaControl pswDialog = null;
		TestLog.o(CommandLineHandler.class, PeaSettings.getCLASS_NAME());
		String errorMessage = "";
		try {
			pswDialog = (PeaControl) Class.forName(PeaSettings.getCLASS_NAME()).newInstance();
		} catch (InstantiationException e) {
			errorMessage = e.getLocalizedMessage() + "\n" + e.toString();
			TestLog.o(CommandLineHandler.class, PeaSettings.getCLASS_NAME() + ": " + e.getLocalizedMessage() + " - " + e.toString(), 5);
		} catch (IllegalAccessException e) {
			errorMessage = e.getLocalizedMessage() + "\n" + e.toString();
			TestLog.o(CommandLineHandler.class, PeaSettings.getCLASS_NAME() + ": " + e.getLocalizedMessage() + " - " + e.toString(), 5);
		} catch (ClassNotFoundException e) {
			errorMessage = e.getLocalizedMessage() + "\n" + e.toString();
			TestLog.o(CommandLineHandler.class, PeaSettings.getCLASS_NAME() + ": " + e.getLocalizedMessage() + " - " + e.toString(), 5);
		}
		if (pswDialog == null) {
			PeaDialog.showMessage(null, "Unexpected error: \n" + errorMessage);
			//return false;
			System.exit(1);
		}
		PeaControl.setDialog(pswDialog);
		
		SwingUtilities.invokeLater(new EventDispatchThread());
		
		return true;				
	}

	/**
	 * Checks the files (existence, access), takes algorithms and parameters
	 * from first valid file, checks other files against this scheme
	 * 
	 * @param commandlineFileNames
	 * @return
	 */
	private static String[] processFiles(String[] commandlineFileNames) {
		if (files == null) {
			TestLog.e(CommandLineHandler.class, "No files to open" ,5);
			System.exit(1);

		} else {
			PeaProperties.setCommandLineMode(true);
			int listLen = files.length;

			// find the first file with valid attachments and set salt, algorithms and parameters		
			int fileIndex = 0;	
			byte[][] settings = null;			
			File firstFile = null;
			while (settings == null && fileIndex < listLen) {

				if (files[fileIndex] != null) {
					// replace leading and trailing quote (file names with spaces)
					files[fileIndex] = files[fileIndex].replaceAll("^\"|\"$", "");//replace("\"", "");
				} else {
					TestLog.e(CommandLineHandler.class, "Missing first file" );
					fileIndex++;
					continue;
				}
				firstFile = new File(files[fileIndex]);
				if (firstFile.exists() == false) {
					TestLog.e(CommandLineHandler.class, "Specified file does not exist: " + files[fileIndex]);
					failedFilesBuilder.append("file does not exist: " + files[fileIndex] + "\n");
					fileIndex++;
					continue;
				}
				if (firstFile.canRead() == false || firstFile.canWrite() == false) {
					TestLog.e(CommandLineHandler.class, "Missing read/write access to the specified file: " + files[fileIndex]);
					failedFilesBuilder.append("No read/write access: " + files[fileIndex] + "\n");
					fileIndex++;
					continue;
				}
				// get salt, algoParams and fileID from first file (if valid) to check other files
				settings = AttachmentHandler.getSaltAlgosFileIDFromFile(firstFile, false);
				if (settings != null) {
					// apply settings from first file:
					// set algos and parameters:
					TestLog.v(CommandLineHandler.class, "Set algos and parameters from file " + firstFile.getAbsolutePath());
					// check and set algorithms and parameters for next decryption if valid
					String error = AlgoParamHandler.readString(new String ( settings[1], AttachmentHandler.getASCIICharset()), false);
					if (error != null) { 
						TestLog.e(CommandLineHandler.class, "Unable to set algorithms and parametrers from file " + files[fileIndex]);
						TestLog.e(CommandLineHandler.class, "The file " + files[fileIndex] + " seems to be invalid...");
						failedFilesBuilder.append("Invalid file: " + files[fileIndex] + "\n");
						settings = null;
						fileIndex++;
						continue;
					}
					PeaProperties.setAlgoParamString(settings[1]);
					KeyDerivation.setSalt(settings[0]);
					Attachments.setFileIdentifier(settings[2]);
				}
				fileIndex++;
			}

			// check algos, salt and fileID against next files and collect valid files in a list:
			if (settings != null) {

				ArrayList<String> validatedList = new ArrayList<String>();
				// always add the first valid file:
				validatedList.add(firstFile.getAbsolutePath());

				// Check the other files
				for (int i = fileIndex; i < listLen; i++) { // start with fileIndex
					if (files[i] != null) {
						// replace leading and trailing quote (file names with spaces)
						files[i] = files[i].replaceAll("^\"|\"$", "");//replace("\"", "");
					} else {
						continue;
					}
					File nextFile = new File(files[i]);
					if (nextFile.exists() == false || nextFile.canRead() == false || nextFile.canWrite() == false) {
						TestLog.e(CommandLineHandler.class, "No access: " + files[i]);
						failedFilesBuilder.append("No access: " + files[i] + "\n");
						continue;
					}
					byte[][] nextSettings = AttachmentHandler.getSaltAlgosFileIDFromFile(nextFile, false);
					if (nextSettings == null) {
						failedFilesBuilder.append("Invalid file: " + files[i] + "\n");
						TestLog.e(CommandLineHandler.class, "Invalid file: " + files[i]);
						continue;
					}
					if (Arrays.equals(settings[2], nextSettings[2]) == true
							&& Arrays.equals(settings[1], nextSettings[1]) == true
							&& Arrays.equals(settings[0], nextSettings[0]) == true) {
						// Add file to validated list:
						validatedList.add(files[i]);
					} else {
						failedFilesBuilder.append("Must be opened separately: " + files[i] + "\n");
						TestLog.ve(CommandLineHandler.class, "Cannot open together with first file: " + files[i]);
						TestLog.e(CommandLineHandler.class, "Cannot open together with first file: " + files[i]);
						continue;
					}					
				}				

				// set validated list:
				files = validatedList.toArray(new String[0]);

			} else {
				TestLog.e(CommandLineHandler.class, "All files seem to be invalid...");
				TestLog.e(CommandLineHandler.class, "Program exits...");
				//System.exit(1);
				return null;
			}
		}
		return files;
	}

	public static String[] getFileNames() {
		//TestLog.v(CommandLineHandler.class, files.length + " files" );
		return files;
	}

	/**
	 * This is called in PeaControl.getKeyMaterial(). 
	 * If the password is not yet set by an argument, 
	 * the user is asked to enter
	 * 
	 * @return	the password as char array
	 */
	static char[] getPassword() {

		// if getting from console failed:
		//  get from password dialog
		if (psw == null) {
			JPasswordField passwordField = new JPasswordField();
			int result = JOptionPane.showConfirmDialog(null, passwordField, 
					PeaProperties.getVmBridge().translate("enter_password"), 
					JOptionPane.OK_CANCEL_OPTION, JOptionPane.PLAIN_MESSAGE);

			if (result == JOptionPane.OK_OPTION) {
				psw = passwordField.getPassword();
			}	
		}

		/*		TestLog.o(CommandLineHandler.class, "!!! "
								+ "Warning: Missing console...");

						try {
							new java.util.Timer().schedule( 
									new java.util.TimerTask() {
										@Override
										public void run() { 

											TestLog.o(CommandLineHandler.class, "--- Enter password:");
											//Enter data using BufferReader 
											BufferedReader reader =  
													new BufferedReader(new InputStreamReader(System.in)); 
											// Reading data using readLine 
											try {

												psw = reader.readLine().toCharArray();

											} catch (NullPointerException e1) {
												TestLog.ve(CommandLineHandler.class, e1.getLocalizedMessage() + " - " + e1.toString(), 5);
											} catch (Exception e1) {
												TestLog.ve(CommandLineHandler.class, e1.getLocalizedMessage() + " - " + e1.toString(), 5);
											} finally {
												try {
													reader.close();
												} catch (IOException e2) {
													TestLog.ve(CommandLineHandler.class, e2.getLocalizedMessage() + " - " + e2.toString(), 5);
												}
											}
										}
									}, 
									100 
									); 

							//		psw = System.console().readLine().toCharArray();
						} catch (Exception e2) {
							TestLog.ve(CommandLineHandler.class, e2.getLocalizedMessage() + " - " + e2.toString(), 5);
						}
		 */



		if (psw == null) {
			PeaDialog.showMessage(null, "Can't get the password.\nStart the PEA without commands...", 
					PeaProperties.getVmBridge().translate("unexpected_error"), 0);
		}

		return psw;
	}
	
	/**
	 * Create arguments for direct start mode: Check command line
	 * arguments
	 * 
	 * @param args	the original arguments from command line
	 * 
	 * @return	the arguments for a direct start mode
	 */
	public final static String[] getDirectStartArgs(String[] args) {
		// check args and create new args
		boolean startFiles = true;
		ArrayList<String> newArgs = new ArrayList<String>();
		StringBuilder filesBuilder = new StringBuilder();
		if (args != null) {
			int argsLen = args.length;
			// check for -h --help --version 
			for (int i = 0; i < argsLen; i++) {
				String arg = args[i];
				if (arg.equals("-h") || arg.equals("--help") || arg.equals("--version")) {
					startFiles = false;
					break;
				} else if (arg.equals("-t") || arg.equals("-r") ) {
					newArgs.add(arg);
				} else if (arg.equals("--files")) { 
					TestLog.e(CommandLineHandler.class, "Start direct with keyfile-only mode, but files as arguments are present...");
					if (argsLen > i + 1 && args[i + 1] != null) {
						// add the files from command line
						filesBuilder.append(args[i + 1] + ",");
						i++;
					}
				} else if (arg.equals("--passphrase") || arg.equals("--password")) {
					TestLog.e(CommandLineHandler.class, "Start direct with keyfile-only mode, but passphrase as arguments is present.");
					TestLog.e(CommandLineHandler.class, "Passphrase is ignored...");
				} else if (arg.equals("--keyfile") ) {
					TestLog.e(CommandLineHandler.class, "Start direct with keyfile-only mode, but keyfile as arguments is present...");
					// check if keyfile equals keyfile in properties:
					String defKeyFileName = PeaProperties.getDefaultKeyFileName();
					if (defKeyFileName == null && argsLen > i + 1) {
						TestLog.e(CommandLineHandler.class, "Keyfile from properties is null. Set keyfile from command line...");
						PeaProperties.setDefaultKeyFileName(args[i + 1]);
					}
					if (argsLen > i + 1 &&  ! args[i + 1].equals(defKeyFileName)) {
						TestLog.e(CommandLineHandler.class, "Keyfile from command line differs from properties: " + args[i+1] + ", keyfile from command line is ignored.");
					}
					i++;
				}
			}
			if (startFiles == true) {
				
				// first arg must be files if not in test mode!
				newArgs.add("--files");
				String comma = "";
				boolean existingFileFound = false;
				ArrayList<String> filesFromProps = PeaProperties.getLastUsedFiles();
				for (String f : filesFromProps) {
					if (f != null) {
						if (new File(f).exists()) {
							existingFileFound = true;
						}
					}
					filesBuilder.append(comma);
					filesBuilder.append(f);
					comma = ",";
				}
				if (existingFileFound == false) {
					TestLog.e(CommandLineHandler.class, "Non of the files to open on direct start exists...");
					PeaProperties.setDirectStart(false);		
					if (newArgs.contains("-t")) {
						String[] newArgArray = {"-t"};
						return newArgArray;						
					} else {
						return null;
					}
				}
				newArgs.add(new String(filesBuilder));
				TestLog.v(CommandLineHandler.class, "File names: " + new String(filesBuilder));					

				newArgs.add("--keyfile");
				if (PeaProperties.getDefaultKeyFileName() != null) {
					File keyFile = new File(PeaProperties.getDefaultKeyFileName());
					if (keyFile.exists() == true) {
						newArgs.add(PeaProperties.getDefaultKeyFileName());
					} else {
						TestLog.e(CommandLineHandler.class, "Keyfile does not exist: " + PeaProperties.getDefaultKeyFileName());
						return null;
					}
				}else {
					TestLog.e(CommandLineHandler.class, "No keyfile specified...");
					return null;
				}

				newArgs.add("--passphrase");
				newArgs.add("");
				
			}
		}
		return  Converter.arrayListToArray( newArgs);
	}


	/**
	 * Get the keyfile name from command line
	 * 
	 * @return the commandLineKeyFileName
	 */
	public static String getCommandLineKeyFileName() {
		return commandLineKeyFileName;
	}
	
	//============ inner classes ================
	
	private static class  EventDispatchThread implements Runnable{

		@Override
		public void run() {
			
			PeaControl.getDialog().startExecution(false);
			if (failedFilesBuilder.length() > 0) {
				PeaDialog.showMessage(JREProperties.getMainWindow(), 
						new String(failedFilesBuilder), PeaProperties.getVmBridge().translate("error"), 0);
			}
		}		
	}
}
