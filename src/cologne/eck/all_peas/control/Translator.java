package cologne.eck.all_peas.control;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashSet;
import java.util.Locale;
import java.util.MissingResourceException;
import java.util.PropertyResourceBundle;
import java.util.ResourceBundle;

import cologne.eck.all_peas.data.PeaProperties;
import cologne.eck.all_peas.gui.PeaDialog;
import cologne.eck.all_peas.gui.menu.SettingMenu;
import cologne.eck.all_peas.vm_specific.JREBridge;
import cologne.eck.all_peas.vm_specific.JREProperties;
import cologne.eck.tools.Converter;
import cologne.eck.tools.PropertyHandler;
import cologne.eck.tools.TestLog;

public class Translator {
	
	/**
	 * The bundle name for all PEAs
	 */
	private static final String bundleFileName = "PeaLanguagesBundle";
	/**
	 * The bundle name for Calendar Lock PEA
	 */
	private static final String calBundleFileName = "CalendarLanguageBundle";
	
	/**
	 * The bundle name for Contact PEA
	 */
	private static final String contactBundleFileName = "ContactLanguageBundle";
	
	/**
	 * The bundle name for Contact PEA
	 */
	private static final String noteBundleFileName = "NoteLanguageBundle";
	
	/**
	 * The bundle name for Contact PEA
	 */
	private static final String imageBundleFileName = "ImageLanguageBundle";
	
	/**
	 * The last successfully set language bundle for all PEAs
	 */
	private ResourceBundle lastBundle = null;	
	/**
	 * The last successfully set language bundle for all PEAs
	 */
	private ResourceBundle lastPeaBundle = null;
	
	
	public String translate(String key, String specificPEA) {
		
		ResourceBundle usedBundle = null;
		if (specificPEA == null) { // bundle for all PEAs
			if (lastBundle == null) {
				try {
					TestLog.v(getClass(), "Set language support");
					boolean setBundleSuccess = setLanguagesBundle(PeaProperties.getLastLanguage());
					if (setBundleSuccess == false) {
						TestLog.ve(getClass(), "Can't set language bundle...", 5);
					}
				} catch (MalformedURLException e) {
					TestLog.ve(getClass(), e.getLocalizedMessage() + " - " + e.toString(), 5);
				}
			}
			usedBundle = lastBundle;//(ResourceBundle)JREProperties.getBundle();
		} else if (specificPEA.equals("calendar")) {
			 usedBundle = lastPeaBundle;
		} else if (specificPEA.equals("contact")) {
			 usedBundle = lastPeaBundle;
		} else if (specificPEA.equals("image")) {
			 usedBundle = lastPeaBundle;
		} else if (specificPEA.equals("text file")) {
			 usedBundle = lastPeaBundle;
		} // TODO File Lock PEA?


		if ( JREProperties.getBundle() != null) {
			if (usedBundle == null) {
				try { // should not happen, should have been set before with setLanguagesBundle()
					TestLog.v(getClass(), "Set language support for specific PEA",5);
					usedBundle = getPeaLanguageBundle(PeaProperties.getLastLanguage());//setLanguagesBundle(null);
					lastPeaBundle = usedBundle;
				} catch (Exception e) {
					TestLog.ve(getClass(), e.getLocalizedMessage() + " - " + e.toString(), 5);
				}
			}
			if (usedBundle != null &&  usedBundle.containsKey(key) ){
				return  usedBundle.getString(key);		
			} else {
				// Something went wrong... try to use default language (English) or as fallback return the key
				String bundleName = null;
				if (specificPEA == null) { // bundle for all PEAs
					bundleName = bundleFileName;
				} else if (specificPEA.equals("calendar")) {
					bundleName = calBundleFileName;
				} else if (specificPEA.equals("contact")) {
					bundleName = contactBundleFileName;
				} else if (specificPEA.equals("image")) {
					bundleName = imageBundleFileName;
				} else if (specificPEA.equals("text file")) {
					bundleName = noteBundleFileName;
				}
				// get value from default file inside jar
				try {
					TestLog.ve(Translator.class, PeaProperties.getLastLanguage() + " - Cant find value for key " + key + ", search in default bundle...", 3);
					//ResourceBundle bundle  = new PropertyResourceBundle(JREBridge.class.getResourceAsStream("/resources/PeaLanguagesBundle.properties"));
					ResourceBundle bundle  = getBundleFromInsideJar("", bundleName);

					if (bundle != null) { // check 
						return bundle.getString(key);
					} else {
						TestLog.ve(Translator.class, "Could not load default bundle from inside jar");
						// get default bundle from folder
						bundle  = getBundleFromExternFolder("", bundleName);
						if (bundle != null) {
							return bundle.getString(key);
						} else {
							return key;
						}
					}
				} catch (Exception e) {
					TestLog.ve(Translator.class, "Cant find value for key " + key + " in default bundle inside jar", 5);
				}					
				return key;
			}
		} else {
			TestLog.e(Translator.class, "Invalid bundle, value not found for " + key, 5);
			return key;
		}
	}

	/**
	 * Get all available languages: Bundles inside jar
	 * and installed languages in folder resources
	 * 
	 * @return	All available languages as array of language codes
	 */
	public final String[] getAvailableLanguages()  {
		HashSet<String> languagesSet = new HashSet<String>();
		// add default language en
		languagesSet.add("en");
		
		HashSet<String> installed = getInstalledLanguages();
		languagesSet.addAll(installed);
		
/*		// get bundles from resource directory in file system:
		File resDir = new File(System.getProperty("user.dir") + File.separator 
				+ "resources");
		if (resDir.exists() && resDir.isDirectory()) {
			File[] resList = resDir.listFiles();
			if (resList != null) {
				for (File f : resList) {
					if (f != null) {
						if (f.isDirectory()) {
							File[] langDir = f.listFiles();
							if (langDir == null) {
								continue;
							} else { // check for the properties file:
								String lang = f.getName();
								String fName = f.getAbsolutePath() + File.separator
										+ "PeaLanguagesBundle_" + lang + ".properties";
								//TestLog.v(Translator.class, "Property files: " + fName);
								if ( new File(fName).exists()) {
									if ( ! lang.equals("jp")  && ! lang.equals("cn") && ! lang.equals("ua")) {
										// Check does not work for jp and cn: jp -> ja, cn -> "zh_CN" 
										try {
											if ( new Locale(lang).getISO3Language() != null) {
												languagesSet.add(lang);
												//TestLog.v(Translator.class, "bundle in resources found: " + lang);
											} else {
												TestLog.ve(Translator.class, "Invalid languages code: " + lang);
											}
										} catch (MissingResourceException e) {
											TestLog.ve(Translator.class, "Invalid languages code: " + lang);
										}									
									}
								} else {
									TestLog.ve(Translator.class, "Not a language folder: " + lang);
								}
								// TODO if (PeaProperties.getFileType().equals("calendar")) {
							}
						}
					}
				}
			}
		} else {
			TestLog.v(Translator.class, "Missing folder or not a valid resource folder...");
		}*/
		
		// get bundles from inside jar: leading / required
		// Possible future languages (by countries where PEAs are most downloaded), for now only left-aligned
		String[] langs = {"de", "tr", "es", "fr", "ru", "bg", "cn", "cs", "cz", "da", "el", "et", "fi", "gr", "hu", "id", "it", "jp", "kr", "nl", "lv", "pl", "pt", "ro", "sk", "sl", "sq", "sr", "sv", "ua" };
		String pre = "/resources/";
		String mid = "/PeaLanguagesBundle_";
		String post = ".properties";
		for (String lang : langs) {
			if (getClass().getResourceAsStream(pre + lang + mid + lang + post) != null) {
				languagesSet.add(lang);
				TestLog.v(Translator.class, "bundle found: " + lang);
			}
		}
		return Converter.hashSetToArray(languagesSet);
	}

	/**
	 * Get installed languages from external resources folder
	 * as set of language codes
	 * 
	 * @return installed languages as set of language codes
	 */
	public HashSet<String> getInstalledLanguages(){
		
		HashSet<String> installedLang = new HashSet<String>();
		// get bundles from resource directory in file system:
		File resDir = new File(System.getProperty("user.dir") + File.separator 
				+ "resources");
		if (resDir.exists() && resDir.isDirectory()) {
			File[] resList = resDir.listFiles();
			if (resList != null) {
				for (File f : resList) {
					if (f != null) {
						if (f.isDirectory()) {
							File[] langDir = f.listFiles();
							if (langDir == null) {
								continue;
							} else { // check for the properties file:
								String lang = f.getName();
								String fName = f.getAbsolutePath() + File.separator
										+ "PeaLanguagesBundle_" + lang + ".properties";
								//TestLog.v(Translator.class, "Property files: " + fName);
								if ( new File(fName).exists()) {
									if ( ! lang.equals("jp")  && ! lang.equals("cn") && ! lang.equals("ua")) {
										// Check does not work for jp and cn: jp -> ja, cn -> "zh_CN" 
										try {
											if ( new Locale(lang).getISO3Language() != null) {
				//								languagesSet.add(lang);
												installedLang.add(lang);
												//TestLog.v(Translator.class, "bundle in resources found: " + lang);
											} else {
												TestLog.ve(Translator.class, "Invalid languages code: " + lang);
											}
										} catch (MissingResourceException e) {
											TestLog.ve(Translator.class, "Invalid languages code: " + lang);
										}									
									}
								} else {
									TestLog.ve(Translator.class, "Not a language folder: " + lang);
								}
								// TODO if (PeaProperties.getFileType().equals("calendar")) {
							}
						}
					}
				}
			}
		} else {
			TestLog.v(Translator.class, "Missing folder or not a valid resource folder...");
		}
		return installedLang;
	}
	
	
	/**
	 * Set the requested language bundle from a language String
	 * for all PEAs and if required for the specific PEA
	 * 
	 * @param _language		the language e.g. "en", "de", "tr"
	 * @throws MalformedURLException if the language String is not valid
	 * 
	 * @return	true for success
	 */
	public final boolean setLanguagesBundle(String _language) throws MalformedURLException {
		
		//ResourceBundle languageBundle = null;
		String langToReset = PeaProperties.getLastLanguage();
		//TestLog.e(getClass(), "Set language: " + _language, 5);

		try {
			Locale defaultLocale = Locale.getDefault();
			TestLog.v(Translator.class, "Set language: " + _language);
			String language = null;

			// i18n: 
			if (_language == null) {
				if (defaultLocale == null) { // openSuse
					defaultLocale = new Locale( System.getProperty("user.language") );
				}		
				language = defaultLocale.getLanguage(); // get default language
				if (language == null) { // fall back
					language = "";
				}
			} else {
				language = _language;
			}

			//======= 1. Check resource inside jar
			lastBundle = getBundleFromInsideJar(language, bundleFileName);

			if (lastBundle == null) {
				//======= 2. Check resource in external folder resources
				lastBundle = getBundleFromExternFolder(language, bundleFileName);
			}
			PeaProperties.setLastLanguage(language);
			if (lastBundle == null) {
				//======= 4. Fallback English
				lastBundle = getFallBackEnglish(bundleFileName);
				PeaProperties.setLastLanguage("en");
			}		
			if (lastBundle == null) { // get any bundle if not found
				lastBundle = ResourceBundle.getBundle("resources." + bundleFileName, new Locale(_language));
			} 
			if (lastBundle == null && langToReset != null) {
				boolean success = setLanguagesBundle(langToReset);
				if (success == false) {
					PeaProperties.getVmBridge().exitWithError(JREProperties.getMainWindow(), "Language error", false);
				}
				language = langToReset;
				PeaProperties.setLastLanguage(language);
			}
			JREProperties.setBundle(lastBundle);
			
		} catch (Exception e) {
			TestLog.ve(Translator.class, e.getLocalizedMessage() + " - " + e.toString());
			if (lastBundle == null && langToReset != null) {
				boolean success = setLanguagesBundle(langToReset);
				if (success == false) {
					PeaProperties.getVmBridge().exitWithError(JREProperties.getMainWindow(), "Language error", false);
					return false;
				}
			}		
		}
		
		// Now set the language bundle specific for the current PEA:
		// Try to use same language, if this fails, use English.
		// for now only Calendar Lock PEA
		if (PeaProperties.getFileType() != null) { // no called in Start.java
			if (PeaProperties.getFileType().equals("calendar")  || PeaProperties.getFileType().equals("contact")) {
				ResourceBundle b = getPeaLanguageBundle(PeaProperties.getLastLanguage());
				if (b == null) {
					return false;
				}
			}		
		}
		return true;
	}
	
	/**
	 * Check if there is a language support for 
	 * the language, ask to install
	 * 
	 * @param defaultLocale	Locale to check for
	 * 
	 * @return	true, if the language pack was installed
	 */
	public static boolean showLanguageInstallOption(Locale defaultLocale) {
		TestLog.v(Translator.class, "Check installoption for language: " + defaultLocale.getLanguage());
		if (PeaProperties.isInstallLanguageOptionShown() == false) {
			String checkLang = defaultLocale.getLanguage();
			String[] suppLangs = PeaControl.getDialog().getSupportedLanguages();
			for (String la : suppLangs) {
				// some languages changed (jp, ua, cn): check also for new Locale(la).getLanguage()
				if (checkLang.equals(la) || checkLang.equals(new Locale(la).getLanguage())) {
					// show install info and option:
					int instLang = PeaDialog.showQuestion(JREProperties.getMainWindow(), 
							"There is a language pack for your currently set language: " + checkLang +  "\n"
							+ "   The language can be reset later in the settings menu.  \n\n"
							+ "Should the language be installed?",
							null, 0);
					if (instLang == 0) {
						return SettingMenu.installLanguages();
					} else {
						//
					}
				}
			}
			// do not ask again
			new PropertyHandler().setOneProperty("install_Lang_option_shown", "TRUE");
		}
		return false;
	}


	public ResourceBundle getPeaLanguageBundle(String language) {
		
		//String language = PeaProperties.getLastLanguage();
		// Now set the language bundle specific for the current PEA:
		// Try to use same language, if this fails, use English.
		// for now only Calendar Lock PEA
		String xBundleFileName = null;
		if (PeaProperties.getFileType().equals("calendar")) {
			xBundleFileName = calBundleFileName;
		} else if (PeaProperties.getFileType().equals("contact")) {
			xBundleFileName = contactBundleFileName;
		} else if (PeaProperties.getFileType().equals("text file")) {
			xBundleFileName = noteBundleFileName;
		} else if (PeaProperties.getFileType().equals("image")) {
			xBundleFileName = imageBundleFileName;
		} else {
			TestLog.ve(getClass(), "Pea specific language bundle not yet supported...", 5);
			return null;
		}
			try {
				//======= 1. Check resource inside jar
				lastPeaBundle = getBundleFromInsideJar(language, xBundleFileName);

				if (lastPeaBundle == null) {
					//======= 2. Check resource in external folder resources
					lastPeaBundle = getBundleFromExternFolder(language, xBundleFileName);
				}
				if (lastPeaBundle == null) {
					lastPeaBundle = ResourceBundle.getBundle("resources." + xBundleFileName, new Locale(language));
				}
				if (lastPeaBundle == null) {
					//======= 4. Fallback English
					lastPeaBundle = getFallBackEnglish(xBundleFileName);
				}		
				if (lastPeaBundle == null) {
					PeaProperties.getVmBridge().exitWithError(JREProperties.getMainWindow(), "Language error", false);
					return null;
				}
			} catch (Exception e) {
				TestLog.ve(Translator.class, e.getLocalizedMessage() + " - " + e.toString());
				if (lastPeaBundle == null) {
					//======= 4. Fallback English
					lastPeaBundle = getFallBackEnglish(xBundleFileName);
				}	
			}			
			return lastPeaBundle;
	}
	
	public ResourceBundle getFallBackEnglish(String _bundleFileName) {
		ResourceBundle result = null;
		result = getBundleFromInsideJar("", _bundleFileName);
		if (result == null) {
			TestLog.ve(Translator.class, "Missing default bundle inside jar. ");// + Locale.forLanguageTag("en").toLanguageTag());
			result = getBundleFromExternFolder("", _bundleFileName);
		}
		if (result == null) {
			TestLog.v(Translator.class, "Resource bundle Fallback English");
			getBundleFromInsideJar("", Translator.bundleFileName);
			if (result == null) {
				result = getBundleFromExternFolder("", Translator.bundleFileName);
			}
		}
		if (result == null) {
			TestLog.e(Translator.class, "No resource bundle English found");
			TestLog.ve(Translator.class, "No resource bundle English found", 9);
		}
		TestLog.v(Translator.class, "Set language to fallback English");
		return result;
	}
		
	/**
	 * Get the language bundle from the external folder resources. 
	 * The bundle is stored in the sub-directory, named as the country code, e.g. "tr"
	 * 
	 * @param newLanguage	the country code, e.g. "tr" or null (for default)
	 * @param bundleName	the base name of the bundle filename without country code
	 * 
	 * @return	the resource bundle or null, if the corresponding bundle was not found
	 */
	public ResourceBundle getBundleFromExternFolder(String newLanguage, String bundleName) {
		ResourceBundle result = null;
		TestLog.v(Translator.class, "Read from external file..." + newLanguage);
		// handle the default bundle en:
		if (newLanguage.equals("en")) {
			newLanguage = ""; // This is the default bundle
		}
		String underscore = "_";
		if (newLanguage == null || newLanguage.length() == 0) {
			underscore = "";
		}
		String fileName = "resources" + File.separator + newLanguage + File.separator + bundleName + underscore + newLanguage + ".properties";
		if (new File(fileName).exists()) {
			TestLog.v(Translator.class, "Bundle found in folder resources");
		} else {
			TestLog.v(Translator.class, "External Resource Bundle "  + fileName + " does not exist");
			return null;
		}
		try {
			result = new  PropertyResourceBundle(Files.newInputStream(Paths.get(fileName)));
					//"resources" + File.separator + language + File.separator + "PeaLanguagesBundle_" + language + ".properties")));
		} catch (IOException e) {
			TestLog.v(Translator.class, fileName + ": " +  e.toString() + " - " + e.getMessage());
		} catch (Exception e) {
			TestLog.v(Translator.class, fileName + ": " +  e.toString() + " - " + e.getMessage());
		}
		if (result != null) {
			TestLog.v(Translator.class, "Resource bundle in folder resources: " + newLanguage);
		}
		return result;		
	}
	
	/**
	 * Get the language bundle from inside the jar archive. 
	 * The bundle is stored in the sub-directory or the folder resources,
	 *  named as the country code, e.g. "tr"
	 * 
	 * @param newLanguage	the country code, e.g. "tr" or null (for default)
	 * @param bundleName	the base name of the bundle filename without country code
	 * 
	 * @return	the resource bundle or null, if the corresponding bundle was not found
	 */
	private ResourceBundle getBundleFromInsideJar(String newLanguage, String bundleName) {
		ResourceBundle result = null;
		TestLog.v(Translator.class, "Search for language bundle inside jar: " + newLanguage);
		// Handle the default bundle en
		if (newLanguage.equals("en")) {
			newLanguage = ""; // This is the default bundle
		}
		String underscore = "_";
		String langFolder = newLanguage + "/";
		if (newLanguage == null || newLanguage.length() == 0) {
			underscore = "";
			langFolder = "";
		}
		//======= 1. Check resource inside jar
		try {
			result = new PropertyResourceBundle(JREBridge.class.getResourceAsStream("/resources/" + langFolder + bundleName + underscore + newLanguage + ".properties"));
			//languageBundle = new  PropertyResourceBundle(Files.newInputStream(Paths.get(
			//		"resources/PeaLanguagesBundle_" + language + ".properties")));
			
		} catch (NullPointerException e) {
			TestLog.v(Translator.class, "Resource Bundle " + langFolder + bundleName + underscore + newLanguage + " not found: " + e.getMessage());
		} catch (IOException e) {
			TestLog.v(Translator.class, "Resource Bundle  " + langFolder + bundleName + underscore + newLanguage + " no access: " + e.getMessage());
		} catch (Exception e) {
			TestLog.v(Translator.class, "Resource Bundle  " + langFolder + bundleName + underscore + newLanguage + " - unexpected error - " + e.getMessage());
		}
		if (result != null) {
			TestLog.v(Translator.class, "Resource bundle inside jar: " + newLanguage);
		}
		return result;
	}

}
