package cologne.eck.all_peas.control;

/*
 * Peafactory - Production of Password Encryption Archives
 * Copyright (C) 20221  Axel von dem Bruch
 * 
 * This library is free software; you can redistribute it and/or modify it 
 * under the terms of the GNU General Public License as published 
 * by the Free Software Foundation; either version 2 of the License, 
 * or (at your option) any later version.
 * This library is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
 * or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU General Public License for more details.
 * See:  http://www.gnu.org/licenses/gpl-2.0.html
 * You should have received a copy of the GNU General Public License 
 * along with this library.
 */

import java.awt.Component;
import java.io.File;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashSet;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;

import cologne.eck.all_peas.data.PeaProperties;
import cologne.eck.all_peas.gui.PeaDialog;
import cologne.eck.all_peas.gui.menu.SettingMenu;
import cologne.eck.all_peas.vm_specific.JREProperties;
import cologne.eck.tools.ExceptionHelper;
import cologne.eck.tools.StringNumberHelper;
import cologne.eck.tools.TestLog;
import cologne.eck.tools.UnexpectedValueException;
import cologne.eck.tools.WebTools;

public class UpdateChecker {
	
	private static File testFile = null;
	protected static void setTestFile(File _testFile) {	testFile = _testFile;}	
	
	/**
	 * Check if there is an update of the current PEA available
	 * (read from PAD file). 
	 * 
	 * @param c				the Component
	 * @param showErrors	true: show errors as dialogs
	 */
	public static void checkUpdates(Component c, boolean showErrors){
		
		TestLog.v(UpdateChecker.class, "Check updates... " );
				
		boolean langUpdate = checkInstalledLanguages();
		if (langUpdate == true) {
			int installLang = PeaDialog.showQuestion(JREProperties.getMainWindow(),  
					PeaProperties.getVmBridge().translate("update_found") + ":\n" 
							+ PeaProperties.getVmBridge().translate("language") + "\n\n"
							+ PeaProperties.getVmBridge().translate("install") + "?", 
							PeaProperties.getVmBridge().translate("update_found"), 0);
			if (installLang == 0) {
				HashSet<String> installedLangs = PeaProperties.getVmBridge().getTranslator().getInstalledLanguages();
				for (String lang : installedLangs) {
					SettingMenu.downloadLanguageFiles(lang);
				}
			}
		} else {
			TestLog.v(UpdateChecker.class, "No language update found");
		}
		
		// create PAD file url: 
		// e.g. https://eck.cologne/peafactory/src/PAD/calendarPAD.xml
		String baseUrl = "https://eck.cologne/peafactory/src/PAD/";
		String peaExtension = null;
		String appType = PeaProperties.getFileType();
		if (appType.equals("calendar")) {
			peaExtension = "calendarPAD.xml";
		} else if (appType.equals("text file")) {
			peaExtension = "notebookPAD.xml";
		} else if (appType.equals("file")) {
			peaExtension = "filePAD.xml";
		} else if (appType.equals("image")) {
			peaExtension = "imagePAD.xml";
		} else if (appType.equals("contact")) {
			peaExtension = "contactPAD.xml";
		} else {
			new UnexpectedValueException("String", "appType", "is not valid: " + appType).printDescription();
			if (showErrors == true) {
				PeaDialog.showMessage(c, PeaProperties.getVmBridge().translate("unexpected_error"), 
						PeaProperties.getVmBridge().translate("error"), 0);
			}
			return;
		}
		String urlString = baseUrl + peaExtension;
		if (WebTools.checkConnection() == false){
			if (showErrors == true) {
				PeaDialog.showMessage(c, PeaProperties.getVmBridge().translate("bad_web_connection"), 
						PeaProperties.getVmBridge().translate("error"), 0);
			}
			return;
		}
		TestLog.v(UpdateChecker.class, "Check URL: " + urlString);
		File file = WebTools.getFileFromURL(urlString);
		// for testing:
		if (testFile != null) {
			file = testFile;
		}
		
		if (file == null) {
			if (showErrors == true) {
				PeaDialog.showMessage(c, PeaProperties.getVmBridge().translate("download_failed"), 
						PeaProperties.getVmBridge().translate("error"), 0);
			}
			return;				
		} else {
			DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory
					.newInstance();
			DocumentBuilder documentBuilder;
			try {
				documentBuilder = documentBuilderFactory.newDocumentBuilder();
				Document document = documentBuilder.parse(file);
				
				//TestLog.v(UpdateChecker.class, "Available version... " );
				String availableVersion = "";
				if (document != null
						&& document.getElementsByTagName("Program_Version") != null 
						&& document.getElementsByTagName("Program_Version").item(0) != null 
						&& document.getElementsByTagName("Program_Version").item(0).getTextContent() != null) {
					availableVersion = document.getElementsByTagName("Program_Version").item(0).getTextContent();
					if (availableVersion == null) {
						TestLog.e(UpdateChecker.class, "Missing available version...");
						if (showErrors == true) {
							PeaDialog.showMessage(c, PeaProperties.getVmBridge().translate("unexpected_error"), 
									PeaProperties.getVmBridge().translate("error"), 0);
						}
						return;
					}
				} else {
					TestLog.e(UpdateChecker.class, "Missing available version...");
					if (showErrors == true) {
						PeaDialog.showMessage(c, PeaProperties.getVmBridge().translate("unexpected_error"), 
								PeaProperties.getVmBridge().translate("error"), 0);						
					}
					return;
				}
				//TestLog.v(UpdateChecker.class, "Available version: " + availableVersion);
				// get release status: Major Update|Minor Update|New Release|Beta|Alpha|Media Only
				String releaseStatus = document.getElementsByTagName("Program_Release_Status").item(0).getTextContent();
				if (releaseStatus == null) {
					releaseStatus = "";
				}
				//TestLog.v(UpdateChecker.class, "Release: " + releaseStatus);
				String currentVersion = PeaControl.getDialog().getVersion();
				//String currentVersionNumber = currentVersion.replaceAll("[^0-9.]", "");
				TestLog.o(UpdateChecker.class, "current version: " + currentVersion
						+ ", available version: " + availableVersion + ", release status: " + releaseStatus);

				int newVersion = StringNumberHelper.checkNewVersionNumber(currentVersion, availableVersion);
				//TestLog.o(UpdateChecker.class, "newVersion: " + newVersion);
				if (newVersion == 1) {
					// always show new version
				} else if (newVersion == 0) {
					// in current version string contains possibly alpha or beta
					// check release status:
					if (currentVersion.toLowerCase().contains("alpha")) {
						if ( releaseStatus.toLowerCase().equals("alpha")) {
							if (showErrors == true) {
								PeaDialog.showMessage(c, PeaProperties.getVmBridge().translate("no_update_found"));
							}
							return;
						} else {
							// show new version
						}
					} else if (currentVersion.contains("beta")) {
						if ( releaseStatus.toLowerCase().equals("beta") || releaseStatus.toLowerCase().equals("alpha")) {
							if (showErrors == true) {
								PeaDialog.showMessage(c, PeaProperties.getVmBridge().translate("no_update_found"));
							}
							return;
						} else {
							// show new version
						}		
					} else {
						if (showErrors == true) {
							PeaDialog.showMessage(c, PeaProperties.getVmBridge().translate("no_update_found"));
						}
						return;
					}
				} else {
					if (showErrors == true) {
						PeaDialog.showMessage(c, PeaProperties.getVmBridge().translate("no_update_found"));
					}
					return;
				}
				// TODO date of new version, changes...
				String[] messageAndLink = {PeaProperties.getVmBridge().translate("web"), 
						"http://eck.cologne/peafactory/en/" + PeaControl.getDialog().getWebsite()
				};
				String[][] messagesAndLinks = {messageAndLink};
				PeaDialog.showMessageWithLink(c, 
						PeaProperties.getVmBridge().translate("update_found") + "\n      " 
								+ PeaProperties.getVmBridge().translate("version") + " " + availableVersion//av
									+ " (" + releaseStatus + ")",
								messagesAndLinks, null, -1);
			} catch (ParserConfigurationException e) {
				TestLog.e(UpdateChecker.class, new ExceptionHelper(e).getInfos());
				if (showErrors == true) {
					PeaDialog.showMessage(c, PeaProperties.getVmBridge().translate("unexpected_error"), 
							PeaProperties.getVmBridge().translate("error"), 0);
				}
			} catch (Exception e) {
				TestLog.e(UpdateChecker.class, e.toString() + " - " + e.getLocalizedMessage());

				if (showErrors == true) {
					PeaDialog.showMessage(c, PeaProperties.getVmBridge().translate("unexpected_error"), 
							PeaProperties.getVmBridge().translate("error"), 0);
				}
			}
		}
		
	}
	/**
	 * Check if there is an update of language packs. 
	 * 
	 * @return	true if there is an update, false for no update or any error
	 */
	private static boolean checkInstalledLanguages() {
		HashSet<String> installedLangs = PeaProperties.getVmBridge().getTranslator().getInstalledLanguages();

		for (String lang : installedLangs) {

			String localFolder = System.getProperty("user.dir") + File.separator + "resources" + File.separator + lang + File.separator;			
			String urlFolder = "https://eck.cologne/peafactory/src/language/" + lang +"/";

			long fileSize = 0;
			
			
			String specificUrlFile = null;
			if ( ! PeaProperties.getFileType().equals("file")) { // all PEAs except File Lock PEA
				// Specific language bundles:
				String bundleFirstPart = "";
				if (PeaProperties.getFileType().equals("calendar")) { 
					bundleFirstPart = "Calendar";
				} else if (PeaProperties.getFileType().equals("contact")) { 
					bundleFirstPart = "Contact";
				} else if (PeaProperties.getFileType().equals("image")) { 
					bundleFirstPart = "Image";
				} else if (PeaProperties.getFileType().equals("text file")) { 
					bundleFirstPart = "Note";
				} else {
					TestLog.ve(UpdateChecker.class, "Unknown PEA type for language bundles: " + PeaProperties.getFileType(), 5);
				}
				specificUrlFile =   bundleFirstPart + "LanguageBundle_" + lang + ".properties";
			}
			String allPeaUrlFile =   "PeaLanguagesBundle_" + lang + ".properties";
			String[] filesToCheck = {allPeaUrlFile, specificUrlFile};

			for (String urlFile : filesToCheck) {
				if (urlFile != null) {
					URL url = null;
					try {
						url = new URL(urlFolder + urlFile);
					} catch (MalformedURLException e) {
						TestLog.ve(UpdateChecker.class, "Invalid URL: " + urlFolder + urlFile);
				/*		PeaDialog.showMessage(JREProperties.getMainWindow(), 
								"Invalid URL: " + urlFolder + urlFile, 
								PeaProperties.getVmBridge().translate("error"), 0); */
						return false;
					}
					File file = new File(localFolder + urlFile);
					if (file.exists()) {
						fileSize = file.length();
					} else {
						TestLog.ve(UpdateChecker.class, "File access failed: " + file.getAbsolutePath());
				/*		PeaDialog.showMessage(JREProperties.getMainWindow(), 
								PeaProperties.getVmBridge().translate("no_access") + "\n" + file.getAbsolutePath(), 
								PeaProperties.getVmBridge().translate("error"), 0); */
						// do not return: old language packs (editor, image) did not contain specific bundles
						fileSize = 0;
					}
					HttpURLConnection conn = null;
					try {
						conn = (HttpURLConnection) url.openConnection();
						conn.setRequestMethod("HEAD");
						long webSize = conn.getContentLengthLong();
						if (webSize == -1) {
							TestLog.ve(UpdateChecker.class, "Get size from web file failed: " + url.toString());
							return false;
						}
						if (webSize != fileSize) {
							///// Update found /////
							TestLog.v(UpdateChecker.class, url.toString() 
									+ " - Language update found: locale size: " + fileSize 
									+ ", web size: " + conn.getContentLengthLong());
							return true;
						}
					} catch (IOException e) {
						TestLog.ve(UpdateChecker.class, url.toString() + " - Failed to compare installed file with file in web. " + e.getLocalizedMessage());
				/*		PeaDialog.showMessage(JREProperties.getMainWindow(), 
								PeaProperties.getVmBridge().translate("failed") + "\n" + e.getLocalizedMessage(), 
								PeaProperties.getVmBridge().translate("error"), 0); */
						return false;
					} finally {
						if (conn != null) {
							conn.disconnect();
						}
					}
				}
			}
		}
		return false;
	}
}
