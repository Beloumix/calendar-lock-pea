package cologne.eck.all_peas.control;

/*
 * Peafactory - Production of Password Encryption Archives
 * Copyright (C) 2020  Axel von dem Bruch
 * 
 * This library is free software; you can redistribute it and/or modify it 
 * under the terms of the GNU General Public License as published 
 * by the Free Software Foundation; either version 2 of the License, 
 * or (at your option) any later version.
 * This library is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
 * or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU General Public License for more details.
 * See:  http://www.gnu.org/licenses/gpl-2.0.html
 * You should have received a copy of the GNU General Public License 
 * along with this library.
 */

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;

import cologne.eck.all_peas.cloud.CloudControl;
import cologne.eck.all_peas.cloud.CloudPropertyHandler;
import cologne.eck.all_peas.cloud.StorageProvider;
import cologne.eck.all_peas.files.CloudFilePanel;
import cologne.eck.all_peas.files.FileTypePanel;
import cologne.eck.all_peas.cloud.gui.WebDavLogin;
import cologne.eck.all_peas.cloud.provider.WebDavProvider;
import cologne.eck.all_peas.data.AttachmentHandler;
import cologne.eck.all_peas.data.Attachments;
import cologne.eck.all_peas.data.PeaProperties;
import cologne.eck.all_peas.gui.PswDialogView;
import cologne.eck.all_peas.vm_specific.JREProperties;
import cologne.eck.peafactory.crypto.AlgoParamHandler;
import cologne.eck.peafactory.crypto.CipherStuff;
import cologne.eck.peafactory.crypto.kdf.KeyDerivation;
import cologne.eck.tools.Converter;
import cologne.eck.tools.ExceptionHelper;
import cologne.eck.tools.TestLog;
import cologne.eck.tools.UnexpectedValueException;
import settings.PeaSettings;



public class FileRecipient { // extends SwingWorker<String[], String> { // failedFileNames, message

	/**
	 * Worker that takes selected files from CloudFileePanel, 
	 * and FileTypePanel without GUI. 
	 * Checks existence, access, fileIdentifier, salt and
	 * passwordIdentifier for each file
	 * 
	 * Always call:
	 * getProviderAndFilesToRemove() - remove from CloudFilePanel
	 * and:
	 * getCollectedErrorsToShow() - display error message
	 * after execution
	 */
	public FileRecipient() {
		downloadedSet = CloudControl.getDownloadedInitialFileNames();
	}

	/**
	 * The file names to return
	 */
	private HashSet<String> fileNameSet = new HashSet<String>();
	
	/**
	 * The localized error messages to display:
	 * Every failed file name
	 */
	private StringBuilder collectedErrors = new StringBuilder();
	
	/**
	 * The file names in CloudFilePanel that are to remove
	 * and to add to the invalidPanel
	 */ // TODO implement
	private HashMap<String, HashSet<String>> providerAndFilesToRemove = new HashMap<String, HashSet<String>>();
	
	/**
	 * Already downloaded cloud files
	 */
	private HashSet<String> downloadedSet = null;

	/**
	 * True if at least one file can be decrypted (password identifier).
	 * Check passwordIdentifier until this value is true
	 */
	private boolean validFileFound = false;

	/**
	 * Get or download all selected encrypted file names from selected FileTypePanel:
	 * 1. from manually opened cloud (downloaded in tmp directory in folder cloud name), 
	 * 2. from cloud in properties (downloaded in tmp directory in folder cloud name), 
	 *  - the key is derived with salt from other files to decrpyt the cloud passwords
	 * 3. and from local files. 
	 * Always call:
	 * getProviderAndFilesToRemove() - remove from CloudFilePanel
	 * and:
	 * getCollectedErrorsToShow() - display error message
	 * after execution
	 * 
	 * @return	the encrypted file names
	 */
	public String[] getFileNames() {//doInBackground() throws Exception {
		
		TestLog.v(FileRecipient.class, "Get file names...");

		validFileFound = false;

		// 1. store passwords of manually opened cloud providers
		// 2. get files from CloudFilePanels
		// 3. get local encrypted files

		//>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
		//>>>>>>>>> Get and arrange the file names for salt and algorithms: 
		//>>>>>>>>>		1. store passwords for manually initialized cloud providers
		//>>>>>>>>> 	2. get cloud files from CloudFilePanel
		//>>>>>>>>>		3. get local files
		//>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

		try {
			// 1. get manually initialized providers with keys (open menu PswDialogView)
			// the cloud password was set before
			HashMap<String, byte[]> manuallyAddedMap = WebDavLogin.getPasswordsFromNewProviderDialog();

			if (manuallyAddedMap != null) {
				for (HashMap.Entry<String, byte[]> entry : manuallyAddedMap.entrySet()) {
					String providerName = entry.getKey();
					// get the password:
					byte[] storedPswBytes = entry.getValue();

					if (providerName != null && storedPswBytes != null) {
						if (CipherStuff.getSessionKeyCrypt().getKey() != null) {
							// encrypt as hex
							String hexEncryptedPsw = CloudPropertyHandler.encryptPasswordFromBytes(storedPswBytes);
							if (hexEncryptedPsw != null) {
								// store encrypted as property
								String error = CloudPropertyHandler.storeEncryptedPasswordFromProvider(hexEncryptedPsw,
										providerName, Converter.bytes2hex(KeyDerivation.getSalt()), 
										Converter.bytes2hex(PeaProperties.getAlgoParamString()), null);
								if (error != null) {
									TestLog.e(FileRecipient.class, "Storing cloud password failed for " + providerName);
									TestLog.ve(FileRecipient.class, "Storing cloud password failed for " + providerName, 5);
									collectedErrors.append("\nCan't store password - " + error);
								}
							} else {
								TestLog.ve(FileRecipient.class, "Could not store password for " + providerName, 5);
							}
						} else {
							TestLog.ve(FileRecipient.class, "Could not store password for " + providerName + ", missing session key", 5);
						}
					} else {						
						TestLog.ve(FileRecipient.class, "Missing value... " + providerName, 5);
					}
				}
				manuallyAddedMap.clear();
			}
		} catch (Exception e){
			TestLog.ve(FileRecipient.class, e.getMessage() + " - " + e.toString(), 5);
		}

		// TODO extra Thread per provider (performance)
		// 2. add cloud files from CloudFilePanel to fileNames (except openedFileNames)
		// get the list of Strings (provider name: file name)
		ArrayList <String> cloudFileNames = new ArrayList<String>();
		FileTypePanel ftp = (FileTypePanel)JREProperties.getTypePanel();
		if (ftp == null) {
			TestLog.e(FileRecipient.class, "Unexpected error" );
			TestLog.ve(FileRecipient.class, "FileTypePanel is null ", 5);
		} else {
			CloudFilePanel cfp = ftp.getCloudFilePanel();
			if (cfp == null) {
				TestLog.e(FileRecipient.class, "Unexpected error" );
				TestLog.ve(FileRecipient.class, "CloudFilePanel is null ", 5);
			} else {
				CloudControl cc = cfp.getCloudControl();
				if (cc == null) {
					TestLog.e(FileRecipient.class, "Unexpected error" );
					TestLog.ve(FileRecipient.class, "CloudControl is null ", 5);
				} else {
					// TestLog.v(FileRecipient.class, "algo: " + new String(((FileTypePanel)JREProperties.getTypePanel()).getAlgoParamString())
					// + "\nsalt: " + Converter.bytes2hex(((FileTypePanel)JREProperties.getTypePanel()).getSalt()));
					String[] providerNames = cc.getInitialProviders();
					if (providerNames != null) {
						int provLen = providerNames.length;
						for (int ip = 0; ip < provLen; ip++) {
							String provider = providerNames[ip];
							if (provider == null || provider.length() == 0) {
								TestLog.e(FileRecipient.class, "Missing provider name" );
								TestLog.ve(FileRecipient.class, "Provider name is null ", 5);
								continue;
							}
							TestLog.v(FileRecipient.class, "get cloud files from provider: " + provider);
							String[] cloudFiles = cc.getSelectedInitialFilesOfProvider(provider);
							if (cloudFiles != null) {
								String filesAsList = "Files: ";
								for(String s : cloudFiles) {
									filesAsList += "\n" + s;
								}
								TestLog.v(FileRecipient.class, filesAsList);
								if (cloudFiles != null && cloudFiles.length > 0) {
									if (KeyDerivation.getSalt() == null) {
										new UnexpectedValueException("byte[]", "salt","is null").printDescription();
										return null;
									} else { // salt was set, key derived

										String[] params = CloudPropertyHandler.getProviderLoginParameters(provider);
										// set password in params
										params[1] = CloudPropertyHandler.getPlainPasswordFromProvider(provider, 
												Converter.bytes2hex(KeyDerivation.getSalt()), Converter.bytes2hex(PeaProperties.getAlgoParamString()));
										if (params[1] == null) {
											// this should not happen, even for manually opened files, the password was stored
											TestLog.e(FileRecipient.class, "Missing password for " + provider);
											TestLog.ve(FileRecipient.class, "Cant get password for " + provider, 5);
											// try next provider
											continue;
										}

										String processError = addFilesFromProvider(provider, params, cloudFiles, cloudFileNames);
										if (processError != null) {
											TestLog.e(FileRecipient.class, "Adding files from " + provider + " failed");
											TestLog.ve(FileRecipient.class, processError, 5);
											// already added in method	collectedErrors.append("\n" + processError);
											//return null;
										}
									} // else salt was already set
								} // no cloud files selected
							} // for loop providers
							if (cloudFileNames.size() > 0) { // valid cloud file found
								fileNameSet.addAll(cloudFileNames);
							} else {
								TestLog.v(FileRecipient.class, "No valid file found in cloud...");
							}
						}
					} else {// no providers to add
						TestLog.v(FileRecipient.class, "No provider found");
					}
				}
			}
		}

		// 3. add local files from FileTypePanel:
		// get local files from FileTypePanel, but add to fileNames later
		//	String[] localFiles = cpc.getEncryptedFileTypePanel().getValidFileNames(true, true);
		String[] localFiles = ((FileTypePanel)JREProperties.getTypePanel()).getValidLocalFileNames(true, true);
		if (localFiles != null && localFiles.length > 0) {
			ArrayList<String> localList = new ArrayList<String>();
			// add to existing list
			for (String fileNameToAdd : localFiles) {
				String checkError = checkFile(fileNameToAdd, false);
				if (checkError == null) {
					localList.add(fileNameToAdd);
				} else {
					collectedErrors.append(fileNameToAdd + ": " 
							+ checkError + "\n");
				}
			}
			fileNameSet.addAll(localList);
		}
		if (fileNameSet.isEmpty()) {
			publish(PeaProperties.getVmBridge().translate("no_valid_file_found"), true);
			return null;
		}
		ArrayList<String> list = Converter.hashSetToArrayList(fileNameSet);
		Collections.sort(list);
		return Converter.arrayListToArray(list);
	}
	
	private void publish(String message, boolean isError) {
		PswDialogView.setMessage(message, isError, false);
	}

	/**
	 * Add file from a cloud provider: authorize, 
	 * download and check files
	 * 
	 * @param provider			the provider name
	 * @param params			login parameters(0: user name, 1: password,
	 * 							2: WebDAV base path
	 * @param cloudFiles		the files to check from property list or manually added
	 * @param cloudFileNames	the list to be expanded
	 * 
	 * @return	null if file collection should continue
	 * 			an error message to display otherwise (break the process)
	 */
	String addFilesFromProvider(String provider, String[] params, 
			String[] cloudFiles, ArrayList<String> cloudFileNames) {
		
		if (cloudFiles == null) {
			TestLog.e(FileRecipient.class, "No files to add from provider " + provider);
			return null;
		}
		if (provider == null) {
			new UnexpectedValueException("String", "provider", "is null").printDescription();
			publish(PeaProperties.getVmBridge().translate("unexpected_error"), true);
			return PeaProperties.getVmBridge().translate("unexpected_error")
					+ " (missing provider name)";
		}
		TestLog.v(FileRecipient.class, "Adding " + cloudFiles.length + " cloud files" );
		
		// Check if files are already downloaded:
		ArrayList<String> alreadyDownloadedWithPath = new ArrayList<String>();// with path
		ArrayList<String> toDownloadWithoutPath = new ArrayList<String>();
		if (downloadedSet != null) {			
			for (String fileToCheck : cloudFiles) {
				String tmpFileName = fileToCheck;
				if ( ! fileToCheck.contains(CloudControl.getTmpDir())){					
					tmpFileName = CloudControl.getTmpFileNameForCloudFile(fileToCheck, provider, null);					
				} else {
					TestLog.e(FileRecipient.class, "Already contains temp directory: " + fileToCheck);
				}
				if (downloadedSet.contains(tmpFileName)) {
					alreadyDownloadedWithPath.add(tmpFileName);
					TestLog.v(FileRecipient.class, "Already downloaded: " + tmpFileName);
				} else {
					toDownloadWithoutPath.add(fileToCheck);
					TestLog.v(FileRecipient.class, "To download: " + tmpFileName);
				}
			}
			cloudFiles = Converter.arrayListToArray(toDownloadWithoutPath);
		} else {
			//TestLog.v(FileRecipient.class, "no downloaded set");
		}
		// decrypt cloud password, authorize, download:
		StorageProvider sp = CloudControl.getOpenProvider(provider);
		if (sp == null) {
			 sp = new WebDavProvider(provider);
		}
		WebDavProvider p = null;
		if (sp instanceof WebDavProvider) {
			p = (WebDavProvider) sp;
		} else {
			new UnexpectedValueException("StorageProvider","sp","is not a WebDAV provider").printDescription();
			publish(PeaProperties.getVmBridge().translate("unexpected_error"), true);
			return PeaProperties.getVmBridge().translate("unexpected_error")
					+ " (Cloud provider is not WebDAV provider)";
		}
		TestLog.o(FileRecipient.class, "Connect to " + provider);
		publish(provider + ": " + PeaProperties.getVmBridge().translate("connect_cloud"), false);
		String[] otherParams = {params[2]};

		if (params[1] != null) {
			String authError = p.authorize(params[0], params[1].toCharArray(), otherParams);
			if (authError != null) {
				publish(PeaProperties.getVmBridge().translate("authorization_failed") + " - " + provider, true);
				collectedErrors.append(authError);
				if (validFileFound == false) {
					collectedErrors = collectedErrors.replace(0, collectedErrors.length(),
							authError);
					return authError;// break the process
				} else {
					return null;
				}
			} else {
				publish(PeaProperties.getVmBridge().translate("successfully_authorized") 
						+ " - " +  provider, false);
			}
			if (cloudFiles == null || cloudFiles.length == 0) {
				TestLog.v(FileRecipient.class, "No cloud files to download...");
			} else {
				TestLog.v(FileRecipient.class, "Download " + cloudFiles.length + " files from " + provider);
				for(String s : cloudFiles) TestLog.v(FileRecipient.class, s);
				//cloudFiles should be without temp dir
				String downloadError = p.downloadFiles(cloudFiles, 
						System.getProperty("java.io.tmpdir") + File.separator + provider);

				if (downloadError != null) {
					TestLog.e(FileRecipient.class, downloadError);
					collectedErrors.append(downloadError + "\n");

					String[] errorLines = downloadError.split("\n");
					if (errorLines != null) {
						for (String line : errorLines) {
							TestLog.e(FileRecipient.class, line);
							if (line.contains(PeaProperties.getVmBridge().translate("file_not_found"))){
								// remove file from properties and view:
								if (line.contains(" - ")){
									String fileNameToRemove = line.substring(0, line.indexOf(" - ")).trim();
									TestLog.o(FileRecipient.class, "Remove non-existing file: " + fileNameToRemove);
									//TestLog.o(FileRecipient.class, "!!! " +CloudPropertyHandler.getSingleProperty(provider,  "files"));
									CloudPropertyHandler.removeFileOfProvider(provider, fileNameToRemove,
											Converter.bytes2hex(KeyDerivation.getSalt()), Converter.bytes2hex(PeaProperties.getAlgoParamString()),null);
									((FileTypePanel)JREProperties.getTypePanel()).getCloudFilePanel().getCloudControl().removeInitialFileOfProvider(provider, fileNameToRemove);						
								}
							}
						}
					} else {
						new UnexpectedValueException("String[]", "errorLines", "is null").printDescription();
					}
				}
			}
		} else {
			TestLog.e(FileRecipient.class, "Missing password for " + provider);
			TestLog.ve(FileRecipient.class, "No password for " + provider, 5);
			collectedErrors.append(provider  + ": missing password\n");
			return PeaProperties.getVmBridge().translate("unexpected_error")
					+ " (missing password for cloud provider)";
		}
		// add downloaded files to list:
		if (cloudFiles != null) {
			for (String fileWithoutPath : cloudFiles) {
				if (fileWithoutPath != null) {
					alreadyDownloadedWithPath.add(CloudControl.getTmpFileNameForCloudFile(fileWithoutPath, provider, null));
				} else {
					TestLog.ve(FileRecipient.class, "null cloud file name in array", 5);
				}
			}
		}
		// check  file
		if (alreadyDownloadedWithPath != null) {
			for (String downloadedFileName : alreadyDownloadedWithPath) {
				if (downloadedFileName == null) {
					continue;// error occurred while download
				}
				//		String fileNameToAdd = CloudControl.getTmpFileNameForCloudFile(downloadedFileName, provider, null);
				String checkError = checkFile(downloadedFileName, true);
				if (checkError == null) {
					cloudFileNames.add(downloadedFileName);
				} else {
					// remove file if these errors occurred:
					if (checkError.equals("salt_error") || checkError.equals("is Folder") || checkError.equals("algo_parameter_error")
							|| checkError.equals(PeaProperties.getVmBridge().translate("not_encrypted_with_this_archive"))
							|| checkError.equals(PeaProperties.getVmBridge().translate("unsuitable_ciphertext_len"))) {
						if (providerAndFilesToRemove.containsKey(provider)){
							providerAndFilesToRemove.get(provider).add(downloadedFileName);
						} else {
							HashSet<String> set = new HashSet<String>();
							set.add(downloadedFileName);
							providerAndFilesToRemove.put(provider, set);
						}
					}
					
					if (checkError.equals(PeaProperties.getVmBridge().translate("password_failed"))
							) {
						collectedErrors.append(provider + " - " + downloadedFileName 
								+ "\n" + PeaProperties.getVmBridge().translate("password_failed") +"\n");
						//+ "\n" + PeaProperties.getVmBridge().translate("password_failed") +"\n");
					} else if (new File(downloadedFileName).exists() == false) {
						collectedErrors.append(provider + " - " + downloadedFileName + ": " 
								+ PeaProperties.getVmBridge().translate("download_failed") + "\n");
					} else {//other error: fileIdentifier, salt, password...
						collectedErrors.append(provider + " - " + downloadedFileName + ": " 
								+ checkError + "\n");				
					}
				}
			}
		}
		// check if valid file found:
		if (validFileFound == false) {
			return  provider + ": \n" //+ downloadedFileName
					+ "\n" + PeaProperties.getVmBridge().translate("password_failed") +"\n"
					+ PeaProperties.getVmBridge().translate("try_new_password_or_deselect_file");
		}
		
		if (p != null) {
			//			p.closeSession();
		}
		return null;
	}
	
	/**
	 * Checks 1. the file (access, existence, not folder)
	 * 2. fileIdentifier (not already checked for cloud files)
	 * 	v2: algorithm parameter String
	 * 3. compares salts with previously set salt
	 *    or sets the salt if not already set
	 * 4. checks pswIdentifier only for salt reference file (first valid file)
	 * - derives key if not already derived
	 * Result PeaProperties.getOsBridge().translate("password_failed") is returned, when 
	 * the password fails for the first (reference) file 
	 * and a new key derivation is required
	 * 
	 * @param fileNameWithPath	the file name to check
	 * @param checkSaltAndAlgos	true: check the salt and the algorithm-parameters,
	 * 							check for cloud files, for local files this was already checked
	 * 
	 * @return	an appropriate localized error message if check fails
	 * 			without the file name, 
	 * 			returns PeaProperties.getOsBridge().translate("password_failed") 
	 * 			if new key derivation required, 
	 * 			or null for success
	 */
	String checkFile(String fileNameWithPath, boolean checkSaltAndAlgos)  {
		TestLog.v(FileRecipient.class, "check file: " + fileNameWithPath);

		if (fileNameWithPath == null) {
			TestLog.e(FileRecipient.class, "Missing file name");
			TestLog.ve(FileRecipient.class, "Missing file name", 5);
			return PeaProperties.getVmBridge().translate("unexpected_error");
		}
		File file = new File(fileNameWithPath);
		//check file existence and access:
		if (file.isDirectory()) {
			TestLog.e(FileRecipient.class, "Is directory " + " - " + fileNameWithPath);
			return "is Folder";
		}
		if ( ! file.exists()) {
			TestLog.e(FileRecipient.class, "Does not exist" + " - " + fileNameWithPath);
			return PeaProperties.getVmBridge().translate("file_not_found");
		}
		if ( ! file.canWrite()) {
			TestLog.e(FileRecipient.class, "No write access" + " - " + fileNameWithPath);
			return PeaProperties.getVmBridge().translate("no_access");
		}
		if ( file.length() == 0) {
			TestLog.e(FileRecipient.class, "Empty file" + " - " + fileNameWithPath);
			return PeaProperties.getVmBridge().translate("empty_file");
		}
/*		if ( ! PeaControl.getDialog().checkFormat(file, true, false, false)){ // simple check			
			TestLog.e(FileRecipient.class, "Unsuitable format" + " - " + fileName);
			return PeaProperties.getVmBridge().translate("unsuitable_format");
		}*/
		// 1. Padding 
		// 2. MAC 
		// 3. pswIdentifier (an plainText)
		// 4. encrypted pswIdentifier (8 Bytes)
		// 5. Nonce/IV 
		// 6. Salt
		// (7)	v2:	algorithm parameter string
		// (8) / 7. fileIdentifier
		int fileIdLen = Attachments.getFileIdentifierSize();
		int algoParamSize = AlgoParamHandler.getAlgoParamStringSize();
		int saltLen = KeyDerivation.getSaltSize();
		int pswIdLen = Attachments.getPswIdentifierSize();
		
		// check version with file identifier
		int version = -1;
		byte[] fileId = null;
		try {
			fileId = Attachments.getEndBytesOfFile( 
					fileNameWithPath, 
					fileIdLen);
		} catch (IOException e1) {
			TestLog.ve(this.getClass(), "No access to " + fileNameWithPath + " - " + e1.getLocalizedMessage(),  5);
			return e1.getLocalizedMessage();
		}	

		if (Arrays.equals(fileId, AttachmentHandler.getVersion1ID())) {
			version = 1;
		} else if (Arrays.equals(fileId, AttachmentHandler.getVersion2ID())) {
			version = 2;
		} else {
			if (PeaSettings.getKeyFileProperty() == true) {
				// randomly generated and set identifier
				if (Arrays.equals(fileId, Attachments.getFileIdentifier())) {//  fileId.equals(Attachments.getFileIdentifier())) {
					version = 0;
				} else {
					TestLog.e(this.getClass(), "file identifier failed: " + new String(fileId));
					TestLog.e(FileRecipient.class, "File identifier failed: " + fileNameWithPath);
					return PeaProperties.getVmBridge().translate("not_encrypted_with_this_archive");
				}
			} else {
				TestLog.e(FileRecipient.class, "File identifier failed: " + fileNameWithPath);
				TestLog.ve(this.getClass(), "file identifier failed: " + new String(fileId));
				return PeaProperties.getVmBridge().translate("not_encrypted_with_this_archive");
			}
		}
		
		int attachmentLen = fileIdLen 
				+ saltLen
				+ Attachments.getNonceSize() 
				+ pswIdLen;
		if (version == 2 || version == 0) {
			attachmentLen += algoParamSize;
		}
		// check the size of the cipher text:
		if (file.length() < attachmentLen ){
			TestLog.e(FileRecipient.class, "Wrong file size " + " - " + fileNameWithPath);
			return PeaProperties.getVmBridge().translate("unsuitable_ciphertext_len");
		}		
		if (PeaProperties.getFileType().equals("file")) {// other attachment scheme for file
			new UnexpectedValueException("String", "fileType", "is file but not implemented").printDescription();
		}
		byte[] end = null;
		try {
			end = Attachments.getEndBytesOfFile( 
					fileNameWithPath, 
					attachmentLen);			
		} catch (IOException e) {
			TestLog.e(FileRecipient.class, new ExceptionHelper(e).getInfos() + " - " + fileNameWithPath);
			return e.getLocalizedMessage();
		} catch (Exception e) {
			TestLog.e(FileRecipient.class, new ExceptionHelper(e).getInfos() + " - " + fileNameWithPath);
			return e.getLocalizedMessage();
		}
		if (checkSaltAndAlgos == true) {
			// salt must have been set before:
			byte[] setSalt = KeyDerivation.getSalt();
			if (setSalt == null) {
				TestLog.ve(this.getClass(), "salt was not set.",  5);
				return PeaProperties.getVmBridge().translate("unexpected_error");
			}			
			// check the salt:
			int saltStartIndex = end.length - Attachments.getFileIdentifierSize() 
					- saltLen;
			if (version == 2 || version == 0) {
				saltStartIndex -= algoParamSize;
			} 
			byte[] fileSalt = new byte[saltLen];
			System.arraycopy(end,  saltStartIndex,  fileSalt,  0,  saltLen);
			if ( ! Arrays.equals(fileSalt,  setSalt)) {
				TestLog.e(this.getClass(), "Different salt found in " + fileNameWithPath);
				TestLog.ve(this.getClass(), "Different salt found: \n"
						+ Converter.bytes2hex(setSalt) + " != " + Converter.bytes2hex(fileSalt)
						+"\nversion: " + version, 5);
				// remove from properties: 
				String fileNameOnly = CloudControl.getFileNameFromTmpFile(fileNameWithPath);
				String providerName = CloudControl.getProviderNameFromTmpFile(fileNameWithPath);
				if (fileNameOnly != null) {
					String propError = CloudPropertyHandler.removeFileOfProvider(providerName, fileNameOnly, 
							Converter.bytes2hex(setSalt), Converter.bytes2hex(PeaProperties.getAlgoParamString()), null);
					if (propError != null) {
						TestLog.ve(FileRecipient.class, propError, 3);
					}
				}
				return "salt_error";
			}
			if (version == 2 || version == 0) {
				// check algoParams:
				byte[] setAlgoParams = PeaProperties.getAlgoParamString();
				byte[] fileAlgoParam = new byte[algoParamSize];
				int algoStartIndex = end.length - Attachments.getFileIdentifierSize() 
						- algoParamSize;
				System.arraycopy(end,  algoStartIndex,  fileAlgoParam,  0,  algoParamSize);
				if ( ! Arrays.equals(setAlgoParams, fileAlgoParam)){
					TestLog.e(this.getClass(), "Different algorithms found in " + fileNameWithPath);
					TestLog.ve(this.getClass(), "Different algorithms-parameters found: " +
							Converter.bytes2hex(setAlgoParams) + " != " + Converter.bytes2hex(fileAlgoParam));
					TestLog.ve(this.getClass(), "Different algorithms-parameters found in " + fileNameWithPath,  5);
					// remove from properties: 
					String fileNameOnly = CloudControl.getFileNameFromTmpFile(fileNameWithPath);
					String providerName = CloudControl.getProviderNameFromTmpFile(fileNameWithPath);
					if (fileNameOnly != null) {
						String propError = CloudPropertyHandler.removeFileOfProvider(providerName, fileNameOnly, 
								Converter.bytes2hex(setSalt), Converter.bytes2hex(setAlgoParams), null);
						if (propError != null) {
							TestLog.ve(FileRecipient.class, propError, 3);
						}
					}
					return "algo_parameter_error";
				}
			}
		}
		if (CipherStuff.getCipherAlgo() == null) {
			TestLog.e(FileRecipient.class, "Missing cipher algo to check password identifier");
			TestLog.ve(FileRecipient.class, "Missing cipher algo to check password identifier", 5);
			return null;
		}
		// get password identifier: 
		byte[] pswIdentifier = new byte[pswIdLen];
		System.arraycopy(end,  0,
				pswIdentifier,  0,  pswIdLen);
		// get nonce from cipher text:
		byte[] nonce = new byte[ Attachments.getNonceSize()];
		System.arraycopy(end,  pswIdLen,  
				nonce,  0,  nonce.length);
		if (CipherStuff.getSessionKeyCrypt().getKey() == null) {
			TestLog.ve(FileRecipient.class, "No key derived...",  5);
			return PeaProperties.getVmBridge().translate("unexpected_error");
		}
		
		boolean pswResult = Attachments.checkPswIdentifier(
				pswIdentifier, CipherStuff.getSessionKeyCrypt().getKey(), nonce);
		if (pswResult == true) {
			validFileFound = true;
			return null;
		} else {
			TestLog.e(this.getClass(),  "password failed for " + fileNameWithPath);	
			String passwordFailedMessage = PeaProperties.getVmBridge().translate("password_failed");
			if (PeaProperties.getKeyFileFlag() > 0) { // default or chosen key file
				passwordFailedMessage = PeaProperties.getVmBridge().translate("key_file") + " / " + passwordFailedMessage;
			}
			return passwordFailedMessage;//PeaProperties.getVmBridge().translate("password_failed");
		}
	}
	
	/**
	 * Get all collected errors to display in a dialog
	 * after this worker was performed
	 * 
	 * @return	collected errors as String 
	 * 			or null
	 */
	public String getCollectedErrorsToShow() {
		if (collectedErrors.length() > 0) {
			return new String(collectedErrors);
		} else { 
			return null;
		}
	}

	/**
	 * Get cloud providers and file names to remove from
	 * CloudFilePanel after this worker was performed. 
	 * These are invalid files only.
	 * 
	 * @return	map of providers and file names to remove
	 * 			from CloudFilePanel after execution
	 */
	public HashMap<String, HashSet<String>> getProviderAndFilesToRemove() {
		return providerAndFilesToRemove;
	}
}
