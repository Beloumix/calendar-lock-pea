package cologne.eck.all_peas.control;

import java.io.File;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Iterator;

import cologne.eck.all_peas.data.PeaProperties;
import cologne.eck.all_peas.gui.PeaDialog;
import cologne.eck.all_peas.gui.PswDialogView;
import cologne.eck.tools.Converter;
import cologne.eck.tools.ReadResources;
import cologne.eck.tools.TestLog;
import cologne.eck.tools.UnexpectedValueException;
import cologne.eck.tools.WriteResources;
import settings.PeaSettings;

/*
 * PeaFactory - Production of Password Encryption Archives
 * Copyright (C) 2015  Axel von dem Bruch
 * 
 * This library is free software; you can redistribute it and/or modify it 
 * under the terms of the GNU General Public License as published 
 * by the Free Software Foundation; either version 2 of the License, 
 * or (at your option) any later version.
 * This library is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
 * or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU General Public License for more details.
 * See:  http://www.gnu.org/licenses/gpl-2.0.html
 * You should have received a copy of the GNU General Public License 
 * along with this library.
 */

public class PathFileManager {
	
	/**
	 * File name of the file containing previously opened files
	 */
	private final static String PATH_FILE_NAME = PeaSettings.getJarFileName() + ".path";

	/**
	 * Checks the path file (file PEA's directory, name of PEA, with extension"path")
	 * for previously opened file names. This method should be called in EDT.
	 * 
	 * @return	the names of the previously opened files (as an array of String)
	 * 			or null if the file does not exist, is empty or not accessible
	 */
	public final static String[] getFileNamesFromPathFile() {
		File file = new File(PATH_FILE_NAME);
		if (! file.exists() ) {
			//TestLog.e(PathFileManager.class, "PeaControl: no path file specified");
			return null;
		}
		if (! file.canRead() ) {
			TestLog.e(PathFileManager.class, "PeaControl: can not read path file " + file.getName() );
			Object win = null;
			if (PswDialogView.getView().isVisible()){
				win = PswDialogView.getView();
			} else {
				win = PeaControl.getDialog().getLockFrame();
			}
			PeaDialog.showMessage(win, 
					PeaProperties.getVmBridge().translate("path_file_access_error")
					//"There is are file containing names of potentially encrypted files,\n" +
					//" but no access to it: \n"
					+ PATH_FILE_NAME, 
					"Info", 1);
			return null;
		}
		
		byte[] pathBytes = ReadResources.readExternFile( file.getName() );
		if (pathBytes == null) {
			TestLog.e(PathFileManager.class, "PeaControl: path file does not contain any file name: " + file.getPath() );
			return null;
		}
		String pathString = new String(pathBytes, PeaProperties.getCharset());

		String[] pathNames = pathString.split("\n");
		// remove null names
		ArrayList<String> list = new ArrayList<String>();
		for (String pn : pathNames) {
			if (pn != null && pn.trim().length() > 1) { // root / in Unix not allowed
				Path p = null;
				try {
					p = Paths.get(pn);
				} catch (java.nio.file.InvalidPathException e) {
					TestLog.e(PathFileManager.class, "Invalid file name (for this OS): " + pn);
					continue;
				}
				// only absolute paths are allowed
				if (p.isAbsolute()) {
					// check if file exists
					File f = p.toFile();
					if (f.exists() == true) {
						list.add(pn);
					}					
				} else {
					TestLog.e(PathFileManager.class, "Invalid file name (for this OS): " + pn);
				}
			}
		}
		return Converter.arrayListToArray(list);
	}

	/**
	 * Add selected files to the path file
	 * (Checks if files already exists in path file
	 * and ask if not).
	 * This method should be called in EDT.
	 * 
	 * @param selectedFileNames		the file names to add to the path file
	 * 								as an array of Strings
	 * 
	 * @return	the added file names or null
	 */
	protected final static String[] addFileNamesToPathFile(String[] selectedFileNames) {
		
		ArrayList<String> newPathFiles = new ArrayList<>(Arrays.asList(selectedFileNames));
		ArrayList<String> updatedList =  addFileNamesToPathFile(newPathFiles);
		if (updatedList == null) {
			return null;
		} else {
			return updatedList.toArray(new String[0]);
		}
	}

	/**
	 * Checks if current encryptedFileName is in path file.
	 * If not: asks to remember the file and store in path file.
	 * This method should be called in EDT.
	 * 
	 * @param newFileName	the file name to check
	 * 
	 * @return				true if the path file was updated with 
	 * 						new file name
	 */
	public final static boolean addFileNameToPathFile(String newFileName) {
		
		HashSet<String> newSet = new HashSet<String>();
		newSet.add(newFileName);
		ArrayList<String> added = addFileNamesToPathFile(newSet, true);//newFile);
		if (added != null && added.size() == 1) {
			return true;
		} else {
			return false;
		}
	} 
	
	/**
	 * Remove one file name from path file if it exist
	 * 
	 * @param fileNameToRemove	the file name to remove
	 */
	public static void removeFileName(String fileNameToRemove) {
		File file = new File(PATH_FILE_NAME);
		if (! file.exists() ) {
			//TestLog.e(PathFileManager.class, "PeaControl: no path file specified");
			return;
		}
		if (! file.canRead() ) {
			TestLog.e(PathFileManager.class, "PeaControl: can not read path file " + file.getName() );
			Object win = null;
			if (PswDialogView.getView().isVisible()){
				win = PswDialogView.getView();
			} else {
				win = PeaControl.getDialog().getLockFrame();
			}
			PeaDialog.showMessage(win, 
					PeaProperties.getVmBridge().translate("path_file_access_error")
					//"There is are file containing names of potentially encrypted files,\n" +
					//" but no access to it: \n"
					+ PATH_FILE_NAME, 
					"Info", 1);
			return;
		}

		byte[] pathBytes = ReadResources.readExternFile( file.getName() );
		if (pathBytes == null) {
			TestLog.e(PathFileManager.class, "PeaControl: path file does not contain any file name: " + file.getPath() );
			return;
		}
		String pathString = new String(pathBytes, PeaProperties.getCharset());
		int indexOfFile = pathString.indexOf(fileNameToRemove);
		if (indexOfFile == -1) {
			// do nothing
			TestLog.e(PathFileManager.class, "PathFileManager: path file does not contain " + fileNameToRemove
			+ " (can't remove)");
		} else {
			pathString = pathString.substring(0, indexOfFile)
					.concat(pathString.substring(indexOfFile + fileNameToRemove.length()));
		}
		// delete double new lines
		pathString = pathString.replace("\n\n", "\n");
		pathString = pathString.replace("\r\n\r\n", "\r\n");// Windows
		boolean writeResult = WriteResources.writeText(pathString, PATH_FILE_NAME);
		if (writeResult == false) {
			new UnexpectedValueException("boolean","writeResult","is false when removing " +fileNameToRemove).printDescription();
		}
	}	
	
	
	/**
	 * Checks if file names are already stored in the path file.
	 * If not: asks to remember the files and store in path file.
	 * This method should be called in EDT.
	 * 
	 * @param newFileNames	the file names to check
	 * @param askToAdd	true: ask to add file, false: add without confirmation - 
	 * 					when closed by timeout, no dialog should stop the closing process
	 * 
	 * @return				the added file names to
	 * 						the path files or null if no file name was added
	 */
	public final static ArrayList<String> addFileNamesToPathFile(HashSet<String> newFileSet,
			boolean askToAdd) {

		if (newFileSet == null || newFileSet.isEmpty()) {
			//TestLog.e(PathFileManager.class, "Set is empty");
			return null;
		}
		// return value and list of files to add to path file
		ArrayList<String> updatedFileNames = new ArrayList<String>();
		// the String to add to the existing path file content
		StringBuilder builder = new StringBuilder();

		// the name of the path file where file names are stored to remember:
		String pathFileName = PeaSettings.getJarFileName() + ".path";
		// check the file
		File file = new File(pathFileName);		
		if (! file.exists() ) { // WriteResources creates new file			
			Iterator<String> fileIterator = newFileSet.iterator();
			while (fileIterator.hasNext()) {
				// check each new file name:	
				String fileName = fileIterator.next();
				if (fileName == null) {
					continue;
				}
				// File from cloud, stored in default temp directory:
				if (fileName.startsWith(System.getProperty("java.io.tmpdir"))){
					continue;
				}
				// path stored inside the PEA
				if (fileName.equals(PeaSettings.getExternalFilePath() )) {
					// no need to remember
				} else if (fileName.equals("resources" + File.separator + "text.lock")){
					// no need to remember			
				} else {
					updatedFileNames.add(fileName.trim());
				}
			}			
		} else {	// path file exists		
			if (! file.canRead() || ! file.canWrite() ) {
				TestLog.e(PathFileManager.class, "PeaControl: can not read and write path file " + file.getName() );
				Object win = null;
				if (PswDialogView.getView().isVisible()){
					win = PswDialogView.getView();
				} else {
					win = PeaControl.getDialog().getLockFrame();
				}
				if (askToAdd == true) {
					PeaDialog.showMessage(win, 
							PeaProperties.getVmBridge().translate("path_file_access_error")
							//"There is are file containing names of potentially encrypted files,\n" +
							//" but no access to it: \n"
							+ PATH_FILE_NAME, 
							"Info", 1);
				}
				return null;				
			}

			// the current content of the path file:
			byte[] pathFileContent = ReadResources.readExternFile(pathFileName);
			if (pathFileContent == null) {
				pathFileContent = " ".getBytes(PeaProperties.getCharset());
			}
			// the current content as String
			String pathNamesString = new String(pathFileContent, PeaProperties.getCharset());
			builder.append(pathNamesString + "\n");
			// file names are separated by \n: String array of current file names
			// in path file
			String[] pathNames = pathNamesString.split("\n");
			// convert into ArrayList to use contains function
			ArrayList<String> oldFileNames = new ArrayList<>(Arrays.asList(pathNames));
			Iterator<String> fileIterator = newFileSet.iterator();
			while (fileIterator.hasNext()) {
				String fileName = fileIterator.next();
			// check each new file name:
				if (fileName == null) {
					continue;
				}
				// File from cloud, stored in default temp directory:
				if (fileName.startsWith(System.getProperty("java.io.tmpdir"))){
					continue;
				}
				// path stored inside the PEA
				if (fileName.equals(PeaSettings.getExternalFilePath() )) {
					// no need to remember
				} else if (fileName.equals("resources" + File.separator + "text.lock")){
					// no need to remember			
				} else {		
					// check against every file name of path file
					if (oldFileNames.contains(fileName)){
						continue;
					} else {
						updatedFileNames.add(fileName.trim());
					}
				}
			}
		}
		if (updatedFileNames != null && updatedFileNames.size() > 0) { // at least one file was not in path file

			Object win = null; // the owner of the JOptionPane
			if (PswDialogView.getView() != null && PswDialogView.getView().isVisible()){
				win = PswDialogView.getView();
			} else {
				if (PeaControl.getDialog() != null && PeaControl.getDialog().getLockFrame() != null) {
					win = PeaControl.getDialog().getLockFrame();
				}
			}
			StringBuilder fileNamesToUpdate = new StringBuilder();
			for (String fileNameToUpdate : updatedFileNames) {
				fileNamesToUpdate.append(fileNameToUpdate + "\n");
			}
			int n = 0;
			if (askToAdd == true) {
				n = PeaDialog.showQuestionWithScollPane(win, 
						PeaProperties.getVmBridge().translate("remember_files") +"\n"
						+ PeaProperties.getVmBridge().translate("path_file_storage_info")
						+ pathFileName + "\n\n"
						+ fileNamesToUpdate,  
						"?", 
						0, //JOptionPane.YES_NO_OPTION, 
						3 //QUESTION_MESSAGE	
						);
			}
			if (n == 0){ // ok				
				//store in path file:
				int updatedLen = updatedFileNames.size();
				//String newContent = "";
				for (int i = 0; i < updatedLen; i++) {
					// separate by \n and add to current list
					builder.append(updatedFileNames.get(i) + "\n");
					//pathNamesString += "\n" + updatedFileNames.get(i);
				}

				if (builder.length() > 1) { // 
					byte[] newContent = new String(builder).getBytes(PeaProperties.getCharset());
					WriteResources.write(newContent, pathFileName, null);
				}

			} else { // cancel,close
				return null;
			}
		} else {
			// no new file found
			return null;
		}
		return updatedFileNames;		
	}

	/**
	 * Checks if file names are already stored in the path file.
	 * If not: asks to remember the files and store in path file.
	 * This method should be called in EDT.
	 * 
	 * @param newFileNames	the file names to check
	 * 
	 * @return				the added file names to
	 * 						the path files or null if no file name was added
	 */
	public final static ArrayList<String> addFileNamesToPathFile(ArrayList <String> newFileNames) {

		if (newFileNames == null) {
			return null;
		}
		// convert to HashSet to avoid duplicates:
		HashSet<String> newFileSet = new HashSet<String>(newFileNames);
		return addFileNamesToPathFile(newFileSet, true);		
	} 
	
	/**
	 * @return the pathFileName
	 */
	public static String getPathFileName() {
		return PATH_FILE_NAME;
	}
}
