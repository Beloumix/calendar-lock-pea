package cologne.eck.all_peas.control;


/*
 * Peafactory - Production of Password Encryption Archives
 * Copyright (C) 2015  Axel von dem Bruch
 * 
 * This library is free software; you can redistribute it and/or modify it 
 * under the terms of the GNU General Public License as published 
 * by the Free Software Foundation; either version 2 of the License, 
 * or (at your option) any later version.
 * This library is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
 * or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU General Public License for more details.
 * See:  http://www.gnu.org/licenses/gpl-2.0.html
 * You should have received a copy of the GNU General Public License 
 * along with this library.
 */

/**
 * Parent class of all pea frames except image pea. 
 */


import java.io.File;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;

import org.bouncycastle.crypto.BlockCipher;
import org.bouncycastle.crypto.Digest;

import cologne.eck.all_peas.cloud.CloudControl;
import cologne.eck.all_peas.cloud.CloudPropertyHandler;
import cologne.eck.all_peas.cloud.gui.UploadWorker;
import cologne.eck.all_peas.data.PeaProperties;
import cologne.eck.all_peas.vm_specific.JREProperties;
import cologne.eck.peafactory.crypto.AuthenticatedEncryption;
import cologne.eck.peafactory.crypto.CipherStuff;
import cologne.eck.peafactory.crypto.HashStuff;
import cologne.eck.tools.ReadResources;
import cologne.eck.tools.TestLog;
import cologne.eck.tools.UnexpectedValueException;
import cologne.eck.tools.WriteResources;


/**
 * Content related utilities. 
 * 
 * @author Axel von dem Bruch
 *
 */
public abstract class ContentUtilities {

	/**
	 * Encrypt and store the current content of a file 
	 * whether as ciphertext or as plaintext with a key. 
	 * The key is then stored encrypted as session key. 
	 * 
	 * @param keyMaterial	the key to encrypt: must be null if cipherBytes are not null
	 * @param plainBytes	the content to encrypt and to store if cipherBytes is null
	 * @param cipherBytes	the encrypted content to store if plainBytes is null
	 * @param fileName		the file to store the encrypted content with absolute path
	 * 
	 * @return	true if content was saved successfully
	 */
	public final static boolean saveContent(
			byte[] keyMaterial, byte[] plainBytes, byte[] cipherBytes, 
			String fileName, boolean encryptBySessionKey) {

		if (new File(fileName).exists() == false) {
			TestLog.e(ContentUtilities.class, "ContentUtilities save: file does not exist: " + fileName);
			new UnexpectedValueException("String", "fileName", "does not eist: " + fileName).printDescription();
			return false;
		}
		if (new File(fileName).isFile() == false) {
			TestLog.e(ContentUtilities.class, "ContentUtilities save: not file " + fileName);
			new UnexpectedValueException("String", "fileName", "is not a name of a file: " + fileName).printDescription();
			return false;
		}
		if (plainBytes != null && cipherBytes != null) {
			TestLog.e(ContentUtilities.class, "ContentUtilities save: One parameter too much " + fileName);
			new UnexpectedValueException("byte[]", "cipherBytes and plainBytes", "both not null for file: " + fileName).printDescription();
			return false;
		}
		if (plainBytes == null && cipherBytes == null) {
			TestLog.e(ContentUtilities.class, "ContentUtilities save: Required parameter is null: " + fileName);
			new UnexpectedValueException("byte[]", "cipherBytes and plainBytes", "both null for file: " + fileName).printDescription();
			return false;
		}
		if (cipherBytes != null && keyMaterial != null) {
			TestLog.e(ContentUtilities.class, "ContentUtilities save: One parameter too much " + fileName);
			new UnexpectedValueException("byte[]", "cipherBytes and keyMaterial", "both not null for file: " + fileName).printDescription();
			return false;
		}

		if (cipherBytes == null) {
			cipherBytes = CipherStuff.getInstance().encrypt(plainBytes, 
				keyMaterial, // keyMaterial is overwritten
				encryptBySessionKey, null ); // key is stored encrypted
		}
		if (cipherBytes == null) {
			TestLog.e(ContentUtilities.class, "Decryption error");
			TestLog.ve(ContentUtilities.class, "Missing ciphertext", 5);
			return false;
		}

		boolean success = WriteResources.write(cipherBytes,fileName, null);
		if (success == false) {
			new UnexpectedValueException("boolean", "success", 
					"is false for writing: " + fileName).printDescription();
		}
		
		if (fileName.contains(CloudControl.getTmpDir())) {
			// save in cloud:
			saveTmpFileInCloud(fileName);
		}
		return success;
	}
	
	/**
	 * Save one single backup file of a cloud file
	 * that is stored in temporary directory
	 * 
	 * @param tmpFileName	the name of the backup file in tmp folder to store
	 */
	public static void saveTmpFileInCloud(String tmpFileName) {
		if (tmpFileName.contains(CloudControl.getTmpDir())) {
			// save in cloud:
			String providerName = CloudControl.getProviderNameFromTmpFile(tmpFileName);
			File[] fileArray = {new File(tmpFileName)};
			TestLog.v(ContentUtilities.class, "Upload " + tmpFileName + "... ");
			new UploadWorker(JREProperties.getMainWindow(), 
					providerName, 
					null, 
					fileArray, 
					null, false).execute();			
		} else {
			TestLog.e(ContentUtilities.class, "ContentUtilities saveTmpFileInCloud: " + tmpFileName
					+ " - is not a cloud file backup");
		}
	}

	/**
	 * Re-encrypt files: decrypt with old key, encrypt with new key.
	 * This can not be performed in RAM for large files, therefore 
	 * check if there are files larger than 256MB and warn/ask user
	 * 
	 * @param fileNames	file names to re-encrypt
	 * @param oldKey	the old key to decrypt (a copy is used)
	 * @param newKey	the new key to encrypt (a copy is used)
	 * @param oldCipher		the cipher for the old key
	 * @param newCipher		the hash algo to check old password identifier 
	 * @param oldHash		the hash algo to create new password identifier
	 * @param newHash		the cipher for the new key 
	 * @param reencryptOnDisk	true: decrypt large files on disk, then encrypt due
	 * 		to heap error (file.lentgh > 256 MB) - check file length before
	 * 		and warn user, that files are unencrypted on disk for a while
	 * 
	 * @return	null for success, a localized error message to show otherwise
	 */
	public static final String reencrypt(String[] fileNames, byte[] oldKey, byte[] newKey,
			BlockCipher oldCipher, BlockCipher newCipher, Digest oldHash, Digest newHash, 
			boolean reencryptOnDisk) {
		if (fileNames == null) {
			TestLog.e(ContentUtilities.class, "No file to re-encrypt");
			TestLog.ve(ContentUtilities.class, "file names to re-encrypt is null", 5);
			return PeaProperties.getVmBridge().translate("file_not_found");
		}
		
		if (oldKey == null) {
			TestLog.e(ContentUtilities.class, "No key to decrypt");
			TestLog.ve(ContentUtilities.class, "old key is null", 5);
			return PeaProperties.getVmBridge().translate("unexpected_error") + " (missing old key)";
		}
		if (newKey == null) {
			TestLog.e(ContentUtilities.class, "No  key to encrypt");
			TestLog.ve(ContentUtilities.class, "new key is null", 5);
			return PeaProperties.getVmBridge().translate("unexpected_error") + " (missing new key)";
		}
		if (Arrays.equals(oldKey,  newKey)) {
			TestLog.e(ContentUtilities.class, "Re-encryption key and previous key are equal");
			TestLog.ve(ContentUtilities.class, "same keys", 5);
			return null;//"re-encryption with same key, break re-encryption...";
		}
		if (oldCipher == null || newCipher == null) {
			if (oldCipher == null) {
				TestLog.e(ContentUtilities.class, "No cipher to decrypt");
			}
			if (newCipher == null) {
				TestLog.e(ContentUtilities.class, "No cipher to encrypt");
			}
			TestLog.ve(ContentUtilities.class, "cipher algo is null", 5);
			return PeaProperties.getVmBridge().translate("unexpected_error") + " (missing cipher)";
		}
		BlockCipher currentCipher = CipherStuff.getCipherAlgo();
		StringBuilder errorBuilder = new StringBuilder();
		AuthenticatedEncryption ae = CipherStuff.getCipherMode();
		for (String fileNameWithPath : fileNames) {
			if (fileNameWithPath == null) {
				errorBuilder.append(PeaProperties.getVmBridge().translate("unexpected_error") + " - file name is null \n");
				continue;
			}
			String fileName = null;
			String dirName = null;
			try {
				if (Paths.get(fileNameWithPath).getFileName() != null){
					Path filePath = Paths.get(fileNameWithPath).getFileName();
					if (filePath != null) {
						fileName = filePath.toString();
					} else {
						TestLog.ve(ContentUtilities.class, "Missing Path", 5);
						errorBuilder.append(PeaProperties.getVmBridge().translate("unexpected_error") + "  \n");
						continue;
					}
				} else {
					TestLog.e(ContentUtilities.class, "Can't get file name: " + fileName);
					errorBuilder.append(PeaProperties.getVmBridge().translate("unexpected_error") + " - file name is null \n");
					continue;
				}
				if (Paths.get(fileNameWithPath).getParent() != null){
					Path dirPath = Paths.get(fileNameWithPath).getParent();
					if (dirPath != null) {
						dirName = dirPath.toString();
					} else {
						TestLog.ve(ContentUtilities.class, "Missing Path", 5);
						errorBuilder.append(PeaProperties.getVmBridge().translate("unexpected_error") + "  \n");
						continue;
					}
				}
			} catch (Exception e) {
				errorBuilder.append(PeaProperties.getVmBridge().translate("unexpected_error") + " - file name is invalid: "+ fileNameWithPath + "\n");
				continue;
			}

			long fileLen = new File(fileNameWithPath).length();
			if (fileLen >= 1024 * 1024 * 256) { // > 256 MB a heap error may occur
				if (reencryptOnDisk == true) {
					CipherStuff.setCipherAlgo(oldCipher);
					String[] names = {fileNameWithPath};					
					String[] decryptError = ae.decryptFiles(names, Arrays.copyOf(oldKey, oldKey.length), false, null);

					if (decryptError[0] != null) {
						if (decryptError[0].equals(PeaProperties.getVmBridge().translate("data_integrity_violated"))){					
							TestLog.e(ContentUtilities.class, PeaProperties.getVmBridge().translate("data_integrity_violated")
									+ ": " + fileNameWithPath + "\n"
									+ PeaProperties.getVmBridge().translate("integrity_check_failed_message"));
							// The file was decrypted
							return 
									fileNameWithPath + ":\n" + PeaProperties.getVmBridge().translate("data_integrity_violated")
									+"\n" + PeaProperties.getVmBridge().translate("integrity_check_failed_message")
									+ "\n\n" + PeaProperties.getVmBridge().translate("warning") + ":\n " 
									+ fileNameWithPath + " - " + PeaProperties.getVmBridge().translate("unencrypted") + "\n"
									+ PeaProperties.getVmBridge().translate("open_file") + " " + fileNameWithPath;
						}
					}
					CipherStuff.setCipherAlgo(newCipher);
					String[] encryptError = ae.encryptFiles(names, Arrays.copyOf(newKey,  newKey.length), false, null, null);
					if (encryptError[0] != null) {
						errorBuilder.append( encryptError[0] + ": " + fileNameWithPath);
						continue;
					}
				} else {
					errorBuilder.append(PeaProperties.getVmBridge().translate("file_too_large") 
							+ fileNameWithPath + "\n");
					continue;
				}
				continue;
			}
			byte[] oldEncrypted = ReadResources.readExternFile(fileNameWithPath);
			if (oldEncrypted == null) {
				TestLog.e(ContentUtilities.class, "Read failed: " + fileNameWithPath);
				TestLog.ve(ContentUtilities.class, ReadResources.getLastErrorMessage(), 5);
				errorBuilder.append(//PeaProperties.getVmBridge().translate("unexpected_error") +
						//fileNameWithPath + ": " + 
						ReadResources.getLastErrorMessage() + "\n");
				continue;
			}

			CipherStuff.setCipherAlgo(oldCipher);
			HashStuff.setHashAlgo(oldHash);
			TestLog.v(ContentUtilities.class, "Temporarely set cipher to: " + CipherStuff.getCipherAlgo().getAlgorithmName()
					+ " and Hash to: " + HashStuff.getHashAlgo().getAlgorithmName());
			byte[] plainText = CipherStuff.getInstance().decrypt( oldEncrypted, Arrays.copyOf(oldKey, oldKey.length), false);
			if (plainText == null) {
				TestLog.e(ContentUtilities.class, "Decryption failed: " + fileNameWithPath);
				TestLog.ve(ContentUtilities.class, CipherStuff.getErrorMessage(), 5);
				errorBuilder.append(PeaProperties.getVmBridge().translate("decryption_failed") + ": "+ fileNameWithPath + "\n");
				continue;
			}
			CipherStuff.setCipherAlgo(newCipher);
			HashStuff.setHashAlgo(newHash);
			TestLog.v(ContentUtilities.class, "Reset cipher to: " + CipherStuff.getCipherAlgo().getAlgorithmName()
					+ " and Hash to: " + HashStuff.getHashAlgo().getAlgorithmName());
			byte[] newEncrypted = CipherStuff.getInstance().encrypt(plainText, Arrays.copyOf(newKey, newKey.length), false, null);
			if (newEncrypted == null) {
				TestLog.e(ContentUtilities.class, "Encryption failed: " + fileNameWithPath);
				TestLog.ve(ContentUtilities.class, CipherStuff.getErrorMessage(), 5);
				errorBuilder.append(PeaProperties.getVmBridge().translate("encryption_failed") + ": "+ fileNameWithPath + "\n");
				continue;
			}
			boolean writeSuccess = WriteResources.write(newEncrypted, fileName, dirName);
			if (writeSuccess == false) {
				TestLog.e(ContentUtilities.class, "Write failed: " + fileNameWithPath);
				TestLog.ve(ContentUtilities.class, WriteResources.getLastErrorMessage(), 5);
				errorBuilder.append(PeaProperties.getVmBridge().translate("unexpected_error") 
						+ fileNameWithPath + WriteResources.getLastErrorMessage() + "\n");
				continue;
			} else {
				TestLog.o(ContentUtilities.class, "Re-encrypted: " + fileNameWithPath);
				if (fileNameWithPath.startsWith(CloudControl.getTmpDir())) {
					// remove old file from properties
					int removedIndex = CloudPropertyHandler.removeFileFromAnyProviderSet(
							CloudControl.getProviderNameFromTmpFile(fileNameWithPath), 
							CloudControl.getFileNameFromTmpFile(fileNameWithPath),  null);
					if (removedIndex >= 0) {
						TestLog.v(ContentUtilities.class, fileNameWithPath + ": file removed from sub-set index " + removedIndex);
					} else if (removedIndex == -1) {
						TestLog.v(ContentUtilities.class, "File to removed from properties was not found: " + fileNameWithPath);
					} else if (removedIndex == -2) {
						TestLog.ve(ContentUtilities.class, "Error occurred when remiving filw from properties " + fileNameWithPath);
					}
					TestLog.v(ContentUtilities.class, "Upload re-encrypted file " + fileNameWithPath + "... ");
					saveTmpFileInCloud(fileNameWithPath);
				} else {
					//TestLog.v(ContentUtilities.class, "local file");
				}
			}			
		}
		CipherStuff.setCipherAlgo(currentCipher);
		if (errorBuilder.length() > 0) {
			return new String(errorBuilder);
		} else {
			return null;
		}
	}
}
