package cologne.eck.all_peas.files;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

import cologne.eck.all_peas.data.AttachmentHandler;
import cologne.eck.all_peas.data.PeaProperties;
import cologne.eck.all_peas.gui.PeaDialog;
import cologne.eck.all_peas.gui.PeaFileChooser;
import cologne.eck.all_peas.vm_specific.JREProperties;
import cologne.eck.tools.FileTools;
import cologne.eck.tools.TestLog;


/*
 * PeaFactory - Production of Password Encryption Archives
 * Copyright (C) 2015  Axel von dem Bruch
 * 
 * This library is free software; you can redistribute it and/or modify it 
 * under the terms of the GNU General Public License as published 
 * by the Free Software Foundation; either version 2 of the License, 
 * or (at your option) any later version.
 * This library is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
 * or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU General Public License for more details.
 * See:  http://www.gnu.org/licenses/gpl-2.0.html
 * You should have received a copy of the GNU General Public License 
 * along with this library.
 */

public class FileComposer {
	
	/**
	 * the associated FileModel with this instance of FileComposer
	 */
	private FileModel fileModel;
	
	/**
	 * The FileTypePanel associated with this instance of FileComposer. 
	 * This is used for the progress bar
	 */
	private FileTypePanel ftp;
	
	/*
	 * true if the shown files are plain text 
	 * false if the shown files are encrypted
	 */
	private boolean plainModus = true;
	/*
	 * For test mode: print all exceptions and warnings
	 */
	private static boolean printErrors;
	/*
	 * true if a warning (directories includes all sub-directories) was already shown
	 * (this warning should appear at most once for each session)
	 */
	private static boolean directoryWarning = false;	
	/*
	 * true: double encryption (encryption of encrypted files) is allowed, 
	 * false: do not add encrypted files in plain modus
	 */
	private boolean doubleEncrypt = false;
	private boolean doubleEncryptDecided = false;
	/*
	 * FileChooser associated with this instance to choose new file
	 */
	private PeaFileChooser chooser;
	/*
	 * last selected file. Used to open FileChooser at this directory
	 */
	private static File lastDir = null;

	public FileComposer(FileModel _fileModel){

		fileModel = _fileModel;		
		if (PeaProperties.isTestMode()){
			printErrors = true;
		}
	}

	/**
	 * Sort an array of files and update
	 * the check box panel. The method always checks if
	 * a directory contains a valid file and sets it
	 * possibly invalid. 
	 * 
	 * @param checkAgain	true if already listed files
	 * 						should be checked again
	 */
	public final void updateFiles(boolean checkAgain){

		File[] allFileNamesFromPanel = fileModel.getAllFiles(); {//ftp.getAllFileNames();
			//for(int i=0;i<fileModel.getAllFiles().length;i++) TestLog.o(FileComposer.class, fileModel.getAllFiles()[i].getAbsolutePath());
			// append "-" as start of annotation
			ftp.markUnselectedValidFiles();
			if (allFileNamesFromPanel == null) {
				return;
			}
			int len = allFileNamesFromPanel.length;
			if (len == 0) {
				return;
			}
			String invalidMarker = FileModel.getInvalidMarker();

			// clear the panel
			ftp.removeAllCheckBoxes();

			if (checkAgain == false) {

				for (int i = 0; i < len; i++){
					File file = allFileNamesFromPanel[i];			
					String fileName = file.getAbsolutePath();
					String annotation = fileModel.getAnnotation(file);
					
					if (file.isFile()) {

						if (annotation.contains(invalidMarker)){
							InvalidPanel.addInvalidFileCheckBox(fileName, annotation);
						} else {        		
							ftp.addValidFileCheckBox(fileName, annotation);
						} 	   	
					} else if (file.isDirectory()){
						if (annotation.contains(invalidMarker)){
							
							InvalidPanel.addInvalidDirectoryCheckBox(fileName, annotation);// valid: false
						} else {
							
							// check if directory contains valid file:
							//if (checkContainingChildren(file, i, allFileNamesFromPanel) == false){
							if (fileModel.checkIncludedChildren(file, printErrors, plainModus, ftp) == 1) {
								// directory does not contain a valid file: mark as invalid
								String newAnnotation = PeaProperties.getVmBridge().translate("no_valid_files_inside") 
										+ invalidMarker;
								// update the map: 
								fileModel.setAnnotation(file, newAnnotation);
								// set check box:
								InvalidPanel.addInvalidDirectoryCheckBox(fileName, annotation);
							} else { // valid file inside	
								ftp.addValidDirectoryCheckBox(fileName, annotation);
							}
						}
					} else {
						InvalidPanel.addInvalidFileCheckBox(fileName, annotation);
					}
				}

			} else { // check all files again: 
				
				// remove all mappings from TreeMap
				fileModel.resetTreeMap();
				fileModel.resetAllSize();
				fileModel.resetFileNumber(); 
				// get list of file names:
				String[] allFileNames = new String[len];
				for (int i = 0; i < len; i++) {
					allFileNames[i] = allFileNamesFromPanel[i].getAbsolutePath();
				}
				ArrayList<String> list = new ArrayList<String>(Arrays.asList(allFileNames));
				// check all files, but do not add check boxes
		    	addFilesToMap(list, false, false, false);
		    	// check for empty files and add check boxes
		    	updateFiles(false);// do not check already listed files again
				ftp.updateWindow();
			} 
		}
		ftp.updateNumberAndSize();
	}

	/**
	 * Create Strings for file names to display the
	 * file hierarchy. 
	 * 
	 * @param allFileNames	the files names to display 
	 * 
	 * @return					an array of these file names
	 * 							with hierarchy view
	 */
	public final String[] showFileHierarchy(String[] allFileNames){
		
		if (allFileNames == null) {
			return null;
		}
		int len = allFileNames.length;
		// result
		ArrayList<String> resultList = new ArrayList<String>();
		
		// marker to display the file hierarchy:
		String marker = " . . "; // ↳ ⤷ ↪ ⇒

		ArrayList<File> parents = new ArrayList<File>();
		for (int i = 0; i < len; i++) {

			if (allFileNames[i].equals("")) {
				// marked for zip button
				continue;
			}
			File file= new File(allFileNames[i]);	
			String fileNameWithPath = file.getAbsolutePath();
			File parent = file.getParentFile();
			while (parents.isEmpty() == false) {// stop if there is no parent					
				// check if the last added parent is parent of this file:
				int lastIndex = parents.size() - 1;
				if (parent.equals(parents.get(lastIndex))){
					// create the space string for hierarchy view
					String space = new String(new char[lastIndex]).replace("\0", "    ");

					// add space and replace parent with marker
					// escape \ with \\ to avoid java.util.regex.PatternSyntaxException in Windows		
					fileNameWithPath = space  + marker + 
							fileNameWithPath.substring(
									parent.getAbsolutePath().length(), 
									fileNameWithPath.length());
					// resulting string is ready
					break;
				} else {
					// list is sorted, last parent is not parent anymore
					parents.remove(lastIndex);
				}
			}
			// add the maybe modified string
			resultList.add(fileNameWithPath);

			if (file.isDirectory()) {
				// to check the next file
				parents.add(file);
			}
		}	
		return resultList.toArray(new String[resultList.size()]);
	}

	/**
	 * Display a file chooser to add new files
	 * 
	 * @param window		parent window
	 * @param message		the message to display on top of the FileChooser
	 * @param hiddenFilesCheckBox	true: add a check box to optionally 
	 * 								show hidden files
	 * @param checkAgain	true: check already listed files again
	 * 
	 * @return 	true if at least one file was added
	 */
	public final boolean addAction(Object window, String message, 
			boolean hiddenFilesCheckBox, boolean checkAgain) {	
			
		chooser = new PeaFileChooser(window);//, locationOnScreen);
		if (lastDir != null){
			chooser.setCurrentDirectory(lastDir);
		}
		chooser.setAcceptAllFileFilterUsed(true);
		chooser.setFileSelectionMode(PeaFileChooser.FILES_AND_DIRECTORIES);
		chooser.setMultiSelectionEnabled(true);
		
		chooser.customizeFileChooser(null, message, true);// no title

	    int returnVal = chooser.showOpenDialog(window);
	    
	    if(returnVal == 0) { //FileChooser.APPROVE_OPTION == 0			    	
	    	
	    	lastDir = chooser.getCurrentDirectory();
	    	File[] files = chooser.getSelectedFiles();
	    	if (files == null || files.length == 0) {
	    		TestLog.e(FileComposer.class, "no files selected");
	    		return false;
	    	}	    	
	    	// check execution time before checking and adding the files:
	    	// this will stop for many or large files with a warning and option to cancel
	    	if (fileModel.checkNumberAndSize(files, window, plainModus, printErrors) == false){
	    		TestLog.e(FileComposer.class, "Adding process canceled because of expected execution time");
	    		// do not check or add file
	    		return false;
	    	}
	    	// create new check boxes for selected files
	    	int len = files.length;
	    	String[] selectedFileNames = new String[files.length];
	    	for (int i = 0; i < len; i++) {
	    		selectedFileNames[i] = chooser.getSelectedFiles()[i].getAbsolutePath();
	    	}	    	
			ArrayList<String> list = new ArrayList<String>(Arrays.asList(selectedFileNames));
	    	addFilesToMap(list, true, checkAgain, true);// true: include sub-directories
	    	// false: do not check already added files again
	    	// false: do not add check boxes
	    	if (window instanceof FileTypePanel) {
	    		updateFiles(false);// do not check already listed files again
	    		ftp.updateWindow();
	    		//ftp.updateNumberAndSize();
	    	}
	    	return true;
	    } else { // cancel or close
	    	return false;
	    }
	}

	/**
	 * Get the current number and size of all valid selected files
	 * in a String
	 * 
	 * @return 	Informations about the current number 
	 * 		and size of all valid selected files as String
	 */
    public final String getCurrentNumberAndSize() {

		if (fileModel.getAllSize() < 0) {
			TestLog.e(FileComposer.class, "FileDisplayPanel: invalid value for size of selected files (fc.getAllSize()) ");
			fileModel.calculateNumberAndSize(ftp.getValidLocalFileNames(true, false));
			//return ("Error: invalid size");
		}
		if (fileModel.getFileNumber() < 0) {
			TestLog.e(FileComposer.class, "FileDisplayPanel: invalid value for number of selected files (fc.getFileNumber()) ");
			fileModel.calculateNumberAndSize(ftp.getValidLocalFileNames(true, false));
			//return ("Error: invalid number");
		}
		//TestLog.o(FileComposer.class, "number : " + fileModel.getFileNumber());
		return (PeaProperties.getVmBridge().translate("number_of_files") 
				+ fileModel.getFileNumber() 
				+ ", " + PeaProperties.getVmBridge().translate("overall_size")
				+ fileModel.getAllSize() + " - (MiB): " 
				+ fileModel.getAllSize()/1024/1024);
    }
    
     /**
     * Checks a file (validity, directory or file), if file is
     * already encrypted, if size or number is too high,
     * sets annotations and shows a single check box if the
     * file is not already listed. The
     * background color and behavior of the check box
     * depends on the validity of the file. For high number
     * of file and large file size a warning message appears.
     * 
     * @param fileNameWithPath		the name of the file to show
     * @param includeChildren	true, if children of directories should be 
     * 							included (for initialization case), false if not
     * 							(all other cases)
     * 
     * @return	true if the view must be updated
     */
    public final boolean addOrUpdateFile(File file,  
    		boolean includeChildren) {
    	
    	boolean isReadingPathFile = AttachmentHandler.isReadingPathFile();
    	
    	boolean updateView = false;// return value
    	
    	boolean alreadyListed = fileModel.containsFile(file);
    	boolean wasInvalid = false;
    	if (alreadyListed == true) {    		
    		wasInvalid = fileModel.getAnnotation(file).contains(FileModel.getInvalidMarker());
    	}    	
    	String fileName = file.getAbsolutePath();
		boolean markAsEncrypted = false;    	
    	if (alreadyListed == false) {
    		// check if plain files are encrypted
    		if (plainModus == true) {
    			if (doubleEncrypt == false) { // do not encrypt encrypted file
    				// check file identifier (true for already encrypted files)
    				if (file.isDirectory() == false) {
    					if (fileModel.checkFileIdentifier(file, printErrors) == null) {
    						if (doubleEncryptDecided == false) { // not asked before
    							// this is an encrypted file, display warning and option to break
    							int result = PeaDialog.showQuestion(JREProperties.getMainWindow(),//does not work: PeaProperties.getTypePanel(), 
    									PeaProperties.getVmBridge().translate("encrypt_encrypted_files"), 
    									PeaProperties.getVmBridge().translate("warning"), // title 
    									0, // YES_NO_OPTION
    									2); // WARNING_MESSAGE
    							if (result == 0) { // YES_OPTION
    								doubleEncryptDecided = true; // do not ask again
    								doubleEncrypt = true; // always add encrypted files
    							} else if (result == 1){ // NO_OPTION 
    								doubleEncryptDecided = true;
    								doubleEncrypt = false;
    								markAsEncrypted = true; 
    							} else { // close
    								return updateView;
    							}
    						} else { //already decided: do not add encrypted files
    							markAsEncrypted = true; 
    						}
    					}
    				} else {  // file is directory
    					// 
    				}
    			} else { // doubleEncrypt true
    				// nothing to to do, don't check, just encrypt again
    			}
    		} else {// encrypted Modus
    			// no extra check for encrypted files
    		}
    	}
    	// update or add file to Map of model:
    	String annotation = fileModel.checkAndAddFile(file, plainModus, printErrors, true);    			
    	if (annotation.contains(FileModel.getInvalidMarker()) == false 
    			&& wasInvalid == true && isReadingPathFile == false) {

    		InvalidPanel.setCheckBoxValid(fileName, annotation);
    		updateView = true;
    		if (file.isFile()) {
    			long fileSize = FileTools.getFileSize(file);
    			fileModel.increaseAllSize(fileSize);
    			fileModel.increaseFileNumber( 1);
    		}
    		return updateView;
    	}    	
    	// update or add file to panel:
    	if (alreadyListed == false) {

    		updateView = true;
    		// add file
    		if (file.isFile()) {
    			if (annotation.contains(FileModel.getInvalidMarker())) {

    				InvalidPanel.addInvalidFileCheckBox(fileName, annotation);    		
    			} else {

    		    	if (markAsEncrypted == true) {
    		    		// set annotation
    		    		fileModel.setAnnotation(
    		    				file, PeaProperties.getVmBridge().translate("already_encrypted") 
    		    				+ FileModel.getInvalidMarker());
    		    		InvalidPanel.addInvalidFileCheckBox(fileName, annotation);      
    		    	} else {
    		    		ftp.addValidFileCheckBox(fileName, annotation);
    		    		long fileSize = FileTools.getFileSize(file);
    		    		fileModel.increaseAllSize(fileSize);
    		    		fileModel.increaseFileNumber( 1);
    		    	}
    			}
    		} else if (file.isDirectory()) {
    			if (annotation.contains(FileModel.getInvalidMarker())) {
    				InvalidPanel.addInvalidDirectoryCheckBox(fileName, annotation);
    				//updateView = false;
    			} else {
    				
        			if (includeChildren == true){ // adding new files

        				//check if at least one child file is valid: 
        				// this will also test execution time and show warning
        				int childFound = fileModel.checkIncludedChildren(file, printErrors, plainModus, ftp);
        				if (childFound == 0) {    // valid file found, not cancelled
        					
        					ftp.addValidDirectoryCheckBox(fileName, annotation);

        					// warning for directory: once for each session
        					// directory contains all sub-directories and (hidden) files...
        					if (plainModus == true && directoryWarning == false 
        							&& isReadingPathFile == false){
        						directoryWarning = true;     	
        						PeaDialog.showMessage(
        								JREProperties.getMainWindow(),
        								PeaProperties.getVmBridge().translate("adding_directory_includes_hidden_files"),
        								PeaProperties.getVmBridge().translate("warning"),
        								2); // JOptionPane.WARNING_MESSAGE		    		
        					}

        					//ArrayList<String> list = new ArrayList<String>(Arrays.asList(includedFileNames));
        					ArrayList<File> list = FileTools.listAllFilesOfFolder(file, false);
        					// add all children
        					int len = list.size();
        					for (int i = 0; i <len; i++) {
        						addOrUpdateFile(list.get(i), false);
        					}
        				} else if (childFound == 1){ // no valid file found, not cancelled
        					// set annotation
        					String newAnnotation = 
        							PeaProperties.getVmBridge().translate("no_valid_files_inside")
        							+ FileModel.getInvalidMarker();
        					fileModel.setAnnotation(file, newAnnotation);
        					// add invalid check box
        					InvalidPanel.addInvalidDirectoryCheckBox(fileName, newAnnotation);//, nextIndex++);
        					
        				} else if (childFound == -1){ // canceled because of warning
        					TestLog.e(FileComposer.class, "adding file cancelled");
        					// remove file from map:
        					fileModel.removeFileFromList(file);
        				}
        			}
    			}
    		} else {
    			InvalidPanel.addInvalidFileCheckBox(fileName, annotation);    
    		}
    	} else { // already listed
    		String oldAnnotation = fileModel.getAnnotation(file);
    		if ( ! oldAnnotation.equals(annotation)) {
    			// update checkBox:
    			ftp.updateCheckBox(fileName, annotation, file.isDirectory());
    			updateView = true;
    		}
    	}
    	//TestLog.v(FileComposer.class, "check " + file.getAbsolutePath() + ": " + updateView, 7);
    	return updateView;
    }

    /**
     * Displays selected files as check boxes, checks files, displays warning, 
     * updates the TreeMap if required and
     * adds comments for invalid files. 
     * 
     * @param newFileNames				the new selected files
     * @param includeSubdirectories		true: include all sub-directories if the 
     * 										chosen files
     * @param checkAgain				true if already listed files should be 
     * 										checked again
     * @param addToPanel				true: add check box for files, 
     * 									false: only set annotations
     * 
     * @return	true if the view must be updated
     */
    public final boolean addFilesToMap(ArrayList<String> newFileNames, 
    		boolean includeSubdirectories, boolean checkAgain, boolean addToPanel) {

    	boolean updateView = false;
    	// sort the file names
    	Collections.sort(newFileNames);
    	// get the index of the first file:
    	int len = newFileNames.size();    	
    	for (int i = 0; i < len; i++) {    		
    		String fileName = newFileNames.get(i);    		
    		if (fileName == null) {// 
    			TestLog.e(FileComposer.class, "Null-entry in file names to add...");
    			continue;
    		}
    		//TestLog.o(FileComposer.class, "FC addFiles " + fileName + ", " + includeSubdirectories+ ", " + checkAgain+ ", " +  addToPanel);
    		//addFileToMap(fileName, includeSubdirectories, checkAgain, addToPanel);
    		boolean update = addOrUpdateFile(new File(fileName), includeSubdirectories);
    		if (update == true) {
    			updateView = true;
    		}
    	} // end for loop over file names
    	ftp.updateFileView();
    	return updateView;
    }

    //================== Getter & Setter =====================

    /**
     * Get the PeaFileChooser associated with this FileComposer.
     * The PeaFileChooser stores the last selected directory.
     * 
     * @return	the PeaFileChooser
     */
	public PeaFileChooser getPeaFileChooser() {
		return chooser;
	}
	
    /**
     * Get the FileModel which is associated 
     * with this instance of FileComposer
     * 
	 * @return the fileModel
	 */
	public FileModel getFileModel() {
		return fileModel;
	}
	
    /**
     * Set the FileModel which is associated 
     * with this instance of FileComposer
     * 
	 * @param fileModel the fileModel to set
	 */
	public void setFileModel(FileModel fileModel) {
		this.fileModel = fileModel;
	}	
	
	/**
	 * Get the current modus: plain text files
	 * or encrypted files
	 *  
	 * @return	true if currently processed files 
	 * 			are plain text files, 
	 * 			false if these files are encrypted
	 */
    public final boolean isPlainModus() {
    	return plainModus;
    }
    
	/**
	 * Set the current modus: plain text files
	 * or encrypted files
	 *  
	 * @param	true to indicate that currently 
	 * 			processed files should be treated 
	 * 			as plain text files, 
	 * 			false if these files should be 
	 * 			treated as encrypted files
	 */
    public final void setPlainModus(boolean _modus) {
    	plainModus = _modus;
    }
    
	/**
	 * @return the ftp
	 */
	public FileTypePanel getFileTypePanel() {
		return ftp;
	}
	
	/**
	 * @param ftp the ftp to set
	 */
	public void setFileTypePanel(FileTypePanel ftp) {
		this.ftp = ftp;
	}

	/**
	 * Print error messages (text mode) or not. 
	 * 
	 * @return	true for print error messages
	 */
	public boolean isPrintErrors(){
		return printErrors;
	}
	
	/**
	 * Get the last used directory
	 * 
	 * @return	the last used directory
	 */
	public File getLastDir() {
		return lastDir;
	}
 }
