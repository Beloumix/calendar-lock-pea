package cologne.eck.all_peas.files;

/*
 * Peafactory - Production of Password Encryption Archives
 * Copyright (C) 2015  Axel von dem Bruch
 * 
 * This library is free software; you can redistribute it and/or modify it 
 * under the terms of the GNU General Public License as published 
 * by the Free Software Foundation; either version 2 of the License, 
 * or (at your option) any later version.
 * This library is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
 * or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU General Public License for more details.
 * See:  http://www.gnu.org/licenses/gpl-2.0.html
 * You should have received a copy of the GNU General Public License 
 * along with this library.
 */

/**
 * Displays the selected files.
 * Displays selected files, hierarchy, invalid files, file number, all selected size.
 * Contains a ProgressBar to show the encryption progress.
 */


import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Dimension;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;

import javax.swing.*;
import javax.swing.border.EmptyBorder;

import cologne.eck.all_peas.control.PeaControl;
import cologne.eck.all_peas.data.PeaProperties;
import cologne.eck.all_peas.gui.CursorManager;
import cologne.eck.all_peas.gui.IconManager;
import cologne.eck.all_peas.gui.PeaDialog;
import cologne.eck.all_peas.gui.PeaFileChooser;
import cologne.eck.all_peas.gui.PswDialogView;
import cologne.eck.all_peas.gui.ScrollbarAction;
import cologne.eck.all_peas.vm_specific.JREProperties;
import cologne.eck.tools.FileTools;
import cologne.eck.tools.TestLog;
//import cologne.eck.tools.Help;
import cologne.eck.tools.UnexpectedValueException;
import cologne.eck.tools.ZipStuff;
import cologne.eck.tools.Converter;
import cologne.eck.tools.FileDropHandler;


@SuppressWarnings("serial")
public class FileTypePanel extends JPanel implements ActionListener{
	
	/*
	 * instance
	 */
	private FileTypePanel ftp;
	/*
	 * the associated FileComposer for this FileTypePanel
	 */
	private FileComposer fc;// = new FileComposer();	
	
	/*
	 * Task to show a progress verticalBar 
	 */
	private ProgressTask task;	

	/*
	 * JPanel with addButton, used in PeaFactory and in test case 
	 */
	private JPanel buttonPanel;// button to add new files, used in PeaFactory	
	/**
	 * JPanel containing cloud files
	 */
	private CloudFilePanel cfp;
	/**
	 * JScrollPane for cloud files
	 */
	private JScrollPane cfpScroll;
	/**
	 * JPanel as placeholder to add CloudFilePanel
	 */
	private JPanel placeHolderPanel;
	/**
	 * JPanel containing all check boxes of valid file names
	 */
	private JPanel checkPanel; // contains the JCheckBoxes: one for each file
	private GridBagConstraints gbc;
	/**
	 * Contains the check box to select all if more than 12 files
	 */
	private JPanel checkAllButtonPanel;

	/*
	 * Label to display file number and size:
	 * Initialization required before FileTypePanel in fc.initFileNames() 
	 */
	private JLabel infoLabel = new JLabel(); // displays file number and size
	/*
	 * Button to open FileChooser
	 */
	private JButton addNewFilesButton;
	
	// to change language
	private JPanel statusPanel;

	
	/**
	 * Panel to show selected file names in check boxes.
	 *  
	 * @param scrollWidth	the width of the scroll pane for check box panel
	 * @param scrollHeight	the height of the scroll pane for check box panel
	 * @param _plainModus	true if files are plain text, false for encrypted files
	 * @param addButton		true: add a button to open a FileChooser
	 * @param _fc			the associated FileComposer with this instance
	 * @param window		the parent window of this panel
	 */
	public FileTypePanel(int scrollWidth, int scrollHeight, 
			boolean _plainModus, boolean addButton, 
			FileComposer _fc, Object windown){
		
		if (_fc == null) {
			TestLog.ve(FileTypePanel.class, "Missing file composer...", 5);
		}
		fc = _fc;
		if (fc == null) {
			TestLog.ve(FileTypePanel.class, "Missing file composer...", 5);
			return;
		}
		fc.setPlainModus(_plainModus);
		
		if (_plainModus == false) {
			this.setTransferHandler(new FileDropHandler());
		}
		fc.setFileTypePanel(this);
		
		ftp = this;
		if (_plainModus == true) {
			PswDialogView.setUI(false);
		} else {
			PswDialogView.setUI(true);
		} 
		this.setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
		
		this.getAccessibleContext().
	    setAccessibleDescription(
	    	    "Group of files that are encrypted with the same algorithms, parameters and keyfiles.");

		// button to open FileChooser
		if (addButton == true){ // not in File PEA but in PeaFactory
			buttonPanel = new JPanel();
			buttonPanel.setLayout(new BoxLayout(buttonPanel, BoxLayout.X_AXIS));
			buttonPanel.setBorder(new EmptyBorder(10,0,5,0));
			addNewFilesButton = new JButton(PeaProperties.getVmBridge().translate("add_new_file"));
			addNewFilesButton.addActionListener(this);
			addNewFilesButton.getAccessibleContext().
		    setAccessibleDescription(
		    	    "Clicking this component opens a dialog to choose files to add.");
			addNewFilesButton.setActionCommand("addFile");
			buttonPanel.add(addNewFilesButton);
			buttonPanel.add(Box.createHorizontalGlue());
			this.add(buttonPanel);	
		}
		String status = null;
		ImageIcon statusIcon = null;
		if (_plainModus == true){
			status = PeaProperties.getVmBridge().translate("status_unencrypted");
			statusIcon = IconManager.loadIcon("unlocked.png", PeaProperties.getVmBridge().translate("open_padlock"), 
					null, PeaProperties.getVmBridge().translate("unencrypted_indicator") );
		} 
		if (status != null || statusIcon != null) {
			statusPanel = new JPanel();
			statusPanel.setLayout(new BoxLayout(statusPanel, BoxLayout.X_AXIS));
			statusPanel.setMaximumSize(new Dimension((int)PswDialogView.getScreenwidth(), 30));
			if (statusIcon != null) {
				statusPanel.add(Box.createHorizontalGlue());
				statusPanel.add(new JLabel(statusIcon));
				statusPanel.add(Box.createHorizontalStrut(10));
			}		
			JLabel statusLabel= new JLabel(status);
			statusLabel.setFont(new Font(Font.MONOSPACED, Font.ITALIC, PeaProperties.getFontSize()));
			statusPanel.add(statusLabel);
			this.add(statusPanel);
		}
    	if (_plainModus == false && ! PeaProperties.getFileType().equals("file")) { // PswDialogView, not File Lock PEA
    		// always add the place holder panel
    		placeHolderPanel = new JPanel();
    		placeHolderPanel.setLayout(new BoxLayout(placeHolderPanel, BoxLayout.PAGE_AXIS));
    		this.add(placeHolderPanel);
    		// always initialize CloudFilePanel and scrollpane
    		String algoParamAsHex = Converter.bytes2hex(fc.getFileModel().getSchemeMarker());
    		if (algoParamAsHex == null) {
    			TestLog.ve(FileTypePanel.class, "Missing algos", 5);
    		}
    		cfp = new CloudFilePanel(PswDialogView.getView(), this, 

    				Converter.bytes2hex(fc.getFileModel().getSalt()), 
    				algoParamAsHex);
    		cfpScroll = new JScrollPane(cfp);
        	cfpScroll.setPreferredSize(new Dimension(scrollWidth, 80));
        	// is added later
    	}
	
		checkPanel = new JPanel(); // the panel that holds the check boxes
		GridBagLayout grid = new GridBagLayout();

		checkPanel.setLayout(grid);
        gbc = new GridBagConstraints();
        gbc.ipady = 5;
  
        gbc.weightx = 1;
        gbc.fill = GridBagConstraints.HORIZONTAL;
        gbc.gridwidth = GridBagConstraints.REMAINDER;
        
        gbc.insets = new Insets(1,0,1,0);
        
        if (_plainModus == true) {
        	JScrollPane scroll = new JScrollPane(checkPanel);		
        	
    		if (JREProperties.getMainWindow() != null && JREProperties.getMainWindow() instanceof JWindow) {
    			JPanel contentPane = (JPanel) ((JWindow) JREProperties.getMainWindow()).getContentPane();
    			if (contentPane != null) {
    				// register keys for keyboard access:
    				ScrollbarAction.getScrollPaneKeyboardAccess(contentPane, scroll, false);
     			}
    		}
        	this.add(scroll);		
        } else {
        	this.add(checkPanel);
        }        
        // panel for button to select all check boxes if more than 12
        checkAllButtonPanel = new JPanel();        
        checkAllButtonPanel.setLayout(new BoxLayout(checkAllButtonPanel, BoxLayout.LINE_AXIS));
        checkAllButtonPanel.setMaximumSize(new Dimension(2000, 30));
        this.add(checkAllButtonPanel);
				
		JPanel infoPanel = new JPanel();// Panel to show number and size of the files
		infoPanel.setLayout(new BoxLayout(infoPanel, BoxLayout.X_AXIS));
		infoPanel.setBorder(new EmptyBorder(0,0,10,0));

		infoLabel.setText(fc.getCurrentNumberAndSize());
	/*	infoLabel.setText(PeaProperties.getVmBridge().translate("number_of_files") 
				+ fc.getFileModel().getFileNumber() 
				+ ", " + PeaProperties.getVmBridge().translate("overall_size")
				+ fc.getFileModel().getAllSize() + " - (MiB): " 
				+ fc.getFileModel().getAllSize()/1024/1024);*/
		infoPanel.add(infoLabel);
		infoPanel.add(Box.createHorizontalGlue());
		this.add(infoPanel);
		if (fc.isPlainModus() == false) { // check if there are files to show
//			fc.initFileNames();
		}
	}
	
	/**
	 * Reset the language to current language bundle
	 */
	public void resetLanguage() {
		String status = null;
		ImageIcon statusIcon = null;
		if (fc.isPlainModus() == true){
			status = PeaProperties.getVmBridge().translate("status_unencrypted");
			statusIcon = IconManager.loadIcon("unlocked.png", PeaProperties.getVmBridge().translate("open_padlock"), 
					null, PeaProperties.getVmBridge().translate("unencrypted_indicator") );
		} 
		if (status != null || statusIcon != null) {
			statusPanel.removeAll();
			if (statusIcon != null) {
				statusPanel.add(Box.createHorizontalGlue());
				statusPanel.add(new JLabel(statusIcon));
				statusPanel.add(Box.createHorizontalStrut(10));
			}		
			JLabel statusLabel= new JLabel(status);
			statusLabel.setFont(new Font(Font.MONOSPACED, Font.ITALIC, PeaProperties.getFontSize()));
			statusPanel.add(statusLabel);
		}
		if (checkAllButtonPanel != null) {
			Component[] comps = checkAllButtonPanel.getComponents();
			if (comps != null) {
				for (Component c : comps) {
					if (c instanceof JCheckBox) {
						((JCheckBox) c).setText(PeaProperties.getVmBridge().translate("all"));
					}
				}
			}
		}
	}

	@Override
	public void actionPerformed(ActionEvent ape) {
		
		String command = ape.getActionCommand();

		if (command.equals("addFile")) {		
			boolean showHiddenFilesOption = false;
			if (PeaProperties.getFileType().equals("file")){
				showHiddenFilesOption = true;
			}
			fc.addAction(this,
					PeaProperties.getVmBridge().translate("select_files_to_encrypt"),
					showHiddenFilesOption, false);
		    updateWindow();
		    infoLabel.setText(fc.getCurrentNumberAndSize());
		    
		} else if (command.equals("dirSelection")) { // non empty directory			
			// get the selected/deselected checkBox:
			JCheckBox dirCheck = (JCheckBox) ape.getSource();		
			dirCheck.getAccessibleContext().
		    setAccessibleDescription(
		    	    "Clicking this component selects or deselects files of this folder.");
			// check if dirCheck is selected or not 
			boolean select;
			if (dirCheck.isSelected() == true) {
				select = true;
			} else {
				select = false;
			} 
			// select/deselect all children of this directory and check
			// if at least the staus of one check box was changed
			boolean stateChanged = selectChildren(select, dirCheck);
			
			// if deselect: deselect all parent directories
			if (select == false && stateChanged == true) {
				deselectParentDirectories(dirCheck);				
			}
			infoLabel.setText(fc.getCurrentNumberAndSize());

		} else if (command.equals("fileSelection")) {					
			// get the associated file model
			FileModel model = fc.getFileModel();
			// get the check box
			JCheckBox fileCheck = (JCheckBox) ape.getSource();
			// get the file name of the check box
			String fileName = fileCheck.getName();
			// get the file size of this file and update allSize
			long fileSize = FileTools.getFileSize(fileName);			
			if (fileCheck.isSelected() == true) {
				model.increaseAllSize(fileSize);//setAllSize(model.getAllSize() + fileSize);
				model.increaseFileNumber( 1);
				// select parent directories if became full
				selectFullParentDirectories(fileCheck);
			} else {		
				model.increaseAllSize( -fileSize);//setAllSize(model.getAllSize() - fileSize);
				model.increaseFileNumber( -1);
				// deselect all parent directories
				deselectParentDirectories(fileCheck);
			}
			infoLabel.setText(fc.getCurrentNumberAndSize());			
		} else if (command.startsWith("unzip")) {
			TestLog.o(FileTypePanel.class, "unzip " + command.substring(5));
			
			String zipFileName = command.substring(5);			
			String zipFolderName = null;
			
			if (zipFileName.endsWith("_pea.zip")) {
				// files were stored with absolute path
				// set root directory:
				zipFolderName = "";// - does not work in windows: File.listRoots()[0].getAbsolutePath();
				
			} else {
				// a zipped folder with relative path names
				// select directory to store zip file
				PeaFileChooser chooser = new PeaFileChooser(this);
				chooser.customizeFileChooser("Zip", "Select a folder to store the zip file content", false);
				chooser.setFileSelectionMode( JFileChooser.DIRECTORIES_ONLY);
				//In response to a button click:
				int returnVal = chooser.showOpenDialog(this);
				if (chooser.getSelectedFile() == null || returnVal != JFileChooser.APPROVE_OPTION) {
					PeaDialog.showMessage(this, "You must choose a folder", "Zip", 0);
					return;					
				} else {
					zipFolderName = chooser.getSelectedFile().getAbsolutePath();					
				}
			}		

			File[] files = ZipStuff.unzip(zipFileName, zipFolderName, (Window)PeaControl.getDialog().getLockFrame());
			// add files:
			// add zip file to FileTypePanel
			if (files == null) {
				TestLog.ve(FileTypePanel.class, "No files unzipped...", 5);
				PeaDialog.showMessage(this, PeaProperties.getVmBridge().translate("unexpected_error"), "Zip", 0);
				return;		
			}
			int len = files.length;
			for (int i = 0; i < len; i++) {
				fc.addOrUpdateFile(files[i], false);
			}
			updateNumberAndSize();
			updateFileView();		
			
			// delete original files?
			int deleteZip = PeaDialog.showQuestion(this, "Zip file unpacked\nDelete original zip file?", "Zip", 0);

			if (deleteZip == 0) { // delete ok

				long size = FileTools.getFileSize(zipFileName);
				String newError = FileTools.wipeFile(zipFileName, true);
				if (newError == null) {
					this.removeCheckBox(zipFileName, size);
				} else {
					PeaDialog.showMessage(this, "Deletion failed: \n" + newError, "Wipe", 0);
				} 
			}
		} else if ( command.equals("rememberDecision") ) {	
			JCheckBox check = (JCheckBox) ape.getSource();
			if (check.isSelected()) {
				PeaProperties.setOverwriteExistingFiles(true);
			} else {
				PeaProperties.setOverwriteExistingFiles(false);
			}
			
		} else if ( command.equals("checkAll") ) {	
			if (ape.getSource() instanceof JCheckBox) {
				int size = checkPanel.getComponentCount();
				FileModel model = fc.getFileModel();
				if ( ((JCheckBox) ape.getSource()).isSelected() == true) {
					for (int i = 0; i < size; i++) {
						if ( checkPanel.getComponent(i) instanceof JCheckBox) {
							JCheckBox check = (JCheckBox) checkPanel.getComponent(i);
							check.setSelected(true);
							String fileName = check.getName();
							File f = new File(fileName);
							fc.getFileModel().checkAndAddFile(f, fc.isPlainModus(), 
									PeaProperties.isTestMode(), false);
							// get the file size of this file and update allSize
							long fileSize = FileTools.getFileSize(f);			
							model.increaseAllSize(fileSize);
							model.increaseFileNumber(1);
						}
					}
					
				} else {
					for (int i = 0; i < size; i++) {
						if ( checkPanel.getComponent(i) instanceof JCheckBox) {
							JCheckBox check = (JCheckBox) checkPanel.getComponent(i);
							check.setSelected(false);
						}
					}
					model.resetTreeMap();
				}		
				//TestLog.v(FileTypePanel.class,"Update number and size",5);
				updateNumberAndSize();
			}			
		}
	}

	//==============================================================================================
	// Internal helper functions
	
	/**
	 * Get the index of a check box in the checkPanel. 
	 * This function iterates over all entries.
	 * 
	 * @param check		the check box
	 * 
	 * @return			the index of the check box or
	 * 					-1 if the check box is not included
	 */
	private final int getComponentIndex(JCheckBox check) {
		if (check != null ) {
			int len = checkPanel.getComponentCount();
			for (int i = 0; i < len; i++) {
				if (checkPanel.getComponent(i) == check){
					return i;
				}
			}
		}
		return -1;
	}
	
	/**
	 * Disable all check boxes
	 */
	final void disableCheckBoxes() {
		int size = checkPanel.getComponentCount();
		for (int i = 0; i < size; i++) {
			Component c = checkPanel.getComponent(i);
			if ( c instanceof JCheckBox) {
				//JCheckBox check = (JCheckBox) checkPanel.getComponent(i);
				((JCheckBox) c).setEnabled(false);
			}
		}
		if (cfp != null) {
			int cloudFileNumber = cfp.getComponentCount();
			if (cloudFileNumber > 0) {
				for (int i = 0; i < cloudFileNumber; i++) {
					Component c = cfp.getComponent(i);
					if (c instanceof JCheckBox) {
						((JCheckBox) c).setEnabled(false);
					}
				}
			}
		}
	}
	
	/**
	 * Enable all check boxes
	 */
	protected final void enableCheckBoxes() {
		int size = checkPanel.getComponentCount();
		for (int i = 0; i < size; i++) {
			Component c = checkPanel.getComponent(i);
			if ( c instanceof JCheckBox) {
				//JCheckBox check = (JCheckBox) checkPanel.getComponent(i);
				((JCheckBox) c).setEnabled(true);
			}
		}
		if (cfp != null) {
			int cloudFileNumber = cfp.getComponentCount();
			if (cloudFileNumber > 0) {
				for (int i = 0; i < cloudFileNumber; i++) {
					Component c = cfp.getComponent(i);
					if (c instanceof JCheckBox) {
						((JCheckBox) c).setEnabled(true);
					}
				}
			}
		}
	}
	
	/**
	 * Show the full name of a file. This method just
	 * takes the current file names in the panel.
	 */
	public final void showFullFileNames(){
		
		int size = checkPanel.getComponentCount();
		for (int i = 0; i < size; i++) {
			if ( checkPanel.getComponent(i) instanceof JButton) {
				continue;
			}
			JCheckBox check = (JCheckBox) checkPanel.getComponent(i);
			check.setText(check.getName());
		}
	}
	
	/**
	 * Cleanup file view: remove unselected and
	 * invalid files and check valid, selected files again
	 */
	public void cleanupView() {
		// get only selected file names
		String[] selectedFileNames = getValidLocalFileNames(true, false);
		if (selectedFileNames == null) {
			return;
		}
		Arrays.sort(selectedFileNames);
		FileModel model = fc.getFileModel();
		// delete all current files
		model.resetTreeMap();
		model.resetFileNumber();
		model.resetAllSize();
		// create new map:
		ArrayList<String> list = new ArrayList<String>(Arrays.asList(selectedFileNames));
    	fc.addFilesToMap(list, false, false, false);// false: include sub-directories
    										// false: do not check already added files again
    										// true: add check boxes
//    	for(int i=0;i<model.getAllFiles().length;i++) TestLog.o(FileTypePanel.class, model.getAllFiles()[i].getAbsolutePath());
    	fc.updateFiles(false);// do not check already listed files again
		updateWindow();
		model.calculateNumberAndSize(getValidLocalFileNames(true, false));
		infoLabel.setText(fc.getCurrentNumberAndSize());
	}
	
	/**
	 * Select or deselect all children of a directory
	 * check box. Only check boxes among the given
	 * check box are affected. 
	 * 
	 * @param select	true for select, false for deselect
	 * @param check		the directory check box
	 * 
	 * @return			true if the status of at least one check box
	 * 					was modified
	 */
	private boolean selectChildren(boolean select, JCheckBox check){
		
		if (check == null) {
			new UnexpectedValueException("check", "JCheckBox", "is null").printDescription();
			return false;
		}

		if ( ! (check instanceof PeaDirCheckBox) ) {
			new UnexpectedValueException("check", "JCheckBox", "has not valid directory color").printDescription();
			return false;
		}		
		boolean stateChanged = false; // check if at least one check box is selected/deselected
		FileModel model = fc.getFileModel();
		String directoryName = check.getName();
		int checkIndex = getComponentIndex(check);// index of this check box in checkPanel
		if (checkIndex == -1) {
			new UnexpectedValueException("checkIndex", "int", "is -1, check box missing").printDescription();
			return false;
		}
		int componentNumber = checkPanel.getComponentCount();
		// select/deselect check boxes among the given check box
		for (int i = checkIndex; i < componentNumber; i++) {
			// get check box among
			if (checkPanel.getComponent(i) instanceof JButton) {// unzip
				continue;
			}
			JCheckBox nextCheck = (JCheckBox) checkPanel.getComponent(i);
			if (nextCheck.getName().contains(directoryName)){			
				if (nextCheck instanceof PeaCheckBox ){
					if (select == true && nextCheck.isSelected() == false) {
						stateChanged = true;					
						model.increaseAllSize( FileTools.getFileSize(nextCheck.getName()));
						model.increaseFileNumber( 1); // update number
					} else if (select == false && nextCheck.isSelected() == true) {
						stateChanged = true;
						model.increaseAllSize( -FileTools.getFileSize(nextCheck.getName()));	
						model.increaseFileNumber( -1); // update number
					}
					nextCheck.setSelected(select); // select/deselct
				} else if (  (nextCheck instanceof PeaDirCheckBox) ) {
					nextCheck.setSelected(select);

				}				
			} else {
				// file of another directory was reached
				return stateChanged;
			}
		}
		return stateChanged;
	}
	
	/**
	 * Deselect check boxes of parent directories of a file
	 * if the file was deselected. Only check boxes of
	 * directories above the check box are affected. 
	 * 
	 * @param check	the file check box that was deselected
	 */
	private void deselectParentDirectories(JCheckBox check){
		
		if (check == null) {
			new UnexpectedValueException("check", "JCheckBox", "is null").printDescription();
			return;
		}

		// get file name of check box
		String fileName = check.getName();
		// get index of check box in checkPanel
		int checkIndex = getComponentIndex(check);
		if (checkIndex == -1) {
			new UnexpectedValueException("checkIndex", "int", "is -1, check box missing").printDescription();
			return;
		}
		// get name of the parent directory
		String rootDirName = fileName.substring(0, fileName.lastIndexOf(File.separator));//secondSeparator);

		// proof only check boxes above
		for (int i = checkIndex -1;i >= 0; i--) {
			// get the next check box above:
			if (checkPanel.getComponent(i) instanceof JButton) {
				// unzip button
				continue;
			}
			JCheckBox nextCheck = (JCheckBox) checkPanel.getComponent(i);
			if (  (nextCheck instanceof PeaDirCheckBox) ) {
				
				if ( rootDirName.equals(nextCheck.getName()) ){
					// deselect the directory check box
					nextCheck.setSelected(false);
					// update directory to check: parent of current directory
					rootDirName = rootDirName.substring(0, rootDirName.lastIndexOf(File.separator));
				} else {
					if  ( ! nextCheck.getName().contains(rootDirName)){
						// reached another directory... 
						return;
					}
				}
			}	
		}
	}
	
	/**
	 * If a file check box was selected, this function
	 * checks if parent directories is complete (all
	 * shown children) selects them in this case.
	 * 
	 * @param check		the selected file check box
	 */
	private void selectFullParentDirectories(JCheckBox check){
		String fileName = check.getName();
		int checkIndex = getComponentIndex(check);
		if (checkIndex == -1) {
			new UnexpectedValueException("checkIndex", "int", "is -1, check box missing").printDescription();
			return;
		}
		// get name of the parent directory, this variable will change
		String rootDirName = fileName.substring(0, fileName.lastIndexOf(File.separator));//secondSeparator);

		int lastCheckedIndex = checkIndex;
		// search for unselected parent directories 
		// check if these are complete now and select in this case
		// proof only check boxes above
		for (int i = checkIndex -1;i >= 0; i--) {
			// get the next check box above:
			if (checkPanel.getComponent(i) instanceof JButton) { // unzip button
				continue;
			}
			JCheckBox nextCheck = (JCheckBox) checkPanel.getComponent(i);

			if (  (nextCheck instanceof PeaDirCheckBox) ) {
				String dirName = nextCheck.getName();
				if ( rootDirName.equals(dirName) ){ // next parent found
					// update directory to check: parent of current directory
					rootDirName = rootDirName.substring(0, rootDirName.lastIndexOf(File.separator));
					
					if (nextCheck.isSelected() == false) {
						// check if now all children are included:
						int completenessResult = checkDirectoryCompleteness(
								nextCheck, dirName, lastCheckedIndex);
						if (completenessResult >= 0) { // is complete
							// select complete directory
							nextCheck.setSelected(true);
							// update last checked index: 
							// next check can begin there
							lastCheckedIndex = completenessResult;
						} else { // deselected check box was found (or failure)
							// no parent of this directory can be complete, so break
							return;
						}
					}
				} else {
					if  ( ! nextCheck.getName().contains(rootDirName)){
						// reached another directory... 
						return;
					}
				}				
			} else if (nextCheck instanceof PeaCheckBox ){
				if (nextCheck.isSelected() == false){
					// directories are not full because of this file
					return;
				}				
			}
		}
	}
	
	/**
	 * Check if a directory check box includes all
	 * shown children
	 * 
	 * @param dirCheck		directory check box to check
	 * @param directoryName	name of the directory
	 * @param index			index of selected file check box:
	 * 						check only below this index
	 * 
	 * @return		the index of the last check box that
	 * 				was checked by this call or -1 if the
	 * 				directory is not complete (and failure)		
	 */
	private int checkDirectoryCompleteness(JCheckBox dirCheck, 
			String directoryName, int index){
		
		if (dirCheck == null) {
			new UnexpectedValueException("check", "JCheckBox", "is null").printDescription();
			return -1;
		}
		if ( ! (dirCheck instanceof PeaDirCheckBox) ) {
			new UnexpectedValueException("check", "JCheckBox", "is not valid directory color").printDescription();
			return -1;
		}		
		if (index < 0) {
			new UnexpectedValueException("index", "int", "is negative, check box missing").printDescription();
			return -1;
		}
		int componentNumber = checkPanel.getComponentCount();
		// check check boxes among the given check box
		for (int i = index; i < componentNumber; i++) {
			// get check box below
			JCheckBox nextCheck = (JCheckBox) checkPanel.getComponent(i);
			// check for deselected files or directories:
			if (nextCheck.getName().contains(directoryName)){				
				//if (nextCheck.getBackground().equals(fileColor)
				if (nextCheck instanceof PeaCheckBox 
						|| (nextCheck instanceof PeaDirCheckBox) ) {
					if (nextCheck.isSelected() == false) {
						// not selected check box found: break search
						return -1;
					} 					
				} 			
			} else {
				// file of another directory was reached:
				// no deselected child found
				return i; // index of last checked check box
			}
		}
		// no deselected child found
		return componentNumber;
	}
	
	/**
	 * Expensive helper function to find the
	 * index of the JCheckBox associated with a file name. 
	 * 
	 * @param fileName	the file name
	 * @return			the index of the check box
	 * 					for this file name or -1 if not found
	 */
	private int getIndexOfCheckBox(String fileName) {
		int len = checkPanel.getComponentCount();
		// iterate the checkPanel to find the file name
		for (int i = 0; i < len; i++) {
			if (checkPanel.getComponent(i) instanceof JButton) { // unzip button
				continue;
			}
			String checkName = ((JCheckBox) checkPanel.getComponent(i)).getName();
			if (checkName.equals(fileName)){
				return i;
			}
		}
		if (fc.isPrintErrors() == true) {
			TestLog.e(FileTypePanel.class, "File name not found in check box: " + fileName);
		}
		return -1; // not found
	} 

    /**
     * Set new text for all check boxes. 
     * Note: the check boxes must have been sorted
     * before, the new texts must have the same order
     * 
     * @param newTexts	the new text for the check boxes
     */
    public void updateCheckBoxTexts(String[] newTexts){
    	// number of all check boxes 
    	int checkNumber = checkPanel.getComponentCount();
    	int fileNumber = newTexts.length;

    	for (int i = 0, j = 0; i < checkNumber && j < fileNumber; i++,j++) {
    		if( checkPanel.getComponent(i) instanceof JButton) {
    			j--;
    			continue;
    		}
    		JCheckBox check = (JCheckBox) checkPanel.getComponent(i);
    		// check if the order is the same:
    		String checkBoxFileName = check.getName().substring(check.getName().lastIndexOf(File.separator), check.getName().length());
    		String fileName = newTexts[j].substring(newTexts[j].lastIndexOf(File.separator), newTexts[j].length());
    		if (! checkBoxFileName.equals(fileName)){
        		new UnexpectedValueException("newTexts[i]", "String", 
        				"is not equal to check box file name: "
        		+ newTexts[j] + ", " + check.getText()).printDescription();
        		return;
    		}
    		check.setText(newTexts[j]);
    	}
    	checkPanel.repaint();
    }

    /**
     * Get the all file names (selected, unselected,valid, invalid), 
     * as stored in TreeMap
     * 
     * @return	the actually file names as an array of strings
     */
    public final String[] getAllFileNames() {
    	
    	File[] allFiles = fc.getFileModel().getAllFiles();
    	int len = allFiles.length;
    	String[] allFileNames = new String[len];
    	for (int i = 0; i < len; i++) {
    		allFileNames[i] = allFiles[i].getAbsolutePath();
    	}
    	return allFileNames;
    }

    /**
     * Mark all unselected valid files
     * with the marker "-" at the
     * beginning of the annotation. 
     */
    protected void markUnselectedValidFiles(){
    	FileModel model = fc.getFileModel();
    	// number of all check boxes 
    	int validCheckNumber = checkPanel.getComponentCount();
    	//String[] allValidFileNames = new String[validCheckNumber];
    	for (int i = 0; i < validCheckNumber; i++) {
    		if( checkPanel.getComponent(i) instanceof JButton) {
    			continue;
    		}
    		JCheckBox check = (JCheckBox) checkPanel.getComponent(i);
    		File file = new File(check.getName());
    		String annotation = model.getAnnotation(file);
    		if (check.isSelected()){
    			model.setAnnotation(file,  annotation);
    		} else {
    			model.setAnnotation(file, "-" + annotation);
    		}	
    	}  
    }
    
	/**
	 *  Updates the root Window (JFrame, JDialog...) 
	 *  to show new files
	 */
	public final void updateWindow() {
		// get the root frame
    	Window frame = (Window) SwingUtilities.getRoot(this);
    	if (frame == null) { // this happens at start  if path file is set
    		if (cfp != null && cfp.getComponentCount() > 0){
    	    	cfp.invalidate();
    	    	cfp.validate();

    	    	//if ( ! this.isAncestorOf(cfpScroll)) {    	    		
    	    	//}
    		}
	    	checkPanel.invalidate();
	    	checkPanel.validate();
    		return;
    	}
    	// get current size:
    	Dimension dim = frame.getSize();
    	boolean invalidateFrame = false;

		if (cfp != null && cfp.getComponentCount() > 0){

	    	if (placeHolderPanel.getComponentCount() == 0) {// new 
	    		placeHolderPanel.add(cfpScroll);
    	    	placeHolderPanel.invalidate();
    	    	placeHolderPanel.validate();    	    	
    	    	invalidateFrame = true;
	    	}
	    	cfp.invalidate();
	    	cfp.validate();			
		}
    	checkPanel.invalidate();
    	checkPanel.validate();
    	
    	if (invalidateFrame == true) {
    		this.invalidate();
			this.validate();			
			frame.invalidate();
			frame.validate();
    	}
		// check if panel was larger than pack size
		if (frame.getPreferredSize().getWidth() >= dim.width
				|| frame.getPreferredSize().getHeight() > dim.height){
			frame.pack();    				
		} else {    				
			frame.setSize(dim);
		}
    }
    
    //==================== public methods ================================================

	/**
	 * Update the info label
	 */
	public void updateNumberAndSize(){
		infoLabel.setText(fc.getCurrentNumberAndSize());
	}	

    /**
     * Add a check box for a valid file to the checkPanel. 
     * The background color of the check box indicates  validity. 
     * 
     * @param fileName			the file name associated with this check box
     * @param annotation		the annotation for this file
     */
	protected final void addValidFileCheckBox(
    		String fileName, String annotation) { 
    	
    	PeaCheckBox newBox = new PeaCheckBox();    	
    	newBox.getAccessibleContext().
	    setAccessibleDescription(
	    	    "Clicking this component selects or deselects this file.");
    	//TestLog.o(FileTypePanel.class, "annotation: " +annotation);
    	if (annotation.startsWith("-")){
    		// check box was deselected before
    		annotation = annotation.substring(1,annotation.length());
    		fc.getFileModel().setAnnotation(new File(fileName), annotation);
    		newBox.setSelected(false);
    	} else {
    		newBox.setSelected(true);
    	}
    	newBox.setText(annotation + fileName);
    	newBox.setName(fileName);
    	newBox.setOpaque(true);
		newBox.addActionListener(this);
		newBox.setActionCommand("fileSelection");
		
		if(fc.isPlainModus() == true && fileName.endsWith(".zip")) {
			JButton unzipButton = new JButton(PeaProperties.getVmBridge().translate("unzip"));
			unzipButton.setToolTipText(PeaProperties.getVmBridge().translate("unzip") + ": " + fileName);
			unzipButton.setFont(new Font(Font.SANS_SERIF, Font.PLAIN, PeaProperties.getFontSize() - 1));
			//unzipButton.setMargin(new Insets(0,0,0,0));
			unzipButton.setBackground(Color.LIGHT_GRAY);
			//unzipButton.setPreferredSize(new Dimension( 40, 20));
			unzipButton.addActionListener(this);
			unzipButton.setActionCommand("unzip" + fileName);
			checkPanel.add(unzipButton);
		}
		//newBox.setBorder(LineBorder.createBlackLineBorder());
		checkPanel.add(newBox, gbc);
    }
	
    /**
     * Add a check box for a valid or invalid directory to the checkPanel. 
     * The background color of the check box indicates validity or invalidity.
     * 
     * @param fileName		the file name associated with this check box
     * @param annotation	the annotation for this file
     */
    protected final void addValidDirectoryCheckBox(
    		String fileName, String annotation){
    	
    	PeaDirCheckBox newBox = new PeaDirCheckBox();
    	newBox.getAccessibleContext().
    	setAccessibleDescription(
    			"Clicking this component selects or deselects this folder and all children.");
    	newBox.setName(fileName);
    	newBox.setOpaque(true);

    	if (annotation.startsWith("-")){
    		// check box was deselected before
    		annotation = annotation.substring(1, annotation.length());
    		fc.getFileModel().setAnnotation(new File(fileName), annotation);
    		newBox.setSelected(false);
    	} else {
    		newBox.setSelected(true);
    	}

    	newBox.addActionListener(this);
    	newBox.setActionCommand("dirSelection");
    	newBox.setText(annotation + fileName);
    	checkPanel.add(newBox, gbc);
    }
    
    /**
     * Update the view of files. This method should always
     * been called after check boxes are added or removed
     */
    public void updateFileView(){
    	if (checkPanel.getComponentCount() > 12) {
    		if (checkAllButtonPanel.getComponentCount() < 2) {
    			JCheckBox checkAll = new JCheckBox();
    			checkAll.setText(PeaProperties.getVmBridge().translate("all"));
    			checkAll.addActionListener(this);
    			checkAll.setActionCommand("checkAll");
    			checkAll.setSelected(true);
    			checkAllButtonPanel.add(Box.createHorizontalStrut(15));
    			checkAllButtonPanel.add(checkAll);
    			checkAllButtonPanel.add(Box.createHorizontalGlue());
    		}
    	} else {
    		if (checkAllButtonPanel.getComponentCount() > 2) {
    			checkAllButtonPanel.removeAll();
    		}
    	}
    	if (cfp != null ) {
    		//cfp.updateView();
    		cfp.invalidate();
    		cfp.validate();
    	}

    	checkPanel.invalidate();
    	checkPanel.validate();    	
    	infoLabel.setText(fc.getCurrentNumberAndSize());
    }
    
    /**
     * Get the shown files which are valid but not selected.
     * This is used to display a warning message when the File PEA
     * is closed and some files remain unencrypted
     * 
     * @return	the unselected valid file names
     * 			as an array of Strings
     */
    public final String[] getUnselectedValidFileNames() {
    	
    	// number of all check boxes 
    	int checkNumber = checkPanel.getComponentCount();
    	if (checkNumber == 0) {
    		TestLog.e(FileTypePanel.class, "no files displayed");
    		return null;
    	}
    	ArrayList<String> list = new ArrayList<String>();

    	for (int i = 0; i < checkNumber; i++) {
    		if (checkPanel.getComponent(i) instanceof JButton) {
    			continue;
    		}
    		JCheckBox check = (JCheckBox) checkPanel.getComponent(i);
    		if (check.isEnabled() == true	// no invalid files
    				&& check.isSelected() == false	// no selected files
    				//&& check.getBackground().equals(fileColor)) { // no directories
    			  && check instanceof PeaCheckBox ){
    			list.add(check.getName());
    		} 
    	}   	
    	if (list.isEmpty()){
    		return null;
    	} else {
    		// create the return value:
    		String[] result = new String[list.size()];
    		return list.toArray(result);
    	}
    }

    /**
     * Get the actual selected local file names, as stored in TreeMap. 
     * This ignores cloud files. 
     * 
     * @param selectedOnly	true: only selected file check boxes, 
     * 						false: also valid files 
     * @param filesOnly		true: only file, no folders
     * 						false: files and folders
     * 
     * @return	the actually selected file names as an array of strings
     */
    public final String[] getValidLocalFileNames(boolean selectedOnly, boolean filesOnly) {

    	// number of all check boxes 
    	int checkNumber = checkPanel.getComponentCount();
    	if (checkNumber == 0) {
    		//TestLog.e(FileTypePanel.class, "no files displayed");
    		return null;
    	}
    	ArrayList<String> list = new ArrayList<String>();

    	for (int i = 0; i < checkNumber; i++) {
    		if (checkPanel.getComponent(i) instanceof JButton) {
    			continue;
    		}
    		JCheckBox check = (JCheckBox) checkPanel.getComponent(i);
    		if (selectedOnly == true){
    			if (check.isSelected() == true) {
    				if (filesOnly == true) {
    					if ( ! (check instanceof PeaDirCheckBox) ) {
    						list.add(check.getName());
    					}
    				} else {
    					list.add(check.getName());
    				}
    			} 
    		} else {
				if (filesOnly == true) {
    				if ( ! (check instanceof PeaDirCheckBox) ) {
						list.add(check.getName());
					}
				} else {
					list.add(check.getName());
				}
    		}
    	}   	
    	if (list.isEmpty()){
    		TestLog.v(FileTypePanel.class, "No local file found");
    		return null;
    	} else {
    		// create the return value:
    		String[] result = new String[list.size()];
    		return list.toArray(result);
    	}
    }
    
    protected final void updateCheckBox(String fileName, String annotation, boolean isDirectory) {
		// get the index of the check box in checkPanel
		int index = getIndexOfCheckBox(fileName);
		JCheckBox check = (JCheckBox) checkPanel.getComponent(index);

		if (isDirectory == true) {
			if (annotation.contains(FileModel.getInvalidMarker())) {
	    		check.setSelected(false);
				check.setEnabled(false);
				check.setText(annotation + fileName);			
				checkPanel.remove(check);
				InvalidPanel.addInvalidDirectoryCheckBox(fileName, annotation);
			} else {
				check.setBorderPainted(true);
				check.setBorderPaintedFlat(true);
	//			check.setBackground(directoryColor);       
				check.addActionListener(this);
				check.setActionCommand("dirSelection");
				check.setText(annotation + fileName);
			}			
		} else { // file and other
			if (annotation.contains(FileModel.getInvalidMarker())) {
		    	check.setText(annotation + fileName);
		    	check.setSelected(false);
		    	check.setEnabled(false);
		    //	check.setBackground(invalidColor);
				if(fc.isPlainModus() == true && fileName.endsWith(".zip")) {	
					// remove zip button
					if (checkPanel.getComponent(index -1) instanceof JButton) {
						checkPanel.remove(index - 1);
						updateFileView();
					}
				}
				checkPanel.remove(check);
				InvalidPanel.addInvalidFileCheckBox(fileName, annotation);
			} else {
				check.setText(annotation + fileName);
		    	check.addActionListener(this);
		    	check.setActionCommand("fileSelection");				
			}
		}
    }
    
	/**
	 * Set an already shown valid check box invalid, number and
	 * size is updated
	 * 
	 * @param fileName			the file name stored in the map of the model
	 * @param newAnnotation		the new annotation for the file
	 * 
	 * @return	the previous index of the check box to set invalid
	 */
	public final int setCheckBoxInvalid(String fileName, String newAnnotation) {
		// untested
		File file = new File(fileName);
		FileModel model = fc.getFileModel();

		// get the index of the check box in checkPanel
		int index = getIndexOfCheckBox(fileName);

		if (index < 0) { // file name not found
			if (fc.isPrintErrors() == true) {
				new UnexpectedValueException("index", "int", 
						"is < 0, file name not found in checkPanel: "
								+ fileName).printDescription();
			}
		} 
		//String annotation = model.getAnnotation(file);
		JCheckBox check = (JCheckBox) checkPanel.getComponent(index);
		check.setText(fileName + FileModel.getInvalidMarker() + newAnnotation);
		check.setSelected(false);
		check.setEnabled(false);

		if (file.isFile() || file.exists() == false){    		
			//check.setBackground(invalidColor);
			check.setEnabled(false);
			if (check.getActionListeners() != null) {
				check.removeActionListener(this);
			}
			// update number and size
			if (fileName.endsWith(".zip")) {
				if (file.exists()) {
					model.increaseAllSize(new File(fileName).length());
				}
			} else {
				if (file.exists()) {
					model.increaseAllSize(FileTools.getFileSize(fileName) );
				}
			}
			model.increaseFileNumber( -1);
			if (fileName.endsWith(".zip")) {
				// remove unzip button
				checkPanel.remove(index -1);
			}
			InvalidPanel.addInvalidFileCheckBox(fileName, newAnnotation);

		} else if (file.isDirectory()){    		
			//check.setBackground(invalidDirectoryColor);
			check.setEnabled(false);
			if (check.getActionListeners() != null) {
				check.removeActionListener(this);
			}
			InvalidPanel.addInvalidDirectoryCheckBox(fileName, newAnnotation);
		} else {
			//
		}
		checkPanel.remove(check);
		updateFileView();
		updateWindow();
		this.repaint();
		return index;
	} 
	
	/**
	 * Removes an already shown valid check box invalid, number and
	 * size is updated
	 * 
	 * @param fileName			the file name stored in the map of the model
	 * @param previousFileSize	the previous size of the file (file may be wiped)
	 * 
	 * @return	the previous index of the check box to set invalid
	 */
	public final boolean removeCheckBox(String fileName, long previousFileSize) {
		// untested
		File file = new File(fileName);
		if (file.isDirectory()) {
			TestLog.ve(FileTypePanel.class, "Can only remove files", 5);
			return false;
		}
		FileModel model = fc.getFileModel();

		// get the index of the check box in checkPanel
		int index = getIndexOfCheckBox(fileName);

		if (index < 0) { // file name not found
			if (fc.isPrintErrors() == true) {
				new UnexpectedValueException("index", "int", 
						"is < 0, file name not found in checkPanel: "
								+ fileName).printDescription();
			}
		} 
		//String annotation = model.getAnnotation(file);
		JCheckBox check = (JCheckBox) checkPanel.getComponent(index);
		if (check != null) {
			boolean updateNumberAndSize = false;
			if ( check.isSelected()) {
				// need to calculate
				updateNumberAndSize = true;
			}
			check.removeActionListener(this);
			checkPanel.remove(check);
			if (fileName.endsWith(".zip")) {
				// remove unzip button
				checkPanel.remove(index -1);
			}		
			
			// update the model:
			model.removeFile(file, updateNumberAndSize, previousFileSize);
		} else {
			TestLog.ve(FileTypePanel.class, "Missing checkbox to remove", 5);
			return false;
		}
		updateFileView();
		updateWindow();
		this.repaint();
		return true;
	} 
	
	protected void removeAllCheckBoxes(){
		checkPanel.removeAll();
	}
 
     
    //========================================================================================
    // Getter & Setter
    
    /**
     * Get the FileComposer associated with this 
     * instance of FileTypePanel
     * 
     * @return	the FileComposer for this FileTypePanel
     */
    public final FileComposer getFileComposer(){
    	return fc;
    }
    
    /**
     * Set the FileComposer associated with this 
     * instance of FileTypePanel
     * 
     * @param 	the new FileComposer to set
     */
    public final void setFileComposer(FileComposer _fc) {
    	fc = _fc;
    } 
    
    /**
     * Get the panel to display files in the cloud
     * 
     * @return	the panel containing check boxes for cloud files
     */
    public final CloudFilePanel getCloudFilePanel() {
    	return cfp;
    }
    
    public final boolean isCloudFilePanelVisible() {
    	if (placeHolderPanel.getComponentCount() > 0) {
    		return true;
    	} else {
    		return false;
    	}
    }
    
    /**
     * Set the CloudFilePanel if not added before
     * 
     * @param _cfp	the CloudFilePanel to add
     */
    public final void setCloudFilePanel(CloudFilePanel _cfp) {
    	if ( ! PeaProperties.getFileType().equals("file")) {	
    		if (placeHolderPanel != null) {
    			placeHolderPanel.removeAll();
    			this.cfp = _cfp;
    			cfpScroll = new JScrollPane(_cfp);
    			cfpScroll.setPreferredSize(new Dimension(new Dimension(this.getSize().width - 20, 90)));	

    			if (_cfp.getComponentCount() > 0){ // only add when not empty
    				placeHolderPanel.add(cfpScroll);
    			}    	
    			placeHolderPanel.revalidate();
    		} else {
    			// set in PeaLockFrame
    			this.cfp = _cfp;
    		}
    	}
    }
    
    /**
     * Remove the button to open a FileChooser
     */
    public final void removeAddButton() {
    	if (buttonPanel != null) {
    		this.remove(buttonPanel);
        	this.validate();
        	this.invalidate();
    	}
    }

    /**
     * Check if a file already exists and ask to overwrite
     * 
     * @param file	the file to check
     * @param rememberCheck	true: add a check box "remember decision"
     * 
     * @return	true, if the file should be overwritten or
     * 			if the file does not exist, 
     * 			false if the file should not be written
     */
	public boolean askToOverwriteFile(File file, boolean rememberCheck) {
		if (file.exists() == true) {
			JPanel panel = new JPanel();
			panel.setLayout(new BoxLayout(panel, BoxLayout.PAGE_AXIS));
			panel.add(new JLabel(PeaProperties.getVmBridge().translate("overwrite_existing_file") +":"));
			panel.add(new JLabel(file.getAbsolutePath()));
			panel.add(Box.createVerticalStrut(5));
			if (rememberCheck == true) {
				JCheckBox check = new JCheckBox(PeaProperties.getVmBridge().translate("remember_decision"));			
				check.setSelected(false);
				panel.add(check);
				check.addActionListener(this);
				check.setActionCommand("rememberDecision");
			}
			Component comp = null;
			if (PswDialogView.getView() != null && PswDialogView.getView().isVisible() == true) {
				comp = PswDialogView.getView();
			} else {
				if (PeaControl.getDialog() != null && PeaControl.getDialog().getLockFrame() != null) {
					comp = (Component) PeaControl.getDialog().getLockFrame();
				}
			}
			int result = PeaDialog.showQuestion(comp, panel, null, 0);
			if (result == 0) { // yes
				return true;	
			} else if (result == 1){ // no
				return false;
			} else { // cancel
				return false;
			}
		} else {
			return true;
		}
	}

	//================================================
	// ProgrssBar to show progress: methods and  inner class
    /**
     * Start the progress verticalBar
     */
	public final void startProgressTask(){
		task = new ProgressTask();
		task.execute();
	}	
	
	/**
	 * Update the progress verticalBar
	 * 
	 * @param value		the new value to show
	 */
	public final void setProgressValue(int value){
		if (task != null) {
			task.setValue(value);
		}
	} 
	
	/**
	 * Close the progress verticalBar
	 */
	public final void closeProgressTask(){
		if (task != null) {
			task.close();
		}
	} 

	/**
	 * Progress verticalBar to display the encryption/decryption progress
	 * for many or large files
	 */
	private class ProgressTask extends SwingWorker<Void, Void> {
		// TODO extra class
    	private JProgressBar threadProgressBar;
    	
    	private JDialog dialog;
    	
    	// Show whether the process is finished: (set in close)
    	private boolean processFinished = false;

        @Override
        public Void doInBackground() {
        	
    		//TestLog.o(FileTypePanel.class, " eventThread: " + SwingUtilities.isEventDispatchThread() );
        	try { //wait 0.5s
        		Thread.sleep(500);
        	} catch (InterruptedException e) {
        		TestLog.e(FileTypePanel.class, "progressTask: thread interrupted " + e.toString());
        		return null;
        	}
        	// check if process is done:
        	if (processFinished == false) {
               	String modusText = ""; // used in title of dialog
            	if (ftp.getFileComposer().isPlainModus() == true) {
            		modusText = PeaProperties.getVmBridge().translate("encryption");//"encryption";
            	} else {
            		modusText = PeaProperties.getVmBridge().translate("decryption");
            	}
            	// create progress verticalBar: 
            	//threadProgressBar = new JProgressBar();
        		threadProgressBar = new JProgressBar(0,100);
        		threadProgressBar.setValue(1);
        		threadProgressBar.setStringPainted(true);
        		threadProgressBar.setPreferredSize(new Dimension(400, 30));        		
            	
            	JPanel contentPane = new JPanel();
            	contentPane.setLayout(new BoxLayout(contentPane, BoxLayout.Y_AXIS));
            	contentPane.add(threadProgressBar);
            	
            	JOptionPane pane = new JOptionPane( contentPane,//threadProgressBar, 
            	   JOptionPane.PLAIN_MESSAGE, 
            	    JOptionPane.DEFAULT_OPTION);

            	dialog = pane.createDialog(ftp, 
            			PeaProperties.getVmBridge().translate("file") + " "
            			+ modusText + " "
            			+ PeaProperties.getVmBridge().translate("in_progress"));// title

            	dialog.setCursor(CursorManager.getWaitCursor());
            	dialog.pack();
            	if (PswDialogView.getView().isVisible()){
            		dialog.setLocationRelativeTo(PswDialogView.getView());
            	}
            	dialog.setVisible(true);

        	} else {
        		processFinished = false;
        	}
            return null;
        }	 
        
        /**
         * Closes the progress verticalBar. If it wasn't started, nothing happens. 
         */
        private void close() { // is called when work is done
        	processFinished = true; // if the dialog is not already open: do not open
        	if (dialog != null) {
        		dialog.dispose(); 
        		task.cancel(true);
        	}
        }
        
        /**
         * Sets a new value to show in the progress verticalBar. 
         * If the verticalBar wasn't started, nothing happens. 
         * @param value	 the value to show
         */
        public void setValue(int value){

        	if (threadProgressBar != null){
	        	threadProgressBar.setValue(value);
	        	if (threadProgressBar.getGraphics() != null) {
	        		threadProgressBar.paint(threadProgressBar.getGraphics());
	        	}
        	} 
        }
    }


    /**
     * Get the salt associated with this FileTypePanel
     * 
	 * @return the salt
	 */
	public byte[] getSalt() {
		return fc.getFileModel().getSalt();
	}

	/**
	 * Get the algorithm and parameter string
	 * associated with this FileTypePanel
	 * 
	 * @return the algoParamString
	 */
	public byte[] getAlgoParamString() {
		return fc.getFileModel().getSchemeMarker();
	}


}