package cologne.eck.all_peas.files;

/*
 * PeaFactory - Production of Password Encryption Archives
 * Copyright (C) 2019  Axel von dem Bruch
 * 
 * This library is free software; you can redistribute it and/or modify it 
 * under the terms of the GNU General Public License as published 
 * by the Free Software Foundation; either version 2 of the License, 
 * or (at your option) any later version.
 * This library is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
 * or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU General Public License for more details.
 * See:  http://www.gnu.org/licenses/gpl-2.0.html
 * You should have received a copy of the GNU General Public License 
 * along with this library.
 */

/**
 * This panel holds the names of encrypted files from cloud servers as check boxes.
 * This panel is to be added to the FileTypePanel. 
 * Cloud providers are read from cloud property file. 
 * Invalid becoming file names are removed and added to the invalidPanel.
 */


import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import cologne.eck.all_peas.cloud.CloudControl;
import cologne.eck.all_peas.cloud.CloudFileModel;
import cologne.eck.all_peas.control.PeaControl;
import cologne.eck.tools.Converter;
import cologne.eck.tools.TestLog;
import cologne.eck.tools.UnexpectedValueException;

import java.awt.Color;
import java.awt.Component;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Window;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;


@SuppressWarnings("serial")
public class CloudFilePanel extends JPanel {
	
	private CloudFilePanel cfp;
	private Window rootWindow;
	
	private CloudControl cc;
	
	private CloudFileModel cfm;
	private FileTypePanel ftp;
	
	private GridBagConstraints gbc;
	
	/**
	 * Panel to show encrypted file names from several cloud providers. 
	 * The file names are stored in cloud.properties. 
	 * 
	 * @param _rootWindow	the root window to add the CloudFilePanel to
	 * @param _cloudFileMap	the map of providers and files to fill the CloudFilePanel
	 */
	public CloudFilePanel(Window _rootWindow, FileTypePanel _ftp, //HashMap<String, HashSet<String>> _cloudFileMap, 
			String salt, String algoParams) {
		if (salt == null || algoParams == null) {
			//new UnexpectedValueException("HashSet","_cloudFileMap","is empty").printDescription();
			TestLog.e(CloudFilePanel.class, "Unexpected error when creating panel for cloud files");
			TestLog.ve(CloudFilePanel.class, "salt/algoParmas are null", 5);
			return;
		}
		cc = new CloudControl(this, salt, algoParams);
		cfm = cc.getCloudFileModel();
		this.rootWindow = _rootWindow;
		this.getAccessibleContext().
	    setAccessibleDescription(
	    	    "Contains selectable file from cloud providers");
		this.ftp =_ftp;	
		GridBagLayout grid = new GridBagLayout();
		this.setLayout(grid);//new BoxLayout(this, BoxLayout.PAGE_AXIS));
        gbc = new GridBagConstraints();
        gbc.ipady = 5;
  
        gbc.weightx = 1;
        gbc.fill = GridBagConstraints.HORIZONTAL;
        gbc.gridwidth = GridBagConstraints.REMAINDER;
		
		this.setBorder(new TitledBorder(new LineBorder(Color.GRAY, 1), "Cloud"));
		HashMap<String, HashSet<String>> initMap = cc.getInitialCloudFileMap();
		loadCloudFiles(initMap);
		cfp = this;
	}
	
	/** 
	 * Creates a new CloudFilePanel from the current CloudFileModel
	 * and sets it as CloudFilePanel in FileTypePanel
	 */
	public void updateView() {
		cfp = new CloudFilePanel(rootWindow, ftp, cfm.getSalt(), cfm.getAlgoParams());
		cfm = cfp.getCloudFileModel();
		cfp.revalidate();
		PeaControl.getDialog().getEncryptedFileTypePanel().setCloudFilePanel(cfp);
	}
	
	/**
	 * Load the files from cloud providers as check boxes
	 * @param cloudFileMap
	 */
	private void loadCloudFiles(HashMap<String, HashSet<String>> cloudFileMap) {
		// Load check boxes
		if (cloudFileMap == null || cloudFileMap.isEmpty()) {
			TestLog.v(CloudFilePanel.class, "cloud map empty");
			return;
		} else {			
			
			for (HashMap.Entry<String, HashSet<String>> entry : cloudFileMap.entrySet()) {
			    String key = entry.getKey();
			    HashSet<String> fileNamesSet = entry.getValue();
			    // convert and sort
			    if (fileNamesSet != null && fileNamesSet.isEmpty() == false) {
			    	ArrayList<String> fileNames = Converter.hashSetToArrayList(fileNamesSet);
			    	if (fileNames == null) {
			    		new UnexpectedValueException("ArrayList<String>", "fileNames", "is null").printDescription();
			    		return;
			    	}
			    	Collections.sort(fileNames);
			    	if (fileNames != null) {
			    		JLabel label = new JLabel("   *   " + key + "   *   ");
			    		label.setBackground(Color.WHITE);
			    		label.setBorder(new LineBorder(Color.GRAY, 2));
			    		label.setOpaque(true);
			    		this.add(label);

			    		if (fileNames != null) {
			    			for (String fileName : fileNames) {
			    				PeaCheckBox newBox = new PeaCheckBox();
			    				newBox.getAccessibleContext().
			    				setAccessibleDescription(
			    						"Clicking this component selects or deselects this cloud file.");
			    				newBox.setText(key + ": " + fileName);
			    				newBox.setOpaque(true);
			    				newBox.setBackground(new Color(245,245,245));// fileColor from FileTypePanel
			    				this.add(newBox, gbc);
			    			}
			    		}
			    	}
			    }
			}
		}
	}
	
	/**
	 * Add a new check box to the CloudFilePanel
	 * 
	 * @param providerName	the name of the provider
	 * @param fileName		the file name to add
	 * @param select		true: select the check box
	 */
	public void addFileToPanel(String providerName, String fileName, boolean select) {
//		TODO an die richtige Stelle?
		PeaCheckBox newBox = new PeaCheckBox();
    	newBox.getAccessibleContext().
	    setAccessibleDescription(
	    	    "Clicking this component selects or deselects this cloud file.");
    	newBox.setText(providerName + ": " + fileName);
    	newBox.setOpaque(true);
		newBox.setBackground(new Color(245,245,245));// fileColor from FileTypePanel
		newBox.setSelected(select);
		this.add(newBox, gbc);
	}
	
	public void selectFileInPanel(String providerName, String fileName) {
		String textToCheck = providerName + ": " + fileName;
		Component[] c = this.getComponents();
		int cLen = c.length;
		for (int i = 0; i < cLen; i++) {
			if (c[i] instanceof PeaCheckBox) {
				if ( ((JCheckBox) c[i]).getText().equals(textToCheck)) {
					((JCheckBox) c[i]).setSelected(true);
					return;
				}
			}			
		}		
	}

	/**
	 * Remove one check box (file) from the panel. The check box
	 * is identified by the provider name and the file name
	 * 
	 * @param provider			the provider name
	 * @param invalidFileName	the file name to remove
	 */
	public void removeFileFromPanel(String provider, String invalidFileName) {
		String textToCheck = provider + ": " + invalidFileName;
		Component[] c = this.getComponents();
		int cLen = c.length;
		for (int i = 0; i < cLen; i++) {
			if (c[i] instanceof JCheckBox) {
				if ( ((JCheckBox) c[i]).getText().equals(textToCheck)) {
					this.remove(c[i]);
					this.invalidate();
					this.validate();
					this.repaint();
					return;
				}
			}			
		}		
	}
	
	/**
	 * Remove one provider with all shown files from the panel. 
	 * 
	 * @param providerName	the provider name to be removed
	 */
	public void removeProvider(String providerName) {
		Component[] c = this.getComponents();
		int cLen = c.length;
		for (int i = 0; i < cLen; i++) {
			if (c[i] instanceof JCheckBox) {
				if ( ((JCheckBox) c[i]).getText().startsWith(providerName)) {
					this.remove(c[i]);
				}
			} else if (c[i] instanceof JLabel) {
				if ( ((JLabel) c[i]).getText().contains(providerName)) {
					this.remove(c[i]);
				}
			}
		}	
		this.invalidate();
		this.validate();
		this.repaint();
		return;
	}
	
	/**
	 * Get the selected cloud file names as a String of
	 * provider name - file name
	 * without the part tmp/provider
	 * 
	 * @return	an array of file names of this provider without tmp/provider or null
	 */
	public String[] getSelectedCloudFilesOfProvider(String providerName) {
		ArrayList<String> list = new ArrayList<String>();
		Component[] comps = this.getComponents();
		//TestLog.v(this.getClass(),"Provider: " + providerName);
		int compsLen = comps.length;
		for (int i = 0; i < compsLen; i++) {
			Component c = comps[i];
			if (c instanceof JCheckBox) {
				JCheckBox cb = (JCheckBox) c;
				if (cb.isSelected()) {
					String text = cb.getText();
					if (text.startsWith(providerName)) {
						text = text.substring(text.indexOf(":") + 1, text.length());
						//TestLog.v(this.getClass(),"Text of checkbox to add: " + text);
						list.add(text.trim());
					}
				}
			}
		}
		if (list.isEmpty()) {
			return null;
		} else {
			String[] fileNames = list.toArray(new String[0]);
			Arrays.sort(fileNames);
			return fileNames;
		}
	}

	public CloudFileModel getCloudFileModel() {
		return cfm;
	}

	public FileTypePanel getFileTypePanel() {
		return ftp;
	}

	public CloudControl getCloudControl() {
		return cc;
	}
}
