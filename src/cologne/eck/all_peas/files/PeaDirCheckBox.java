package cologne.eck.all_peas.files;

/*
 * Peafactory - Production of Password Encryption Archives
 * Copyright (C) 2020  Axel von dem Bruch
 * 
 * This library is free software; you can redistribute it and/or modify it 
 * under the terms of the GNU General Public License as published 
 * by the Free Software Foundation; either version 2 of the License, 
 * or (at your option) any later version.
 * This library is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
 * or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU General Public License for more details.
 * See:  http://www.gnu.org/licenses/gpl-2.0.html
 * You should have received a copy of the GNU General Public License 
 * along with this library.
 */

import java.awt.Color;
import java.awt.Font;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.JCheckBox;
import javax.swing.border.LineBorder;

import cologne.eck.all_peas.data.PeaProperties;


@SuppressWarnings("serial")
class PeaDirCheckBox extends JCheckBox implements MouseListener, ItemListener {
	
	private static final Color ROLLOVER_COLOR = new Color(215, 230, 225);//(235, 250, 245);
	private static final Color SELECTED_COLOR = new Color(240, 240, 240);//(250, 250, 250);
	private static final Color DESELECTED_COLOR = new Color(215,215,215);//(235, 235, 235);
	
	/**
	 * Checkbox for a valid directory
	 */
	public PeaDirCheckBox() {
		this.setOpaque(true);
		
		this.setBackground(SELECTED_COLOR);
		//this.setMargin(new Insets(10,10,10,10));
		this.setBorder(new LineBorder(Color.DARK_GRAY, 2));
		this.setBorderPainted(true);
		this.setBorderPaintedFlat(true);
		
		this.setRolloverEnabled(true);
		this.addMouseListener(this);
		this.addItemListener(this);
		this.getAccessibleContext().
	    setAccessibleDescription(
	    	    "Clicking this component selects or deselects the folder and all of its files and sub-folders.");
	}

	@Override
	public void mouseClicked(MouseEvent mc) {
		JCheckBox check = (JCheckBox)mc.getComponent();
		if (check.isSelected() == true) {
			this.setBackground(SELECTED_COLOR);
		} else {
			this.setBackground(DESELECTED_COLOR);
		}
	}
	
	@Override
	public void mouseEntered(MouseEvent arg0) {
		this.setBackground(ROLLOVER_COLOR);
		this.setFont(new Font(Font.SANS_SERIF, Font.BOLD, PeaProperties.getFontSize()));
	}
	
	@Override
	public void mouseExited(MouseEvent me) {

		this.setFont(new Font(Font.SANS_SERIF, Font.PLAIN, PeaProperties.getFontSize()));

		if (((JCheckBox)me.getComponent()).isSelected() == true) {
			this.setBackground(SELECTED_COLOR);
		} else {
			this.setBackground(DESELECTED_COLOR);
		}		
	}
	
	@Override
	public void mousePressed(MouseEvent arg0) {}
	@Override
	public void mouseReleased(MouseEvent arg0) {}	

	@Override
	public void itemStateChanged(ItemEvent e) {
		 if (e.getStateChange() == ItemEvent.SELECTED) {
			 this.setBackground(SELECTED_COLOR);
		 } else if (e.getStateChange() == ItemEvent.DESELECTED) {
			 this.setBackground(DESELECTED_COLOR);
		 }		
	}
}
