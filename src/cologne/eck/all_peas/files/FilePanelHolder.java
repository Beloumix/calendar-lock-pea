package cologne.eck.all_peas.files;

/*
 * Peafactory - Production of Password Encryption Archives
 * Copyright (C) 2020  Axel von dem Bruch
 * 
 * This library is free software; you can redistribute it and/or modify it 
 * under the terms of the GNU General Public License as published 
 * by the Free Software Foundation; either version 2 of the License, 
 * or (at your option) any later version.
 * This library is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
 * or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU General Public License for more details.
 * See:  http://www.gnu.org/licenses/gpl-2.0.html
 * You should have received a copy of the GNU General Public License 
 * along with this library.
 */


import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.ButtonGroup;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.SwingUtilities;
import javax.swing.border.EmptyBorder;

import cologne.eck.all_peas.bridge.VMBridge;
import cologne.eck.all_peas.cloud.CloudControl;
import cologne.eck.all_peas.data.AttachmentHandler;
import cologne.eck.all_peas.data.PeaProperties;
import cologne.eck.all_peas.gui.IconManager;
import cologne.eck.all_peas.gui.PeaDialog;
import cologne.eck.all_peas.gui.PeaLockFrame;
import cologne.eck.all_peas.gui.PswDialogView;
import cologne.eck.all_peas.vm_specific.JREProperties;
import cologne.eck.peafactory.crypto.AlgoParamHandler;
import cologne.eck.peafactory.crypto.kdf.KeyDerivation;
import cologne.eck.tools.Converter;
import cologne.eck.tools.TestLog;
import cologne.eck.tools.UnexpectedValueException;


@SuppressWarnings("serial")
public class FilePanelHolder extends JPanel 
		implements ActionListener, 	// for info buttons
		ItemListener {				// for radio buttons

	// list of buttons: same index as other lists
	private static ArrayList<JRadioButton> buttonList = new ArrayList<JRadioButton>();
	// list of FileTypePanels
	private static ArrayList<FileTypePanel> ftpList = new ArrayList<FileTypePanel>();
	// list of FileModel: same index as other lists
	private static ArrayList<FileModel> modelList = new ArrayList<FileModel>();
	// list of FileComposer: same index as other lists
	private static ArrayList<FileComposer> composerList = new ArrayList<FileComposer>();
	// group of radio buttons with ItemListener
	private static ButtonGroup group = new ButtonGroup();
	
	// this instance
	private static FilePanelHolder fph;
	/**
	 * Panel to hold the FileTypePanels from ftpList
	 */
	private static JPanel ftpBasePanel;
	/**
	 * If only one FileTypePanel is present, the panel
	 * with radio and info button get invisible,
	 * otherwise this is null
	 */
	private static JPanel invisibleButtonPanel;
	/**
	 * Placeholder for the InvalidPanel
	 */
	private static JPanel invalidBasePanel;
	/**
	 * Panel to show invalid files
	 */
	private static InvalidPanel invalidPanel;
	
	/**
	 * The index of the selected FileTypePanel. 
	 * All checkboxes in this panel are selected 
	 * an enabled per default, all other checkboxes 
	 * are disabled.
	 */
	private static int currentIndex = -1;
	/*
	 * show invalid panel (invalid files occurred) or not
	 */
	private static boolean showInvalidPanel = false;
	
	// to change language:
	private static JPanel statusPanel;
	
	/**
	 * Panel that holds several FileTypePanel, one for each category
	 * depending on salt, algorithms and parameters. 
	 */
	private FilePanelHolder() {
		fph = this;
		JPanel contentPane = new JPanel();
		contentPane.setLayout(new BoxLayout(contentPane, BoxLayout.PAGE_AXIS));
		
		int[] cipherColor = PeaProperties.getCipherPeaColor();
		this.setBackground(new Color(cipherColor[0], cipherColor[1], cipherColor[2]));
		ftpBasePanel = new JPanel();
		ftpBasePanel.setLayout(new BoxLayout(ftpBasePanel, BoxLayout.PAGE_AXIS));
		
		invalidBasePanel = new JPanel();
		invalidBasePanel.setLayout(new BoxLayout(invalidBasePanel, BoxLayout.PAGE_AXIS));
		
		this.getAccessibleContext().
	    setAccessibleDescription(
	    	    "Encrypted files that can be opened together are grouped.");
		
		contentPane.add(ftpBasePanel);
		contentPane.add(invalidBasePanel);
		
		VMBridge bridge = PeaProperties.getVmBridge();
		String status =  bridge.translate("status_encrypted");
		ImageIcon statusIcon = IconManager.loadIcon("locked.png", bridge.translate("closed_padlock"), 
					null, bridge.translate("encrypted_indicator") ); 
		
		if (status != null || statusIcon != null) {
			statusPanel= new JPanel();
			statusPanel.setLayout(new BoxLayout(statusPanel, BoxLayout.X_AXIS));
			statusPanel.setMaximumSize(new Dimension((int)PswDialogView.getScreenwidth(), 30));
			int[] colorValues = PeaProperties.getCipherPeaColor();
			statusPanel.setBackground(new Color(colorValues[0], colorValues[1], colorValues[2]));
			if (statusIcon != null) {
				statusPanel.add(Box.createHorizontalGlue());
				statusPanel.add(new JLabel(statusIcon));
				statusPanel.add(Box.createHorizontalStrut(10));
			}		
			JLabel statusLabel= new JLabel(status);
			statusLabel.setFont(new Font(Font.MONOSPACED, Font.ITALIC, PeaProperties.getFontSize()));
			statusPanel.add(statusLabel);
			this.add(statusPanel);
		}

		JScrollPane scroll = new JScrollPane(ftpBasePanel);//contentPane);
		scroll.setPreferredSize(new Dimension(500,400));//scrollWidth, scrollHeight));		
		this.add(scroll);
		fph.setLayout(new BoxLayout(fph,BoxLayout.PAGE_AXIS));
		invalidPanel = InvalidPanel.getInstance(SwingUtilities.getWindowAncestor(fph));
		fph.setMaximumSize(new Dimension((int) PswDialogView.getScreenwidth() - 200, (int) PswDialogView.getScreenheight() - 200));
		//fph.setMinimumSize(new Dimension(400, 50));
		int prefBarWidth = scroll.getVerticalScrollBar().getPreferredSize().width;
		//System.out.println(fph.getPreferredSize().width);
		//fph.setPreferredSize(new Dimension (500,300));
		fph.setPreferredSize(new Dimension (fph.getPreferredSize().width+(prefBarWidth * 3),300));
	}

	/**
	 * Get the static instance of FilePanelHolder
	 * that holds all FileTypePanels
	 * 
	 * @return	instance of FilePanelHolder
	 */
	public static FilePanelHolder getInstance() {
		if (fph == null) {
			fph = new FilePanelHolder();
			// fill the FileTypePanel with previous stored file names
			AttachmentHandler.initFileNames();
		}
		if (ftpList.isEmpty() == false 
				&& PswDialogView.getView() != null
				&& PswDialogView.getView().isVisible()) { // at least one file is present
			if (KeyDerivation.getSalt() == null) {
				new UnexpectedValueException("byte", "KeyDerivation.getSalt()", "is null - was not set").printDescription();
			}			
		} else {

		}
		return fph;		
	}
	
	/**
	 * Reset the language to the current language bundle
	 */
	public static void resetLanguage(){
		//statusLabel.setText(JREProperties.getVmBridge().translate("status_encrypted"));
		//TestLog.v(FilePanelHolder.class, "!!!!!!!!!!REST LANG");
		if (JREProperties.getMainWindow() instanceof PswDialogView) {
			statusPanel.removeAll();
			VMBridge bridge = PeaProperties.getVmBridge();
			String status =  bridge.translate("status_encrypted");
			ImageIcon statusIcon = IconManager.loadIcon("locked.png", bridge.translate("closed_padlock"), 
					null, bridge.translate("encrypted_indicator") ); 

			if (status != null || statusIcon != null) {
				int[] colorValues = PeaProperties.getCipherPeaColor();
				statusPanel.setBackground(new Color(colorValues[0], colorValues[1], colorValues[2]));
				if (statusIcon != null) {
					statusPanel.add(Box.createHorizontalGlue());
					statusPanel.add(new JLabel(statusIcon));
					statusPanel.add(Box.createHorizontalStrut(10));
				}		
				JLabel statusLabel= new JLabel(status);
				statusLabel.setFont(new Font(Font.MONOSPACED, Font.ITALIC, PeaProperties.getFontSize()));
				statusPanel.add(statusLabel);
			}

			for(FileTypePanel ftp : ftpList) {
				ftp.updateNumberAndSize();
			}
			if (invalidPanel != null) {
				invalidPanel.resetLanguage();
			}
		} else if (JREProperties.getMainWindow() instanceof PeaLockFrame) {
			// do nothing: Everything is done in PeaLockFrame.resetFrame()
		}
	}

	/**
	 * Add a file from cloud or local to a panel and set this panel as typePanel.
	 * This method does not revalidate the view. 
	 * 
	 * @param file	the file to add
	 * @param index	the index of the panel
	 * 
	 * @return	true if the view must be updated
	 */
	public static boolean addFile(File file, int index) {
		
		if (index < 0) {
			TestLog.ve(FilePanelHolder.class, "Invalid index of FileTypePanel: " + index, 5);
			TestLog.e(FilePanelHolder.class, "Can't add file: " + file.getAbsolutePath());
			return false;
		}
		
		boolean added = false;
		if (file.getAbsolutePath().startsWith(CloudControl.getTmpDir())) {
			// this is a cloud file
			String providerName = CloudControl.getProviderNameFromTmpFile(file.getAbsolutePath());
			if (providerName != null) {
				// add to CloudFilePanel
				String[] fileNameArray = {file.getName()};
				added = ftpList.get(index).getCloudFilePanel().getCloudControl().addInitialFileNamesToProvider(
						providerName, fileNameArray, false);
			}
		} else {
			//TestLog.o(FilePanelHolder.class, "add " + file.getAbsolutePath() + " to index " + index);
			FileComposer fc = composerList.get(index);
			added = fc.addOrUpdateFile(file, false);
		}
		JREProperties.setTypePanel(ftpList.get(index));
		//currentIndex = index;
		setCurrentIndex(index);
		return added;
	}
	
	/**
	 * Add a new scheme with FileTypePanel, FileComposer and
	 * FileModel for a given schemeMarker and salt. 
	 * 
	 * @param file			the file of this scheme
	 * @param algoParam	the scheme marker from the file
	 * @param salt			the salt from the file
	 * 
	 * @return	true, if the scheme was added, false if the file
	 * 			was invalid (pswIdentifier...)
	 */
	public static boolean addScheme(File file, byte[] algoParam, byte[] salt) {
		
		if (PeaProperties.isCommandLineMode() == true) {
			TestLog.ve(FilePanelHolder.class, "Invalid method call", 5);
			return false;
		}

		if( file == null) {
			TestLog.ve(FilePanelHolder.class, "Missing file for scheme", 5);
			return false;
		}
		if (algoParam == null) {
			TestLog.ve(FilePanelHolder.class, "Missing algos for scheme", 5);
			return false;
		}
		if (salt == null) {
			TestLog.ve(FilePanelHolder.class, "Missing salt for scheme", 5);
			return false;
		}
		boolean newSchemeAdded = false;
		FileModel model = new FileModel(true, algoParam, salt);
		FileComposer fc = new FileComposer(model);
		fc.setPlainModus(false);// encrypted files
		FileTypePanel encryptedFileTypePanel =  new FileTypePanel(
				350, 250, false, false, 
				fc, SwingUtilities.getWindowAncestor(fph));		
		boolean added = false;
		if (file.getAbsolutePath() != null) {
			if (file.getAbsolutePath().startsWith(CloudControl.getTmpDir())) {
				// this is a cloud file
				String providerName = CloudControl.getProviderNameFromTmpFile(file.getAbsolutePath());
				if (providerName != null) {
					// add to CloudFilePanel
					String[] fileNameArray = {file.getName()};
					if (encryptedFileTypePanel.getCloudFilePanel() == null) {
						TestLog.e(FilePanelHolder.class, "cloud file panel is null");
					}
					if (encryptedFileTypePanel.getCloudFilePanel().getCloudControl() == null) {
						TestLog.e(FilePanelHolder.class, "instance is null");
					}
					added = encryptedFileTypePanel.getCloudFilePanel().getCloudControl().addInitialFileNamesToProvider(
							providerName, fileNameArray, false);
				}
			} else {
				added = fc.addOrUpdateFile(file, false);
			}
		}
		if (added == true) {// && model.getFileNumber() > 0) {
			
			modelList.add(model);
			composerList.add(fc);
			ftpList.add(encryptedFileTypePanel);
			String tooltip = AlgoParamHandler.getReadableString(new String (algoParam, AttachmentHandler.getASCIICharset()), false);
			JPanel panel= new JPanel();
			panel.setLayout(new BoxLayout(panel, BoxLayout.LINE_AXIS));

			JRadioButton radio = new JRadioButton("   ");
			// enlarge the clickable area of radio buttons
			radio.setFont(new Font(Font.SANS_SERIF, Font.PLAIN, PeaProperties.getFontSize() * 2));
			radio.setHorizontalTextPosition(JRadioButton.RIGHT);//SwingConstants.CENTER);
			radio.setVerticalTextPosition(JRadioButton.TOP);
			radio.setToolTipText(tooltip);
			radio.getAccessibleContext().
		    setAccessibleDescription(
		    	    "Clicking this component selects the files in this panel to decrypt.");
			radio.setSelected(true);
			//radio.addActionListener(fph);
			radio.addItemListener(fph);
			int index = ftpList.size() - 1;
			//radio.setActionCommand("" + index);
			radio.setName("" + index);
			//radio.setMaximumSize(radio.getPreferredSize());
			group.add(radio);
			buttonList.add(radio);
			//panel.add(radio);
			//panel.add(Box.createHorizontalStrut(10));
			Icon infoIcon = IconManager.loadIcon("info.png", "info", 
					"show info", "show infos about used algorithms" );
			JButton infoButton = new JButton(infoIcon);//new ImageIcon("resources" + File.separator + "info.png"));
			// empty border is needed to show focus:
			infoButton.setBorder(new EmptyBorder(2,2,2,2));
			//infoButton.setMaximumSize(infoButton.getPreferredSize());
			infoButton.getAccessibleContext().
		    setAccessibleDescription(
		    	    "Clicking this component shows a dialog with used algoritms of related files.");
			infoButton.addActionListener(fph);
			infoButton.setActionCommand("info" + index);//new String (schemeMarker, AttachmentHandler.getASCIICharset()));

			JPanel buttonPanel = new JPanel();
			buttonPanel.setLayout(new BoxLayout(buttonPanel, BoxLayout.PAGE_AXIS));
			buttonPanel.add(radio);
			buttonPanel.add(infoButton);
			buttonPanel.add(Box.createRigidArea(new Dimension(5, 5)));
			buttonPanel.add(Box.createVerticalGlue());
			panel.add(buttonPanel);
			panel.add(encryptedFileTypePanel);			
			ftpBasePanel.add(panel);//encryptedFileTypePanel);
			// disable all check boxes:
			encryptedFileTypePanel.disableCheckBoxes();
			newSchemeAdded = true;
		} else {
			//TestLog.o(FilePanelHolder.class, "No changes for " + file.getAbsolutePath());
		}
		return newSchemeAdded;
	}
	
	@Override
	public void itemStateChanged(ItemEvent isc) {
		
		if (isc.getSource() instanceof JRadioButton) {
			JRadioButton rb = (JRadioButton) isc.getSource();
			if (rb.isSelected()) { // otherwise an action is performed for two radio buttons
				String indexString = rb.getName();
				if (indexString.matches("-?\\d+")) { // index of panel:

					int index = Integer.parseInt(indexString);
					if (index  < 0 ) {
						new UnexpectedValueException("int", "index", "is -1 for: " + rb.getName()).printDescription();
						return;
					}

					if (index != currentIndex && currentIndex >= 0) {
						ftpList.get(currentIndex).disableCheckBoxes();
						ftpList.get(index).enableCheckBoxes();
					}
					setCurrentIndex(index);
					//TestLog.e(getClass(), "XXXXXXXXXXXXXXXXXX "+ PeaProperties.isKeyFileOnlyMode());// TODO
					if (PeaProperties.isKeyFileOnlyMode() == true) {
						PswDialogView.showHideDirectStartPanel(true);
					} else {
						PswDialogView.showHideDirectStartPanel(false);
					}
				} else {
					new UnexpectedValueException("String", "indexString", "is not a number").printDescription();
				}
			}
		} else {
			new UnexpectedValueException("Object", "isc.getSource()", "not instanceof JRadioButton").printDescription();
		}		
	}
	
	/**
	 * Set the current index of the selected
	 * FileTypePanel (synchronized)
	 * 
	 * @param newIndex	the new current index
	 */
	private static synchronized void setCurrentIndex(int newIndex) {
		if (newIndex < 0) {
			TestLog.ve(FilePanelHolder.class, "Invalid index of FileTypePanel: " + newIndex, 5);
			TestLog.e(FilePanelHolder.class, "Invalid Index: " + newIndex);
			return;
		}
		FilePanelHolder.currentIndex = newIndex;
		JREProperties.setTypePanel(ftpList.get(newIndex));
		if (modelList.get(newIndex).getSalt() != null) {			
			KeyDerivation.setSalt(modelList.get(newIndex).getSalt());
		} else {
			TestLog.ve(FilePanelHolder.class, "Missing salt..", 5);
			new UnexpectedValueException("byte[]", "salt", "is null for index "+ newIndex).printDescription();
		}
		if (modelList.get(newIndex).getSchemeMarker() != null) {
			byte[] marker = modelList.get(newIndex).getSchemeMarker();
			PeaProperties.setAlgoParamString(marker);
			AlgoParamHandler.readString(new String(marker, AttachmentHandler.getASCIICharset()), false);
		} else {
			new UnexpectedValueException("byte[]", "schemeMarker", "is null for index " + newIndex).printDescription();
		}
	}
	
	@Override
	public void actionPerformed(ActionEvent ae) {
		
		String command = ae.getActionCommand();
		//TestLog.o(FilePanelHolder.class, command);
		if (command.startsWith("info")) {
			command = command.substring(4, command.length());
			if (command.matches("-?\\d+")) { // index of panel:
				int index = Integer.parseInt(command);
			byte[][] marker = AttachmentHandler.getScheme(index);
			String version = "";
			if (Arrays.equals(marker[0], AttachmentHandler.getVersion1ID())) {
				version = PeaProperties.getVmBridge().translate("version") + " 1";
			} else if (Arrays.equals(marker[0], AttachmentHandler.getVersion2ID())) {
				version = PeaProperties.getVmBridge().translate("version") + " 2";
			}
			String algoParam = AlgoParamHandler.getReadableString(
					new String(marker[1], AttachmentHandler.getASCIICharset()), true);
			String saltString =  "Salt: " + Converter.bytes2hex(marker[2]);
			String message = version + "\n\n" + algoParam + "\n\n" + saltString;
			PeaDialog.showMessage(fph, message);
			}
		} else {
			// not an info button
		}
	}
	
	/**
	 * Get the selected index in all lists 
	 * (ftpList, buttonList, modelList, composerList)
	 * 
	 * @return the selected index in all lists or -1 if there are no elements
	 */
	private static int getSelectedIndexFromView(){
		
		if (ftpList.size() == 0) {
			return -1;
		} else if (ftpList.size() == 1 && modelList.size() == 1) {
			//there are no buttons:
			return 0;
		} else {
			//int currentIndex = -1;
			int len = group.getButtonCount();
			if (len != ftpList.size() || len != buttonList.size()){
				new UnexpectedValueException("int", "len", "differs from list sizes, len: " + len 
					+ ", panelList: " + ftpList.size() + ", buttonList: " + buttonList.size())
				.printDescription();
			}		
			for (int i = 0; i < len; i++) {
				if (buttonList.get(i).isSelected() == true) {
					currentIndex = i;					
					return i;
				}
			}
			// there must be one scheme selected:
			if (currentIndex == -1) {
				buttonList.get(0).setSelected(true);
				currentIndex = 0;//TODO test
			}
			setCurrentIndex(currentIndex);
		}
    	TestLog.e(FilePanelHolder.class, "No selected button found...");
    	return -1;
	}
	
	/**
	 * Get the currently selected (enabled) FileTypePanel
	 * 
	 * @return	the currently selected FileTypePanel
	 */
	public static FileTypePanel getSelectedPanel() {
		
		int index = currentIndex;//getSelectedIndex();
		if (index > -1) {
			return ftpList.get(index);
		} else {			
			return null;
		}
	}
	
	/**
	 * Get the currently selected FileModel
	 * 
	 * @return	the currently selected FileModel
	 */
	public static FileModel getSelectedModel() {
		int index = currentIndex;//getSelectedIndex();
		if (index > -1) {
			return modelList.get(index);
		} else {			
			return null;
		}
	}
	
	/**
	 * Show the full name of a file in all FileTypePanels. 
	 * This method just takes the current file names in the panel.
	 */
	public final void showFullFileNames() {
		for(FileTypePanel ftp : ftpList) {
			ftp.showFullFileNames();
		}
	}

	/**
	 * Updates all FileTypePanels, but not the
	 * invalidPanel.
	 * Should be called whenever a checkbox was added.  
	 */
    public static void updateFileView(){
		for(FileTypePanel ftp : ftpList) {
			ftp.updateFileView();
		}
		if (fph != null) {
			fph.invalidate();
			fph.validate();
		} else {
			if (PeaProperties.isCommandLineMode() == false) {
				TestLog.ve(FilePanelHolder.class, "Missing instance", 5);
			}
		}
		//JREProperties.getMainWindow().pack();
    }
    
    /**
     * Update the window. 
     * Updates all FileTypePanels, enables/disables 
     * all checkboxes, revalidates the invalidPanel. 
     * This should be called when a component was added to the window.
     */
    public static final void updateWindow() {
    	
    	//TestLog.v(FilePanelHolder.class, "UPDATE");
    	if (ftpList.isEmpty()) {
    		return;
    	}

		JPanel invalidCheckPanel = InvalidPanel.getInvalidCheckPanel();
		if (showInvalidPanel == false){
    		if (invalidCheckPanel.getComponentCount() > 0){
    			showInvalidPanel = true;
    			fph.add(invalidPanel);
    			invalidPanel.invalidate();
    			invalidPanel.validate();
    		}
    	} else {
			invalidCheckPanel.invalidate();
			invalidCheckPanel.validate();
    	} 
		fph.invalidate();
		fph.validate();
		Window frame = (Window) SwingUtilities.getRoot(fph);
		if (frame != null) { // in initialization null
			frame.invalidate();
			frame.validate();
		}
		// enable all check boxes of selected panel
		// set FileTypePanel and salt
		int selectedIndex = getSelectedIndexFromView();
		if (selectedIndex < 0) {
			selectedIndex = 0;			
			if (ftpList.get(selectedIndex) == null) {
				TestLog.ve(FilePanelHolder.class, "Invalid index of FileTypePanel: " + selectedIndex, 5);
					//TestLog.e(FilePanelHolder.class, "Can't add file: " + file.getAbsolutePath());
			} else {
				buttonList.get(0).setSelected(true);
			}
		} else {
			//buttonList.get(0).setSelected(true);
		}

		for (int i = 0; i < ftpList.size(); i++) {
			if (i != selectedIndex) {
				ftpList.get(i).disableCheckBoxes();
			} else {
				ftpList.get(i).enableCheckBoxes();
				currentIndex = selectedIndex;
			}
		}
		if (ftpList.size() == 1) { // only one scheme present
			// set radio button and info button invisible
			if (ftpBasePanel != null && ftpBasePanel.getComponent(0) instanceof JPanel) {
				JPanel p = (JPanel) ftpBasePanel.getComponent(0);
				if (p.getComponent(0) instanceof JPanel) {
					JPanel buttonPanel = (JPanel) p.getComponent(0);
					buttonPanel.setVisible(false);
					invisibleButtonPanel = buttonPanel;
				}
			}
		} else if (ftpList.size() > 1) {
			if (invisibleButtonPanel != null) {
				invisibleButtonPanel.setVisible(true);
			}
		}		

		for(FileTypePanel ftp : ftpList) {
			ftp.updateWindow();
		}
		JREProperties.setTypePanel(ftpList.get(selectedIndex));
		if (modelList.get(selectedIndex).getSalt() != null) {
			KeyDerivation.setSalt(modelList.get(selectedIndex).getSalt());
		} else {
			TestLog.e(FilePanelHolder.class, "Missing salt from model");
			TestLog.ve(FilePanelHolder.class, "Missing salt from model", 5);
		}
		if (modelList.get(selectedIndex).getSchemeMarker() != null) {
			PeaProperties.setAlgoParamString(modelList.get(selectedIndex).getSchemeMarker());
		} else {
			TestLog.e(FilePanelHolder.class, "Missing scheme from model");
			TestLog.ve(FilePanelHolder.class, "Missing scheme from model",  5);
		}
    }

    /**
     * Get the number of all files of all models
     * 
     * @return	number of all files of all models
     */
    public final static int getFileNumber() {
    	int number = 0;
		for(int i = 0; i < modelList.size(); i++) {
			number += modelList.get(i).getFileNumber();
		}
		return number;
    }
    
    /**
     * Get all file names from all FileTypePanels
     * as an ArrayList
     * 
     * @return	all files from all FileTypePanels
     */
    public final ArrayList<String[]> getAllFileNames() {
    	ArrayList<String[]> list = new ArrayList<String[]>();
		for(int i = 0; i < ftpList.size(); i++) {
			String[] names = ftpList.get(i).getAllFileNames();
			list.add(names);
		}
		return list;
    }
    
    public final ArrayList<String[]> getUnselectedValidFileNames() {
    	ArrayList<String[]> list = new ArrayList<String[]>();
		for(int i = 0; i < ftpList.size(); i++) {
			String[] names = ftpList.get(i).getUnselectedValidFileNames();
			list.add(names);
		}
		return list;
    }

    /**
     * Checks if the invalidPanel is shown
     * (if there are invalid files)
     * 
     * @return	true if the invalidPanel is shown
     * 			(there are invalid files)
     */
    protected static boolean isShowInvalidPanel() {
    	return showInvalidPanel;
    }
    
    /**
     * Set the invalidPanel if not already set
     * and revalidate the window
     */
    protected static void setInvalidPanel() {
    	
		JPanel invalidCheckPanel = InvalidPanel.getInvalidCheckPanel();
		//int invalidFileNumber = invalidCheckPanel.getComponentCount();
		if (showInvalidPanel == false){			
			updateWindow();
    	} else {
    		if (invalidCheckPanel != null) {
			invalidCheckPanel.invalidate();
			invalidCheckPanel.validate();
    		} else {
    			if (PeaProperties.isCommandLineMode() == false) {
    				TestLog.ve(FilePanelHolder.class, "Missing instance of invalidCheckPanel", 5);
    			}
    		}
    	} 
    	showInvalidPanel = true;
    }

    /**
     * Get all FileTypePanel as list
     * 
     * @return	all shown FileTypePanels
     */
    public static ArrayList<FileTypePanel> getFileTypePanelList() {
    	return ftpList;
    }
}
