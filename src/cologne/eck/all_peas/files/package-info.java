/**
 * Everything that has to do with distributing, checking and showing 
 * the files in the password dialog (at start).
 */
/**
 * @author axl
 *
 */
package cologne.eck.all_peas.files;