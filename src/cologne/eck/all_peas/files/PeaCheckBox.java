package cologne.eck.all_peas.files;

/*
 * Peafactory - Production of Password Encryption Archives
 * Copyright (C) 2020  Axel von dem Bruch
 * 
 * This library is free software; you can redistribute it and/or modify it 
 * under the terms of the GNU General Public License as published 
 * by the Free Software Foundation; either version 2 of the License, 
 * or (at your option) any later version.
 * This library is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
 * or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU General Public License for more details.
 * See:  http://www.gnu.org/licenses/gpl-2.0.html
 * You should have received a copy of the GNU General Public License 
 * along with this library.
 */

import java.awt.Color;
import java.awt.Container;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.JCheckBox;
import javax.swing.KeyStroke;
import javax.swing.SwingUtilities;

import cologne.eck.all_peas.cloud.CloudPropertyHandler;
import cologne.eck.all_peas.control.PathFileManager;
import cologne.eck.all_peas.data.PeaProperties;
import cologne.eck.all_peas.gui.PeaDialog;
import cologne.eck.all_peas.gui.PeaLockFrame;
import cologne.eck.all_peas.vm_specific.JREProperties;
import cologne.eck.tools.TestLog;


@SuppressWarnings("serial")
class PeaCheckBox extends JCheckBox implements MouseListener,ItemListener {
	
	private static final Color ROLLOVER_COLOR = new Color(235, 250, 245);
	private static final Color SELECTED_COLOR = new Color(250, 250, 250);
	private static final Color DESELECTED_COLOR = new Color(235, 235, 235);

	/**
	 * Checkbox for a valid file, not a directory
	 */
	public PeaCheckBox() {
		this.setOpaque(true);
		this.setBackground(Color.WHITE);
		this.setRolloverEnabled(true);
		this.addMouseListener(this);
		this.addItemListener(this);
		this.getActionMap().put("removeCheckBox", removeCheckBox);
		this.getInputMap(JCheckBox.WHEN_FOCUSED).put(KeyStroke.getKeyStroke("DELETE"), 
				"removeCheckBox");
		this.getInputMap(JCheckBox.WHEN_FOCUSED).put(KeyStroke.getKeyStroke("BACK_SPACE"), 
				"removeCheckBox");
		this.getAccessibleContext().
	    setAccessibleDescription(
	    	    "Clicking this component selects or deselects the file.");
	}

	@Override
	public void mouseClicked(MouseEvent mc) {
		if (SwingUtilities.isLeftMouseButton(mc) ) {
			JCheckBox check = (JCheckBox)mc.getComponent();
			if (check.isSelected() == true) {
				this.setBackground(SELECTED_COLOR);
			} else {
				this.setBackground(DESELECTED_COLOR);
			}
		} else if (SwingUtilities.isRightMouseButton(mc)){
			if (mc.getSource() instanceof JCheckBox) {
				removeAction((JCheckBox)mc.getSource());
			}
		}
	}
	
	private void removeAction(JCheckBox check) {
		int delete = PeaDialog.showQuestion(check, PeaProperties.getVmBridge().translate("remove_file"), null, 0);
		Container parent = null;
		if (delete == 0) {
			parent = this.getParent();
			if (parent != null) {
				// remove from path file or from properties
				if (parent instanceof CloudFilePanel) {
					CloudFilePanel cfp = (CloudFilePanel) parent;
					String saltAsHex = cfp.getCloudFileModel().getSalt();
					String algosAsHex = cfp.getCloudFileModel().getAlgoParams();
					// providerName + ": " + fileName
					String[] providerAndFile = check.getText().split(": ");
					if (providerAndFile == null || providerAndFile.length != 2) {
						TestLog.ve(PeaCheckBox.class, "Invalid text on cloud check box: " + check.getText(), 5);
					}
					CloudPropertyHandler.removeFileOfProvider(providerAndFile[0], providerAndFile[1], 
							saltAsHex, algosAsHex, null);
					//							cfp.getCloudControl().removeInitialFileOfProvider(providerName, fileName)
				} else {
					//TestLog.o(PeaCheckBox.class, PeaProperties.getFileType() + " " + JREProperties.getMainWindow().getClass().getSimpleName());
					//TestLog.o(PeaCheckBox.class, PeaProperties.getFileType().equals("file") + "  "  + (JREProperties.getMainWindow() instanceof PeaLockFrame));
					if (parent.getParent() instanceof FileTypePanel
							|| parent.getParent().getParent().getParent() instanceof FileTypePanel) { // File Lock PEA decrypted 
						
						if (PeaProperties.getFileType().equals("file") 
								&& (JREProperties.getMainWindow() instanceof PeaLockFrame)) {
							// this is an plain text file that will remain unencrypted
							int stillDelete = PeaDialog.showQuestion(check, PeaProperties.getVmBridge().translate("leave_files_unencrypted"), 
									PeaProperties.getVmBridge().translate("warning"), 0);
							if (stillDelete != 0) {
								TestLog.v(PeaCheckBox.class, "Remove file from list cancelled...");
								return;
							}							
						}
						//this.setSelected(false);
						if (this.isSelected()) {
							// update number and size, deselect parent directory if necessary:
							this.doClick();
						}						
						TestLog.v(PeaCheckBox.class, "Remove " + check.getName());
						PathFileManager.removeFileName(check.getName());
					}
				}
				parent.remove(this);
				parent.revalidate();
				parent.repaint();				
			}
		} else {
			return;
		}
	}
	
	@Override
	public void mouseEntered(MouseEvent arg0) {
		this.setBackground(ROLLOVER_COLOR);
		this.setFont(new Font(Font.SANS_SERIF, Font.BOLD, PeaProperties.getFontSize()));
	}
	
	@Override
	public void mouseExited(MouseEvent me) {

		this.setFont(new Font(Font.SANS_SERIF, Font.PLAIN, PeaProperties.getFontSize()));

		if (((JCheckBox)me.getComponent()).isSelected() == true) {
			this.setBackground(SELECTED_COLOR);
		} else {
			this.setBackground(DESELECTED_COLOR);
		}		
	}
	
	@Override
	public void mousePressed(MouseEvent arg0) {}
	@Override
	public void mouseReleased(MouseEvent arg0) {}


	@Override
	public void itemStateChanged(ItemEvent e) {
		 if (e.getStateChange() == ItemEvent.SELECTED) {
			 this.setBackground(SELECTED_COLOR);
		 } else if (e.getStateChange() == ItemEvent.DESELECTED) {
			 this.setBackground(DESELECTED_COLOR);
		 }
	}
	
	private Action removeCheckBox = new AbstractAction() {
		@Override
		public void actionPerformed(ActionEvent ae) {
			JCheckBox check = null;
			if (ae.getSource() instanceof JCheckBox) {
				check = (JCheckBox) ae.getSource();
			} else {
				return;
			}
			removeAction(check);
		}
	};
}
