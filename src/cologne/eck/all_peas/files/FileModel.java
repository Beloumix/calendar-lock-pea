package cologne.eck.all_peas.files;


import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.TreeMap;

import cologne.eck.all_peas.control.PeaControl;
import cologne.eck.all_peas.data.Attachments;
import cologne.eck.all_peas.data.PeaProperties;
import cologne.eck.tools.FileTools;
import cologne.eck.tools.TestLog;
import cologne.eck.tools.UnexpectedValueException;

/*
 * PeaFactory - Production of Password Encryption Archives
 * Copyright (C) 2015  Axel von dem Bruch
 * 
 * This library is free software; you can redistribute it and/or modify it 
 * under the terms of the GNU General Public License as published 
 * by the Free Software Foundation; either version 2 of the License, 
 * or (at your option) any later version.
 * This library is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
 * or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU General Public License for more details.
 * See:  http://www.gnu.org/licenses/gpl-2.0.html
 * You should have received a copy of the GNU General Public License 
 * along with this library.
 */

/*
 * This class contains methods for file, that are specific
 * for all PEAs of the PeaFactory 
 */
/**
 * 
 * @author Axel von dem Bruch
 *
 */

/*
 * NOTES:
 * TreeMap: 
 * log(n) time cost for containsKey, get, put and remove
 * put overwrites the existing value, 
 * so containsKey must be called before, if this behavior is unsuitable. 
 * 
 * get key/value by index (this is expensive):
 * String key = map.keySet().toArray()[***INDEX***];
 * String value = map.get(key);
 * to get only the value:
 * map.values().toArray()[***INDEX***]; 
 * or iterate over the map
 */

public class FileModel {
	
	// TODO ? store file size along with annotation for each file in map
	// -> performance improvement, when (de)selecting file check boxes: no file operation required
	// BUT: When file size is changed at runtime (in plain mode), values are incorrect...
	// -> only for encrypted mode
	
	/**
	 * TreeMap of File and String: 
	 * the key is the original file, 
	 * the value is the annotations for the file. 
	 * Annotations may contain 
	 * invalid markers, 
	 * short descriptions of the invalidity, 
	 * directory markers, 
	 * space to indicate the file hierarchy, 
	 */
	private TreeMap<File, String> map = new TreeMap<File, String>();	
	
	private byte[] schemeMarker; 
	private byte[] salt;

	private int fileNumber = 0; // number of displayed valid files
	private long allSize = 0; // size of displayed valid files 
	
	// these are helper values to check if number of files and size
	// is greater than limits
	private long newFileNumber = 0;
	private long newFileSize = 0;
		
	private static final String INVALID_MARKER =  ": ~";

	/**
	 * Model of files associated with a scheme and a salt
	 * 
	 * @param encryptedMode	true if files are encrypted
	 * @param _schemeMarker	The scheme associated with this model (algorithms and parameters) 
	 * 						or null for plain files
	 * @param _salt			The salt associated with this model
	 * 						or null for plain files
	 */
	public FileModel(boolean encryptedMode, byte[] _schemeMarker, byte[] _salt) {
		TestLog.v(FileModel.class, "encrypted mode: " + encryptedMode);
		//new UnexpectedValueException("","SALT",new String(_salt)).printDescription();
		if (_schemeMarker == null && encryptedMode == true) {
			new UnexpectedValueException("byte[]","_schemeMarker","is null").printDescription();
			TestLog.ve(FileModel.class, "algorithm is null", 5);
		} else {
			this.schemeMarker = _schemeMarker;
		}
		if (_salt == null && encryptedMode == true) {
			new UnexpectedValueException("byte[]","_salt","is null").printDescription();
			TestLog.ve(FileModel.class, "salt is null", 5);
		} else {
			this.salt = _salt;
		}
	}
	
	/**
	 * Checks a file if it is a valid to encrypt/decrypt or not. 
	 * Checks existence, read and write access, if empty or too large.
	 * The file is not added to the map and the annotation is not set.
	 * 
	 * @param file			the file to check
	 * @param printErrors	true: print error messages (test mode)
	 * 
	 * @return			empty String, if file is valid, the invalid annotation otherwise
	 */
	private final String checkAccess(File file, boolean printErrors) {

  		// check if exists:
		if ( ! file.exists() ) {				
			if (printErrors == true){
				TestLog.e(FileModel.class, "File notfound: " + file.getAbsolutePath());
			}
			return PeaProperties.getVmBridge().translate("can_not_find") + INVALID_MARKER;
		}	
		// check access:
		if ( ! file.canRead() ) {
			if (printErrors == true){
				TestLog.e(FileModel.class, "No read access: " + file.getAbsolutePath());
			}
			return PeaProperties.getVmBridge().translate("can_not_read") + INVALID_MARKER;
		}
		if ( ! file.canWrite() ) {
			if (printErrors == true){
				TestLog.e(FileModel.class, "No write access: " + file.getAbsolutePath());
			}
			return PeaProperties.getVmBridge().translate("can_not_write") + INVALID_MARKER;
		}				
		if ( file.length() == 0 && ! file.isDirectory() ) {
			if (printErrors == true){
				TestLog.e(FileModel.class, "Empty file: " + file.getAbsolutePath());
				TestLog.e(FileModel.class, "Empty file: " + file.getAbsolutePath(),5);
			}
			return PeaProperties.getVmBridge().translate("empty_file") + INVALID_MARKER;
		}		
		if ( file.length() > Long.MAX_VALUE || file.length() < 0) {
			if (printErrors == true){
				TestLog.e(FileModel.class, "Invalid file size: " + file.getAbsolutePath());
			}
			return PeaProperties.getVmBridge().translate("file_too_large") + INVALID_MARKER;
		}
		return "";// no access error occurred
	}
	
	/**
	 * Checks if a file contains the file identifier of the given PEA.
	 * If not, an invalid marker is returned but not set.
	 * Note: The file must not be listed in the map. 
	 * 
	 * @param file			the file to check
	 * @param printErrors	print error messages to stderr (true) or not (false)
	 * 
	 * @return	null if the file contains the fileIdentifier or could not test,
 				file identifier error annotation otherwise
	 */
	protected final String checkFileIdentifier (
			File file, boolean printErrors) {//, boolean addFile) {		
		
		RandomAccessFile f;
		String fileName = file.getAbsolutePath();
		
		if (! file.isFile()) {
			if ( printErrors == true) {
				new UnexpectedValueException("file", "File", "is not a file: " + file.getAbsolutePath()).printDescription();
			}
			return PeaProperties.getVmBridge().translate("unknown_file_type") 
					+ INVALID_MARKER;
		}		
		
		try {
			f = new RandomAccessFile(fileName, "r" );
			if(Attachments.checkFileIdentifier(f, false) >= 0) {
				f.close();
				return null;
			} else {
				if (printErrors == true ) {
					TestLog.v(FileModel.class, "File does not contain file identifier: " + fileName);
				}
				f.close();
				return PeaProperties.getVmBridge().translate("not_encrypted_with_this_archive") 
						+ INVALID_MARKER;
			}			

		} catch (FileNotFoundException e) {
			if (printErrors == true) {
				TestLog.e(FileModel.class, "File not found: " + fileName);
			}
			return null;
		} catch (IOException e) {
			if (printErrors == true) {
				TestLog.e(FileModel.class, "No access to file: " + fileName);
			}
			return null;
		}
	}	
	
    
    /**
     * Checks and adds a file and sets possibly an invalid marker as value.
     * 
     * @param file			the file to check
     * @param plainModus	the modus: encrypted file = false
     * 						decrypted files = true
     * @param printErrors	print error messages to stderr (true) or not (false)
     * @param checkAgain	true if already listed files should be checked again
     * 
     * @return		the Annotation for the file
     */
	protected final String checkAndAddFile( File file, 
			boolean plainModus, boolean printErrors, boolean checkAgain) {
		
		if (file == null) {
			TestLog.e(FileModel.class, "Missing file to check...");
			new UnexpectedValueException("file", "File", "is null").printDescription();
			return null;
		}		
		String annotation = "";		
		boolean alreadyInList = map.containsKey(file);
		
		if (alreadyInList == false) {
			// add the file to the map: 
			map.put(file, "");
		} 
		if (alreadyInList == false || checkAgain == true) {

			String accessError = checkAccess(file, printErrors);
			// add file to map
			map.put(file, accessError);
			annotation = accessError;

			if (file.isFile() ) {

				if (plainModus == true) {
					if (PeaControl.getDialog().checkFormat(file, true, true, false) == true) {
						return annotation;
					} else {
						// replace annotation
						map.put(file, PeaProperties.getVmBridge().translate("unsuitable_format") + INVALID_MARKER);
						annotation = PeaProperties.getVmBridge().translate("unsuitable_format") + INVALID_MARKER;
					}
				} else { // plainModus == false									
					if( accessError.equals("") ){
						String identifierError = checkFileIdentifier(file, printErrors);
						//TestLog.o(FileModel.class, "indentifier error: " + identifierError);
						if (identifierError != null) {							
							map.put(file, identifierError);
							annotation = identifierError;
							return annotation;
						} else {
							//return false;
						}
					} else {
						// don't check
					}
				}

			} else if (file.isDirectory()) {      
				String[] fileList = file.list();
				if (fileList == null){
					map.put(file, 
							PeaProperties.getVmBridge().translate("invalid_empty_directory") 
							+ INVALID_MARKER);  	
					annotation = PeaProperties.getVmBridge().translate("invalid_empty_directory") 
							+ INVALID_MARKER;					
				} else  if (fileList.length == 0){
					map.put(file, 
							PeaProperties.getVmBridge().translate("invalid_empty_directory") 
							+ INVALID_MARKER);  	
					annotation = PeaProperties.getVmBridge().translate("invalid_empty_directory") 
							+ INVALID_MARKER;					
				}
			} else {     		
				map.put(file, PeaProperties.getVmBridge().translate("unknown_file_type") 
						+ INVALID_MARKER);
				annotation = PeaProperties.getVmBridge().translate("unknown_file_type") 
						+ INVALID_MARKER;
			}   	
		} 
		return annotation;
	}
	
    /**
     * Checks if the TreeMap contains at least 
     * one valid file to encrypt/decrypt
     * 
     * @return	true if at least one valid file was found
     */
	public boolean checkForValidFile(){
		
		boolean validFileFound = false;
		int size = map.size();
		if (size == 0) {
			return false;
		}
		File[] files = map.keySet().toArray(new File[map.size()]);
		for (int i = 0; i < size; i++) {
			if (files[i].isDirectory()){
				continue;
			}
			if (map.get(files[i]).contains(INVALID_MARKER)){
				continue;
			} else {
				validFileFound = true;
				break;
			}
		}
		return validFileFound;
	}

	/**
	 * Checks if a directory contains at least 
	 * one valid file to encrypt/decrypt. This function does
	 * not set annotation or add files to the map
	 * 
	 * @param directory 	the directory to check
     * @param printErrors	print error messages to stderr (true) or not (false)	 
     * @param plainModus	true: plain text files,false: encrypted files
     * @param window		the component for dialogs
	 * 
	 * @return				0 if one valid file was found, 1 no valid file found, 
	 * 						-1 if directory should not be added (execution time break)
	 */
	protected final int checkIncludedChildren(File directory, 
			boolean printErrors, boolean plainModus, Object window) {
		
		if (directory == null) {
			if (printErrors == true) {
				new UnexpectedValueException("directory", "File", "is null").printDescription();
			}
			return 1;
		}
		if (directory.isDirectory() == false) {
			if (printErrors == true) {
				new UnexpectedValueException("directory", "File", "is not directory").printDescription();
			}
			return 1;
		}
		// update helper values
		newFileNumber = 0;
		newFileSize = 0;
		if (checkNumberAndSizeOfFolder(directory, window, plainModus, printErrors) == false){
			return -1;
		}
		File[] files = FileTools.getAllFilesOfFolder(directory, false);// list also directories
	
		if (files == null || files.length == 0){		
			return 1;
		}
		int len = files.length;
		for (int i = 0; i < len; i++) {
			File file = files[i];
			if (file.isDirectory()){
				// no need to check
			} else if (file.isFile()){
				
				if (checkAccess(file, printErrors).equals("")) { // no access error
					if (plainModus == false) {
						if (checkFileIdentifier(file, printErrors) == null) {// file identifier found
							return 0;
						}
					} else {
						// format check for PEA: only simple check
		    			if (PeaControl.getDialog().checkFormat(file, true, false, false) == true) {
		    				return 0;
		    			} 
					}
				}
			} 		
		}
		return 1;// no valid file found
	}
	
	/**
	 * Check number and size before checking and
	 * adding the files
	 * 
	 * @param files		the files to check
	 * @param window	the component for warning dialog
	 * @param plainModus	true: plain text, false: encrypted
	 * @param printErrors  print error messages in test mode
	 * 
	 * @return		true: files should be added
	 * 				false: do not add files (process canceled or closed)
	 */
	protected boolean checkNumberAndSize(File[] files, Object window, 
			boolean plainModus, boolean printErrors) {		
		
		// reset helper values:
		newFileNumber = 0;
		newFileSize = 0;
		if (files == null) {
			new UnexpectedValueException("files", "File[]", "is null").printDescription();
			return false;
		}
		int len = files.length;
		for (int i= 0; i < len; i++) {
			File file = files[i];
			if (file.isFile()) {
            	// check size limits:
				newFileSize = FileTools.getFileSize(files[i].getAbsolutePath());
				newFileNumber ++;
                if (ExecutionTimeObserver.warnExecutionTime(
                		newFileNumber, newFileSize, this, window) == false){
                	// remove if in map
                	map.remove(file);
                	return false;
                }
			} else if (file.isDirectory()){
				if (checkNumberAndSizeOfFolder(file, window, plainModus, printErrors) == false){
					return false;
				}
			} else {
				// nothing to do: invalid file
			}
		}
		return true; // no warning or warnings ignored		
	}

	/**
	 * Check if the number of files and overall size of a folder
	 * is greater than limits. This function is rather fast 
	 * than accurate and checks only to give a warning message
	 * for long file number or large size (no format check.  
	 * 
	 * @param directory			the directory
	 * @param window			the parent frame or component for the
	 * 							warning dialog
	 * @param plainModus		true: plain text, false: encrypted
	 * @param printErrors	print error messages in test mode
	 * 
	 * @return	true: files should be added, false: do not add files
	 */
	public boolean checkNumberAndSizeOfFolder(File directory, Object window, 
			boolean plainModus, boolean printErrors) {
		
		if (directory == null) {
			new UnexpectedValueException("directory", "File", "is null").printDescription();
			return false;
		}
		if (! directory.isDirectory()) {
			new UnexpectedValueException("directory", "File", "is not directory: " + directory.getAbsolutePath()).printDescription();
			//return false;		
		}
		// get direct children: files and directories
        File[] fileList = directory.listFiles();        
        if (fileList == null) {
        	if (printErrors == true){
        		new UnexpectedValueException("fileList", "File[]", "is null or empty: " + directory.getAbsolutePath()).printDescription();
        	}
			return false;		
        }
        int len = fileList.length;
        newFileNumber += len;
        // check number limits:
        if (ExecutionTimeObserver.warnExecutionTime(newFileNumber, newFileSize, this, window) == false){
        	// remove from map if it was added 
        	map.remove(directory);
        	return false;
        }

        for (int i = 0; i < len; i++) {
        	File file = fileList[i];
            if (file.isFile()) {
            	
            	if (plainModus == false) {
            		if (checkFileIdentifier(file, printErrors) == null) {// file identifier found

            			newFileSize += FileTools.getFileSize(file.getAbsolutePath());
            			// check size limits:
            			if (ExecutionTimeObserver.warnExecutionTime(
            					newFileNumber, newFileSize, this, window) == false){
            				// remove from map if it was added 
            				map.remove(file);
            				return false;
            			}
            		}
            	}
            } else if (file.isDirectory()) {
            	if (checkNumberAndSizeOfFolder(file, window, plainModus, printErrors) == false){
            		return false;
            	}            		
            }
        }
        return true;
	}
	
	/**
	 * Remove all invalid and deselected files from map
	 */
	public void cleanupMap(){
		File[] files = map.keySet().toArray(new File[map.size()]);
		int len = files.length;
		if (len == 0){
			return;
		}
		for (int i = 0; i < len; i++) {
			String annotation = map.get(files[i]);
			if (annotation.contains(INVALID_MARKER)	// invalid
					|| annotation.startsWith("-")){	// deselected
				map.remove(files[i]);
			}
		}
	}
	
	/**
	 * Calculate and set the file number and overall size
	 * of the files of the current map. 
	 */
	public void calculateNumberAndSize(String[] selectedValidFileNames){
		//reset:
		allSize = 0;
		fileNumber = 0;
		// get all files:
	//	File[] files = map.keySet().toArray(new File[map.size()]);
	//	int len = files.length;
		if (selectedValidFileNames == null) {
			return;
		}
		int len = selectedValidFileNames.length;
		// set values:
		for (int i = 0; i < len; i++) {
			File file = new File (selectedValidFileNames[i]);
			if (file.isFile()){ // only valid files
				if (map.get(file) != null){
					if (! (map.get(file).contains(INVALID_MARKER) // invalid
							|| map.get(file).startsWith("-"))){ // deselected
						try {
							allSize += FileTools.getFileSize(file);
							fileNumber++;
						} catch (Exception e) {
							map.put(file, 
									PeaProperties.getVmBridge().translate("unexpected_error")
									+ " -size- "
									+ INVALID_MARKER);
						}
					}
				} else {
					TestLog.ve(getClass(), "File " + file.getAbsolutePath() + " has no annotation", 5);
				}
			}
		}
	}
	/**
	 * Get valid file names: only files, not directories,
	 * not invalid or not selected files
	 * 
	 * @return	the valid file names
	 */
/*	public String[] getValidFileNames() {
		
		File[] files = map.keySet().toArray(new File[map.size()]);
		int len = files.length;
		int index = 0;
		String[] validFileNames = new String[len];
		// set values:
		for (int i = 0; i < len; i++) {
			File file = files[i];
			if (file.isFile()){ // only valid files
				if (! (map.get(file).contains(INVALID_MARKER) // invalid
						|| map.get(file).startsWith("-"))){ // deselected
					validFileNames[index] = file.getAbsolutePath();
					index++;
				}
			}
		}
		String[] result = new String[index];
		System.arraycopy(validFileNames,  0,  result,  0,  index);
		return result;
	} 
*/
	/**
	 * Checks if a file is already listed in the map
	 * 
	 * @param file	the file to check
	 * @return		true if the file is already listed
	 * 				false otherwise
	 */
	public boolean containsFile(File file) {
		if (map.containsKey(file)){
			return true;
		} else {
			return false;
		}
	}
	//TODO: performance
	/**
	 * Checks if a directory is has a child
	 * in the current map
	 * 
	 * @param file	the directory to check
	 * @return		true if there is a child in the map
	 */
/*	public boolean hasValidChild(File file) {
		File[] files = map.keySet().toArray(new File[map.size()]);
		int len = files.length;
		for (int i = 0; i < len; i++) {
			if (files[i].getParentFile().equals(file)) {
				return true;
			}
		}
		return false;
	}*/

	/**
	 * Get the annotation of the given file
	 * 
	 * @param file	the file
	 * @return		the annotation for this file
	 */
	protected String getAnnotation(File file) {
		return map.get(file);
	}
	/**
	 * The the annotation for a file
	 * 
	 * @param file			the file
	 * @param annotation	the annotation for the file
	 */
	public void setAnnotation(File file, String annotation) {
		map.put(file, annotation);
	}

	/**
	 * Removes all of the mappings from this map
	 * and sets file number and size to zero
	 */
	public void resetTreeMap(){
		map.clear();
		map = new TreeMap<File,String>();
		fileNumber = 0;
		allSize = 0;
	}
	
	public File[] getAllFiles(){
		return map.keySet().toArray(new File[map.size()]);
	}
	
	/**
	 * Get the TreeMap containing the files
	 * and annotations
	 * 
	 * @return	the current TreeMap
	 */
	public TreeMap<File, String> getTreeMap() {
		return map;
	} 	
	
	/**
	 * Get the number of all selected files
	 * 
	 * @return	the fileNumber
	 */
	public int getFileNumber() {
		return fileNumber;
	}
	
	/**
	 * Set the number of all selected files to 0
	 */
	protected void resetFileNumber() {
		this.fileNumber = 0;
	}
	
	/**
	 * Increase or decrease number of files
	 * 
	 * @param numberToAdd
	 */
	protected final void increaseFileNumber(int numberToAdd) {
		this.fileNumber = this.fileNumber + numberToAdd;
	}
	
	/**
	 * Increase or decrease all size value
	 * 
	 * @param sizeToAdd
	 */
	final void increaseAllSize(long sizeToAdd) {
		this.allSize = this.allSize + sizeToAdd;
	}
	
	/**
	 * Get the size of all selected files
	 * 
	 * @return	the allSize
	 */
	public long getAllSize() {
		return allSize;
	}
	
	/**
	 * Set the size of all selected files to 0
	 */
	protected void resetAllSize() {
		this.allSize = 0;
	}
	
	/**
	 * Get the String that indicates that a file is invalid. 
	 * This String indicates also the end of the annotation and
	 * the beginning of the file name. 
	 * 
	 * @return	the INVALID_MARKER
	 */
	public static String getInvalidMarker() {
		return INVALID_MARKER;
	}

	/**
	 * Removes a key-value-pair from the map
	 * without calculating number and size
	 * 
	 * @param file	the key to remove
	 */
	protected final void removeFileFromList(File file) {
		map.remove(file);
	}
	
	/**
	 * Removes a key-value-pair from the map, 
	 * calculates number and size
	 * 
	 * @param file	the key to remove
	 * @param calculateNumderAndSize true: subtract size and number
	 * @param size	0 or the size of the file if it does no longer exist
	 */
	final void removeFile(File file, boolean calculateNumderAndSize, long size) {
		String annotation = map.get(file);
		if (annotation != null) {
			if ( ! annotation.contains(INVALID_MARKER)) { // valid file
				if (calculateNumderAndSize == true) {
					fileNumber = fileNumber -1;
					if (size >= 0) {
						allSize = allSize - size;
					} else if (file.exists()) {
						allSize = allSize - FileTools.getFileSize(file);
					} else {
						TestLog.ve(FileModel.class, "File to remove does not exist, can't decrease all size...", 5);
					}
				}
			}
		}		
		map.remove(file);
	}

	/**
	 * Get the marker for algorithms and parameters as an array of bytes
	 * 
	 * @return the schemeMarker
	 */
	public byte[] getSchemeMarker() {
		return schemeMarker;
	}
	
	/**
	 * @return the salt
	 */
	public byte[] getSalt() {
		return salt;
	}
}
