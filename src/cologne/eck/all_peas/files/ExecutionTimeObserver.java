package cologne.eck.all_peas.files;

/**
 * Based upon the number and the overall size of a set of
 * selected files, this observer checks whether the amount of files
 * will result in an unexpected long execution time. In this case 
 * a warning message appears in two levels of urgency and the user
 * is asked to cancel the process.
 * The limits depend on the type of the PEA: For Image Lock PEA and 
 * Notebook PEA, only one file of the selected files is decrypted, the
 * other files are only checked for the file identifier, but this file
 * must be processed as text or image, 
 * for File Lock PEA all selected files are decrypted. 
 * Each PEA should set its own limits. 
 */

/*
 * PeaFactory - Production of Password Encryption Archives
 * Copyright (C) 2015  Axel von dem Bruch
 * 
 * This library is free software; you can redistribute it and/or modify it 
 * under the terms of the GNU General Public License as published 
 * by the Free Software Foundation; either version 2 of the License, 
 * or (at your option) any later version.
 * This library is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
 * or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU General Public License for more details.
 * See:  http://www.gnu.org/licenses/gpl-2.0.html
 * You should have received a copy of the GNU General Public License 
 * along with this library.
 */

import cologne.eck.all_peas.data.PeaProperties;
import cologne.eck.all_peas.gui.PeaDialog;


public class ExecutionTimeObserver {
	
	/**
	 * These variables can be set by a PEA to PEA-specific values
	 */
	private static int numberLimit = 256; // Warning if selected file number > numberLimit
	private static int extremeNumberLimit = 512; 
	private static long sizeLimit = 		1024 * 1024 * 128; // 128 MiB	
	private static long extremeSizeLimit = 	1024 * 1024 * 1024; // 1 GiB	
	
	/*
	 * true if a warning (long execution time because of size) was already shown
	 * (this warning should appear at most once for each session)
	 */
	private static boolean sizeWarning = false;
	/*
	 * true if a warning (extreme long execution time) was already shown
	 * (this warning should appear at most once for each session)
	 */
	private static boolean extremeSizeWarning = false;
	/*
	 * true if a warning (long execution time because of number of files) was already shown
	 * (this warning should appear at most once for each session)
	 */
	private static boolean numberWarning = false;
	/*
	 * true if a warning (extreme long execution time because of number of files) was already shown
	 * (this warning should appear at most once for each session)
	 */
	private static boolean extremeNumberWarning = false;
	
	
    /**
     * Display a warning (long execution time) and an option to break 
     * if a large number or large overall size of files was selected
     *    
     * @param newNumber		the number of files to check
     * @param newSize		the overall size of the files to check
     * @param fileModel		the FileModel that hold fileNumber and allSize
     * @param window		the component or window for the warning dialog
     * 
     * @return				true if the process should continue ( no warning
     * 						or warning accepted), false if the files should not
     * 						be added (user selected the cancel or close option),
     * 						in this case the file must be removed from the map
     * 						manually, because it was added by checking access...
     */
	protected final static boolean warnExecutionTime(long newNumber, long newSize, 
    		FileModel fileModel, Object window) {

    	// extreme high size of files:
		if ((fileModel.getAllSize() + newSize) > extremeSizeLimit 
				&& extremeSizeWarning == false) {
			
			String warningMessage = PeaProperties.getVmBridge().translate("extreme_size_warning");

			boolean continueOption =  showWarningAndBreakOption(window, 
					warningMessage, 
					true); // default is to break the process
			if (continueOption == true){
				extremeSizeWarning = true;
				sizeWarning = true;
				return true;
			} else {
				return false;
			}
		}			
		// high size of files:
		if (fileModel.getAllSize() + newSize > sizeLimit 
				&& sizeWarning == false) {
			
			String warningMessage = PeaProperties.getVmBridge().translate("size_warning");

			boolean continueOption =  showWarningAndBreakOption(window, 
					warningMessage, 
					false); // default is to continue
			if (continueOption == true){
				sizeWarning = true;
				return true;
			} else {
				return false;
			}
		}			
		// extreme high number of files:
		if (fileModel.getFileNumber() + newNumber > extremeNumberLimit 
				&& extremeNumberWarning == false) {
			
			String warningMessage = PeaProperties.getVmBridge().translate("extrem_number_warning");

			boolean continueOption =  showWarningAndBreakOption(window, 
					warningMessage, 
					true); // default is to break the process
			if (continueOption == true){
				extremeNumberWarning = true;
				numberWarning = true;
				return true;
			} else {
				return false;
			}
		}	
		// high number:
		if (fileModel.getFileNumber() + newNumber > numberLimit 
				&& numberWarning == false) {
			
			String warningMessage = PeaProperties.getVmBridge().translate("number_warning");
			boolean continueOption =  showWarningAndBreakOption(window, 
					warningMessage, 
					false); // default is to continue
			if (continueOption == true){
				numberWarning = true;
				return true;
			} else {
				return false;
			}        }		
		return true;
    }

    /**
     * Show a warning and option to break or to go on. 
     * 
     * @param window			the window owner
     * @param warningMessage	the warning message
     * @param extremeWarning	true for extreme size/number warning:
     * 							the default option is to break, other option
     *  
     * @return					true if the process goes on, 
     * 							false for breaking the process
     */
    public static boolean showWarningAndBreakOption(
    		Object window, 
    		String warningMessage, 
    		boolean extremeWarning) {
    	
		String goOnOption = ""; // String for ok option
		int initialValue = 0;	// default: break (1) or continue (0)
		if (extremeWarning == true) {
			goOnOption = PeaProperties.getVmBridge().translate("ignore_and_go_on");
			initialValue = 1;
		} else {
			goOnOption = PeaProperties.getVmBridge().translate("go_on");
			initialValue = 0;
		}
    	Object[] options = {goOnOption,
    			PeaProperties.getVmBridge().translate("cancel")};
		
		int n = PeaDialog.showOptions(window, 					// window owner
				warningMessage, 								// the message, specifying the warning
				PeaProperties.getVmBridge().translate("warning"), // title of the dialog window
				0, 												// JOptionPane.YES_NO_OPTION
				2, 												// JOptionPane.WARNING_MESSAGE
				options, 										// options  as Strings
				options[initialValue]);							// default option
			
		if (n == 0) {
			return true; 			
		} else { // cancel or close
			return false;
		}
	}
    
    //================ Getter & Setter ==================================

	/**
	 * Set, if warning for file size should be displayed
	 * 
	 * @param warn	true: display size warning
	 */
	public static void setSizeWarning(boolean warn) {
		sizeWarning = warn;
	}

	/**
	 * Set, if warning for file number should be displayed
	 * 
	 * @param warn	true: display number warning
	 */
	public static void setNumberWarning(boolean warn) {
		numberWarning = warn;
	}
}
