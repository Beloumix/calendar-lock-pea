package cologne.eck.all_peas.files;

/*
 * Peafactory - Production of Password Encryption Archives
 * Copyright (C) 2020  Axel von dem Bruch
 * 
 * This library is free software; you can redistribute it and/or modify it 
 * under the terms of the GNU General Public License as published 
 * by the Free Software Foundation; either version 2 of the License, 
 * or (at your option) any later version.
 * This library is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
 * or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU General Public License for more details.
 * See:  http://www.gnu.org/licenses/gpl-2.0.html
 * You should have received a copy of the GNU General Public License 
 * along with this library.
 */

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.border.LineBorder;

import cologne.eck.all_peas.data.AttachmentHandler;
import cologne.eck.all_peas.data.PeaProperties;
import cologne.eck.all_peas.gui.PswDialogView;
import cologne.eck.all_peas.vm_specific.JREProperties;
import cologne.eck.tools.TestLog;
import cologne.eck.tools.UnexpectedValueException;

@SuppressWarnings("serial")
public class InvalidPanel extends JPanel implements ActionListener{

	private static InvalidPanel invalidPanel;
	
	/**
	 * JPanel containing all check boxes of invalid file names
	 */
	private static JPanel invalidCheckPanel; // contains the JCheckBoxes: one for each file

	/**
	 * Button to close or open invalid panel
	 */
	private static JButton invalidCloseButton;
	


	private static final Color invalidDirectoryColor = new Color(235,210,210); // background color of JCheckBox for directories
	private static final Color invalidColor = new Color(255, 220, 220);
	
	/**
	 * Scroll pane for invalidCheckPanel. This can be added or removed
	 */
	private JScrollPane invalidScroll;
	
	private JLabel invalidLabel;
	
	/**
	 * Panel that contains invalid check boxes and label + button to close.
	 * This is only added if invalid files occur
	 * 
	 * @param window		the parent window of this panel
	 */
	private InvalidPanel(Object window) {
		
		invalidPanel = this;//new InvalidPanel(window);
		invalidPanel.setLayout(new BoxLayout(invalidPanel, BoxLayout.PAGE_AXIS));
		invalidPanel.setBorder(new LineBorder(Color.GRAY, 1));
		
		this.getAccessibleContext().
	    setAccessibleDescription(
	    	    "Contains invalid files that can not be opened with this PEA.");
		int[] colorValues = PeaProperties.getCipherPeaColor();
		Color backgroundColor = new Color(colorValues[0], colorValues[1], colorValues[2]);
		invalidPanel.setBackground(backgroundColor);
		
		JPanel invalidButtonPanel = new JPanel();
		invalidButtonPanel.setLayout(new BoxLayout(invalidButtonPanel, BoxLayout.LINE_AXIS));
		invalidButtonPanel.setMaximumSize(new Dimension((int)PswDialogView.getScreenwidth(), 30));
		invalidButtonPanel.setBackground(backgroundColor);
		invalidLabel = new JLabel(PeaProperties.getVmBridge().translate("invalid") + ": ");
		invalidLabel.setOpaque(true);
		invalidButtonPanel.add(Box.createHorizontalStrut(5));
		invalidButtonPanel.add(invalidLabel);
		invalidButtonPanel.add(Box.createHorizontalGlue());
		// default is closed
		invalidCloseButton = new JButton("+");		
		invalidCloseButton.getAccessibleContext().
	    setAccessibleDescription(
	    	    "Clicking this component opens the display of invalid files.");
		invalidCloseButton.setFont(new Font(Font.MONOSPACED , Font.PLAIN, PeaProperties.getFontSize() - 1));

		invalidCloseButton.addActionListener(this);
		invalidCloseButton.setActionCommand("invalidCheckPanel");
		invalidButtonPanel.add(invalidCloseButton);
		invalidButtonPanel.add(Box.createHorizontalStrut(5));
		invalidPanel.add(invalidButtonPanel);

		invalidCheckPanel = new JPanel(); // the panel that holds the check boxes
		invalidCheckPanel.setLayout(new BoxLayout(invalidCheckPanel, BoxLayout.Y_AXIS));	
		invalidCheckPanel.setBackground(backgroundColor);


		invalidScroll = new JScrollPane(invalidCheckPanel);
		invalidScroll.setPreferredSize(new Dimension(200, 130));		
	}
	
	protected static InvalidPanel getInstance(Object window) {
		if (invalidPanel == null) {
			invalidPanel = new InvalidPanel(window);
		}
		return invalidPanel;
	}	
	
    /**
     * Add a check box for an invalid file to the checkPanel. 
     * The background color of the check box indicates invalidity. 
     * 
     * @param fileName			the file name associated with this check box
     * @param annotation		the annotation for this file
     */
    public static final void addInvalidFileCheckBox(
    		String fileName, String annotation) { 
    	JCheckBox newBox = new JCheckBox(annotation + fileName);
    	newBox.setName(fileName);
    	newBox.setOpaque(true);
		newBox.setSelected(false);
		newBox.setEnabled(false);
		newBox.setBackground(invalidColor);
		//showInvalidPanel = true; // new for CalendarLockPEA
		if (invalidCheckPanel != null) { // is null in command line mode
			invalidCheckPanel.add(newBox);
		} else {
			if (PeaProperties.isCommandLineMode() == false) {
				TestLog.ve(InvalidPanel.class, "Missing invalid panel", 5);
			}
		}
		
		FilePanelHolder.setInvalidPanel();
    }    
    
    /**
     * Add a check box for a valid or invalid directory to the checkPanel. 
     * The background color of the check box indicates validity or invalidity.
     * 
     * @param fileName		the file name associated with this check box
     * @param annotation	the annotation for this file
     */
    protected final static void addInvalidDirectoryCheckBox(
    		String fileName, String annotation){
    	
    	PeaDirCheckBox newBox = new PeaDirCheckBox();
    	newBox.getAccessibleContext().
	    setAccessibleDescription(
	    	    "Clicking this component selects or deselects this folder and all children.");
    	newBox.setName(fileName);
		newBox.setOpaque(true);

    	newBox.setSelected(false);
		newBox.setEnabled(false);
		newBox.setBackground(invalidDirectoryColor);
		newBox.setText(annotation + fileName);			   
		if (invalidCheckPanel != null) { // is null in command line mode
			invalidCheckPanel.add(newBox);
		} else {
			if (PeaProperties.isCommandLineMode() == false) {
				TestLog.ve(InvalidPanel.class, "Missing invalid panel", 5);
			}
		}
    }
    
	/**
	 * Set an already shown invalid check box valid
	 * 
	 * @param fileName			the file name stored in the map of the model
	 * @param newAnnotation		the new annotation for the file
	 * 
	 * @return	the previous index of the check box in invalidPanel
	 */
	protected final static int setCheckBoxValid(String fileName, String newAnnotation) {
		// untested
		File file = new File(fileName);

		// get the index of the check box in invalidCheckPanel
		int index = -1;
		if (invalidCheckPanel == null || FilePanelHolder.isShowInvalidPanel() == false) {
			if (PeaProperties.isCommandLineMode() == false) {
				TestLog.e(InvalidPanel.class, "set valid failed: no invalidCheckPanel");
			}
		} else {
			int len = invalidCheckPanel.getComponentCount();
			// iterate the inalidCheckPanel to find the file name
			for (int i = 0; i < len; i++) {
				String checkName = ((JCheckBox) invalidCheckPanel.getComponent(i)).getName();
				if (checkName.equals(fileName)){
					index = i;
				}
			}
			if (index == -1) {
				TestLog.e(InvalidPanel.class, "File name not found in check box: " + fileName);
				if (PeaProperties.isTestMode()) {
					new UnexpectedValueException("index", "int", 
							"is < 0, file name not found in invalidCheckPanel: "
									+ fileName).printDescription();
				}
				return -1;
			} else {
				// remove from invalidCheckPanel
				invalidCheckPanel.remove(index);
			}
		}
		// add to checkPanel:
		boolean added = false;
		if (JREProperties.getMainWindow() instanceof PswDialogView) {  // encrypted mode
			// distribute file:
			File[] files = {file};
			added = AttachmentHandler.distributeFiles(files);
		} else if (PeaProperties.getFileType().equals("file") ) { // plain mode in File Lock PEA
			// do not distribute, just add
			if (JREProperties.getTypePanel() instanceof FileTypePanel) {
				FileTypePanel plainFTP = (FileTypePanel) JREProperties.getTypePanel();				
				if (file.isFile()) {
					plainFTP.addValidFileCheckBox(fileName, newAnnotation);
				} else if (file.isDirectory()) {
					plainFTP.addValidDirectoryCheckBox(fileName, newAnnotation);
				}
				added = true;
			} else {
				TestLog.ve(InvalidPanel.class, "Invalid type panel", 5);
			}
		}
		if (added == true) {
			FilePanelHolder.updateFileView();
			FilePanelHolder.updateWindow();
		}
		if (invalidPanel != null) {
			invalidPanel.repaint();
		} else {
			if (PeaProperties.isCommandLineMode() == false) {
				TestLog.ve(InvalidPanel.class, "Missing instance", 5);
			}
		}
		return index;
	} 

	@Override
	public void actionPerformed(ActionEvent ape) {
		
		String command = ape.getActionCommand();
		if (command.equals("invalidCheckPanel")) {	
			
			JButton button = (JButton) ape.getSource();
			button.getAccessibleContext().
		    setAccessibleDescription(
		    	    "Clicking this component shows or hides the display of invalid files.");
			String text = button.getText();
			
			if (text.equals("+")){
				button.setText("-");
				if ( invalidScroll.getParent() != invalidPanel) {
					invalidPanel.add(invalidScroll);
				} 
			} else if (text.equals("-")){

				invalidPanel.remove(invalidScroll);
				button.setText("+");				
			}
			invalidCheckPanel.invalidate();
			invalidCheckPanel.validate();
			invalidPanel.invalidate();
			invalidPanel.validate();
			FilePanelHolder.updateWindow();
		}
	}
	
	/**
	 * Update all translations with current language bundle
	 */
	protected void resetLanguage() {
		if (invalidLabel != null) {
			invalidLabel.setText(PeaProperties.getVmBridge().translate("invalid") + ": ");
		}
	}
	
	protected static JPanel getInvalidCheckPanel() {
		return invalidCheckPanel;
	}
}
