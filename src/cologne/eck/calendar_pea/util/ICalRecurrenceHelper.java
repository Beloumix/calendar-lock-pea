package cologne.eck.calendar_pea.util;

/*
 * PeaFactory - Production of Password Encryption Archives
 * Copyright (C) 2015  Axel von dem Bruch
 * 
 * This library is free software; you can redistribute it and/or modify it 
 * under the terms of the GNU General Public License as published 
 * by the Free Software Foundation; either version 2 of the License, 
 * or (at your option) any later version.
 * This library is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
 * or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU General Public License for more details.
 * See:  http://www.gnu.org/licenses/gpl-2.0.html
 * You should have received a copy of the GNU General Public License 
 * along with this library.
 */

import java.time.temporal.ChronoUnit;

import cologne.eck.all_peas.data.PeaProperties;
import cologne.eck.tools.TestLog;
import cologne.eck.tools.UnexpectedValueException;

class ICalRecurrenceHelper {
	
	/**
	 * Get the suitable ChronoUnit for a iCalendar 
	 * frequency String (FREQ:...). 
	 * 
	 * @param frequencyString	an iCalendar frequency String 
								(DAILY, WEEKLY, MONTHLY, YEARLY)
	 * 
	 * @return	the ChronoUnit for this String
	 */
	protected static ChronoUnit getChronoUnitFromString(String frequencyString) {
		
		ChronoUnit unit = null;
		if (frequencyString.equals("YEARLY")) {
			unit = ChronoUnit.YEARS;
		} else if (frequencyString.equals("MONTHLY")) {
			unit = ChronoUnit.MONTHS;
		} else if (frequencyString.equals("WEEKLY")) {
			unit = ChronoUnit.WEEKS;
		} else if (frequencyString.equals("DAILY")) {
			unit = ChronoUnit.DAYS;
		} else if (frequencyString.equals("HOURLY")) {
			//unit = ChronoUnit.HOURS;
			unit = null;
			if (PeaProperties.isTestMode()) {
				TestLog.e(ICalRecurrenceHelper.class, "Frequency HOURLY not supported...");
			}
		} else if (frequencyString.equals("MINUTELY")) {
			//unit = ChronoUnit.MINUTES;
			unit = null;
			if (PeaProperties.isTestMode()) {
				TestLog.e(ICalRecurrenceHelper.class, "Frequency MINUTELY not supported...");
			}
		} else if (frequencyString.equals("SECONDLY")) {
			//unit = ChronoUnit.SECONDS;
			unit = null;
			if (PeaProperties.isTestMode()) {
				TestLog.e(ICalRecurrenceHelper.class, "Frequency SECONDLY not supported...");
			}
		} else {
			new UnexpectedValueException("frequencyString", "String", "invalid frequencyString: " + frequencyString).printStackTraceLines();
		}  
		return unit;
	}

	/**
	 * Get the frequency of a RecurringPEvent as an iCalendar
	 * valid String (DAILY, WEEKLY, MONTHLY, YEARLY)
	 * 
	 * @param frequency	the frequency of the event as ChronoUnit
	 * 
	 * @return	the iCalendar String for the frequency or null
	 * 			(DAILY, WEEKLY, MONTHLY, YEARLY)
	 */
	protected static String getFrequencyAsICalString(ChronoUnit frequency) {

		//ChronoUnit frequency = null;
		String frequencyString = null;
		if (frequency.equals(ChronoUnit.DAYS)){
			frequencyString = "DAILY";
		} else if (frequency.equals(ChronoUnit.WEEKS)){
			frequencyString = "WEEKLY";
		} else if (frequency.equals(ChronoUnit.MONTHS)){
			frequencyString = "MONTHLY";
		} else if (frequency.equals(ChronoUnit.YEARS)){
			frequencyString = "YEARLY";
		} else if (frequency.equals(ChronoUnit.SECONDS)){
			if (PeaProperties.isTestMode()) {
				TestLog.e(ICalRecurrenceHelper.class, "Frequency SECONDLY not supported...");
			}
			frequencyString = null;
		} else if (frequency.equals(ChronoUnit.MINUTES)){
			if (PeaProperties.isTestMode()) {
				TestLog.e(ICalRecurrenceHelper.class, "Frequency MINUTELY not supported...");
			}
			frequencyString = null;
		} else if (frequency.equals(ChronoUnit.HOURS)){
			if (PeaProperties.isTestMode()) {
				TestLog.e(ICalRecurrenceHelper.class, "Frequency HOURLY not supported...");
			}
			frequencyString = null;
		} else {
			if (PeaProperties.isTestMode()) {
				new UnexpectedValueException("frequency", "ChronoUnit", "invalid frequency: " + frequency).printStackTraceLines();
			}
		}
		return frequencyString;
	}
}
