package cologne.eck.calendar_pea.util;

/*
 * PeaFactory - Production of Password Encryption Archives
 * Copyright (C) 2015  Axel von dem Bruch
 * 
 * This library is free software; you can redistribute it and/or modify it 
 * under the terms of the GNU General Public License as published 
 * by the Free Software Foundation; either version 2 of the License, 
 * or (at your option) any later version.
 * This library is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
 * or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU General Public License for more details.
 * See:  http://www.gnu.org/licenses/gpl-2.0.html
 * You should have received a copy of the GNU General Public License 
 * along with this library.
 */

import cologne.eck.calendar_pea.control.CalendarConstants;

class ICalStringHelper {

	// String to mark a line > 75 octets, that was split
	private static final String CRLFHTAB = "\r\n\t"; // at least 75 characters 
	private static final int LINE_LEN = 45;
	
	/**
	 * Read a string value such as values of summary or description: 
	 * remove property name
	 * remove line splitting with tab or whitespace, 
	 * remove escaping
	 * 
	 * @param lines		calendar file split in lines
	 * @param index		index of string property
	 * 
	 * @return	the string without property name, line splitters, escaping
	 */
	protected static String readString(String[] lines, int index) {
		
		// TODO language properties
		
		// first line of string value including property name such as DESCRIPTION
		String s = lines[index];		
		
		// remove property name
		s = s.substring(s.indexOf(":") + 1, s.length());
		// add next lines if start with whitespace or tab
		while (lines[index + 1].startsWith(" ")
				|| lines[index + 1].startsWith("\t")) {
			s += lines[index + 1].substring(1);// remove first whitespace or tab
			index++; // check next line
		}		
		// escape some characters
		s = readEscapedString(s);
		
		return s;		
	}
	
	/**
	 * Get the property name, e.g. SUMMARY
	 * 
	 * @param lines	line containing the property
	 * 
	 * @return	the property name or full line, if there
	 * 			is no ":" oder empty String if line is empty or null
	 */
	protected static String getPropertyName(String line) {
		String s = line;
		if (s == null || s.length() == 0) {
			return "";
		}
		String substringMarker = ":";
		int colonIndex = s.indexOf(":"); // -1 if no occurrence
		int semicolonIndex = s.indexOf(";"); 
		if (semicolonIndex > 0 && semicolonIndex < colonIndex) {
			// properties such as VALUE or LANGUAGE
			substringMarker = ";";
		}
		
		if ( ! s.contains(":")) {
			return s;
		}
		// get property name
		s = s.substring(0, s.indexOf(substringMarker) );
		return s;
	}
	
	/**
	 * Prepare a string for writing: 
	 * escape characters, split in lines if too long
	 * 
	 * @param s	the string to prepare
	 * 
	 * @return	prepared string for writing (escaped, split if required)
	 */
	protected static String prepareString(String s) {
		s = escapeString(s);
		s = splitInLines(0, s);
		
		return s;
	}	
	
	private static String readEscapedString(String s) {

		// read as described in RFC 6868
		s = s.replace("^n", "\n");
		s = s.replace("^^", "^");
		s = s.replace("^'", "\'");
		
		// 3.3.11.  Text in RFC 5545
		s = s.replace("\\,", ",");
		s = s.replace("\\;", ";");
		s = s.replace("\\\\", "\\");
		return s;
	}
	
	private static String escapeString(String s) {
		
		// escape as described in RFC 6868
		s = s.replace("^", "^^");// must be first
		s = s.replace("\n", "^n");
		s = s.replace("\'", "^'");
		
		// 3.3.11.  Text in RFC 5545
		s = s.replace("\\", "\\\\");// must be first!!!
		s = s.replace(",", "\\,");
		s = s.replace(";", "\\;");	
		return s;
	}
	
	/**
	 * Split a String > 75 characters in several lines of LINE_LEN characters:
	 * Insert new line + tab to mark several lines
	 * 
	 * @param firstIndex	the index to start the split operation
	 * @param s				the String to be split partially
	 * 
	 * @return		the result String: only lines > 75 octets are split
	 */
	private static String splitInLines(int firstIndex, String s){
		if (s == null) return "";
		//s = escapeString(s);
		int sLen = s.getBytes(CalendarConstants.getCharset()).length;
		int charNumber = s.length();
		if (firstIndex + sLen <= 75){ // 75 octets
			return s;
		} else {	
			// quick & dirty: split in lines of LINE_LEN chars (ignoring 4 byte characters)
			// ... 75 is only a SHOULD recommendation...
			StringBuilder builder = new StringBuilder();
			builder.append(s.substring(0, LINE_LEN - firstIndex) + CRLFHTAB);
			firstIndex = LINE_LEN - firstIndex;
			while (true) {
				if (firstIndex + LINE_LEN > charNumber) {
					builder.append(s.substring(firstIndex, charNumber));
					break;
				}
				else {
					builder.append(s.substring(firstIndex, firstIndex + LINE_LEN));
					firstIndex+= LINE_LEN;
					if (firstIndex == charNumber){
						break;
					} else {
						builder.append(CRLFHTAB);
					}
				}				
			}
			return new String(builder);
		}
	}
}
