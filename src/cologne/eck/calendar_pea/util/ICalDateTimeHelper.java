package cologne.eck.calendar_pea.util;

/*
 * PeaFactory - Production of Password Encryption Archives
 * Copyright (C) 2015  Axel von dem Bruch
 * 
 * This library is free software; you can redistribute it and/or modify it 
 * under the terms of the GNU General Public License as published 
 * by the Free Software Foundation; either version 2 of the License, 
 * or (at your option) any later version.
 * This library is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
 * or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU General Public License for more details.
 * See:  http://www.gnu.org/licenses/gpl-2.0.html
 * You should have received a copy of the GNU General Public License 
 * along with this library.
 */

import java.time.DateTimeException;
import java.time.Duration;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeParseException;
import java.time.temporal.Temporal;
import java.time.zone.ZoneRulesException;

import cologne.eck.all_peas.data.PeaProperties;
import cologne.eck.all_peas.gui.PeaDialog;
import cologne.eck.calendar_pea.control.CalendarSetting;
import cologne.eck.calendar_pea.data.PCalendar;
import cologne.eck.calendar_pea.data.PEvent;
import cologne.eck.calendar_pea.gui.LockFrameCalendar;
import cologne.eck.tools.TestLog;
import cologne.eck.tools.UnexpectedValueException;

public class ICalDateTimeHelper {
	
	// the current default zone ID to use as target for all dateTimes
	// and to use as calendar zone ID for writing
	private static ZoneId defaultZoneId = ZoneId.systemDefault();
	
	
	/*
	 * The zone ID of of calendar that is currently read 
	 * or null (-> use LocalDateTime)
	 */
	private static ZoneId currentCalendarZone = null;

	/**
	 * Get an array of LocalDates from a String containing a list of 
	 * dates or periods. All list elements are converted to LocalDate. 
	 * Can be used for RDATE and EXDATE
	 * 
	 * @param dateTimeListString	the string (full line with EXDATE or RDATE)
	 * 								containing a list of dates or periods
	 * 
	 * @return	an array of LocalDates, generated from the string
	 */
	public static LocalDate[] getDatesFromList(String dateTimeListString) {
		// examples: EXDATE;VALUE=DATE-TIME;TZID=America/New_York:20140905T103000,20140906T103000
		// RDATE:19960402T010000Z,19960403T010000Z,19960404T010000Z
		
		LocalDate[] dates = null; // return value
		
		String zIdPrefix = "";
		if (dateTimeListString.contains("TZID=")) {
			int tzidPos = dateTimeListString.indexOf("TZID=") + 5;
			int tzidLastPos = dateTimeListString.indexOf(";",tzidPos);
			if (tzidLastPos == -1) { // no other parameter follows
				tzidLastPos = dateTimeListString.indexOf(":");
			}
			String zoneIDString = dateTimeListString.substring(
					tzidPos, tzidLastPos);
				//TestLog.o(XXXX.class, "Zone: " + zoneIDString);
			
			ZoneId zId = getZoneIdFromString(zoneIDString, null, null);
			if (zId == null) { // warning was shown...
				//zId = defaultZoneId;
				return null;
			} else { // valid zone ID
				zIdPrefix = "TZID=" + zoneIDString + ":";
			}			
		}		
		String datesString = dateTimeListString.substring(dateTimeListString.indexOf(":") + 1, 
				dateTimeListString.length());
		String[] dateStrings = datesString.split(",");
		int datesLen = dateStrings.length;
		//for(int i=0;i<dateStrings.length;i++)TestLog.o(XXXX.class, "     " + dateStrings[i]);

		dates = new LocalDate[datesLen];
		for (int i = 0; i < datesLen; i++) {
			Temporal temp = getDateTime(zIdPrefix + dateStrings[i]);
			if (temp instanceof LocalDateTime) {
				dates[i] = ((LocalDateTime) temp).toLocalDate();
			} else if (temp instanceof LocalDate) {
				dates[i] = (LocalDate) temp;
			}
		}
		return dates;
	}
	
	/**
	 * Get the Temporal of a given String. This is
	 * a LocalDate if the String contains VALUE=DATE
	 * or a LocalDateTime otherwise. UTC and time zone
	 * parameters are considered. All values are converted into
	 * the default zone ID of the system.
	 * 
	 * @param dateTimeString	the String containing temporal informations
	 * 
	 * @return	a LocalDate or a LocalDateTime or null of parsing failed
	 */
	public static final Temporal getDateTime(String dateTimeString) {
		
		// extract the dateTime:
		String dateTime = dateTimeString.substring(dateTimeString.indexOf(":") + 1, dateTimeString.length());


		Temporal temp = null;
		if (dateTime.endsWith("Z")) { // this is always a date-time
			
			ZoneId utcId = ZoneId.of("UTC");
			// do not consider currentCalendarZone
			// extract LocalDateTime String 
			String ldtString = dateTime.substring(0,dateTime.length() - 1); //remove Z
			// get ZonedDateTime:
			try {
				ZonedDateTime ldt = ZonedDateTime.parse(ldtString, CalendarSetting.getIcalDateTimeFormatter().withZone(utcId));
				temp = zonedDateTimeToLocalDateTime(ldt);
			} catch (Exception e) { // TODO check if leading zero in time is missing
				TestLog.ve(ICalDateTimeHelper.class, "Cant parse string " + ldtString + " - " + e.getLocalizedMessage() + " - " + e.toString());
				TestLog.e(ICalDateTimeHelper.class, "Cant parse string " + ldtString + " - try set to day start...");
				ldtString = dateTime.substring(0, ldtString.indexOf("T")) + "T000000"; // set to 
				try {
				ZonedDateTime ldt = ZonedDateTime.parse(ldtString, CalendarSetting.getIcalDateTimeFormatter().withZone(ZoneId.systemDefault()));
				temp = zonedDateTimeToLocalDateTime(ldt);
				} catch (Exception e1) {
					TestLog.ve(ICalDateTimeHelper.class, "Cant parse string " + ldtString + " - " + e1.getLocalizedMessage() + " - " + e1.toString());

				}
			}

		} else if (dateTimeString.contains("TZID=")) { // date-time or date-only
			// example: DTSTART;TZID=Europe/Berlin:20180329T193000
			// do not consider currentCalendarZone
			// extract the zone ID String:
			int tzidPos = dateTimeString.indexOf("TZID=") + 5;
			int tzidLastPos = dateTimeString.indexOf(";",tzidPos);
			if (tzidLastPos == -1) { // no other parameter follows
				tzidLastPos = dateTimeString.indexOf(":");
			}
			String zoneIDString = dateTimeString.substring(
					tzidPos, tzidLastPos);
			//TestLog.o(XXXX.class, zoneIDString);
			ZoneId zId = getZoneIdFromString(zoneIDString, null, null);
			if (zId == null) { // warning was shown...
				zId = defaultZoneId;
			}			
			
			// get ZonedDateTime of this String:
			ZonedDateTime zdt = null;
						
			if (dateTime.contains("T")) { // date-time
				
				try {
					zdt = ZonedDateTime.parse(
						dateTime, CalendarSetting.getIcalDateTimeFormatter().withZone(zId));	
				} catch(DateTimeException dte) {
					ReadICal.setInvalidFormatFound(true);
					new UnexpectedValueException("dateTime", "String", "Can't be parsed to ZoneDateTime: " + dateTimeString).printDescription();
					return null;
				}
				//convert to LocalDateTime
				temp = zonedDateTimeToLocalDateTime(zdt);
				
			} else { // date-only				
				
				//if ( ! (dateTimeString.contains("VALUE=DATE;") || dateTimeString.contains("VALUE=DATE:"))) {
				//	TestLog.e(getClass(), "Invalid format: date-only is not marked by VALUE: " + dateTimeString);
				//}
				try {
					zdt = ZonedDateTime.parse(
						dateTime + "T000000", CalendarSetting.getIcalDateTimeFormatter().withZone(zId));
				} catch (Exception e) {
					ReadICal.setInvalidFormatFound(true);
					new UnexpectedValueException("dateTime", "String", "Can't be parsed to ZoneDateTime: " + dateTimeString).printDescription();
					return null;
				}
				//convert to LocalDateTime and then to LocalDate
				temp = zonedDateTimeToLocalDate(zdt);
			}
			
		} else if (currentCalendarZone != null) { // no UTC, no TZID: use time zone of calendar
			
			if (dateTime.contains("T")) { // date-time
				
				try {
					// use formatter with zone and convert to LocalDateTime		
					// create ZoneDateTime from String with calendar zone
					ZonedDateTime tempZone = ZonedDateTime.parse(
							dateTime, CalendarSetting.getIcalDateTimeFormatter().withZone(currentCalendarZone));
					// convert to LocalDateTime		
					temp = zonedDateTimeToLocalDateTime(tempZone);		
				} catch (DateTimeParseException e1) {
					ReadICal.setInvalidFormatFound(true);
					new UnexpectedValueException("dateTimeString", "String", "Can't be parsed: " + dateTime).printDescription();
				}
				
			} else { //  date-only: use zone ID of calendar and convert to LocalDate: 
				
				try {
					// use formatter with zone and get LocalDateTime from String
					// used middle of day,otherwise day start may change: 12:00 h
					ZonedDateTime tempZone = ZonedDateTime.parse // add time to use DateTimeFormatter
							(dateTime + "T120000", CalendarSetting.getIcalDateTimeFormatter().withZone(currentCalendarZone));
					// get LocalDate from defZone
					temp = zonedDateTimeToLocalDate(tempZone);		
					
				} catch (DateTimeParseException e1) {
					ReadICal.setInvalidFormatFound(true);
					new UnexpectedValueException("dateTime", "String", "Can't be parsed: "+ dateTime).printDescription();
				}
			}			
			
		} else { // no UTC, no TZID, no time zone of calendar: Use LocalDate or LocalDateTime
			
			if (dateTime.contains("T")) { // date-time
				try {
					temp = LocalDateTime.parse(dateTime, CalendarSetting.getIcalDateTimeFormatter());
				} catch (DateTimeParseException e1) {
					ReadICal.setInvalidFormatFound(true);
					new UnexpectedValueException("dateTimeString", "String", "Can't be parsed: " + dateTime).printDescription();
				}
			} else { // date-only
				try {
					temp = LocalDate.parse(dateTime, CalendarSetting.getIcalDateFormatter());
				} catch (DateTimeParseException e1) {
					ReadICal.setInvalidFormatFound(true);
					new UnexpectedValueException("dateTime", "String", "Can't be parsed: "+ dateTime).printDescription();
				}
			}			
		}
		return temp;
	}
	
	public static LocalDateTime zonedDateTimeToLocalDateTime(ZonedDateTime zdt) {
		// create ZoneDateTime from tempZpne with default zone
		ZonedDateTime defZoned = zdt.withZoneSameInstant(defaultZoneId);
		// get LocalDateTime from defZone
		return defZoned.toLocalDateTime();	
	}
	
	public static LocalDate zonedDateTimeToLocalDate(ZonedDateTime zdt) {
		// create ZoneDateTime from tempZpne with default zone
		ZonedDateTime defZoned = zdt.withZoneSameInstant(defaultZoneId);
		// get LocalDateTime from defZone
		return defZoned.toLocalDate();	
	}
	/**
	 * Get number of seconds from duration string.
	 * This ignores the minus sign. 
	 * 
	 * @param durationString	the String containing the duration info
	 * 
	 * @return	number of seconds as an always positive long
	 */
	protected static final long getDurationInSeconds(String _durationString) {
		
		long d = 0; // duration in seconds
		String durationString = _durationString;
		if (_durationString.startsWith("-")) { // ignore the minus sign
			durationString = _durationString.substring(1);
		}
		
		if (durationString.contains("W")) {// weeks: first value after P
			int weeks = Integer.parseInt(durationString.substring(durationString.indexOf('P') + 1, durationString.indexOf('W')));
			d += weeks *60*60*24*7;
		}
		if (durationString.contains("D")) {// days
			int days = 0;
			if (durationString.contains("W")) { // after W
				days = Integer.parseInt(durationString.substring(durationString.indexOf('W') + 1, durationString.indexOf('D')));
			} else { //after  P
				days = Integer.parseInt(durationString.substring(1, durationString.indexOf('D')));
			}
			d += days *60*60*24;
		}
		if (durationString.contains("T")){ // time
			if (durationString.contains("H")){ // first value after T
				int hours = Integer.parseInt(durationString.substring(durationString.indexOf('T') + 1, durationString.indexOf('H')));
				d += hours *60*60;
			}
			if (durationString.contains("M")){
				int min = 0;
				if (durationString.contains("H")){ // after H
					min = Integer.parseInt(durationString.substring(durationString.indexOf('H') + 1, durationString.indexOf('M')));
				} else { // after T
					min = Integer.parseInt(durationString.substring(durationString.indexOf('T') + 1, durationString.indexOf('M')));
				}			
				d += min *60;
			}
			if (durationString.contains("S")){
				int s = 0;
				if (durationString.contains("M")){ // after M
					s = Integer.parseInt(durationString.substring(durationString.indexOf('M') + 1, durationString.indexOf('S')));	
				} else {
					if (durationString.contains("H")){ // after H
						s = Integer.parseInt(durationString.substring(durationString.indexOf('H') + 1, durationString.indexOf('S')));	
					} else { // after T
						s = Integer.parseInt(durationString.substring(durationString.indexOf('T') + 1, durationString.indexOf('S')));	
					}									
				}	
				d += s;
			}						
		}
		return d;
	}
	
	/**
	 * Get the duration of a duration string. 
	 * This does not show whether the value is positive or negative.
	 * 
	 * @param _durationString	the string representation of a duration
	 * 
	 * @return	the Duration, not null, but maybe Duration.ZERO
	 */
	public static final Duration getDurationFromString(String _durationString) {
		
		Duration dur = Duration.ZERO;
		String durationString = _durationString;
		if (_durationString.startsWith("-")) { // ignore the minus sign
			durationString = _durationString.substring(1);
		}
		
		if (durationString.contains("W")) {// weeks: first value after P
			
			int weeks = Integer.parseInt(durationString.substring(durationString.indexOf('P') + 1, durationString.indexOf('W')));
			//d += weeks *60*60*24*7;
			dur = dur.plusDays(weeks * 7);
		}
		if (durationString.contains("D")) {// days
			int days = 0;
			if (durationString.contains("W")) { // after W
				days = Integer.parseInt(durationString.substring(durationString.indexOf('W') + 1, durationString.indexOf('D')));
			} else { //after  P
				days = Integer.parseInt(durationString.substring(1, durationString.indexOf('D')));
			}
			// += days *60*60*24;
			dur = dur.plusDays(days);
		}
		if (durationString.contains("T")){ // time
			if (durationString.contains("H")){ // first value after T
				int hours = Integer.parseInt(durationString.substring(durationString.indexOf('T') + 1, durationString.indexOf('H')));
				//d += hours *60*60;
				dur = dur.plusHours(hours);
			}
			if (durationString.contains("M")){
				int min = 0;
				if (durationString.contains("H")){ // after H
					min = Integer.parseInt(durationString.substring(durationString.indexOf('H') + 1, durationString.indexOf('M')));
				} else { // after T
					min = Integer.parseInt(durationString.substring(durationString.indexOf('T') + 1, durationString.indexOf('M')));
				}			
				//d += min *60;
				dur = dur.plusMinutes(min);
			}
/*			if (durationString.contains("S")){
				int s = 0;
				if (durationString.contains("M")){ // after M
					s = Integer.parseInt(durationString.substring(durationString.indexOf('M') + 1, durationString.indexOf('S')));	
				} else {
					if (durationString.contains("H")){ // after H
						s = Integer.parseInt(durationString.substring(durationString.indexOf('H') + 1, durationString.indexOf('S')));	
					} else { // after T
						s = Integer.parseInt(durationString.substring(durationString.indexOf('T') + 1, durationString.indexOf('S')));	
					}									
				}	
				//d += s;
				dur = dur.plusSeconds(s);
			}					*/	
		}
		return dur;
	}
		
	/**
	 * Convert an event duration value
	 * to an event date-time value (point in time). 
	 * 
	 * @param dateTime		the dateStart or dateEnd of the event
	 * @param durationString	the String containing the duration infos
	 * 
	 * @return	the point in time (LocalDateTime)
	 */
	public static final LocalDateTime durationToDateTime(LocalDateTime dateTime, String durationString) {
		// example: DURATION:PT1H0M0S  one hour, zero min and s
		// DURATION:PT15M   15 min 
		// P15DT5H0M20S  15 days, 5 hours, and 20 seconds
		// P7W  7 weeks
		// leading P (period?)
		// T before time values
		// W D H M S  week, day, hour, minute, second	
		// check all periods and add seconds
		long d = 0; // duration in seconds
		if (! (durationString.startsWith("P") || durationString.startsWith("-P"))) {
			TestLog.ve(ICalDateTimeHelper.class,"Invalid duration string: no leading P");
			ReadICal.setInvalidFormatFound(true);
		}
		boolean afterDateTime = true;
		if (durationString.startsWith("-")) { // typically for trigger of VALARM
			afterDateTime = false;
		}
		d = getDurationInSeconds(durationString);
		
		// convert duration in LocalDateTime:
		if (afterDateTime == true) {
			return dateTime.plusSeconds(d);
		} else {
			return dateTime.minusSeconds(d);
		}
	}
	
	/**
	 * Get an Instant for time stamps from an iCalendar
	 * DTSTAMP-String excluding DTSTAMP:, including Z
	 * 
	 * @param utcString The String excluding DTSTAMP:, including Z
	 * 
	 * @return	the Instant of the String or null if parsing failed
	 */
	public static Instant getInstantFromUtcString(String utcString) {
		utcString = utcString.substring(0 ,utcString.length() - 1); //remove Z 
		Instant instant = null; 
		try {
			instant = ZonedDateTime.parse(utcString, CalendarSetting.getIcalDateTimeFormatter().withZone(ZoneId.of("UTC"))).toInstant();
		} catch (Exception e){
			ReadICal.setInvalidFormatFound(true);
			TestLog.ve(ICalDateTimeHelper.class, "Cant parse string " + utcString + " - " + e.getLocalizedMessage() + " - " + e.toString());
			try { // TODO check if time value is too short - missing leading zero?
				utcString = utcString.substring(0, utcString.indexOf("T")) + "T000000"; // set to 
				instant = ZonedDateTime.parse(utcString, CalendarSetting.getIcalDateTimeFormatter().withZone(ZoneId.systemDefault())).toInstant();
			} catch (Exception e1) {
				TestLog.ve(ICalDateTimeHelper.class, "Cant still parse string " + utcString + " - " + e1.getLocalizedMessage());
			}
		}
		return instant;
	}
	
	/**
	 * Get the zone ID from a String. 
	 * This String should contain only the zone ID. 
	 * The function handles exceptions
	 * 
	 * @param zoneIdString	the String containing only the zone ID
	 * @param cal			the used calendar or null
	 * @param e				the used event or null
	 * 
	 * @return	the zone ID of the String or null
	 */
	public static ZoneId getZoneIdFromString(String zoneIdString, PCalendar cal, PEvent e) {
		// get the zone ID from String
		ZoneId zId = null;
		try {
			zId = ZoneId.of(zoneIdString);
		} catch (ZoneRulesException zre) {//  the zone ID is a region ID that cannot be found
			if (PeaProperties.isTestMode()) {
				TestLog.ve(ICalDateTimeHelper.class, "zone ID is a region ID that cannot be found: " 
					+ zoneIdString + " for " + ((cal != null && cal.getName() != null) ? "  for: " + cal.getName() : " ") );
			}
			
			PeaDialog.showMessage(
					LockFrameCalendar.getFrame(), 
					CalendarSetting.calTranslate("reading_time_zone_failed")
					+ "\n" + zoneIdString 
						+	((cal != null && cal.getName() != null) ? "  for: " + cal.getName() : " ") 
						+	((e != null && e.getSummary() != null) ? " - " + e.getSummary() : " ") 
					+ "\n" + zre.getLocalizedMessage(), 
					CalendarSetting.calTranslate("error"),
					0); // error message	
			//ReadCard.setUnsupportedPropertiesFound(true);
		} catch (DateTimeException dte) { // zone ID has an invalid format
			if (PeaProperties.isTestMode()) {
				if (cal != null && cal.getName() != null) {
					TestLog.ve(ICalDateTimeHelper.class,zoneIdString  + " - zone ID has an invalid format: " + cal.getName());
				}
			}
			PeaDialog.showMessage(
					LockFrameCalendar.getFrame(), 
					CalendarSetting.calTranslate("reading_time_zone_failed")
					+ "\n" 
					+	(( cal != null) ? cal.getName() : "no calendar") + "   " 
					+ 	(( e != null) ? e.getSummary() : "no event")
					+ "\n" + dte.getLocalizedMessage(), 
					CalendarSetting.calTranslate("error"),
					0); // error message					
			ReadICal.setInvalidFormatFound(true);
			new UnexpectedValueException("zoneIdString", "String", "Can't find zone ID").printStackTraceLines();
		}
		return zId;
	}
	
	/**
	 * Get the time zone difference in hours between the 
	 * time zone of an external event or calendar or event
	 * at a specific date and the here used system default time zone.
	 * This does consider summer/winter time
	 * 
	 * @param externZoneId	time zone of external calendar or event
	 * @param date			date to compute the difference
	 * 
	 * @return	the difference to here used default time zone in hours
	 * 			considering summer and winter time
	 */	
/*	private static int getTimeZineDiffOfDate(ZoneId externZoneId, LocalDate date) {
		return externZoneId
	    .getRules() // all rules like summer and winter time
	    .getOffset( 
	        date          // Specify a certain date. Has no concept of time zone or offset.
	        .atStartOfDay( externZoneId )  // Determine the first moment of the day on that date in that region. Not always `00:00:00` because of anomalies such as Daylight Saving Time.
	       .toLocalDateTime(). .toInstant()     // Adjust to UTC by extracting an `Instant`. 
	    ) 
	    .getTotalSeconds() / 60 / 60;// in hours, not seconds
	} */
	
	/**
	 * Get the current default zone ID of the system
	 * to use as calendar zone ID for writing
	 * 
	 * @return 	the default zone ID
	 */
	protected static ZoneId getDefaultZoneId() {
		return defaultZoneId;
	}

	/**
	 * Set the currently read time zone for one calendar
	 * 
	 * @param	the time zone of the currently read calendar
	 */
	protected static void setCurrentCalendarZone(ZoneId newZone) {
		currentCalendarZone = newZone;
	}
}
