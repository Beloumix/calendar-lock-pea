package cologne.eck.calendar_pea.util;

/*
 * PeaFactory - Production of Password Encryption Archives
 * Copyright (C) 2015  Axel von dem Bruch
 * 
 * This library is free software; you can redistribute it and/or modify it 
 * under the terms of the GNU General Public License as published 
 * by the Free Software Foundation; either version 2 of the License, 
 * or (at your option) any later version.
 * This library is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
 * or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU General Public License for more details.
 * See:  http://www.gnu.org/licenses/gpl-2.0.html
 * You should have received a copy of the GNU General Public License 
 * along with this library.
 */

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Arrays;


import cologne.eck.calendar_pea.data.PEvent;
import cologne.eck.calendar_pea.data.RecurringPEvent;
import cologne.eck.tools.TestLog;
import cologne.eck.tools.UnexpectedValueException;

public class RecurringEventComposer {
	
	private RecurringPEvent re;
	private ArrayList<ArrayList<PEvent>> resultLists; // one list for one day
	/**
	 * true: only check if event ends before date, false: create list of day lists
	 */
	private boolean checkEnd = false;
	
	// order of process:
	private ChronoUnit frequency;// = re.getFrequency();
	private int interval;// = re.getInterval();
	private String[] byDay;// = re.getByDay();
	private int[] byMonth;
	private int count;// = re.getCount();
	private boolean checkCount;// = false;
	private LocalDateTime until;// = re.getUntil();
	private LocalDate[] rDates;// = re.getRecurrenceDates();// sorted
	private LocalDate[] exceptions;// = re.getExceptionDates();// sorted
	private boolean checkExceptions;// = true;
	private LocalDate untilDate;// = null;


	// values for performance and readability
	private boolean daily = false;
	private boolean weekly = false;
	private boolean monthly = false;
	private boolean yearly = false;	
	private LocalDate startDate;// = eventStartDateTime.toLocalDate();		
	private LocalDate beginDate;
	private int numberOfDays;
	private LocalDate endDateExclusive;
	private int doneCount = 0;
	private int[] dayOfWeekValues;
	private int byDayLength = 0;
	private int[] byIntegers;// positive and negative
//	private DayOfWeek[] dayOfWeek;			
	private int[] dayOfWeek;
	private int startDayOfMonth;
	
	/*
	 * If the recurring event and all its instances end before the given date
	 */
//	private boolean endsBefore = true;
	

	/**
	 * Composer for recurring events: Get day lists to display in view or 
	 * check if recurring event ends before the given date (for data at rest)
	 * 
	 * @param _re			the recurring event
	 * @param _checkEnd		true: only check if event ends before date (_resultLists is null)
	 * @param _resultLists	the resulting list of lists: one list for each day or null to check for data at rest
	 * @param _beginDate	the begin day (where events are included)
	 * @param _numberOfDays	the number of days (and lists) - to check if event is after a date, set to Integer.MAX
	 */
	public RecurringEventComposer(RecurringPEvent _re, // boolean _checkEnd, 
			ArrayList<ArrayList<PEvent>> _resultLists, LocalDate _beginDate, int _numberOfDays) {
		this.re = _re;
		this.rDates = re.getRecurrenceDates();// sorted
//		this.checkEnd = _checkEnd;
		this.resultLists = _resultLists;
		this.beginDate = _beginDate;
		this.numberOfDays = _numberOfDays;
		if(this.checkEnd == true && this.resultLists != null) {
			TestLog.ve(getClass(), "Invalid arguments for constructor", 5);
		} else if (this.checkEnd == false && this.resultLists == null) {
			TestLog.ve(getClass(), "Invalid arguments for constructor", 5);
		}
		// order of process:
		frequency = re.getFrequency();
		interval = re.getInterval();
		byDay = re.getByDay();
		if (byDay != null) {
			byDayLength = byDay.length;
		}
		count = re.getCount();
		checkCount = false;
		if (count > -1) {
			checkCount = true;
		}
		until = re.getUntil();
		exceptions = re.getExceptionDates();// sorted
		if (exceptions == null ) {
			checkExceptions = false;
		} else { 
			checkExceptions = true;
		}
		untilDate = null;
		if (until != null) {
			untilDate = until.toLocalDate();
		}

		// values for performance and readability
		if (frequency == null) {
			if (rDates == null) {
				new UnexpectedValueException("frequency", "String", "is null for "+ re.getSummary() +" and no rDates defined").printDescription();
				
			} else {
				// only RDATE
			}			
		} else if (frequency.equals(ChronoUnit.DAYS)) {
			daily = true;
		} else if (frequency.equals(ChronoUnit.WEEKS)) {
			weekly = true;
		} else if (frequency.equals(ChronoUnit.MONTHS)) {
			monthly = true;
		} else if (frequency.equals(ChronoUnit.YEARS)) {
			yearly = true;
		} else {
			new UnexpectedValueException("frequency", "ChronoUnit", "is invalid)").printDescription();
		}		
		startDate = re.getDateStart().toLocalDate();		
		endDateExclusive = beginDate.plusDays(numberOfDays);
		
		if (byDay != null) {
			//int byDayLength = byDay.length;
			// get the defined weekdays
			if (weekly == true) { // || daily == true) {
				dayOfWeekValues = re.getByDayAsInt();
			/*	dayOfWeekValues = new int[byDayLength];
				for (int byDayIndex = 0; byDayIndex < byDayLength; byDayIndex++) {
					String byDayVal = byDay[byDayIndex];
					dayOfWeekValues[byDayIndex] = CalendarSetting.getDayOfWeekValue(byDayVal);
				}

				// sort to allow binary search
				Arrays.sort(dayOfWeekValues);			*/
			} else if (monthly == true || yearly == true) { 
				int[][] byDayNumAndWD = re.getByDayNumbersAndWeekDays();
				if (byDayNumAndWD != null) {
					byIntegers = byDayNumAndWD[0];// positive and negative
					dayOfWeek = byDayNumAndWD[1];	
				}
				
		/*		// preparatory work: set weekdays and integers
				byIntegers = new int[byDayLength];// positive and negative
				dayOfWeek = new DayOfWeek[byDayLength];				
				// get the defined weekdays, signs and integers
				for (int byDayIndex = 0; byDayIndex < byDayLength; byDayIndex++) {
					String byDayVal = byDay[byDayIndex];
					// weekday:
					String weekDayString = byDayVal.substring(byDayVal.length() - 2, byDayVal.length());
					//dayOfWeek[byDayIndex] = DayOfWeek.of(Arrays.asList(weekDayShortNames).indexOf(weekDayString) + 1);// +1 because of index
					dayOfWeek[byDayIndex] = CalendarSetting.getDayOfWeek(weekDayString);
					// integers: always one digit + sign - no range check (1-4)
					String intString = byDayVal.substring(0, byDayVal.length() - 2);// include the sign
					byIntegers[byDayIndex] = Integer.parseInt(intString);
				}*/
			}
		} 		
		byMonth = re.getByMonth();
		startDayOfMonth = startDate.getDayOfMonth();
	}
	
	// TODO 
	// ? constructor ? without array list...
	// ODER: in class RecurringEvent
	// function getDayLists(LocalDate start, numberOfDays, ArrayList<ArrayList
	// ? EXTRA function isBefore(LocalDate date)
	// ? INSIDE getDayLists ?  triggerDate statt endDate ? : function getAlarms(LocalDateTime start,LocalDateTime end)
	//  -> getDayLists(LocalDateTIME start, LocalDateTime end, ArrayList<ArrayList
	//  -> bei Alarms: start - triggerDuration, end - triggerDuration => extra ArrayList<PAlarm>
	// Aufteilen in Teilfunktionen ? rDates, exDates interval+frequency+byDay: aufteilen daily, weekly...?
	
	/**
	 * If checkEnd is false:
	 * Add event if it matches conditions 
	 * to all days of requested period.
	 * If checkEnd is true: Check if event ends before date
	 */
	public void processRecurringEvent() {

		
		// rDates extra loop //////////////////////////////////////////////////							
		// check rDates - independent of recurrence rules
		if (rDates != null) {
			int rDatesSize = rDates.length;
			for (int rDateIndex = 0; rDateIndex < rDatesSize; rDateIndex++) {
				LocalDate ld = rDates[rDateIndex];
				if (ld.isBefore(beginDate)) {
					continue;
				} else if (ld.isBefore(endDateExclusive)) { // add ?
					// check exceptions
					if (checkExceptions == true) {
						if (Arrays.binarySearch(exceptions, ld) < 0) { // no exception for this date
							// get index in resultLists (day)
							int daysIndex = (int) ChronoUnit.DAYS.between(beginDate, ld);
					//		if (checkEnd == false) {
								resultLists.get(daysIndex).add(re);// add to rcurringList
								//					TestLog.o(XXXX.class, "added: " + re.getSummary() + " at " + beginDate.plusDays(daysIndex));
					/*		} else {
								endsBefore = false;
								return;
							*/
						}							
					} else {
						// get index in resultLists (day)
						int daysIndex = (int) ChronoUnit.DAYS.between(beginDate, ld);
					//	if (checkEnd == false) {
							resultLists.get(daysIndex).add(re);// add to rcurringList
					/*	} else {
							endsBefore = false;
							return;
						}*/
					}
				} else if ( ! ld.isBefore(endDateExclusive)) {
					break;
				}
			}
		}
		if (frequency == null) {
			// only RDATE, no RRULE
			// set start date manually:
			checkEventAndAdd(startDate);
			return;
		}
		

		if (untilDate != null && untilDate.isBefore(beginDate)) {
			return;
		}

		boolean continueProcess = true;
		// set extra: starting week/month/year
		// this may also increment doneRepetitions
		continueProcess = setStartChronoUnit();

		if (continueProcess == false) {
			return;
		}


		//	TestLog.o(XXXX.class, "done repetitions after processing start unit: " + doneRepetitions);
		if (checkCount == true && doneCount >= count) {
			return;
		}
		// next step
		// TODO set checkDate to next match of beginDate not startDate
		
		LocalDate checkDate = startDate.plus(interval, frequency);// one step
		if (monthly == true) { 
			// 31. (30., 29.) of month problem: RFC5545 is not clear about this
			if (checkDate.lengthOfMonth() < startDayOfMonth) {
				// iOS solution: take the last day if startDayofMonth is not available:
				checkDate = checkDate.withDayOfMonth(checkDate.lengthOfMonth());				
				// Google Calendar solution: do not add if startOfMonth does not exist
			} else {
				checkDate = checkDate.withDayOfMonth(startDayOfMonth);
			}
		}

		// recurrence rule loop without byDay 
		if (byDay == null || daily == true) {
			LocalDate lastDateToCheck = endDateExclusive;
			if (numberOfDays == Integer.MAX_VALUE) {
				lastDateToCheck = beginDate.plus(interval + 1, frequency);
				if (daily == true) {
					lastDateToCheck = beginDate.plus(1, ChronoUnit.WEEKS);
				}
			}

			while (checkDate.isBefore(lastDateToCheck)) {

				continueProcess = checkEventAndAdd(checkDate);

				if (continueProcess == false){
					return;
				}
				if (yearly == true) {
					checkDate = checkDate.withYear(checkDate.getYear() + (interval * 1));
					checkDate = checkDate.withMonth(startDate.getMonthValue()).withDayOfMonth(startDayOfMonth);
					
				} else {
					checkDate = checkDate.plus(interval, frequency);// one step
					if (monthly == true) { // 31. will be interpreted as last day of month
						// if no available: take last day
						if (checkDate.lengthOfMonth() < startDayOfMonth) {
							// take the last day
							checkDate = checkDate.withDayOfMonth(checkDate.lengthOfMonth());
						} else {
							checkDate = checkDate.withDayOfMonth(startDayOfMonth);
						}
					}
				}
			}

		} else { // if (byDay == null)						

			// BYDAY EVENTS ============================================>

			if (weekly == true) { //daily == true || weekly == true) {

				// TODO WKST parameter
				// default week start is Monday (WKST)
				int startDayValue = checkDate.getDayOfWeek().getValue();// 1-7
				// set checkDate to previous Monday
				checkDate = checkDate.minusDays(startDayValue - 1);

				LocalDate lastDateToCheck = endDateExclusive;
				if (numberOfDays == Integer.MAX_VALUE) {
					lastDateToCheck = beginDate.plus(interval + 1, frequency);
				}

				while (checkDate.isBefore(lastDateToCheck)) {
				// loop for daily and weekly events
//X				while (checkDate.isBefore(endDateExclusive)) {

					// handle the week: (checkDate is always a Monday)
					// include dates if in required period and match weekdays									
					LocalDate nextCheckDate = null;//  don't modify checkDate here
					for (int wIndex = 0; wIndex < byDayLength; wIndex++) {
						// check next weekday defined in BYDAY
						nextCheckDate = checkDate.plusDays(dayOfWeekValues[wIndex] -1);// MO is 1, but should add 0
						continueProcess = checkEventAndAdd(nextCheckDate);
						if (continueProcess == false){
							return;
						} 
					}// end for weekly			

					// increment the date to check by one interval
					checkDate = checkDate.plus(interval, frequency);// one step
				} // end while (checkDate.isBefore(endDateExclusive))				

			} else if (monthly == true || yearly == true) { // yearly: exact one BYMONTH is defined
				
				checkDate = checkDate.withDayOfMonth(1);// first day of month
				if (yearly == true) {
					if (re.getByMonth() != null) {
						// set to BYMONTH value 
						checkDate = checkDate.withMonth(re.getByMonth()[0]);		
					} else { // DTSTART
						checkDate = checkDate.withMonth(startDate.getMonthValue());
					}
				}				
			
				LocalDate lastDateToCheck = endDateExclusive;
				if (numberOfDays == Integer.MAX_VALUE) {
					lastDateToCheck = beginDate.plus(interval + 1, frequency);
				}

				while (checkDate.isBefore(lastDateToCheck)) {
				// the loop for monthly and yearly events with BYDAY parameter:
//X				while (checkDate.isBefore(endDateExclusive)) {

					LocalDate nextCheckDate = null;
					for (int byDayIndex = 0; byDayIndex < byDayLength; byDayIndex++) { // check all BYDAY values
						nextCheckDate = RecurringPEvent.getXthWeekDayOfMonth(DayOfWeek.of(dayOfWeek[byDayIndex]), byIntegers[byDayIndex], checkDate);
						continueProcess = checkEventAndAdd(nextCheckDate);
						// TODO other BYDAY might be before this date...
						// (BYDAY is only sorted by String order).
						// -> collect first all occurrences of this interval and sort by date
						// then add until doneCount > count
						if (continueProcess == false) {
							// do not return here: dayOfWeek is not sorted!!
							continue;
						}

					}
					checkDate = checkDate.plus(interval, frequency);// one step
				}
			}
		}
	}
	
	/**
	 * Check if a recurring event ends before the begin date given in the
	 * constructor after processing. 
	 * 
	 * @return	true: event ends before given date, 
	 * 			false: at least one recurrence is after the given date
	 */
/*	public boolean endsBeforeX() {
		return this.endsBefore;
	} */


	/**
	 * get the positive or negative (first/last) x'th weekday of a month of a LocalDate, 
	 * e.g. the second(-last) Tuesday of the month of a date
	 * 
	 * @param dow	the day of the week as DayOfWeek instance
	 * @param xTH	the ordinal value (the second, the third...) with sign
	 * 				this must be in the range 1 - 4 or -1 - -4
	 * @param date	the date to check its month
	 * 
	 * @return	the x'th first weekday of the month of the given LocalDate
	 */
/*	public static LocalDate getXthWeekDayOfMonth(DayOfWeek dow, int xTH, LocalDate date) {

		return date.with(TemporalAdjusters.dayOfWeekInMonth(xTH, dow));
	}*/

	/**
	 * Check this date and add RecurringEvent 
	 * if date is not after until (if until defined),
	 * if date is before beginDate,
	 * if date is after endDateExclusive,
	 * if all repetitions are done,
	 * and if no exceptions for this date are defined. 
	 * This increments the done repetitions only if the event was added
	 * 
	 * @param checkDate	 the date to check
	 * 
	 * @return	true: continue to process this event,
	 * 			false: break process (end is reached: until, repetitions or endDate of requested period)
	 */
	private boolean checkEventAndAdd(LocalDate checkDate) {		
		
		// check until
		if (untilDate != null && checkDate.isAfter(untilDate)) {
			return false;
		}
		if (! checkDate.isBefore(endDateExclusive)) {
			return false;
		}
		if (checkCount == true && doneCount >= count) {
			return false;
		}
		if (checkDate.isBefore(beginDate)) {
			checkExceptionsAndCount(checkDate, false); //  do not add event but increment doneCount
			//return true;
		} else {					
			checkExceptionsAndCount(checkDate, true); //  add event
		}

		if (checkCount == true && doneCount >= count) {
			return false;
		}
		return true;
	}
		
	/**
	 * Set the first unit: starting day, week, month, year. 
	 * Add events if they match the requirements. 
	 * This must be done extra, because some BYDAYs of the 
	 * starting unit may be before startDate and therefore must not
	 * be shown
	 * 
	 * @return	false: end of event was reached 
	 * 				(until, repetitions or endDate of requested interval)
	 * 				-> stop the process of this event
	 * 			true: continue to process the event
	 */
	private boolean setStartChronoUnit() {
		// handle the DTSTART: don't check weekday here
		boolean continueProcess = true;

		// Handle the start date: add if in period and increment always doneCount
		if ( startDate.isAfter(beginDate) || startDate.equals(beginDate)){
			checkExceptionsAndCount(startDate, true);// add
		} else {
			checkExceptionsAndCount(startDate, false);// do not add
		} 
		if (byDay == null) {
			// start date was added, no need for further check
			return true;
		} else  {
			
		/*	int[][] byDayNumAndWD = re.getByDayNumbersAndWeekDays();
			if (byDayNumAndWD != null) {
				byIntegers = byDayNumAndWD[0];// positive and negative
				dayOfWeek = byDayNumAndWD[1];	
			}*/

		// Handle other dates of this unit for week, month, year
		//if (byDay != null) {
			if (weekly == true) {
				// check if next BYDAY in week is after startDate 
				int startDayOfWeekValue = startDate.getDayOfWeek().getValue();
				LocalDate startOfWeekDate = startDate.minusDays(startDayOfWeekValue - 1);// set to Monday of start week
				LocalDate nextCheckDate = null;
				//		TestLog.o(XXXX.class, "startOfWeekDate: " + startOfWeekDate + " day: " + startOfWeekDate.getDayOfWeek() );
				for (int wIndex = 0; wIndex < byDayLength; wIndex++) {
					nextCheckDate = startOfWeekDate.plusDays(dayOfWeekValues[wIndex] -1);// MO is 1, but should add 0
					if (dayOfWeekValues[wIndex] <= startDayOfWeekValue) { // is before or equal startDate
						continue;
					}
					boolean result = checkEventAndAdd(nextCheckDate);

					if (result == false) {
						return false;
					} 
				}

			} else if (monthly == true || yearly == true) {
				

				if (monthly == true) {
					LocalDate checkDate = startDate.withDayOfMonth(1);// first day of month
					LocalDate nextCheckDate = null;
					for (int byDayIndex = 0; byDayIndex < byDayLength; byDayIndex++) { // check all BYDAY values
						nextCheckDate = RecurringPEvent.getXthWeekDayOfMonth(DayOfWeek.of(dayOfWeek[byDayIndex]), byIntegers[byDayIndex], checkDate);
						if ( nextCheckDate.isAfter(startDate)) {
							// TODO other BYDAY might be before this date...
							// (BYDAY is only sorted by String order).
							// -> collect first all occurrences of this interval and sort by date
							// then add until doneCount > count
							continueProcess = checkEventAndAdd(nextCheckDate);
							if (continueProcess == false) {
								// do not return: dayOfWeek is not sorted!!
								continue;
							}
						}
					}

				} else if (yearly == true){ // TODO delete
					if (byMonth != null && (startDate.getMonthValue() != byMonth[0])){
						// byMonth does not match startDate's month: 
						//TestLog.e(getClass(), "byMonth does not match startDate's month");
						new UnexpectedValueException("byMonth[0]", "int", 
								"doesn't match start date month: byMonth[0] " + byMonth[0] + ", month: " + startDate.getMonthValue()).printDescription();

						if (byMonth[0] > startDate.getMonthValue()) {
							LocalDate checkDate = startDate.withMonth(byMonth[0]).withDayOfMonth(1);// first day of month
							LocalDate nextCheckDate = null;
							for (int byDayIndex = 0; byDayIndex < byDayLength; byDayIndex++) { // check all BYDAY values
								nextCheckDate = RecurringPEvent.getXthWeekDayOfMonth(DayOfWeek.of(dayOfWeek[byDayIndex]), byIntegers[byDayIndex], checkDate);
								if ( nextCheckDate.isAfter(startDate)) {
									// TODO other BYDAY might be before this date...
									// (BYDAY is only sorted by String order).
									// -> collect first all occurrences of this interval and sort by date
									// then add until doneCount > count
									continueProcess = checkEventAndAdd(nextCheckDate);
									if (continueProcess == false) {
										// do not return: dayOfWeek is not sorted!!
										continue;
									}
								}
							}
						} else {
							// BYMONTH before start month: don't set
						}
					} else { // byMonth matches start month
						
						LocalDate checkDate = startDate.withDayOfMonth(1);// first day of month
						LocalDate nextCheckDate = null;
						for (int byDayIndex = 0; byDayIndex < byDayLength; byDayIndex++) { // check all BYDAY values
							nextCheckDate = RecurringPEvent.getXthWeekDayOfMonth(DayOfWeek.of(dayOfWeek[byDayIndex]), byIntegers[byDayIndex], checkDate);
							if ( nextCheckDate.isAfter(startDate)) {
								// TODO other BYDAY might be before this date...
								// (BYDAY is only sorted by String order).
								// -> collect first all occurrences of this interval and sort by date
								// then add until doneCount > count
								continueProcess = checkEventAndAdd(nextCheckDate);
								if (continueProcess == false) {
									// do not return: dayOfWeek is not sorted!!
									continue;
								}
							}
						}
					}
				}
			}
		}
		return true;
	} 

	/**
	 * Check exceptions for a date and check and increment the doneRepetitions
	 * 
	 * @param date		the date to check
	 * @param addEvent	true: add the event, false: only increment doneRepetitions
	 */
	private void checkExceptionsAndCount(LocalDate date, boolean addEvent) {
		//TestLog.o(XXXX.class, "checkExceptionsAndRepetitions date: " + date + ", addEvent: " + addEvent + ", checkExceptions: " + checkExceptions);
		
		if (checkCount == true && doneCount >= count) {
			return;
		}
		if (checkExceptions == true) {
			//for(int i=0;i<exceptions.length;i++)TestLog.o(XXXX.class, "exception: " + exceptions[i]);
			if (Arrays.binarySearch(exceptions, date) < 0) { // no exception for this date		
				// get index in resultLists (day)
				if (addEvent == true) {
					int daysIndex = (int) ChronoUnit.DAYS.between(beginDate, date);
				//	if (checkEnd == false) {
						resultLists.get(daysIndex).add(re);// add to rcurringList
				/*	} else {
						endsBefore = false;
					}	*/				
				}
				doneCount++;					
			}
		} else {
			if (addEvent == true) {
				// get index in resultLists (day)
				int daysIndex = (int) ChronoUnit.DAYS.between(beginDate, date);
			//	if (checkEnd == false) {
					resultLists.get(daysIndex).add(re);// add to rcurringList
			/*	} else {
					endsBefore = false;
				}*/
			}
			doneCount++;
		}
	}
}
