package cologne.eck.calendar_pea.util;

/*
 * PeaFactory - Production of Password Encryption Archives
 * Copyright (C) 2015  Axel von dem Bruch
 * 
 * This library is free software; you can redistribute it and/or modify it 
 * under the terms of the GNU General Public License as published 
 * by the Free Software Foundation; either version 2 of the License, 
 * or (at your option) any later version.
 * This library is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
 * or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU General Public License for more details.
 * See:  http://www.gnu.org/licenses/gpl-2.0.html
 * You should have received a copy of the GNU General Public License 
 * along with this library.
 */

import java.awt.Color;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import cologne.eck.all_peas.data.PeaProperties;
import cologne.eck.calendar_pea.control.CalendarConstants;
import cologne.eck.calendar_pea.control.CalendarSetting;
import cologne.eck.calendar_pea.data.CalendarComponent;
import cologne.eck.calendar_pea.data.PAlarm;
import cologne.eck.calendar_pea.data.PCalendar;
import cologne.eck.calendar_pea.data.PEvent;
import cologne.eck.calendar_pea.data.PTodo;
import cologne.eck.calendar_pea.data.RecurringPEvent;
import cologne.eck.tools.TestLog;
import cologne.eck.tools.UnexpectedValueException;

/**
 * Simplified and incomplete ICalendar writer as defined in:
 * https://tools.ietf.org/html/rfc5545
 * and extensions as defined in: 
 * https://tools.ietf.org/html/draft-daboo-icalendar-extensions-06
 */
public class WriteICal {	
	
	// default line break
	private static final String CRLF = "\r\n";
	
	/**
	 * Converts several PCalendars into one valid 
	 * ICalendar String representation. 
	 * This String is currently used for internal
	 * storing. Old properties (X-WR-) are not supported.  
	 * 
	 * @param calendars		PCalendars as an array
	 * 
	 * @return				iCalendars as a String
	 */
	public static String iCalsToString(PCalendar[] calendars) {
		
		StringBuilder sb = new StringBuilder();		
		int len = calendars.length;
		for (int i = 0; i < len; i++) {
			sb.append(iCalToString(calendars[i]));
		}		
		return new String(sb);
	}
	
	/**
	 * Writes one PCalendar as readable text. 
	 * 
	 * @param calendar		PCard to write
	 * 
	 * @return				iCalendar as a readable text
	 */
	public static String calComponentToReadableString(CalendarComponent calComponent) {

		StringBuilder sb = new StringBuilder();		

		if (calComponent.getSummary() != null) {
		sb.append(calComponent.getSummary() + "\n");
		}
		if (calComponent.getDescription() != null) {
		sb.append(calComponent.getDescription() + "\n");
		}
		if (calComponent.getLocation() != null) {
			sb.append(CalendarSetting.calTranslate("location") + calComponent.getLocation() + "\n");
		}
		if (calComponent.getPriority() > 0) {
			sb.append(CalendarSetting.calTranslate("priority") +  calComponent.getPriority() + "\n");
		}
		if (calComponent.getStatus() != null) {
			String status = CalendarSetting.calTranslate(calComponent.getStatus().toLowerCase());
			sb.append(CalendarSetting.calTranslate("status") +  status + "\n");
		}
		if (calComponent.getUrl() != null) {
			sb.append(PeaProperties.getVmBridge().translate("url") + ": " + calComponent.getUrl() + "\n");
		}		
		if (calComponent.getRelatedTO() != null) {
			// TODO
			//PCalendar relCal = calComponent.getCalendar();
		}		
		DateTimeFormatter fullDateFormatter = CalendarSetting.getFullDateFormatter();
		if (calComponent.getDateStart() != null) {
			sb.append(CalendarSetting.calTranslate("start") +   fullDateFormatter.format(calComponent.getDateStart()) + "\n");
		}
		if (calComponent.getDuration() != null) {
			sb.append(CalendarSetting.calTranslate("time") +  calComponent.getDuration().toString() + "\n");
		}	
		
		if (calComponent instanceof PTodo) {
			PTodo todo = (PTodo) calComponent;
			if (todo.getDue() != null) {
				sb.append(CalendarSetting.calTranslate("due") + ": " +  fullDateFormatter.format(todo.getDue()) + "\n");
			}
			
		} else if (calComponent instanceof PEvent) {
			PEvent event = (PEvent) calComponent;
			if (event.getDateEnd() != null) {
				sb.append(CalendarSetting.calTranslate("end") +   fullDateFormatter.format(event.getDateEnd()) + "\n");
			}
			if (event.isAllDay() == true) {
				sb.append(CalendarSetting.calTranslate("all_day_event") + "\n");
			}
			if (event instanceof RecurringPEvent) {
				
				RecurringPEvent rEvent = (RecurringPEvent) event;
				if (rEvent.getRecurrenceDates() != null) {
					LocalDate[] recDates = rEvent.getRecurrenceDates();
					sb.append(CalendarSetting.calTranslate("single_repetitions") +  "\n");
					for (LocalDate r : recDates) {
						sb.append("   " +  fullDateFormatter.format(r) + "\n");
					}
				}
				if (rEvent.getFrequencyString() != null) {
					String freq = CalendarSetting.calTranslate(rEvent.getFrequencyString().toLowerCase());
					sb.append(CalendarSetting.calTranslate("repeat") + ": " + freq + "\n");
				}
				if (rEvent.getInterval() > 1) {
					sb.append(CalendarSetting.calTranslate("interval") + ": " + rEvent.getInterval() + "\n");
				}
				if (rEvent.getByDay() != null) { // TODO lang
					String days = "";
					for (String d : rEvent.getByDay()) {
						days = days + d + " ";
					}
					sb.append(CalendarSetting.calTranslate("days") + ": " + days + "\n");
				}
				if (rEvent.getByMonth() != null) {
					String month = "";
					for (int m : rEvent.getByMonth()) {
						month = month + m + " ";
					}
					sb.append(CalendarSetting.calTranslate("in_month") + ": " + month + "\n");					
				}
				if (rEvent.getUntil() != null) {
					sb.append(CalendarSetting.calTranslate("until") + ": " +  fullDateFormatter.format(rEvent.getUntil()) + "\n");
				}
				if (rEvent.getCount() > -1) {
					sb.append(CalendarSetting.calTranslate("occurrences") + ": " + rEvent.getCount() + "\n");
				}
				if (rEvent.getExceptionDates() != null) {
					LocalDate[] exDates = rEvent.getExceptionDates();
					sb.append(CalendarSetting.calTranslate("exceptions") + "\n");
					for (LocalDate r : exDates) {
						sb.append("   " +  fullDateFormatter.format(r) + "\n");
					}
				}				
			}			
		}
		return new String(sb);
	}

	
	/**
	 * Converts one PCard into one valid 
	 * ICalendar String representation. 
	 * This String is currently used for internal
	 * storing. Old properties (X-WR-) are not supported.  
	 * 
	 * @param calendar		PCard to write
	 * 
	 * @return				iCalendar as a String
	 */
	public static String iCalToString(PCalendar calendar) {

		StringBuilder sb = new StringBuilder();		

		PCalendar cal = calendar;
		// fixed String for all Calendars:
		sb.append(CalendarConstants.getFixedCalendarString());

		// extension from https://tools.ietf.org/html/draft-ietf-calext-extensions-01
		String name = cal.getName();
		sb.append("NAME:");
		sb.append(name + CRLF);

		if (cal.getDescription() != null) {
			String calendarDescription = ICalStringHelper.prepareString(cal.getDescription());
			sb.append("DESCRIPTION:");
			sb.append(calendarDescription + CRLF);
		}

		String calId = cal.getCalUid();
		sb.append("UID:");
		sb.append(calId + CRLF);

		Color color = cal.getColor();
		sb.append("COLOR:" + color.getRed() + ":" +  color.getGreen() + ":" + color.getBlue());
		//String hexColor = String.format("#%02X%02X%02X", color.getRed(), color.getGreen(), color.getBlue());
		//sb.append(hexColor);
		sb.append(CRLF);

		// define default local time zone in calendar
		// as defined in extension 6
		// like: TIMEZONE-ID:America/New_York
		String zoneID = ICalDateTimeHelper.getDefaultZoneId().toString();
		sb.append("TIMEZONE-ID:");
		sb.append(zoneID + CRLF);

		List<PEvent> events = cal.getEventList();
		StringBuilder eventsString = createEventsString(events);
		if (eventsString != null) {
			sb.append(eventsString);
		}
		
		List<PTodo> todos = cal.getTodoList();
		StringBuilder todoString = createTodosString(todos);
		if (todoString != null) {
			sb.append(todoString);
		}
		sb.append("END:VCALENDAR\r\n");

		return new String(sb);
	}
	
	/**
	 * Converts PCalendars into a valid 
	 * ICalendar String representation. 
	 * This String can be exported. 
	 * 
	 * @param calendar		PCard
	 * @boolean newFormat	true: use the new RFC7986
	 * 						false: use RFC5545 with X-extensions
	 * @return				iCalendar as String
	 */
	public static String singleICalToString(PCalendar calendar, boolean newFormat) {

		StringBuilder sb = new StringBuilder();

		PCalendar cal = calendar;
		// fixed String for all Calendars:
		sb.append(CalendarConstants.getFixedCalendarString());

		// extension from https://tools.ietf.org/html/draft-ietf-calext-extensions-01
		String name = cal.getName();
		if (newFormat == true) {
			sb.append("NAME:");
			sb.append(name + CRLF);
		} else {
			// add X-WR property for backward compatibility:
			sb.append("X-WR-CALNAME:");
			sb.append(name + CRLF);	
		}

		if (cal.getDescription() != null) {			
			String calendarDescription = ICalStringHelper.prepareString(cal.getDescription());// splitInLines("DESCRIPTION:".length() ,cal.getDescription());
			if (newFormat == true) {
				sb.append("DESCRIPTION:");
				sb.append(calendarDescription + CRLF);
			} else {
				// add X-WR property
				sb.append("X-WR-CALDESC:");
				sb.append(calendarDescription + CRLF);	
			}
		}

		String calId = cal.getCalUid();
		if (newFormat == true) {
			sb.append("UID:");
			sb.append(calId + CRLF);
		} else {
			// add X-WR property:
			sb.append("X-WR-RELCALID:");
			sb.append(calId + CRLF);		
		}

		Color color = cal.getColor();
		if (newFormat == true) {
			// COLOR:255:0:255
			sb.append("COLOR:");
			//String hexColor = String.format("#%02X%02X%02X", color.getRed(), color.getGreen(), color.getBlue());
			//sb.append(hexColor);
			sb.append(color.getRed() + ":");
			sb.append(color.getGreen() + ":");
			sb.append(color.getBlue());
			sb.append(CRLF);
		} else {
			sb.append("X-APPLE-CALENDAR-COLOR:");
			String hexColor = String.format("#%02X%02X%02X", color.getRed(), color.getGreen(), color.getBlue()); 
			sb.append(hexColor);
			sb.append(CRLF);
		}

		// define default local time zone in calendar
		// as defined in extension 6
		// like: TIMEZONE-ID:America/New_York
		String zoneID = ICalDateTimeHelper.getDefaultZoneId().toString();
		if (newFormat == true) {
			sb.append("TIMEZONE-ID:");
			sb.append(zoneID + CRLF);
		} else {
			// add X-WR property:
			sb.append("X-WR-TIMEZONE:");
			sb.append(zoneID + CRLF);		
		}

		List<PEvent> events = cal.getEventList();//(PeaData.getDate0(), PeaData.getDate3000());
		StringBuilder eventsString = createEventsString(events);
		if (eventsString != null) {
			sb.append(eventsString);
		}
		
		List<PTodo> todos = cal.getTodoList();//(PeaData.getDate0(), PeaData.getDate3000());
		StringBuilder todoString = createTodosString(todos);
		if (todoString != null) {
			sb.append(todoString);
		}		
		sb.append("END:VCALENDAR\r\n");
		return new String(sb);
	}
	
	/**
	 * Creates a StringBuilder from several PEvents
	 * 
	 * @param events	the PEvents to write as String
	 * 
	 * @return	a StringBuilder containing the PEvents
	 */
	private static StringBuilder createEventsString( List<PEvent> events){

		if (events == null) {
			return null;
		}
		int eventNumber = events.size();
		if (eventNumber == 0) {
			return null;
		}
		//TestLog.o(XXXX.class, "event number: " + events.size());
		StringBuilder eventBuilder = new StringBuilder();
		Iterator<PEvent> iter = events.iterator();
		while (iter.hasNext()) {
			eventBuilder.append("BEGIN:VEVENT\r\n");
			PEvent e = iter.next();
			//TestLog.o(XXXX.class, "\nnext event");

			//DTSTAMP 
			// UTC time stamp: converted in UTC and append "Z"
			if( e.getTimeStamp() == null) {
				e.setTimeStamp(Instant.now());
			}
			String timeStamp = CalendarSetting.getIcalDateTimeFormatter().format(
				//	LocalDateTime.ofInstant(e.getTimeStamp(), ZoneOffset.UTC)) + "Z";
					e.getTimeStamp().atZone(ZoneId.of("UTC"))) + "Z";
			eventBuilder.append("DTSTAMP:" + timeStamp + CRLF);
			
			// set time stamp also as LAST-MODIFIED:
			eventBuilder.append("LAST-MODIFIED:" + timeStamp + CRLF);

			// UID of event: 
			if (e.getUid() == null) {			 
				String randomString = Integer.toHexString(PeaProperties.getRandom().nextInt());
				eventBuilder.append("UID:" + System.nanoTime() + "-" + randomString +  "@eck.cologne" + CRLF);
			} else {
				eventBuilder.append("UID:" + e.getUid() + CRLF);
			}
			//long gmtMilliSeconds = dateTime.getTime();
			//String stamp = new Date().format(DateTimeFormatter.ofPattern("yyyy-MM-dd hh:mm:ss"));


			if (e.isAllDay()) {

				// RFC 5545:
				// For cases where a "VEVENT" calendar component
			    //  specifies a "DTSTART" property with a DATE value type but no
			    //  "DTEND" nor "DURATION" property, the event's duration is taken to
			    //  be one day.  
				LocalDateTime start = e.getDateStart();
				String formattedStart = CalendarSetting.getIcalDateFormatter().format(start);
				eventBuilder.append("DTSTART;VALUE=DATE:" + formattedStart + CRLF);

				if (e.isSeveralDays()) {

					LocalDateTime end = e.getDateEnd();
					// use date format for end:
					String formattedEnd = CalendarSetting.getIcalDateFormatter().format(end);
					eventBuilder.append("DTEND;VALUE=DATE:" + formattedEnd + CRLF);
				}
			} else {
				// Not all-day event
				LocalDateTime start = e.getDateStart();
				// ISO 8601 local time
				String formattedStart = CalendarSetting.getIcalDateTimeFormatter().format(start);
				eventBuilder.append("DTSTART:" + formattedStart + CRLF);
				LocalDateTime end = e.getDateEnd();
				if (end != null) {
					// ISO 8601 
					String formattedEnd = CalendarSetting.getIcalDateTimeFormatter().format(end);
					eventBuilder.append("DTEND:" + formattedEnd + CRLF);
				} else {
					TestLog.e(WriteICal.class, "no date end");
				}
			}
			if (e.isRecurring()) {
				if (e instanceof RecurringPEvent) {
					RecurringPEvent re = (RecurringPEvent) e;
					if (re.getFrequency() == null && re.getRecurrenceDates() == null) {
						new UnexpectedValueException("frequency", "String", " to write is null " ).printDescription();
						continue;
					} else {
						if (re.getFrequency() != null) {
							eventBuilder.append("RRULE:FREQ=" +  ICalRecurrenceHelper.getFrequencyAsICalString(re.getFrequency()));

							//+ ";COUNT=" + re.getRepetitionNumber() + CRLF);
							if (re.getInterval() > 1) {	
								eventBuilder.append(";INTERVAL=" + re.getInterval());
							}
							if (re.getByMonth() != null) {
								eventBuilder.append(";BYMONTH=");
								int[] byMonth = re.getByMonth();
								int byMonthLength = byMonth.length;
								for (int byMonthIndex = 0; byMonthIndex < byMonthLength; byMonthIndex++) {
									eventBuilder.append(byMonth[byMonthIndex]);
									if (byMonthIndex != byMonthLength -1) {// last item
										eventBuilder.append(",");
									}
								}
							}
							if (re.getByDay() != null) {
								eventBuilder.append(";BYDAY=");
								String[] byDay = re.getByDay();
								int byDayLength = byDay.length;
								for (int byDayIndex = 0; byDayIndex < byDayLength; byDayIndex++) {
									eventBuilder.append(byDay[byDayIndex]);
									if (byDayIndex != byDayLength -1) {// last item
										eventBuilder.append(",");
									}
								}
							}
							if (re.getUntil() != null) {
								eventBuilder.append(";UNTIL=" + CalendarSetting.getIcalDateTimeFormatter().format(re.getUntil()));
							} else if (re.getCount() > 0) {
								eventBuilder.append(";COUNT=" + (re.getCount() ));//  (inclusive start date)
							}
							// end of recurrence rule:
							eventBuilder.append(CRLF);
						} else {
							// only RDATE, no RRULE
						}
					}
					// RDATE extra
					if (re.getRecurrenceDates() != null) {
						eventBuilder.append("RDATE;VALUE=DATE:");
						LocalDate[] rDates = re.getRecurrenceDates();
						int rDateLength = rDates.length;
						for (int rDateIndex = 0; rDateIndex < rDateLength; rDateIndex++) {
							eventBuilder.append(CalendarSetting.getIcalDateFormatter().format(rDates[rDateIndex]));
							if (rDateIndex < rDateLength - 1) {
								eventBuilder.append(",");
							}
						}
						eventBuilder.append(CRLF);
					}
					// EXDATE extra
					if (re.getExceptionDates() != null) {
						eventBuilder.append("EXDATE;VALUE=DATE:");
						LocalDate[] exDates = re.getExceptionDates();
						int exDateLength = exDates.length;
						for (int exDateIndex = 0; exDateIndex < exDateLength; exDateIndex++) {
							eventBuilder.append(CalendarSetting.getIcalDateFormatter().format(exDates[exDateIndex]));
							if (exDateIndex < exDateLength - 1) {
								eventBuilder.append(",");
							}
						}
						eventBuilder.append(CRLF);
					}		
					
				} else { // e is not instance of RecurringPEvent
					TestLog.e(WriteICal.class, "Couldn't write recurrence rule: wrong instance");
				}
			}
			
			String summary = e.getSummary();
			if (summary != null) {
				eventBuilder.append("SUMMARY:" + ICalStringHelper.prepareString(summary) + CRLF);
			}			
			String description = e.getDescription();
			if ( description != null && description.length() > 0) {
				//eventBuilder.append("DESCRIPTION:" + splitInLines("DESCRIPTION:".length(), description) + CRLF);
				eventBuilder.append("DESCRIPTION:" + ICalStringHelper.prepareString(description) + CRLF);
			}
			String location = e.getLocation();
			if (location != null) {
				eventBuilder.append("LOCATION:" + ICalStringHelper.prepareString(location) + CRLF);
			}
			
			int priority = e.getPriority();
			if (priority > 0 && priority < 10) { // ignore invalid values
				eventBuilder.append("PRIORITY:" + priority + CRLF);
			}
			String status = e.getStatus();
			if (status != null 
					&& (status.equals("TENTATIVE") || status.equals("CONFIRMED") || status.equals("CANCELLED"))) {
				eventBuilder.append("STATUS:" + status + CRLF);
			}			
			if (e.getUrl() != null) {
				String url = e.getUrl().toString();
				eventBuilder.append("URL:" + ICalStringHelper.prepareString(url) + CRLF);			
			}			
			if (e.getAlarms() != null) { // TODO
				String alarmString = createAlarmString(e);
				eventBuilder.append(alarmString);
			}
			eventBuilder.append("END:VEVENT\r\n");
		}		
		return eventBuilder;
	}
	

	/**
	 * Creates a StringBuilder from several PTodos
	 * 
	 * @param events	the PTodos to write as String
	 * 
	 * @return	a StringBuilder containing the PTodos
	 */
	private static StringBuilder createTodosString( List<PTodo> events){

		if (events == null) {
			return null;
		}
		int eventNumber = events.size();
		if (eventNumber == 0) {
			return null;
		}
		//TestLog.o(XXXX.class, "event number: " + events.size());
		StringBuilder todoBuilder = new StringBuilder();
		Iterator<PTodo> iter = events.iterator();
		while (iter.hasNext()) {
			todoBuilder.append("BEGIN:VTODO\r\n");
			PTodo e = iter.next();
			//TestLog.o(XXXX.class, "\nnext event");

			//DTSTAMP 
			// UTC time stamp: converted in UTC and append "Z"
			if( e.getTimeStamp() == null) {
				e.setTimeStamp(Instant.now());
			}
			String timeStamp = CalendarSetting.getIcalDateTimeFormatter().format(
				//	LocalDateTime.ofInstant(e.getTimeStamp(), ZoneOffset.UTC)) + "Z";
					e.getTimeStamp().atZone(ZoneId.of("UTC"))) + "Z";
			todoBuilder.append("DTSTAMP:" + timeStamp + CRLF);
			
			// set time stamp also as LAST-MODIFIED:
			todoBuilder.append("LAST-MODIFIED:" + timeStamp + CRLF);

			// UID of event: 
			if (e.getUid() == null) {			 
				String randomString = Integer.toHexString(PeaProperties.getRandom().nextInt());
				todoBuilder.append("UID:" + System.nanoTime() + "-" + randomString +  "@eck.cologne" + CRLF);
			} else {
				todoBuilder.append("UID:" + e.getUid() + CRLF);
			}

			// Not all-day event
			LocalDateTime start = e.getDateStart();
			// ISO 8601 local time
			if (start != null) {
				String formattedStart = CalendarSetting.getIcalDateTimeFormatter().format(start);
				todoBuilder.append("DTSTART:" + formattedStart + CRLF);
			}
			LocalDateTime due = e.getDue();
			if (due != null) {
				// ISO 8601 
				String formattedEnd = CalendarSetting.getIcalDateTimeFormatter().format(due);
				todoBuilder.append("DUE:" + formattedEnd + CRLF);
			} 
			String summary = e.getSummary();
			if (summary != null) {
				todoBuilder.append("SUMMARY:" + ICalStringHelper.prepareString(summary) + CRLF);
			}			
			String description = e.getDescription();
			if ( description != null && description.length() > 0) {
				//eventBuilder.append("DESCRIPTION:" + splitInLines("DESCRIPTION:".length(), description) + CRLF);
				todoBuilder.append("DESCRIPTION:" + ICalStringHelper.prepareString(description) + CRLF);
			}
			String location = e.getLocation();
			if (location != null) {
				todoBuilder.append("LOCATION:" + ICalStringHelper.prepareString(location) + CRLF);
			}
			
			int priority = e.getPriority();
			if (priority > 0 && priority < 10) { // ignore invalid values
				todoBuilder.append("PRIORITY:" + priority + CRLF);
			}
			String status = e.getStatus();
			if (status != null) {
				if (status.equals("NEEDS-ACTION") || status.equals("COMPLETED") || status.equals("IN-PROCESS") || status.equals("CANCELLED")) {
					todoBuilder.append("STATUS:" + status + CRLF);
				} else {
					TestLog.ve(WriteICal.class, "Invalid status: " + e.getStatus());
				}
			}
			if (e.getUrl() != null) {
				String url = e.getUrl().toString();
				todoBuilder.append("URL:" + ICalStringHelper.prepareString(url) + CRLF);			
			}
			if (e.getAlarms() != null) {
				String alarmString = createAlarmString(e); // TODO
				todoBuilder.append(alarmString);
			}
			
			todoBuilder.append("END:VTODO\r\n");
		}		
		return todoBuilder;
	}
	/**
	 * Create an alarm string as property of event or todo
	 * 
	 * @param comp	the PEvent or PTodo
	 * 
	 * @return		the String that represents the alarm
	 */
	private static String createAlarmString(CalendarComponent comp) { // TODO Test
		ArrayList<PAlarm> alarmList = comp.getAlarms();
		if (alarmList != null && alarmList.size() > 0) {
			StringBuilder alarmBuilder = new StringBuilder();
			for (PAlarm a : alarmList) {
				//TestLog.e(WriteICal.class, "Not yet implemented: VALARM");

				alarmBuilder.append("BEGIN:VALARM" + CRLF);
				alarmBuilder.append("TRIGGER:" + a.getTrigger() + CRLF);
				alarmBuilder.append("ACTION:" + a.getAction() + CRLF);
				alarmBuilder.append("DESCRIPTION:" + a.getDescription() + CRLF);
				// TODO other alarm properties: REPEAT, DURATION, ATTACH, SUMMARY, ATTENDEE
				alarmBuilder.append("END:VALARM" + CRLF);
			}
			return new String(alarmBuilder);
		} else {
			return null;
		}		
	}
}