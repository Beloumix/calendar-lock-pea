package cologne.eck.calendar_pea.util;

/*
 * PeaFactory - Production of Password Encryption Archives
 * Copyright (C) 2015  Axel von dem Bruch
 * 
 * This library is free software; you can redistribute it and/or modify it 
 * under the terms of the GNU General Public License as published 
 * by the Free Software Foundation; either version 2 of the License, 
 * or (at your option) any later version.
 * This library is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
 * or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU General Public License for more details.
 * See:  http://www.gnu.org/licenses/gpl-2.0.html
 * You should have received a copy of the GNU General Public License 
 * along with this library.
 */

import java.awt.Color;
import java.net.MalformedURLException;
import java.net.URL;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.temporal.ChronoUnit;
import java.time.temporal.Temporal;
import java.util.ArrayList;
import java.util.HashSet;


// TODO remove GUI: set CloudControl in PeaProperties
import cologne.eck.all_peas.files.FileTypePanel;

import cologne.eck.all_peas.cloud.CloudControl;
import cologne.eck.all_peas.cloud.CloudPropertyHandler;
import cologne.eck.all_peas.data.PeaProperties;
import cologne.eck.all_peas.vm_specific.JREProperties;
import cologne.eck.calendar_pea.control.CalendarConstants;
import cologne.eck.calendar_pea.control.CalendarControl;
import cologne.eck.calendar_pea.data.CalendarComponent;
import cologne.eck.calendar_pea.data.PAlarm;
import cologne.eck.calendar_pea.data.PCalendar;
import cologne.eck.calendar_pea.data.PEvent;
import cologne.eck.calendar_pea.data.PTodo;
import cologne.eck.calendar_pea.data.RecurringPEvent;
import cologne.eck.peafactory.crypto.kdf.KeyDerivation;
import cologne.eck.tools.Converter;
import cologne.eck.tools.TestLog;
import cologne.eck.tools.UnexpectedValueException;



/** 
 * @see:
 * https://tools.ietf.org/html/rfc5545
 * https://tools.ietf.org/html/draft-daboo-icalendar-extensions-06
 * and: https://tools.ietf.org/html/rfc7986
 * 
 * @author Axel von dem Bruch
 *
 */


public class ReadICal {
	/*
	 * The index of the lines of the ics file.
	 * Calendars and Events are created line by line.
	 */
	private int index = 0;
	
	/*
	 * If the dtstart's time is T000000, this is maybe an
	 * all-day event if dtend's is also T000000. This variable
	 * holds the value until dtend is reached
	 */
	private boolean maybeAllDayEvent = false;

	/*
	 * true if there is invalid format
	 */
	private static boolean invalidFormatFound = false;
	/*
	 * true if no calendar was created
	 */
	private static boolean noCalendarAdded = false;
	/*
	 * number of successfully read events
	 */
	private static int numberOfCalendarComponents = 0;
	
	private static HashSet<String> unsupportedSet = new HashSet<String>();


	/**
	 * Read PCalendars from byte array. The array is converted 
	 * to String, split in lines and fed in getPeaCalendars. 
	 * For success: file names are associated with the calendars,
	 * mapping file-calendar is set and usedCloudFiles if required
	 * 
	 * @param content	the plain text content as byte array
	 * @param fileName	the file name, this calendars are read from
	 * @param useFileName 	true if the file name should be used to store the calendar
	 * 						false otherwise (import)
	 * 
	 * @return	the PCalendars, created from the content
	 */
	public PCalendar[] readCalendarsFromBytes(byte[] content, String fileName, boolean useFileName) {
		// reset:
		setNumberOfEvents(0);
		maybeAllDayEvent = false;
		setInvalidFormatFound(false);
		setNoCalendarAdded(false);
		
		// reset the unsupported Components
		unsupportedSet = new HashSet<String>();
		
		if (content == null) {
			TestLog.ve(ReadICal.class, "Missing content to read calendars from: " + fileName);
			noCalendarAdded = true;
			return null;
		}		
		String allContent = new String(content, CalendarConstants.getCharset());
		//TestLog.o(ReadCard.class, "allContent: " + allContent);		
		// split in lines by CRLF:	
		String[] lines = allContent.split("\r\n");		
		// dirty hack for non conform calendars: 
		if (content.length > 100 && lines.length <= 1) {
			// maybe wrong new line: try \n
			TestLog.ve(ReadICal.class, "Invalid line break in calendar. Warning calendar is maybe displayed incorrect");
			lines = allContent.split("\n");
			setInvalidFormatFound(true);
		}
		// reset index
		index = 0;	
		// create calendars from the file
		PCalendar[] cals = getPeaCalendars(lines, fileName, useFileName);//, true);
		if (cals == null || cals.length == 0) {
			TestLog.ve(ReadICal.class, "Content doesn't contain a calendar: " + fileName);
			noCalendarAdded = true;
			return null;
		} else {
			if (useFileName == true) {
				// set file name as associated file name for every PCard insider the file:
				for (PCalendar c : cals) {
					c.addAssociatedFileName(fileName);
				}
				// add to file-calendar map:
				CalendarControl.addToFileCalendarsMap(fileName, cals);

				// If cloud file: add to model and properties
				if (fileName != null && fileName.contains(CloudControl.getTmpDir())) {
					String providerName = CloudControl.getProviderNameFromTmpFile(fileName);
					String fileNameNoPath = CloudControl.getFileNameFromTmpFile(fileName);
					CloudControl cc = ((FileTypePanel) JREProperties.getTypePanel()).getCloudFilePanel().getCloudControl();
					boolean added = cc.addUsedFileNameToProvider(providerName, fileNameNoPath);
					if (added == true) {
						String[] usedFiles = cc.getUsedFileNamesOfProvider(providerName);
						String error = CloudPropertyHandler.addFileNamesToProperties(providerName, usedFiles,
								Converter.bytes2hex(KeyDerivation.getSalt()), Converter.bytes2hex(PeaProperties.getAlgoParamString()));
						if (error != null) {
							TestLog.e(ReadICal.class, error);
						}
					}
				}
			}
			return cals;// there is only one Calendar per file
		}
	}	
	
	/**
	 * Get PCalendars from an array of lines.
	 * Creates a calendar for every found BEGIN:VCALENDAR String.
	 * 
	 * @param lines			the lines to create a Calendar object
	 * 
	 * @return	the valid Calendars created or
	 * 			or null if no Calendar was created
	 */
	private PCalendar[] getPeaCalendars(String[] lines, String fileName, boolean useFileName)  {

		ArrayList <PCalendar> calendarList = new ArrayList<PCalendar>();
		int len = lines.length;
		for (index =  0; index < len; index++) {

			String line = lines[index];

			if (line.startsWith("BEGIN:VCALENDAR")){
				PCalendar cal = createPCalendar(lines, fileName, useFileName);
				if (cal != null) {
					calendarList.add(cal);
				} 
			}
		}
		if (calendarList.isEmpty()) {
			TestLog.ve(ReadICal.class, "No Calendar created: " + useFileName);
			noCalendarAdded = true;
			return null;
		} else {
			PCalendar[] calendars = new PCalendar[calendarList.size()];
			calendars = calendarList.toArray(calendars);	
			return calendars;
		}
	}

	/**
	 * Create one PCard from lines. 
	 * Read line by line until END:VCALENDAR is reached. 
	 * 
	 * @param lines		the String lines
	 * @param fileName	the file name to associate
	 * @param useFileName	true: associate calendar with file name
	 * 						false: Import - do not associate
	 * 
	 * @return			the PCard 
	 * 					created from the lines
	 */
	private PCalendar createPCalendar(String[] lines, String fileName, boolean useFileName) {

		PCalendar cal = null;
		if (lines[index++].toUpperCase().startsWith("BEGIN:VCALENDAR")) {
			//TestLog.ve(ReadCard.class, "New calendar..." + index);
			// create Calendar with no name, no description, no color
			HashSet<String> set = new HashSet<String>();
			if (useFileName == true && fileName != null) {
				set.add(fileName);
			}
			cal = new PCalendar(null, null, null, set);//null, null, null);
		} else {
			TestLog.ve(ReadICal.class, "Invalid ics file format: file does not start with BEGIN:VCALENDAR");
			setInvalidFormatFound(true);
			return null;
		}
		String line = "";
		// read line by line until END:VCALENDAR
		for ( ; index < lines.length; index++) {
			
			line = lines[index];
			//TestLog.ve(ReadCard.class,  index + ": "+line);

			// ignore these components:===================================
			if (line.startsWith("BEGIN:VJOURNAL")){
				//setUnsupportedPropertiesFound(true);
				unsupportedSet.add("VJOURNAL");
				if (PeaProperties.isTestMode()) {
					TestLog.e(ReadICal.class, "Journal is currently not supported");
				}
				while (! line.startsWith("END:VJOURNAL")) {
					line = lines[index++];
				}
				index--;
			} else if (line.startsWith("BEGIN:VFREEBUSY")){
				unsupportedSet.add("VFREEBUSY");
				//setUnsupportedPropertiesFound(true);
				if (PeaProperties.isTestMode()) {
					TestLog.e(ReadICal.class, "Free Busy is currently not supported");
				}
				while (! line.startsWith("END:VFREEBUSY")) {
					line = lines[index++];
				}
				index--;
			} else if (line.startsWith("BEGIN:VALARM")){ // only allowed inside an event or todo
				unsupportedSet.add("VALARM");
				//setUnsupportedPropertiesFound(true);
				if (PeaProperties.isTestMode()) {
					TestLog.e(ReadICal.class, "Alarm is currently not supported");	
				}
				while (! line.startsWith("END:VALARM")) {
					line = lines[index++];
				}
				index--;				
				
			// consider these elements: =============================================
				
			// There are three different formats for time zone: 
				// 1. inside a VTIMEZONE as TZID
				// 2. as TIMEZONE-ID as described in extension#6
				// 3. as X-WR-TIMEZONE used e.g. by Google
			} else if (line.startsWith("BEGIN:VTIMEZONE")){ // time zone of calendar
				// Multiple "VTIMEZONE" calendar components can
			    //  exist in an iCalendar object.
				// ignore all elements
				while (! line.startsWith("END:VTIMEZONE")) {//END:VTIMEZONE
					line = lines[index++];
				}
				index--;
			} else if (line.startsWith("TIMEZONE-ID:")){ // defined in extension #6
				// set time zone for calendar
				String zoneIDString = line.substring(line.indexOf(":") + 1, line.length());
				ZoneId newZone = ICalDateTimeHelper.getZoneIdFromString(zoneIDString, cal, null);
				if (newZone == null) {
					TestLog.ve(ReadICal.class, "Set default time zone, but this might be wrong...");
					//setUnsupportedPropertiesFound(true);
					unsupportedSet.add(line);
				} else if (! newZone.equals(ICalDateTimeHelper.getDefaultZoneId())) {						
					// set this time zone for the calendar or null if an error occurred
					ICalDateTimeHelper.setCurrentCalendarZone(newZone);
				} 
				
			// for example Google uses X-WR-TIMEZONE
			} else if (line.startsWith("X-WR-TIMEZONE:")){ 
				// set time zone for calendar
				String zoneIDString = line.substring(line.indexOf(":") + 1, line.length());
				ZoneId newZone = ICalDateTimeHelper.getZoneIdFromString(zoneIDString, cal, null);
				if (newZone == null) {
					TestLog.ve(ReadICal.class, "Set default time zone, but this might be wrong...");
					//setUnsupportedPropertiesFound(true);
					unsupportedSet.add(line);
				} else if (! newZone.equals(ICalDateTimeHelper.getDefaultZoneId())) {						
					// set this time zone for the calendar or null if an error occurred
					ICalDateTimeHelper.setCurrentCalendarZone(newZone);
				} 	

			} else if (line.startsWith("UID:")){

				String calID = line.substring(line.indexOf(":") + 1, line.length());
				cal.setCalUid(calID);
				
			} else if (line.startsWith("X-WR-RELCALID:")){

				String calID = line.substring(line.indexOf(":") + 1, line.length());
				cal.setCalUid(calID);


			} else if (line.startsWith("CALSCALE:")){
				if (! line.contains("GREGORIAN")) {
					TestLog.e(ReadICal.class, "Calendar Lock PEA supports"
							+ " currently only Gregorian Calendar");
					//setUnsupportedPropertiesFound(true);
					unsupportedSet.add(line);
				}
			} else if (line.startsWith("PRODID:")){
				// ignore: calendar is added to default file
			} else if (line.startsWith("METHOD:")){

				if (! line.contains("PUBLISH")  && ! line.contains("REQUEST")) {
					// no interaction with other users, only publish
					if (PeaProperties.isTestMode()) {
						TestLog.e(ReadICal.class, "Line " + index + ": Calendar Lock PEA supports"
							+ " currently only PUBLISH or REQUEST method");
					}
					//setUnsupportedPropertiesFound(true);
					unsupportedSet.add(line);
				}
			} else if (line.startsWith("VERSION:")){
				// check if version is o.k. 
				String version = line.substring(line.indexOf(":") + 1, line.length());
				if (! version.equals(PCalendar.getVersion())){
					// message
					TestLog.e(ReadICal.class, "Line " + index + ": Warning: Wrong version: " + version+ ", must be" + PCalendar.getVersion());
					setInvalidFormatFound(true);
				}							
				
			} else if (line.startsWith("COLOR:")){
				// TODO color names: 
				// only basic color names: aqua, black, blue, fuchsia, gray, green, lime, maroon, navy, olive, purple, red, silver, teal, white, and yellow. 
				// see: https://tools.ietf.org/html/rfc7986#section-5.9
				// all PEA events are in RGB format, no color names are allowed
				//integer ":" integer ":" integer
				String color = line.substring(6, line.length()).trim();
				try {
					String[] colors = color.split(":");
					Color calColor = new Color( Integer.parseInt(colors[0]), 
							Integer.parseInt(colors[1]), 
							Integer.parseInt(colors[2] )); 
					cal.setColor(calColor);
					//TestLog.o(ReadCard.class, "Color:" + calColor.toString());
				} catch (NumberFormatException nfe) {
					TestLog.e(ReadICal.class, "Line " + index + ": Can't read color value of calendar");
					//setUnsupportedPropertiesFound(true);
					unsupportedSet.add(line);
				}
			} else if (line.startsWith("X-APPLE-CALENDAR-COLOR:")){
				// example: X-APPLE-CALENDAR-COLOR:#711A76 
				String colorStr = line.substring(line.indexOf(":") + 1, line.length());
				Color color = null;
				try {
					color = new Color(
				            Integer.valueOf( colorStr.substring( 1, 3 ), 16 ),
				            Integer.valueOf( colorStr.substring( 3, 5 ), 16 ),
				            Integer.valueOf( colorStr.substring( 5, 7 ), 16 ) );
				} catch (Exception e) {
					TestLog.ve(ReadICal.class, "Line " + index + ": couldn't read color (X-APPLE-CALENDAR-COLOR)");
				}
				if (color != null) {
					cal.setColor(color);
				}
			} else if (line.startsWith("X-OUTLOOK-COLOR:")){
				// example: X-OUTLOOK-COLOR:#054a9c
				String colorStr = line.substring(line.indexOf(":") + 1, line.length());
				Color color = null;
				try {
					color = new Color(
				            Integer.valueOf( colorStr.substring( 1, 3 ), 16 ),
				            Integer.valueOf( colorStr.substring( 3, 5 ), 16 ),
				            Integer.valueOf( colorStr.substring( 5, 7 ), 16 ) );
				} catch (Exception e) {
					TestLog.ve(ReadICal.class, "Line " + index + ": couldn't read color (X-OUTLOOK-COLOR)");
				}
				if (color != null) {
					cal.setColor(color);
				}

			} else if (line.startsWith("NAME:")){
				String name = line.substring(line.indexOf(":") + 1, line.length());
				cal.setName(name);				
				
			// X-WR-CALNAME:
			} else if (line.startsWith("X-WR-CALNAME:")){
				String name = line.substring(line.indexOf(":") + 1, line.length());
				cal.setName(name);	

			} else if (line.startsWith("DESCRIPTION")){			
				
				cal.setDescription(ICalStringHelper.readString(lines, index));		
				
			} else if (line.startsWith("X-WR-CALDESC")){
				//X-WR-CALDESC;VALUE=TEXT:
				String description = line.substring(line.indexOf(":") + 1, line.length());
				cal.setDescription(description);	
				
			} else if (line.startsWith("BEGIN:VEVENT")) {
				PEvent e = createPeaEvent(lines, cal);//importMode, cal);//, index++);
				if (e != null) {
					cal.addEvent( e);
				} else {
					if (PeaProperties.isTestMode()) {
						TestLog.ve(ReadICal.class, "Line " + index + ": Invalid Event was not added");	
					}
					setInvalidFormatFound(true);
				}
			} else if (line.startsWith("BEGIN:VTODO")){
				PTodo t = createPTodo(lines, cal);//importMode, cal);//, index++);
				if (t != null) {
					cal.addTodo(t);
				} else {
					if (PeaProperties.isTestMode()) {
						TestLog.ve(ReadICal.class, "Line " + index + ": Invalid Todo was not added");	
					}
					setInvalidFormatFound(true);
				}
			} else if (line.startsWith("END:VCALENDAR")) {
				
				// end of calendar: reset calendarZone
				ICalDateTimeHelper.setCurrentCalendarZone(null);
				return cal;
			} else {
				if (PeaProperties.isTestMode()) {
					TestLog.ve(ReadICal.class, "Line " + index + ": Unsupported calendar property:   "+ line);
				}
				//setUnsupportedPropertiesFound(true);
				unsupportedSet.add(line);
			}
		} // for loop
		TestLog.ve(ReadICal.class, "Line " + index + ": No Calendar created...");
		return null;
	}

	/**
	 * Create an Event from lines.
	 * Read line by line until END:VEVENT is reached
	 * 
	 * @param lines		the line (Strings)
	 * @param cal		the PCard, the event belongs to
	 * 
	 * @return			the PEvent or null if an error occurred
	 */
	private PEvent createPeaEvent(String[] lines, PCalendar cal) {

		PEvent e = new PEvent(cal);
		
		// reset value
		maybeAllDayEvent = false;

		// check for valid Event: 
		String line = lines[index++];

		int len = lines.length;
		for ( ; index < len; index++) {
			line = lines[index];
			//int lineLen = line.length();

			// Location
			/*} else if (line.startsWith("LOCATION:")){
				String location = calendarLine.substring(9, calendarLine.length());
			} */
				// Comment
				/*				if (line.startsWith("COMMENT:")){
				String comment = calendarLine.substring(8, calendarLine.length());
			}
			// Recurrence Rule
			if (line.startsWith("RRULE:")){
			}  */
				//VALARM: content is encrypted -> no alarm
				// properties RFC 5545 for VEVENT
/*			} else if (line.startsWith("ATTACH")) {					
			} else if (line.startsWith("CATEGORIES")) {				
			} else if (line.startsWith("CLASS")) {						
			} else if (line.startsWith("COMMENT")) {				
			} else if (line.startsWith("GEO")) {									
			} else if (line.startsWith("PERCENT-COMPLETE")) {					
			} else if (line.startsWith("RESOURCES")) {									
			} else if (line.startsWith("TRANSP")) {							
			} else if (line.startsWith("TZNAME")) {
			} else if (line.startsWith("TZOFFSETFROM")) {				
			} else if (line.startsWith("TZOFFSETTO")) {				
			} else if (line.startsWith("TZURL")) {
			} else if (line.startsWith("ATTENDEE")) {				
			} else if (line.startsWith("CONTACT")) {				
			} else if (line.startsWith("ORGANIZER")) {				
			} else if (line.startsWith("RECURRENCE-ID")) {
			} else if (line.startsWith("RELATED-TO")) {								
			} else if (line.startsWith("EXDATE")) {				
			} else if (line.startsWith("RDATE")) {				
			} else if (line.startsWith("RRULE")) {				
			} else if (line.startsWith("CREATED")) {								
			} else if (line.startsWith("LAST-MODIFIED")) {				
			} else if (line.startsWith("SEQUENCE")) {				
			} else if (line.startsWith("X-")) { // Non-Standard Properties				
			} else if (line.startsWith("REQUEST-STATUS")) {
*/
			if (line.startsWith("UID:")){ // String!
				String uid = line.substring(line.indexOf(":") + 1, line.length());
				e.setUid(uid);
	
			} else if (line.startsWith("DTSTART")){
				
				// reset maybeAlldayEvent from previous event:
				maybeAllDayEvent = false;
				String dtstart = line.substring(8, line.length());
				
				// check if time is 000000: this is maybe an all-day event
				// if dtend is also T000000
				if (line.endsWith("T000000")) {
					maybeAllDayEvent = true;
				}

				// get the Temporal; this might be a LocalDate or LocalDateTime	
				Temporal temp = ICalDateTimeHelper.getDateTime(dtstart);
				if (temp == null) {
					new UnexpectedValueException("Temporal", "temp", "is null in cal: " + cal.getName()).printDescription();
					TestLog.ve(ReadICal.class, "No event created...");
					return null;
				}
				if (temp instanceof LocalDate) {
					// mark as all-day event
					e.setAllDay(true);
					// set at start of day:
					e.setDateStart( ((LocalDate) temp).atStartOfDay());
				} else {
					// this is a LocalDateTime
					e.setDateStart( (LocalDateTime) temp);
				}
				
			} else if (line.startsWith("DTEND")){ // must be the same format like start

				String dtend = line.substring(6, line.length());
				// get the Temporal; this might be a LocalDate or LocalDateTime	
				Temporal temp = ICalDateTimeHelper.getDateTime(dtend);
				if (temp == null) {
					new UnexpectedValueException("Temporal", "temp", "is null in cal: " + cal.getName()).printDescription();
					TestLog.ve(ReadICal.class, "No event created...");
					return null;
				}
				LocalDateTime endDateTime = null;
				if (temp instanceof LocalDate) {
					// mark as all-day event
					e.setAllDay(true);
					// convert to LocalDateTime:
					endDateTime = ((LocalDate) temp).atStartOfDay();
					// check if several days affected:
					// set if this event extends to more than one day or not
					// here: the date after NEXT day!!!
					if (e.getDateStart() != null) { // DTSTART is sometimes set after DTEND
						if (endDateTime.toLocalDate().isAfter(e.getDateStart().plusDays(1).toLocalDate())) {
							e.setSeveralDays(true);
							e.setDateEnd( endDateTime);
						} else {
							e.setSeveralDays(false);	
							// do not set date end: this is an one-day event
							e.setDateEnd( null);
						}
					} else {
						TestLog.ve(ReadICal.class, "Missing DTSTART, cant proof if event is several days");
						e.setDateEnd( endDateTime);
					}
					
				} else {
					// this is a LocalDateTime
					endDateTime = (LocalDateTime) temp;
					// set at start of day:
					e.setDateEnd( endDateTime);
				}
				
			} else if (line.startsWith("DURATION:")){
				
				if (e.getDateStart() == null) {
					new UnexpectedValueException("String", "dateStart", "is null in cal: " + cal.getName()).printDescription();
					TestLog.ve(ReadICal.class, "No event created...");
					return null;
				}
				// TODO all-day events with time 000000
				String durationString = line.substring(line.indexOf(":") + 1, line.length());
				LocalDateTime endDateTime = ICalDateTimeHelper.
						durationToDateTime(e.getDateStart(), durationString);
				// check for all-day events:
				// check if time (hour, minute and second) exist
				// TODO if exist: check if values are 0 - but this would be unusual
				if (maybeAllDayEvent == true && ! line.contains("T") && ! line.contains("H")
						&& ! line.contains("M") && ! line.contains("S")) {
					// this is an all-day event: set time of start and end to zero
					e.setAllDay(true);
					// check if several days affected:
					// set if this event extends to more than one day or not
					// here: the date after NEXT day!!!
					if (endDateTime.toLocalDate().isAfter(e.getDateStart().plusDays(1).toLocalDate())) {
						e.setSeveralDays(true);
						// set date end:
						e.setDateStart(e.getDateStart().withHour(0).withMinute(0).withSecond(0));
						e.setDateEnd(endDateTime.withHour(0).withMinute(0).withSecond(0));
					} else {
						e.setSeveralDays(false);	
						// do not set date end: this is an one-day event
						e.setDateEnd(null);
					}
				} else {

					// set date end:
					e.setDateEnd( endDateTime);
					// set if this event extends to more than one day or not
					if (endDateTime.toLocalDate().isAfter(e.getDateStart().toLocalDate())) {
						e.setSeveralDays(true);
					} else {
						e.setSeveralDays(false);	
					}
				}
			} else if (line.startsWith("LAST-MODIFIED")){ 
				// LAST-MODIFIED;VALUE=DATE-TIME:20210522T094547Z
				// always set as time stamp if exists: overwrite existing time stamp

				Instant instant = ICalDateTimeHelper.getInstantFromUtcString(line.substring(line.indexOf(":") + 1, line.length())); //remove DTSTAMP:  
				if (instant == null) {
					if (PeaProperties.isTestMode()) {
						TestLog.ve(ReadICal.class, "Line " + index + ": Couldn't read LAST-MODIFIED value: set time stamp of now...");
					}
					instant = Instant.now();
				}
				e.setTimeStamp(instant);

			} else if (line.startsWith("DTSTAMP")){ 
				if (e.getTimeStamp() != null) {
					// time stamp was set by LAST_MODIFIED
				} else {
					Instant instant = ICalDateTimeHelper.getInstantFromUtcString(line.substring(line.indexOf(":") + 1, line.length())); //remove DTSTAMP:  
					if (instant == null) {
						if (PeaProperties.isTestMode()) {
							TestLog.ve(ReadICal.class, "Line " + index + ": Couldn't read time stamp: set time stamp of now...");
						}
						instant = Instant.now();
					}
					e.setTimeStamp(instant);
				}
				
			} else if (line.startsWith("CREATED")){ 
				// do nothing

			} else if (line.startsWith("RDATE")){ // TODO PERIOD
				
				LocalDate[] rDates = ICalDateTimeHelper.getDatesFromList(line);				
				
				e.setRecurring(true);
				RecurringPEvent re  = RecurringPEvent.castToRecurringPEvent(e);
				
				re.setRecurrenceDates(rDates);
				e = re;						

			} else if (line.startsWith("EXDATE")){ // 
				
				LocalDate[] exDates = ICalDateTimeHelper.getDatesFromList(line);
				
				e.setRecurring(true);
				RecurringPEvent re = RecurringPEvent.castToRecurringPEvent(e);
				
				re.setExceptionDates(exDates);
				e = re;

			} else if (line.startsWith("RRULE:FREQ=")){ 

				RecurringPEvent re = null;
				
				String frequencyString = getParameterValue("FREQ", line);

				ChronoUnit unit = ICalRecurrenceHelper.getChronoUnitFromString(frequencyString);
				if (unit == null) { // invalid or unsupported
					//re.setRecurring(false);
					new UnexpectedValueException("unit", "ChronoUnit", "is null").printDescription();
					//unSupportedPropertiesFound = true;
					unsupportedSet.add(line);
					TestLog.ve(ReadICal.class, "Couldn't read recurrence rule - event is treated as non recurring...");
				} else {
					
					e.setRecurring(true);
					re = RecurringPEvent.castToRecurringPEvent(e);

					re.setFrequency(unit);
					
					// interval:
					if (line.contains("INTERVAL=")) {
						re.setInterval(Integer.parseInt(getParameterValue("INTERVAL", line)));

					}
					if (line.contains("BYMONTH=")) {
						String byMonthString = getParameterValue("BYMONTH", line);
						String[] byMonthStrings = byMonthString.split(",");
						int[] byMonths = new int[byMonthStrings.length];
						int byMonthLen = byMonthStrings.length;
						for (int bmIndex = 0; bmIndex < byMonthLen; bmIndex++) {
							byMonths[bmIndex] = Integer.parseInt(byMonthStrings[bmIndex]);
						}
						re.setByMonth(byMonths);
					}
					if (line.contains("BYDAY=")) {
						String byDayString = getParameterValue("BYDAY", line);
						String[] byDayStrings = byDayString.split(",");
						re.setByDay(byDayStrings);
					}

					if (line.contains("COUNT=")) {
						
						String countString = getParameterValue("COUNT", line);
						//TestLog.o(ReadCard.class, "count: " + countString);
						re.setCount(Integer.parseInt(countString));// occurrences 
					} else if (line.contains("UNTIL=")) {
						String untilDateString = getParameterValue("UNTIL", line);

						//TestLog.o(ReadCard.class, "until: " + untilDateString);
						Temporal until = ICalDateTimeHelper.getDateTime(untilDateString);
						if (until instanceof LocalDate) {
							re.setUntil( ((LocalDate) until).atStartOfDay());
						} else if (until instanceof LocalDateTime) {
							// time zone... set to start of day
							re.setUntil( ((LocalDateTime) until).toLocalDate().atStartOfDay());
						}
					} 
				}
				if (re != null) {
					e = re;
				}
			} else if (line.startsWith("SUMMARY")){
				e.setSummary(ICalStringHelper.readString(lines, index));
				
			} else if (line.startsWith("\t") || line.startsWith(" ")){ // tab or whitespace: ignore
			} else if (line.startsWith("DESCRIPTION")){
				
				e.setDescription(ICalStringHelper.readString(lines, index));
				
			} else if (line.startsWith("LOCATION")){
				if (line.startsWith("LOCATION;ALTREP")) {
					// no alternate representation is supported like URL
					//setUnsupportedPropertiesFound(true);
					unsupportedSet.add(line);
				} else {
					e.setLocation(ICalStringHelper.readString(lines, index));			
				}
				// Status
				// Priority
			} else if (line.startsWith("PRIORITY:")){ // 0 - 9
				String priority = line.substring(9, line.length());
				int priorityValue = Integer.parseInt(priority);
				if (priorityValue > 0 && priorityValue < 10) {
					e.setPriority(priorityValue);
				}
			} else if (line.startsWith("STATUS:")){ // 0 - 9
				String status = line.substring(line.indexOf(":") + 1, line.length());
				if (status.equals("TENTATIVE") || status.equals("CONFIRMED") || status.equals("CANCELLED")) {
					e.setStatus(status);
				} else {
					setInvalidFormatFound(true);	
				}
				
			} else if (line.startsWith("URL:")){ // 
				URL url = null; 
				try {
					url = new URL( ICalStringHelper.readString(lines, index));					
				} catch (MalformedURLException e1) {
					TestLog.e(ReadICal.class, "Invalid URL: " + url);
				}
				if (url != null) {
					e.setUrl(url);
				}
			} else if (line.startsWith("BEGIN:VALARM")){ // TODO
				createPAlarm(e, lines);

			} else if (line.startsWith("END:VEVENT")){
				
				if (e.getDateStart() == null) {
					new UnexpectedValueException("String", "dateStart", "is null in cal: " + cal.getName()).printDescription();
					TestLog.ve(ReadICal.class, "No event created...");
					return null;
				}

				if (e.getDateEnd() == null 
						&& e.isAllDay() == false 
						&& e.isRecurring() == false) {

					TestLog.o(ReadICal.class, e.getSummary() + " - no date end, is set automatically to one hour later...");
					setInvalidFormatFound(true);

					//Date x = Calendar.getInstance().add(Calendar.HOUR, 1);//.setTimeInMillis( e.getStart().getTime() + 1000 ).getTimeInMillis();
					if (e.getDateStart() != null  && e != null) {
						//LocalDateTime x = new LocalDateTime( e.getDateStart().getTime() + 3600 * 1000);
						e.setDateEnd(e.getDateStart().plusHours(1));//  x);//.setHours(e.getStart().getHours() + 1));
					}
				}
				
				// check if start and end are at time T000000
				// -> this is an all-day event
				LocalDateTime endDateTime = e.getDateEnd();
				if (endDateTime != null) {
					if (maybeAllDayEvent == true 
							&& line.endsWith("T000000")) {
						e.setAllDay(true);
						// check if several days affected:
						// set if this event extends to more than one day or not
						// here: the date after NEXT day!!!
						if (endDateTime.toLocalDate().isAfter(e.getDateStart().plusDays(1).toLocalDate())) {
							e.setSeveralDays(true);
							// set start and end to time zero:
							e.setDateStart(e.getDateStart().toLocalDate().atStartOfDay());
							e.setDateEnd(endDateTime.toLocalDate().atStartOfDay());
						} else {
							e.setSeveralDays(false);	
							// do not set date end: this is an one-day event
							e.setDateEnd( null);
						}
					} else {
						// set if this event extends to more than one day or not
						if (endDateTime.toLocalDate().isAfter(e.getDateStart().toLocalDate())) {
							e.setSeveralDays(true);
						} else {
							e.setSeveralDays(false);	
						}
					}
				}
				
				// reset value
				maybeAllDayEvent = false;
				
				numberOfCalendarComponents++;
				return e;

			} else if (line.startsWith("BEGIN:VEVENT")  && e.getDateEnd() == null){

				TestLog.ve(ReadICal.class, "Missing Event end: is set automatically one hour later");
				setInvalidFormatFound(true);
				if (e.getDateStart() == null) {
					new UnexpectedValueException("String", "dateStart", "is null in cal: " + cal.getName()).printDescription();
					TestLog.ve(ReadICal.class, "No event created...");
					return null;
				}
				e.setDateEnd(e.getDateStart().plusHours(1));
				numberOfCalendarComponents++;
				return e;
			} else if (line.startsWith("END:VCALENDAR")){
				if ( e.getDateEnd() == null){
					TestLog.ve(ReadICal.class, "Missing Event end: is set automatically one hour later");
					setInvalidFormatFound(true);
					if (e.getDateStart() == null) {
						new UnexpectedValueException("String", "dateStart", "is null in cal: " + cal.getName()).printDescription();
						TestLog.ve(ReadICal.class, "No event created...");
						return null;
					}
					e.setDateEnd(e.getDateStart().plusHours(1));
					numberOfCalendarComponents++;					
				} 
				return e;

			} else {
				unsupportedSet.add(ICalStringHelper.getPropertyName(line));
				TestLog.ve(ReadICal.class, "Line " + index + ": Unsupported event property: " + line);				
			}
		} // for loop
		TestLog.ve(ReadICal.class, "No event created...");
		return null;		
	}

	/**
	 * Create a todo from lines.
	 * Read line by line until END:VTODO is reached
	 * 
	 * @param lines		the line (Strings)
	 * @param cal		the PCard, the todo belongs to
	 * 
	 * @return			the PTodo or null if an error occurred
	 */
	private PTodo createPTodo(String[] lines, PCalendar cal) {

		PTodo todo = new PTodo(cal);
		
		// reset value
		//maybeAllDayEvent = false;

		// check for valid Event: 
		String line = lines[index++];

		int len = lines.length;
		for ( ; index < len; index++) {
			line = lines[index];

			if (line.startsWith("UID:")){ // String!
				String uid = line.substring(line.indexOf(":") + 1, line.length());
				todo.setUid(uid);
	
			} else if (line.startsWith("DTSTART")){
			
				String dtstart = line.substring(8, line.length());
				

				// get the Temporal; this might be a LocalDate or LocalDateTime	
				Temporal temp = ICalDateTimeHelper.getDateTime(dtstart);
				if (temp == null) {
					TestLog.ve(ReadICal.class, "Invalid Temporal: " + dtstart, 5);//"Temporal", "temp", "is null in cal: " + cal.getName()).printDescription();
					TestLog.ve(ReadICal.class, "No event created...");
					return null;
				}
				LocalDateTime startDateTime = null;
				if (temp instanceof LocalDateTime) {
					// this is a LocalDateTime
					startDateTime = (LocalDateTime) temp;
				} else if (temp instanceof LocalDate) {
					startDateTime = ((LocalDate)temp).atStartOfDay();
				} else {
					TestLog.ve(ReadICal.class, "Invalid Temporal: " + startDateTime + " in cal: " + cal.getName());
					//new UnexpectedValueException("Temporal", "temp", "is null in cal: " + cal.getName()).printDescription();
					TestLog.ve(ReadICal.class, "No event created...");
					return null;
				}
				// set at start of day:
				todo.setDateStart( startDateTime);
				
			} else if (line.startsWith("DUE")){ // must be the same format like start

				String dtend = line.substring(4, line.length());
				// get the Temporal; this might be a LocalDate or LocalDateTime	
				Temporal temp = ICalDateTimeHelper.getDateTime(dtend);
				if (temp == null) {
					TestLog.ve(ReadICal.class, "Invalid Temporal: " + dtend + " in cal: " + cal.getName());
					//new UnexpectedValueException("Temporal", "temp", "is null in cal: " + cal.getName()).printDescription();
					TestLog.ve(ReadICal.class, "No event created...");
					return null;
				}
				LocalDateTime endDateTime = null;
				if (temp instanceof LocalDateTime) {
					// this is a LocalDateTime
					endDateTime = (LocalDateTime) temp;
				} else if (temp instanceof LocalDate) {
					endDateTime = ((LocalDate)temp).atStartOfDay();
				} else {
					TestLog.ve(ReadICal.class, "Invalid Temporal: " + dtend + " in cal: " + cal.getName());
					//new UnexpectedValueException("Temporal", "temp", "is null in cal: " + cal.getName()).printDescription();
					TestLog.ve(ReadICal.class, "No event created...");
					return null;
				}
				// set at start of day:
				todo.setDue( endDateTime);
				
				
			} else if (line.startsWith("DURATION:")){
				
				if (todo.getDateStart() == null) {
					new UnexpectedValueException("String", "dateStart", "is null in cal: " + cal.getName()).printDescription();
					TestLog.ve(ReadICal.class, "No event created...");
					return null;
				}
				// TODO all-day events with time 000000
				String durationString = line.substring(line.indexOf(":") + 1, line.length());
				LocalDateTime endDateTime = ICalDateTimeHelper.
						durationToDateTime(todo.getDateStart(), durationString);
				todo.setDue( endDateTime);

			} else if (line.startsWith("LAST-MODIFIED")){ 
				// LAST-MODIFIED;VALUE=DATE-TIME:20210522T094547Z
				// always set as time stamp if exists: overwrite existing time stamp

				Instant instant = ICalDateTimeHelper.getInstantFromUtcString(line.substring(line.indexOf(":") + 1, line.length())); //remove DTSTAMP:  
				if (instant == null) {
					if (PeaProperties.isTestMode()) {
						TestLog.ve(ReadICal.class, "Line " + index + ": Couldn't read LAST-MODIFIED value: set time stamp of now...");
					}
					instant = Instant.now();
				}
				todo.setTimeStamp(instant);

			} else if (line.startsWith("DTSTAMP")){ 
				if (todo.getTimeStamp() != null) {
					// time stamp was set by LAST_MODIFIED
				} else {
					Instant instant = ICalDateTimeHelper.getInstantFromUtcString(line.substring(line.indexOf(":") + 1, line.length())); //remove DTSTAMP:  
					if (instant == null) {
						if (PeaProperties.isTestMode()) {
							TestLog.ve(ReadICal.class, "Line " + index + ": Couldn't read time stamp: set time stamp of now...");
						}
						instant = Instant.now();
					}
					todo.setTimeStamp(instant);
				}
				
			} else if (line.startsWith("CREATED")){ 
				// do nothing

/*			} else if (line.startsWith("RDATE")){ // TODO PERIOD
				
				LocalDate[] rDates = ICalDateTimeHelper.getDatesFromList(line);				
				
				e.setRecurring(true);
				RecurringPEvent re  = RecurringPEvent.castToRecurringPEvent(e);
				
				re.setRecurrenceDates(rDates);
				e = re;						

			} else if (line.startsWith("EXDATE")){ // 
				
				LocalDate[] exDates = ICalDateTimeHelper.getDatesFromList(line);
				
				e.setRecurring(true);
				RecurringPEvent re = RecurringPEvent.castToRecurringPEvent(e);
				
				re.setExceptionDates(exDates);
				e = re;

			} else if (line.startsWith("RRULE:FREQ=")){ 

				RecurringPEvent re = null;
				
				String frequencyString = getParameterValue("FREQ", line);

				ChronoUnit unit = ICalRecurrenceHelper.getChronoUnitFromString(frequencyString);
				if (unit == null) { // invalid or unsupported
					//re.setRecurring(false);
					new UnexpectedValueException("unit", "ChronoUnit", "is null").printDescription();
					unSupportedPropertiesFound = true;
					TestLog.ve(ReadCard.class, "Couldn't read recurrence rule - event is treated as non recurring...");
				} else {
					
					e.setRecurring(true);
					re = RecurringPEvent.castToRecurringPEvent(e);

					re.setFrequency(unit);
					
					// interval:
					if (line.contains("INTERVAL=")) {
						re.setInterval(Integer.parseInt(getParameterValue("INTERVAL", line)));

					}
					if (line.contains("BYMONTH=")) {
						String byMonthString = getParameterValue("BYMONTH", line);
						String[] byMonthStrings = byMonthString.split(",");
						int[] byMonths = new int[byMonthStrings.length];
						int byMonthLen = byMonthStrings.length;
						for (int bmIndex = 0; bmIndex < byMonthLen; bmIndex++) {
							byMonths[bmIndex] = Integer.parseInt(byMonthStrings[bmIndex]);
						}
						re.setByMonth(byMonths);
					}
					if (line.contains("BYDAY=")) {
						String byDayString = getParameterValue("BYDAY", line);
						String[] byDayStrings = byDayString.split(",");
						re.setByDay(byDayStrings);
					}
					
					//TestLog.o(ReadCard.class, "freq: " + frequency);
					// figure out if there are more than one parameter after frequency:
					//int secondSemicolonIndex = line.indexOf(";", firstSemicolonIndex +1);
					if (line.contains("COUNT=")) {
						
						String countString = getParameterValue("COUNT", line);
						//TestLog.o(ReadCard.class, "count: " + countString);
						re.setCount(Integer.parseInt(countString));// occurrences 
					} else if (line.contains("UNTIL=")) {
						String untilDateString = getParameterValue("UNTIL", line);

						//TestLog.o(ReadCard.class, "until: " + untilDateString);
						Temporal until = ICalDateTimeHelper.getDateTime(untilDateString);
						if (until instanceof LocalDate) {
							re.setUntil( ((LocalDate) until).atStartOfDay());
						} else if (until instanceof LocalDateTime) {
							// time zone... set to start of day
							re.setUntil( ((LocalDateTime) until).toLocalDate().atStartOfDay());
						}
					} 
				}
				if (re != null) {
					e = re;
				}*/
			} else if (line.startsWith("SUMMARY")){
				todo.setSummary(ICalStringHelper.readString(lines, index));
				
			} else if (line.startsWith("\t") || line.startsWith(" ")){ // tab or whitespace: ignore
			} else if (line.startsWith("DESCRIPTION")){
				
				todo.setDescription(ICalStringHelper.readString(lines, index));
				
			} else if (line.startsWith("LOCATION")){
				if (line.startsWith("LOCATION;ALTREP")) {
					// no alternate representation is supported like URL
					//setUnsupportedPropertiesFound(true);
					unsupportedSet.add(line);
				} else {
					todo.setLocation(ICalStringHelper.readString(lines, index));			
				}
				// Status
				// Priority
			} else if (line.startsWith("PRIORITY:")){ // 0 - 9
				String priority = line.substring(9, line.length());
				int priorityValue = Integer.parseInt(priority);
				if (priorityValue > 0 && priorityValue < 10) {
					todo.setPriority(priorityValue);
				}
			} else if (line.startsWith("STATUS:")){ // 0 - 9
				String status = line.substring(line.indexOf(":") + 1, line.length());
				if (status.equals("NEEDS-ACTION") || status.equals("COMPLETED") || status.equals("IN-PROCESS") || status.equals("CANCELLED")) {
					todo.setStatus(status);
				} else {
					TestLog.ve(ReadICal.class, "Invalid status for VTODO: " + status);
					setInvalidFormatFound(true);	
				}
				
			} else if (line.startsWith("URL:")){ // 
				URL url = null; 
				try {
					url = new URL( ICalStringHelper.readString(lines, index));					
				} catch (MalformedURLException e1) {
					TestLog.e(ReadICal.class, "Invalid URL: " + url);
				}
				if (url != null) {
					todo.setUrl(url);
				}
			} else if (line.startsWith("BEGIN:VALARM")){ // TODO
				createPAlarm(todo, lines);

			} else if (line.startsWith("END:VTODO")){
			
				numberOfCalendarComponents++;
				return todo;

			} else if (line.startsWith("BEGIN:VTODO") ){

				TestLog.ve(ReadICal.class, "Missing todo end");
				setInvalidFormatFound(true);
				numberOfCalendarComponents++;
				return todo;
			} else if (line.startsWith("END:VCALENDAR")){
				TestLog.ve(ReadICal.class, "Missing todo end");
				numberOfCalendarComponents++;
				return todo;

			} else {
				unsupportedSet.add(ICalStringHelper.getPropertyName(line));
				TestLog.ve(ReadICal.class, "Line " + index + ": Unsupported event property: " + line);				
			}
		} // for loop
		TestLog.ve(ReadICal.class, "No todo created...");
		return null;		
	}
	
	private void createPAlarm(CalendarComponent comp, String[] lines) {
		// TODO Test
		//TestLog.e(getClass(), "Not yet implemented: VALARM");
		
		String line = lines[index++];

		int len = lines.length;
		String trigger = null;
		String action = null;
		String description = null;
		
		for ( ; index < len; index++) {
			line = lines[index];
			
			if (line.startsWith("TRIGGER")){ 
				
				if (line.contains(";VALUE=DATE-TIME:")) { // point in time: TODO compute duration		
					TestLog.ve(getClass(), "Can't read date-time trigger: " + line);
					break;
				}			
				trigger = line.substring(line.indexOf(":") + 1, line.length());				
	
			} else if (line.startsWith("ACTION:")){
				String actionString = line.substring(line.indexOf(":") + 1, line.length());
				String[] actionStrings = PAlarm.getActionStrings();
				for (int i = 0; i < 3; i++) {
					String a = actionStrings[i];
					if (actionString.toUpperCase().equals(a)) {
						action = a;
						break;
					}					
				}
				
			} else if (line.startsWith("DESCRIPTION:")){
				description = line.substring(line.indexOf(":") + 1, line.length());	
				
			} else if (line.startsWith("END:VALARM")){
				break;
			} else {
				TestLog.ve(getClass(), "Unknown VALARM property: " + line);
			}			
		}
		// set the alarm if complete:
		if (comp != null && trigger != null && action != null) {
			PAlarm alarm = new PAlarm(comp, trigger, action, description);
			comp.addAlarm(alarm);
		} else {
			TestLog.ve(getClass(),"Incomplete VALARM is skipped");
		}
	}
	
	/**
	 * Get the value of an iCalendar parameter
	 * 
	 * @param paramName	the name of the parameter without =/:
	 * 					for example "INTERVAL". 
	 * 					Note: The existence in the line must be checked before
	 * @param line		the line that contains the parameter name
	 * 
	 * @return		the value of the parameter as string
	 */
	private String getParameterValue(String paramName, String line) {
		String resultString = null;
		if (line.indexOf(";", line.indexOf(paramName)) > -1) {// there are other parameters
			resultString = line.substring(line.indexOf(paramName) + paramName.length()+1,// +1 for = or : 
					line.indexOf(";", line.indexOf(paramName)));
		} else { // this is the last parameter
			resultString = line.substring(line.indexOf(paramName) + paramName.length()+1,// +1 for = or : 
					line.length());
		}
		return resultString;
	}

	/**
	 * Check if there was at least one invalid file format found
	 * 
	 * @return invalidFormatFound	true if there was an invalid format found
	 */
	public static synchronized boolean isInvalidFormatFound() {
		return invalidFormatFound;
	}
	
	/**
	 * Set if there was at least one invalid file format found
	 * 
	 * @param invalidFormatFound	true if there was an invalid format found
	 */
	protected static synchronized void setInvalidFormatFound(boolean _invalidFormatFound) {
		invalidFormatFound = _invalidFormatFound;
	}
	
	/**
	 * Check if there was at least one calendar successfully added
	 * 
	 * @return	true if there was no calendar added
	 */
	public static synchronized boolean isNoCalendarAdded() {
		return noCalendarAdded;
	}
	
	private static synchronized void setNoCalendarAdded(boolean added) {
		noCalendarAdded = added;
	}
	
	/**
	 * Get the unsupported properties and components
	 * 
	 * @return  the unsupported properties and components as multi-line string
	 */
	public static String getUnsupportedProps() {
		if (unsupportedSet.isEmpty()) {
			return null;
		}
		StringBuilder sb = new StringBuilder();
		for (String s : unsupportedSet) {
			sb.append(s + "\n");
		}
		return new String(sb);
	}
	
	/**
	 * Get the number of successfully added events from all calendars
	 * 
	 * @return	the number of added events
	 */
	public static synchronized int getNumberOfEvents() {
		return numberOfCalendarComponents;
	}	
	
	private static synchronized void setNumberOfEvents(int newNumber) {
		ReadICal.numberOfCalendarComponents = newNumber;
	}	
}