package cologne.eck.calendar_pea.util;

/*
 * PeaFactory - Production of Password Encryption Archives
 * Copyright (C) 2015  Axel von dem Bruch
 * 
 * This library is free software; you can redistribute it and/or modify it 
 * under the terms of the GNU General Public License as published 
 * by the Free Software Foundation; either version 2 of the License, 
 * or (at your option) any later version.
 * This library is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
 * or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU General Public License for more details.
 * See:  http://www.gnu.org/licenses/gpl-2.0.html
 * You should have received a copy of the GNU General Public License 
 * along with this library.
 */

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Collections;

import cologne.eck.calendar_pea.control.AlarmControl;
import cologne.eck.calendar_pea.control.CalendarControl;
import cologne.eck.calendar_pea.data.CalendarComponent;
import cologne.eck.calendar_pea.data.PAlarm;
import cologne.eck.calendar_pea.data.PCalendar;
import cologne.eck.calendar_pea.data.PEvent;
import cologne.eck.calendar_pea.data.PTodo;
import cologne.eck.calendar_pea.data.RecurringPEvent;
import cologne.eck.calendar_pea.gui.event_display.EventButton;
import cologne.eck.tools.TestLog;
import cologne.eck.tools.UnexpectedValueException;

public class CalendarComposer {

	private final static LocalTime DAY_START = LocalTime.of(0,  0);
	
	private static LocalDateTime now;
	private static LocalDateTime end;
	private static LocalDateTime beforeDayTime;

	/**
	 * Sort and ArrayList of PEvents for one day and create two list
	 * of EventButtons for this events: 
	 * at index 0 all-day events
	 * at index 1 with-time events
	 * 
	 * @return	two sorted lists of events, 
	 * 			sorted by the start time in this day
	 * 			at index 0 all-day events
	 * 			at index 1 with-time events
	 */
	public static ArrayList<ArrayList<EventButton>> sortEventList(ArrayList<PEvent> eventList, LocalDate date){

		if (eventList == null) {
			return null;
		}
		int listSize = eventList.size();		
		if (listSize == 0) {
			return null;
		}
		// Needed for alarms:
		now = LocalDateTime.now();
		end = now.plusHours(AlarmControl.getUpdateAfterHours());
		beforeDayTime = LocalDate.now().minusDays(1).atTime(DAY_START);
		//ArrayList<PAlarm> alarms = new ArrayList<PAlarm>();
		// TODO: check next day if this is for a DayView
		
		//TestLog.o(XXXX.class, "Composer list size: " + listSize);
		ArrayList<ArrayList<EventButton>> list = new ArrayList<ArrayList<EventButton>>(2);		
		ArrayList<EventButton> allDayList = new ArrayList<EventButton>();
		ArrayList<EventButton> withTimeList = new ArrayList<EventButton>();
		LocalDateTime dayStartDateTime = date.atTime(DAY_START);
		
		for (int i = 0; i < listSize; i++) {
			// set startInBetween for all events			
			
			PEvent e = eventList.get(i);			
			if (e.getCalendar().getEventList().size() == 0) {
				new UnexpectedValueException("int", "size of eventList", "is 0:"+ 
						e.getCalendar().getName() + " " + e.getCalendar().getCalUid()).printDescription();
			}
			if (e.isAllDay()) {
				allDayList.add( new EventButton(e, DAY_START));
				checkAlarm(e, dayStartDateTime);
			} else if (e.isSeveralDays()) {
				if (e.getDateStart().isBefore(dayStartDateTime)) {
					withTimeList.add( new EventButton(e, DAY_START));
					checkAlarm(e, dayStartDateTime);
				} else {
					withTimeList.add(new EventButton(e, e.getDateStart().toLocalTime()));
					checkAlarm(e, dayStartDateTime);
				}				
			} else {
				withTimeList.add(new EventButton(e, e.getDateStart().toLocalTime()));
				checkAlarm(e, dayStartDateTime);
			}
		}		
		Collections.sort(withTimeList);
		Collections.sort(allDayList);
		list.add(allDayList);
		list.add(withTimeList);
		// add alarms of todos
		checkTodoAlarms();
		return list;
	}	
	
	/**
	 * Iterate of all PTodos and set alarms 
	 */
	private static void checkTodoAlarms() {

		ArrayList<PCalendar> cals = CalendarControl.getShownCalendarList();
		ArrayList<PTodo> todoList = new ArrayList<PTodo>();
		for (PCalendar c : cals) {
			todoList.addAll(c.getTodoList());
		}
		LocalDateTime dayStart = LocalDate.now().atTime(LocalTime.of(0,  0));

		for (PTodo t : todoList) {
			ArrayList<PAlarm> alarms = t.getAlarms();
			if (alarms == null || alarms.size() == 0) {
				return;
			}		
			for (PAlarm alarm : alarms) {
				LocalDateTime triggerTime = alarm.getTriggerTime();
				if (triggerTime != null) {
					if (triggerTime.isAfter(end)) {
						continue;
					} else if (triggerTime.isBefore(now) ) {
						// show only missed triggers of this day
						if (triggerTime.isAfter(dayStart)) {
							AlarmControl.addMissedDate(alarm);
						}
					} else {
						AlarmControl.addAlarm(alarm);
					}
				} else {
					TestLog.ve(CalendarComponent.class,"Missing trigger time", 3);
				}
			}
		}
	}

	/**
	 * Check if alarms are in the period to show.
	 * This sets also missed alarms of the same day.
	 * 
	 * @param e		the event to check
	 * @param startDateTime	the day start to check
	 */
	private static void checkAlarm(PEvent e, LocalDateTime startDateTime) {
		ArrayList<PAlarm> alarms = e.getAlarms();
		if (alarms == null || alarms.size() == 0) {
			return;
		}
		
		if (startDateTime.isBefore(beforeDayTime)) {
			// Not relevant, whether for missing dates nor for current alarms
			if (e.isRecurring() == false) {
				return;
			} else {
				if (e instanceof RecurringPEvent) {
					if (((RecurringPEvent) e).getLastOccurrence() != null) {
						if (((RecurringPEvent) e).getLastOccurrence().isBefore(beforeDayTime.toLocalDate()) == true) {
							return;
						}
					}
				}
			}
		}
		
		for (PAlarm alarm : alarms) {
			if (e.isRecurring() == false) {
				LocalDateTime triggerTime = alarm.getTriggerTime();
				if (triggerTime != null) {
					if (triggerTime.isAfter(end)) {
						continue;
					} else if (triggerTime.isBefore(now)) {
						AlarmControl.addMissedDate(alarm);
					} else {
						AlarmControl.addAlarm(alarm);
					}
				} else {
					TestLog.ve(CalendarComponent.class,"Missing trigger time", 3);
				}
			} else {
				// get alarms of whole day:
				ArrayList<PAlarm>  a = ((RecurringPEvent) alarm.getCalComponent())
						.getAlarmsOfDay(now.toLocalDate().atTime(DAY_START), now.toLocalDate().atTime(LocalTime.of(23, 59)));
				if (a != null && a.size() > 0) {
					LocalTime trigTime = alarm.getTriggerTimeOfRecurringEvent();
					if (trigTime.isBefore(now.toLocalTime())) { // TODO TEST
						AlarmControl.addMissedDate(alarm);
					} else if (trigTime.isAfter(now.toLocalTime() )
							&& trigTime.isBefore(end.toLocalTime())) {
						AlarmControl.addAlarm(alarm);
					}
				}
			}
		}
	}
}
