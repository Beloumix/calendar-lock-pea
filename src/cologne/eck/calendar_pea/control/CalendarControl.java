package cologne.eck.calendar_pea.control;

import java.awt.Component;

/*
 * PeaFactory - Production of Password Encryption Archives
 * Copyright (C) 2015  Axel von dem Bruch
 * 
 * This library is free software; you can redistribute it and/or modify it 
 * under the terms of the GNU General Public License as published 
 * by the Free Software Foundation; either version 2 of the License, 
 * or (at your option) any later version.
 * This library is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
 * or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU General Public License for more details.
 * See:  http://www.gnu.org/licenses/gpl-2.0.html
 * You should have received a copy of the GNU General Public License 
 * along with this library.
 */

import java.awt.Dimension;
import java.net.MalformedURLException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.ResourceBundle;

import javax.swing.JOptionPane;

import cologne.eck.all_peas.control.Translator;
import cologne.eck.all_peas.data.PeaProperties;
import cologne.eck.all_peas.gui.PswDialogView;
import cologne.eck.calendar_pea.data.PCalendar;
import cologne.eck.calendar_pea.data.PEvent;
import cologne.eck.calendar_pea.data.PTodo;
import cologne.eck.calendar_pea.gui.LockFrameCalendar;
import cologne.eck.calendar_pea.gui.calendar_view.CalendarView;
import cologne.eck.calendar_pea.gui.event_display.EventButton;
import cologne.eck.calendar_pea.util.CalendarComposer;
import cologne.eck.tools.TestLog;
import cologne.eck.tools.UnexpectedValueException;

public class CalendarControl {
		
	/**
	 * List of all shown calendars
	 */
	private static ArrayList<PCalendar> shownCalendarList = new ArrayList<PCalendar>();
	
	/**
	 * List of hidden calendars
	 */
	private static ArrayList<PCalendar> hiddenCalendarList = new ArrayList<PCalendar>();	
	

	// to check, which calendars are save in a file, set in ReadCard
	private static HashMap<String, HashSet<PCalendar>> fileCalendarsMap = new HashMap<String, HashSet<PCalendar>>();
	
	// register merged calendars: set docChangesUnsaved
	private static boolean calendarsMerged = false;
	
	
	/**
	 * Set the language support for the PEA
	 * 
	 * @param _language					the language to set
	 * @throws MalformedURLException
	 */
	public final static void setCalendarLanguagesBundle(String _language) throws MalformedURLException {
	
		TestLog.v(CalendarControl.class, "Set calendar language: " + _language + "...");
		
		Translator translator = PeaProperties.getVmBridge().getTranslator();
		ResourceBundle calBundle = translator.getPeaLanguageBundle(_language);
		CalendarSetting.setResourceBundle(calBundle);
	}
	
	/**
	 * Add an Event. The view is not refreshed!
	 * 
	 * @param e		the Event to add
	 * @param cal	the PeaCalendar, this Event belongs to
	 * 
	 * @return		true if this collection changed as a result of the call
	 */
	public static final boolean addEvent(PEvent e, PCalendar cal) {
		boolean result = cal.addEvent(e);
		LockFrameCalendar.getFrame().markUnsavedContentChanges();
		return result;
	}
	
	/**
	 * Add a todo. The view is not refreshed!
	 * 
	 * @param e		the todo to add
	 * @param cal	the PeaCalendar, this todo belongs to
	 * 
	 * @return		true if this collection changed as a result of the call
	 */
	public static final boolean addTodo(PTodo e, PCalendar cal) {
		boolean result = cal.addTodo(e);
		LockFrameCalendar.getFrame().markUnsavedContentChanges();
		return result;
	}
	
	/**
	 * Remove an Event. The view is not refreshed!
	 * 
	 * @param e		the Event to remove from the list
	 * @param cal	the PeaCalendar, this Event belongs to
	 * 	 * 
	 * @return		true if this list contained the specified element
	 */
	public final static boolean removeEvent( PEvent e, PCalendar cal) {
		boolean result = cal.removeEvent(e);
		LockFrameCalendar.getFrame().markUnsavedContentChanges();
		return result;
	}
	
	/**
	 * Remove a todo. The view is not refreshed!
	 * 
	 * @param e		the todo to remove from the list
	 * @param cal	the PeaCalendar, this todo belongs to
	 * 	 * 
	 * @return		true if this list contained the specified element
	 */
	public final static boolean removeTodo( PTodo e, PCalendar cal) {
		boolean result = cal.removeTodo(e);
		LockFrameCalendar.getFrame().markUnsavedContentChanges();
		return result;
	}
	
	/**
	 * Check if a calendar already exists by UID and
	 * ask to add or not. This should run in EDT.
	 * 
	 * @param cal	the calendar to check
	 * @param newFileName	the new file name
	 * 
	 * @return	true: calendar should be added, false don't add
	 */
	private static boolean checkDuplicateCalendarToShow(PCalendar cal, String newFileName) {
		int len = shownCalendarList.size();
		String newCalUID = cal.getCalUid();
		String calendarName = cal.getName();
		Component com = null;
		if (PswDialogView.getView().isShowing() == true) {
			com = PswDialogView.getView();
		} else {
			 com = LockFrameCalendar.getFrame();
		}
		String newFileNameString = null;
		if (newFileName ==  null) {
			new UnexpectedValueException("String", "newFileName", "is null").printDescription();
			return false;
		}
		for (int i = 0; i < len; i++) {
			PCalendar c = shownCalendarList.get(i);
			String cCalsString = "";
			String[] aFileNames = c.getAssociatedFileNames();
			for (String aFileName : aFileNames) {
				cCalsString += "\n   " + aFileName; 
			}
			if (c.getCalUid().equals(newCalUID)) {

				// duplicate calendar
				int result = JOptionPane.showConfirmDialog(
						com, 
						CalendarSetting.calTranslate("calendar") + ": " + cal.getName() 
						+"\n" + newFileName +
						"\n(UID " + newCalUID + "):\n\n" +
						CalendarSetting.calTranslate("duplicate_calendar")
						+ cCalsString, 
						CalendarSetting.calTranslate("warning"), 
						JOptionPane.YES_NO_OPTION);
				if (result == JOptionPane.YES_OPTION) {
					// merge calendar
					c.mergeCalendar(cal, newFileName);
					removeFileFromFileCalendarsMap(newFileName);					
					addToFileCalendarsMap(newFileName, c);
					if (LockFrameCalendar.getFrame() != null) {
					LockFrameCalendar.getFrame().markUnsavedContentChanges();
					}
					calendarsMerged = true;
					return false;// do not add
				} else {
					// do not add
					return false;
				}
			} else {
				if (c.getName().equals(calendarName)) {
					// TODO option to rename new calendar
					// duplicate calendar
					int result = JOptionPane.showConfirmDialog(
							com, newFileNameString +
							CalendarSetting.calTranslate("calendar") + ": " + calendarName + ":\n" +
							CalendarSetting.calTranslate("duplicate_calendar"), 
							CalendarSetting.calTranslate("warning"), 
							JOptionPane.YES_NO_OPTION);
					if (result == JOptionPane.YES_OPTION) {
						// merge calendar
						c.mergeCalendar(cal, newFileName);
						removeFileFromFileCalendarsMap(newFileName);	
						addToFileCalendarsMap(newFileName, c);
						//LockFrameContact.setDocChangeUnsaved(true);
						if (LockFrameCalendar.getFrame() != null) {
							LockFrameCalendar.getFrame().markUnsavedContentChanges();
						}
						calendarsMerged = true;
						return false;
					} else {
						// do not add
						return false;
					}
				}
			}
		}
		return true; // no duplicate
	}

	/**
	 * Get the currently shown list of Calendars 
	 * 
	 * @return	the list of currently shown Calendars
	 */
	public static ArrayList<PCalendar> getShownCalendarList() {
		return shownCalendarList;
	}
	
	/**
	 * Get the currently shown and hidden list of Calendars 
	 * 
	 * @return	the list of currently shown and hidden Calendars
	 */
	public static ArrayList<PCalendar> getShownAndHiddenCalendarList() {
		ArrayList<PCalendar> allList = new ArrayList<PCalendar>();
		allList.addAll(shownCalendarList);
		allList.addAll(hiddenCalendarList);
		return allList;
	}
	
	/**
	 * Get the currently hidden list of Calendars 
	 * 
	 * @return	the list of currently hidden Calendars
	 */
	public static ArrayList<PCalendar> getHiddenCalendarList() {
		return hiddenCalendarList;
	}
	
	/**
	 * Show one Calendar and add to list of shown Calendars, 
	 * checks for duplicates and refreshes the view if requiered
	 * 
	 * @param cal	the Calendar to show
	 * @param newFileName	the name of the file, this calendar is stored in
	 * 
	 * @return	true if status was changed (Calendar was not in list before)
	 */
	public static boolean showCalendar(PCalendar cal, String newFileName) {

		// check if calendar UID exists
		if (checkDuplicateCalendarToShow(cal, newFileName) == false) {
			// do not add calendar, no status change
			return false;
		}
		boolean result = shownCalendarList.add(cal);
		if (result == true) { // list was changed
			refreshView();
		}
		if (LockFrameCalendar.getFrame() != null) {
			LockFrameCalendar.getFrame().markUnsavedContentChanges();
		}
		return result;
	}
	
	/**
	 * Add one calendar to list of shown Calendars
	 * without refreshing the view
	 * 
	 * @param cal	the Calendar to show
	 * @param newFileName	the name of the file, this calendar is stored in
	 */
	public static void addShownCalendar(PCalendar cal, String newFileName) {
		if (cal == null) {
			new UnexpectedValueException("PCard", "cal", "is null").printDescription();
			return;
		}
		// check if calendar UID exists
		if (checkDuplicateCalendarToShow(cal, newFileName) == false) {
			// do not add calendar, no status change
			return;
		}
		shownCalendarList.add(cal);
		if (LockFrameCalendar.getFrame() != null) {
			LockFrameCalendar.getFrame().markUnsavedContentChanges();
		}
	}
	
	/**
	 * Hide a calendar: Remove from shown calendar list and add
	 * to hidden calendar list. This does not refresh the view
	 * and does not touch other models except the view. 
	 * 
	 * @param cal	the calendar to hide
	 */
	public static void hideShownCalendar(PCalendar cal) {
		if (cal == null) {
			new UnexpectedValueException("PCard","cal","is null").printDescription();
			return;
		}
		boolean added = hiddenCalendarList.add(cal);
		if (added == false) {
			new UnexpectedValueException("PCard","cal","was already in hiddenCalendarList").printDescription();
		}
		boolean contained = shownCalendarList.remove(cal);
		if (contained == false) {
			new UnexpectedValueException("PCard","cal","was not in shownCalendarList").printDescription();
		}
	}
	
	/**
	 * Hide a calendar: Remove from shown calendar list and add
	 * to hidden calendar list. This does not refresh the view
	 * and does not touch other models except the view. 
	 * 
	 * @param cal	the calendar to hide
	 */
	public static void showHiddenCalendar(PCalendar cal) {
		if (cal == null) {
			new UnexpectedValueException("PCard","cal","is null").printDescription();
			return;
		}
		boolean added = shownCalendarList.add(cal);
		if (added == false) {
			new UnexpectedValueException("PCard","cal","was already in shownCalendarList").printDescription();
		}
		boolean contained = hiddenCalendarList.remove(cal);
		if (contained == false) {
			new UnexpectedValueException("PCard","cal","was not in hiddenCalendarList").printDescription();
		}
	}
	
	/**
	 * Adds a list to shown calendar list
	 * All existing calendars remain. The view is not refreshed
	 * 
	 * @param shownCalendarList	the list of Calendars to show
	 * @param fileName	the name of the file, this calendars are stored in
	 */
	protected static void addShownCalendarList(ArrayList<PCalendar> newShownCalendarList, String fileName) {

		int len = newShownCalendarList.size();
		for (int i = 0; i < len; i++) {
			// check if calendar UID exists
			if (checkDuplicateCalendarToShow(newShownCalendarList.get(i), fileName) == true) {
				// add calendar
				shownCalendarList.add(newShownCalendarList.get(i));
			}
		}
		if (LockFrameCalendar.getFrame() != null) {
			LockFrameCalendar.getFrame().markUnsavedContentChanges();
		}
	}

	/**
	 * Refresh the current calendar view:
	 * remove all events of all calendars an create the view again. 
	 * This should always be called if more than one day is affected
	 * by any change of the view: new properties or new events.
	 * This should run in EDT.
	 */
	public static void refreshView() {
		CalendarView view = LockFrameCalendar.getView();
		// keep the size:
		Dimension dim = view.getPanel().getSize();
		LockFrameCalendar.getView().refreshView();
		// TODO refresh TODO
		LockFrameCalendar.getTodoView().refreshView();
		view.getPanel().setPreferredSize(dim);
	}

	/**
	 * Get a calendar by its name
	 * Searches in shown and hidden calendars.
	 * 
	 * @param selectedCalendarName	the name of the calendar
	 * 
	 * @return	the calendar or null, if there is no such calendar in the list
	 */
	public static PCalendar getCalendarByName(String selectedCalendarName) {
		int size = shownCalendarList.size();
		for (int i = 0; i < size; i++) {
			if (shownCalendarList.get(i).getName().equals(selectedCalendarName)) {			
				return shownCalendarList.get(i);
			}
		}		
		for (PCalendar c : hiddenCalendarList) {
			if (c.getName().equals(selectedCalendarName)) {
				return c;
			}
		}
		return null;
	}
	
	/**
	 * Get a calendar by its unique identifier. 
	 * Searches in shown and hidden calendars.
	 * 
	 * @param _UID	the UID of the calendar
	 * 
	 * @return	the calendar or null, if there is no such calendar in the list
	 */
	public static PCalendar getCalendarByUID(String _UID) {
		int size = shownCalendarList.size();
		for (int i = 0; i < size; i++) {
			if (shownCalendarList.get(i).getCalUid().equals(_UID)) {			
				return shownCalendarList.get(i);
			}
		}		
		for (PCalendar c : hiddenCalendarList) {
			if (c.getCalUid().equals(_UID)) {
				return c;
			}
		}
		return null;
	}
	
	/**
	 * Remove a calendar from the shown list and refresh view if required
	 * 
	 * @param pCal	the calendar to remove
	 * 
	 * @return	true, if the call changes the list
	 */
	public static final boolean removeCalendar(PCalendar pCal) {
		boolean result = shownCalendarList.remove(pCal);
		if (result == true) {
			refreshView();
			if (LockFrameCalendar.getFrame() != null) {
				LockFrameCalendar.getFrame().markUnsavedContentChanges();
			}
		}
		return result;
	}
	
	/**
	 * Get the names of shown calendars as an array of Strings
	 * 
	 * @return	the calendar names as an array of Strings
	 */
	public static final String[] getShownCalendarNameList() {
		
		int listSize = shownCalendarList.size();
		if (listSize > 0) {
			String[] calendarNames = new String[listSize];
			for (int i = 0; i < listSize; i++) {
				calendarNames[i] = shownCalendarList.get(i).getName();
			}	
			return calendarNames;
		} else {
			return null;
		}
	}
	
	/**
	 * Get the number of all events from all calendars
	 * 
	 * @return	 the number of all events
	 */
	public static final int getNumberOfAllShownEvents() {
		int listSize = shownCalendarList.size();
		int result = 0;
		for (int i = 0; i < listSize; i++) {
			result += shownCalendarList.get(i).getEventList().size();			
		}
		return result;
	}

	/**
	 * Get all events as EventButtons for the given period, 
	 * one list per day containing list of all-day events and events with time, 
	 * sorted by start time. 
	 * 
	 * @param firstShownDate	first day of the requested period
	 * @param numberOfDays		number of days of the requested period
	 * 
	 * @return	an ArrayList of one EventButton list for each day, 
	 * 				that contains two lists:
	 * 					index 0: all-day events, index 1: events with time
	 */
	public static ArrayList<ArrayList<ArrayList<EventButton>>> getSortedShownEventButtonList(
			LocalDate firstShownDate, int numberOfDays){
		
		if (shownCalendarList == null) {
			new UnexpectedValueException("shownCalendarList", "ArrayList<PCard", "is null").printDescription();
			return null;
		}
		
		// list of day-lists that contain separated all-day events and events with time
		ArrayList<ArrayList<ArrayList<EventButton>>> resultList = new ArrayList<ArrayList<ArrayList<EventButton>>>(numberOfDays);
		// list of day-lists of events: all-day and events with time mixed
		ArrayList<ArrayList<PEvent>> eventList = new ArrayList<ArrayList<PEvent>>(numberOfDays);
		// initialize the lists:
		for (int dayIndex = 0; dayIndex < numberOfDays; dayIndex++) {;
			eventList.add(new ArrayList<PEvent>());
		}

		int calSize = shownCalendarList.size();
		// collect events of all calendars:
		if ( shownCalendarList != null && calSize > 0) { 
			for (int calIndex = 0; calIndex < calSize; calIndex++) {// iterate over calendars
				
				ArrayList<ArrayList<PEvent>>  eList = shownCalendarList.get(calIndex).getEventsOfDays(firstShownDate, numberOfDays);
				if (eList != null) {
					for (int dayIndex = 0; dayIndex < numberOfDays; dayIndex++) {
						eventList.get(dayIndex).addAll(eList.get(dayIndex));
					}
				}
			}
			for (int dayIndex = 0; dayIndex < numberOfDays; dayIndex++) {
				// sort the list and split in all-day and events with time:
				ArrayList<ArrayList<EventButton>> dayList = CalendarComposer.sortEventList(eventList.get(dayIndex), firstShownDate.plusDays(dayIndex));
				//TestLog.o(XXXX.class, "dayLists size: " + dayList.size());
				resultList.add(dayList);
			}			
		} else {
			// empty lists:
			for (int dayIndex = 0; dayIndex < numberOfDays; dayIndex++) {
				ArrayList<ArrayList<EventButton>> dayList = new ArrayList<ArrayList<EventButton>>();
				dayList.add(new ArrayList<EventButton>());
				dayList.add(new ArrayList<EventButton>());
				resultList.add(dayList);
			}	
		}
		return resultList;
	}
	
	/**
	 * Get all file names that were loaded in this session
	 * to show calendars. Files in the cloud are stored as backup
	 * files in system default tmp directory
	 * 
	 * @return	all used file names as Array
	 */
	public static String[] getAllAssociatedFileNames() {
		return fileCalendarsMap.keySet().toArray(new String[fileCalendarsMap.size()]);
	}

	/**
	 * Get the map of file names and the list of 
	 * calendars stored in this files
	 * 
	 * @return	the map of file names and calendars in these files
	 */
	public static HashMap<String, HashSet<PCalendar>> getFileCalendarsMap() {
		return fileCalendarsMap;
	}
	
	/**
	 * Set the map of file names and the list of 
	 * calendars stored in this files
	 * 
	 * @param	the map of file names and calendars in these files
	 */
	protected static void setFileCalendarsMap(HashMap<String, HashSet<PCalendar>> newMap) {
		fileCalendarsMap = newMap;
	}

	/**
	 * Add a PCalendars to a file name (the file
	 * contains this calendar)
	 * 
	 * @param fileName	the file name
	 * @param cal		the PCalendars as array contained in the file
	 */
	public static void addToFileCalendarsMap(String fileName, PCalendar[] cals) {
		if (fileName == null) {
			new UnexpectedValueException("String", "fileName", "is null").printDescription();
			return;
		}
		if (cals == null) {
			new UnexpectedValueException("PCard[]", "cals", "is null").printDescription();
			return;
		}
		if (CalendarControl.fileCalendarsMap.containsKey(fileName)) {
			CalendarControl.fileCalendarsMap.get(fileName).addAll(new ArrayList<>(Arrays.asList(cals)));
		} else {
			CalendarControl.fileCalendarsMap.put(fileName, new HashSet<>(Arrays.asList(cals)));
		}
	}	
	
	/**
	 * Add a PCard to a file name, if the file
	 * contains this calendar
	 * 
	 * @param fileName	the file name
	 * @param cal		the PCard contained in the file
	 */
	public static void addToFileCalendarsMap(String fileName, PCalendar cal) {
		if (fileName == null) {
			new UnexpectedValueException("String", "fileName", "is null").printDescription();
			return;
		}
		if (cal == null) {
			new UnexpectedValueException("PCard", "cal", "is null").printDescription();
			return;
		}
		if (CalendarControl.fileCalendarsMap.containsKey(fileName)) {
			CalendarControl.fileCalendarsMap.get(fileName).add(cal);
		} else {
			HashSet<PCalendar> calSet = new HashSet<PCalendar>();
			calSet.add(cal);
			CalendarControl.fileCalendarsMap.put(fileName, calSet);
		}
	}
	
	/**
	 * Remove one file name from the mapping file-calendars
	 * 
	 * @param fileNameToRemove	the file name to remove
	 * 
	 * @return	if it was removed: true
	 */
	public static boolean removeFileFromFileCalendarsMap(String fileNameToRemove) {
		if ( CalendarControl.fileCalendarsMap.remove(fileNameToRemove) == null) {
			return false;
		} else {
			return true;
		}
	}
	
	/**
	 * Get all PCalendars that are stored in a file
	 * 
	 * @param fileName	the file name, that contains calendars
	 * 
	 * @return	the calendars of the file as PCard[]
	 */
	protected static PCalendar[] getCalendarsOfFile(String fileName) {
		HashSet<PCalendar> set = fileCalendarsMap.get(fileName);
		return set.toArray(new PCalendar[set.size()]);		
	}
	
	/**
	 * Get all PCalendars that are stored in a file as set
	 * 
	 * @param fileName	the file name, that contains calendars
	 * 
	 * @return	the calendars of the file as HashSet
	 */
	public static HashSet<PCalendar> getCalendarsOfFileAsSet(String fileName) {
		return fileCalendarsMap.get(fileName);	
	}

	/**
	 * @return the calendarsMerged
	 */
	protected static boolean isCalendarsMerged() {
		return calendarsMerged;
	}
}
