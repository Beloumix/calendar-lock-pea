package cologne.eck.calendar_pea.control;

/*
 * PeaFactory - Production of Password Encryption Archives
 * Copyright (C) 2015  Axel von dem Bruch
 * 
 * This library is free software; you can redistribute it and/or modify it 
 * under the terms of the GNU General Public License as published 
 * by the Free Software Foundation; either version 2 of the License, 
 * or (at your option) any later version.
 * This library is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
 * or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU General Public License for more details.
 * See:  http://www.gnu.org/licenses/gpl-2.0.html
 * You should have received a copy of the GNU General Public License 
 * along with this library.
 */

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Insets;
import java.awt.Point;
import java.awt.Window;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.time.LocalDate;
import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map.Entry;
import java.util.Properties;
import java.util.Set;
import java.util.zip.CRC32;

import javax.swing.JColorChooser;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;

import cologne.eck.all_peas.cloud.CloudControl;
import cologne.eck.all_peas.cloud.CloudPropertyHandler;
import cologne.eck.all_peas.control.CommandLineHandler;
import cologne.eck.all_peas.control.FileRecipient;
import cologne.eck.all_peas.control.PeaControl;
import cologne.eck.all_peas.data.AttachmentHandler;
import cologne.eck.all_peas.data.PeaProperties;
import cologne.eck.all_peas.files.FileComposer;
import cologne.eck.all_peas.files.FileModel;
import cologne.eck.all_peas.files.FilePanelHolder;
import cologne.eck.all_peas.files.FileTypePanel;
import cologne.eck.all_peas.files.InvalidPanel;
import cologne.eck.all_peas.gui.CursorManager;
import cologne.eck.all_peas.gui.KeyFileDialogs;
import cologne.eck.all_peas.gui.PeaDialog;
import cologne.eck.all_peas.gui.PeaFileChooser;
import cologne.eck.all_peas.gui.PeaLockFrame;
import cologne.eck.all_peas.gui.PswDialogView;
import cologne.eck.all_peas.gui.Toaster;
import cologne.eck.all_peas.vm_specific.JREProperties;
import cologne.eck.calendar_pea.data.PCalendar;
import cologne.eck.calendar_pea.gui.CalendarMenuBar;
import cologne.eck.calendar_pea.gui.LockFrameCalendar;
import cologne.eck.calendar_pea.gui.calendar_view.CalendarView;
import cologne.eck.calendar_pea.gui.calendar_view.DayPanel;
import cologne.eck.calendar_pea.gui.calendar_view.MonthPanel;
import cologne.eck.calendar_pea.gui.calendar_view.WeekPanel;
import cologne.eck.calendar_pea.util.ReadICal;
import cologne.eck.peafactory.crypto.AlgoParamHandler;
import cologne.eck.peafactory.crypto.CipherStuff;
import cologne.eck.peafactory.crypto.RandomStuff;
import cologne.eck.peafactory.crypto.kdf.KeyDerivation;
import cologne.eck.tools.Comparator;
import cologne.eck.tools.Converter;
import cologne.eck.tools.EntropyPool;
import cologne.eck.tools.ExceptionHelper;
import cologne.eck.tools.FileTools;
import cologne.eck.tools.PropertyHandler;
import cologne.eck.tools.ReadResources;
import cologne.eck.tools.TestLog;
import cologne.eck.tools.UnexpectedValueException;


public class CalendarPEAControl extends PeaControl {
	
	private static CalendarPEAControl pswDialog;
	private static volatile LockFrameCalendar lockFrame; 
	private final static String peaPropFileName = "calendar.properties";
	private static final String[] supportedExtensions = {"ics", "ICS"};
	private static final String[] supportedLanguages = {"cz", "de", "es", "fr", 
			"gr", "id", "it", "lv", "nl", "pt", "ro",  "ru", "tr", 
			 "cn", "kr", "jp" //experimental
			}; 
	
	private static final String PEA_NAME = "Calendar-Lock-PEA";
	private static final String VERSION = "1.7 (PEA version 2)";
	private static final String YEAR_OF_PUBLICATION = "2023"; 
	private static final String WEBSITE = "/html/calendar_pea.html";
	private static final String SOURCE_LINK = "http://eck.cologne/peafactory/src/downloads/calendarPEA/src/calendar-lock-pea-1.7-src.zip"; 
	
	/**
	 * Files from initialization. These files must be added
	 * to the models after creation of PeaLockFrame
	 */
	private File[] initializedFiles = null;
	
	// to show errors and failed files only once: collect errors
	private StringBuilder errorsToShow = new StringBuilder();
	
	private boolean validCalendarFound = false;
	
	
	public CalendarPEAControl() {
		pswDialog = this;		
		setDialog(pswDialog);
		//dialogView = PswDialogView.getInstance();
		PeaControl.setPlainTextMode(true);
		//JREProperties.setMainWindow(dialogView);
		JREProperties.setMainWindow(PswDialogView.getInstance());
	}
	
	private static CalendarPEAControl getInstance() {
		CalendarPEAControl dialogInstance = null;
		if (pswDialog == null) {			
			dialogInstance = new CalendarPEAControl();
		} else {
			// return null
		}
		return dialogInstance;
	}
	
	public static void main(String[] args) {
		
		PeaProperties.setVersionInfo(PEA_NAME + " " + VERSION);
		PeaProperties.setFileType("calendar");
		
		boolean commandLineMode = handleMainCommands(args);

		if (commandLineMode == false) {
			PeaControl.initializeVariables();		
			SwingUtilities.invokeLater(new EventDispatchThread());		
		} else {
			TestLog.v(CalendarPEAControl.class, "Command line mode...");
		}
	}

	@Override
	public void preComputeInThread() {
		checkUpdates();
	}

	@Override
	public void clearSecretValues() {
		if (CipherStuff.getSessionKeyCrypt() != null) {
			CipherStuff.getSessionKeyCrypt().clearKeys();
		}
	}
	
	private void publish(String message, boolean isError) {
		if (PeaProperties.isCommandLineMode() == false) {
			PswDialogView.setMessage(message, isError, false);
		} else {
			if (isError == true) {
				TestLog.e(CalendarPEAControl.class, message);
			} else {
				TestLog.o(CalendarPEAControl.class, message);
			}
		}
	}
	
	/**
	 * Reset cursor, enable ok button and optionally
	 * update the view of the password dialog.
	 * This is called in the done method only.
	 * 
	 * @param returnToDialog	true, if the program should return to the 
	 * 							password dialog and check boxes were added or removed,
	 * 							eventually set salt and key are to be cleared
	 * 							(an error occurred)
	 */
	private void cleanUpOnReturn(boolean returnToDialog) {
		
		if (PeaProperties.isCommandLineMode() == true && returnToDialog == true) {
			TestLog.ve(CalendarPEAControl.class, "Decryption failed",5);
			if (errorsToShow != null && errorsToShow.length() > 0) {
				TestLog.e(CalendarPEAControl.class,  new String(errorsToShow));
			}
			TestLog.o(CalendarPEAControl.class, "Program exists...");
			System.exit(1);
		}
		// show collected error messages:
		if (errorsToShow != null && errorsToShow.length() > 0) {
			PeaDialog.showMessage(PswDialogView.getInstance(), errorsToShow, PeaProperties.getVmBridge().translate("error"), 0);
		}
		// reset errors:
		errorsToShow = new StringBuilder();		
		PswDialogView.getInstance().setCursor(CursorManager.getDefaultCursor());
		PswDialogView.setInitializing(false);
		if (returnToDialog == true) {
			PeaControl.setPlainTextMode(false);
			PswDialogView.enableOkButton();
			if (KeyFileDialogs.isNewChosenDefaultKeyFile() == true) {
				PeaProperties.setDefaultKeyFileName(null);
			}
			//KeyFileDialogs.resetProbablyNewDefaultKeyFileName();
			CipherStuff.getSessionKeyCrypt().clearKeys();
			// maybe files were removed...
			FilePanelHolder.updateFileView();
			PswDialogView.clearPassword();
			JREProperties.setMainWindow(PswDialogView.getInstance());
			PswDialogView.getInstance().pack();
		} else {
			PeaControl.setPlainTextMode(true);	
			JREProperties.setMainWindow(lockFrame);			
			// TODO TEST			
			AlarmControl.createService();
		}
	}

	@Override
	public void startDecryption() {

		String[] fileNames = null;
		validCalendarFound = false;

		publish(PeaProperties.getVmBridge().translate("decryption") + "...", false);
		if (PeaProperties.isCommandLineMode() == false) {
			PswDialogView.getInstance().setCursor(CursorManager.getWaitCursor());
		}

		if (PswDialogView.isInitializing() == false) {	

			if ( CipherStuff.getSessionKeyCrypt().isKeySet() == true) {
				TestLog.e(CalendarPEAControl.class, "Existing key before KDF...");
				cleanUpOnReturn(true);
				return;
			}
			if (PeaProperties.isCommandLineMode() == false) {
				if (FilePanelHolder.getFileNumber() == 0 &&  FilePanelHolder.getFileTypePanelList().size() == 0) {
					TestLog.e(CalendarPEAControl.class, "No file...");
					errorsToShow.append("\n" + PeaProperties.getVmBridge().translate("no_valid_file_found")
							+ "\n\n  -> " + CalendarSetting.calTranslate("file") + "  -> " + PeaProperties.getVmBridge().translate("init_new_file"));
					PswDialogView.setMessage(PeaProperties.getVmBridge().translate("open_or_initialize"), true, false);
					cleanUpOnReturn(true);
					return;
				}
			}
			if (KeyDerivation.getSalt() == null) {				
				TestLog.e(CalendarPEAControl.class, "Missing salt...");
				cleanUpOnReturn(true);
				return;
			}
			byte[] keyMaterial;
			// KDF: derive the key from password and salt: 
			publish(PeaProperties.getVmBridge().translate("key_derivation") + "... ", false);
			keyMaterial = getKeyMaterial();
			if (keyMaterial == null) {
				TestLog.e(CalendarPEAControl.class, "key derivation failed");
				if (PeaProperties.isTestMode()) {
					new UnexpectedValueException("keyMaterial", "byte[]", "is null").printDescription();
				}
				errorsToShow.append("\n" + PeaProperties.getVmBridge().translate("unexpected_error") + " (kdf)");					
				//PeaDialog.showMessage(dialogView, errorsToShow, PeaProperties.getOsBridge().translate("error"), 0);
				publish(PeaProperties.getVmBridge().translate("unexpected_error") + " (kdf)", true);
				//					PswDialogView.clearPassword();
				cleanUpOnReturn(true);
				return;
			} else {
				CipherStuff.getSessionKeyCrypt().storeKey(keyMaterial);
			}

			if (PeaProperties.isCommandLineMode() == false) {
				FileRecipient frw = new FileRecipient();
				fileNames = frw.getFileNames();

				// From FileRecetionWorker: collect to show later with other errors
				if (frw.getCollectedErrorsToShow() != null && frw.getCollectedErrorsToShow().length() > 0) {
					TestLog.ve(CalendarPEAControl.class, frw.getCollectedErrorsToShow());
					errorsToShow.append( frw.getCollectedErrorsToShow());
				}
				if (fileNames == null) {
					publish(PeaProperties.getVmBridge().translate("no_valid_file_found"), true);
					//errorsToShow.append("\n" + PeaProperties.getOsBridge().translate("no_valid_file_found") + " (kdf)");					
					//PeaDialog.showMessage(dialogView, errorsToShow, PeaProperties.getOsBridge().translate("error"), 0);
					cleanUpOnReturn(true);
					return;
				}
				HashMap<String, HashSet<String>> removeMap = frw.getProviderAndFilesToRemove();
				if (removeMap != null && removeMap.size() > 0) {
					Iterator<HashMap.Entry<String, HashSet<String>>> it = removeMap.entrySet().iterator();
					while (it.hasNext()) {
						HashMap.Entry<String, HashSet<String>> pair = it.next();
						HashSet<String> set = pair.getValue();
						for (String s : set) {
							//TestLog.o(ContactPEAControl.class, s);
							this.getEncryptedFileTypePanel().getCloudFilePanel().getCloudControl().removeInitialFileOfProvider(pair.getKey(), s);
							//			cpc.getEncryptedFileTypePanel().getCloudFilePanel().remove( pair.getKey(), s);
							// show as invalid file
							InvalidPanel.addInvalidFileCheckBox( pair.getKey() + "-" + s, PeaProperties.getVmBridge().translate("error") 
									+ " - " + (String) pair.getKey() + FileModel.getInvalidMarker());
						}
						//TestLog.o(ContactPEAControl.class, pair.getKey() + " = " + pair.getValue());
						it.remove(); // avoids a ConcurrentModificationException
					}
				}
			} else { // command line mode
				fileNames = CommandLineHandler.getFileNames();
			}

			//>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
			//>>>>>>>>> Checks and Preparations: 
			//>>>>>>>>>		return if no valid file found
			//>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>		

			// check the key against all files:
			// read cipher text from file and check pswIdentifier
			//int firstFileIndex = 0;
			boolean passowrdFittingFileFound = false;
			int len = fileNames.length;
			for (int i = 0; i < len; i++) {
				if(new File(fileNames[i]).isFile()) {
					//firstFileIndex = i;
					byte[] cipherText = ReadResources.readExternFile(fileNames[i]);
					if (cipherText == null) {
						if (PeaProperties.isTestMode()) {
							new UnexpectedValueException("cipherText", "byte[]", "is null for " + fileNames[i]).printDescription();
						} 		
						errorsToShow.append("\n" + fileNames[i] + PeaProperties.getVmBridge().translate("unexpected_error") + " (read ct)");					
						publish(PeaProperties.getVmBridge().translate("unexpected_error") + " (read ct)", true);
						continue;
					}
					// check password identifier
					String error = PeaControl.checkPassword(cipherText, CipherStuff.getSessionKeyCrypt().getKey());
					if (error != null) {
						// password failed: display error
						//			PswDialogView.clearPassword();
						TestLog.ve(CalendarPEAControl.class, error + " (password check)");
						publish(error, true);
						errorsToShow.append("\n" + fileNames[i] + "\n" + error + "\n(password check)");					
						//PeaDialog.showMessage(dialogView, errorsToShow, PeaProperties.getOsBridge().translate("error"), 0);
						continue;
					} else { // suitable file found
						passowrdFittingFileFound = true;
						break;
					}
				}
			}
			if (passowrdFittingFileFound == false) {
				TestLog.ve(CalendarPEAControl.class,"No valid file found");
				cleanUpOnReturn(true);
				return;
			}				
			TestLog.v(CalendarPEAControl.class,"Valid file found");
			PeaControl.setAvailableFileNames(new ArrayList<String>(Arrays.asList(fileNames)));

		} else { // initializing...
			if (PeaProperties.isCommandLineMode() == true) {
				TestLog.e(CalendarPEAControl.class,"Unexpected error...");
				TestLog.ve(CalendarPEAControl.class,"Initializing in command line mode...", 5);
				cleanUpOnReturn(true);
			}
			publish(PeaProperties.getVmBridge().translate("init_new_file") + "... ", false);

			// set the newest last modification now:
			//CalendarSetting.setLastModAtStart(System.currentTimeMillis());
			// set new salt to attach to file 
			byte[] lastSalt = null;
			// take salt from last selected FileTypePanel if there is one
			if (KeyDerivation.getSalt() != null && FilePanelHolder.getFileNumber() > 0) { 
				lastSalt = KeyDerivation.getSalt();
			} else {
				// get last salt from properties:
				byte[] propSalt = PeaProperties.getLastSalt();
				if (propSalt != null) {
					lastSalt = propSalt;
				}
			}
			if (lastSalt != null) {
				// ask
				int result = PeaDialog.showQuestion(
						//FileControl.dialogView, 
						PswDialogView.getView(),
						//		PeaProperties.getOsBridge().translate("add_new_files_when_decrypted") + "\n" +
						PeaProperties.getVmBridge().translate("open_files_together"), 
						PeaProperties.getVmBridge().translate("init_new_file"),
						0, 1);
				if (result == 0){ // yes  ok
					KeyDerivation.setSalt(lastSalt);
				} else {
					TestLog.v(CalendarPEAControl.class, "Create new salt");
					KeyDerivation.setSalt(
							new RandomStuff().createRandomBytes(KeyDerivation.getSaltSize()));
				}
			} else {
				// FilePanelHolder should created a salt... so this should not happen
				if (KeyDerivation.getSalt() == null) {
					TestLog.v(CalendarPEAControl.class, "Create new salt");
					KeyDerivation.setSalt(
							new RandomStuff().createRandomBytes(KeyDerivation.getSaltSize()));
				}
			}
			// derive key from password dialog:
			byte[] keyMaterial = getKeyMaterial();
			if (keyMaterial == null) {
				TestLog.e(CalendarPEAControl.class, "key derivation failed");
				TestLog.ve(CalendarPEAControl.class, "key derivation failed: keyMaterial is null", 5);
				errorsToShow.append("\n" + PeaProperties.getVmBridge().translate("unexpected_error") + " (kdf)");					
				//PeaDialog.showMessage(dialogView, errorsToShow, PeaProperties.getOsBridge().translate("error"), 0);
				publish(PeaProperties.getVmBridge().translate("unexpected_error") + " (kdf)", true);
				//					PswDialogView.clearPassword();
				cleanUpOnReturn(true);
				return;
			} 
		
			// Create new FileTypePanel and CloudFilePanel from default
			FileModel model = new FileModel(true, PeaProperties.getDefaultAlgoParamString().getBytes(AttachmentHandler.getASCIICharset()), 
					KeyDerivation.getSalt());
			FileComposer fc = new FileComposer(model);
			fc.setPlainModus(false);// encrypted files
			FileTypePanel ftp =  new FileTypePanel(
					350, 250, false, false, 
					fc, JREProperties.getMainWindow());	
			JREProperties.setTypePanel(ftp);
			
			// store the key to use later:
			CipherStuff.getSessionKeyCrypt().storeKey(keyMaterial);
			// salt and nonce are set, file was prepared

			fileNames = new String[PeaControl.getAvailableFileNames().size()];
			fileNames = PeaControl.getAvailableFileNames().toArray(fileNames);

			TestLog.o(CalendarPEAControl.class, "Initialization...");		
		}		// initialization

		// change background color
		PswDialogView.setUI(false);

		// lockFrame: 
		if (lockFrame == null) {
			new PropertyHandler().setSpecificPropValue();
			Dimension dim = new Dimension(JREProperties.getFrameDim()[0], JREProperties.getFrameDim()[1]);//CalendarSetting.getViewDimension();
			//			WeekPanel weekPanel = new WeekPanel(LocalDate.now(),dim);//new Dimension( dim.width - 90, dim.height - 150));
			//			LockFrameContact.setView(weekPanel);
			//lockFrame = LockFrameContact.getInstance(dialogView.getLocation(), dim);
			Point loc = new Point(100,100);
			if (PeaProperties.isCommandLineMode() == true) {
				loc = new Point(JREProperties.getFrameLocation()[0], JREProperties.getFrameLocation()[1]);
			} else {
				loc = PswDialogView.getView().getLocation();
			}
			setLockFrame(LockFrameCalendar.getInstance(loc, dim));
		}

		//>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
		//>>>>>>>>> Iterate through file names: 
		//>>>>>>>>>		decrpyt content
		//>>>>>>>>> 	check for duplicates and sort them out
		//>>>>>>>>>		add calendars to lockFrame

		// this list contains all successfully decrypted file names whether or not they are double
		// and wether or not they contain valid calendars
		HashMap<String, HashSet<PCalendar>> validFileWithCalendarsMap = new HashMap<String, HashSet<PCalendar>>();

		// collect the duplicates to set associated file names of duplicates later
		ArrayList<HashSet<String>> listOfDuplicateSets = new ArrayList<HashSet<String>>();
		checkDuplicateContent(fileNames, validFileWithCalendarsMap, listOfDuplicateSets);

		if (PswDialogView.isInitializing() == false) {
			if (validFileWithCalendarsMap.isEmpty()) { // no valid file
				publish(PeaProperties.getVmBridge().translate("no_valid_file_found"), true);
				errorsToShow.append("\n" + PeaProperties.getVmBridge().translate("no_valid_file_found"));					
				cleanUpOnReturn(true);
				return;
			}
			if (validCalendarFound == false) { // valid file, but no calendar
				publish(CalendarSetting.calTranslate("no_valid_calendar_found"), true);
				errorsToShow.append("\n" + PeaProperties.getVmBridge().translate("no_valid_file_found"));					
				cleanUpOnReturn(true);
				return;			
			}
		} 

		// loop to add duplicate file names
		String[] allCalendarNames = CalendarControl.getShownCalendarNameList();
		if (allCalendarNames == null || allCalendarNames.length == 0) {
			new UnexpectedValueException("String", "calStr", "is null").printDescription();
			publish(CalendarSetting.calTranslate("no_valid_calendar_found"), true);
			errorsToShow.append("\n" + PeaProperties.getVmBridge().translate("no_valid_file_found"));					
			cleanUpOnReturn(true);
			return;		
		}
		if (listOfDuplicateSets != null && listOfDuplicateSets.size() > 0) {
			for (String calendarName : allCalendarNames) { // check calendar by calendar...
				PCalendar c = CalendarControl.getCalendarByName(calendarName);
				String[] cFileNames = c.getAssociatedFileNames();
				for (String cFileName : cFileNames) { // check fileName by fileName if duplicates occurred
					for (HashSet<String> set : listOfDuplicateSets) {
						if (set.contains(cFileName)) { // duplicates occurred
							for (String setFileName : set) { // add all duplicate file names to the 3 models:
								// 1. associated file names: files where one calendar is stored
								// 2. fileName-calendar map: calendars stored in one file
								// 3. cloud.properties if file is in cloud
								// add file to calendar to save modifications:
								c.addAssociatedFileName(setFileName);
								CalendarControl.addToFileCalendarsMap(setFileName, c);
								// add to validFileMap with same calendars:
								if (validFileWithCalendarsMap.containsKey(setFileName)) {
									validFileWithCalendarsMap.get(setFileName).addAll(
											validFileWithCalendarsMap.get(cFileName));
								} else {
									validFileWithCalendarsMap.put(setFileName, validFileWithCalendarsMap.get(cFileName));
								}
								// add provider to CloudControl
								if (setFileName.contains(CloudControl.getTmpDir())) {
									String providerName = CloudControl.getProviderNameFromTmpFile(setFileName);
									String cloudFileName = CloudControl.getFileNameFromTmpFile(setFileName);

									String[] filesToAdd = {cloudFileName};
									String error = CloudPropertyHandler.addFileNamesToProperties(providerName, filesToAdd,
											Converter.bytes2hex(KeyDerivation.getSalt()), Converter.bytes2hex(PeaProperties.getAlgoParamString()));
									if (error != null) {
										TestLog.e(CalendarPEAControl.class, "adding files failed: " + error);
										TestLog.ve(CalendarPEAControl.class, "adding files failed: " + error,  5);
									}
								}
							}
						}
					}
				}
			}
		}

		if (PswDialogView.isInitializing() == false) {

			// set all files:
			CalendarControl.setFileCalendarsMap(validFileWithCalendarsMap);
			// set cloud files too
			Set<String> validFileSet = validFileWithCalendarsMap.keySet();
			Iterator<String> validFileSetIterator = validFileSet.iterator();
			while (validFileSetIterator.hasNext()) {
				String validFileName = validFileSetIterator.next();
				// add cloud files to model
				if (validFileName.contains(CloudControl.getTmpDir())) {
					this.getEncryptedFileTypePanel().getCloudFilePanel().getCloudControl().addUsedFileNameToProvider(
							CloudControl.getProviderNameFromTmpFile(validFileName), 
							CloudControl.getFileNameFromTmpFile(validFileName));
				}
			}
			publish(PeaProperties.getVmBridge().translate("number_of_files") + ": " + validFileSet.size(), false);
		}

		CalendarControl.refreshView();// SOLUTION????
		lockFrame.setVisible(true);// ANZEIGE-ERROR
		
		if (CalendarSetting.isShowTodos() == false) {
			LockFrameCalendar.getTodoView().collapseOrExpand();
		}
		JREProperties.setMainWindow(lockFrame);
		PeaControl.setPlainTextMode(true);
		if (CalendarControl.isCalendarsMerged() == true) {
			//LockFrameContact.setDocChangeUnsaved(true);
			LockFrameCalendar.getFrame().markUnsavedContentChanges();
		} else {
			//LockFrameContact.setDocChangeUnsaved(false);
			LockFrameCalendar.getFrame().markContentIsSaved();
		}
		// Add errors occurred in ReadCard:
		String unsupportedProps = ReadICal.getUnsupportedProps();
		boolean isUnsupportedPropertiesFound = false;
		if (unsupportedProps != null) {
			isUnsupportedPropertiesFound = true;
			PeaDialog.showMessage(this, unsupportedProps,
					CalendarSetting.calTranslate("unsupported_properties"), 2);// Warning
		}
		if (ReadICal.isInvalidFormatFound()) {
			if (isUnsupportedPropertiesFound == true) {
				errorsToShow.append("\n" + CalendarSetting.calTranslate("errors_occurred") 
						+ ": " +  CalendarSetting.calTranslate("unsupported_properties")
						+ ", " + CalendarSetting.calTranslate("invalid_format"));
			} else {
				errorsToShow.append("\n" + CalendarSetting.calTranslate("errors_occurred") 
						+ ": " + CalendarSetting.calTranslate("invalid_format"));
			}
		} else if (isUnsupportedPropertiesFound == true) {
			errorsToShow.append("\n" + CalendarSetting.calTranslate("errors_occurred") 
					+ ": " + CalendarSetting.calTranslate("unsupported_properties"));
		} else {
			lockFrame.setMessage(CalendarSetting.calTranslate("events_added") + ": " + ReadICal.getNumberOfEvents(), false);
		}

		EntropyPool.getInstance().stopCollection();
		if (errorsToShow != null && errorsToShow.length() > 0) {
			// show all collected errors
			if (PeaProperties.isCommandLineMode() == true) {
				TestLog.e(getClass(), new String(errorsToShow));
			} else {
				PeaDialog.showMessage(PswDialogView.getInstance(), errorsToShow, PeaProperties.getVmBridge().translate("error"), 0);
			}
			// reset errors
			errorsToShow = new StringBuilder();
		}
		// Add successfully added files to cloud.properties
		if (this.getEncryptedFileTypePanel() != null 
				&& this.getEncryptedFileTypePanel().getCloudFilePanel() != null
				&& this.getEncryptedFileTypePanel().getCloudFilePanel().getCloudControl() != null) {
			String[] providers = this.getEncryptedFileTypePanel().getCloudFilePanel().getCloudControl().getUsedProviders();

			if (providers != null && providers.length > 0) {
				int providersLen = providers.length;
				for (int i = 0; i < providersLen; i++) {

					String[] usedFiles = this.getEncryptedFileTypePanel().getCloudFilePanel().getCloudControl().getUsedFileNamesOfProvider(providers[i]);
					String error = CloudPropertyHandler.addFileNamesToProperties(providers[i], usedFiles,
							Converter.bytes2hex(KeyDerivation.getSalt()), Converter.bytes2hex(PeaProperties.getAlgoParamString()));
					if (error != null) {
						TestLog.e(CalendarPEAControl.class, "adding files failed: " + error);
						TestLog.ve(CalendarPEAControl.class, "adding files failed: " + error, 5);
					}
				}
			}
		}
		if (PeaProperties.isCommandLineMode() == false) {
			PswDialogView.getView().setVisible(false);		
		}
		cleanUpOnReturn(false);

		StringBuilder toastBuilder = new StringBuilder();
		for (HashSet<String> set : listOfDuplicateSets) {
			if (set.size() > 1) {
				toastBuilder.append(PeaProperties.getVmBridge().translate("synchronized" ) + ": \n");
				for (String s : set) {
					toastBuilder.append("   " + s + "\n");
				}
				toastBuilder.append("\n");
			}
		}
		if (toastBuilder.length() > 1) { // show synchronized files
			Toaster.toast(JREProperties.getMainWindow(), new String(toastBuilder), 3);
		}
		if (PswDialogView.isInitializing() == true) {
			// otherwise an empty new calendar would not be saved:
			String error = ((CalendarMenuBar)((JPanel)(LockFrameCalendar.getFrame().getContentPane()).getComponent(0)).getComponent(0)).saveAllCalendars();
			if (error != null) {
				TestLog.ve(CalendarPEAControl.class, "save file failed: " + error, 5);
			} else {
				// add initialized files to models
				if (initializedFiles != null) {
					AttachmentHandler.distributeFiles(initializedFiles);
				}
			}
		}
	}
	
	private static synchronized void setLockFrame(LockFrameCalendar newLockFrame) {
		CalendarPEAControl.lockFrame = newLockFrame;
		if (JREProperties.getMainWindow() instanceof PeaLockFrame) {
			JREProperties.setMainWindow(CalendarPEAControl.lockFrame);
		}
	}
	
	/**
	 * Check the list of file names for duplicates:
	 * check 1. file size, 2. file name,3. checksum
	 * 
	 * @param fileNames				the file names to check
	 * @param decryptedFileWithCalendarsMap		map of successfully decrypted files (whether or not duplicate and contain calendars)  
	 * 								with calendars in that file
	 * @param listOfDuplicateSets	a list of sets of duplicate file names (each set contains file names that are duplicate)
	 */
	private void checkDuplicateContent(	String[] fileNames, 
			HashMap<String, HashSet<PCalendar>> decryptedFileWithCalendarsMap, 
			ArrayList<HashSet<String>> listOfDuplicateSets) {
		
		// iterate over file names: 
		int len = fileNames.length;
		byte[] keyMaterial = null;//CipherStuff.getSessionKeyCrypt().getKey();

		// check for duplicate contents: 
		// key: the file size, 
		//	value: entry with index in fileNames and value checksum of file
		HashMap<Long,  ArrayList<Entry <Integer, Long>>> sizeChecksumMap = new HashMap<Long,  ArrayList<Entry <Integer, Long>>>();

		CRC32 crc = new CRC32();
		long newestLastMod = 0;
		
		for (int i = 0; i < len; i++) {

			File file = new File(fileNames[i]);
			//TestLog.o(ContactPEAControl.class, "*** check: "+fileNames[i] + " size: " + file.length());
			if (file.isFile()) { // there may be directories
				// keyMaterial is cleared after decryption and stored as session key:
				keyMaterial = CipherStuff.getSessionKeyCrypt().getKey();
				Comparator.checkNullVector(keyMaterial);
				// import each calendar
				byte[] plainText = CipherStuff.getInstance().decrypt(
						ReadResources.readExternFile(fileNames[i]), 
						keyMaterial, 
						true);			// TODO create crc-32 inside decryption
				if (plainText == null) {
					TestLog.e(CalendarPEAControl.class, "Could not decrypt file " + fileNames[i]);
					TestLog.e(CalendarPEAControl.class, CipherStuff.getErrorMessage());
					if (fileNames[i].startsWith(System.getProperty("java.io.tmpdir") )) {
						// remove and add to invalidPanel 
						String provider = CloudControl.getProviderNameFromTmpFile(fileNames[i]);
						String invalidFileName = CloudControl.getFileNameFromTmpFile(fileNames[i]);
						this.getEncryptedFileTypePanel().getCloudFilePanel().getCloudControl().removeInitialFileOfProvider(provider, invalidFileName);
					//	cpc.getEncryptedFileTypePanel().getCloudFilePanel().remove(provider, invalidFileName);
					}
					// collect the failed files to show later
					errorsToShow.append("\n" + PeaProperties.getVmBridge().translate("decryption_failed") + ": " + fileNames[i]);					
					continue;
				} else {
					decryptedFileWithCalendarsMap.put(fileNames[i], new HashSet<PCalendar>());// empty list
					// set newest last modification if newer
					// cloud files' value was updated to last modification in cloud
					if (file.lastModified() > newestLastMod) {
						//TestLog.o(ContactPEAControl.class, fileNames[i] + " lastMod: " + file.lastModified() + " - " + "newest: " + newestLastMod);
						newestLastMod = file.lastModified();
					}
				}
				long fileSize = file.length(); // get file length
				crc.update(plainText);
				long  checksum1 = crc.getValue(); // get crc checksum
				crc.reset();
				// 1. check file size
				if (sizeChecksumMap.containsKey(fileSize)) {
					//TestLog.o(ContactPEAControl.class, "same size exists: "+fileNames[i] + "  "+ fileSize);
					// 2. check filenames:
					String fileName1 = new File(fileNames[i]).getName();//fileNames[i].substring(fileNames[i].lastIndexOf(File.separator) + 1);
					// get all previously stored different entries for this file size (index in fileNames, checksum)
					ArrayList<Entry<Integer, Long>> entries = sizeChecksumMap.get(fileSize);
					//TestLog.o(ContactPEAControl.class, entries.size() +" file names with same size stored "+fileSize + "  "+ entries.get(0).getKey());
					String identicFileName = null;// if there is no, remains null
					// loop for already stored files:
					for (Entry<Integer, Long> entry : entries) {
						// check one entry from already stored list for this file size
						String fileName2withPath = fileNames[entry.getKey()];	
						// get file name only:
						String fileName2 =  new File(fileName2withPath).getName();//fileNames[j].substring(fileNames[j].lastIndexOf(File.separator) + 1);
						//TestLog.o(ContactPEAControl.class, "Compare file names: "+fileName1 + " " + fileName2 + ", index: " + entry.getKey());
						// check if file names are equal
						if (fileName1.equals(fileName2)) {
							//TestLog.o(ContactPEAControl.class, "file name equals");
							// check checksum
							// get checksum from previous file
							long checksum2 = entry.getValue();
							//TestLog.o(ContactPEAControl.class, "Compare crc: " + checksum2 + "  " + checksum);
							if (checksum1 == checksum2 ) {
								//TestLog.o(ContactPEAControl.class, "crc euqal");
								identicFileName = fileName2withPath;
								publish(PeaProperties.getVmBridge().translate("duplicate") + ": " + fileName1, false);
								//TestLog.o(ContactPEAControl.class, "Duplicate checksum: " + fileName2 + " = " + fileName1);
								// no need for further checks
								break;											
							} else {
								// do nothing
								//TestLog.o(ContactPEAControl.class, "Different checksum");
							}
						} else {// file size equal, but not file name
							//TestLog.o(ContactPEAControl.class, "Different file name");
							// do nothing
						}
					} // end loop for already stored files

					if (identicFileName != null) { // identical file name and content found

						TestLog.o(CalendarPEAControl.class, "Identical content sorted out: " + fileNames[i] + " - is identical to " + identicFileName);
						// check if identicFileName is already in duplicateSets
						boolean alreadyInSets = false;
						for (HashSet<String> set: listOfDuplicateSets) {
							  if (set.contains(identicFileName)) {
								  // add to set
								  set.add(fileNames[i]);
								  //TestLog.o(ContactPEAControl.class, "Add to set: " + fileNames[i] + ", index: " + i);
								  alreadyInSets = true;
								  break;
							  }
						}
						if (alreadyInSets == false) { // add new set
							HashSet<String> set = new HashSet<String>();
							set.add(fileNames[i]);
							set.add(identicFileName);
							listOfDuplicateSets.add(set);
						}
						// check the next file, do not read the content e.g. Calendars
						continue;
					} else { // same size, but different file name or checksum
						// add index of file name to file size
						//TestLog.o(ContactPEAControl.class, "add new index to file size "+fileSize + " - "+fileNames[i] + ", index: " + i);
						Entry<Integer, Long> e = new AbstractMap.SimpleEntry<Integer, Long>(i, checksum1);
						entries.add(e);
						sizeChecksumMap.put(fileSize, entries);// old value is replaced
					}

				} else { // new file size: create new ArrayList<Entry>
					//TestLog.o(ContactPEAControl.class, "New size occurred: " + fileSize + " in " + fileNames[i] + ", index: " + i);
					ArrayList<Entry<Integer, Long>> newListEntry = new ArrayList<Entry<Integer, Long>>();
					Entry<Integer, Long> e = new AbstractMap.SimpleEntry<Integer, Long>(i, checksum1);
					newListEntry.add(e);
					sizeChecksumMap.put(fileSize, newListEntry);
				}

				//Now read calendars from the file that is not duplicate and no error occurred
				PCalendar[] cals = new ReadICal().readCalendarsFromBytes(plainText, fileNames[i], true);

				if (cals != null && cals.length > 0) {
					decryptedFileWithCalendarsMap.get(fileNames[i]).addAll(new ArrayList<>(Arrays.asList(cals)));
					validCalendarFound = true;
					CalendarControl.addShownCalendarList(new ArrayList<>(Arrays.asList(cals)), fileNames[i]);	
					CalendarControl.addToFileCalendarsMap(fileNames[i], cals);
					// add provider to CloudControl
					if (fileNames[i].contains(CloudControl.getTmpDir())) {
						String providerName = CloudControl.getProviderNameFromTmpFile(fileNames[i]);
						String propFileName = CloudControl.getFileNameFromTmpFile(fileNames[i]);

						String[] filesToAdd = {propFileName};
						String error = CloudPropertyHandler.addFileNamesToProperties(providerName, filesToAdd,
								Converter.bytes2hex(KeyDerivation.getSalt()), Converter.bytes2hex(PeaProperties.getAlgoParamString()));
						if (error != null) {
							TestLog.e(CalendarPEAControl.class, "adding files failed: " + error);
							TestLog.ve(CalendarPEAControl.class, "adding files failed: " + error,  5);
						}
					}

				} else {
					TestLog.e(CalendarPEAControl.class, "No calendars found in " + fileNames[i]);
					errorsToShow.append("\n" + CalendarSetting.calTranslate("no_valid_calendar_found") + ": " + fileNames[i]);					
				}
			} else {
				TestLog.e(CalendarPEAControl.class, "Is not a file: " + fileNames[i]);
			}
		} // end for
	}
	
	/**
	 * Choose the file name to store the calendar - set the associated file names.
	 * The Calendar must be initialized with color and name. The selected file name
	 * is set as associated file name and in file-calendar map
	 * 
	 * @param newCalendar	the initialized PCard
	 * @param owner			the owner to show messages
	 * 
	 * @return	the file name to store the calendar or null
	 */
	public static String[] setFileNameforCalendar(PCalendar newCalendar, Component owner) {
		
		String newCalendarName = newCalendar.getName();
		// check if one single file name is used, 
		// then ask to store calendar in this or in extra file
		boolean askForExtraFile = true;
		
		String[] openedFiles = CalendarControl.getAllAssociatedFileNames();
		int openedFilesLen = openedFiles.length;
		if (openedFilesLen == 0) {
			askForExtraFile = false;
		} else if (openedFilesLen <= 1) {
			askForExtraFile = true;
		} else if (openedFilesLen <= 5) { // maybe several versions in cloud and local
			// check if all openedFiles are associated with all opened calendars
			HashSet<PCalendar> hs = null;
			for (int i = 0; i < openedFilesLen; i++) {
				if (hs != null) {
					if (! hs.equals(CalendarControl.getCalendarsOfFileAsSet(openedFiles[i]))) {
						askForExtraFile = false;
						break;
					}
				} else {
					hs = CalendarControl.getCalendarsOfFileAsSet(openedFiles[i]);
				}
			}
		} else { // more than 5 files
			askForExtraFile = false;
		}
		
		// depending on askForExtraFile: select file name
		if (askForExtraFile == true) {
			String[] options = {
					CalendarSetting.calTranslate("existing_file") 
						+ " (" + CalendarSetting.calTranslate("for_all_calendars") + ")", 
						CalendarSetting.calTranslate("extra_file")};

			int result = PeaDialog.showOptions(owner, 
					PeaProperties.getVmBridge().translate("where_to_save"), CalendarSetting.calTranslate("new"), 1, 3, options, 0);
			if (result == 0) { // save in all existing file names
				// for all opened files (all same version): add calendar to file
				for (int i = 0; i < openedFilesLen; i++) {
					if (openedFiles[i] == null) {
						continue;
					}
					newCalendar.addAssociatedFileName(openedFiles[i]);
					CalendarControl.addToFileCalendarsMap(openedFiles[i], newCalendar);
				}
				return openedFiles;
			} else if (result == 1) { // extra file
				// open file chooser to select a directory to store file
				PeaFileChooser pfc = new PeaFileChooser(owner);
				pfc.customizeFileChooser(CalendarSetting.calTranslate("save"), 
						CalendarSetting.calTranslate("choose_default_location"), false);
				pfc.setApproveButtonText(CalendarSetting.calTranslate("select"));
				pfc.setMultiSelectionEnabled(false);
				pfc.setFileSelectionMode(PeaFileChooser.FILES_ONLY);
				// set default directory if present:
				String defaultDir = CalendarSetting.getDefaultFolder();
				if (defaultDir != null) {
					// suggest default path and calendar name
					pfc.setSelectedFile(new File(defaultDir + File.separator + newCalendarName + ".ics"));
				} else {
					// only suggest name of calendar as file name, but allow other names		
					pfc.setSelectedFile(new File(System.getProperty("user.home") + File.separator + newCalendarName + ".ics"));
				}
				int chooserResult = pfc.showOpenDialog(owner);
				if (chooserResult != PeaFileChooser.APPROVE_OPTION) {
					return null;
				}
				// get selected file
				File selectedFile = pfc.getSelectedFile();

				if (selectedFile != null) {
					// check if file exists and ask to overwrite
					if (FileTools.askToOverwriteFile(selectedFile) == false) {
						// already exists and should not be overwritten
						return null;
					}
					// create empty file:
					//selectedFile.getParentFile().mkdirs(); 
					try {
						selectedFile.createNewFile();
					} catch (IOException e) {
						// show all collected errors
						if (PeaProperties.isCommandLineMode() == true) {
							TestLog.e(CalendarPEAControl.class, e.getLocalizedMessage());
						} else {
							PeaDialog.showMessage(PswDialogView.getView(), 
									PeaProperties.getVmBridge().translate("unexpected_error") + "\n"
											+ e.getLocalizedMessage(),
											PeaProperties.getVmBridge().translate("error"),  0);
							TestLog.e(CalendarPEAControl.class, "ContactPEAControl " + new ExceptionHelper(e).getInfos());
						}
						return null;
					}
					newCalendar.addAssociatedFileName(selectedFile.getAbsolutePath());
					CalendarControl.addToFileCalendarsMap(selectedFile.getAbsolutePath(), newCalendar);
					//CalendarControl.addAssociatedFileName(directory + File.separator + newCalendarName);
					String[] returnValue = {selectedFile.getAbsolutePath()};
					return returnValue;
				} else {
					return null;
				}
			} else { // cancel
				return null;
			}
		} else { // extra file without confirmation
			// open file chooser to select a directory to store file
			PeaFileChooser pfc = new PeaFileChooser(owner);
			pfc.customizeFileChooser(CalendarSetting.calTranslate("save"), 
					CalendarSetting.calTranslate("choose_default_location"), false);
			pfc.setApproveButtonText(CalendarSetting.calTranslate("select"));
			pfc.setMultiSelectionEnabled(false);
			pfc.setFileSelectionMode(PeaFileChooser.FILES_ONLY);
			String defaultDir = CalendarSetting.getDefaultFolder();
			if (defaultDir != null) {
				// suggest default path and calendar name
				pfc.setSelectedFile(new File(defaultDir + File.separator + newCalendarName + ".ics"));
			} else {
				// only suggest name of calendar as file name, but allow other names		
				pfc.setSelectedFile(new File(System.getProperty("user.home") + File.separator + newCalendarName + ".ics"));
			}
			pfc.showOpenDialog(owner);
			// get selected directory
			File selectedFile = pfc.getSelectedFile();
			if (selectedFile != null) {
				// check if file exists and ask to overwrite
				if (FileTools.askToOverwriteFile(selectedFile) == false) {
					// already exists and should not be overwritten
					return null;
				}
				// create empty file:
				//selectedFile.getParentFile().mkdirs(); 
				try {
					selectedFile.createNewFile();
				} catch (IOException e) {
					if (PeaProperties.isCommandLineMode() == true) {
						TestLog.e(CalendarPEAControl.class, e.getLocalizedMessage());
					} else {
						PeaDialog.showMessage(PswDialogView.getView(), 
								PeaProperties.getVmBridge().translate("unexpected_error") + "\n"
										+ e.getLocalizedMessage(),
										PeaProperties.getVmBridge().translate("error"),  0);
						TestLog.e(CalendarPEAControl.class, "ContactPEAControl " + new ExceptionHelper(e).getInfos());
					}
					return null;
				}
				newCalendar.addAssociatedFileName(selectedFile.getAbsolutePath());
				CalendarControl.addToFileCalendarsMap(selectedFile.getAbsolutePath(), newCalendar);
				String[] returnValue = {selectedFile.getAbsolutePath()};
				// check success:
				if (newCalendar.getAssociatedFileNames() == null 
						|| newCalendar.getAssociatedFileNames().length ==0) {
					new UnexpectedValueException("String[]", "associatedFileNames", "is null for calendar "+ newCalendar.getName()).printDescription();
				}
				if (  CalendarControl.getCalendarsOfFile(selectedFile.getAbsolutePath()) ==  null
						|| CalendarControl.getCalendarsOfFile(selectedFile.getAbsolutePath()).length == 0 ) {
					new UnexpectedValueException("PCard[]", "calendarsOfFile", "is null for file "+ selectedFile.getAbsolutePath()).printDescription();
				}
				TestLog.o(CalendarPEAControl.class, "set " + selectedFile.getAbsolutePath() + " - " + newCalendar.getName());
				return returnValue;
			} else {
				return null;
			}
		}
	}

	@Override
	public String initializeNewFiles() {
		
		String algoParamString = null;
		if (JREProperties.getMainWindow() instanceof PswDialogView) {
			// get default values from properties
			algoParamString = PeaProperties.getDefaultAlgoParamString();
			TestLog.v(CalendarPEAControl.class, "Default algorithm-parameter string: " + algoParamString);
		} else if (JREProperties.getMainWindow() instanceof PeaLockFrame) {
			// get current values from properties
			algoParamString = new String(PeaProperties.getAlgoParamString(), AttachmentHandler.getASCIICharset());
			TestLog.v(CalendarPEAControl.class, "Current used algorithm-parameter string: " + algoParamString);

		}
		if (algoParamString != null) {
			// set to use these settings
			AlgoParamHandler.readString(algoParamString, false);
			// set algo parameter string to use
			PeaProperties.setAlgoParamString(algoParamString.getBytes(AttachmentHandler.getASCIICharset()));//AlgoParamHandler.writeString().getBytes(AttachmentHandler.getASCIICharset()));			
		}
		PCalendar newCalendar = null;
		Window owner = JREProperties.getMainWindow();
		if (owner == null || owner.isShowing() == false) {
			if (PswDialogView.getView().isShowing() == true) {
				owner = PswDialogView.getView();
			} else if (this.getLockFrame() != null 
					&& this.getLockFrame() instanceof PeaLockFrame 
					&& ((PeaLockFrame)this.getLockFrame()).isShowing() == true) {
				owner = (PeaLockFrame)this.getLockFrame();
			}
		}
		String newCalendarName = JOptionPane.showInputDialog(owner, 
				CalendarSetting.calTranslate("type_name"), 
				CalendarSetting.calTranslate("new"), -1);
		if (newCalendarName == null) {
			return null;//PeaProperties.getVmBridge().translate("unexpected_error");
		}
		if (newCalendarName.equals("")) {
			newCalendarName = "default";
		}
		// Replace problematic characters
		newCalendarName	= newCalendarName
				.replaceAll("<", "_")
				.replaceAll("&", "_")
				.replaceAll(";", "_");

		Color color = JColorChooser.showDialog(owner,
				CalendarSetting.calTranslate("choose_calendar_color"), 
				Color.CYAN);
		
		String newFileName = null;
		
		// check if one single file name is used, 
		// then ask to store calendar in this or in extra file
		boolean askForExtraFile = true;
		String[] openedFiles = CalendarControl.getAllAssociatedFileNames();
		List<String> openedList = Arrays.asList( openedFiles );
		//HashSet<String> openedSet = new HashSet<String>( Arrays.asList( openedFiles ) );

		int openedFilesLen = openedFiles.length;
		if (openedFilesLen == 0) {
			askForExtraFile = false;
		} else if (openedFilesLen <= 1) {
			askForExtraFile = true;
		} else if (openedFilesLen <= 5) { // maybe several versions in cloud and local
			// check if all openedFiles are associated with all opened calendars
			HashSet<PCalendar> hs = null;
			for (int i = 0; i < openedFilesLen; i++) {
				if (hs != null) {
					if (! hs.equals(CalendarControl.getCalendarsOfFileAsSet(openedFiles[i]))) {
						askForExtraFile = false;
						break;
					}
				} else {
					hs = CalendarControl.getCalendarsOfFileAsSet(openedFiles[i]);
				}
			}
		} else { // more than 5 files
			askForExtraFile = false;
		}
		if (askForExtraFile == true) {
			String[] options = {
					CalendarSetting.calTranslate("existing_file") 
						+ " (" + CalendarSetting.calTranslate("for_all_calendars") + ")", 
						CalendarSetting.calTranslate("extra_file")};

			int result = PeaDialog.showOptions(owner, 
					PeaProperties.getVmBridge().translate("where_to_save"), CalendarSetting.calTranslate("new"), 1, 3, options, 0);
			if (result == 0) {
				HashSet<String> list = new HashSet<String>();
				list.addAll(openedList);
				newCalendar = new PCalendar(newCalendarName, "", color, list);
				// for all opened files (all same version): add calendar to file
				for (int i = 0; i < openedFilesLen; i++) {
					CalendarControl.addToFileCalendarsMap(openedFiles[i], newCalendar);
				}
				newFileName = openedFiles[0];
			} else if (result == 1) { // extra file
				// open file chooser to select a directory to store file
				PeaFileChooser pfc = new PeaFileChooser(owner);
				pfc.customizeFileChooser(CalendarSetting.calTranslate("save"), 
						CalendarSetting.calTranslate("choose_default_location"), false);
				pfc.setApproveButtonText(CalendarSetting.calTranslate("select"));
				pfc.setMultiSelectionEnabled(false);
				pfc.setFileSelectionMode(PeaFileChooser.FILES_ONLY);
				// set default directory if present:
				String defaultDir = CalendarSetting.getDefaultFolder();
				if (defaultDir != null) {
					// suggest default path and calendar name
					pfc.setSelectedFile(new File(defaultDir + File.separator + newCalendarName + ".ics"));
				} else {
					// only suggest name of calendar as file name, but allow other names		
					pfc.setSelectedFile(new File(System.getProperty("user.home") + File.separator + newCalendarName + ".ics"));
				}
				pfc.showOpenDialog(owner);
				// get selected file
				File selectedFile = pfc.getSelectedFile();

				if (selectedFile != null) {
					// check if file exists and ask to overwrite
					if (FileTools.askToOverwriteFile(selectedFile) == false) {
						// already exists and should not be overwritten
						return null;
					}
					// create empty file:
					//selectedFile.getParentFile().mkdirs(); 
					try {
						selectedFile.createNewFile();
					} catch (IOException e) {
						PeaDialog.showMessage(PswDialogView.getView(), 
								PeaProperties.getVmBridge().translate("unexpected_error") + "\n"
								+ e.getLocalizedMessage(),
								PeaProperties.getVmBridge().translate("error"),  0);
						TestLog.e(CalendarPEAControl.class, "ContactPEAControl " + new ExceptionHelper(e).getInfos());
						return PeaProperties.getVmBridge().translate("unexpected_error");
					}
					HashSet<String> list = new HashSet<String>();
					list.add(selectedFile.getAbsolutePath());// + File.separator + newCalendarName);
					newCalendar = new PCalendar(newCalendarName, "", color, list);
					CalendarControl.addToFileCalendarsMap(selectedFile.getAbsolutePath(), newCalendar);
					//CalendarControl.addAssociatedFileName(directory + File.separator + newCalendarName);
					newFileName = selectedFile.getAbsolutePath();
				}
			} else { // cancel
				return null;
			}
		} else { // extra file without confirmation
			// open file chooser to select a directory to store file
			PeaFileChooser pfc = new PeaFileChooser(owner);
			pfc.customizeFileChooser(CalendarSetting.calTranslate("save"), 
					CalendarSetting.calTranslate("choose_default_location"), false);
			pfc.setApproveButtonText(CalendarSetting.calTranslate("select"));
			pfc.setMultiSelectionEnabled(false);
			pfc.setFileSelectionMode(PeaFileChooser.FILES_ONLY);
			String defaultDir = CalendarSetting.getDefaultFolder();
			if (defaultDir != null) {
				// suggest default path and calendar name
				pfc.setSelectedFile(new File(defaultDir + File.separator + newCalendarName + ".ics"));
			} else {
				// only suggest name of calendar as file name, but allow other names		
				pfc.setSelectedFile(new File(System.getProperty("user.home") + File.separator + newCalendarName + ".ics"));
			}
			pfc.showOpenDialog(owner);
			// get selected directory
			File selectedFile = pfc.getSelectedFile();
			if (selectedFile != null) {
				// check if file exists and ask to overwrite
				if (FileTools.askToOverwriteFile(selectedFile) == false) {
					// already exists and should not be overwritten
					return null;
				}
				// create empty file:
				try {
					selectedFile.createNewFile();
				} catch (IOException e) {
					PeaDialog.showMessage(PswDialogView.getView(), 
							PeaProperties.getVmBridge().translate("unexpected_error") + "\n"
							+ e.getLocalizedMessage(),
							PeaProperties.getVmBridge().translate("error"),  0);
					TestLog.e(CalendarPEAControl.class, "ContactPEAControl " + new ExceptionHelper(e).getInfos());
					return PeaProperties.getVmBridge().translate("unexpected_error");
				}
				if (CalendarSetting.getDefaultFolder() == null) {
					CalendarSetting.setDefaultFolder(selectedFile.getParent());
				}
				HashSet<String> list = new HashSet<String>();
				list.add(selectedFile.getAbsolutePath());// + File.separator + newCalendarName);
				newCalendar = new PCalendar(newCalendarName, "", color, list);
				CalendarControl.addToFileCalendarsMap(selectedFile.getAbsolutePath(), newCalendar);
				newFileName = selectedFile.getAbsolutePath();
			}
		}
		if (newCalendar != null) {
			CalendarControl.addShownCalendar(newCalendar, newFileName);	// fileName only required for check duplicates			
		} else {
			new UnexpectedValueException("PCard", "newCalendar", "is null").printDescription();
			return PeaProperties.getVmBridge().translate("unexpected_error");
		}
		if (newFileName != null) {
			File[] ifn = { new File(newFileName)};
			initializedFiles = ifn;
		}
		return null;
	}

	@Override
	public void openEncryptedFiles() {

		TestLog.v(CalendarPEAControl.class, "Open files...");
		boolean fileAdded = AttachmentHandler.addEncryptedFiles(JREProperties.getMainWindow(), 
				PeaProperties.getVmBridge().translate("open_encrypted_file"), 
				true, false);// hidden files, do not check listed files

		if (fileAdded == false) { // no file was added
			TestLog.e(CalendarPEAControl.class, "no file was added");
			return;
		}		
		boolean validFileFound = FilePanelHolder.getSelectedModel().checkForValidFile();
		if (validFileFound == false) {
			//PswDialogView.setMessage(PeaProperties.getOsBridge().translate("no_valid_file_found"), true);
			PswDialogView.setMessage(PeaProperties.getVmBridge().translate("open_or_initialize"), true, false);
		} else {
			// clear errorMessageLabel
			PswDialogView.setMessage("   ", true, false);
		}		
		
	}

	@Override
	public String[] getSelectedFileNames() {		
		return FilePanelHolder.getSelectedPanel().getValidLocalFileNames(true, false);
	}

	@Override
	public String getVersion() {
		return VERSION;
	}

	@Override
	public String getYearOfPublication() {
		return YEAR_OF_PUBLICATION;
	}

	@Override
	public String getWebsite() {
		return WEBSITE;
	}

	@Override
	public String getSourceLink() {
		return SOURCE_LINK;
	}

	@Override
	public synchronized Object getLockFrame() {
		return lockFrame;
	}
	
	@Override
	public synchronized void setLockFrame(PeaLockFrame _lockFrame) {
		if (_lockFrame instanceof LockFrameCalendar) {
			if (CalendarPEAControl.lockFrame != null && CalendarPEAControl.lockFrame.isShowing()) {
				CalendarPEAControl.lockFrame.setVisible(false);
			}
			CalendarPEAControl.lockFrame = (LockFrameCalendar) _lockFrame;
		} else {
			TestLog.e(CalendarPEAControl.class, "unexpected error");
			TestLog.e(CalendarPEAControl.class, "Invalid frame", 5);
		}		
	}
	
	@Override
	public boolean checkFormat(File file, boolean easyCheck, boolean middleCheck, boolean expensiveCheck) {
		String fileName = file.getAbsolutePath();
		if (easyCheck == true) {
			String extension = "";
			int i = fileName.lastIndexOf('.');
			if (i > 0) {
			    extension = fileName.substring(i+1);
			}
			if (! extension.toUpperCase().equalsIgnoreCase("ICS")) {
		    	if (PeaProperties.isTestMode()) {
					TestLog.e(CalendarPEAControl.class, 
							CalendarSetting.calTranslate("unsuitable_ics_file")
							//PeaProperties.getVmBridge().translate("unsuitable_ics_file") 
							+ ": "  + fileName); 
					TestLog.e(CalendarPEAControl.class, "File extension is not .ics");

		    	}
				return false;
			}
		}
		if (middleCheck == true) { // TODO test
			// check if file content starts with BEGIN:VCALENDAR
			BufferedReader brTest;
			String firstLine = "";
			try {
				brTest = new BufferedReader(new FileReader(file));
				firstLine = brTest.readLine();
			} catch (FileNotFoundException e) {
				TestLog.e(CalendarPEAControl.class, "ContactPEAControl " + new ExceptionHelper(e).getInfos());
			} catch (IOException e) {
				TestLog.e(CalendarPEAControl.class, "ContactPEAControl " + new ExceptionHelper(e).getInfos());
			}
			if (firstLine == null) {
				TestLog.e(CalendarPEAControl.class, "No line in file " + file.getAbsolutePath());
				return false;
			}
		    if (! firstLine.toUpperCase().contains("BEGIN:VCALENDAR")) {
		    	if (PeaProperties.isTestMode()) {
		    		TestLog.e(CalendarPEAControl.class, 
		    				CalendarSetting.calTranslate("unsuitable_ics_file")
						//PeaProperties.getVmBridge().translate("unsuitable_ics_file") 
						+ ": "  + fileName); 
		    		TestLog.e(CalendarPEAControl.class, "Unsuitable control sequenz: Could not find sequenz BEGIN:VCALENDAR in first line");
		    	}
		    	return false;
		    }
		}
		if (expensiveCheck == true) {
			String begin = "BEGIN:VCALENDAR";
			boolean containsBegin = false;
			String version = "VERSION";
			boolean containsVersion = false;
			String prodID = "PRODID";
			boolean containsProdID = false;
			String end = "END:VCALENDAR";
			boolean containsEnd = false;
			
			// check if file content starts with {\rtf1 in the first line
			BufferedReader brTest = null;
			String line = "";
			try {
				brTest = new BufferedReader(new FileReader(file));
				// read all lines and check if parameter exists
			    while ((line = brTest.readLine()) != null) {
			      if (line.toUpperCase().startsWith(begin)) {
			    	  containsBegin = true;
			      } else if (line.toUpperCase().startsWith(version)) {
			    	  containsVersion = true;
			      } else if (line.toUpperCase().startsWith(prodID)) {
			    	  containsProdID = true;
			      } else if (line.toUpperCase().startsWith(end)) {
			    	  containsEnd = true;
			      }
			    }
			} catch (FileNotFoundException e) {
		    	if (PeaProperties.isTestMode()) {
		    		new UnexpectedValueException("file", "File", e.toString() + ", " + e.getMessage()).printStackTraceLines();
		    	}
			} catch (IOException e) {
		    	if (PeaProperties.isTestMode()) {
		    		new UnexpectedValueException("file", "File", e.toString() + ", " + e.getMessage()).printStackTraceLines();
		    	}
			}
		    if (! (containsBegin == true && containsVersion == true 
		    		&& containsProdID == true && containsEnd == true)) {
		    	if (PeaProperties.isTestMode()) {
		    		TestLog.e(CalendarPEAControl.class, "Unsuitable file: Couln't find one of the required parameters: begin, version, prodID, end");
		    	}
		    	return false;
		    }
		}
		return true;
	}

	@Override
	protected byte[] getPlainBytes() {
		throw new IllegalArgumentException("function not implemented");
	}

	/**
	 * Set all properties for this specific PEA from property file
	 * 
	 * @param prop	properties for this specific PEA
	 * @param inputStream 	the InputStream to read the properties
	 * 
	 * @return	errorMessage or null
	 */
	@Override
	public String setPeaPropValues(Properties prop, InputStream inputStream) {
		
		String errorMessage = null;
		try {
			String defaultFolder = prop.getProperty("default_folder");
			if (defaultFolder != null) {
				//System.getProperty("user.home")
				CalendarSetting.setDefaultFolder(defaultFolder);
			}

			String lineNumberString = prop.getProperty("number_of_lines_in_month_panel");
			if (lineNumberString != null) {
				try {
					CalendarSetting.setNumberLinesInMonthPanel(Integer.parseInt(lineNumberString));
				} catch (Exception e) {
					new UnexpectedValueException("prop", "Properties", "is not digit: " + prop.getProperty("number_of_lines_in_month_panel")).printDescription();
				}
			}
			String startHourString = prop.getProperty("start_hour_of_day");
			if (startHourString != null) {
				try {
					CalendarSetting.setViewStartHour( Integer.parseInt(startHourString));
				} catch (Exception e) {
					new UnexpectedValueException("startHourString", "Propoerties", "is not digit: " + prop.getProperty("start_hour_of_day")).printDescription();
				}
			}
			String fontSizeString = prop.getProperty("event_font_size");
			if (fontSizeString != null) {
				try {
					CalendarSetting.setEventButtonFont(new Font(Font.SANS_SERIF, Font.PLAIN, Integer.parseInt(fontSizeString)));
				} catch (Exception e) {
					new UnexpectedValueException("prop", "Propoerties", "is not digit: " + prop.getProperty("event_font_size")).printDescription();
				}
			}
			
			String showTodos = prop.getProperty("show_todos");
			if (showTodos != null) {
				if (showTodos.toUpperCase().equals("TRUE")) {
					CalendarSetting.setShowTodos(true);
				} else if (showTodos.toUpperCase().equals("FALSE")) {
					CalendarSetting.setShowTodos(false);
				} else {
					TestLog.ve(CalendarPEAControl.class, "invalid value for show_todos: " + showTodos);
				}
			}

			int r = 0, g = 0, b = 0;
			String rString = prop.getProperty("day_background_color_r");
			if (rString != null) {
				try {
					r = Integer.parseInt(rString);
				} catch (Exception e) {
					r = CalendarSetting.getDayBackgroundColor().getRed();
					new UnexpectedValueException("rString", "String", "is not digit: " + rString).printDescription();
				}
			} else {
				r = CalendarSetting.getDayBackgroundColor().getRed();
			}
			String gString = prop.getProperty("day_background_color_g");
			if (gString != null) {
				try {
					g = Integer.parseInt(gString);
				} catch (Exception e) {
					g = CalendarSetting.getDayBackgroundColor().getGreen();
					new UnexpectedValueException("gString", "String", "is not digit: " + gString).printDescription();
				}
			} else {
				g = CalendarSetting.getDayBackgroundColor().getGreen();
			}
			String bString = prop.getProperty("day_background_color_b");
			if (bString != null) {
				try {
					b = Integer.parseInt(bString);
				} catch (Exception e) {
					b = CalendarSetting.getDayBackgroundColor().getBlue();
					new UnexpectedValueException("bString", "String", "is not digit: " + bString).printDescription();
				}
			} else {
				b = CalendarSetting.getDayBackgroundColor().getBlue();
			}
			CalendarSetting.setDayBackgroundColor(new Color(r, g, b));
			
			String rTodayString = prop.getProperty("today_color_r");
			if (rTodayString != null) {
				try {
					r = Integer.parseInt(rTodayString);
				} catch (Exception e) {
					r = CalendarSetting.getTodayColor().getRed();
					new UnexpectedValueException("rTodayString", "String", "is not digit: " + rTodayString).printDescription();
				}
			} else {
				r = CalendarSetting.getTodayColor().getRed();
			}
			String gTodayString = prop.getProperty("today_color_g");
			if (gTodayString != null) {
				try {
					g = Integer.parseInt(gTodayString);
				} catch (Exception e) {
					g = CalendarSetting.getTodayColor().getGreen();
					new UnexpectedValueException("gTodayString", "String", "is not digit: " + gTodayString).printDescription();
				}
			} else {
				g = CalendarSetting.getTodayColor().getGreen();
			}
			String bTodayString = prop.getProperty("today_color_b");
			if (bTodayString != null) {
				try {
					b = Integer.parseInt(bTodayString);
				} catch (Exception e) {
					b = CalendarSetting.getTodayColor().getBlue();
					new UnexpectedValueException("bTodayString", "String", "is not digit: " + bTodayString).printDescription();
				}
			} else {
				b = CalendarSetting.getTodayColor().getBlue();
			}
			CalendarSetting.setTodayColor(new Color(r, g, b));
						
			String lastViewString = prop.getProperty("last_view");
			CalendarView view = null;
			if (lastViewString != null) {
				int[] dimValues = JREProperties.getFrameDim();
				Dimension newDim = new Dimension(dimValues[0], dimValues[1]);
				if (lastViewString.equals("DAY")) {
					view = new DayPanel(LocalDate.now(), newDim, false);
				} else if (lastViewString.equals("WEEK")) {
					view = new WeekPanel(LocalDate.now(), newDim);
				} else if (lastViewString.equals("MONTH")) {
					view = new MonthPanel(LocalDate.now(), newDim);
				} else {
					new UnexpectedValueException("lastViewString", "String", "is invalid: " + lastViewString).printDescription();
				}								
			}
			if (view != null) {
				CalendarSetting.setStartView(view);
				//LockFrameContact.setView(view);
			}

					
		} catch ( Exception e){
			TestLog.e(CalendarPEAControl.class, e + ", " + e.getMessage());
			new UnexpectedValueException("","","set pea properties").printDescription();
			errorMessage = e.toString() + ", " + e.getLocalizedMessage();
		}
		return errorMessage;
	}

	@Override
	public String updatePeaPropValues(Properties prop) {//, OutputStream outputStream) {
		String errorMessage = null;
		try {
			if (CalendarSetting.getDefaultFolder() != null) {
				prop.setProperty("default_folder", CalendarSetting.getDefaultFolder());
			}
			//prop.setProperty("default_file_absolute_path",CalendarSetting.getDefaultFileAbsolutePath());
			prop.setProperty("number_of_lines_in_month_panel", "" + CalendarSetting.getNumberLinesInMonthPanel());
			
			prop.setProperty("event_font_size", "" + CalendarSetting.getEventButtonFont().getSize());
			prop.setProperty("start_hour_of_day", "" + CalendarSetting.getViewStartHour());
			
			boolean showTodos = CalendarSetting.isShowTodos();
			if (showTodos == true) {
				prop.setProperty("show_todos", "TRUE");
			} else {
				prop.setProperty("show_todos", "FALSE");
			}

			prop.setProperty("day_background_color_r","" + CalendarSetting.getDayBackgroundColor().getRed());
			prop.setProperty("day_background_color_g","" + CalendarSetting.getDayBackgroundColor().getGreen());
			prop.setProperty("day_background_color_b","" + CalendarSetting.getDayBackgroundColor().getBlue());

			prop.setProperty("today_color_r", "" + CalendarSetting.getTodayColor().getRed());
			prop.setProperty("today_color_g", "" + CalendarSetting.getTodayColor().getGreen());
			prop.setProperty("today_color_b", "" + CalendarSetting.getTodayColor().getBlue());
			
			// set last view as start view
			if (LockFrameCalendar.getView() instanceof DayPanel) {
				prop.setProperty("last_view", "DAY");
			} else if (LockFrameCalendar.getView() instanceof WeekPanel) {
				prop.setProperty("last_view", "WEEK");
			} else if (LockFrameCalendar.getView() instanceof MonthPanel) {
				prop.setProperty("last_view", "MONTH");
			}
		} catch ( NullPointerException e){
			TestLog.e(CalendarPEAControl.class, new ExceptionHelper(e).getInfos());
			errorMessage = e.toString() + ", " + e.getLocalizedMessage();
		} catch ( Exception e){
			TestLog.e(CalendarPEAControl.class, new ExceptionHelper(e).getInfos());
			errorMessage = e.toString() + ", " + e.getLocalizedMessage();
		}
		return errorMessage;
	}
	
	private static class  EventDispatchThread implements Runnable{

		@Override
		public void run() {
			PswDialogView.setUI(true);
			// customize button margin
			UIManager.put("Button.contentMargins", new Insets( 5,9,5,9) );	
			pswDialog = CalendarPEAControl.getInstance();	
			//CalendarPEAControl.dialogView.setVisible(true);
			PswDialogView.getInstance().setVisible(true);
			
			setDialog(pswDialog);			
			if (FilePanelHolder.getFileNumber() == 0) {
				PswDialogView.setMessage(PeaProperties.getVmBridge().translate("open_or_initialize"), true, false);
			}
			startEDT();

			if (LockFrameCalendar.getFrame() != null) {
				LockFrameCalendar.getFrame().markContentIsSaved();
			}
		}		
	}

	@Override
	public String getPeaName() {
		return PEA_NAME;
	}

	@Override
	public String[] getSupportedExtensions() {
		return supportedExtensions;
	}

	@Override
	public void setLanguage(String languageShortName) {
		try {
			CalendarControl.setCalendarLanguagesBundle(languageShortName);
		} catch (MalformedURLException e) {
			TestLog.ve(CalendarPEAControl.class, "Failed to set language " + languageShortName, 5);
		}		
	}

	@Override
	public String[] getSupportedLanguages() {
		return supportedLanguages;
	}


	@Override
	public String getPeaPropFileName() {
		return peaPropFileName;
	}
}
