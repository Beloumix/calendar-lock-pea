package cologne.eck.calendar_pea.control;

/*
 * PeaFactory - Production of Password Encryption Archives
 * Copyright (C) 2015  Axel von dem Bruch
 * 
 * This library is free software; you can redistribute it and/or modify it 
 * under the terms of the GNU General Public License as published 
 * by the Free Software Foundation; either version 2 of the License, 
 * or (at your option) any later version.
 * This library is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
 * or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU General Public License for more details.
 * See:  http://www.gnu.org/licenses/gpl-2.0.html
 * You should have received a copy of the GNU General Public License 
 * along with this library.
 */

import java.nio.charset.Charset;


/*
 * This class contains variables and their Getters 
 * that can't be modified
 */
public class CalendarConstants {

	/*
	 * The default character set, used whenever a string
	 * is converted 
	 */
	private static final Charset UTF_8 = Charset.forName("UTF-8");	

	/* PRODID - must
	 * globally unique identifier, text that describes the product and version
	 */
	private static final String PROD_ID = "-//PEAFACTORY//CALENDAR LOCK PEA//EN";
	// the string to start a PCard
	private static final String FIXED_CALENDAR_STRING = "BEGIN:VCALENDAR\r\nPRODID:" + PROD_ID + "\r\nVERSION:2.0\r\nCALSCALE:GREGORIAN\r\n";
	
	
	/**
	 * Get the default charset: UTF-8
	 * 
	 * @return the UTF-8 charset
	 */
	public static Charset getCharset() {
		return UTF_8;
	}

	/**
	 * Get the fixed start String to write a PCard
	 * (contains scale, version,prodID, method)
	 * 
	 * @return	the fixed start String to write a Calendar
	 */
	public static String getFixedCalendarString() {
		return FIXED_CALENDAR_STRING;
	}
}
