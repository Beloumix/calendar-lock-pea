package cologne.eck.calendar_pea.control;

import java.io.BufferedInputStream;

/*
 * PeaFactory - Production of Password Encryption Archives
 * Copyright (C) 2023  Axel von dem Bruch
 * 
 * This library is free software; you can redistribute it and/or modify it 
 * under the terms of the GNU General Public License as published 
 * by the Free Software Foundation; either version 2 of the License, 
 * or (at your option) any later version.
 * This library is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
 * or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU General Public License for more details.
 * See:  http://www.gnu.org/licenses/gpl-2.0.html
 * You should have received a copy of the GNU General Public License 
 * along with this library.
 */


import java.io.File;
import java.io.InputStream;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import javax.sound.sampled.*;

import cologne.eck.all_peas.gui.PeaDialog;
import cologne.eck.all_peas.vm_specific.JREProperties;
import cologne.eck.calendar_pea.data.CalendarComponent;
import cologne.eck.calendar_pea.data.PAlarm;
import cologne.eck.calendar_pea.util.CalendarComposer;
import cologne.eck.calendar_pea.data.PCalendar;
import cologne.eck.calendar_pea.data.PEvent;
import cologne.eck.calendar_pea.data.PTodo;
import cologne.eck.calendar_pea.data.RecurringPEvent;
import cologne.eck.tools.FileTools;
import cologne.eck.tools.TestLog;

/*
 Alarm component controlling: 
 PAlarm
 CalendarComponent: list of alarms as field
 CalendarComposer: Set alarms for PEvents and PTodos
 ComponentSetter: createAlarmPanel()
 EventSetter / TodoSetter: add alarm panel
 CalendarPEAControl: starts alarm service in cleanupOnReturn()
 ReadICal / WriteICal: read and write alarms
 */

public class AlarmControl {
	
	/**
	 * Alarms to schedule
	 */
	private static HashSet<PAlarm> alarms;
	
	/**
	 * Missing alarm dates to show at start
	 */
	private static HashSet<PAlarm> missedDates;
	

//	private File audioFile = new File("resources/sound.wav");
	private String audioFileName = "sound.wav";

	
	
	private static int updateAfterHours = 3;
	
	private static AlarmControl control;
	private static ScheduledExecutorService scheduler = null;

	public AlarmControl() {
		control = this;
	}

	/**
	 * Create an alarm scheduler
	 * 
	 * @return	true if scheduler was created, false otherwise
	 */
	public static synchronized boolean createService() {
		
		if (control == null) {
			new AlarmControl();
		}
		
		if (alarms == null) {
			alarms = new HashSet<PAlarm>(); // empty list: only update
		}
		
		if (scheduler != null) { // shutdown running service
			try {
				scheduler.shutdownNow();
			} catch (Exception e) {
				TestLog.e(AlarmControl.class, e.getLocalizedMessage() + " - " + e.toString(), 3);
			}
		} else {
			// first start: Show missed Dates
			showMissedDates();			
		}
		//TestLog.e(AlarmControl.class, "Number of alarms: " + alarms.size());
		scheduler =
		        Executors.newScheduledThreadPool(alarms.size() + 1);	
		//ScheduledFuture<?> updateHandle = 
				scheduler.schedule(control.new UpdateTask(), updateAfterHours, TimeUnit.HOURS);
// for tests				scheduler.schedule(control.new UpdateTask(), updateAfterHours, TimeUnit.MINUTES);		
		//ArrayList<ScheduledFuture<?>> alarmHandles = new ArrayList<ScheduledFuture<?>>();
		LocalDateTime nowTime = LocalDateTime.now();
		for (PAlarm alarm : alarms) {

			LocalDateTime triggerTime = null;
			if ( ! ( alarm.getCalComponent() instanceof RecurringPEvent) ){
				triggerTime = alarm.getTriggerTime();			
				
			} else { // RecurringPEvent:
				
				LocalTime trigTime = alarm.getTriggerTimeOfRecurringEvent();
				//TestLog.e(AlarmControl.class,"trigTime: " +trigTime);
				triggerTime = nowTime.toLocalDate().atTime(trigTime);
				//TestLog.e(AlarmControl.class,"triggerTime: " +trigTime);
			}			
	//		LocalDateTime triggerTime = alarm.getTriggerTime();
	//		TestLog.v(AlarmControl.class,"Trigger time: " +triggerTime);
			int minutes = (int) ChronoUnit.MINUTES.between(nowTime, triggerTime); 
			//ScheduledFuture<?> alarmHandle = 
					scheduler.schedule(control.new AlarmTask(alarm), minutes, TimeUnit.MINUTES);
		}		
		return true;
	}
	
	/**
	 * Show missed alarm dates of the current day in one dialog
	 */
	private static void showMissedDates() {
		if (missedDates != null && missedDates.size() > 0) {
			StringBuilder builder = new StringBuilder();
			builder.append(CalendarSetting.calTranslate("missed_alarms") 
					+ " " + LocalDate.now().toString() + ":\n\n"); 
			HashSet<CalendarComponent> compSet = new HashSet<CalendarComponent>();
			for (PAlarm pa : missedDates) { 
				if (pa != null) {
					CalendarComponent cc = pa.getCalComponent();
					if (compSet.contains(cc)) {
						// do nothing
						continue;
					} else {
						compSet.add(cc);

						String dateTimeString = "";
						LocalDateTime timeToShow = null;
						if (cc instanceof PEvent) {							
							timeToShow = cc.getDateStart();
							if (timeToShow != null) {
								dateTimeString = "\n" + CalendarSetting.getShortDateFormatter().format(cc.getDateStart())
										+ " " + CalendarSetting.getTimeFormatter().format(cc.getDateStart());
							}
						} else if (cc instanceof PTodo) {
							timeToShow = ((PTodo) cc).getDue();
							if (timeToShow != null) {
								dateTimeString = "\n" + CalendarSetting.calTranslate("due") + ": " 
										+ CalendarSetting.getShortDateFormatter().format(((PTodo)cc).getDue())
										+ " " + CalendarSetting.getTimeFormatter().format(((PTodo)cc).getDue());
							}
						}
						
						if (pa.getAction().equals(PAlarm.getActionStrings()[0])) {
							builder.append(cc.getSummary() 
									+ dateTimeString + "\n\n");
						} else if (pa.getAction().equals(PAlarm.getActionStrings()[1])) {
							if (pa.getDescription() != null) {
								builder.append(pa.getDescription() + "\n" +
										cc.getSummary() + dateTimeString + "\n\n");
							} else {
								builder.append(cc.getSummary() + 
										dateTimeString + "\n\n");
							}
						}
					}
				}
			}
			PeaDialog.showMessage(JREProperties.getMainWindow(), new String(builder));
		}
	}
	
	/**
	 * Add an alarm to schedule
	 * 
	 * @param alarm	the alarm to schedule
	 * 
	 * @return true if the list changed
	 */
	public static synchronized boolean addAlarm(PAlarm alarm) {
		if (alarms == null) {
			alarms = new HashSet<PAlarm>();
		}
		//TestLog.v(AlarmControl.class, "Alarm added: " + alarm.getTriggerTime().toString());
		return alarms.add(alarm);
	}
	
	public static synchronized void addAlarmToRunningScheduler(PAlarm alarm) {
		if (scheduler == null) {
			scheduler =
			        Executors.newScheduledThreadPool(2);
			scheduler.schedule(control.new UpdateTask(), updateAfterHours, TimeUnit.HOURS);
		}
		LocalDateTime nowTime = LocalDateTime.now();
		LocalDateTime triggerTime = null;

		if ( ! (alarm.getCalComponent() instanceof RecurringPEvent) ){
			triggerTime = alarm.getTriggerTime();			
		} else {
			LocalTime trigTime = alarm.getTriggerTimeOfRecurringEvent();
			triggerTime = nowTime.toLocalDate().atTime(trigTime);		}
		int minutes = (int) ChronoUnit.MINUTES.between(nowTime, triggerTime); 
		
		//ScheduledFuture<?> alarmHandle = 
				scheduler.schedule(control.new AlarmTask(alarm), minutes, TimeUnit.MINUTES);
	}
	
	/**
	 * Get the duration in hours when 
	 * alarms are updated
	 * 
	 * @return	duration until next update of alarms
	 */
	public static int getUpdateAfterHours() {
		return updateAfterHours;
	}


/*	public static void setUpdateAfterHours(int _updateAfterHours) {
		AlarmControl.updateAfterHours = _updateAfterHours;
	}*/
	
	/**
	 * Add an alarm that should have been shown before now
	 * 
	 * @param alarm	the alarm to add
	 */
	public static synchronized void addMissedDate(PAlarm alarm) { 
		if (missedDates == null) {
			missedDates = new HashSet<PAlarm>();
		}
		missedDates.add(alarm);
	}

	/**
	 * Update all event and todo alarms after a period of time: 
	 * Shutdown running tasks, read events and todos, set new alarms
	 */
	private class UpdateTask implements Runnable {

		@Override
		public void run() {
			TestLog.v(getClass(), "Updating alarm tasks...");
			// read all events an create new tasks
			ArrayList<PCalendar> calList = CalendarControl.getShownAndHiddenCalendarList();
			for (PCalendar pc : calList) {
				// This sets the new alarms
				CalendarComposer.sortEventList(pc.getEventList(), LocalDate.now());
			}
			createService();
		}		
	}
	
	/**
	 * 	Play alarm sound at a specific time point
	 */
	private class AlarmTask implements Runnable {
		private PAlarm alarm;
		private AlarmTask(PAlarm _alarm) { 
			this.alarm = _alarm; 
		}
		

		private void playSound() {

			try {
				Object audioInputObject = FileTools.getResourceFileFromJarOrFileSystem(
						audioFileName, false);

				if (audioInputObject != null ) { 

					AudioInputStream audioStream = null;						

					if (audioInputObject instanceof InputStream) {

						InputStream bufferedIn = new BufferedInputStream((InputStream) audioInputObject);
						audioStream = AudioSystem.getAudioInputStream(bufferedIn);
						Clip clip = AudioSystem.getClip();
						clip.open(audioStream);
						clip.start(); 
						TimeUnit.SECONDS.sleep(2);
						/*					                
					    // Convert the file path string to a URL
					     URL sound = getClass().getResource("/resources/" + audioFileName);
					    // Get audio input stream from the file
					    AudioInputStream audioInputStream = AudioSystem.getAudioInputStream(sound);
						// Get clip resource
					    clip = AudioSystem.getClip();
					    // Open clip from audio input stream
					    clip.open(audioInputStream);
					    clip.start();
					    TimeUnit.SECONDS.sleep(3);

					    JarFile jar = new JarFile(System.getProperty("user.dir"));
					    JarEntry jen;
					    jen = new JarEntry("resources/" + audioFileName);
					    audioInputStream = AudioSystem.getAudioInputStream( jar.getInputStream(jen) );
					    // Get clip resource
					    clip = AudioSystem.getClip();
					    // Open clip from audio input stream
					    clip.open(audioInputStream);
					    clip.start();
					    TimeUnit.SECONDS.sleep(3);
						 */					                

					} else if (audioInputObject instanceof File) {

						audioStream = AudioSystem.getAudioInputStream((File) audioInputObject);
						AudioFormat format = audioStream.getFormat();				 
						DataLine.Info info = new DataLine.Info(Clip.class, format);	

						Clip audioClip = (Clip) AudioSystem.getLine(info);
						if (audioClip.isActive() ) {// .isOpen()) {
							TestLog.e(getClass(), "Audio stream is open...", 7);
						}
						audioClip.open(audioStream);
						audioClip.start();
						TimeUnit.SECONDS.sleep(2);
						audioClip.flush();
						audioClip.close();
						audioStream.close(); 
					}

				} else {
					TestLog.e(AlarmControl.class, "Can't find audio file to play sound");
					// try to beep
					java.awt.Toolkit.getDefaultToolkit().beep();
				} 
			} catch (java.lang.IllegalStateException e) {
				TestLog.e(AlarmControl.class, e.getMessage() + " - several alarms at same time");
			} catch (Exception e) {
				TestLog.e(AlarmControl.class, e.getMessage() + " - " + e.toString(), 5);
			} 
		}
	    
	    public void run() {

	    	//TestLog.v(getClass(), "Run alarm task...");
	    	CalendarComponent cc = alarm.getCalComponent();
	    	if (cc instanceof PTodo) {

	    		//TodoInfo.setNewInfo(cc, null); TODO
	    	} else if (cc instanceof PEvent) {

	    		//EventInfo.setNewInfo(cc, null); TODO
	    	}
	    	
	    	if (alarm.getAction().contains(PAlarm.getActionStrings()[0])) {
	    		playSound();
	    	}
	    	if (alarm.getAction().contains(PAlarm.getActionStrings()[1])) {
	    		String description = alarm.getDescription();
	    		if (description == null || description.length() == 0) {
	    			description = alarm.getCalComponent().getSummary();
	    		}
	    		PeaDialog.showMessage(JREProperties.getMainWindow(), description);
	    	}
	    }
	}
}
