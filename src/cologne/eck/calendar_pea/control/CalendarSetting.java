package cologne.eck.calendar_pea.control;

/*
 * PeaFactory - Production of Password Encryption Archives
 * Copyright (C) 2015  Axel von dem Bruch
 * 
 * This library is free software; you can redistribute it and/or modify it 
 * under the terms of the GNU General Public License as published 
 * by the Free Software Foundation; either version 2 of the License, 
 * or (at your option) any later version.
 * This library is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
 * or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU General Public License for more details.
 * See:  http://www.gnu.org/licenses/gpl-2.0.html
 * You should have received a copy of the GNU General Public License 
 * along with this library.
 */

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.text.DateFormatSymbols;
import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Locale;
import java.util.ResourceBundle;

import cologne.eck.all_peas.control.Translator;
import cologne.eck.all_peas.data.PeaProperties;
import cologne.eck.all_peas.vm_specific.JREProperties;
import cologne.eck.calendar_pea.gui.calendar_view.CalendarView;
import cologne.eck.calendar_pea.gui.calendar_view.WeekPanel;
import cologne.eck.tools.TestLog;


/*
 * This class contains variables and their Getters & Setters 
 * that might be changed by users
 */
public class CalendarSetting {
	
	// Formatter for iCal date and time values
	private static DateTimeFormatter iCalDateTimeFormatter = DateTimeFormatter.ofPattern("yyyyMMdd'T'HHmmss", PeaProperties.getLocale());
	// for all-day events: date only
	private static DateTimeFormatter iCalDateFormatter = DateTimeFormatter.ofPattern("yyyyMMdd", PeaProperties.getLocale());

	// format for date and time: // TODO language change
	// used for one day events:
	private static DateTimeFormatter fullDateFormatter = DateTimeFormatter.ofLocalizedDate(FormatStyle.FULL).withLocale(PeaProperties.getLocale());
	// used for several day events:
	private static DateTimeFormatter shortDateFormatter = DateTimeFormatter.ofLocalizedDate(FormatStyle.SHORT).withLocale(PeaProperties.getLocale());
	private static DateTimeFormatter timeFormatter = DateTimeFormatter.ofPattern("HH:mm", PeaProperties.getLocale());
	
	private static String[] translatedWeekDaysWithoutNull //= new DateFormatSymbols(PeaProperties.getLocale()).getShortWeekdays();
		= {"MO", "TU", "WE", "TH", "FR", "SA", "SU"};
	private static final String[] fixedWeekDaysWithoutNull //= new DateFormatSymbols(PeaProperties.getLocale()).getShortWeekdays();
	= {"MO", "TU", "WE", "TH", "FR", "SA", "SU"};
	
	// this can't be final: initialize the first time it is used
	private static ArrayList<String> fixedWeekDaysList;

	/**
	 * The used Translator instance
	 */
	private static Translator translator = null;
	/**
	 * The language bundle for Calendar Lock PEA
	 */
//	private static ResourceBundle languageBundle;		
	/**
	 * The last used view (day, week, month)
	 */
	private static CalendarView startView = new WeekPanel(LocalDate.now(), 
			new Dimension(JREProperties.getFrameDim()[0], JREProperties.getFrameDim()[1]));
	
	/**
	 * The default file name to store the encrypted content of all calendars 
	 */
	private static String defaultFolder;	
	
	/**
	 * Formatter for Label of singleDayPanels in MonthPanel to show the date 
	 */
	// short names of Months
	private static DateTimeFormatter dayDateFormatter = DateTimeFormatter.ofPattern ( "dd MMM yyyy" ).withLocale ( PeaProperties.getLocale() );

	/**
	 * Lines of days to show in the MonthPanel
	 */
	private static int numberOfLinesInMonthPanel = 5;
	
	/**
	 * the background color of days in MonthPanel
	 */
	private static Color dayBackgroundColor = new Color(252, 254, 253);//new Color(248, 248, 248);
	
	/**
	 * The background color for today in MonthPanel
	 */
	private static Color todayColor = new Color(255, 242, 204);
	
	/**
	 * The font used to show events in tool tips
	 */
	private static Font eventButtonFont = new Font(Font.SANS_SERIF, Font.PLAIN, 8);
	
	/**
	 * The time shown first in the scroll pane of DayView
	 */
	private static int viewStartHour = 7;
	
	/**
	 * Show the todo panel
	 */
	private static boolean showTodos = true;
	

	
	//================== GETTERS & SETTERS ================//
	
	/**
	 * Set the language bundle for Calendar Lock PEA
	 * 
	 * @param newBundle	the language bundle to use
	 */
	public static void setResourceBundle(ResourceBundle newBundle) {
//		languageBundle = newBundle;
		// change formatters
		Locale locale = PeaProperties.getLocale();
		setiCalDateTimeFormatter(locale);
		setiCalDateFormatter(locale);
		setFullDateFormatter(locale);
		setShortDateFormatter(locale);
		setTimeFormatter(locale);
		setTranslatedWeekDaysWithoutNull(locale);
		setDayDateFormatter(locale);
	}
	
	/**
	 * Get the current language bundle or null
	 * 
	 * @return		the current language bundle or null
	 */
/*	protected final static ResourceBundle getResourceBundle(){
		return languageBundle;
	}	*/

	/**
	 * @return the iCalDateTimeFormatter
	 */
	public static DateTimeFormatter getIcalDateTimeFormatter() {
		return iCalDateTimeFormatter;
	}
	
	/**
	 * @param iCalDateTimeFormatter the iCalDateTimeFormatter to set
	 */
	private static void setiCalDateTimeFormatter(Locale locale) {
		CalendarSetting.iCalDateTimeFormatter = DateTimeFormatter.ofPattern("yyyyMMdd'T'HHmmss", locale);
	}
	
	/**
	 * @return the iCalDateFormatter
	 */
	public static DateTimeFormatter getIcalDateFormatter() {
		return iCalDateFormatter;
	}
	
	/**
	 * @param iCalDateFormatter the iCalDateFormatter to set
	 */
	private static void setiCalDateFormatter(Locale locale) {
		CalendarSetting.iCalDateFormatter = DateTimeFormatter.ofPattern("yyyyMMdd", locale);
	}
	
	/**
	 * @return the fullDateFormatter
	 */
	public static DateTimeFormatter getFullDateFormatter() {
		return fullDateFormatter;
	}
	
	/**
	 * @param fullDateFormatter the locale to use
	 */
	private static void setFullDateFormatter(Locale locale) {
		CalendarSetting.fullDateFormatter =  DateTimeFormatter.ofLocalizedDate(FormatStyle.FULL).withLocale(locale);
	}
	
	/**
	 * @return the shortDateFormatter
	 */
	public static DateTimeFormatter getShortDateFormatter() {
		return shortDateFormatter;
	}
	
	/**
	 * @param shortDateFormatter the locale to use
	 */
	private static void setShortDateFormatter(Locale locale) {
		CalendarSetting.shortDateFormatter = DateTimeFormatter.ofLocalizedDate(FormatStyle.SHORT).withLocale(locale);
	}
	
	/**
	 * @return the timeFormatter
	 */
	public static DateTimeFormatter getTimeFormatter() {
		return timeFormatter;
	}
	
	/**
	 * @param timeFormatter the locale to use
	 */
	private static void setTimeFormatter(Locale locale) {
		CalendarSetting.timeFormatter = DateTimeFormatter.ofPattern("HH:mm", locale);
	}
	
	/**
	 * Translate using the calendar language bundle
	 * 
	 * @param key	the key to translate
	 * @return	the translated key or the key if no matching key is found
	 */
	public final static String calTranslate(String key) {
		
		if (translator == null) {
			translator = PeaProperties.getVmBridge().getTranslator();		
		}
		return translator.translate(key,  "calendar");
	}
	
	/**
	 * Get the formatter to display a day date
	 * Default is LONG, possible formatters: FULL, MEDIUM, SHORT
	 * 
	 * @return 	the formatter to display a day date
	 */
	public static DateTimeFormatter getDayDateFormatter() {
		return dayDateFormatter;
	}
	
	/**
	 * Set the formatter to display a day date
	 * Default is LONG, possible formatters: FULL, MEDIUM, SHORT
	 * 
	 * @param 	the locale to use
	 */
	private static void setDayDateFormatter(Locale locale) {
		dayDateFormatter = DateTimeFormatter.ofPattern ( "dd MMM yyyy" ).withLocale ( locale );
	}

	/**
	 * Set the translated weekday string  
	 * like MO, TU, WE, TH, FR, SA, SU
	 * without leading null value for the given locale
	 * 
	 * @param	the locale to use
	 */
	private static void setTranslatedWeekDaysWithoutNull(Locale locale) {
		try {
			// get week days with leading null value
			String[] localWeekDayWithNull = new DateFormatSymbols(locale).getShortWeekdays();
			// always start with Sunday at index 1 (Sunday is 1 and Saturday is 7),
			// but should start with Monday
			translatedWeekDaysWithoutNull[0] = localWeekDayWithNull[2];// Mo
			translatedWeekDaysWithoutNull[1] = localWeekDayWithNull[3];// Tu
			translatedWeekDaysWithoutNull[2] = localWeekDayWithNull[4];// We
			translatedWeekDaysWithoutNull[3] = localWeekDayWithNull[5];// Th
			translatedWeekDaysWithoutNull[4] = localWeekDayWithNull[6];// Fr
			translatedWeekDaysWithoutNull[5] = localWeekDayWithNull[7];// Sa
			translatedWeekDaysWithoutNull[6] = localWeekDayWithNull[1];// So
		} catch (Exception e) {
			TestLog.ve(CalendarSetting.class, e.getLocalizedMessage() + " - " + e.toString(), 5);
			// something went wrong... use EN array
			translatedWeekDaysWithoutNull = fixedWeekDaysWithoutNull.clone();
		}		
	}
	
	/**
	 * @return the fixedweekdayswithoutnull
	 */
	public static String[] getFixedWeekDaysWithoutNull() {
		return fixedWeekDaysWithoutNull;
	}

	/**
	 * Get the value of a weekDay, as used in iCalendar, 
	 *  MO is 1, not 0 !!!
	 * 
	 * @param weekDayShortName	the short name of the weekday:
	 * 							MO,TU.WE,TH,FR,SA,SU
	 * 
	 * @return	the value (not index) of the weekday: MO is 1,
	 * 			-1 if there is no such short name
	 */
	public static int getDayOfWeekValue(String weekDayShortName) {
		if (fixedWeekDaysList == null) {
			fixedWeekDaysList = new ArrayList<>(Arrays.asList(fixedWeekDaysWithoutNull));
			//fixedWeekDaysList = Converter.arrayToArrayList(fixedWeekDaysWithoutNull);
		}
		if (fixedWeekDaysList.contains(weekDayShortName)) {
			return fixedWeekDaysList.indexOf(weekDayShortName) + 1;// +1 because of index
		} else {
			TestLog.e(CalendarSetting.class, "Invalid day of week: " + weekDayShortName);
			TestLog.ve(CalendarSetting.class, "Invalid DayOfWeek: " + weekDayShortName, 5);
			return -1;
		}
	}
	
	/**
	 * Get the weekDay from English (iCal) weekday short name 
	 * 
	 * @param weekDayShortName	the short name of the weekday:
	 * 							MO,TU.WE,TH,FR,SA,SU 
	 * 
	 * @return	the weekday as DayOfWeek or null if there is no such short name
	 */
	public static DayOfWeek getDayOfWeek(String weekDayShortName) {
		if (fixedWeekDaysList == null) {
			fixedWeekDaysList = new ArrayList<>(Arrays.asList(fixedWeekDaysWithoutNull));
		}
		if (fixedWeekDaysList.contains(weekDayShortName)) {
			return DayOfWeek.of(fixedWeekDaysList.indexOf(weekDayShortName) + 1);
		} else {
			TestLog.e(CalendarSetting.class, "Invalid day of week: " + weekDayShortName);
			TestLog.ve(CalendarSetting.class, "Invalid DayOfWeek: " + weekDayShortName, 5);
			return null;
		}
	}

	/**
	 * Get the number of lines of days in MonthPanel.
	 * One line contains always seven days. 
	 * 
	 * @return	the number of lines to show
	 */
	public static int getNumberLinesInMonthPanel() {
		return numberOfLinesInMonthPanel;
	}
	
	/**
	 * Set the number of lines of days in MonthPanel.
	 * One line contains always seven days. 
	 * 
	 * @param	the number of lines to show
	 */
	public static void setNumberLinesInMonthPanel(int newNumberOfLinesInMonthPanel) {
		CalendarSetting.numberOfLinesInMonthPanel = newNumberOfLinesInMonthPanel;
	}

	/**
	 * Set the background color of days in MonthPanel
	 * 
	 * @return	the background color of days
	 */
	public static Color getDayBackgroundColor() {
		return dayBackgroundColor;
	}
	
	/**
	 * Set the background color of days in MonthPanel
	 * 
	 * @param	the background color of days
	 */
	public static void setDayBackgroundColor(Color dayBackgroundColor) {
		CalendarSetting.dayBackgroundColor = dayBackgroundColor;
	}
	
	/**
	 * Get the background color of the todays day panel in MonthPanel
	 * 
	 * @return	the color for today
	 */
	public static Color getTodayColor() {
		return todayColor;
	}
	
	/**
	 * Set the background color of the todays day panel in MonthPanel
	 * 
	 * @param	the color for today
	 */
	protected static void setTodayColor(Color todayColor) {
		CalendarSetting.todayColor = todayColor;
	}
	
	/**
	 * Get the font of event buttons in MonthPanel
	 * 
	 * @return	the font of event buttons
	 */
	public static Font getEventButtonFont() {
		return eventButtonFont;
	}
	
	/**
	 * Set the font of event buttons in MonthPanel
	 * 
	 * @param	the font of event buttons
	 */
	public static void setEventButtonFont(Font newEventButtonFont) {
		CalendarSetting.eventButtonFont = newEventButtonFont;
	}
	
	/**
	 * Get the default folder to save 
	 * the encrypted content of all calendars as files
	 * 
	 * @return the default folder to save calendars
	 */
	public static String getDefaultFolder() {
		return defaultFolder;
	}
	
	/**
	 * Set the default folder to save 
	 * the encrypted content of all calendars as files
	 * 
	 * @param the default folder to save calendars
	 */
	public static void setDefaultFolder(String _newDefaultFolder) {
		CalendarSetting.defaultFolder = _newDefaultFolder;
	}

	/**
	 * Get the first shown hour in scroll pane of DayView
	 * (position of scroll bar)
	 * 
	 * @return the first shown hour (position of scroll bar)
	 */
	public static int getViewStartHour() {
		return viewStartHour;
	}
	
	/**
	 * Set the first shown hour in scroll pane of DayView
	 * (position of scroll bar)
	 * 
	 * @param the first shown hour (position of scroll bar)
	 */
	public static void setViewStartHour(int viewStartHour) {
		CalendarSetting.viewStartHour = viewStartHour;
	}
	
	/**
	 * Get the used view (day, week, month)of the last session
	 * 
	 * @return	the last used view
	 */
	public static CalendarView getStartView() {
		return startView;
	}
	
	/**
	 * Set the used view (day, week, month)of the last session
	 * 
	 * @param startView	the last used view
	 */
	protected static void setStartView(CalendarView startView) {
		CalendarSetting.startView = startView;
	}

	/**
	 * Check if todo panel should be shown
	 * 
	 * @return the showTodos	true: show todos
	 */
	public static boolean isShowTodos() {
		return showTodos;
	}
	
	/**
	 * Set if todo panel should be shown
	 * 
	 * @param showTodos true show todos
	 */
	public static void setShowTodos(boolean showTodos) {
		CalendarSetting.showTodos = showTodos;
	}
}
