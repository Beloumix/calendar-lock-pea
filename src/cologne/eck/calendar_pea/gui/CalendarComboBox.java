package cologne.eck.calendar_pea.gui;

/*
 * PeaFactory - Production of Password Encryption Archives
 * Copyright (C) 2015  Axel von dem Bruch
 * 
 * This library is free software; you can redistribute it and/or modify it 
 * under the terms of the GNU General Public License as published 
 * by the Free Software Foundation; either version 2 of the License, 
 * or (at your option) any later version.
 * This library is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
 * or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU General Public License for more details.
 * See:  http://www.gnu.org/licenses/gpl-2.0.html
 * You should have received a copy of the GNU General Public License 
 * along with this library.
 */

import java.awt.Dimension;
import java.util.ArrayList;

import javax.swing.JComboBox;
import javax.swing.JOptionPane;

import cologne.eck.calendar_pea.control.CalendarControl;
import cologne.eck.calendar_pea.control.CalendarSetting;
import cologne.eck.calendar_pea.data.PCalendar;

@SuppressWarnings("serial")
public class CalendarComboBox extends JComboBox<String> {
	
	// TODO background of items in calendar color
	
	
	/**
	 * Combo box that displays the current list of shown calendars
	 */
	public CalendarComboBox() {
		ArrayList<PCalendar> list = CalendarControl.getShownCalendarList();
		int size = list.size();
		if (size <= 0) { // this is proven first in EventSetter
			JOptionPane.showMessageDialog(this, CalendarSetting.calTranslate("calendar_missing"));
			return;
		}
		this.getAccessibleContext().
	    setAccessibleDescription(
	    	    "Clicking this component selects a calendar for the event or todo.");

		this.setMaximumSize(new Dimension(300, 25));
		for (int i = 0; i < size; i++) {
			// get the name of the Calendar
			String calendarName = list.get(i).getName();
			// add the item
			this.addItem(calendarName);
		}
		if (size == 1) { // only one calendar
			// set this Calendar selected
			this.setSelectedIndex(0);
		}		
	}
	/**
	 * Set a calendar name as selected item
	 * 
	 * @param calName	the calendar name to set selected
	 */
	public void setSelectedCalendar(String calName) {
		int len = this.getItemCount();
		for(int i = 0; i < len; i++) {
			if (((String)this.getItemAt(i)).equals(calName)){
				this.setSelectedIndex(i);
			}
		}
	}
}
