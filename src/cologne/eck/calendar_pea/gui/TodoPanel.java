package cologne.eck.calendar_pea.gui;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Collections;
import java.awt.GridLayout;
import java.awt.Component;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

import cologne.eck.calendar_pea.control.CalendarControl;
import cologne.eck.calendar_pea.control.CalendarSetting;
import cologne.eck.calendar_pea.data.PCalendar;
import cologne.eck.calendar_pea.data.PTodo;
import cologne.eck.calendar_pea.gui.event_display.TodoButton;
import cologne.eck.calendar_pea.gui.event_setting.TodoSetter;
//import cologne.eck.tools.TestLog;


@SuppressWarnings("serial")
public class TodoPanel extends JPanel 
				implements ActionListener {
	
	private JPanel todoPanel;
	private JButton plusButton;
	
	private JPanel todoListPanel;
	
	TodoPanel(){
		this.setLayout(new BoxLayout(this, BoxLayout.PAGE_AXIS));
		
		todoPanel = new JPanel();
		todoPanel.setLayout(new BoxLayout(todoPanel, BoxLayout.PAGE_AXIS));
		this.add(todoPanel);		
		
		todoListPanel = new JPanel();
		todoListPanel.setLayout(new GridLayout(13,1));//BoxLayout(todoListPanel, BoxLayout.PAGE_AXIS));
		
		// read the todos
		ArrayList<PCalendar> cals = CalendarControl.getShownCalendarList();
		ArrayList<PTodo> todoList = new ArrayList<PTodo>();
		for (PCalendar c : cals) {
			todoList.addAll(c.getTodoList());
		}
		Collections.sort(todoList);

		for(PTodo t : todoList) {
			TodoButton b = new TodoButton(t, t.getDue());
			todoListPanel.add(b);
		}

		JPanel plusPanel= new JPanel();
		plusPanel.setAlignmentX(Component.CENTER_ALIGNMENT);
		JButton plusButton = new JButton("+");
		plusButton.setMinimumSize(new Dimension(40, 30));
		plusButton.setMaximumSize(new Dimension(40, 30));
		plusButton.setPreferredSize(new Dimension(40, 30));
		plusButton.getAccessibleContext().
	    setAccessibleDescription(
	    	    "Clicking this component opens a dialog to create a todo.");
		plusPanel.add(plusButton);
		plusButton.addActionListener(this);
		plusButton.setActionCommand("+");
		todoListPanel.add(plusPanel);		
		
		JScrollPane todoScroll = new JScrollPane(todoListPanel);
		todoScroll.setPreferredSize(new Dimension (130, 500));
		todoPanel.add(todoScroll);
		
		this.setMinimumSize(new Dimension(20, 200));
		this.setMaximumSize(new Dimension(200, 1000));
	}
	
	public void refreshView() {
		todoListPanel.removeAll();

		// read the todos
		ArrayList<PCalendar> cals = CalendarControl.getShownCalendarList();
		ArrayList<PTodo> todoList = new ArrayList<PTodo>();
		for (PCalendar c : cals) {
			todoList.addAll(c.getTodoList());
		}
		Collections.sort(todoList);
		for(PTodo t : todoList) {
			TodoButton b = new TodoButton(t, t.getDue());
			todoListPanel.add(b);
		}	

		JPanel plusPanel= new JPanel();
		plusPanel.setAlignmentX(Component.CENTER_ALIGNMENT);
		plusButton = new JButton("+");
		plusButton.setMinimumSize(new Dimension(10, 10));
		plusButton.setMaximumSize(new Dimension(60, 30));
		plusButton.setPreferredSize(new Dimension(50, 50));
		plusButton.setToolTipText("add a new todo");
		plusPanel.add(plusButton);
		plusButton.addActionListener(this);
		plusButton.setActionCommand("+");
		todoListPanel.add(plusPanel);
		
		todoListPanel.revalidate();
		todoListPanel.repaint();
	}
	
	public void collapseOrExpand() {
		if (todoPanel.isDisplayable() && todoPanel.isShowing() ) {
			todoPanel.setVisible(false);
			CalendarSetting.setShowTodos(false);		
			// Set X or _
			LockFrameCalendar.setShowTodoButton(false);
		} else {
			todoPanel.setVisible(true);
			CalendarSetting.setShowTodos(true);
			// Set X or _
			LockFrameCalendar.setShowTodoButton(true);
		}
	}

	/**
	 * Check if alarms are in the period to show.
	 * This sets also missed alarms of the same day.
	 * 
	 * @param t		the todo to check
	 * @param now 	current date time
	 * @param end	end of period to show alarms (time of update)
	 * @param dayStart	start of current day
	 */
/*	private static void checkAlarm(PTodo t, 
			LocalDateTime now, LocalDateTime end,LocalDateTime dayStart) {
		ArrayList<PAlarm> alarms = t.getAlarms();
		if (alarms == null || alarms.size() == 0) {
			return;
		}		
		
		for (PAlarm alarm : alarms) {
			LocalDateTime triggerTime = alarm.getTriggerTime();
			if (triggerTime != null) {
				if (triggerTime.isAfter(end)) {
					continue;
				} else if (triggerTime.isBefore(now) ) {
					// show only missed triggers of this day
					if (triggerTime.isAfter(dayStart)) {
						AlarmControl.addMissedDate(alarm);
					}
				} else {
					AlarmControl.addAlarm(alarm);
				}
			} else {
				TestLog.ve(CalendarComponent.class,"Missing trigger time", 3);
			}
		}
	} */

/*	public TodoButton getTodoButton(PTodo todo) {
		Component[] comps = todoListPanel.getComponents();
		for (Component c : comps) {
			if (c instanceof TodoButton) {
				PTodo t = ((TodoButton) c).getTodo();
				if (t.equals(todo)) {
					return (TodoButton) c;
				}
			}
		}
		return null;
	}*/
	
	@Override
	public void actionPerformed(ActionEvent e) {
		String command = e.getActionCommand();
		if (e.getSource() instanceof JButton) {
			//TestLog.o(XXXX.class, ((JButton)e.getSource()).getText());
			//TestLog.o(XXXX.class, command);
			
			if (command.equals("+")) {
				//TestLog.o(XXXX.class, "plus");
				//@SuppressWarnings("unused")
				TodoSetter setter = new TodoSetter(this, //Point location,
						null, true, 
						null);
				setter.setVisible(true);
			}
		}
	}
}
