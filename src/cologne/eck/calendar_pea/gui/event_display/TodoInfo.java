package cologne.eck.calendar_pea.gui.event_display;

/*
 * PeaFactory - Production of Password Encryption Archives
 * Copyright (C) 2021  Axel von dem Bruch
 * 
 * This library is free software; you can redistribute it and/or modify it 
 * under the terms of the GNU General Public License as published 
 * by the Free Software Foundation; either version 2 of the License, 
 * or (at your option) any later version.
 * This library is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
 * or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU General Public License for more details.
 * See:  http://www.gnu.org/licenses/gpl-2.0.html
 * You should have received a copy of the GNU General Public License 
 * along with this library.
 */

import java.awt.Color;
import java.awt.Font;
import java.util.Arrays;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSeparator;
import javax.swing.JTextArea;

import cologne.eck.calendar_pea.control.CalendarSetting;
import cologne.eck.calendar_pea.data.CalendarComponent;
import cologne.eck.calendar_pea.data.PTodo;
import cologne.eck.tools.TestLog;


class TodoInfo { 


	// default font to use for label and text areas
	private static Font infoFont = new Font(Font.SANS_SERIF, Font.PLAIN, 12);

	private static int fixedInfoWidth = 350;// width of this

	
	/**
	 * Tool tip like info window to show infos about an event, 
	 * when a mouse enters an EventButton
	 * 
	 * @param _t		the todo to show
	 * @param _tb	the affected TodoButton
	 */
	protected static void setNewInfo(CalendarComponent _t, ComponentButton _tb) {
		//info = this;	
		PTodo t = null; 
		TodoButton tb = null;
		if (_t instanceof PTodo) {
			t = (PTodo) _t;
		} else {
			TestLog.ve(TodoInfo.class, "Missing event", 5);
			return;
		}
		if (_tb != null && _tb instanceof TodoButton) {
			tb = (TodoButton) _tb;
		} else {
			// called from PAlarm
//TestLog.ve(TodoInfo.class, "Missing event button", 5);
			//return;
		}	

		// background color depends an calendar color
		Color backgroundColor = t.getCalendar().getColor();
		backgroundColor = new Color(backgroundColor.getRed(),backgroundColor.getGreen(), backgroundColor.getBlue(), 127);
		// font color depends on brightness of background (black or white)
		Color fontColor = EventButton.getAppropriateFontColor(backgroundColor);

		JPanel panel = new JPanel();// (JPanel) this.getContentPane();	
		if (t.getStatus() != null) {
			if (t.getStatus().equals("CANCELLED")) {
				panel = new CancelledEventPanel();
			} 
		}
		
		panel.setLayout(new BoxLayout(panel, BoxLayout.PAGE_AXIS));		
		
		// getPreferredSize does not work because of line wrap in text areas
		// -> collect heights of components
		double prefHeight = 0;
		// time: all-day: no time, one-day: start and end time in one label
		// several days: start and end separated
		//String endTime = null;
		JPanel timeLabelPanel = null;
		if (t.getDue() != null) {
			//endTime = ContactConstants.getTimeFormatter().format(e.getDue());
			timeLabelPanel = new JPanel();
			timeLabelPanel.setLayout(new BoxLayout(timeLabelPanel, BoxLayout.LINE_AXIS));
			String endString = CalendarSetting.getTimeFormatter().format(t.getDue());
			timeLabelPanel.add(Box.createHorizontalStrut(10));
			JLabel dueLabel = new JLabel(CalendarSetting.calTranslate("due") + ": " );
			dueLabel.setFont(new Font(infoFont.getFontName(), Font.BOLD, infoFont.getSize() + 2));
			timeLabelPanel.add(dueLabel);
			JLabel dateLabel = new JLabel(CalendarSetting.getFullDateFormatter().format(t.getDue()));
			timeLabelPanel.add(dateLabel);
			JLabel timeLabel = new JLabel(endString);
			timeLabel.setFont(infoFont);
			timeLabelPanel.add(Box.createHorizontalStrut(10));
			timeLabelPanel.add(timeLabel);
			timeLabelPanel.add(Box.createHorizontalGlue());
			prefHeight += timeLabelPanel.getPreferredSize().getHeight();
		} else {
			// maybe recurrence rule sets date end
			//endTime = "unknown";
		}
		// show summary always, event if null
		String summary = null;
		if (t.getSummary() == null) {
			summary = " ";
		} else {
			summary = t.getSummary();
		}
		JTextArea summaryArea = new JTextArea();
		summaryArea.setBackground(backgroundColor);
		summaryArea.setForeground(fontColor);
		summaryArea.setText(summary);
		summaryArea.setFont(new Font(infoFont.getFontName(), Font.BOLD, infoFont.getSize() + 2));
		summaryArea.setLineWrap(true);
		summaryArea.setWrapStyleWord(true);
		// estimate the number of lines
		int stringWidth = summaryArea.getFontMetrics(summaryArea.getFont()).stringWidth(summary);
		if (stringWidth > fixedInfoWidth) {
			// quick and dirty...
			double lines = stringWidth / fixedInfoWidth;// + 1;
			if (lines > 4) {
				//lines = lines +1;
			}
			prefHeight += summaryArea.getPreferredSize().getHeight() * lines;
		} else {
			prefHeight += summaryArea.getPreferredSize().getHeight();
		}
		// show description if exists
		JTextArea descriptionArea = null;		
		if (t.getDescription() != null && ( ! t.getDescription().equals(""))) {
			descriptionArea = new JTextArea();
			descriptionArea.setFont(infoFont);
			descriptionArea.setBackground(backgroundColor);
			descriptionArea.setForeground(fontColor);
			descriptionArea.setLineWrap(true);
			descriptionArea.setWrapStyleWord(true);
			descriptionArea.setText(t.getDescription());
			stringWidth = descriptionArea.getFontMetrics(infoFont).stringWidth(t.getDescription());
			if (stringWidth > fixedInfoWidth) {
				// quick and dirty...
				double lines = stringWidth / fixedInfoWidth;// + 1;
				prefHeight += descriptionArea.getPreferredSize().getHeight() * lines;
			} else {
				prefHeight += descriptionArea.getPreferredSize().getHeight();
			}
		}
		// show location if exists
		JTextArea locationArea = null;		
		if (t.getLocation() != null && ( ! t.getLocation().equals(""))) {
			locationArea = new JTextArea();
			locationArea.setFont(infoFont);
			locationArea.setBackground(backgroundColor);
			locationArea.setForeground(fontColor);
			locationArea.setLineWrap(true);
			locationArea.setWrapStyleWord(true);
			locationArea.setText(t.getLocation());
			stringWidth = locationArea.getFontMetrics(infoFont).stringWidth(t.getLocation());
			if (stringWidth > fixedInfoWidth) {
				// quick and dirty...
				double lines = stringWidth / fixedInfoWidth;// + 1;
				prefHeight += locationArea.getPreferredSize().getHeight() * lines;
			} else {
				prefHeight += locationArea.getPreferredSize().getHeight();
			}
		}
		JPanel statusAndPriorityPanel = new JPanel();
		statusAndPriorityPanel.setLayout(new BoxLayout(statusAndPriorityPanel, BoxLayout.LINE_AXIS));
		JLabel priorityLabel = null;
		int priority = t.getPriority();
		if (priority > 0) {
			char[] prio = new char[10- priority];
			Arrays.fill(prio, '*');
			priorityLabel = new JLabel(CalendarSetting.calTranslate("priority") 
					+ " " + String.valueOf(prio));
			priorityLabel.setForeground(Color.RED);
			prefHeight += priorityLabel.getPreferredSize().getHeight();
			statusAndPriorityPanel.add(priorityLabel);
			statusAndPriorityPanel.add(Box.createHorizontalGlue());
		}
		String status = t.getStatus();
		JLabel statusLabel = null;
		if (status != null) {
			if (status.equals("NEEDS-ACTION")) {
				statusAndPriorityPanel.add(new JLabel(CalendarSetting.calTranslate("status") + " " + CalendarSetting.calTranslate("needs_action")));
			} else if (status.equals("COMPLETED")) {
				statusAndPriorityPanel.add(new JLabel(CalendarSetting.calTranslate("status")  + " " + CalendarSetting.calTranslate("completed")));
				
				statusLabel = new JLabel("✓");//\2713");
				statusLabel.setForeground(Color.GREEN);
				statusLabel.setFont(new Font(infoFont.getFontName(), Font.BOLD, infoFont.getSize() + 10));
				statusAndPriorityPanel.add(Box.createHorizontalStrut(10));
				statusAndPriorityPanel.add(statusLabel);
				prefHeight += statusLabel.getPreferredSize().getHeight();
			} else if (status.equals("IN-PROCESS")) {
				statusAndPriorityPanel.add(new JLabel(CalendarSetting.calTranslate("status")  + " " + CalendarSetting.calTranslate("in_process") + "..."));
			} else if (status.equals("CANCELLED")) {
				statusAndPriorityPanel.add(new JLabel(CalendarSetting.calTranslate("status")  + " " + CalendarSetting.calTranslate("cancelled")));
			} 
		}

		//contentPane.add(dateLabelPanel);
		if (timeLabelPanel != null) {
			panel.add(timeLabelPanel);
		}

		panel.add(new JSeparator(JSeparator.HORIZONTAL));
		panel.add(summaryArea);
		if (descriptionArea != null) {
			panel.add(new JSeparator(JSeparator.HORIZONTAL));
			panel.add(descriptionArea);
		}
		if (locationArea != null) {
			panel.add(new JSeparator(JSeparator.HORIZONTAL));
			panel.add(locationArea);
		}
		if (priority > 0 || status != null) {
			panel.add(statusAndPriorityPanel);
		}
		
		ComponentInfo.setInfo(t, tb, panel, prefHeight);
	}
}
