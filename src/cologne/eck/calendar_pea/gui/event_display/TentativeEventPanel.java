package cologne.eck.calendar_pea.gui.event_display;

/*
 * PeaFactory - Production of Password Encryption Archives
 * Copyright (C) 2015  Axel von dem Bruch
 * 
 * This library is free software; you can redistribute it and/or modify it 
 * under the terms of the GNU General Public License as published 
 * by the Free Software Foundation; either version 2 of the License, 
 * or (at your option) any later version.
 * This library is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
 * or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU General Public License for more details.
 * See:  http://www.gnu.org/licenses/gpl-2.0.html
 * You should have received a copy of the GNU General Public License 
 * along with this library.
 */

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;


import javax.swing.JFrame;
import javax.swing.JPanel;

@SuppressWarnings("serial")
class TentativeEventPanel extends JPanel {

	public TentativeEventPanel() {

	}

	public static void main(String[] args) {
		JFrame f = new JFrame();
		f.add(new TentativeEventPanel());
		f.setSize(new Dimension(350, 200));
		f.setVisible(true);

	}
	@Override
	public void paintComponent(Graphics g) {
	    super.paintComponent(g);	    
	    Graphics2D g2 = (Graphics2D) g;
	    g2.setColor(Color.GRAY);
	   g.setFont(new Font(Font.SERIF, Font.BOLD, 80));
	    g.drawString("?", 5,10+80);
	}
}
