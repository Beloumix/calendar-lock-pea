package cologne.eck.calendar_pea.gui.event_display;

/*
 * PeaFactory - Production of Password Encryption Archives
 * Copyright (C) 2015  Axel von dem Bruch
 * 
 * This library is free software; you can redistribute it and/or modify it 
 * under the terms of the GNU General Public License as published 
 * by the Free Software Foundation; either version 2 of the License, 
 * or (at your option) any later version.
 * This library is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
 * or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU General Public License for more details.
 * See:  http://www.gnu.org/licenses/gpl-2.0.html
 * You should have received a copy of the GNU General Public License 
 * along with this library.
 */

import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.time.LocalTime;

import javax.swing.BorderFactory;

import cologne.eck.all_peas.gui.BorderButtonFocusListener;
import cologne.eck.calendar_pea.control.CalendarSetting;
import cologne.eck.calendar_pea.data.PEvent;
import cologne.eck.calendar_pea.gui.event_setting.EventSetter;
import cologne.eck.tools.UnexpectedValueException;

@SuppressWarnings("serial")
public class EventButton extends ComponentButton 
	implements MouseListener, KeyListener, Comparable<EventButton> {

	/*
	 * The time, this events starts at the day it is shown
	 */
	private LocalTime startTimeOfDay;
	/*
	 * The event, this button belongs to
	 */
	private PEvent e;
	
	// to prevent fluttering:
	private boolean infoShown = false;
	private boolean infoDisposed = false;
	
		
	/**
	 * Button for one event in MonthPanel: 
	 * Button text is summary of event, 
	 * button shows event values as tool tip,
	 * when clicked, EventSetter is opened
	 * 
	 * @param e	the event for this button
	 * @param startTime	 the time,the event of this button starts
	 */
	public EventButton(PEvent _e, LocalTime startTimeOfDay) {

		e = _e;

		this.startTimeOfDay = startTimeOfDay;
		// === create Button in panel for this event
		this.setText(e.getSummary() + "  "); // to display events without summary
		this.setFont(CalendarSetting.getEventButtonFont());
		//this.setMargin(new Insets(0,0,0,0));//1,1,1,1));
		// does not work: this.setMinimumSize(new Dimension(50, 20));
		Color eventColor = e.getCalendar().getColor();
		if (e.isAllDay()) {			
			this.setBackground(eventColor.darker());
			//this.setBorder(new LineBorder(Color.LIGHT_GRAY, 1));
			//this.setMargin(new Insets(5,5,5,5));//1,1,1,1));

		} else {
			this.setBackground(eventColor);
		}
		Color borderColor;
		int r = eventColor.getRed();
        int g = eventColor.getGreen();
        int b = eventColor.getBlue();
       // int alpha = eventColor.getAlpha();		

		if (e.getPriority() > 0) { // set border to show priority
			// check if eventColor is near to red:
			int diff = (255 - r) + g + b;
			if (diff > 225) { // set red
				borderColor = new Color(255,0,0);//Color.RED;
			} else { // set black
				borderColor = new Color(0,0,0);// Color.BLACK;//getAppropriateFontColor(eventColor);
			}
			this.setBorder(BorderFactory.createCompoundBorder(
					BorderFactory.createLineBorder(borderColor,(10 - e.getPriority()) ),// outside
					BorderFactory.createEmptyBorder( 3,3,3,3)));// inside: padding
			this.setBackground(new Color(r,g,b,255));// set not transparent
			this.addFocusListener(BorderButtonFocusListener.getInstance());
		}

		// set font white or black, depending on brightness of background
		this.setForeground(getAppropriateFontColor(eventColor));
		this.addMouseListener(this);
		
		// otherwise EventInfo is displayed far away from button if summary is long:
		this.setPreferredSize(new Dimension(100, (int) this.getPreferredSize().getHeight()));		
		
		this.setFocusable(true);
		this.addKeyListener(this); // for enter key
	}
	
	/**
	 * Get suitable foreground color for a given background:
	 * Black or white depending on the brightness
	 * 
	 * @param c	the background color
	 * @return	black or white depending on the background color
	 */
	public static Color getAppropriateFontColor(Color c) {
	    if ( Math.sqrt(
	      c.getRed() * c.getRed() * .241 +
	      c.getGreen() * c.getGreen() * .691 +
	      c.getBlue() * c.getBlue() * .068) > 130) {
	    	return Color.BLACK;
	    } else {
	    	return Color.WHITE;
	    }
	}
	
	/**
	 * Get the time, the event of this button starts
	 * 
	 * @return	the start time of the event as LocalTime
	 */
	public LocalTime getStartTimeOfDay() {
		return startTimeOfDay;
	}

	/**
	 * Set the time, the event of this button starts
	 * 
	 * @param	the start time of the event as LocalTime
	 */
	public void setStartTimeOfDay(LocalTime startTime) {
		this.startTimeOfDay = startTime;
	}

	/**
	 * Get the event for this EventButton
	 * 
	 * @return	the PEvent for this EventButton
	 */
	public PEvent getEvent() {
		return e;
	}

	/**
	 * Allow sorting of EventButtons by time
	 */
	@Override
	public int compareTo(EventButton eb) {
		// all-day events always on top:
		if (e.isAllDay()) {
			return -1;
		} else {		
			return this.startTimeOfDay.compareTo(eb.getStartTimeOfDay());
		}
	}

	@Override
	public void mouseClicked(MouseEvent arg0) {
		// close event info
		ComponentInfo.disposeInfo();		
		infoDisposed = true; 
		infoShown = false;
	}

	@Override
	public void mouseEntered(MouseEvent me) {

		// show event info
		if (infoShown == false) {
			EventInfo.setNewInfo(e, this);
			infoShown = true; 
			infoDisposed = false;
		}
	}

	@Override
	public void mouseExited(MouseEvent me) {

		// prevent fluttering:
		/* Rectangle r = me.getComponent().getBounds();
	     Point p = me.getPoint();
	     if ( p.x > 0 && p.x < (r.width )
	        &&  p.y > 0 && p.y < (r.height )) {
	        	return;
	     } */
		// close event info
		if (infoDisposed == false) {
			ComponentInfo.disposeInfo();	
			infoDisposed = true; 
			infoShown = false;
		}
	}

	@Override
	public void mousePressed(MouseEvent ae) {
		// close event info
		ComponentInfo.disposeInfo();	
		infoDisposed = true; 
		infoShown = false;
		// start the EventSetter for this event
		EventSetter setter = new EventSetter(this, //this.getLocationOnScreen(), 
				e, false, // event, newEvent = false
				null);		// no default date
		if (e.getCalendar().getEventList().size() == 0) {
			new UnexpectedValueException("int", "size of eventList", "is 0:"+ 
					e.getCalendar().getName() + " " + e.getCalendar().getCalUid()).printDescription();
		}
		setter.setVisible(true);
	}

	@Override
	public void mouseReleased(MouseEvent arg0) {}
	@Override
	public void keyPressed(KeyEvent ke) {

		if (ke.getKeyCode()==KeyEvent.VK_ENTER){

			ComponentInfo.disposeInfo();	
			infoDisposed = true; 
			infoShown = false;
			// start the EventSetter for this event
			EventSetter setter = new EventSetter(this, //this.getLocationOnScreen(), 
					e, false, // event, newEvent = false
					null);		// no default date
			setter.setVisible(true);
		}

	}
	@Override
	public void keyReleased(KeyEvent arg0) {}
	@Override
	public void keyTyped(KeyEvent arg0) {}

}
