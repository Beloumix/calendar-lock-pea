package cologne.eck.calendar_pea.gui.event_display;

/*
 * PeaFactory - Production of Password Encryption Archives
 * Copyright (C) 2015  Axel von dem Bruch
 * 
 * This library is free software; you can redistribute it and/or modify it 
 * under the terms of the GNU General Public License as published 
 * by the Free Software Foundation; either version 2 of the License, 
 * or (at your option) any later version.
 * This library is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
 * or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU General Public License for more details.
 * See:  http://www.gnu.org/licenses/gpl-2.0.html
 * You should have received a copy of the GNU General Public License 
 * along with this library.
 */

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.geom.Line2D;

import javax.swing.JPanel;

@SuppressWarnings("serial")
class CancelledEventPanel extends JPanel {

	public CancelledEventPanel() {
		this.setOpaque(true);
		//this.setBackground(Color.WHITE);
	}
	
	@Override
	public void paintComponent(Graphics g) {
	    super.paintComponent(g);   
	    
	    int width = this.getWidth();
	    int height = this.getHeight();
	    
	    Graphics2D g2 = (Graphics2D) g;
	    g2.setColor(Color.LIGHT_GRAY);
	    int stroke = 10;
        g2.setStroke(new BasicStroke(stroke));
        int margin = 10;
        g2.draw(new Line2D.Float(margin, margin + 30, width - margin, height - margin));
        g2.draw(new Line2D.Float(margin, height - margin, width - margin, margin + 30));
	}
}
