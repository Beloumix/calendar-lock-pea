package cologne.eck.calendar_pea.gui.event_display;

/*
 * PeaFactory - Production of Password Encryption Archives
 * Copyright (C) 2015  Axel von dem Bruch
 * 
 * This library is free software; you can redistribute it and/or modify it 
 * under the terms of the GNU General Public License as published 
 * by the Free Software Foundation; either version 2 of the License, 
 * or (at your option) any later version.
 * This library is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
 * or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU General Public License for more details.
 * See:  http://www.gnu.org/licenses/gpl-2.0.html
 * You should have received a copy of the GNU General Public License 
 * along with this library.
 */

import java.awt.Color;
import java.awt.Font;

import java.util.Arrays;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSeparator;
import javax.swing.JTextArea;

import cologne.eck.calendar_pea.control.CalendarSetting;
import cologne.eck.calendar_pea.data.CalendarComponent;
import cologne.eck.calendar_pea.data.PEvent;
import cologne.eck.calendar_pea.data.RecurringPEvent;
import cologne.eck.tools.TestLog;


class EventInfo { 

	// default font to use for label and text areas
	private static Font infoFont = new Font(Font.SANS_SERIF, Font.PLAIN, 12);

	private static int fixedInfoWidth = 350;// width of this

	
	/**
	 * Tool tip like info window to show infos about an event, 
	 * when a mouse enters an EventButton
	 * 
	 * @param e		the event to show
	 * @param eb	the affected EventButton
	 */
	protected static void setNewInfo(CalendarComponent _e, ComponentButton _eb) {
		
		PEvent e = null; 
		EventButton eb = null;
		if (_e instanceof PEvent) {
			e = (PEvent) _e;
		} else {
			TestLog.ve(EventInfo.class, "Missing event", 5);
			return;
		}
		if (_eb instanceof EventButton) {
			eb = (EventButton) _eb;
		} else {
			TestLog.ve(EventInfo.class, "Missing event button", 5);
			return;
		}		
		// background color depends an calendar color
		Color backgroundColor = e.getCalendar().getColor();
		backgroundColor = new Color(backgroundColor.getRed(),backgroundColor.getGreen(), backgroundColor.getBlue(), 127);
		// font color depends on brightness of background (black or white)
		Color fontColor = EventButton.getAppropriateFontColor(backgroundColor);

		JPanel panel = new JPanel();
		
		if (e.getStatus() != null) {
			if (e.getStatus().equals("CANCELLED")) {
				panel = new CancelledEventPanel();
			} else if (e.getStatus().equals("TENTATIVE")) {
				panel = new TentativeEventPanel();
			}
		}
		panel.setLayout(new BoxLayout(panel, BoxLayout.PAGE_AXIS));		

		// getPreferredSize does not work because of line wrap in text areas
		// -> collect heights of components
		double prefHeight = 0;

		// panel for date: one-day events: one date, several: two separated dates
		JPanel dateLabelPanel = new JPanel();
		dateLabelPanel.setLayout(new BoxLayout(dateLabelPanel, BoxLayout.LINE_AXIS));
		if (e.isSeveralDays()) { // show start at begin and end at end
			String dateString = CalendarSetting.getShortDateFormatter().format(e.getDateStart());
			JLabel dateStartLabel = new JLabel(dateString);
			dateStartLabel.setFont(infoFont);		
			dateLabelPanel.add(dateStartLabel);
			dateLabelPanel.add(Box.createHorizontalGlue());
			if (e.getDateEnd() != null) {
				JLabel dateEndLabel = new JLabel(CalendarSetting.getShortDateFormatter().format(e.getDateEnd()));	
				dateEndLabel.setFont(infoFont);
				dateLabelPanel.add(dateEndLabel);
			}
		} else { // one day event			
			JLabel dateLabel = new JLabel(CalendarSetting.getFullDateFormatter().format(e.getDateStart()));
			dateLabel.setFont(infoFont);		
			dateLabelPanel.add(dateLabel);
			dateLabelPanel.add(Box.createHorizontalGlue());
			prefHeight += dateLabelPanel.getPreferredSize().getHeight();
		}

		// time: all-day: no time, one-day: start and end time in one label
		// several days: start and end separated
		String endTime = null;
		if (e.getDateEnd() != null) {
			endTime = CalendarSetting.getTimeFormatter().format(e.getDateEnd());
		} else {
			// maybe recurrence rule sets date end
			//endTime = "unknown";
		}
		JPanel timeLabelPanel = null;
		if (! e.isAllDay() ) {
			timeLabelPanel = new JPanel();
			timeLabelPanel.setLayout(new BoxLayout(timeLabelPanel, BoxLayout.LINE_AXIS));

			if (e.isSeveralDays()) { // show start at begin and end at end
				JLabel timeLabel = new JLabel(CalendarSetting.getTimeFormatter().format(e.getDateStart()));
				timeLabel.setFont(infoFont);
				timeLabelPanel.add(Box.createHorizontalStrut(10));
				timeLabelPanel.add(timeLabel);
				timeLabelPanel.add(Box.createHorizontalGlue());
				if (e.getDateEnd() != null) {
					JLabel timeEndLabel = new JLabel(endTime);
					timeEndLabel.setFont(infoFont);
					timeLabelPanel.add(timeEndLabel);
				}
			} else {
				String startString = CalendarSetting.getTimeFormatter().format(e.getDateStart());
				if (endTime != null) {
					startString += " - " + endTime;
				}
				JLabel timeLabel = new JLabel(startString);
				timeLabel.setFont(infoFont);
				timeLabelPanel.add(Box.createHorizontalStrut(10));
				timeLabelPanel.add(timeLabel);
				timeLabelPanel.add(Box.createHorizontalGlue());
			}
			prefHeight += timeLabelPanel.getPreferredSize().getHeight();
		}
		// only for recurring events: show frequency
		JPanel dateTimeInfoPanel = null;
		if (e.isRecurring()) {
			if (e instanceof RecurringPEvent) {
				RecurringPEvent re = (RecurringPEvent) e;
				dateTimeInfoPanel = new JPanel();
				dateTimeInfoPanel.setLayout(new BoxLayout(dateTimeInfoPanel, BoxLayout.LINE_AXIS));

				JLabel dateTimeInfoLabel = null;			

				String dtInfo = null;
				if (re.getFrequencyString() != null) {
					dtInfo = CalendarSetting.calTranslate(re.getFrequencyString().toString());
				} else {
					dtInfo = CalendarSetting.calTranslate("recurring_event");
				}
				if (re.getCount() > 0) {
					dtInfo += " * " + (re.getCount() );
				} else if (re.getUntil() != null) {
					dtInfo += " " + CalendarSetting.calTranslate("until_date") + " " + CalendarSetting.getShortDateFormatter().format(re.getUntil());
				}
				if (re.getInterval() > 1) {
					dtInfo += ", " + CalendarSetting.calTranslate("interval") + ": " + re.getInterval();
				}
				dateTimeInfoLabel = new JLabel(dtInfo);
				dateTimeInfoLabel.setFont(infoFont);
				dateTimeInfoPanel.add(dateTimeInfoLabel);
				prefHeight += dateTimeInfoPanel.getPreferredSize().getHeight();
			} else {
				TestLog.ve(EventInfo.class, "Event is marked as recurring but is not: " + e.getSummary());
				e.setRecurring(false);
			}
		}
		// show summary always, event if null
		String summary = null;
		if (e.getSummary() == null) {
			summary = " ";
		} else {
			summary = e.getSummary();
		}
		JTextArea summaryArea = new JTextArea();
		summaryArea.setBackground(backgroundColor);
		summaryArea.setForeground(fontColor);
		summaryArea.setText(summary);
		summaryArea.setFont(new Font(infoFont.getFontName(), Font.BOLD, infoFont.getSize() + 2));
		summaryArea.setLineWrap(true);
		summaryArea.setWrapStyleWord(true);
		// estimate the number of lines
		int stringWidth = summaryArea.getFontMetrics(summaryArea.getFont()).stringWidth(summary);
		if (stringWidth > fixedInfoWidth) {
			// quick and dirty...
			double lines = stringWidth / fixedInfoWidth;// + 1;
			if (lines > 4) {
				//lines = lines +1;
			}
			prefHeight += summaryArea.getPreferredSize().getHeight() * lines;
		} else {
			prefHeight += summaryArea.getPreferredSize().getHeight();
		}
		// show description if exists
		JTextArea descriptionArea = null;		
		if (e.getDescription() != null && ( ! e.getDescription().equals(""))) {
			descriptionArea = new JTextArea();
			descriptionArea.setFont(infoFont);
			descriptionArea.setBackground(backgroundColor);
			descriptionArea.setForeground(fontColor);
			descriptionArea.setLineWrap(true);
			descriptionArea.setWrapStyleWord(true);
			descriptionArea.setText(e.getDescription());
			stringWidth = descriptionArea.getFontMetrics(infoFont).stringWidth(e.getDescription());
			if (stringWidth > fixedInfoWidth) {
				// quick and dirty...
				double lines = stringWidth / fixedInfoWidth;// + 1;
				prefHeight += descriptionArea.getPreferredSize().getHeight() * lines;
			} else {
				prefHeight += descriptionArea.getPreferredSize().getHeight();
			}
		}
		// show location if exists
		JTextArea locationArea = null;		
		if (e.getLocation() != null && ( ! e.getLocation().equals(""))) {
			locationArea = new JTextArea();
			locationArea.setFont(infoFont);
			locationArea.setBackground(backgroundColor);
			locationArea.setForeground(fontColor);
			locationArea.setLineWrap(true);
			locationArea.setWrapStyleWord(true);
			locationArea.setText(e.getLocation());
			stringWidth = locationArea.getFontMetrics(infoFont).stringWidth(e.getLocation());
			if (stringWidth > fixedInfoWidth) {
				// quick and dirty...
				double lines = stringWidth / fixedInfoWidth;// + 1;
				prefHeight += locationArea.getPreferredSize().getHeight() * lines;
			} else {
				prefHeight += locationArea.getPreferredSize().getHeight();
			}
		}
		JPanel statusAndPriorityPanel = new JPanel();
		statusAndPriorityPanel.setLayout(new BoxLayout(statusAndPriorityPanel, BoxLayout.LINE_AXIS));
		JLabel priorityLabel = null;
		int priority = e.getPriority();
		if (priority > 0) {
			char[] prio = new char[10- priority];
			Arrays.fill(prio, '*');
			priorityLabel = new JLabel(CalendarSetting.calTranslate("priority") 
					+ " " + String.valueOf(prio));
			priorityLabel.setForeground(Color.RED);
			prefHeight += priorityLabel.getPreferredSize().getHeight();
			statusAndPriorityPanel.add(priorityLabel);
			statusAndPriorityPanel.add(Box.createHorizontalGlue());
		}
		String status = e.getStatus();
		if (status != null) {
			if (status.equals("TENTATIVE")) {
				statusAndPriorityPanel.add(new JLabel(CalendarSetting.calTranslate("status")  + CalendarSetting.calTranslate("tentative")));
			} else if (status.equals("CANCELLED")) {
				statusAndPriorityPanel.add(new JLabel(CalendarSetting.calTranslate("status")  + CalendarSetting.calTranslate("cancelled")));
			} else if (status.equals("CONFIRMED")) {
				statusAndPriorityPanel.add(new JLabel(CalendarSetting.calTranslate("status")  + CalendarSetting.calTranslate("confirmed")));
			}  
		}
		
		panel.add(dateLabelPanel);
		if (timeLabelPanel != null) {
			panel.add(timeLabelPanel);
		}
		if (dateTimeInfoPanel != null) {
			panel.add(dateTimeInfoPanel);
		}
		panel.add(new JSeparator(JSeparator.HORIZONTAL));
		panel.add(summaryArea);
		if (descriptionArea != null) {
			panel.add(new JSeparator(JSeparator.HORIZONTAL));
			panel.add(descriptionArea);
		}
		if (locationArea != null) {
			panel.add(new JSeparator(JSeparator.HORIZONTAL));
			panel.add(locationArea);
		}
		if (priority > 0 || status != null) {
			panel.add(statusAndPriorityPanel);
		}
		// Show this info panel:
		ComponentInfo.setInfo(e, eb, panel, prefHeight);
	}
}
