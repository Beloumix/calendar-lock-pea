package cologne.eck.calendar_pea.gui.event_display;


/*
 * PeaFactory - Production of Password Encryption Archives
 * Copyright (C) 2021  Axel von dem Bruch
 * 
 * This library is free software; you can redistribute it and/or modify it 
 * under the terms of the GNU General Public License as published 
 * by the Free Software Foundation; either version 2 of the License, 
 * or (at your option) any later version.
 * This library is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
 * or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU General Public License for more details.
 * See:  http://www.gnu.org/licenses/gpl-2.0.html
 * You should have received a copy of the GNU General Public License 
 * along with this library.
 */

import java.awt.Component;
import java.awt.Dimension;
import java.awt.GraphicsConfiguration;
import java.awt.Point;

import javax.swing.JPanel;
import javax.swing.JWindow;

import cologne.eck.all_peas.control.PeaControl;
import cologne.eck.all_peas.gui.PswDialogView;
import cologne.eck.all_peas.vm_specific.JREProperties;
import cologne.eck.calendar_pea.data.CalendarComponent;
import cologne.eck.tools.TestLog;

@SuppressWarnings("serial")
public class ComponentInfo extends JWindow {

	/**
	 * Single instance of ComponentInfo
	 */
	private static ComponentInfo info = null;	
	
	private static int fixedInfoWidth = 350;// width of this
	private static int minHeight = 120; // minimum height
	private static int maxHeight = 500; // maximum height

	
	 /**
	  * Set EventInfo relative to EventButton, 
	  * but never cover the EventButton.
	  * If possible right beside and same y, 
	  * if not left beside / over EventButton. 
	  * This should also work for multiple screens
	  * on Linux (workaround)
	  * 
	  * @param c				Component to be relative to
	  * @param fixedInfoWidth	EventInfo width
	  * @param infoHeight		EventInfo height
	  */
	 private void setLocationRelativeNoCover(Component c, int fixedInfoWidth, int infoHeight) {
		 
		 int xLoc = 0;
		 int yLoc = 0;
		 int compWidth = 200;
		 int compHeight = 100;
		 
		 int priorY = 0;
		 if (c != null && c.isShowing()) {
			 priorY = c.getLocationOnScreen().y; 
			 super.setLocationRelativeTo(c);
			 //this.setVisible(true);
			 compWidth = c.getWidth();//.getPreferredSize().width;
			 compHeight = c.getHeight();//  c.getPreferredSize().height;
		 } else { // called from PAlarm
			 c = JREProperties.getMainWindow();
			 super.setLocationRelativeTo(c);
			 return;
			 //TestLog.ve(getClass(), (c == null) ? "component is null" : "component not showing");
			 //return;
		 }
		 this.setVisible(true);
		 
		 // now shift:
		 int x = this.getLocationOnScreen().x;
		 int y = this.getLocationOnScreen().y;
		 
		 GraphicsConfiguration config = c.getGraphicsConfiguration();
		 
		 if (config != null) {

			 xLoc = x + ((fixedInfoWidth + compWidth) / 2);// right beside
			 if (xLoc + fixedInfoWidth > (config.getBounds().width + config.getBounds().x) ) {
				 xLoc = x - ((fixedInfoWidth + compWidth) / 2);// left beside
			 }		 

			 yLoc = priorY ;// same
			 if (yLoc + infoHeight > (config.getBounds().height + config.getBounds().y) ) {
				 yLoc = y - ((infoHeight + compHeight) / 2);// over
			 }		 
		 } else {
			 TestLog.ve(getClass(), "Graphics configuration is null");
		 }
		 this.setLocation(xLoc, yLoc);		 
	 }	

	/**
	 * Dispose the current EventInfo or TodoInfo
	 */
	public static void disposeInfo() {
		if (info != null  && info.isDisplayable()) {
			info.dispose();
		}
	}
	
	/**
	 * Set the current EventInfo or TodoInfo: 
	 * Tool tip like info window to show infos about an event or todo, 
	 * when a mouse enters a button
	 * 
	 * @param cc			PEvent or PTodo
	 * @param cb			EventButton or TodoButton
	 * @param panel			the panel to add
	 * @param prefHeight	the preferred height depending on the calendar component
	 */
	protected static void setInfo(CalendarComponent cc, ComponentButton cb, JPanel panel, double prefHeight) {
		
		if (cc == null ||  panel == null) {
			TestLog.ve(ComponentInfo.class, "Parameter is null", 5);
			return;
		}
		if (info == null) {
			info = new ComponentInfo();
		}

		// Remove all and add new panel:
		JPanel contentPane = (JPanel) info.getContentPane();
		contentPane.removeAll();
		contentPane.add(panel);
		
		// set the height: minimum and maximum
		double infoHeight = prefHeight + 20;//some extra space for margins...
		if (infoHeight < minHeight) {
			infoHeight = minHeight;
		}
		if (infoHeight > maxHeight) {
			infoHeight = maxHeight;
		}
		info.setPreferredSize(new Dimension(fixedInfoWidth, (int)infoHeight));				
		info.pack();		
		//this.setLocationRelativeTo(eb);// unsuitable: covers the EventButton

		// set location manually
		if (cb != null) {
			Point ebLoc = cb.getLocationOnScreen(); // top-left on screen


			if (PswDialogView.getNumberOfScreens() < 2 || PeaControl.isWindowsOS() == true) {
				// Linux displays the EventInfo on wrong position if app is on the second screen
				double  screenHeight = PswDialogView.getScreenheight();
				double screenWidth = PswDialogView.getScreenwidth();
				int xLoc = ebLoc.x + (cb.getWidth() );// end of EventButton
				// get x
				if ((xLoc + fixedInfoWidth) > screenWidth) {
					//xLoc = ebLoc.x - ebWidth;
					xLoc = (int) (screenWidth - fixedInfoWidth);// move left to fit in screen
				}
				// get y
				int yLoc = ebLoc.y + (cb.getHeight() / 2); // middle of EventButton
				if ( (yLoc + infoHeight) > screenHeight) {
					yLoc = (int) (screenHeight - infoHeight);// set over screen end
					if ( ( ebLoc.x - fixedInfoWidth) > 0) { // set x on left side of EventButton if this is inside the screen
						xLoc = ebLoc.x - fixedInfoWidth;
					}
				}
				//TestLog.v(XXXX.class, "LOCATION: x: "+ xLoc + ", y: " + yLoc);
				info.setLocation(new Point(xLoc, yLoc));// in parent coordinates!
			} else { 
				// multiple screens: that meen problem especially under Linux... 
				// This is a workaround:
				info.setLocationRelativeNoCover(cb, fixedInfoWidth, (int) infoHeight);
			}		
		} else {
TestLog.ve(ComponentInfo.class,"show info from alarm " );
			info.setLocationRelativeTo(JREProperties.getMainWindow());
TestLog.ve(ComponentInfo.class,info.getSize().getWidth()+" " + info.getSize().getHeight() + " " + info.getLocationOnScreen().getX());
		}
		info.setVisible(true);
	}	
}