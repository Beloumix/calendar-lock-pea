package cologne.eck.calendar_pea.gui.event_display;

/*
 * PeaFactory - Production of Password Encryption Archives
 * Copyright (C) 2021  Axel von dem Bruch
 * 
 * This library is free software; you can redistribute it and/or modify it 
 * under the terms of the GNU General Public License as published 
 * by the Free Software Foundation; either version 2 of the License, 
 * or (at your option) any later version.
 * This library is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
 * or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU General Public License for more details.
 * See:  http://www.gnu.org/licenses/gpl-2.0.html
 * You should have received a copy of the GNU General Public License 
 * along with this library.
 */

import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.time.LocalDateTime;


import javax.swing.BorderFactory;

import cologne.eck.all_peas.gui.BorderButtonFocusListener;
import cologne.eck.calendar_pea.control.CalendarSetting;
import cologne.eck.calendar_pea.data.PTodo;
import cologne.eck.calendar_pea.gui.event_setting.TodoSetter;
import cologne.eck.tools.TestLog;
import cologne.eck.tools.UnexpectedValueException;

@SuppressWarnings("serial")
public class TodoButton extends ComponentButton 
	implements MouseListener, KeyListener {


	/*
	 * The event, this button belongs to
	 */
	private PTodo todo;
	
	// to prevent fluttering:
	private boolean infoShown = false;
	private boolean infoDisposed = false;
	
	/**
	 * Button for one CalendarComponent: 
	 * Button text is summary of todo, 
	 * button shows event values as tool tip,
	 * when clicked, ComponentSetter is opened
	 * 
	 * @param _t	the todo for this button
	 * @param _dueDateTime	 the due date time
	 */
	public TodoButton(PTodo _t, LocalDateTime _dueDateTime) {

		todo = _t;
		if (todo == null) {
			TestLog.ve(TodoButton.class, "Missing todo to show", 5);
			return;
		}

		//LocalDateTime dueDateTime = _dueDateTime;
		// === create Button in panel for this event
		this.setText(todo.getSummary() + "  "); // to display events without summary
		this.setFont(CalendarSetting.getEventButtonFont());
		//this.setMargin(new Insets(0,0,0,0));//1,1,1,1));
		// does not work: this.setMinimumSize(new Dimension(50, 20));
		Color eventColor = todo.getCalendar().getColor();
		this.setBackground(eventColor);

		Color borderColor;
		int r = eventColor.getRed();
        int g = eventColor.getGreen();
        int b = eventColor.getBlue();
       // int alpha = eventColor.getAlpha();		

		if (todo.getPriority() > 0) { // set border to show priority
			// check if eventColor is near to red:
			int diff = (255 - r) + g + b;
			if (diff > 225) { // set red
				borderColor = new Color(255,0,0);//Color.RED;
			} else { // set black
				borderColor = new Color(0,0,0);// Color.BLACK;//getAppropriateFontColor(eventColor);
			}
			this.setBorder(BorderFactory.createCompoundBorder(
					BorderFactory.createLineBorder(borderColor,(10 - todo.getPriority()) ),// outside
					BorderFactory.createEmptyBorder( 3,3,3,3)));// inside: padding
			this.setBackground(new Color(r,g,b,255));// set not transparent
			this.addFocusListener(BorderButtonFocusListener.getInstance());
		}

		// set font white or black, depending on brightness of background
		this.setForeground(getAppropriateFontColor(eventColor));
		this.addMouseListener(this);
		
		// otherwise EventInfo is displayed far away from button if summary is long:
		this.setPreferredSize(new Dimension(100, (int) this.getPreferredSize().getHeight()));		
		//this.setMinimumSize(new Dimension(10, (int) this.getPreferredSize().getHeight()));
		//this.setPreferredSize(new Dimension(100, (int) this.getPreferredSize().getHeight()));
		
		this.setFocusable(true);
		this.addKeyListener(this); // for enter key
	}
	
	public PTodo getTodo() {
		return this.todo;
	}

	@Override
	public void mouseClicked(MouseEvent arg0) {
		// close event info
		ComponentInfo.disposeInfo();		
		infoShown = false;
		infoDisposed = true;
	}

	@Override
	public void mouseEntered(MouseEvent me) {		
		//TestLog.v(getClass(), "Enter " + " - " + me.getX() + " " + me.getY() + " - " + me.getComponent().getBounds() );
		if (infoShown == false) {
			// show event info
			TodoInfo.setNewInfo(todo, this);
			infoShown = true;
			infoDisposed = false;
		}
	}

	@Override
	public void mouseExited(MouseEvent me) {

		if (infoDisposed == false) {
			// prevent fluttering: old version
			/*Rectangle r = me.getComponent().getBounds();
	     	Point p = me.getPoint();
	     	if ( p.x > 0 && p.x < (r.width )
	        	&&  p.y > 0 && p.y < (r.height )) {
	        		return;
	     	}*/
			// close event info
			ComponentInfo.disposeInfo();	
			infoShown = false;
			infoDisposed = true;
		}
	}

	@Override
	public void mousePressed(MouseEvent ae) {
		// close event info
		ComponentInfo.disposeInfo();	
		infoShown = false;
		infoDisposed = true;
		// start the EventSetter for this event
		TodoSetter setter = new TodoSetter(this, //this.getLocationOnScreen(), 
				todo, false, // event, newEvent = false
				null);		// no default date
		if (todo.getCalendar().getTodoList().size() == 0) {
			new UnexpectedValueException("int", "size of todoList", "is 0:"+ 
					todo.getCalendar().getName() + " " + todo.getCalendar().getCalUid()).printDescription();
		}
		setter.setVisible(true);
	}

	@Override
	public void mouseReleased(MouseEvent arg0) {}
	@Override
	public void keyPressed(KeyEvent ke) {

		if (ke.getKeyCode()==KeyEvent.VK_ENTER){

			ComponentInfo.disposeInfo();	
			infoShown = false;
			infoDisposed = true;
			// start the EventSetter for this event
			TodoSetter setter = new TodoSetter(this, //this.getLocationOnScreen(), 
					todo, false, // event, newEvent = false
					null);		// no default date
			setter.setVisible(true);
		}
	}
	
	@Override
	public void keyReleased(KeyEvent arg0) {}
	@Override
	public void keyTyped(KeyEvent arg0) {}
}
