package cologne.eck.calendar_pea.gui.event_display;

/*
 * PeaFactory - Production of Password Encryption Archives
 * Copyright (C) 2021  Axel von dem Bruch
 * 
 * This library is free software; you can redistribute it and/or modify it 
 * under the terms of the GNU General Public License as published 
 * by the Free Software Foundation; either version 2 of the License, 
 * or (at your option) any later version.
 * This library is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
 * or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU General Public License for more details.
 * See:  http://www.gnu.org/licenses/gpl-2.0.html
 * You should have received a copy of the GNU General Public License 
 * along with this library.
 */

import java.awt.Color;
import java.time.LocalTime;

import javax.swing.JButton;


@SuppressWarnings("serial")
abstract class ComponentButton extends JButton {

	/*
	 * The time, this events starts at the day it is shown
	 */
	private LocalTime startTimeOfDay;

	/**
	 * Get suitable foreground color for a given background:
	 * Black or white depending on the brightness
	 * 
	 * @param c	the background color
	 * @return	black or white depending on the background color
	 */
	static Color getAppropriateFontColor(Color c) {
	    if ( Math.sqrt(
	      c.getRed() * c.getRed() * .241 +
	      c.getGreen() * c.getGreen() * .691 +
	      c.getBlue() * c.getBlue() * .068) > 130) {
	    	return Color.BLACK;
	    } else { 
	    	return Color.WHITE;
	    }
	}
	
	/**
	 * Get the time, the event of this button starts
	 * 
	 * @return	the start time of the event as LocalTime
	 */
	public LocalTime getStartTimeOfDay() {
		return startTimeOfDay;
	}

	/**
	 * Set the time, the event of this button starts
	 * 
	 * @param	the start time of the event as LocalTime
	 */
	public void setStartTimeOfDay(LocalTime startTime) {
		this.startTimeOfDay = startTime;
	}
}
