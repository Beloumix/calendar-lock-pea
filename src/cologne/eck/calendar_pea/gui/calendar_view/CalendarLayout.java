package cologne.eck.calendar_pea.gui.calendar_view;

/*
 * PeaFactory - Production of Password Encryption Archives
 * Copyright (C) 2015  Axel von dem Bruch
 * 
 * This library is free software; you can redistribute it and/or modify it 
 * under the terms of the GNU General Public License as published 
 * by the Free Software Foundation; either version 2 of the License, 
 * or (at your option) any later version.
 * This library is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
 * or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU General Public License for more details.
 * See:  http://www.gnu.org/licenses/gpl-2.0.html
 * You should have received a copy of the GNU General Public License 
 * along with this library.
 */

import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Insets;
import java.awt.LayoutManager;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.ArrayList;

import javax.swing.JLabel;
import javax.swing.JLayeredPane;
import javax.swing.JPanel;
import javax.swing.JViewport;
import javax.swing.SwingConstants;
import javax.swing.border.MatteBorder;

import cologne.eck.calendar_pea.control.CalendarControl;
import cologne.eck.calendar_pea.control.CalendarSetting;
import cologne.eck.calendar_pea.data.PEvent;
import cologne.eck.calendar_pea.gui.LockFrameCalendar;
import cologne.eck.calendar_pea.gui.event_display.EventButton;
import cologne.eck.tools.TestLog;
import cologne.eck.tools.UnexpectedValueException;

class CalendarLayout implements LayoutManager {

	private JLayeredPane container;

	private double width;
	private double height;

	private double oneHourHeight;
	private double oneMinuteHeight;

	private static final MatteBorder border = new MatteBorder(1,0,0,0, Color.LIGHT_GRAY);
	
	// label with hour 0 - 23
	private JLabel[] hourLabel = new JLabel[24];


	public CalendarLayout() {
	}

	@Override
	public void addLayoutComponent(String arg0, Component arg1) {}

	@Override
	public void layoutContainer(Container _container) {

		JLayeredPane viewPanel = null;
		DayPanel dayPanel = null;
		WeekPanel weekPanel = null;
		
		if (_container.getParent() instanceof JViewport) { // DayView	

			JViewport viewScroll = (JViewport) _container.getParent();

			if (! (viewScroll.getView() instanceof JLayeredPane) ) {
				TestLog.e(CalendarLayout.class, "Invalid container for CalendarLayout: "  + viewScroll.getView().getClass().getName());
				new UnexpectedValueException("viewScroll.getView()", "Component", "is not instance of JLayeredPane: " + _container.getClass().getName()).printStackTraceLines();
			}
			
			viewPanel = (JLayeredPane) viewScroll.getView();
			dayPanel = (DayPanel) viewPanel.getParent().getParent().getParent();
			if (dayPanel == null) {
				new UnexpectedValueException("dayPanel", "DayPanel","is null").printDescription();
				return;		
			}

			// set background color
			if (dayPanel.getFirstShownDate().equals(LocalDate.now())) {
				viewScroll.setBackground(CalendarSetting.getTodayColor());
			} else {
				viewScroll.setBackground(CalendarSetting.getDayBackgroundColor());
			}
			
		} else { // WeekView

			if (_container instanceof JLayeredPane) {
				viewPanel = (JLayeredPane) _container;
			} else {
				new UnexpectedValueException("viewPanel", "JLayeredPane","is not instance of JLayeredPane").printDescription();
				return;
			}
			if (viewPanel.getParent() instanceof DayPanel) {
				dayPanel = (DayPanel) viewPanel.getParent();
				if (dayPanel == null) {
					new UnexpectedValueException("dayPanel", "DayPanel","is null").printDescription();
					return;		
				}
			}
			if (LockFrameCalendar.getView() instanceof WeekPanel) {
				weekPanel = (WeekPanel) LockFrameCalendar.getView();
			}
		}
		this.container = viewPanel;//(JLayeredPane) ((JViewport)_container.getParent()).getView();
		
		if (dayPanel == null) {
			new UnexpectedValueException("dayPanel", "DayPanel","is null").printDescription();
			return;		
		}
		
		// get the date to show:
		LocalDate date = dayPanel.getFirstShownDate();//LocalDate.now();
		//TestLog.o(XXXX.class, "Layout Date: " + date + ", week :  " + (LockFrameContact.getView() instanceof WeekPanel));
		if (date == null) {
			//date = LocalDate.now();
			new UnexpectedValueException("dayPanel", "DayPanel","is null").printDescription();
			String stack = "StackTrace: \n";
			for (StackTraceElement ste : Thread.currentThread().getStackTrace()) {
			    stack += ste + "\n";
			}
			TestLog.e(CalendarLayout.class, stack);
			return;	
		}
		//LocalDateTime dayStart = date.atStartOfDay();
		LocalDateTime dayEnd = date.atTime(LocalTime.MAX);
		

		// clear the container: remove all components
		container.removeAll();

		// get width and height:
		Insets insets = viewPanel.getInsets();
		width = viewPanel.getSize().getWidth() 
				- (insets.left + insets.right);
		height = viewPanel.getSize().getHeight()
				- (insets.top + insets.bottom);


		if (dayPanel.isInWeekView() == false) {		
			dayPanel.setVerticalScrollBar((int) (oneHourHeight * CalendarSetting.getViewStartHour() ));
		} else {	
			weekPanel.setVerticalScrollBar((int) (oneHourHeight * CalendarSetting.getViewStartHour() ));	
		}
		
		// get height for hour and minute:
		oneHourHeight = height / 24;
		oneMinuteHeight = height / (24 * 60);

		// draw hours and lines in background
		int labelY = 0;//(int)oneHourHeight;

		//MatteBorder border = new MatteBorder(1,0,0,0, Color.LIGHT_GRAY);
		//container.add(new JLabel(""), 0);
		for (int i = 0; i < 24; i++) {		
			JLabel label = new JLabel("" + i );
			label.setForeground(Color.GRAY);
			//label.setBackground(CalendarSetting.getDayBackgroundColor());
			label.setVerticalAlignment(SwingConstants.TOP);
			label.addMouseListener(dayPanel);
			label.setBorder(border);
			label.setBounds(0, labelY, (int)width, (int)oneHourHeight);

			container.add(label, 1);
			
			labelY += oneHourHeight;
			// for easy access
			hourLabel[i] = label;			
		}

		width -= 15;// small space right and vertical scroll bar

		//==============================================================================
		

		// get list of calendars 
		//ArrayList<PCard> calList = CalendarControl.getShownCalendarList();
		//int calSize = calList.size();
		// create list to collect EventButtons for this day
		ArrayList<EventButton> eventList = null;
		// extra list for all-day events:
		ArrayList<EventButton> allDayEventList = null;

		if (dayPanel.isInWeekView()) {
			ArrayList<ArrayList<EventButton>> lists = weekPanel.getNextDayList();
			if (lists== null) {
				eventList = new ArrayList<EventButton>();
				allDayEventList = new ArrayList<EventButton>();
			} else {
				allDayEventList = lists.get(0);
				eventList = lists.get(1);
			}

		} else {

			// create list to collect EventButtons for this day
			eventList = new ArrayList<EventButton>();
			// extra list for all-day events:
			allDayEventList = new ArrayList<EventButton>();
			
			ArrayList<ArrayList<ArrayList<EventButton>>> lists = CalendarControl.getSortedShownEventButtonList(date, 1);
			// there is only one day...
			if (lists != null && lists.get(0) != null) {
				if (lists.get(0).get(1) != null) {
					eventList = lists.get(0).get(1);
				}
				if (lists.get(0).get(0) != null) {
					allDayEventList = lists.get(0).get(0);
				}
			}
		}
		//=====================================================================
		// get position and size for EventButtons
		int eventListSize = eventList.size();

		// arrays to store value at index of eventList
		int[] xLocs = new int[eventListSize];
		int[] yLocs = new int[eventListSize];
		int[] widths = new int[eventListSize];
		int[] heights = new int[eventListSize];

		// event indices with overlapping times
		ArrayList<Integer> group = new ArrayList<Integer>();
		// the current maximum end time of the group
		LocalTime lastEndTimeOfGroup = null;

		for (int eventIndex = 0; eventIndex < eventListSize; eventIndex++) {	

			EventButton eb = eventList.get(eventIndex);
			LocalTime eventStartTime = eb.getStartTimeOfDay();			
			//TestLog.o(XXXX.class, "Event " + eventStartTime + " - " + eb.getEvent().getSummary());
			PEvent e = eb.getEvent();
			
			//>>>>>>>>>> Y position			
			
			int yLoc = 0;			
			// labels and buttons resizes different -> set according to hour label
			Component comp = hourLabel[eventStartTime.getHour()];
			if (comp == null) {
				new UnexpectedValueException("comp", "Component", " is null").printDescription();
				yLoc =  (int)((eventStartTime.getHour() * oneHourHeight) + (eventStartTime.getMinute() * oneMinuteHeight));
			} else {
				yLoc =  (int) (comp.getY() + eventStartTime.getMinute() * oneMinuteHeight);
			}
			yLocs[eventIndex] = yLoc;
			//TestLog.o(XXXX.class, eb.getEvent().getSummary() + " - " + date + ", oneHourHeight: " + oneHourHeight);
			//TestLog.o(XXXX.class, "start: " + eventStartTime + ", compText: " + ((JLabel) comp).getText() + ",comp.getY: " + comp.getY() + ", yLoc: " + yLoc);
			
			//>>>>>>>>>> Height
			LocalTime eventEndTime = null;
			if (e.isAllDay()
					|| ( e.isSeveralDays() && e.getDateEnd().isAfter(dayEnd)) ) { // several days
				eventEndTime = LocalTime.MAX;//23:59
			} else {
				if (e.getDateEnd() == null) {
					TestLog.ve(CalendarLayout.class, "Missing end date time of event", 5);
					TestLog.ve(CalendarLayout.class, "Set one hour later");
					e.setDateEnd(e.getDateStart().plusHours(1));
				}
				eventEndTime = e.getDateEnd().toLocalTime();
			}
			//comp = container.getComponentAt(10,  (int)(eventEndTime.getHour() * (oneHourHeight - 1)));
			comp = hourLabel[eventEndTime.getHour()];// component for end hour
			if (comp == null) {
				new UnexpectedValueException("comp", "Component", " is null").printDescription();
				heights[eventIndex] = yLoc -  (int)((eventEndTime.getHour() * oneHourHeight) + (eventEndTime.getMinute() * oneMinuteHeight));
			} else {
				//TestLog.o(XXXX.class, "height: " + heights[eventIndex] );
				//TestLog.o(XXXX.class, ((JLabel)comp).getText() + "  compY: " + comp.getY() + " , eventEndTime.getmin: " + eventEndTime.getMinute() + ", yLoc: " + yLoc );
				//TestLog.o(XXXX.class,  comp.getY() + " + " + eventEndTime.getMinute() + " * " + oneMinuteHeight + " - " + yLoc);
				heights[eventIndex] = (int) ((comp.getY() - yLoc) + eventEndTime.getMinute() * oneMinuteHeight);
			}			

			//>>>>>>>>>>> Width and x location
			
			if (lastEndTimeOfGroup == null) { // start: first event
				group.clear();
				//start new group:
				group.add(eventIndex);
				lastEndTimeOfGroup = eventEndTime;
				continue;
			}
			
			if (eventStartTime.isAfter(lastEndTimeOfGroup)
					|| eventStartTime.equals(lastEndTimeOfGroup)) { // close group
				// set xLoc and width for this event
				xLocs[eventIndex] = 0;
				widths[eventIndex] = (int) width - 25;
				// set width and xLocs in group:
				int groupSize = group.size();
				if (groupSize > 0) { // previous group is closed
					// get width for all events in group
//					int groupWidth = (int) (width / groupSize);
					// set width for all events in group
					
					//---------------------------------------------------------------
					// ?? TODO ??
					// if previous column is free: set overlapping event in previous column
					// create ArrayList with all end times of a group and check where to set a new event of the group...

	
					//---------------------------------------------------------------
					int groupWidth = (int) ((width - 25) / groupSize);
					for (int i = 0; i < groupSize; i++) {
						// get index of event in group and set width
						widths[group.get(i)] = groupWidth;
						//TestLog.o(XXXX.class, "widht set for index " + group.get(i) + ": " + groupWidth);
						// set xLocs:
						xLocs[group.get(i)] = groupWidth * i;
					}
				}
				group.clear();
				//start new group:
				group.add(eventIndex);
				lastEndTimeOfGroup = eventEndTime;
				continue;
			} else {
				// previous group: add this event to the group:
				group.add(eventIndex);
				if (eventEndTime.isAfter(lastEndTimeOfGroup)) {
					lastEndTimeOfGroup = eventEndTime;
				}
			}
		}
		// set width and xLocs in last group:
		int groupSize = group.size();
		if (groupSize > 0) { // previous group is closed
			// get width for all events in group
			int groupWidth = (int) ((width - 25) / groupSize);
			// set width for all events in group
			for (int i = 0; i < groupSize; i++) {
				// get index of event in group and set width
				widths[group.get(i)] = groupWidth;
				//TestLog.o(XXXX.class, "widht set for index " + group.get(i) + ": " + groupWidth);
				// set xLocs:
				xLocs[group.get(i)] = groupWidth * i;
			}
			group.clear();
		}
		
		//====================================================================
		// set EventButtons
		for (int eventIndex = 0; eventIndex < eventListSize; eventIndex++) {
			EventButton eb = eventList.get(eventIndex);
			// add to container and set position and size
			container.add(eb, 2);
			eb.setBounds(25 + xLocs[eventIndex],  yLocs[eventIndex], //y, //(int)oneHourHeight + yLocs[eventIndex], 
					widths[eventIndex], heights[eventIndex]);
			//TestLog.o(XXXX.class, eb.getEvent().getSummary() + ", bounds: " + eb.getBounds());
		}
		container.setLayer(hourLabel[0],0);
		container.setLayer(hourLabel[23],0);
			
		//======================================================================
		// set all-day EventButtons

		JPanel allDayPanel = dayPanel.getAllDayPanel();
		if (allDayPanel == null) {
			TestLog.e(CalendarLayout.class, "Missing allDayPanel...");
			new UnexpectedValueException("allDayPanel", "JPanel", "is null ").printStackTraceLines();
		} else {
			allDayPanel.removeAll();
			//allDayPanel.setPreferredSize(new Dimension((int) width, allDayPanel.getSize().height));
		}
		int allDayListSize = allDayEventList.size();
		if (allDayListSize > 0) {
			int w = (int) (width / allDayListSize );// don't use full width: last button will not be shown
			for (int i = 0; i < allDayListSize; i++) {
				EventButton eb = allDayEventList.get(i);				
				eb.setMargin(new Insets(5,0,5,0));
				//TestLog.o(XXXX.class, i+" width: " + width + ", allDayListSize: " + allDayListSize + ", w: " +w);
				eb.setPreferredSize(new Dimension(w , (int) oneHourHeight));
				eb.setMinimumSize(new Dimension(w , (int) oneHourHeight));
				eb.setMaximumSize(new Dimension(w , (int) oneHourHeight));
				//		container.add(eb, 2);
				//		eb.setBounds(w * i, 0, w, (int) oneHourHeight);
				allDayPanel.add(eb);
			}
			allDayPanel.validate();
			allDayPanel.repaint();
		}		
	}

	@Override
	public Dimension minimumLayoutSize(Container parent) {
		return new Dimension (200,200);
	}

	@Override
	public Dimension preferredLayoutSize(Container parent) {
			return new Dimension(500,400);
	}
	@Override
	public void removeLayoutComponent(Component comp) {}
}
