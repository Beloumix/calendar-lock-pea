package cologne.eck.calendar_pea.gui.calendar_view;

/*
 * PeaFactory - Production of Password Encryption Archives
 * Copyright (C) 2015  Axel von dem Bruch
 * 
 * This library is free software; you can redistribute it and/or modify it 
 * under the terms of the GNU General Public License as published 
 * by the Free Software Foundation; either version 2 of the License, 
 * or (at your option) any later version.
 * This library is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
 * or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU General Public License for more details.
 * See:  http://www.gnu.org/licenses/gpl-2.0.html
 * You should have received a copy of the GNU General Public License 
 * along with this library.
 */

import java.awt.Component;
import java.awt.Dimension;
import java.awt.Point;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.time.LocalDate;
import java.time.format.TextStyle;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JLabel;
import javax.swing.JLayeredPane;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollBar;
import javax.swing.JScrollPane;
import javax.swing.border.EmptyBorder;

import cologne.eck.all_peas.data.PeaProperties;
import cologne.eck.all_peas.gui.PeaDialog;
import cologne.eck.all_peas.gui.PswDialogView;
import cologne.eck.all_peas.gui.ScrollbarAction;
import cologne.eck.calendar_pea.control.CalendarControl;
import cologne.eck.calendar_pea.control.CalendarSetting;
import cologne.eck.calendar_pea.gui.LockFrameCalendar;
import cologne.eck.calendar_pea.gui.event_display.ComponentInfo;
import cologne.eck.calendar_pea.gui.event_setting.EventSetter;


@SuppressWarnings("serial")
public class DayPanel extends JPanel 
	implements CalendarView, MouseListener {


	/*
	 * The date of this DayPanel
	 */
	private LocalDate firstShownDate;	
	/*
	 * Label to display the currently shown date
	 */
	private JLabel dateLabel;	
	/*
	 * panel to show events except all-day events
	 */
	private JLayeredPane viewPanel;
	/*
	 * extra panel for all-day events
	 */
	private JPanel allDayPanel;
	/*
	 * Scroll bar to set the view port
	 */
	private JScrollBar verticalBar;	
	/**
	 * True if this DayPanel is part of a WeekPanel:
	 * show the date of this panel in short (true) or long (false) version, 
	 */
	private boolean inWeekView;
	

	/**
	 * panel to show event of a day: 
	 * extra panel for all-day events
	 * 
	 * @param _date	the date of this panel
	 * @param dim  the preferred size of this DayPanel
	 * @param inWeekView true if this panel is part of a WeekPanel
	 */
	public DayPanel(LocalDate _date, Dimension dim, boolean _inWeekView) {

		
		this.firstShownDate = _date;
		this.inWeekView = _inWeekView;
		this.setLayout(new BoxLayout(this, BoxLayout.PAGE_AXIS));

		if (inWeekView == false) {
			dateLabel = new JLabel(CalendarSetting.getFullDateFormatter().format(firstShownDate));
			this.add(dateLabel);
			this.add(Box.createVerticalStrut(10));
		} else {			
			dateLabel = new JLabel(
					firstShownDate.getDayOfWeek().getDisplayName( TextStyle.SHORT, PeaProperties.getLocale()) 
					+ " " + CalendarSetting.getShortDateFormatter().format(firstShownDate));
			// set date to open DayView
			dateLabel.setName("dateLabel");
			dateLabel.addMouseListener(this);
			this.setBackground(CalendarSetting.getDayBackgroundColor());
		}

		// add the panel for all-day events: 
		// this is filled in layout of viewPanel
		allDayPanel = new JPanel();
		allDayPanel.setLayout(new BoxLayout(allDayPanel, BoxLayout.LINE_AXIS));
		allDayPanel.setBorder(new EmptyBorder(0,0,0,0));
		// all size settings are required
		allDayPanel.setMinimumSize(new Dimension(50, 30));
		allDayPanel.setPreferredSize(new Dimension(100, 30));
		allDayPanel.setMaximumSize(new Dimension((int) PswDialogView.getScreenwidth(), 30));// required
		
		if (inWeekView == false) {
			this.add(allDayPanel);
			this.add(Box.createVerticalStrut(2));
		}
		
		// viewPanel for non all-day events
		viewPanel = new JLayeredPane();
		
		CalendarLayout layout = new CalendarLayout();
		viewPanel.setLayout(layout);//BoxLayout(this, BoxLayout.PAGE_AXIS));		
		viewPanel.setBorder(new EmptyBorder(0,0,0,0));
		// set the view of the view port:
		viewPanel.setPreferredSize(new Dimension(dim.width - (dim.width / 4), dim.height + (dim.height / 2)));

		if (inWeekView == true) {
			this.add(viewPanel);
		} else {
			JScrollPane viewScroll = new JScrollPane(viewPanel);
			viewScroll.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
			viewScroll.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
			viewScroll.setPreferredSize(dim);//new Dimension((dim.width ),dim.height + 500));
			verticalBar = viewScroll.getVerticalScrollBar();
			
			// register keys for keyboard access: left, right,up, down
			ScrollbarAction.getScrollPaneKeyboardAccess(this, viewScroll, false);
			this.add(viewScroll);
		}

		this.setPreferredSize(dim);
		this.addMouseListener(this);
	}
	
	
	@Override
	public void refreshView() {
		if (inWeekView == false) {
			dateLabel.setText(CalendarSetting.getFullDateFormatter().format(this.firstShownDate));
		} else {
			dateLabel.setText(this.firstShownDate.getDayOfWeek().getDisplayName( TextStyle.SHORT, PeaProperties.getLocale()) 
					+ " " + CalendarSetting.getShortDateFormatter().format(this.firstShownDate));

		}
		if  (this.firstShownDate.equals(LocalDate.now())) {
			dateLabel.setOpaque(true);
			dateLabel.setBackground(CalendarSetting.getTodayColor());
		} else {
			dateLabel.setOpaque(false);
			dateLabel.setBackground(this.getBackground());
		}
		viewPanel.getLayout().layoutContainer(viewPanel);
	}
	/**
	 * Set the vertical scroll bar of the view port
	 * of the viewPanel. This will set the first shown hour.
	 * 
	 * @param value	the first shown hour
	 */
	public void setVerticalScrollBar(int value) {
		verticalBar.setValue(value);
	}

	/**
	 * Get the panel for all-day events
	 * 
	 * @return	the panel for all-day events
	 */
	public JPanel getAllDayPanel() {
		return this.allDayPanel;
	}

	@Override
	public void mouseClicked(MouseEvent arg0) {
		// close event info
		ComponentInfo.disposeInfo();	
	}

	@Override
	public void mouseEntered(MouseEvent e) {
		// close event info
		ComponentInfo.disposeInfo();	
	}

	@Override
	public void mouseExited(MouseEvent e) {
		// close event info
		ComponentInfo.disposeInfo();	
	}

	@Override
	public void mousePressed(MouseEvent e) {
		// close event info
		ComponentInfo.disposeInfo();	
		Point p = e.getPoint();
		Component c  =  (Component) e.getSource();
		int hour = 0;
		if (c instanceof JLabel) {
			if (c.getName() != null && c.getName().equals("dateLabel")){ // date label
				// -> ask to change to day view
				int result = PeaDialog.showQuestion(c, 
						CalendarSetting.calTranslate("ask_open_day_view"),
						//"Open day view for this date?", 
						null, 0, -1);
				if (result == 0) {					
					LockFrameCalendar.getView().setFirstShownDate(this.firstShownDate);// to start DayPanel with this date
					LockFrameCalendar.getFrame().changeView(0);//"dayView");
				}
				
			} else { // time label -> start EventSetter
				
				if (CalendarControl.getShownCalendarList() == null
						|| CalendarControl.getShownCalendarList().isEmpty()) {
					JOptionPane.showMessageDialog(this, CalendarSetting.calTranslate("calendar_missing"));			
					return;
				} else {

					hour = Integer.parseInt(((JLabel) e.getSource()).getText());

					int height = c.getHeight();
					double minuteGap =  60 /(double)height ;
					int minute = (int) ( p.getY()* minuteGap);
					EventSetter setter = new EventSetter(LockFrameCalendar.getFrame(), null, true, firstShownDate.atTime(hour, minute));
					setter.setVisible(true);
				}
			}
		}
	}

	@Override
	public void mouseReleased(MouseEvent e) {}

	@Override
	public LocalDate getFirstShownDate() {
		return this.firstShownDate;
	}
	/**
	 * Set the date for this day view.
	 * The view needs to be refreshed. 
	 * 
	 * @param newDate	the date for this day view
	 */
	public void setFirstShownDate(LocalDate newDate) {
		if (inWeekView == false) {
			dateLabel.setText(CalendarSetting.getFullDateFormatter().format(firstShownDate));
		} else {
			dateLabel.setText(CalendarSetting.getShortDateFormatter().format(firstShownDate));
		}
		this.firstShownDate = newDate;
	}
	
	public JLayeredPane getViewPanel() {
		return this.viewPanel;
	}
	public JLabel getDateLabel() {
		return this.dateLabel;
	}
	public boolean isInWeekView() {
		return this.inWeekView;
	}

	@Override
	public JPanel getPanel() {
		return this;
	}
}
