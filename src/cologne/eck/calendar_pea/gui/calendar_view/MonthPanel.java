package cologne.eck.calendar_pea.gui.calendar_view;

/*
 * PeaFactory - Production of Password Encryption Archives
 * Copyright (C) 2015  Axel von dem Bruch
 * 
 * This library is free software; you can redistribute it and/or modify it 
 * under the terms of the GNU General Public License as published 
 * by the Free Software Foundation; either version 2 of the License, 
 * or (at your option) any later version.
 * This library is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
 * or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU General Public License for more details.
 * See:  http://www.gnu.org/licenses/gpl-2.0.html
 * You should have received a copy of the GNU General Public License 
 * along with this library.
 */

import java.awt.Dimension;
import java.awt.GridLayout;
import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.format.TextStyle;
import java.util.ArrayList;
import java.util.Locale;

import javax.swing.BoxLayout;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

import cologne.eck.all_peas.data.PeaProperties;
import cologne.eck.all_peas.gui.ScrollbarAction;
import cologne.eck.calendar_pea.control.CalendarControl;
import cologne.eck.calendar_pea.control.CalendarSetting;
import cologne.eck.calendar_pea.gui.event_display.EventButton;


@SuppressWarnings("serial")
public class MonthPanel extends JPanel 
		implements CalendarView {

	/*
	 * The panel that holds the view: here daysPanel
	 */
	private JPanel viewPanel;
	/*
	 * The panel that contains one panel for each day.
	 * Get one singleDayPanel: 
	 * JPanel singleDayPanel = (JPanel) ((JViewport)((JScrollPane)daysPanel.getComponent(index)).getViewport()).getView();
	 */
	private JPanel daysPanel = null;
	/*
	 * The date of the first day panel
	 */
	private LocalDate firstShownDate;
	
	private JPanel monthPanel;

	/**
	 * Month view
	 * 
	 * @param _date		date to start
	 * @param dim		dimension of this panel
	 */
	public MonthPanel(LocalDate _date, Dimension dim) {
		
		this.monthPanel = this;
		if (_date == null) {
			//new UnexpectedValueException(null,null,null).printStackTraceLines();
			firstShownDate = LocalDate.now();
		} else {
			firstShownDate = _date;
		}		
		this.setLayout(new BoxLayout(this, BoxLayout.PAGE_AXIS));
		
		// first line: name of weekdays
		JPanel weekdaysPanel = new JPanel();
		weekdaysPanel.setMaximumSize(new Dimension(10000, 30));
		GridLayout weekdaysGridLayout = new GridLayout(1,7);// 1 line, 7 rows
		weekdaysPanel.setLayout(weekdaysGridLayout);
		Locale locale = PeaProperties.getLocale();
		weekdaysPanel.add(new JLabel(DayOfWeek.MONDAY.getDisplayName( TextStyle.FULL, locale)));
		weekdaysPanel.add(new JLabel(DayOfWeek.TUESDAY.getDisplayName( TextStyle.FULL, locale)));
		weekdaysPanel.add(new JLabel(DayOfWeek.WEDNESDAY.getDisplayName( TextStyle.FULL, locale)));
		weekdaysPanel.add(new JLabel(DayOfWeek.THURSDAY.getDisplayName( TextStyle.FULL, locale)));
		weekdaysPanel.add(new JLabel(DayOfWeek.FRIDAY.getDisplayName( TextStyle.FULL, locale)));
		weekdaysPanel.add(new JLabel(DayOfWeek.SATURDAY.getDisplayName( TextStyle.FULL, locale)));
		weekdaysPanel.add(new JLabel(DayOfWeek.SUNDAY.getDisplayName( TextStyle.FULL, locale)));
		this.add(weekdaysPanel);

		// Create the panel with days and events
		viewPanel = new JPanel();
		viewPanel.setLayout(new BoxLayout(viewPanel, BoxLayout.PAGE_AXIS));
		// create the daysPanel
		createDaysPanel(firstShownDate);
		viewPanel.add(daysPanel);
		this.add(viewPanel);
		
		if (dim != null) {
			this.setPreferredSize(dim);
		}
	}
	
	/**
	 * Create the dayPanel for a given start date
	 * 
	 * @param dayDate	the date to start the dayPanel, if this is not
	 * 					Monday, then this date is not the first day
	 */
	private final void createDaysPanel(LocalDate dayDate) {

		//===============================================
		// create the panel that holds the singleDayPanels
		daysPanel = new JPanel();
		// GridLayout for singleDayPanels:
		GridLayout gridLayout = new GridLayout(0,7);//  7 rows, lines are created automatically
		daysPanel.setLayout(gridLayout);
		//daysPanel.setBackground(new Color(237, 237, 222));

		//=====================================
		// Get the first and last date to show:
		if (dayDate == null) {
			// default: now
			dayDate = LocalDate.now();
		}
		// set day to right week day:
		int dayOfWeek = dayDate.getDayOfWeek().getValue();
		dayDate = dayDate.minusDays(dayOfWeek - 1);
		firstShownDate = dayDate; // always a Monday!
		//LocalDateTime viewBeginDateTime = firstShownDate.atStartOfDay();
		//LocalDateTime endDateTime = viewBeginDateTime.plusDays(CalendarSetting.getNumberLinesInMonthPanel() * 7);

		//========================================================
		// Create the singleDayPanels with the labels for the date, but without events:
		// the number of days to show in the MonthPanel:
		int numberOfDays = CalendarSetting.getNumberLinesInMonthPanel() * 7;
		SingleDayPanel[] singleDayPanel = new SingleDayPanel[numberOfDays];
		// text to show in first line
		//String dateText = null;
		for (int i = 0; i < numberOfDays; i++) {
			singleDayPanel[i] = new SingleDayPanel(dayDate);
			dayDate = dayDate.plusDays(1);
			// add a horizontal scroll pane
			JScrollPane scrollPane = new JScrollPane(singleDayPanel[i]);
			scrollPane.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);	
			
			// register keys for keyboard access: left, right,up, down
			ScrollbarAction.getScrollPaneKeyboardAccess(this, scrollPane, false);
			// add this day:
			daysPanel.add(scrollPane);	
		}

		//==========================================
		// get all events from all calendars to show as list of list of EventButton list
		ArrayList<ArrayList<ArrayList<EventButton>>> eventButtonLists = CalendarControl.getSortedShownEventButtonList(firstShownDate, numberOfDays);
		
		//==========================================
		// add the EventButtons:
		for (int dayPanelIndex = 0; dayPanelIndex < numberOfDays; dayPanelIndex++) {
			// this list contains a list for all-day events and a list for time-events
			ArrayList<ArrayList<EventButton>> oneDayLists = eventButtonLists.get(dayPanelIndex);
			if (oneDayLists != null && oneDayLists.isEmpty() == false) {
				// the SingleDayPanel to add to:
				SingleDayPanel sdp = singleDayPanel[dayPanelIndex];
				// all-day EventButtons of the day:
				ArrayList<EventButton> allDayList = oneDayLists.get(0);
				if (allDayList != null && allDayList.isEmpty() == false) {
					int allDayListSize = allDayList.size();
					for (int allDayIndex = 0; allDayIndex < allDayListSize; allDayIndex++) {
						sdp.add(allDayList.get(allDayIndex));
					}
				}
				// events with time for this day:
				ArrayList<EventButton> timeList = oneDayLists.get(1);
				if (timeList != null && timeList.isEmpty() == false) {
					int timeListSize = timeList.size();
					for (int timeIndex = 0; timeIndex < timeListSize; timeIndex++) {
						sdp.add(timeList.get(timeIndex));
					}
				}
			}
		}
		this.revalidate();
		this.repaint();
	}

	@Override
	public void refreshView() {
		
		monthPanel.removeAll();
		// first line: name of weekdays
		JPanel weekdaysPanel = new JPanel();
		weekdaysPanel.setMaximumSize(new Dimension(10000, 30));
		GridLayout weekdaysGridLayout = new GridLayout(1,7);// 1 line, 7 rows
		weekdaysPanel.setLayout(weekdaysGridLayout);
		Locale locale = PeaProperties.getLocale();
		weekdaysPanel.add(new JLabel(DayOfWeek.MONDAY.getDisplayName( TextStyle.FULL, locale)));
		weekdaysPanel.add(new JLabel(DayOfWeek.TUESDAY.getDisplayName( TextStyle.FULL, locale)));
		weekdaysPanel.add(new JLabel(DayOfWeek.WEDNESDAY.getDisplayName( TextStyle.FULL, locale)));
		weekdaysPanel.add(new JLabel(DayOfWeek.THURSDAY.getDisplayName( TextStyle.FULL, locale)));
		weekdaysPanel.add(new JLabel(DayOfWeek.FRIDAY.getDisplayName( TextStyle.FULL, locale)));
		weekdaysPanel.add(new JLabel(DayOfWeek.SATURDAY.getDisplayName( TextStyle.FULL, locale)));
		weekdaysPanel.add(new JLabel(DayOfWeek.SUNDAY.getDisplayName( TextStyle.FULL, locale)));
		this.add(weekdaysPanel);

		// Create the panel with days and events
		viewPanel = new JPanel();
		viewPanel.setLayout(new BoxLayout(viewPanel, BoxLayout.PAGE_AXIS));
		// create the daysPanel
		createDaysPanel(firstShownDate);
		viewPanel.add(daysPanel);
		this.add(viewPanel);
	}

	@Override
	public LocalDate getFirstShownDate() {
		return firstShownDate;
	}
	
	@Override
	/**
	 * Set the date of the first shown day in daysPanel
	 * 
	 * @param	the first shown date in the MonthPanel as LocalDate
	 */
	public void setFirstShownDate(LocalDate newDate) {
		firstShownDate = newDate;
	}

	@Override
	public JPanel getPanel() {
		return this;
	}
}
