package cologne.eck.calendar_pea.gui.calendar_view;

/*
 * PeaFactory - Production of Password Encryption Archives
 * Copyright (C) 2015  Axel von dem Bruch
 * 
 * This library is free software; you can redistribute it and/or modify it 
 * under the terms of the GNU General Public License as published 
 * by the Free Software Foundation; either version 2 of the License, 
 * or (at your option) any later version.
 * This library is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
 * or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU General Public License for more details.
 * See:  http://www.gnu.org/licenses/gpl-2.0.html
 * You should have received a copy of the GNU General Public License 
 * along with this library.
 */

import java.time.LocalDate;

import javax.swing.JPanel;


public interface CalendarView {
	
	/**
	 * Refresh the view of this container: 
	 * remove all events of all calendars, sort and add again
	 */
	public void refreshView();
	
	/**
	 * Get the first shown - maybe only - date in this container
	 * 
	 * @return	the first shown date in this container as LocalDate
	 */
	public LocalDate getFirstShownDate();
	
	/**
	 * Set the first shown - maybe only - date in this container
	 * 
	 * @param date	the first shown date in this container as LocalDate
	 */
	public void setFirstShownDate(LocalDate date);
	
	/**
	 * Get the JPanel of the view
	 * 
	 * @return	the JPanel of the view
	 */
	public JPanel getPanel();
}
