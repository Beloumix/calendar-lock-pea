package cologne.eck.calendar_pea.gui.calendar_view;

/*
 * PeaFactory - Production of Password Encryption Archives
 * Copyright (C) 2015  Axel von dem Bruch
 * 
 * This library is free software; you can redistribute it and/or modify it 
 * under the terms of the GNU General Public License as published 
 * by the Free Software Foundation; either version 2 of the License, 
 * or (at your option) any later version.
 * This library is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
 * or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU General Public License for more details.
 * See:  http://www.gnu.org/licenses/gpl-2.0.html
 * You should have received a copy of the GNU General Public License 
 * along with this library.
 */

import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridLayout;

import java.time.LocalDate;
import java.time.temporal.WeekFields;
import java.util.ArrayList;


import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollBar;
import javax.swing.JScrollPane;
import javax.swing.border.MatteBorder;

import cologne.eck.all_peas.data.PeaProperties;
import cologne.eck.all_peas.gui.ScrollbarAction;
import cologne.eck.all_peas.vm_specific.JREProperties;
import cologne.eck.calendar_pea.control.CalendarControl;
import cologne.eck.calendar_pea.control.CalendarSetting;
import cologne.eck.calendar_pea.gui.event_display.EventButton;
import cologne.eck.tools.UnexpectedValueException;


@SuppressWarnings("serial")
public class WeekPanel extends JPanel 
	implements CalendarView {

	/*
	 * The date of this DayPanel
	 */
	private LocalDate firstShownDate;	
	/*
	 * Label to display the currently shown date
	 */
	private JLabel weekNumberLabel;	

	/*
	 * panel for events except all-day events
	 */
	private JPanel weekSouthPanel;

	/*
	 * Scroll bar to set the view port depending on setting start time
	 */
	private JScrollBar verticalBar;	
	
	private ArrayList<ArrayList<ArrayList<EventButton>>> eventButtonLists;
	private int dayIndex = 0;	

	/**
	 * panel to show event of a day: 
	 * extra panel for all-day events
	 * 
	 * @param _date	the date of this panel
	 * @param dim 	the dimension of this panel
	 */
	public WeekPanel(LocalDate _date, Dimension dim) {
		
		this.firstShownDate = _date;
		this.setLayout(new BoxLayout(this, BoxLayout.PAGE_AXIS));
		if (dim == null) {
			int[] dimValues = JREProperties.getFrameDim();
			dim = new Dimension(dimValues[0], dimValues[1]);
			JREProperties.getFrameDim();//CalendarSetting.getViewDimension();//new Dimension(125, 500);
			new UnexpectedValueException("dim", "Dimension", "is null").printStackTraceLines();
		} 

		WeekFields weekFields = WeekFields.of(PeaProperties.getLocale()); 
		int weekNumber = firstShownDate.get(weekFields.weekOfWeekBasedYear());
		String week = CalendarSetting.calTranslate("week") + ": " + weekNumber;
		if (firstShownDate.getDayOfWeek().getValue() != 1) { //not monday
			week = week + " - " + (weekNumber +1);
		}
		weekNumberLabel = new JLabel(week);//ContactConstants.getFullDateFormatter().format(firstShownDate));
		this.add(weekNumberLabel);
		this.add(Box.createVerticalStrut(10));
		
		int minWidth = 30;
		int prefWidth = 30;
		
		JPanel weekNorthPanel = new JPanel(); // holds dates and all day events
		weekNorthPanel.setLayout(new GridLayout(1,7));
		// Must be set: Otherwise dates and event fields shift differently when resizing
		weekNorthPanel.setMinimumSize(new Dimension (minWidth, 30));
		weekNorthPanel.setPreferredSize(new Dimension (prefWidth, 60));
		weekNorthPanel.setMaximumSize(new Dimension (Integer.MAX_VALUE, 60));
		
		// holds the day panels without dates and all day events in a scroll pane
		weekSouthPanel = new JPanel();
		weekSouthPanel.setLayout(new GridLayout(1,7));		
		// higher height for scrolling
		weekSouthPanel.setMinimumSize(new Dimension (minWidth, (int) dim.height + (dim.height / 2 )));
		weekSouthPanel.setPreferredSize(new Dimension (prefWidth, (int) dim.height + (dim.height / 2 )));
		weekSouthPanel.setMaximumSize(new Dimension (Integer.MAX_VALUE, Integer.MAX_VALUE));
		
		// border as separator for days
		MatteBorder rightSeparator = new MatteBorder(0, 0, 0, 1, Color.LIGHT_GRAY);
		// dimension for DayPanels
		Dimension dayDim = new Dimension((dim.width / 7), dim.height);	
		for (int i = 0; i < 7; i++) {
			// panel for dates and all day events
			JPanel dateAndAllDayPanel = new JPanel();
			dateAndAllDayPanel.setLayout(new BoxLayout(dateAndAllDayPanel, BoxLayout.PAGE_AXIS));
			
			// day panel with events except all day events:
			DayPanel dayPanel = new DayPanel(firstShownDate.plusDays(i), dayDim, true);
			JPanel allDayPanel = dayPanel.getAllDayPanel();
			if (i < 6) { // add border as separator except last day
				dayPanel.setBorder(rightSeparator);
				dateAndAllDayPanel.setBorder(rightSeparator);
			}			
			JLabel dateLabel = dayPanel.getDateLabel();
			// panel to align dates left
			JPanel dateLabelPanel = new JPanel();
			dateLabelPanel.setLayout(new BoxLayout(dateLabelPanel, BoxLayout.LINE_AXIS));
			dateLabelPanel.add(Box.createHorizontalStrut(5));
			dateLabelPanel.add(dateLabel);
			dateLabelPanel.add(Box.createHorizontalGlue());
			dateAndAllDayPanel.add(dateLabelPanel);
			dateAndAllDayPanel.add(allDayPanel);
			
			weekNorthPanel.add(dateAndAllDayPanel);			
			weekSouthPanel.add(dayPanel);
		}

		// scroll pane for vertical scroll only (horizontal is handled in LockFrameContact)
		// this scroll includes only the DayPanels without date label and all-day events
		JScrollPane timeScroll = new JScrollPane(weekSouthPanel);
		// for constant gap, otherwise dates and event fields shift differently when resizing
		timeScroll.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
		// date label and all-day events are not part of the scroll pane...
		timeScroll.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
		timeScroll.setPreferredSize(new Dimension( dim.width, dim.height));
		// the vertical scroll bar is set in CalendarLayout
		verticalBar = timeScroll.getVerticalScrollBar();
		verticalBar.setValue((verticalBar.getMaximum() / 24) * CalendarSetting.getViewStartHour());
		
		// register keys for keyboard access: left, right,up, down
		ScrollbarAction.getScrollPaneKeyboardAccess(this, timeScroll, false);
		
		// Add a gap on the right side for the scroll bar width
		JPanel rightGapPanel = new JPanel();
		rightGapPanel.setLayout(new BoxLayout(rightGapPanel, BoxLayout.LINE_AXIS));
		rightGapPanel.setMinimumSize(new Dimension (minWidth, 30));
		rightGapPanel.setPreferredSize(new Dimension (prefWidth, 60));
		rightGapPanel.setMaximumSize(new Dimension (Integer.MAX_VALUE, 60));
		rightGapPanel.add(weekNorthPanel);
		rightGapPanel.add(Box.createHorizontalStrut(18));// width of scroll bar
		
		this.add(rightGapPanel);
		this.add(timeScroll);
	}	
	
	@Override
	public void refreshView() {
		
		eventButtonLists = CalendarControl.getSortedShownEventButtonList(this.firstShownDate, 7);
	
		WeekFields weekFields = WeekFields.of(PeaProperties.getLocale()); 
		int weekNumber = this.firstShownDate.get(weekFields.weekOfWeekBasedYear());
		String week = CalendarSetting.calTranslate("week") + ": " + weekNumber;
		if (this.firstShownDate.getDayOfWeek().getValue() != 1) { //not Monday
			week = week + " - " + (weekNumber +1);
		}
		weekNumberLabel.setText(week);
		int size = 7;//dayPanelHolder.getComponentCount();
		for (int i = 0; i < size; i++) {
			DayPanel viewPanel = (DayPanel) weekSouthPanel.getComponent(i);//).getViewPanel();
			viewPanel.setFirstShownDate(this.firstShownDate.plusDays(i));
			// for background reset
			if (viewPanel.getFirstShownDate().equals(LocalDate.now())) {
				viewPanel.setBackground(CalendarSetting.getTodayColor());
			} else {
				viewPanel.setBackground(CalendarSetting.getDayBackgroundColor());
			}
			viewPanel.refreshView();
		}
		this.revalidate();
		this.repaint();
	}
	
	/**
	 * Get an ArrayList of EventButtons for the next day. 
	 * This list contains two lists: at index 0 all-day events, 
	 * at index 1 events with time
	 * 
	 * @return 	a list of two lists of EventButtons: at index 0 all-day events, 
	 * 			at index 1 events with time
	 */
	public ArrayList<ArrayList<EventButton>> getNextDayList(){
		
		if (eventButtonLists == null) {
			ArrayList<ArrayList<EventButton>> emptyLists = new ArrayList<ArrayList<EventButton>>();
			emptyLists.add(new ArrayList<EventButton>());
			emptyLists.add(new ArrayList<EventButton>());
			return emptyLists;
		} else {
			if (dayIndex > 6) {
				dayIndex = 0; //reset
			}
			return eventButtonLists.get(dayIndex++);
		}
	}
	
	/**
	 * Set the vertical scroll bar of the view port
	 * of the viewPanel. This will set the first shown hour.
	 * 
	 * @param value	the first shown hour
	 */
	public void setVerticalScrollBar(int value) {
		//verticalBar.get
		verticalBar.setValue(value);
	}


	@Override
	public LocalDate getFirstShownDate() {
		return firstShownDate;
	}
	
	/**
	 * Set the date for this day view.
	 * The view needs to be refreshed. 
	 * 
	 * @param newDate	the date for this day view
	 */
	public void setFirstShownDate(LocalDate newDate) {
		//weekNumberLabel.setText(ContactConstants.getFullDateFormatter().format(firstShownDate));
		this.firstShownDate = newDate;
		WeekFields weekFields = WeekFields.of(PeaProperties.getLocale()); 
		int weekNumber = firstShownDate.get(weekFields.weekOfWeekBasedYear());
		String week = CalendarSetting.calTranslate("week") + ": " + weekNumber;
		if (firstShownDate.getDayOfWeek().getValue() != 1) { //not monday
			week = week + " - " + (weekNumber +1);
		}
		weekNumberLabel.setText(week);
	}

	@Override
	public JPanel getPanel() {
		return this;
	}
}
