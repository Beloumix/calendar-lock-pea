package cologne.eck.calendar_pea.gui.calendar_view;

/*
 * PeaFactory - Production of Password Encryption Archives
 * Copyright (C) 2015  Axel von dem Bruch
 * 
 * This library is free software; you can redistribute it and/or modify it 
 * under the terms of the GNU General Public License as published 
 * by the Free Software Foundation; either version 2 of the License, 
 * or (at your option) any later version.
 * This library is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
 * or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU General Public License for more details.
 * See:  http://www.gnu.org/licenses/gpl-2.0.html
 * You should have received a copy of the GNU General Public License 
 * along with this library.
 */

import java.awt.Color;
import java.awt.Component;
import java.awt.Point;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.time.LocalDate;

import javax.swing.BoxLayout;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;

import cologne.eck.all_peas.gui.PeaDialog;
import cologne.eck.calendar_pea.control.CalendarControl;
import cologne.eck.calendar_pea.control.CalendarSetting;
import cologne.eck.calendar_pea.gui.LockFrameCalendar;
import cologne.eck.calendar_pea.gui.event_display.ComponentInfo;
import cologne.eck.calendar_pea.gui.event_setting.EventSetter;

@SuppressWarnings("serial")
class SingleDayPanel extends JPanel implements MouseListener {
	
	private LocalDate dayDate;

	/**
	 * Panel to show one day in MonthPanel: 
	 * contains label with date and EventButtons, 
	 * when clicked, opens an EventSetter for this date
	 * 
	 * @param dayDate	the LocalDate for this SingleDayPanel
	 */
	protected SingleDayPanel(LocalDate _dayDate) {
		this.dayDate = _dayDate;

		this.setLayout(new BoxLayout(this, BoxLayout.PAGE_AXIS));
		this.setBorder(new LineBorder(Color.LIGHT_GRAY, 2));
		this.setBackground(CalendarSetting.getDayBackgroundColor());
		// format the date to display in label
		String dateText = dayDate.format(CalendarSetting.getDayDateFormatter());
		JLabel dateLabel = new JLabel(dateText);
		dateLabel.setBorder(new EmptyBorder(2,2,2,2));
		dateLabel.addMouseListener(this);
		if (dayDate.equals(LocalDate.now())) {
			this.setBackground(CalendarSetting.getTodayColor());
		}
		// add one day for next label
	//	dayDate = dayDate.plusDays(1);
		this.add(dateLabel);
		// listener to open EventSetter
		this.addMouseListener(this);
		this.setName(dateLabel.getText());	
	}

	@Override
	public void mouseClicked(MouseEvent mce) {
		// on Windows this is a double click
	}

	@Override
	public void mouseEntered(MouseEvent arg0) {
		// close event info
		ComponentInfo.disposeInfo();		
	}

	@Override
	public void mouseExited(MouseEvent arg0) {
		// close event info
		ComponentInfo.disposeInfo();
	}

	@Override
	public void mousePressed(MouseEvent mpe) {
		
		Point p = mpe.getPoint();
		Component c  =  mpe.getComponent();//.getSource();

		if (c instanceof JPanel) { // start EventSetter					
			
			if (CalendarControl.getShownCalendarList() == null
					|| CalendarControl.getShownCalendarList().isEmpty()) {
				JOptionPane.showMessageDialog(this, CalendarSetting.calTranslate("calendar_missing"));			
				return;
			} else {

				String sourceName = c.getName();		
				int height = c.getHeight();
				double hourGap =  24 /(double)height ;
				int hour = (int) ( p.getY()* hourGap);

				// start EventSetter for new event for date of dayPanel
				EventSetter setter = new EventSetter(
						this, //this.getLocationOnScreen(), 		// owner 
						null, true, 							// no event, newEvent = true
						LocalDate.parse(sourceName, CalendarSetting.getDayDateFormatter()).atTime(hour, 0));
				setter.setVisible(true);
			}
			
		} else if (c instanceof JLabel) { // ask to open day view for this date
			
			//String labelText = ((JLabel) c).getText();
			//TestLog.o(XXXX.class, "date: " + dayDate);
			int result = PeaDialog.showQuestion(c, 
					CalendarSetting.calTranslate("ask_open_day_view"),
					//"Open day view for this date?", 
					null, 0, -1);
			if (result == 0) {
				LockFrameCalendar.getView().setFirstShownDate(dayDate);// to start DayPanel with this date
				if (LockFrameCalendar.getFrame() != null) {
					LockFrameCalendar.getFrame().changeView(0);//"dayView");
				}
			}
		}
	}

	@Override
	public void mouseReleased(MouseEvent arg0) {}
}
