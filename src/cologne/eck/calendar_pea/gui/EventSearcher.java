package cologne.eck.calendar_pea.gui;

import java.awt.Color;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.awt.event.ActionListener;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.KeyStroke;
import javax.swing.text.BadLocationException;
import javax.swing.text.DefaultHighlighter;
import javax.swing.text.Highlighter;
import javax.swing.text.Highlighter.HighlightPainter;

import cologne.eck.all_peas.data.PeaProperties;
import cologne.eck.all_peas.gui.CloseOnEscapeAction;
import cologne.eck.calendar_pea.control.CalendarControl;
import cologne.eck.calendar_pea.control.CalendarSetting;
import cologne.eck.calendar_pea.data.PCalendar;
import cologne.eck.calendar_pea.data.PEvent;
import cologne.eck.calendar_pea.gui.calendar_view.WeekPanel;
import cologne.eck.tools.TestLog;

class EventSearcher implements ActionListener {
	
	private Component component;
	
	private JDialog hitListDialog = null;
	
	private DateTimeFormatter ddf = DateTimeFormatter.ofLocalizedDate(FormatStyle.SHORT);
	
	/**
	 * The number of characters shown before and after the search text
	 */
	private int contextLen = 25;
	
	EventSearcher(Component _component) {
		this.component = _component;
	}
	
	void search() {
		TestLog.v(EventSearcher.class, "Search...");
		ArrayList<PCalendar> shownList = CalendarControl.getShownCalendarList();
		if (shownList == null || shownList.size() == 0) {
			TestLog.v(EventSearcher.class, "No calendar...");
			return;
		}
		JPanel checkBoxPanel = new JPanel();
		checkBoxPanel.setLayout(new BoxLayout(checkBoxPanel, BoxLayout.PAGE_AXIS));
		checkBoxPanel.add(new JLabel(PeaProperties.getVmBridge().translate("search")));
		JCheckBox summaryCheck = new JCheckBox(CalendarSetting.calTranslate("summary"));
		summaryCheck.setSelected(true);
		JCheckBox descriptionCheck = new JCheckBox(CalendarSetting.calTranslate("description"));
		descriptionCheck.setSelected(true);
		JCheckBox locationCheck = new JCheckBox(CalendarSetting.calTranslate("location"));
		locationCheck.setSelected(true);
		checkBoxPanel.add(summaryCheck);
		checkBoxPanel.add(descriptionCheck);
		checkBoxPanel.add(locationCheck);
		checkBoxPanel.add(Box.createVerticalStrut(5));
		JPanel calCheckPanel = null;
		if (shownList.size() > 1) {
			checkBoxPanel.add(new JLabel(CalendarSetting.calTranslate("calendar")));
			calCheckPanel = new JPanel();
			calCheckPanel.setLayout(new BoxLayout(calCheckPanel, BoxLayout.PAGE_AXIS));
			for (PCalendar c : shownList) {
				JCheckBox cCheck = new JCheckBox(c.getName());
				cCheck.setSelected(true);
				calCheckPanel.add(cCheck);
			}
			if (shownList != null && shownList.size() > 0) {
				checkBoxPanel.add(calCheckPanel);
				checkBoxPanel.add(Box.createVerticalStrut(5));
			}
		}
		// open input dialog
		String searchText = (String) JOptionPane.showInputDialog(component, checkBoxPanel, 
				PeaProperties.getVmBridge().translate("search"), 
				-1, null, null, null);
		if (searchText == null || searchText.length() == 0) {
			TestLog.v(EventSearcher.class, "No search text...");
			return;
		} else {
			TestLog.v(this.getClass(), "search for " + searchText);
		}
		boolean sum = summaryCheck.isSelected();
		boolean des =  descriptionCheck.isSelected();
		boolean loc =  locationCheck.isSelected();
		ArrayList<String> selectedNames = new ArrayList<String>();
		if (calCheckPanel != null) {
			Component[] comps = calCheckPanel.getComponents();
			for (Component comp : comps) {
				if (comp instanceof JCheckBox) {
					if (((JCheckBox) comp).isSelected() == true) {
						selectedNames.add(((JCheckBox) comp).getText());
					}
				}
			}
		} else { // only one calendar, always add
			selectedNames.add(shownList.get(0).getName());
		}

		HashMap<PEvent, String> hitMap = new HashMap<PEvent, String>();
		for (PCalendar c : shownList) {
			if (selectedNames.contains(c.getName())) {
				ArrayList<PEvent> eventList = c.getEventList();
				for (PEvent e : eventList) {
					if (sum == true) {
						String summary = e.getSummary();
						if (summary != null) {
							int pos = summary.indexOf(searchText);
							if (pos > -1) {
								// found: 
								String showText = CalendarSetting.calTranslate("summary") + " \n" 
										+ getContext(pos, searchText, summary);
								hitMap.put(e, showText);
								continue;
							}
						}
					}
					if (des == true) {
						String description = e.getDescription();
						if (description != null) {
							int pos = description.indexOf(searchText);
							if (pos > -1) {
								// found: 
								String showText = CalendarSetting.calTranslate("description") + " \n" 
										+ getContext(pos, searchText, description);
								hitMap.put(e, showText);
								continue;
							}
						}
					}
					if (loc == true) {
						String location = e.getLocation();
						if (location != null) {
							int pos = location.indexOf(searchText);
							if (pos > -1) {
								// found: 
								String showText = CalendarSetting.calTranslate("location") + " \n" 
										+ getContext(pos, searchText, location);
								hitMap.put(e, showText);
								continue;
							}
						}
					}
				}
			}
		}
		if (hitMap.size() == 0) {
			TestLog.o(EventSearcher.class, "No search results");
			LockFrameCalendar.getFrame().setMessage(PeaProperties.getVmBridge().translate("no_result"), false);
			return;
		} else {
			TestLog.o(EventSearcher.class, hitMap.size() + " hits");
		}
		
		hitListDialog = new JDialog(LockFrameCalendar.getFrame(), PeaProperties.getVmBridge().translate("search"));
		JPanel contentPane = new JPanel();//  (JPanel) hitListDialog.getContentPane();
		contentPane.setLayout(new BoxLayout(contentPane, BoxLayout.PAGE_AXIS));
		JScrollPane scroll = new JScrollPane(contentPane);
		hitListDialog.add(scroll);
	// TODO	hitListDialog.getContentPane().setLayout(new BoxLayout(hitListDialog.getContentPane(), BoxLayout.PAGE_AXIS));
		//hitListDialog.add(Box.createVerticalStrut(10));
		contentPane.add(Box.createVerticalStrut(10));
		
		//PswDialogView pdv = PswDialogView.getView();
		//pdv.removeAllMnemonic(hitListDialog);

	    Iterator<Map.Entry<PEvent, String>> it = hitMap.entrySet().iterator();
	    
	    while (it.hasNext()) {
	        Map.Entry<PEvent, String> pair = (Map.Entry<PEvent, String>)it.next();
	        //System.out.println(pair.getKey() + " = " + pair.getValue());
	        PEvent e = (PEvent) pair.getKey();
	        String txt = (String) pair.getValue();
	        JTextArea area = new JTextArea();
	        area.setEditable(false);
	        area.setText(txt);
	        // highlight the key word
	        Highlighter highlighter = area.getHighlighter();
	        HighlightPainter painter = 
	               new DefaultHighlighter.DefaultHighlightPainter(Color.green);
	        int p0 = txt.indexOf(searchText);
	        int p1 = p0 + searchText.length();
	        try {
				highlighter.addHighlight(p0, p1, painter );
			} catch (BadLocationException e1) {
				TestLog.ve(EventSearcher.class, "Highlighting failed for " + searchText, 5);
			}
	        
	        //hitListDialog.add(area);
	        contentPane.add(area);
	        JPanel buttonPanel = new JPanel();
	        buttonPanel.setLayout(new BoxLayout(buttonPanel, BoxLayout.LINE_AXIS));
	        buttonPanel.add(new JLabel(CalendarSetting.calTranslate("go_to_date")));
	        buttonPanel.add(Box.createHorizontalStrut(5));
	        
	        JButton dateButton = new JButton();
	        String dateString = e.getDateStart().format(ddf);//CalendarSetting.getDayDateFormatter());
	        dateButton.setText(dateString);
	        //dateButton.setMnemonic(pdv.getMnemo(hitListDialog, dateString));
	        dateButton.addActionListener(this);
	        dateButton.setActionCommand(dateString);
	        buttonPanel.add(dateButton);
	        buttonPanel.add(Box.createHorizontalGlue());
	        contentPane.add(buttonPanel);
	        contentPane.add(Box.createVerticalStrut(15));
//	        hitListDialog.add(buttonPanel);
//	        hitListDialog.add(Box.createVerticalStrut(15));
	        
	        it.remove(); // avoids a ConcurrentModificationException
	    }
	    hitListDialog.pack();
	    
	    hitListDialog.getRootPane().getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW)
        .put(KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), "CLOSE_ON_ESCAPE");
	    hitListDialog.getRootPane().getActionMap().put("CLOSE_ON_ESCAPE", new CloseOnEscapeAction() );
	    
	   // hitListDialog.setLocationRelativeTo(LockFrameContact.getFrame());
	    hitListDialog.setLocation(component.getLocationOnScreen());
		hitListDialog.setVisible(true);

	}
	
	private String getContext(int position, String keyWord, String text) {
		int startPos = 0; 
		int endPos = 0;
		if (position > contextLen) {
			startPos = position - contextLen;
		} else {
			startPos = 0;
		}
		if (position + keyWord.length() + contextLen < text.length()) {
			endPos = position + keyWord.length() + contextLen;
		} else {
			endPos = position + keyWord.length() + ( text.length() - (position + keyWord.length() ) );
		}
		return "... " + text.substring(startPos,  endPos) + " ...";		
	}

	@Override
	public void actionPerformed(ActionEvent arg0) {
		String command = arg0.getActionCommand();
		
		LocalDate ld = LocalDate.parse(command, ddf);
		if (ld == null) {
			TestLog.ve(EventSearcher.class, "Inalid date format: " + command);
			hitListDialog.setVisible(false);
			return;
		} else {
		
			LockFrameCalendar.getView().setFirstShownDate(ld);
			boolean oldDocStatus = LockFrameCalendar.getFrame().isContentUnsaved();
					//LockFrameContact.isDocChangeUnsaved();
			CalendarControl.refreshView();
			if (LockFrameCalendar.getView() instanceof WeekPanel) {
				LockFrameCalendar.getFrame().pack();
			}
			if (oldDocStatus == true) {
				LockFrameCalendar.getFrame().markUnsavedContentChanges();
			} else {
				LockFrameCalendar.getFrame().markContentIsSaved();
			}
		}		
		hitListDialog.setVisible(false);
	}

}
