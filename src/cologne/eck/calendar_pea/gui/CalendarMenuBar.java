package cologne.eck.calendar_pea.gui;

/*
 * PeaFactory - Production of Password Encryption Archives
 * Copyright (C) 2015  Axel von dem Bruch
 * 
 * This library is free software; you can redistribute it and/or modify it 
 * under the terms of the GNU General Public License as published 
 * by the Free Software Foundation; either version 2 of the License, 
 * or (at your option) any later version.
 * This library is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
 * or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU General Public License for more details.
 * See:  http://www.gnu.org/licenses/gpl-2.0.html
 * You should have received a copy of the GNU General Public License 
 * along with this library.
 */

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.io.File;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import javax.swing.AbstractAction;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JColorChooser;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JFormattedTextField;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.JTextField;
import javax.swing.KeyStroke;
import javax.swing.SpinnerModel;
import javax.swing.SpinnerNumberModel;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.filechooser.FileFilter;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.plaf.FontUIResource;

import cologne.eck.all_peas.cloud.CloudControl;
import cologne.eck.all_peas.cloud.CloudPropertyHandler;
import cologne.eck.all_peas.cloud.StorageProvider;
import cologne.eck.all_peas.cloud.gui.CloudMenu;
import cologne.eck.all_peas.cloud.gui.UploadWorker;
import cologne.eck.all_peas.control.PathFileManager;
import cologne.eck.all_peas.control.PeaControl;
import cologne.eck.all_peas.files.FileTypePanel;
import cologne.eck.all_peas.data.PeaProperties;
import cologne.eck.all_peas.gui.CloseOnEscapeAction;
import cologne.eck.all_peas.gui.PeaDialog;
import cologne.eck.all_peas.gui.PeaFileChooser;
import cologne.eck.all_peas.gui.PswDialogView;
import cologne.eck.all_peas.gui.menu.HelpMenu;
import cologne.eck.all_peas.gui.menu.PeaMenuBar;
import cologne.eck.all_peas.gui.menu.SettingMenu;
import cologne.eck.all_peas.vm_specific.JREProperties;
import cologne.eck.calendar_pea.control.CalendarConstants;
import cologne.eck.calendar_pea.control.CalendarControl;
import cologne.eck.calendar_pea.control.CalendarPEAControl;
import cologne.eck.calendar_pea.control.CalendarSetting;
import cologne.eck.calendar_pea.data.PCalendar;
import cologne.eck.calendar_pea.gui.calendar_view.MonthPanel;
import cologne.eck.calendar_pea.gui.calendar_view.WeekPanel;
import cologne.eck.calendar_pea.gui.event_display.EventButton;
import cologne.eck.calendar_pea.gui.event_setting.AtRestDialog;
import cologne.eck.calendar_pea.gui.event_setting.DatePicker;
import cologne.eck.calendar_pea.gui.event_setting.EventSetter;
import cologne.eck.calendar_pea.util.ReadICal;
import cologne.eck.calendar_pea.util.WriteICal;
import cologne.eck.peafactory.crypto.CipherStuff;
import cologne.eck.peafactory.crypto.kdf.KeyDerivation;
import cologne.eck.tools.Converter;
import cologne.eck.tools.FileTools;
import cologne.eck.tools.ReadResources;
import cologne.eck.tools.TestLog;
import cologne.eck.tools.UnexpectedValueException;
import cologne.eck.tools.WriteResources;


@SuppressWarnings("serial")
public
class CalendarMenuBar  extends PeaMenuBar implements ActionListener {

	/*
	 * Item to perform saving the all calendars
	 */
	private JMenuItem saveItem = null;
	private JMenuItem saveAsItem = null;
	private JMenuItem defaultFileItem = null;
	
	private JSpinner yearSpinner; 
	
	private CloudMenu cloudMenu;// to fire save action programmatically
	
	// dialog with list of calendars to hide or show
	private JDialog calendarListDialog;

	
	public CalendarMenuBar() {
		
		Font font = new Font(Font.SANS_SERIF, Font.PLAIN, PeaProperties.getFontSize());
		
		PswDialogView pdv = PswDialogView.getInstance();

		JMenu fileMenu = new JMenu(CalendarSetting.calTranslate("file"));// File");//\u00FC");
		fileMenu.getAccessibleContext().
	    setAccessibleDescription(
	    	    "Clicking this component opens a menu for basic calendar operations like new, save, import, export.");
		// pdv.getMnemo(this, PeaProperties.getVmBridge().translate("file"))
		fileMenu.setMnemonic(pdv.getMnemo(this, PeaProperties.getVmBridge().translate("file")));//CalendarSetting.calTranslate("file").charAt(0));
		fileMenu.setFont(font);
		this.add(fileMenu);
		
		JMenuItem newItem = new JMenuItem(CalendarSetting.calTranslate("new"));//CalendarSetting.calTranslate("instruction"));
		newItem.getAccessibleContext().
	    setAccessibleDescription(
	    "Clicking this component opens a dialog to create a new calendar with name and color.");
		newItem.setActionCommand("new");
		newItem.setFont(font);
		newItem.addActionListener(this);
		fileMenu.add(newItem);
		
		JMenuItem openItem = new JMenuItem(PeaProperties.getVmBridge().translate("open_encrypted_file"));//CalendarSetting.calTranslate("instruction"));
		openItem.getAccessibleContext().
	    setAccessibleDescription(
	    "Clicking this component opens a dialog to open an encrypted calendar file.");
		openItem.setActionCommand("openEncrypted");
		openItem.setFont(font);
		openItem.addActionListener(this);
		fileMenu.add(openItem);
		
		saveItem = new JMenuItem(CalendarSetting.calTranslate("save"));//CalendarSetting.calTranslate("instruction"));
		saveItem.getAccessibleContext().
	    setAccessibleDescription(
	    	    "Clicking this component saves all open calendars in the default file.");
		saveItem.setActionCommand("save");
		saveItem.setFont(font);
		saveItem.addActionListener(this);
		fileMenu.add(saveItem);		
		
		saveAsItem = new JMenuItem(CalendarSetting.calTranslate("save_as"));//CalendarSetting.calTranslate("instruction"));
		saveAsItem.getAccessibleContext().
	    setAccessibleDescription(
	    	    "Clicking this component starts a dialog to save all open calendars in a specific file.");
		saveAsItem.setActionCommand("saveAs");
		saveAsItem.setFont(font);
		saveAsItem.addActionListener(this);
		fileMenu.add(saveAsItem);		
		
		JMenuItem importItem = new JMenuItem(CalendarSetting.calTranslate("import"));//CalendarSetting.calTranslate("instruction"));
		importItem.getAccessibleContext().
	    setAccessibleDescription(
	    	    "Clicking this component opens a file chooser to select a calendar file to import.");
		importItem.setActionCommand("import");
		importItem.setFont(font);
		//colorItem.setMnemonic(KeyStroke.getKeyStroke(CalendarSetting.calTranslate("instruction_mnemonic")).getKeyCode());
		importItem.addActionListener(this);
		fileMenu.add(importItem);		
		
		JMenuItem exportItem = new JMenuItem(CalendarSetting.calTranslate("export"));//CalendarSetting.calTranslate("instruction"));
		exportItem.getAccessibleContext().
	    setAccessibleDescription(
	    		"Clicking this component opens a file chooser to select a calendar file to export one calendar unencrypted.");
		exportItem.setActionCommand("export");
		exportItem.setFont(font);
		//colorItem.setMnemonic(KeyStroke.getKeyStroke(CalendarSetting.calTranslate("instruction_mnemonic")).getKeyCode());
		exportItem.addActionListener(this);
		fileMenu.add(exportItem);
		
		
		defaultFileItem = new JMenuItem(CalendarSetting.calTranslate("default_folder"));//CalendarSetting.calTranslate("instruction"));
		defaultFileItem.setToolTipText(CalendarSetting.calTranslate("choose_default_location"));
		defaultFileItem.getAccessibleContext().
	    setAccessibleDescription(
	    		"Clicking this component opens a file chooser to select the default folder to save calendars.");
		defaultFileItem.setActionCommand("default-folder");
		defaultFileItem.setFont(font);
		defaultFileItem.addActionListener(this);
		fileMenu.add(defaultFileItem);
		
		JMenuItem quitItem = new JMenuItem(CalendarSetting.calTranslate("quit"));//CalendarSetting.calTranslate("instruction"));
		quitItem.getAccessibleContext().
	    setAccessibleDescription(
	    	    "Clicking this component saves all open calendars and exits the program.");
		quitItem.setActionCommand("quit");
		quitItem.setFont(font);
		quitItem.addActionListener(this);
		fileMenu.add(quitItem);
		

		JMenu navigationMenu  = new JMenu(CalendarSetting.calTranslate("navigation"));//PeaProperties.getOsBridge().translate("file"));// File");//\u00FC");
		navigationMenu.getAccessibleContext().
	    setAccessibleDescription(
	    	    "Clicking this component opens possibilities to navigate to a specific year, month or date.");
		navigationMenu.setMnemonic(pdv.getMnemo(this, CalendarSetting.calTranslate("navigation")));//CalendarSetting.calTranslate("navigation").charAt(0));
		navigationMenu.setFont(font);
		this.add(navigationMenu);
		
		JMenu monthMenu = new JMenu(CalendarSetting.calTranslate("month"));
		monthMenu.getAccessibleContext().
	    setAccessibleDescription(
	    	    "Clicking this component opens a month spinner to navigate to a specific month.");
		monthMenu.setFont(font);
		String[] monthStrings = DatePicker.generateMonthStrings();
		for (int i = 0; i < 12; i++) {
			JMenuItem item = new JMenuItem(monthStrings[i]);
			item.addActionListener(this);
			item.setActionCommand("setMonth" + monthStrings[i]);
			monthMenu.add(item);
		}
		navigationMenu.add(monthMenu);	
		
		JMenu yearItem = new JMenu(CalendarSetting.calTranslate("year"));//CalendarSetting.calTranslate("instruction"));
		yearItem.getAccessibleContext().
	    setAccessibleDescription(
	    	    "Clicking this component opens a year spinner to navigate to a specific year. "
	    	    + "Use TAB key");
		yearItem.setFont(font);
		// to access yearSpinner (and ok button) by keyboard:
		yearItem.getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW)
        .put(KeyStroke.getKeyStroke(KeyEvent.VK_TAB, 0), "FOCUS_ON_YEAR_SPINNER");
		yearItem.getActionMap().put("FOCUS_ON_YEAR_SPINNER", new FocusOnYearSpinnerAction() );
		
		JPanel yearPanel= new JPanel();
		yearPanel.setFocusable(true);
        SpinnerModel yearModel = new SpinnerNumberModel(LocalDate.now().getYear(), //initial value
                0, //min
                5000, //max
                1);                //step
        JLabel yearLabel = new JLabel(CalendarSetting.calTranslate("year"));
        yearLabel.setFont(font);
		yearSpinner = new JSpinner( yearModel);		
		yearSpinner.setFont(new Font(Font.SANS_SERIF, Font.PLAIN, PeaProperties.getFontSize() + 2));
//		yearSpinner.requestFocus();// get focus with TAB key
		// remove the dot after thousand and add whitespace to right
		JSpinner.NumberEditor ne = new JSpinner.NumberEditor(yearSpinner,"#");
		yearSpinner.setEditor(ne);
		if (ne.getTextField() instanceof JFormattedTextField) {
			((JFormattedTextField)ne.getTextField()).setHorizontalAlignment(JTextField.CENTER);//.setAlignmentX(CENTER_ALIGNMENT);
		}
		yearSpinner.setPreferredSize(new Dimension(120, 30));
		JButton setYearButton = new JButton(PeaProperties.getVmBridge().translate("ok"));
		//setYearButton.setMnemonic(pdv.getMnemo(this, PeaProperties.getVmBridge().translate("ok")));
		setYearButton.getAccessibleContext().
	    setAccessibleDescription(
	    	    "Clicking this component causes a navigation to the specified date.");
		setYearButton.setFont(font);
		setYearButton.addActionListener(this);
		setYearButton.setActionCommand("setYear");
		yearPanel.add(yearLabel);
		yearPanel.add(yearSpinner);
		yearPanel.add(setYearButton);
		yearItem.add(yearPanel);
		navigationMenu.add(yearItem);	
				
		JMenuItem datePickerItem = new JMenuItem(CalendarSetting.calTranslate("go_to_date"));//CalendarSetting.calTranslate("instruction"));
		datePickerItem.getAccessibleContext().
	    setAccessibleDescription(
	    	    "Clicking this component opens a date picker to select a specific date to navigate to.");
		datePickerItem.setActionCommand("setBeginDate");
		datePickerItem.setFont(font);
		datePickerItem.addActionListener(this);
		navigationMenu.add(datePickerItem);	

		JMenuItem searchItem = new JMenuItem(PeaProperties.getVmBridge().translate("search"));//CalendarSetting.calTranslate("instruction"));
		searchItem.getAccessibleContext().
		setAccessibleDescription(
				"Clicking this component opens a dialog to search for key words.");
		searchItem.setActionCommand("search");
		searchItem.setFont(font);
		searchItem.addActionListener(this);
		navigationMenu.add(searchItem);	

		JMenu editMenu = new JMenu(CalendarSetting.calTranslate("edit"));//PeaProperties.getOsBridge().translate("file"));// File");//\u00FC");
		editMenu.getAccessibleContext().
		setAccessibleDescription(
				"Clicking this component opens items to edit calendars like removing, renaming, color and password.");
		editMenu.setMnemonic(pdv.getMnemo(this, PeaProperties.getVmBridge().translate("edit")));//CalendarSetting.calTranslate("edit").charAt(0));
		editMenu.setFont(font);
		this.add(editMenu);
		
		JMenuItem setEventItem = new JMenuItem(CalendarSetting.calTranslate("set_event"));//CalendarSetting.calTranslate("instruction"));
		setEventItem.getAccessibleContext().
	    setAccessibleDescription(
	    "Clicking this component opens a dialog to set an event.");
		setEventItem.setActionCommand("setEvent");
		setEventItem.setFont(font);
		//colorItem.setMnemonic(KeyStroke.getKeyStroke(CalendarSetting.calTranslate("instruction_mnemonic")).getKeyCode());
		setEventItem.addActionListener(this);
		editMenu.add(setEventItem);	
		
		JMenuItem removeCalendarItem = new JMenuItem(CalendarSetting.calTranslate("remove_calendar"));//CalendarSetting.calTranslate("instruction"));
		removeCalendarItem.getAccessibleContext().
	    setAccessibleDescription(
	    "Clicking this component opens a dialog to select and to delete a calendar.");
		removeCalendarItem.setActionCommand("removeCalendar");
		removeCalendarItem.setFont(font);
		//colorItem.setMnemonic(KeyStroke.getKeyStroke(CalendarSetting.calTranslate("instruction_mnemonic")).getKeyCode());
		removeCalendarItem.addActionListener(this);
		editMenu.add(removeCalendarItem);	
		
		JMenuItem calendarColorItem = new JMenuItem(CalendarSetting.calTranslate("change_color"));//CalendarSetting.calTranslate("instruction"));
		calendarColorItem.getAccessibleContext().
	    setAccessibleDescription(
	    	    "Clicking this component opens a dialog to select a calendar to change its color.");
		calendarColorItem.setActionCommand("calendarColor");
		calendarColorItem.setFont(font);
		//colorItem.setMnemonic(KeyStroke.getKeyStroke(CalendarSetting.calTranslate("instruction_mnemonic")).getKeyCode());
		calendarColorItem.addActionListener(this);
		editMenu.add(calendarColorItem);		
		
		JMenuItem renameItem = new JMenuItem(CalendarSetting.calTranslate("rename_calendar"));//CalendarSetting.calTranslate("instruction"));
		renameItem.getAccessibleContext().
	    setAccessibleDescription(
	    	    "Clicking this component opens a dialog to select a calendar to change its name.");
		renameItem.setActionCommand("rename");
		renameItem.setFont(font);
		//colorItem.setMnemonic(KeyStroke.getKeyStroke(CalendarSetting.calTranslate("instruction_mnemonic")).getKeyCode());
		renameItem.addActionListener(this);
		editMenu.add(renameItem);
		
		JMenuItem atRestItem = new JMenuItem(CalendarSetting.calTranslate("delete_or_store_at_rest"));
		atRestItem.getAccessibleContext().
	    setAccessibleDescription(
	    	    "Clicking this component opens a dialog to select a date for data at rest or deleting these data.");
		atRestItem.setActionCommand("dataAtRest");
		atRestItem.setFont(font);
		//colorItem.setMnemonic(KeyStroke.getKeyStroke(CalendarSetting.calTranslate("instruction_mnemonic")).getKeyCode());
		atRestItem.addActionListener(this);
		editMenu.add(atRestItem);
		

		JMenu viewMenu = new JMenu(CalendarSetting.calTranslate("view"));//PeaProperties.getOsBridge().translate("file"));// File");//\u00FC");
		viewMenu.getAccessibleContext().
	    setAccessibleDescription(
	    	    "Clicking this component opens items to modify the view of the calendar like color, font size, start time.");
		//appearanceMenu.setMnemonic(KeyStroke.getKeyStroke(PeaProperties.getOsBridge().translate("file_mnemonic")).getKeyCode());
		viewMenu.setMnemonic(pdv.getMnemo(this, PeaProperties.getVmBridge().translate("view")));//CalendarSetting.calTranslate("view").charAt(0));
		viewMenu.setFont(font);
		this.add(viewMenu);
		
		JMenuItem manageCalendarItem = new JMenuItem(CalendarSetting.calTranslate("hide_show_calendars"));//CalendarSetting.calTranslate("instruction"));
		manageCalendarItem.getAccessibleContext().
	    setAccessibleDescription(
	    "Clicking this component opens a dialog with calendar to hide or show.");
		manageCalendarItem.setActionCommand("manageCalendars");
		manageCalendarItem.setFont(font);
		//colorItem.setMnemonic(KeyStroke.getKeyStroke(CalendarSetting.calTranslate("instruction_mnemonic")).getKeyCode());
		manageCalendarItem.addActionListener(this);
		viewMenu.add(manageCalendarItem);	
		
		
		JMenuItem calendarFilesItem = new JMenuItem(CalendarSetting.calTranslate("calendars_and_files"));//CalendarSetting.calTranslate("instruction"));
		calendarFilesItem.getAccessibleContext().
	    setAccessibleDescription(
	    "Clicking this component shows the files of a calendar.");
		calendarFilesItem.setActionCommand("calendarFiles");
		calendarFilesItem.setFont(font);
		//colorItem.setMnemonic(KeyStroke.getKeyStroke(CalendarSetting.calTranslate("instruction_mnemonic")).getKeyCode());
		calendarFilesItem.addActionListener(this);
		viewMenu.add(calendarFilesItem);	
		
		
		JMenu viewSettingMenu = new JMenu(CalendarSetting.calTranslate("view_settings"));//PeaProperties.getOsBridge().translate("file"));// File");//\u00FC");
		viewSettingMenu.getAccessibleContext().
	    setAccessibleDescription(
	    	    "Clicking this component opens items to modify the view");
		//appearanceMenu.setMnemonic(KeyStroke.getKeyStroke(PeaProperties.getOsBridge().translate("file_mnemonic")).getKeyCode());
		//viewSettingMenu.setMnemonic(CalendarSetting.calTranslate("view_settings").charAt(0));
		viewSettingMenu.setFont(font);
		viewMenu.add(viewSettingMenu);
		
		JMenuItem dayColorItem = new JMenuItem(CalendarSetting.calTranslate("background_days"));//CalendarSetting.calTranslate("instruction"));
		dayColorItem.getAccessibleContext().
	    setAccessibleDescription(
	    	    "Clicking this component opens a dialog to choose a color for the background of the calendar days.");
		dayColorItem.setActionCommand("daysColor");
		dayColorItem.setFont(font);
		//colorItem.setMnemonic(KeyStroke.getKeyStroke(CalendarSetting.calTranslate("instruction_mnemonic")).getKeyCode());
		dayColorItem.addActionListener(this);
		viewSettingMenu.add(dayColorItem);		
		
		JMenuItem buttonFontSizeItem = new JMenuItem(CalendarSetting.calTranslate("font_size_event"));//CalendarSetting.calTranslate("instruction"));
		buttonFontSizeItem.getAccessibleContext().
	    setAccessibleDescription(
	    	    "Clicking this component opens a dialog to set the font size of the event buttons.");
		buttonFontSizeItem.setActionCommand("eventButtonFont");
		buttonFontSizeItem.setFont(font);
		//colorItem.setMnemonic(KeyStroke.getKeyStroke(CalendarSetting.calTranslate("instruction_mnemonic")).getKeyCode());
		buttonFontSizeItem.addActionListener(this);
		viewSettingMenu.add(buttonFontSizeItem);		
		
		JMenuItem startTimeItem = new JMenuItem(CalendarSetting.calTranslate("start_time_day"));//CalendarSetting.calTranslate("instruction"));
		startTimeItem.getAccessibleContext().
	    setAccessibleDescription(
	    	    "Clicking this component opens a dialog to select the time, the calendar view starts with.");
		startTimeItem.setActionCommand("startTimeDay");
		startTimeItem.setFont(font);
		//colorItem.setMnemonic(KeyStroke.getKeyStroke(CalendarSetting.calTranslate("instruction_mnemonic")).getKeyCode());
		startTimeItem.addActionListener(this);
		viewSettingMenu.add(startTimeItem); 
		
		JMenuItem numberOfLinesItem = new JMenuItem(CalendarSetting.calTranslate("number_weeks"));//CalendarSetting.calTranslate("instruction"));
		numberOfLinesItem.getAccessibleContext().
	    setAccessibleDescription(
	    	    "Clicking this component opens a dialog to choose the number of lines of days, a month view should show.");
		numberOfLinesItem.setActionCommand("weekNumber");
		numberOfLinesItem.setFont(font);
		//colorItem.setMnemonic(KeyStroke.getKeyStroke(CalendarSetting.calTranslate("instruction_mnemonic")).getKeyCode());
		numberOfLinesItem.addActionListener(this);
		viewSettingMenu.add(numberOfLinesItem); 
		
		// General PA setting:
		
		JMenuItem increaseFontSizeItem = new JMenuItem(PeaProperties.getVmBridge().translate("enlarge_font"));//CalendarSetting.calTranslate("cleanup"));
		increaseFontSizeItem.setActionCommand("increaseFontSize");
		increaseFontSizeItem.getAccessibleContext().
	    setAccessibleDescription(
	    	    "Clicking this component increases the font size for this program.");
		increaseFontSizeItem.setFont(font);
		increaseFontSizeItem.addActionListener(this);
		viewSettingMenu.add(increaseFontSizeItem);
		
		JMenuItem decreaseFontSizeItem = new JMenuItem(PeaProperties.getVmBridge().translate("reduce_font"));//CalendarSetting.calTranslate("cleanup"));
		decreaseFontSizeItem.setActionCommand("decreaseFontSize");
		decreaseFontSizeItem.getAccessibleContext().
	    setAccessibleDescription(
	    	    "Clicking this component decreases the font size for this program.");
		decreaseFontSizeItem.setFont(font);
		decreaseFontSizeItem.addActionListener(this);
		viewSettingMenu.add(decreaseFontSizeItem);
		
		JMenuItem colorItem = new JMenuItem(PeaProperties.getVmBridge().translate("background_color"));//CalendarSetting.calTranslate("cleanup"));
		colorItem.setActionCommand("setColor");
		colorItem.getAccessibleContext().
	    setAccessibleDescription(
	    	    "Clicking this component opens a color chooser to set the background color for this program.");
		colorItem.setFont(font);
		colorItem.addActionListener(this);
		viewSettingMenu.add(colorItem);
						
		cloudMenu = new CloudMenu(pdv.getMnemo(this, PeaProperties.getVmBridge().translate("cloud")));
		this.add(cloudMenu);
		
		SettingMenu settingMenu = new SettingMenu(false, pdv.getMnemo(this, PeaProperties.getVmBridge().translate("settings")));
		this.add(settingMenu);
		
		HelpMenu helpMenu = new HelpMenu(pdv.getMnemo(this, PeaProperties.getVmBridge().translate("help")));
		this.add(helpMenu);
	}
	@Override
	public void actionPerformed(ActionEvent ae) {

		String command = ae.getActionCommand();
		// reset message label:
		LockFrameCalendar.getFrame().setMessage(null,  false);
		if (command.equals("import")) {

			try {

				int previousEventNumber = CalendarControl.getNumberOfAllShownEvents();
				PeaFileChooser pfc = new PeaFileChooser(this);
				pfc.customizeFileChooser(CalendarSetting.calTranslate("import"), CalendarSetting.calTranslate("select_calendar_file"), true);
				FileFilter filter = new FileNameExtensionFilter("iCalendar", "ics");
				pfc.setFileFilter(filter);
				int chooserResult = pfc.showOpenDialog(this);
				if (chooserResult != PeaFileChooser.APPROVE_OPTION) {
					return;
				}
				File file = pfc.getSelectedFile();
				if (file != null && file.canRead() && file.isFile()) {
					// Read calendars from file
					// get content of file: 
					byte[] content = ReadResources.readExternFile(file.getAbsolutePath());
					TestLog.v(CalendarMenuBar.class, "Read imported file " + file.getAbsoluteFile());
					PCalendar[] cals = new ReadICal().readCalendarsFromBytes(content, null, false);
					if (cals == null) {
						LockFrameCalendar.getFrame().setMessage(CalendarSetting.calTranslate("no_valid_calendar_found"), true);
						return;
					}
					// add all calendars
					int len = cals.length;
					for (int i = 0; i < len; i++) {
						// add to models 1. associated file name for calendar, 2. file-calendar map
						CalendarPEAControl.setFileNameforCalendar(cals[i], this);
						// display calendars
						CalendarControl.showCalendar(cals[i], file.getAbsolutePath());
					}
				}
				if (ReadICal.isNoCalendarAdded() == true) {
					LockFrameCalendar.getFrame().setMessage(CalendarSetting.calTranslate("no_valid_calendar_found"), true);
				}
				String unsupportedProps = ReadICal.getUnsupportedProps();
				boolean isUnsupportedPropertiesFound = false;
				if (unsupportedProps != null) {
					isUnsupportedPropertiesFound = true;
					PeaDialog.showMessage(this, unsupportedProps,
							CalendarSetting.calTranslate("unsupported_properties"), 2);// Warning
				}
				else if (ReadICal.isInvalidFormatFound()) {
					if (isUnsupportedPropertiesFound == true) {
						LockFrameCalendar.getFrame().setMessage(CalendarSetting.calTranslate("errors_occurred") 
								+ ": " +  CalendarSetting.calTranslate("unsupported_properties")
								+ ", " + CalendarSetting.calTranslate("invalid_format"), true);
					} else {
						LockFrameCalendar.getFrame().setMessage(CalendarSetting.calTranslate("errors_occurred") 
								+ ": " + CalendarSetting.calTranslate("invalid_format"), true);
					}
				} else if (isUnsupportedPropertiesFound == true) {
					LockFrameCalendar.getFrame().setMessage(CalendarSetting.calTranslate("errors_occurred") 
							+ ": " +  CalendarSetting.calTranslate("unsupported_properties"), true);

				} else {
					int addedEvents = CalendarControl.getNumberOfAllShownEvents() - previousEventNumber;
					LockFrameCalendar.getFrame().setMessage(CalendarSetting.calTranslate("events_added") + ": " + addedEvents, false);
				}

				//LockFrameContact.setDocChangeUnsaved(true);
				if (LockFrameCalendar.getFrame() != null) {
					LockFrameCalendar.getFrame().markUnsavedContentChanges();
				}
			} catch (Exception e) {
				PeaDialog.showMessage(this, CalendarSetting.calTranslate("import") +"\n" 
						+ CalendarSetting.calTranslate("operation_failed") + "\n"
						+ e.getLocalizedMessage());
				TestLog.e(CalendarMenuBar.class, e.getLocalizedMessage() + ", " + e.toString(), 2);
				TestLog.ve(CalendarMenuBar.class, e.getLocalizedMessage() + ", " + e.toString(), 6);
			}
			//PeaControl.resetFocus();
		} else if (command.equals("new")) {	
			
			PeaControl.getDialog().initializeNewFiles();
			CalendarControl.refreshView();

			LockFrameCalendar.getFrame().setMessage(CalendarSetting.calTranslate("new"), false);// + ": " + newCalendarName, false);
			//LockFrameContact.setDocChangeUnsaved(true);
			if (LockFrameCalendar.getFrame() != null) {
				LockFrameCalendar.getFrame().markUnsavedContentChanges();
			}
			PeaControl.resetFocus();

		} else if (command.equals("openEncrypted")) {	
			
			int previousEventNumber = CalendarControl.getNumberOfAllShownEvents();

			File file = LockFrameCalendar.getFrame().openFile(new FileNameExtensionFilter("iCalendar", "ics"));

			if (file != null) {
				LockFrameCalendar.getFrame().decryptAndLoadFileToView(file.getAbsolutePath());

				int addedEvents = CalendarControl.getNumberOfAllShownEvents() - previousEventNumber;
				LockFrameCalendar.getFrame().setMessage(CalendarSetting.calTranslate("events_added") + ": " + addedEvents, false);
			}
			
		} else if (command.equals("export")) {
			// get names of all shown calendars
			String[] calendarNames = CalendarControl.getShownCalendarNameList();		
			String selectedCalendarName = null;
			// check if exists:
			if (calendarNames == null || calendarNames.length == 0) {
				PeaDialog.showMessage(this, CalendarSetting.calTranslate("calendar_missing")
						+ "\n → " + CalendarSetting.calTranslate("file")
						+ "\n    → " + CalendarSetting.calTranslate("new")
						);
				return;
			}
			// select one calendar
			if (calendarNames != null && calendarNames.length > 0) {
				selectedCalendarName = (String) JOptionPane.showInputDialog(LockFrameCalendar.getFrame(), 
						CalendarSetting.calTranslate("export_calendar"),
						CalendarSetting.calTranslate("export_calendar"),
						JOptionPane.QUESTION_MESSAGE, 
						null, 
						calendarNames, 
						calendarNames[0]);
				if (selectedCalendarName == null || selectedCalendarName.equals("") ) {
					TestLog.e(CalendarMenuBar.class, "No calendar to export selected...");
					PeaControl.resetFocus();
					return;
				}
			}
			PCalendar cal =  CalendarControl.getCalendarByName(selectedCalendarName); 
			exportCalendar(cal, LockFrameCalendar.getFrame());

		} else if (command.equals("default-folder")) {
			
			// get existing default file
			String defaultFolder = CalendarSetting.getDefaultFolder();
			String firstText = null;
			if (defaultFolder == null
					|| new File(defaultFolder).exists() == false) {
				firstText = "";
			} else {
				firstText = CalendarSetting.calTranslate("change_default_folder") + "\n"
						+ defaultFolder + "\n\n";
			}
			//
			// open file chooser to select a directory to store file
			PeaFileChooser pfc = new PeaFileChooser(this);
			pfc.customizeFileChooser(CalendarSetting.calTranslate("save"), firstText + CalendarSetting.calTranslate("choose_default_location"), false);
			pfc.setApproveButtonText(CalendarSetting.calTranslate("select"));
			pfc.setMultiSelectionEnabled(false);
			pfc.setFileSelectionMode(PeaFileChooser.DIRECTORIES_ONLY);
			pfc.showOpenDialog(this);
			// get selected directory
			File directory = pfc.getSelectedFile();

			if (directory != null) {
				CalendarSetting.setDefaultFolder(directory.getAbsolutePath());
			} else {
				TestLog.e(CalendarMenuBar.class, "No directory chosen to store calendars...");
				return;
			}

			LockFrameCalendar.getFrame().setMessage(CalendarSetting.calTranslate("default_folder") + ": " + directory.getAbsolutePath(), false);
			//LockFrameContact.setDocChangeUnsaved(true);
			if (LockFrameCalendar.getFrame() != null) {
				LockFrameCalendar.getFrame().markUnsavedContentChanges();
			}

		} else if (command.equals("quit")) {
			try {
				((LockFrameCalendar)PeaControl.getDialog().getLockFrame()).close();//checkUnsavedTextChanges();
			} catch (Exception e) {
				int result = PeaDialog.showQuestion(this, e.getLocalizedMessage() + " - " + e.toString()
						+ "\n   " + PeaProperties.getVmBridge().translate("quit") + "?", 
						PeaProperties.getVmBridge().translate("error"), 0);
				if (result == 0) {
					System.exit(0);
				}
			} finally {
				System.exit(0);
			}

		} else if (command.equals("saveAs")) {			
			
			// choose calendar to save:
			String calendarName = selectCalendar(CalendarSetting.calTranslate("select_calendar"));
			if (calendarName == null) {
				return;
			}
			
			// choose a file to store the encrypted calendars:
			PeaFileChooser pfc = new PeaFileChooser(this);
			pfc.customizeFileChooser(CalendarSetting.calTranslate("save_as"), CalendarSetting.calTranslate("select_calendar_file"), false);
			FileFilter filter = new FileNameExtensionFilter("iCalendar", "ics");
			pfc.setApproveButtonText(CalendarSetting.calTranslate("select"));
			//pfc.setSelectedFile(new File(CalendarSetting.getDefaultFileName()));
			String dir = CalendarSetting.getDefaultFolder();
			if (dir != null && new File(dir).exists()) {
				pfc.setCurrentDirectory(new File(dir));
			}
			pfc.setFileFilter(filter);
			pfc.showOpenDialog(this);
			File file = pfc.getSelectedFile();
			// check if file was selected:
			if (file == null) {
				TestLog.e(CalendarMenuBar.class, "No file to save calendars selected...");
				return;
			} else {
				
				// rename file if extension is not .ics
				if (! file.getName().endsWith(".ics")) {
					file = new File(file.getAbsolutePath() + ".ics");
				}	
				String fileName = file.getAbsolutePath();
				// if file already exists: ask to overwrite
				if (((FileTypePanel)JREProperties.getTypePanel()).askToOverwriteFile(file, false) == false) {
					return;
				}				
				// find calendar by selected name:
				PCalendar c = CalendarControl.getCalendarByName(calendarName);
				// get the calendar as String:
				String calAsString = WriteICal.iCalToString(c);
				// convert to bytes:
				byte[] plainBytes = calAsString.getBytes(PeaProperties.getCharset());
				// encrypt:
				byte[] cipherBytes = CipherStuff.getInstance().encrypt(plainBytes, 
						CipherStuff.getSessionKeyCrypt().getKey(), // keyMaterial is overwritten
						true, null); // key is stored encrypted							
				// write local:
				boolean success = WriteResources.write(cipherBytes,fileName, null);
				if (success == false) {
					TestLog.e(CalendarMenuBar.class, "Save calendar failed: " + calendarName + " in " + fileName);
					PeaDialog.showMessage(this, 
							PeaProperties.getVmBridge().translate("operation_failed") + ":\n"
							+ PeaProperties.getVmBridge().translate("save") + ": " + fileName, 
							PeaProperties.getVmBridge().translate("error"), 0);		
					LockFrameCalendar.getFrame().setMessage(CalendarSetting.calTranslate("operation_failed"), true);					
					return;
				} else {
					TestLog.o(CalendarMenuBar.class, "Calendar " + calendarName + " saved in " + fileName);									
					// add to associated file names
					c.addAssociatedFileName(fileName);	
					CalendarControl.addToFileCalendarsMap(fileName, c);
					// to remember in path file:
					PeaControl.addAvailableFileName(fileName);
					// show message:
					LockFrameCalendar.getFrame().setMessage(CalendarSetting.calTranslate("save_as") + ": " 
							+ fileName, false);
				}	
			}

		} else if (command.equals("save")) {
			
			String error = saveAllCalendars();
			if (error == null) {
				// show message:
				LockFrameCalendar.getFrame().setMessage(PeaProperties.getVmBridge().translate("save") + ": " 
					+ PeaProperties.getVmBridge().translate("success"), false);
				if (LockFrameCalendar.getFrame() != null) {
					LockFrameCalendar.getFrame().markContentIsSaved();
				}
			} else {
				PeaDialog.showMessage(this, 
						PeaProperties.getVmBridge().translate("operation_failed") + ":\n"
						+ PeaProperties.getVmBridge().translate("save"), 
						PeaProperties.getVmBridge().translate("error"), 0);		
				LockFrameCalendar.getFrame().setMessage(CalendarSetting.calTranslate("operation_failed"), true);	
			}


// TODO///////////////////////////////////////////////////////////////////////////////////
/*		
			try {
				cologne.eck.js_cal.JS_Setter.setJsFile(21);
			} catch (Exception e) {
				// 
				StackTraceElement[] ste = Thread.currentThread().getStackTrace();
				String lines = "";
				for (int i=0; i < 5;i++) {
					lines += ste[i].toString() + "\n";
				}
				PeaDialog.showInputDialog(this, "Couldn't set...\n"
						+ e.getLocalizedMessage() +"\n"
						+ e.toString() +"\n"
						+ lines, null, 0);
				e.printStackTrace();
			}

*/
// ======================= Navigation Menu ===========================
			
		} else if (command.startsWith("setMonth")) {
			String month = command.substring(8);
			String[] months = DatePicker.generateMonthStrings();
			int newMonth = 0;
			// check which name was selected
			for (int i = 0;i < 12; i++) {
				if (month.equals(months[i])) {
					newMonth = i;
					break;
				}
			}	
			boolean oldDocStatus = LockFrameCalendar.getFrame().isContentUnsaved();//LockFrameContact.isDocChangeUnsaved();
			LockFrameCalendar.getView().setFirstShownDate(LockFrameCalendar.getView().getFirstShownDate().withMonth(newMonth + 1));// +1 because newMonth is index
			CalendarControl.refreshView();
			if (oldDocStatus == true) {
				LockFrameCalendar.getFrame().markUnsavedContentChanges();
			} else {
				LockFrameCalendar.getFrame().markContentIsSaved();
			}
			if (LockFrameCalendar.getView() instanceof WeekPanel) {
				LockFrameCalendar.getFrame().pack();
			}
			
		} else if (command.equals("focusToYearSpinner")) {
			yearSpinner.requestFocus();
			TestLog.v(getClass(), "yearItem");
			
		} else if (command.equals("setYear")) {

			LockFrameCalendar.getView().setFirstShownDate(LockFrameCalendar.getView().getFirstShownDate().withYear((int) yearSpinner.getValue()));
			boolean oldDocStatus = LockFrameCalendar.getFrame().isContentUnsaved();//isDocChangeUnsaved();
			CalendarControl.refreshView();
			if (LockFrameCalendar.getView() instanceof WeekPanel) {
				LockFrameCalendar.getFrame().pack();
			}
			if (oldDocStatus == true) {
				LockFrameCalendar.getFrame().markUnsavedContentChanges();
			} else {
				LockFrameCalendar.getFrame().markContentIsSaved();
			}
			// menu must be closed manually:
			javax.swing.MenuSelectionManager.defaultManager().clearSelectedPath();

		} else if (command.equals("setBeginDate")) {

			LocalDate x = new DatePicker(LocalDate.now(), true)
					.showDatePicker(this, CalendarSetting.calTranslate("go_to_date"));
			LockFrameCalendar.getView().setFirstShownDate(x);
			boolean oldDocStatus = LockFrameCalendar.getFrame().isContentUnsaved();//LockFrameContact.isDocChangeUnsaved();
			CalendarControl.refreshView();
			if (LockFrameCalendar.getView() instanceof WeekPanel) {
				LockFrameCalendar.getFrame().pack();
			}
			if (oldDocStatus == true) {
				LockFrameCalendar.getFrame().markUnsavedContentChanges();
			} else {
				LockFrameCalendar.getFrame().markContentIsSaved();
			}
			PeaControl.resetFocus();
			
		} else if (command.equals("search")) {

			TestLog.v(CalendarMenuBar.class, "Search...");
			new EventSearcher(this).search();


//========================= EDIT MENU ================================

        	
		} else if (command.equals("setEvent")) {
			
			EventSetter es = new EventSetter(JREProperties.getMainWindow(), 
					null, true, LocalDateTime.now());
			es.setVisible(true);
		} else if (command.equals("rename")) {

			String selectedCalendarName = selectCalendar(CalendarSetting.calTranslate("rename_calendar"));
			if (selectedCalendarName == null || selectedCalendarName.equals("")) {
				return;
			}
			String newCalendarName = PeaDialog.showInputDialog(JREProperties.getMainWindow(), CalendarSetting.calTranslate("type_name"), 
						CalendarSetting.calTranslate("new"), -1);

				if (newCalendarName == null || newCalendarName.equals("") ) {
					return;
				} else {
					if (CalendarControl.getCalendarByName(newCalendarName) == null) {
						CalendarControl.getCalendarByName(selectedCalendarName).setName(newCalendarName);
					} else {
						PeaDialog.showMessage(JREProperties.getMainWindow(), CalendarSetting.calTranslate("calendar_exists"), 
								CalendarSetting.calTranslate("operation_failed"), 0);
						return;
					}					
				}
				LockFrameCalendar.getFrame().setMessage(CalendarSetting.calTranslate("rename_calendar") + ": "
			+ selectedCalendarName + " -> " + newCalendarName, false);
			//LockFrameContact.setDocChangeUnsaved(true);
			LockFrameCalendar.getFrame().markUnsavedContentChanges();
			
		} else if (command.equals("removeCalendar")) {	
			
			String selectedCalendarName = selectCalendar(CalendarSetting.calTranslate("remove_calendar"));

			//TestLog.o(ContactMenuBar.class, "Remove " + selectedCalendarName);
			if (selectedCalendarName == null || selectedCalendarName.equals("") ) {
				return;
			}
			PCalendar c = CalendarControl.getCalendarByName(selectedCalendarName);
			String[] cFiles = c.getAssociatedFileNames();
			
			for (int i = 0; i < cFiles.length;i++) {
				// remove file, if there are no other calendars in the file
				if (CalendarControl.getCalendarsOfFileAsSet(cFiles[i]).size() == 1) {
					CalendarControl.removeFileFromFileCalendarsMap(cFiles[i]);
					PeaControl.removeAvailableFileName(cFiles[i]);
					if (cFiles[i].contains(CloudControl.getTmpDir())) { // cloud file
						String prov = CloudControl.getProviderNameFromTmpFile(cFiles[i]);
						String fName = CloudControl.getFileNameFromTmpFile(cFiles[i]);
						if (prov != null) {// 
							CloudPropertyHandler.removeFileOfProvider(prov, fName,
									Converter.bytes2hex(KeyDerivation.getSalt()), 
									Converter.bytes2hex(PeaProperties.getAlgoParamString()), null);
						}
					} else { // local file
						PathFileManager.removeFileName(cFiles[i]);
					}
				}
			}

			CalendarControl.removeCalendar(CalendarControl.getCalendarByName(selectedCalendarName));
			LockFrameCalendar.getFrame().setMessage(CalendarSetting.calTranslate("remove_calendar") + ": "
					+ selectedCalendarName , false);

			CalendarControl.refreshView();
			
			// Ask to delete files (wipe)
			StringBuilder fileListAsString = new StringBuilder();
			for (String f : cFiles) {
				fileListAsString.append(f + "\n");
			}
			int result = PeaDialog.showQuestion(JREProperties.getMainWindow(), CalendarSetting.calTranslate("irretrievably_delete_files") 
					+ "\n" + new String(fileListAsString), 
					PeaProperties.getVmBridge().translate("select_file_to_wipe"), 2 , -1); // Ok/Cancel and plain message
			if (result == 0) {
				TestLog.o(CalendarMenuBar.class, "Securely wipe files: " + new String(fileListAsString));
				for (String f : cFiles) {
					if (f.contains(CloudControl.getTmpDir())) {
						// delete file from cloud
						String providerName = CloudControl.getProviderNameFromTmpFile(f);
						String fileNameToDelete = CloudControl.getFileNameFromTmpFile(f);
						StorageProvider prov = CloudControl.getOpenProvider(providerName);
						String error = prov.deleteFile(fileNameToDelete);
						if (error != null) {
							PeaDialog.showMessage(this, error, PeaProperties.getVmBridge().translate("error"),  0);
						} else {
							TestLog.o(CalendarMenuBar.class, "Deleted in cloud: " + fileNameToDelete);
						}
					}
					FileTools.wipeFile(f, true);
				}
			}
			
		} else if (command.equals("dataAtRest")) {	

			TestLog.v(CalendarMenuBar.class, "Delete or store data at rest...");
			if (LockFrameCalendar.getFrame().isContentUnsaved() == true) {
				PeaDialog.showMessage(this, PeaProperties.getVmBridge().translate("unsaved_modifications"));
				return;
			}
			AtRestDialog dlg = new AtRestDialog();
			dlg.setVisible(true);		
	
			boolean delete = AtRestDialog.isDelete();
			LocalDate date = AtRestDialog.getDateUntil();
			String atRestFileNameWithPath = AtRestDialog.getFileName();
			TestLog.v(getClass(), delete + " - " + date + " - " + atRestFileNameWithPath);	
			
			if (date == null) { // cancel
				return;
			}			
			
			// ask to overwrite file
			if (atRestFileNameWithPath != null && new File(atRestFileNameWithPath).exists()) {
				int result = PeaDialog.showQuestion(this, PeaProperties.getVmBridge().translate("overwrite_existing_file"), 
						PeaProperties.getVmBridge().translate("warning"), 0);
				if (result != 0) { // not YES
					PeaControl.resetFocus();
					return;
				}
			}
			
			// Remove events from calendars (create new calendars with old events: name + "-until-" + date
			ArrayList<PCalendar> allCals = CalendarControl.getShownCalendarList();
			ArrayList<PCalendar> restList = new ArrayList<PCalendar>();
			int numberOFCalendarComponents = 0;
			for (PCalendar pc : allCals) {
				PCalendar restCal = pc.separateEventsBefore(date, atRestFileNameWithPath);
				if (restCal != null && restCal.getEventList() != null) {
					numberOFCalendarComponents += restCal.getEventList().size();
				}
				if (restCal != null && restCal.getTodoList() != null) {
					numberOFCalendarComponents += restCal.getTodoList().size();
				}
				if (restCal != null) {
					restList.add(restCal);
				}
			}
			if (numberOFCalendarComponents == 0) {
				PeaDialog.showMessage(this, //CalendarSetting.calTranslate("remove_event") + ": 0\n" +
				PeaProperties.getVmBridge().translate("no_change"));
				return;
			}
			if (delete == true) {
				if (restList.isEmpty()) {
					TestLog.v(getClass(), "No calendars deleted");
				} else {
					// confirm:
					int result = PeaDialog.showQuestion(this, numberOFCalendarComponents + " " +
							PeaProperties.getVmBridge().translate("files") + "\n" +
							PeaProperties.getVmBridge().translate("delete") + "?", 
							PeaProperties.getVmBridge().translate("warning"), 0);
					if (result != 0) { // not YES
						PeaControl.resetFocus();
						return;
					}
				}
				// remove events from calendar view: update view				
				CalendarControl.refreshView();
				saveAllCalendars();
				PeaControl.resetFocus();
				return;
			}
			if (restList.isEmpty()) {
				TestLog.v(getClass(), "No calendars at rest needed");
				PeaControl.resetFocus();
				return;
			}
			
			// Create string with old calendars
			PCalendar[] allCalArray = restList.toArray(new PCalendar[0]);
			String calAsString = WriteICal.iCalsToString(allCalArray);
			// encrypt and write to file
			// convert to bytes:
			byte[] plainBytes = calAsString.getBytes(PeaProperties.getCharset());
			// encrypt:
			byte[] cipherBytes = CipherStuff.getInstance().encrypt(plainBytes, 
					CipherStuff.getSessionKeyCrypt().getKey(), // keyMaterial is overwritten
					true, null); // key is stored encrypted							
			// write local:
			boolean success = WriteResources.write(cipherBytes,atRestFileNameWithPath, null);
			if (success == false) {
				TestLog.e(CalendarMenuBar.class, "Save calendars failed: " + atRestFileNameWithPath);
				PeaDialog.showMessage(this, 
						PeaProperties.getVmBridge().translate("operation_failed") + ":\n"
						+ PeaProperties.getVmBridge().translate("save") + ": " + atRestFileNameWithPath, 
						PeaProperties.getVmBridge().translate("error"), 0);		
				LockFrameCalendar.getFrame().setMessage(CalendarSetting.calTranslate("operation_failed"), true);
				PeaControl.resetFocus();
				return;
			} else {
				TestLog.v(CalendarMenuBar.class, "Stored at rest: " + allCalArray.length + " calendars with "
						+ numberOFCalendarComponents + " elements.");
			}
			
			// remove events from calendar view: update view
			CalendarControl.refreshView();
			// save after refresh:
			String error = saveAllCalendars();
			if (error == null) {
				// show message:
				LockFrameCalendar.getFrame().setMessage(PeaProperties.getVmBridge().translate("save") + ": " 
					+ PeaProperties.getVmBridge().translate("success"), false);
				if (LockFrameCalendar.getFrame() != null) {
					LockFrameCalendar.getFrame().markContentIsSaved();
				}
			} else {
				PeaDialog.showMessage(this, 
						PeaProperties.getVmBridge().translate("operation_failed") + ":\n"
						+ PeaProperties.getVmBridge().translate("save"), 
						PeaProperties.getVmBridge().translate("error"), 0);		
				LockFrameCalendar.getFrame().setMessage(CalendarSetting.calTranslate("operation_failed"), true);	
			}
			PeaControl.resetFocus();
			
		} else if (command.equals("manageCalendars")) {//("removeCalendar")) {
			
			calendarListDialog  = new JDialog(JREProperties.getMainWindow());
			// Avoid keyboard trap: Close with ESC
			calendarListDialog.getRootPane().getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW)
	    		.put(KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), "CLOSE_ON_ESCAPE");
			calendarListDialog.getRootPane().getActionMap().put("CLOSE_ON_ESCAPE", new CloseOnEscapeAction() );
			calendarListDialog.setTitle(CalendarSetting.calTranslate("hide_show_calendars"));
			JPanel mainPane = (JPanel) calendarListDialog.getContentPane();
			mainPane.setLayout(new BoxLayout(mainPane, BoxLayout.PAGE_AXIS));
			ArrayList<PCalendar> shownList = CalendarControl.getShownCalendarList();
			ArrayList<PCalendar> hiddenList = CalendarControl.getHiddenCalendarList();
			for (PCalendar pc : shownList) {
				if (pc != null ) {//&& pc.getName() != null && ! pc.getName().equals("")) {
					JCheckBox cb = new JCheckBox(pc.getName());
					cb.setName(pc.getCalUid());
					//TestLog.o(ContactMenuBar.class, "Name: " +pc.getName());
					cb.setOpaque(true);
					cb.setBackground(pc.getColor());
					Color fontColor = EventButton.getAppropriateFontColor(pc.getColor());
					cb.setForeground(fontColor); 
					cb.setSelected(true);
					mainPane.add(cb);
				}
			}
			for (PCalendar pc : hiddenList) {
				if (pc != null && pc.getName() != null && ! pc.getName().equals("")) {
					JCheckBox cb = new JCheckBox(pc.getName());
					cb.setName(pc.getCalUid());
					cb.setOpaque(true);
					cb.setBackground(pc.getColor());
					Color fontColor = EventButton.getAppropriateFontColor(pc.getColor());
					cb.setForeground(fontColor);
					cb.setSelected(false);
					mainPane.add(cb);
				}
			}
			mainPane.add(Box.createVerticalStrut(10));
			JButton showHideButton = new JButton(PeaProperties.getVmBridge().translate("ok"));
			showHideButton.addActionListener(this);
			showHideButton.setActionCommand("showHide");
			mainPane.add(showHideButton);
			calendarListDialog.pack();
			calendarListDialog.setMinimumSize(new Dimension(200, 100));
			calendarListDialog.setLocationRelativeTo((Component)this);
			calendarListDialog.setVisible(true);

			
		} else if (command.equals("showHide")) {
			
			ArrayList<PCalendar> shownList = CalendarControl.getShownCalendarList();
			//			ArrayList<PCard> hiddenList = CalendarControl.getHiddenCalendarList();
			//String s = "";	for(PCard scal : shownList) {s += " " + scal.getName() + " ";}TestLog.o(ContactMenuBar.class, "shown: " + s);
			//String h = "";	for(PCard hcal : hiddenList) {h += " " + hcal.getName() + " ";}TestLog.o(ContactMenuBar.class, "hidden: " + h);
			
			if (calendarListDialog == null) {
				new UnexpectedValueException("JDialog", "calendarListDialog","is null").printDescription();
				return;
			}
			if ( ! (calendarListDialog.getContentPane() instanceof JPanel) ) {
				new UnexpectedValueException("Component", "calendarListDialog.getComponent(0)","is not JPanel").printDescription();
				TestLog.e(CalendarMenuBar.class, calendarListDialog.getComponent(1).getClass().getName());
				return;
			}
			JPanel mainPane = (JPanel) calendarListDialog.getContentPane();
			Component[] comps = mainPane.getComponents();
			for (Component c : comps) {
				if (c instanceof JCheckBox) {
					JCheckBox cb = (JCheckBox) c;
					// get the calendar by name
					//PCard cal = CalendarControl.getCalendarByName(cb.getText());
					// get calendar by UID from name of check box
					PCalendar cal = CalendarControl.getCalendarByUID(cb.getName());
					if (cal == null) {
						new UnexpectedValueException("PCard","cal","is null").printDescription();
						calendarListDialog.dispose();
						return;
					}
					if (cb.isSelected() == false) {	
						if (shownList.contains(cal)) {
							CalendarControl.hideShownCalendar(cal);
						}
					} else {
						if ( ! shownList.contains(cal)) {
							CalendarControl.showHiddenCalendar(cal);
						}
					}
				}
			}
			CalendarControl.refreshView();
			calendarListDialog.dispose();			
			
		} else if (command.equals("calendarFiles")) {
			
			String selectedCalendarName = selectCalendar(CalendarSetting.calTranslate("calendars_and_files"));
			if (selectedCalendarName == null || selectedCalendarName.equals("")) {
				return;
			}

			// get associated file names:
			PCalendar cal = CalendarControl.getCalendarByName(selectedCalendarName);
			String[] associatedFileNames = cal.getAssociatedFileNames();
			StringBuilder fileNamesAsString = new StringBuilder();
			for (String s : associatedFileNames) {
				if (s.contains(CloudControl.getTmpDir())) {
					String providerName = CloudControl.getProviderNameFromTmpFile(s);
					String fileName = CloudControl.getFileNameFromTmpFile(s);
					fileNamesAsString.append(PeaProperties.getVmBridge().translate("cloud")
							+ " " + providerName + ": " + fileName + "\n");
				} else {
					fileNamesAsString.append(s + "\n");
				}
			}
			PeaDialog.showMessage(JREProperties.getMainWindow(), "   " + cal.getName() + ":\n\n" + fileNamesAsString);

		} else if (command.equals("calendarColor")) {

			String selectedCalendarName = selectCalendar(CalendarSetting.calTranslate("change_color"));
			if (selectedCalendarName == null || selectedCalendarName.equals("") ) {
				return;
			}
			//TestLog.o(ContactMenuBar.class, "Change color " + selectedCalendarName);			
			Color color = JColorChooser.showDialog(JREProperties.getMainWindow(),
					CalendarSetting.calTranslate("choose_calendar_color"), Color.BLACK);

			if (color != null) {
				//TestLog.o(ContactMenuBar.class, "color: " + color.toString());
				CalendarControl.getCalendarByName(selectedCalendarName).setColor(color);
				CalendarControl.refreshView();
				LockFrameCalendar.getFrame().markUnsavedContentChanges();
			}
			
		} else if (command.equals("increaseFontSize")) {
			
			// change font size
			int oldFontSize = PeaProperties.getFontSize();
			int newFontSize = oldFontSize;
			if (oldFontSize < 18) {
				newFontSize = oldFontSize + 2;
			PeaProperties.setFontSize( newFontSize);
			} 
		    
			// set font size (UIManager)
			FontUIResource f = new FontUIResource(Font.SANS_SERIF, Font.PLAIN, newFontSize);			
			java.util.Enumeration<Object> keys = UIManager.getDefaults().keys();
		    while (keys.hasMoreElements())  {
		        Object key = keys.nextElement();
		        Object value = UIManager.get(key);
		        if (value instanceof javax.swing.plaf.FontUIResource) {
		            UIManager.put(key, f);
		        }
		    }

		    UIManager.getLookAndFeelDefaults()
	        .put("defaultFont", new Font(Font.SANS_SERIF, Font.PLAIN, newFontSize));

			if (SwingUtilities.getWindowAncestor(this) instanceof JFrame){ // LockFrame: plain text mode
				PswDialogView.setUI(false);
			} else if (SwingUtilities.getWindowAncestor(this) instanceof JDialog){ // Password dialog: encrypted mode
				PswDialogView.setUI(true);
			} else {
				new UnexpectedValueException("window ancgestor", "Window",
						"is not JDialog or JFrame: " + SwingUtilities.getWindowAncestor(this).toString()).printDescription();
			}
			Window win = SwingUtilities.windowForComponent(this);
			SwingUtilities.updateComponentTreeUI(win);
			win.pack();
			win.setVisible(true);
		    		    
		} else if (command.equals("decreaseFontSize")) {
			
			// change font size
			int oldFontSize = PeaProperties.getFontSize();
			int newFontSize = oldFontSize;
			if (oldFontSize > 10) {
				newFontSize = oldFontSize - 2;
			PeaProperties.setFontSize( newFontSize);
			} 
			
			// set font size (UIManager)
			FontUIResource f = new FontUIResource(Font.SANS_SERIF, Font.PLAIN, newFontSize);			
			java.util.Enumeration<Object> keys = UIManager.getDefaults().keys();
		    while (keys.hasMoreElements())  {
		        Object key = keys.nextElement();
		        Object value = UIManager.get(key);
		        if (value instanceof javax.swing.plaf.FontUIResource) {
		            UIManager.put(key, f);
		        }
		    }  

		    // resizes currently shown items, labels...
		    UIManager.getLookAndFeelDefaults()
	        .put("defaultFont", new Font(Font.SANS_SERIF, Font.PLAIN, newFontSize));
		    
		    // update (current) view
		    SwingUtilities.updateComponentTreeUI(PswDialogView.getView());		      
			if (SwingUtilities.getWindowAncestor(this) instanceof JFrame){ // LockFrame: plain text mode
				PswDialogView.setUI(false);
			} else if (SwingUtilities.getWindowAncestor(this) instanceof JDialog){ // Password dialog: encrypted mode
				PswDialogView.setUI(true);
			} else {
				new UnexpectedValueException("window ancgestor", "Window",
						"is not JDialog or JFrame: " + SwingUtilities.getWindowAncestor(this).toString()).printDescription();
			}
			Window win = SwingUtilities.windowForComponent(this);
			SwingUtilities.updateComponentTreeUI(win);
			win.pack();
			win.setVisible(true);

		} else if (command.equals("setColor")) {
			int[] colorVals = PeaProperties.getPeaColor();
			Color peaCol = new Color(colorVals[0], colorVals[1], colorVals[2]);
			Color color = JColorChooser.showDialog(this,PeaProperties.getVmBridge().translate("background_color"), peaCol);
			if (color == null) {
				return;
			}
			// check darkness:
			int darkness = ((30 * color.getRed() +
		            59 * color.getGreen() +
		            11 * color.getBlue()) / 100);
			while (darkness < 210) { // make brighter
				color = color.brighter();
				int newDarkness = ((30 * color.getRed() +
		            59 * color.getGreen() +
		            11 * color.getBlue()) / 100);
				if (newDarkness == darkness) { // this occurs if colors are 0
					int r = color.getRed();
					int g = color.getGreen();
					int b = color.getBlue();
					if ( r < 50) {
						r = r + 50;
					}
					if (g < 50) {
						g = g + 50;
					}
					if (b < 50) {
						b = b + 50;
					}
					color = new Color(r,g,b);
					darkness = ((30 * color.getRed() +
				            59 * color.getGreen() +
				            11 * color.getBlue()) / 100);
				} else {
					darkness = newDarkness;
				}
			}

			int[] newCol = {color.getRed(), color.getGreen(), color.getBlue()};
			PeaProperties.setPeaColor(newCol);
			if (SwingUtilities.getWindowAncestor(this) instanceof JFrame){ // LockFrame: plain text mode
				PswDialogView.setUI(false);
			} else if (SwingUtilities.getWindowAncestor(this) instanceof JDialog){ // Password dialog: encrypted mode
				PswDialogView.setUI(true);
			} else {
				new UnexpectedValueException("window ancgestor", "Window",
						"is not JDialog or JFrame: " + SwingUtilities.getWindowAncestor(this).toString()).printDescription();
			}
			
			Window win = SwingUtilities.windowForComponent(this);
			SwingUtilities.updateComponentTreeUI(win);
			win.pack();
			win.setVisible(true);
			PswDialogView.getView().validate();
			PswDialogView.getView().repaint();

			
			//========================= APPEARANCE MENU ================================

		} else if (command.equals("daysColor")) {

			Color color = JColorChooser.showDialog(this,CalendarSetting.calTranslate("background_days"), Color.BLACK);
			if (color == null) {
				TestLog.o(CalendarMenuBar.class, "color is null ");
				return;
			}

			CalendarSetting.setDayBackgroundColor(color);

			boolean oldDocStatus = LockFrameCalendar.getFrame().isContentUnsaved();//LockFrameContact.isDocChangeUnsaved();
			CalendarControl.refreshView();
			if (LockFrameCalendar.getView() instanceof WeekPanel) {
				LockFrameCalendar.getFrame().pack();
			}
			if (oldDocStatus == true) {
				LockFrameCalendar.getFrame().markUnsavedContentChanges();
			} else {
				LockFrameCalendar.getFrame().markContentIsSaved();
			}
			
		} else if (command.equals("startTimeDay")) {
			// TODO hour spinner to set start time in week/day view
			Object[] startHour = {"0", "3", "5", "7", "9", "12", "15"};
		 	String hourString = (String) JOptionPane.showInputDialog(LockFrameCalendar.getFrame(), 
		 			CalendarSetting.calTranslate("start_time_day"), CalendarSetting.calTranslate("start_time_day"), 
		 			JOptionPane.PLAIN_MESSAGE, null, 
		 			startHour, startHour[3]);
		 	if (hourString != null) {
		 		int hour = Integer.parseInt(hourString);
		 		CalendarSetting.setViewStartHour(hour);
				boolean oldDocStatus = LockFrameCalendar.getFrame().isContentUnsaved();//LockFrameContact.isDocChangeUnsaved();
				CalendarControl.refreshView();
				if (LockFrameCalendar.getView() instanceof WeekPanel) {
					LockFrameCalendar.getFrame().pack();
				}
				if (oldDocStatus == true) {
					LockFrameCalendar.getFrame().markUnsavedContentChanges();
				} else {
					LockFrameCalendar.getFrame().markContentIsSaved();
				}
		 	}
		 	PeaControl.resetFocus();

		} else if (command.equals("eventButtonFont")) {
			Object[] fontSizes = {"8", "10", "12", "14", "16","18", "20"};
		 	String fontString = (String) JOptionPane.showInputDialog(LockFrameCalendar.getFrame(), 
		 			CalendarSetting.calTranslate("font_size_event"), CalendarSetting.calTranslate("font_size_event"), 
		 			JOptionPane.PLAIN_MESSAGE, null, 
		 			fontSizes, fontSizes[2]);
		 	if (fontString != null) {
		 		int fontSize = Integer.parseInt(fontString);
		 		CalendarSetting.setEventButtonFont(new Font(Font.SANS_SERIF, Font.PLAIN,fontSize));
				boolean oldDocStatus = LockFrameCalendar.getFrame().isContentUnsaved();//LockFrameContact.isDocChangeUnsaved();
				CalendarControl.refreshView();
				//LockFrameContact.setDocChangeUnsaved(oldDocStatus);
				if (LockFrameCalendar.getView() instanceof WeekPanel) {
					LockFrameCalendar.getFrame().pack();
				}
				if (oldDocStatus == true) {
					LockFrameCalendar.getFrame().markUnsavedContentChanges();
				} else {
					LockFrameCalendar.getFrame().markContentIsSaved();
				}
		 	}
		 	PeaControl.resetFocus();

		} else if (command.equals("weekNumber")) {
			Object[] numberOptions = {"1", "3", "5", "7", "9", "11"};
		 	String weekString = (String) JOptionPane.showInputDialog(LockFrameCalendar.getFrame(), 
		 			CalendarSetting.calTranslate("number_weeks"), CalendarSetting.calTranslate("number_weeks"), 
		 			JOptionPane.PLAIN_MESSAGE, null, 
		 			numberOptions, numberOptions[2]);
		 	if (weekString != null) {
		 		int weekNumber = Integer.parseInt(weekString);
		 		CalendarSetting.setNumberLinesInMonthPanel(weekNumber);
		 		if (LockFrameCalendar.getView() instanceof MonthPanel) {
		 			boolean oldDocStatus = LockFrameCalendar.getFrame().isContentUnsaved();//LockFrameContact.isDocChangeUnsaved();
		 			CalendarControl.refreshView();
					if (oldDocStatus == true) {
						LockFrameCalendar.getFrame().markUnsavedContentChanges();
					} else {
						LockFrameCalendar.getFrame().markContentIsSaved();
					}
		 		}
		 	}
		 	PeaControl.resetFocus();
		}
	}
	//==================== HELPER FUNCTIONS =========================
	/**
	 * Save all currently shown calendars encrypted in all associated files
	 * 
	 * @return	error message as String or null (no error)
	 */
	public String saveAllCalendars() {

		// get all active Calendars as Collection:
		Collection <PCalendar> ncsCol =  CalendarControl.getShownAndHiddenCalendarList();
		// check if collection is null or empty
		if (ncsCol == null || ncsCol.size() == 0) {
			PeaDialog.showMessage(this, CalendarSetting.calTranslate("calendar_missing")
					+ "\n → " + CalendarSetting.calTranslate("file")
					+ "\n    → " + CalendarSetting.calTranslate("new")
					);
			return CalendarSetting.calTranslate("calendar_missing");
		}			
		
		// create a HashMap of all used file names as key and their content as value (first empty):
		String[] allFileNames = CalendarControl.getAllAssociatedFileNames();
		// create map for cloud file contents
		HashMap<String, HashMap<String, byte[]>> cloudMap = new HashMap<String, HashMap<String, byte[]>>();
		// create map for plain texts: <file name , PCard as String>
		HashMap<String, StringBuilder> map = new HashMap<String, StringBuilder>();
		for (String fileOfAllFileName : allFileNames) {
			map.put(fileOfAllFileName, new StringBuilder());
		}
		// get all used calendars:
		PCalendar[] ncs = ncsCol.toArray(new PCalendar[ncsCol.size()]);
		int calLen = ncs.length;
		for (int i = 0; i < calLen; i++) {
			PCalendar c = ncs[i];
			// get the calendar as String
			String calAsString = WriteICal.iCalToString(c);
			if (calAsString == null || calAsString.length() == 0) {
				new UnexpectedValueException("String", "calAsString", "is null or empty").printDescription();
			}
			// get all file names, the calendars were loaded from:
			String[] calFileNames = c.getAssociatedFileNames();		
			//for(String s : calFileNames)TestLog.o(ContactMenuBar.class, "Associated: "+ s);
			// add content (PCard) to map
			for (String fileNameOfCal : calFileNames) {
				StringBuilder sb = map.get(fileNameOfCal);
				//TestLog.o(ContactMenuBar.class, "Append "+ calAsString +"\nTO "+ fileNameOfCal);
				// append this calendar to the content
				if (sb == null) {
					sb = new StringBuilder(calAsString);
				} else {
					sb.append(calAsString);
				}
			}
		}

		// Write the contents to the files:
		for (HashMap.Entry<String, StringBuilder> entry : map.entrySet()) {
		    //TestLog.o(ContactMenuBar.class, entry.getKey() + "/" + entry.getValue());
			if (entry == null) {
				continue;
			}
			if (entry.getKey() == null) {
				continue;
			}
			byte[] plainBytes = new String(entry.getValue()).getBytes(PeaProperties.getCharset());
			if (plainBytes == null || plainBytes.length == 0) {
				new UnexpectedValueException("byte[]","plainBytes","is null: "+ entry.getKey()).printDescription();
			}
			byte[] cipherBytes = CipherStuff.getInstance().encrypt(plainBytes, 
					CipherStuff.getSessionKeyCrypt().getKey(), // keyMaterial is overwritten
					true, null ); // key is stored encrypted			

			if (entry.getKey().contains(CloudControl.getTmpDir())) {
				// Load in cloud:
				// performance: Collect files from provider and upload together
				String cloudProvider = CloudControl.getProviderNameFromTmpFile(entry.getKey());
				if (cloudMap.containsKey(cloudProvider)) {
					cloudMap.get(cloudProvider).put(CloudControl.getFileNameFromTmpFile(entry.getKey()), cipherBytes);
				} else {
					HashMap<String, byte[]> entryMap = new HashMap<String, byte[]>();
					entryMap.put(CloudControl.getFileNameFromTmpFile(entry.getKey()), cipherBytes);
					cloudMap.put(cloudProvider, entryMap);
				}

			} else {
				// write local:
				boolean success = WriteResources.write(cipherBytes,entry.getKey(), null);
				
				if (success == false) {
					TestLog.e(CalendarMenuBar.class, "Save calendar failed");
					PeaDialog.showMessage(this, 
							PeaProperties.getVmBridge().translate("operation_failed") + ":\n"
							+ PeaProperties.getVmBridge().translate("save") + ": " + entry.getKey(), 
							PeaProperties.getVmBridge().translate("error"), 0);
					//LockFrameContact.setDocChangeUnsaved(true);
					if (LockFrameCalendar.getFrame() != null) {
						LockFrameCalendar.getFrame().markUnsavedContentChanges();
					}
					return CalendarSetting.calTranslate("error") + "\n" + CipherStuff.getErrorMessage();
				} else {
					TestLog.o(CalendarMenuBar.class, "Calendars saved: " + entry.getKey());
					//LockFrameContact.setDocChangeUnsaved(false);
					//LockFrameContact.getFrame().markContentIsSaved();
					//return null;
				}
			}
		}
		// when uploading fails, this will be reset
		LockFrameCalendar.getFrame().markContentIsSaved();
		
		// Upload collected maps to the clouds in threads:
		Iterator<Map.Entry<String, HashMap<String, byte[]>>> providerIterator = cloudMap.entrySet().iterator();
		while (providerIterator.hasNext()) {
		        Map.Entry<String, HashMap<String, byte[]>> pair = (Map.Entry<String, HashMap<String, byte[]>>)providerIterator.next();
			// A file can be uploading by sending a PUT request to the file and sending the raw file contents as the request body.
			// check if content exists 
			String cloudName = pair.getKey();
			HashMap<String, byte[]> providerMap = pair.getValue();
			UploadWorker uw = new UploadWorker((Window)PeaControl.getDialog().getLockFrame(), cloudName, null, 
			null, providerMap, true);
			uw.execute();
		}
		return null;
	}
	
	/**
	 * Export a calendar
	 * 
	 * @param cal	the calendar to export
	 * @param owner	window owner for dialogs
	 * 
	 * @return true for success, false otherwise
	 */
	public static boolean exportCalendar(PCalendar cal, Window owner) {
		
		String selectedCalendarName = cal.getName();
		if (owner == null) {
			TestLog.ve(CalendarMenuBar.class, "Missing window owner for export of " + selectedCalendarName, 3);
			owner = JREProperties.getMainWindow();
		}

		// open file chooser to select a directory to store files
		PeaFileChooser pfc = new PeaFileChooser(owner);//JREProperties.getMainWindow());
		pfc.setFileSelectionMode(PeaFileChooser.DIRECTORIES_ONLY);
		pfc.setApproveButtonText(CalendarSetting.calTranslate("select"));
		pfc.customizeFileChooser(CalendarSetting.calTranslate("export"), CalendarSetting.calTranslate("select_folder_for_calendar_file"), false);
		pfc.showOpenDialog(owner);//JREProperties.getMainWindow());
		// get selected directory
		File directory = pfc.getSelectedFile();

		if (directory != null && directory.isDirectory()) {
			
			// if file is shown: break
			String f = new File(directory.getAbsolutePath()+ File.separator + selectedCalendarName + ".ics").getAbsolutePath();
			if (Converter.arrayToArrayList(CalendarControl.getAllAssociatedFileNames()).contains(f)) {
				PeaDialog.showMessage(owner,//JREProperties.getMainWindow(), 
						JREProperties.getVmBridge().translate("file_already_open"), 
						JREProperties.getVmBridge().translate("error"), 0);
				return false;
			}		
		
			// if file already exists: ask to overwrite
			if (((FileTypePanel)JREProperties.getTypePanel()).askToOverwriteFile(new File(directory.getAbsolutePath()+ File.separator + selectedCalendarName + ".ics"), false) == false) {
				return false;
			}				
			//PCalendar cal =  CalendarControl.getCalendarByName(selectedCalendarName); 
			// select a format for export:
			String[] calendarFormats = { CalendarSetting.calTranslate("format_5545_extensions"),
					CalendarSetting.calTranslate("format_7986")};
			String selectedCalendarFormat = (String) JOptionPane.showInputDialog(owner,//LockFrameCalendar.getFrame(), 
					CalendarSetting.calTranslate("select_calendar_format"),
					CalendarSetting.calTranslate("calendar_format"),
					JOptionPane.QUESTION_MESSAGE, 
					null, 
					calendarFormats, 
					calendarFormats[0]);
			if (selectedCalendarFormat == null) {
				PeaControl.resetFocus();
				return false;
			}
			boolean newFormat = false;
			if (selectedCalendarFormat.equals(CalendarSetting.calTranslate("format_5545_extensions"))) {
				newFormat = false;
			} else {
				newFormat = true;
			}
			String calString = WriteICal.singleICalToString(cal, newFormat);
			boolean success = false;
			success = WriteResources.write(calString.getBytes(CalendarConstants.getCharset()), cal.getName() + ".ics", directory.getAbsolutePath());
			if (success == false) {
				LockFrameCalendar.getFrame().setMessage(CalendarSetting.calTranslate("operation_failed"), true);
				PeaDialog.showMessage(owner,//JREProperties.getMainWindow(), 
						CalendarSetting.calTranslate("operation_failed") + "\n"
						+  WriteResources.getLastErrorMessage(), CalendarSetting.calTranslate("error"), 0);
				return false;
			} else {
				LockFrameCalendar.getFrame().setMessage(CalendarSetting.calTranslate("export") + ": " + directory.getAbsolutePath() + File.separator + cal.getName() + ".ics", false);
				return true;
			}

		} else {
			TestLog.e(CalendarMenuBar.class, "No directory selected to export calendar...");
			return false;
		}
	}
	
	/**
	 * Show an option dialog to choose a calendar name
	 * from the currently opened calendars
	 * 
	 * @param message 	the message to show in the dialog 
	 * 					(why select)
	 * 
	 * @return	the selected calendar name or null if no
	 * 			calendar was selected or an error occurred
	 */
	private String selectCalendar(String message) {
		
		// select calendar
		String[] calendarNames = CalendarControl.getShownCalendarNameList();	
		if (calendarNames != null && calendarNames.length > 0) {
			
			String selectedCalendarName = (String) JOptionPane.showInputDialog(JREProperties.getMainWindow(), 
					message,
					CalendarSetting.calTranslate("select_calendar"),
					JOptionPane.QUESTION_MESSAGE, 
					null, 
					calendarNames, 
					calendarNames[0]);
			if (selectedCalendarName == null || selectedCalendarName.equals("") ) {
				return null;
			} else {
				return selectedCalendarName;
			}

		} else {
			PeaDialog.showMessage(this, CalendarSetting.calTranslate("calendar_missing")
					+ "\n → " + PeaProperties.getVmBridge().translate("file")
					+ "\n    → " + PeaProperties.getVmBridge().translate("new")
					);
			return null;
		}
	}
	
	/**
	 * For keyboard accessibility of yearSpinner (navigate to other year)
	 */
	private class FocusOnYearSpinnerAction extends AbstractAction {
		@Override
		public void actionPerformed(ActionEvent arg0) {
			yearSpinner.requestFocus();			
		}		
	}
}
