package cologne.eck.calendar_pea.gui;

/*
 * PeaFactory - Production of Password Encryption Archives
 * Copyright (C) 2015  Axel von dem Bruch
 * 
 * This library is free software; you can redistribute it and/or modify it 
 * under the terms of the GNU General Public License as published 
 * by the Free Software Foundation; either version 2 of the License, 
 * or (at your option) any later version.
 * This library is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
 * or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU General Public License for more details.
 * See:  http://www.gnu.org/licenses/gpl-2.0.html
 * You should have received a copy of the GNU General Public License 
 * along with this library.
 */

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.border.LineBorder;

import cologne.eck.all_peas.cloud.CloudControl;
import cologne.eck.all_peas.cloud.StorageProvider;
import cologne.eck.all_peas.control.ContentUtilities;
import cologne.eck.all_peas.control.PathFileManager;
import cologne.eck.all_peas.control.PeaControl;
import cologne.eck.all_peas.data.PeaProperties;
import cologne.eck.all_peas.files.CloudFilePanel;
import cologne.eck.all_peas.files.FileTypePanel;
import cologne.eck.all_peas.gui.BorderButtonFocusListener;
import cologne.eck.all_peas.gui.PeaBorderFactory;
import cologne.eck.all_peas.gui.PeaDialog;
import cologne.eck.all_peas.gui.PeaLockFrame;
import cologne.eck.all_peas.gui.PswDialogView;
import cologne.eck.all_peas.vm_specific.JREProperties;
import cologne.eck.calendar_pea.control.CalendarControl;
import cologne.eck.calendar_pea.control.CalendarSetting;
import cologne.eck.calendar_pea.data.PCalendar;
import cologne.eck.calendar_pea.gui.calendar_view.CalendarView;
import cologne.eck.calendar_pea.gui.calendar_view.DayPanel;
import cologne.eck.calendar_pea.gui.calendar_view.MonthPanel;
import cologne.eck.calendar_pea.gui.calendar_view.WeekPanel;
import cologne.eck.calendar_pea.util.ReadICal;
import cologne.eck.calendar_pea.util.WriteICal;
import cologne.eck.peafactory.crypto.CipherStuff;
import cologne.eck.tools.Converter;
import cologne.eck.tools.ExceptionHelper;
import cologne.eck.tools.PropertyHandler;
import cologne.eck.tools.ReadResources;
import cologne.eck.tools.TestLog;
import cologne.eck.tools.UnexpectedValueException;


@SuppressWarnings("serial")
public class LockFrameCalendar extends PeaLockFrame 
		implements WindowListener, ActionListener {
	
	private static LockFrameCalendar frame;
	
	private static CalendarView view;// = new WeekPanel(LocalDate.now(), PeaProperties.getFrameDim());
	
	private static CalendarMenuBar menuBar;
	/*
	 * The label for messages and errors
	 */
	private static JLabel messageLabel;
	
	private static JPanel viewPanel;
	
	private static TodoPanel todoPanel;
	private static JButton showHideTodosButton;
	
	private static JComboBox<String> viewList;
	
	// if the content of the current document was modified: true
	private boolean docChangeUnsaved = false;

	
	private static JButton saveStatusButton = null;
	/**
	 * The Frame that contains MonthPanel or WeekPanel or DayPanel
	 * 
	 * @param location	location to display the frame
	 * @param dim
	 */
	private LockFrameCalendar(Point location, Dimension dim){
		
		boolean previousMode = PeaControl.isPlainTextMode();
		// always plain text mode - CloudMenu needs this
		PeaControl.setPlainTextMode(true);
		
		String lang = PeaProperties.getLastLanguage();
		try {
			CalendarControl.setCalendarLanguagesBundle(lang);
		} catch (MalformedURLException e) {
			TestLog.ve(LockFrameCalendar.class, "Failed to set language " + lang, 5);
		}
		
		frame = this;	
		this.addWindowListener(this);
		this.setDefaultCloseOperation(DO_NOTHING_ON_CLOSE); // do not allow to close or to exit:
		// files may remain unencrypted
		
		if (dim == null) {
			int[] dimValues = JREProperties.getFrameDim();
			dim = new Dimension(dimValues[0], dimValues[1]);//PeaProperties.getFrameDim();//CalendarSetting.getViewDimension();
		}
		
		//ResourceBundle CalendarSetting.calTranslate = CalendarSetting.getResourceBundle();
		
	    this.setIconImage( PswDialogView.getImage() );		
		this.setDefaultCloseOperation(DO_NOTHING_ON_CLOSE); // do not allow to close or to exit:
															// files may remain unencrypted
		this.addWindowListener(this);// for windowClosing
		
		JPanel contentPane = new JPanel();
		contentPane.setLayout(new BoxLayout(contentPane, BoxLayout.PAGE_AXIS));
		this.setContentPane(contentPane);		
		
		JPanel menuPanel = new JPanel();
		menuPanel.setLayout(new BoxLayout(menuPanel, BoxLayout.LINE_AXIS));
		menuBar = new CalendarMenuBar();
		menuPanel.add(menuBar);
		menuPanel.add(Box.createHorizontalGlue());
		menuPanel.setMinimumSize(new Dimension(300, 30));
		menuPanel.setMaximumSize(new Dimension((int)PswDialogView.getScreenwidth(), 40));

		contentPane.add(menuPanel);
		
		// add the view: 
		todoPanel = new TodoPanel();
		
		JPanel statusLabelPanel = new JPanel();
		statusLabelPanel.setLayout(new BoxLayout(statusLabelPanel, BoxLayout.LINE_AXIS));
		saveStatusButton = new JButton(PeaProperties.getVmBridge().translate("save") + "?");
	//	saveStatusButton.setContentAreaFilled(false); 
		saveStatusButton.setBorder(PeaBorderFactory.getButtonBorder());//new LineBorder(Color.GRAY));
	//	saveStatusButton.setMargin(new Insets(2,2,2,2));
		saveStatusButton.setFont(new Font(Font.SERIF, Font.ITALIC, 12));
		saveStatusButton.addActionListener(this);
		saveStatusButton.setActionCommand("save");
		saveStatusButton.setVisible(false);
		statusLabelPanel.add(Box.createHorizontalGlue());//Strut(50));
		statusLabelPanel.add(saveStatusButton);
		statusLabelPanel.add(Box.createHorizontalGlue());
		statusLabelPanel.setMaximumSize(new Dimension((int) PswDialogView.getScreenwidth(), 50));
		
		String[] viewStrings = { CalendarSetting.calTranslate("day_view"), 
				CalendarSetting.calTranslate("week_view"), 
				CalendarSetting.calTranslate("month_view") };
		viewList = new JComboBox<String>(viewStrings);
		viewList.getAccessibleContext().
	    setAccessibleDescription(
	    	    "Clicking this component opens the day, week or month view.");
		int selectedIndex = 0;
		if (view instanceof DayPanel) {
			selectedIndex = 0;
		} else if (view instanceof WeekPanel) {
			selectedIndex = 1;
		} else if (view instanceof MonthPanel) {
			selectedIndex = 2;
		} else {
			view = CalendarSetting.getStartView();
		}
		viewList.setSelectedIndex(selectedIndex);
		viewList.addActionListener(this);
		statusLabelPanel.add(viewList);	
		
		statusLabelPanel.add(Box.createHorizontalStrut(25));
		
		JLabel todoLabel = new JLabel(CalendarSetting.calTranslate("todo") + ": ");
		todoLabel.setLabelFor(showHideTodosButton);
		statusLabelPanel.add(todoLabel);
		
		if (CalendarSetting.isShowTodos() == false) {
			showHideTodosButton = new JButton(" __ ");
		} else {
			showHideTodosButton = new JButton(" X ");//setText("□");//"\25A1");	
		}
		showHideTodosButton.getAccessibleContext().
	    setAccessibleDescription(
	    	    "Clicking this component shows or hides the view of todos.");
		showHideTodosButton.setContentAreaFilled(false);
		showHideTodosButton.setBorder(new LineBorder(Color.GRAY, 1));
		showHideTodosButton.setBackground(contentPane.getBackground());
		showHideTodosButton.setMinimumSize(new Dimension(20, 20));
		showHideTodosButton.setMaximumSize(new Dimension(40, 40));
		showHideTodosButton.addActionListener(this);
		showHideTodosButton.setActionCommand("showHide");
showHideTodosButton.addFocusListener(BorderButtonFocusListener.getInstance());
		//showHideTodosButton.
		
		statusLabelPanel.add(Box.createHorizontalStrut(10));
		statusLabelPanel.add(showHideTodosButton);
		
		contentPane.add(statusLabelPanel);
		contentPane.add(Box.createVerticalStrut(10));
		
		JPanel allPanel = new JPanel();
		allPanel.setLayout(new BoxLayout(allPanel, BoxLayout.LINE_AXIS));
		contentPane.add(allPanel);
		
		JPanel calPanel = new JPanel();
		calPanel.setLayout(new BoxLayout(calPanel, BoxLayout.PAGE_AXIS));
		
		allPanel.add(calPanel);

		// add the view: 
		viewPanel = new JPanel();
		viewPanel.setLayout(new GridLayout(1,1));
		viewPanel.add((Component) view);
		calPanel.add(viewPanel);		
		allPanel.add(todoPanel);
		
		JPanel messagePanel= new JPanel();
		messagePanel.setLayout(new BoxLayout(messagePanel, BoxLayout.LINE_AXIS));
		messageLabel = new JLabel();
		messagePanel.add(messageLabel);
		messagePanel.add(Box.createHorizontalGlue());
		contentPane.add(messagePanel);
		contentPane.add(Box.createVerticalStrut(10));
		
		JPanel forwardBackPanel = new JPanel();
		forwardBackPanel.setLayout(new BoxLayout(forwardBackPanel, BoxLayout.LINE_AXIS));
		JButton backButton = new JButton(CalendarSetting.calTranslate("back"));
		backButton.getAccessibleContext().
	    setAccessibleDescription(
	    	    "Clicking this component causes the display of the previous day/week/month.");
		backButton.addActionListener(this);
		backButton.setActionCommand("back");
		backButton.setBorder(PeaBorderFactory.getButtonBorder());
		forwardBackPanel.add(backButton);
		forwardBackPanel.add(Box.createHorizontalGlue());
		JButton todayButton = new JButton(CalendarSetting.calTranslate("today"));
		todayButton.getAccessibleContext().
	    setAccessibleDescription(
	    	    "Clicking this component causes the display of the current day/week/month.");
		todayButton.addActionListener(this);
		todayButton.setActionCommand("today");
		todayButton.setBorder(PeaBorderFactory.getButtonBorder());
		forwardBackPanel.add(todayButton);
		forwardBackPanel.add(Box.createHorizontalGlue());
		JButton forwardButton = new JButton(CalendarSetting.calTranslate("forward"));
		forwardButton.getAccessibleContext().
	    setAccessibleDescription(
	    	    "Clicking this component causes the display of the next day/week/month.");
		forwardButton.addActionListener(this);
		forwardButton.setActionCommand("forward");
		forwardButton.setBorder(PeaBorderFactory.getButtonBorder());
		forwardBackPanel.add(forwardButton);		
		contentPane.add(forwardBackPanel);
		
		this.setLocation(location);
		pack();// otherwise vertical scroll bar is not set...

		this.setSize(dim);

		if (view instanceof WeekPanel) {
			this.setMinimumSize( new Dimension (500, 300));//this.getSize() );
		} else {
			this.setMinimumSize( new Dimension (400, 300));//this.getSize() );
		}
		// reset mode:
		PeaControl.setPlainTextMode(previousMode);
	}

	/**
	 * Create the instance of LockFrameContact.
	 * There is always only one instance. The get the
	 * existing instance, call getFrame()
	 * 
	 * @param loc	the location to show
	 * @param dim	the dimension to show
	 * 
	 * @return	the instance of LockFrameContact, never null
	 */
	public final static LockFrameCalendar getInstance(Point loc, Dimension dim){

		if (frame == null) {
			frame = new LockFrameCalendar(loc, dim);// selectedFileNames);
		}
		return frame;
	}
	/**
	 * Get the instance of LockFrameContact. Note: If this
	 * is not created, the functions returns null
	 * 
	 * @return	the current LockFrameContact or null
	 */
	public final static LockFrameCalendar getFrame() {
		return frame;
	}
	
	@Override
	public void windowActivated(WindowEvent arg0) {}
	@Override
	public void windowClosed(WindowEvent arg0) {}

	@Override
	public void windowClosing(WindowEvent arg0) {
		close();
	}

	@Override
	public void windowDeactivated(WindowEvent arg0) {}
	@Override
	public void windowDeiconified(WindowEvent arg0) {}
	@Override
	public void windowIconified(WindowEvent arg0) {}
	@Override
	public void windowOpened(WindowEvent arg0) {}

	protected void close() {		

		PeaProperties.setLastUsedFiles(getUsedFileNames());
		
		// 1. check if unsaved changes, ask to save
		if (checkUnsavedContentChanges() == true) {
			// do not check twice
			markContentIsSaved();
			return;
		} else {
			try {
				// Check if single empty file (initialization): this not marked as unsaved
				if (CalendarControl.getShownCalendarList().size() == 1) {
					PCalendar shownCal = CalendarControl.getShownCalendarList().get(0);
					if (shownCal != null) {
						if (shownCal.getEventList().size() == 0 && shownCal.getTodoList().size() == 0) {
							String[] calFileName = shownCal.getAssociatedFileNames();{
								if (calFileName.length == 1) {
									((CalendarMenuBar)((JPanel)((JPanel)frame.getContentPane()).getComponent(0)).getComponent(0)).saveAllCalendars();
									markContentIsSaved();
								}
							}
						}	
					}
				}
			} catch (Exception e) {
				TestLog.ve(LockFrameCalendar.class, e.getMessage(), 5);
			}
		}
		
		// 2. check if all files are in path file and ask to store if not
		try {
			HashSet<String> availableSet = new HashSet<String>(PeaControl.getAvailableFileNames());
			//TestLog.o(LockFrameContact.class, "Available\n");for(String s : PeaControl.getAvailableFileNames()) TestLog.o(LockFrameContact.class, s);
			//TestLog.o(LockFrameContact.class, "Associated\n");for(String s: CalendarControl.getAllAssociatedFileNames())TestLog.o(LockFrameContact.class, s);
			if (CalendarControl.getAllAssociatedFileNames() != null) {
				availableSet.addAll( new ArrayList<>(Arrays.asList(CalendarControl.getAllAssociatedFileNames())));
			}
			if (availableSet.size() > 0) {
				PathFileManager.addFileNamesToPathFile(availableSet, true);
			}
		} catch (Exception e) {
			TestLog.e(LockFrameCalendar.class, "Could store file names for next session...");
			TestLog.ve(LockFrameCalendar.class, e.getMessage() + "- " + e.toString(), 5);
		}

		// 3. save properties
		try {
			new PropertyHandler().updateAllPropValues();
		} catch (Exception e) {
			TestLog.e(LockFrameCalendar.class, "Could not store properties for next session...");
			TestLog.ve(LockFrameCalendar.class, e.getMessage() + "- " + e.toString(), 5);
		}

		// 4. check if cloud processes are still running and ask to exit
		if (CloudControl.getNumberOfRunningCloudProcesses() > 0) {
			String[] options = {PeaProperties.getVmBridge().translate("cancel"),
					PeaProperties.getVmBridge().translate("ok")};			
			int result = PeaDialog.showOptionYAxis(this, 
					PeaProperties.getVmBridge().translate("cloud_process_warning")
					+ "\n" + PeaProperties.getVmBridge().translate("terminating_damage"), 
					PeaProperties.getVmBridge().translate("warning"), 
					2,  options, 0);
			if (result == 1) {
				TestLog.o(LockFrameCalendar.class, "Closing although cloud process is running...");
			} else {
				TestLog.o(LockFrameCalendar.class, "Upload process is running.... Break exit...");
				return;
			}
		}	

		// 5. clear key
		CipherStuff.getSessionKeyCrypt().clearKeys();

		// 6. Close all providers
		Collection<StorageProvider> openProviders = CloudControl.getOpenProviders();
		if (openProviders != null &&  ! openProviders.isEmpty()) {
			for (StorageProvider sp : openProviders) {
				String error = sp.closeSession();
				if (error != null) {
					TestLog.e(LockFrameCalendar.class, error);
				}
			}
		}

		// 7. delete all temporary backup files of cloud files:
		try {
			FileTypePanel ftp = (FileTypePanel)JREProperties.getTypePanel();
			if (ftp != null) {
				CloudFilePanel cfp = ftp.getCloudFilePanel();
				if (cfp != null && cfp.getCloudControl() != null) {
					String[] providers = cfp.getCloudControl().getUsedProviders();

					if (providers != null ) {
						for (String provName : providers) {						
							String[] filesOfProv = cfp.getCloudControl().getUsedFileNamesOfProvider(provName);
							if (filesOfProv != null) {
								for (String fileName : filesOfProv) {
									File fileToDelete = new File(CloudControl.getTmpFileNameForCloudFile(fileName, provName, null));
									boolean deleteResult = fileToDelete.delete();
									if (deleteResult == false) {
										TestLog.e(LockFrameCalendar.class, "Could not delete " + fileToDelete);
									} else {
									}
								}
							}
						}
					}
				}
			}
		} catch (Exception e) {
			TestLog.e(LockFrameCalendar.class, "Could not delete (encrypted) cloud backup files...");
			TestLog.ve(LockFrameCalendar.class, e.getMessage() + "- " + e.toString(), 5);
		}
		TestLog.o(LockFrameCalendar.class, "Exit");
		System.exit(0);			
	}
	
	/**
	 * Check if the currently shown calendars
	 * were modified and not saved manually and ask 
	 * to save them in this case. 
	 * 
	 * @return	true if save process is initialized, 
	 * 			false if no unsaved changes or unsaved
	 * 			changes should remain
	 */
	@Override
	public boolean checkUnsavedContentChanges() {
		if (docChangeUnsaved == true) {
			int n = JOptionPane.showConfirmDialog(
				    this,
				    PeaProperties.getVmBridge().translate("save_modifications"),//"Save changes?",
				    PeaProperties.getVmBridge().translate("unsaved_modifications"),//"Unsaved Changes",
				    JOptionPane.YES_NO_OPTION);
			if (n == JOptionPane.YES_OPTION) {
				// save calendars in default file
				((CalendarMenuBar)((JPanel)((JPanel)frame.getContentPane()).getComponent(0)).getComponent(0)).saveAllCalendars();
				return true;
			} else {
				return false;
			}
		}
		return false; // no unsaved content remains
	}
	@Override
	public void markUnsavedContentChanges() {
		docChangeUnsaved = true;
		if (saveStatusButton != null) {
			saveStatusButton.setVisible(true);
		}
	}

	@Override
	public void markContentIsSaved() {
		docChangeUnsaved = false;
		if (saveStatusButton != null) {
			saveStatusButton.setVisible(false);
		}
	}
	
	@Override
	public boolean isContentUnsaved() {
		// TODO Auto-generated method stub
		return docChangeUnsaved;
	}

	@Override
	public String saveCurrentContentInCloudTempFile(String providerName, String fileNameToStore) {

		HashSet<PCalendar> calsInFileSet = CalendarControl.getCalendarsOfFileAsSet(fileNameToStore);
		if (calsInFileSet == null) {
			new UnexpectedValueException("HashSet<PCard>","calsInFileSet", "is null - file does not contain a calendar: "+ fileNameToStore).printDescription();
			return CalendarSetting.calTranslate("no_valid_calendar_found")
					 + fileNameToStore;
		}

		// get calendars as one String:
		//String plainString = WriteCard.iCalsToString(ncs);
		PCalendar[] calsInFile = calsInFileSet.toArray(new PCalendar[calsInFileSet.size()]);
		String plainString = WriteICal.iCalsToString(calsInFile);
		if (plainString == null || plainString.length() == 0) {
			new UnexpectedValueException("String", "plainString", "is null or empty").printDescription();
			return CalendarSetting.calTranslate("no_valid_calendar_found") + " (empty) " 
					 + fileNameToStore;

		}
		String orig = null;
		if ( ! fileNameToStore.contains(CloudControl.getTmpDir())) {
			orig = fileNameToStore;
		}

		String tmpFileName = CloudControl.getTmpFileNameForCloudFile(new File(fileNameToStore).getName(), providerName, orig);
		File file = new File(tmpFileName);
		if (file.exists() == false) {
			try {
				file.getParentFile().mkdirs();
				file.createNewFile();
				TestLog.o(LockFrameCalendar.class, "Temp file created for cloud " + providerName + ": " + file.getAbsolutePath());
			} catch (IOException e) {
				TestLog.e(LockFrameCalendar.class, fileNameToStore + " - " + new ExceptionHelper(e).getInfos());
				return PeaProperties.getVmBridge().translate("error") + "\nCreate tmp file failed";
			}
		}
		// encrypt the calendars by session key and store in file
		// TODO: if calendar unchanged: copy encrypted content (same IV)
		boolean success = ContentUtilities.saveContent(CipherStuff.getSessionKeyCrypt().getKey(), 
				plainString.getBytes(PeaProperties.getCharset()), null,
				tmpFileName, true);
		if (success == false) {
			TestLog.e(LockFrameCalendar.class, fileNameToStore + " - " + "Save calendar in temp file failed");
			return PeaProperties.getVmBridge().translate("error") + "\n" + CipherStuff.getErrorMessage();
		} else {
			//TestLog.o(LockFrameContact.class, "Calendars saved in temp file " + tmpFileName);
			return null;
		}
	}

	@Override
	public void actionPerformed(ActionEvent ae) {

		if (ae.getSource() instanceof JButton) {

			String command = ae.getActionCommand();
			if (command.equals("save")) {
				// save calendars in default file
				String error = ((CalendarMenuBar)((JPanel)((JPanel)frame.getContentPane()).getComponent(0)).getComponent(0)).saveAllCalendars();
				if (error == null) {
				
					// UploadWorker did not return yet...
					//markContentIsSaved();
				} else {
					TestLog.ve(LockFrameCalendar.class, error, 5);
				}					
			} else if (command.equals("back")) {
				LocalDate shownDate = view.getFirstShownDate();
				if (view instanceof DayPanel) {
					shownDate = shownDate.minusDays(1);
				} else if (view instanceof WeekPanel) {
					shownDate = shownDate.minusDays(7);
				} else if (view instanceof MonthPanel) {
					shownDate = shownDate.minusDays(CalendarSetting.getNumberLinesInMonthPanel() * 7);
				}
				view.setFirstShownDate(shownDate);
				view.refreshView();

			} else if (command.equals("today")) {
				LocalDate shownDate = LocalDate.now();
				view.setFirstShownDate(shownDate);
				view.refreshView();

			} else if (command.equals("forward")) {
				LocalDate shownDate = view.getFirstShownDate();
				if (view instanceof DayPanel) {
					shownDate = shownDate.plusDays(1);
				} else if (view instanceof WeekPanel) {
					shownDate = shownDate.plusDays(7);
				} else if (view instanceof MonthPanel) {
					shownDate = shownDate.plusDays(CalendarSetting.getNumberLinesInMonthPanel() * 7);
				}
				view.setFirstShownDate(shownDate);
				view.refreshView();
			} else if (command.equals("showHide")) {
				todoPanel.collapseOrExpand();
			}
		} else if (ae.getSource() instanceof JComboBox) {

			@SuppressWarnings("rawtypes")
			int viewIndex = ((JComboBox)ae.getSource()).getSelectedIndex();
			// 0 is day, 1 is week, 2 is month
			changeView(viewIndex);
		}
	}

	/**
	 * Change the current view 
	 * 
	 * @param viewIndex		the index of the  view: 
	 * 						0 for day view, 1 for week view, 2 for month view
	 */
	public void changeView(int viewIndex) {
		
		Dimension dim = ((Component)view).getSize();
		LocalDate date = LockFrameCalendar.view.getFirstShownDate();
		if (viewIndex == 0) {
			if (LockFrameCalendar.view instanceof DayPanel){
				return;
			}		
			viewPanel.remove((Component) view);//MonthPanel.getInstance());
			DayPanel dp = new DayPanel(date, dim, false);
			//LockFrameContact.view = dp;
			LockFrameCalendar.setView(dp);
			viewPanel.add(dp);
			viewList.setSelectedIndex(0);// called from date label
		} else if (viewIndex == 1) { // WeekPanel
			if (LockFrameCalendar.view instanceof WeekPanel){
				return;
			}			
			viewPanel.remove((Component) view);
			WeekPanel wp = new WeekPanel(date, dim);//viewSize);
			viewPanel.add(wp);
			//LockFrameContact.view = wp;
			LockFrameCalendar.setView(wp);
			viewList.setSelectedIndex(1);// called from date label
		} else if (viewIndex == 2) {
			if (LockFrameCalendar.view instanceof MonthPanel){
				return;
			}
			viewPanel.remove((Component)view);
			MonthPanel monthPanel = new MonthPanel(date, dim);//viewSize);
			//LockFrameContact.view.setFirstShownDate(date);
			viewPanel.add(monthPanel);
			//LockFrameContact.view = monthPanel;
			LockFrameCalendar.setView(monthPanel);
			viewList.setSelectedIndex(2);// called from date label
		}
		viewPanel.revalidate();// refresh may cause NullPointer otherwise
		viewPanel.repaint();
		view.refreshView();// maybe an event was added
		
	}
	/**
	 * Display a message: error in red, otherwise black
	 * 
	 * @param message	the message to show
	 * @param isError	true if message is an error
	 * 					(message font is red)
	 */	
	@Override	
	public void setMessage(String message, boolean isError) {
		if(isError == true) {
			messageLabel.setForeground(Color.red);
			messageLabel.setText(message);			
		} else {
			messageLabel.setForeground(Color.black);
			messageLabel.setText(message);
		}
	}

	/**
	 * Get the current view: day, week, month
	 * 
	 * @return	the current view
	 */
	public static CalendarView getView() {
		return view;
	}
	
	/**
	 * Get the current todo panel
	 * 
	 * @return	the current todo panel
	 */
	public static TodoPanel getTodoView() {
		return todoPanel;
	}

	/**
	 * Set the view: day,week or month
	 * 
	 * @param _view
	 */
	public static synchronized void setView(CalendarView _view) {
		LockFrameCalendar.view = _view;
	}


	@Override
	public String[] getCurrentFileNames() {
		return CalendarControl.getAllAssociatedFileNames();
	}
	@Override
	public void setCurrentFileNames(String[] fileNamesToSet) {
		new UnexpectedValueException("function", "setCurrentFileNames", "not implemented").printDescription();
	}

	@Override
	public void updateModelWithCloudFiles(File[] files, String providerName) {
		
	//	String modelError = "";
		for (int i = 0; i < files.length;i++) {
			HashSet<PCalendar> cals = CalendarControl.getFileCalendarsMap().get(files[i].getAbsolutePath());
			if (cals != null && cals.size() > 0) {
				Iterator<PCalendar> calIterator = cals.iterator();
				while(calIterator.hasNext()) {
					for (int j = 0; j < cals.size(); j++) {
						PCalendar cal = calIterator.next();
						// get the backup file 
						String cloudFileTmp = CloudControl.getTmpFileNameForCloudFile(files[i].getName(), providerName, null);
						// add associated file name to calendar
						cal.addAssociatedFileName(cloudFileTmp);
						// add to file-calendar map:
						CalendarControl.addToFileCalendarsMap(cloudFileTmp, cal);
					}
				}		
			} else {
				// new uploaded: Get file-calendar map (was set in CloudMenu)
				if (((FileTypePanel)JREProperties.getTypePanel()) != null 
						&& ((FileTypePanel)JREProperties.getTypePanel()).getCloudFilePanel() != null) {

					String origin = ((FileTypePanel)JREProperties.getTypePanel()).getCloudFilePanel().getCloudControl().getCloudFileModel().getLocalOrigin(files[i].getAbsolutePath());
					if (origin == null) { // there is no origin, only the cloud file
						TestLog.e(LockFrameCalendar.class, "Can't find the original local file that is uploaded to the cloud: " + files[i].getAbsolutePath());
						origin = files[i].getAbsolutePath();
					}
					HashSet<PCalendar> originCals = CalendarControl.getCalendarsOfFileAsSet(origin);
					if (originCals == null || originCals.size() == 0) {

					} else {
						Iterator<PCalendar> originCalsIterator = originCals.iterator();
						while (originCalsIterator.hasNext()) {
							PCalendar cal = originCalsIterator.next();
							// get the backup file 
							String cloudFileTmp = CloudControl.getTmpFileNameForCloudFile(files[i].getName(), providerName, null);
							cal.addAssociatedFileName(cloudFileTmp);
							//TestLog.o(LockFrameContact.class, "added: " + cloudFileTmp + " in " + cal.getName());
							// add to file-calendar map:
							CalendarControl.addToFileCalendarsMap(cloudFileTmp, cal);
						}
					}
				} else { // Initialization and save in cloud
					TestLog.ve(LockFrameCalendar.class, "Missing cloud file panel to set calendar map...");
				}
			}
		}
	}

	@Override
	public String decryptAndLoadFileToView(String fileName) {
		byte[] cipherText = ReadResources.readExternFile(fileName);
		if (cipherText == null) {
			new UnexpectedValueException("byte[]", "cipherText", "is null").printDescription();
			return fileName + " - " + PeaProperties.getVmBridge().translate("no_access");
		}
		// check password identifier
		byte[] keyMaterial = CipherStuff.getSessionKeyCrypt().getKey();
		String error = PeaControl.checkPassword(cipherText, keyMaterial);
		if (error != null) {
			new UnexpectedValueException("String", "error", "is not null").printDescription();
			return fileName + " - " + error;
		} 
		// decrypt
		byte[] plainText = CipherStuff.getInstance().decrypt(
				cipherText, keyMaterial, true);
		if (plainText == null) {
			new UnexpectedValueException("byte[]", "plainText", "is null").printDescription();
			TestLog.e(LockFrameCalendar.class, CipherStuff.getErrorMessage());
			return fileName + " - " + PeaProperties.getVmBridge().translate("failed") + " (decryption)";
		}
		PCalendar[] cals = new ReadICal().readCalendarsFromBytes(plainText, fileName, true);
		if (cals == null) {
			new UnexpectedValueException("PCard[]", "cals", "is null").printDescription();
			return fileName + " - " + CalendarSetting.calTranslate("no_valid_calendar_found");
		}
		for (int i = 0; i < cals.length; i++) {
			CalendarControl.showCalendar(cals[i],fileName);
		}
		return null;// no error occurred
	}

	@Override
	public String saveCurrentContent() {
		// TODO TEST
		// ContactMenuBar:  saveAllCalendars
		String error = menuBar.saveAllCalendars();
		if (error == null) {
		} else {
			PeaDialog.showMessage(this, 
							PeaProperties.getVmBridge().translate("operation_failed") + ":\n"
					+ PeaProperties.getVmBridge().translate("save"), 
					PeaProperties.getVmBridge().translate("error"), 0);		
			((LockFrameCalendar)PeaControl.getDialog().getLockFrame() )
				.setMessage(PeaProperties.getVmBridge().translate("operation_failed"), true);	
		}
		return null;
	}
	@Override
	public final synchronized void resetFrame() {
		String lang = PeaProperties.getLastLanguage();
		try {
			CalendarControl.setCalendarLanguagesBundle(lang);
		} catch (MalformedURLException e) {
			TestLog.ve(LockFrameCalendar.class, "Failed to set language " + lang, 5);
		}
		Point loc = frame.getLocationOnScreen();
		Dimension dim = frame.getSize();
		frame.dispose();
		frame = new LockFrameCalendar(loc, dim);
		frame.setSize(dim);
		frame.setVisible(true);
		view.refreshView();
		//((ContactPEAControl) PeaControl.getDialog()).setLockFrame(frame);
		if (JREProperties.getMainWindow() == null || JREProperties.getMainWindow() instanceof PeaLockFrame) {
			JREProperties.setMainWindow(frame);
		}		
		if (todoPanel.isDisplayable() && todoPanel.isShowing() ) {
			setShowTodoButton(true);
		} else {
			setShowTodoButton(false);
		}
	}
	
	static void setShowTodoButton(boolean showing) {
		if (showing == true) {// CalendarSetting.isShowTodos() == false) {
			showHideTodosButton.setText(" X ");//" □ ");
			CalendarSetting.setShowTodos(true);
		} else {
			showHideTodosButton.setText(" __ ");
			CalendarSetting.setShowTodos(false);
		}
	}

	@Override
	public String[] chooseFileNamesToApply() {
		return null;
	}

	@Override
	public boolean changePassword() {
		LockFrameCalendar.getFrame().storeValuesBeforeChange();

		String[] fileNamesToApply = LockFrameCalendar.getFrame().chooseFileNamesToApply();
		String errorMessage = LockFrameCalendar.getFrame().changePasswordOrApplyChanges(
				LockFrameCalendar.getFrame(), fileNamesToApply);
		if (errorMessage != null) {
			PeaDialog.showMessage(LockFrameCalendar.getFrame(), 
					errorMessage, 
					PeaProperties.getVmBridge().translate("error"), 0);
			return false;
		} else {
			return true;
		}
	}

	@Override
	public ArrayList<String> getUsedFileNames() { // TODO TEST
		//return PeaControl.getAvailableFileNames();
		return Converter.arrayToArrayList(CalendarControl.getAllAssociatedFileNames());
		
		
	}

}
