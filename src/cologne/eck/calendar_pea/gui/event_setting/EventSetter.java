package cologne.eck.calendar_pea.gui.event_setting;

/*
 * PeaFactory - Production of Password Encryption Archives
 * Copyright (C) 2015  Axel von dem Bruch
 * 
 * This library is free software; you can redistribute it and/or modify it 
 * under the terms of the GNU General Public License as published 
 * by the Free Software Foundation; either version 2 of the License, 
 * or (at your option) any later version.
 * This library is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
 * or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU General Public License for more details.
 * See:  http://www.gnu.org/licenses/gpl-2.0.html
 * You should have received a copy of the GNU General Public License 
 * along with this library.
 */

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.KeyEvent;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneId;
import java.time.temporal.ChronoUnit;
import java.util.Calendar;
import java.util.Date;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSpinner;
import javax.swing.JSpinner.DateEditor;//setStartDateSpinner
import javax.swing.JTabbedPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.KeyStroke;
import javax.swing.SpinnerDateModel;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.text.Document;

import cologne.eck.all_peas.control.PeaControl;
import cologne.eck.all_peas.gui.BorderButtonFocusListener;
import cologne.eck.all_peas.gui.CloseOnEscapeAction;
import cologne.eck.all_peas.gui.IconManager;
import cologne.eck.all_peas.gui.PswDialogView;
import cologne.eck.calendar_pea.control.CalendarSetting;
import cologne.eck.calendar_pea.data.CalendarComponent;
import cologne.eck.calendar_pea.data.PEvent;
import cologne.eck.calendar_pea.data.RecurringPEvent;
import cologne.eck.calendar_pea.gui.CalendarComboBox;
import cologne.eck.calendar_pea.gui.LockFrameCalendar;
import cologne.eck.tools.TestLog;
import cologne.eck.tools.UnexpectedValueException;


@SuppressWarnings("serial")
public class EventSetter extends ComponentSetter//JDialog 
	implements  ChangeListener, DocumentListener {
	
	private PEvent existingEvent = null;

	private boolean newEvent = false;
	
	
	private JTabbedPane tabbedPane;
	private JPanel contentPaneMain;

	private JTextField summaryField;
	private JTextArea descriptionArea;
	private JTextField locationField;
	
	private CalendarComboBox calendarComboBox;
	private JSpinner startDateSpinner;
	private TimePicker startTimePicker;
	private JSpinner endDateSpinner;
	private TimePicker endTimePicker;
	
	private JLabel mainMessageLabel;
	private JLabel moreMessageLabel;
	private JLabel alarmMessageLabel;
	private JLabel recurrenceMessageLabel;
	
	private JCheckBox allDayCheck;

	private JScrollPane scroll;
	
	/*
	 * Panels to remove and add time setting for
	 * all-day events
	 */
	private JPanel endDatePanel;
	private JPanel startTimeAndDatePanel;	
	private JPanel endTimeAndDatePanel;	

	private boolean allDay = false;
	private boolean isRecurring = false;
	
	private RecurrencePanel rp;
	
	private JComboBox <String> priorityComboBox;
	private JComboBox <String> statusComboBox;		
	//private JTextField categoriesField;
	private JTextField urlField;

	/**
	 * Show an EventSetter to set date, time, summary and description...
	 * 
	 * @param owner			the owner component
	 * @param _existingEvent		the event to set or null for new event
	 * @param _newEvent		true, if the Event is not yet listed
	 * @param selectedDate	the date and time to start the EventSetters date spinner or null
	 */
	@SuppressWarnings("unchecked")
	public EventSetter(Component owner, //Point location,
			PEvent _existingEvent, boolean _newEvent, 
			LocalDateTime selectedDate) { 
		
		super.existingCalComponent = _existingEvent;
		super.newCalComponent = _newEvent;
		super.calendarComponentClass = PEvent.class.getSimpleName();
		
		// Avoid keyboard trap: Close with ESC
		this.getRootPane().getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW)
    		.put(KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), "CLOSE_ON_ESCAPE");
		this.getRootPane().getActionMap().put("CLOSE_ON_ESCAPE", new CloseOnEscapeAction() );
		
		// reset last message:
		((LockFrameCalendar)PeaControl.getDialog().getLockFrame() ).setMessage(null, false);
		mainMessageLabel = new JLabel();
		
		// initialize start and end:
		LocalDateTime defaultStartDateTime = null;
		LocalDateTime defaultEndDateTime = null;
		// add new Event or modify existing Event
		this.newEvent =	_newEvent; 

		if (newEvent == false) { // EventSetter was started by click on eventButton
			TestLog.v(EventSetter.class, "Modify existing event...");
			existingEvent = _existingEvent;
			if (existingEvent == null) {
				TestLog.e(EventSetter.class, "Event to add is null!");
				new UnexpectedValueException("existingEvent", "PEvent", "is null").printDescription();
				return;
			}
			if (existingEvent.getCalendar() == null) {
				new UnexpectedValueException("existingEvent.getCalendar()", "PCard", "for existing event is null: " + existingEvent.getSummary()).printDescription();
				return;
			}
			if (existingEvent.isRecurring()) {
				setRecurring(true);
			} else {
				setRecurring(false);
			}

			// set date start and end, even if selectedDate != null, 
			// because of recurring and several-days events:
			defaultStartDateTime = existingEvent.getDateStart();
			if (existingEvent.getDateEnd() != null) {
				if (existingEvent.isAllDay() == true 
						// default for one day event: start date = end date
						&& ! existingEvent.getDateEnd().equals(existingEvent.getDateStart())) {
					// decrement one day in spinner
					defaultEndDateTime = existingEvent.getDateEnd().minusDays(1);
				} else {
					defaultEndDateTime = existingEvent.getDateEnd();
				}
			} else {
				if (existingEvent.isAllDay() == true) { // one day event
					defaultEndDateTime = defaultStartDateTime;
				} else {
					// set one hour later
					defaultEndDateTime = existingEvent.getDateStart().plusHours(1);
					mainMessageLabel.setText("Missing date end: is set one hour after date start");
					this.pack();
				}
			}

		} else { // EventSetter was started by click on dayPanel, monthPanel...:			
			if (selectedDate != null) {
				defaultStartDateTime = selectedDate;//, LocalTime.of(12, 0));
				defaultEndDateTime = defaultStartDateTime.plusHours(1);
			} else {
				// set date to now and time to default time 12:00
				defaultStartDateTime = LocalDateTime.now().withHour(12).withMinute(0).withSecond(0);
				// set one hour later:
				defaultEndDateTime = defaultStartDateTime.plusHours(1);
			}
		}

		this.setTitle("Calendar Lock PEA - " + CalendarSetting.calTranslate("event_setting"));
		this.setModal(false);
		this.setAlwaysOnTop(true);			

		contentPaneMain = new JPanel();//(JPanel) this.getContentPane();
		this.add(contentPaneMain);
		contentPaneMain.setLayout(new BoxLayout(contentPaneMain, BoxLayout.PAGE_AXIS));
		contentPaneMain.setBorder(new EmptyBorder(10, 10, 10, 10));
		String okButtonText = CalendarSetting.calTranslate("set_event");
		if (newEvent == false) {
			okButtonText = CalendarSetting.calTranslate("change_event");
		}

		// ComboBox to select the PCard for this Event
		JComponent[] calComps = createCalendarComboBox();
		if (calComps[0] instanceof CalendarComboBox) {
		calendarComboBox = (CalendarComboBox) calComps[0];
		}
		if (calComps[1] instanceof JPanel) {
			contentPaneMain.add(calComps[1]);
		}
		// set the background color
		String cal = null;
		if (existingEvent == null) {
			cal = (String) calendarComboBox.getSelectedItem();		
		}
		setColorOfCalendarComponent(cal);

		JPanel allDayCheckPanel = new JPanel();
		allDayCheckPanel.setLayout(new BoxLayout(allDayCheckPanel, BoxLayout.LINE_AXIS));
		allDayCheck = new JCheckBox(CalendarSetting.calTranslate("all_day_event"));
		allDayCheckPanel.add(Box.createHorizontalGlue());
		allDayCheckPanel.add(allDayCheck);
		if (_newEvent == false && existingEvent != null) {
			if (existingEvent.isAllDay()) {
				allDayCheck.setSelected(true);
				allDay = true;
			} 
		}
		allDayCheck.addActionListener(this);
		allDayCheck.setActionCommand("all-day");

		//contentPaneMain.add(allDayCheckPanel);
		contentPaneMain.add(allDayCheckPanel);

		/* Panel with Spinners to choose start and end time and date */
		JPanel timeAndDatePanel = new JPanel();
		timeAndDatePanel.setLayout(new BoxLayout(timeAndDatePanel, BoxLayout.PAGE_AXIS));

		JPanel startLabelPanel = new JPanel();
		startLabelPanel.setLayout(new BoxLayout(startLabelPanel, BoxLayout.LINE_AXIS));
		JLabel startLabel = new JLabel(CalendarSetting.calTranslate("start"));
		startLabel.setFont(new Font(Font.SANS_SERIF, Font.BOLD, 12));
		startLabelPanel.add(startLabel);
		startLabelPanel.add(Box.createHorizontalGlue());
		timeAndDatePanel.add(startLabelPanel);

		/* Panel for start date and time */
		startTimeAndDatePanel = new JPanel();
		startTimeAndDatePanel.setLayout(new BoxLayout(startTimeAndDatePanel, BoxLayout.LINE_AXIS));		
		startTimeAndDatePanel.add(Box.createHorizontalStrut(10));		
		/* start date */
		JLabel startSpinnerDateLabel = new JLabel(CalendarSetting.calTranslate("date"));
		startSpinnerDateLabel.setLabelFor(startDateSpinner);
		startTimeAndDatePanel.add(startSpinnerDateLabel);
		startTimeAndDatePanel.add(Box.createHorizontalStrut(5));
		// Spinner for start date: 
		SimpleDateFormat dateModel = new SimpleDateFormat("dd.MM.yy");

		Date defaultStartDate = java.util.Date
				.from(defaultStartDateTime.atZone(ZoneId.systemDefault())
						.toInstant());

		SpinnerDateModel startDateModel = new SpinnerDateModel(defaultStartDate,
					null, null, Calendar.DATE);
		startDateSpinner = new JSpinner(startDateModel);
		startDateSpinner.setName("startDateSpinner");
		startDateSpinner.setEditor(new JSpinner.DateEditor(startDateSpinner, dateModel.toPattern()));
		startDateSpinner.setMaximumSize(new Dimension(150, 25));
		startDateSpinner.setMinimumSize(new Dimension(150, 25));
		startDateSpinner.addChangeListener(this);
		
		// add a date-picker button
		JButton startDatePickerButton = new JButton(IconManager.loadScaledIcon("date-picker.png", 
				"Date Picker to select a date", 30, 25,"Date Picker", "Date Picker to select a date"));
		startDatePickerButton.setBorder(BorderFactory.createEmptyBorder());
		startDatePickerButton.addActionListener(this);
		startDatePickerButton.setActionCommand("datePickerStart");
		startDatePickerButton.addFocusListener(BorderButtonFocusListener.getInstance());
		
		startTimeAndDatePanel.add(startDateSpinner);
		this.add(Box.createHorizontalStrut(5));
		startTimeAndDatePanel.add(startDatePickerButton);

		/* start time */
		startTimePicker = new TimePicker(defaultStartDateTime.toLocalTime());
		startTimePicker.getHourSpinner().setName("startHourSpinner");
		startTimePicker.getHourSpinner().addChangeListener(this);

		startTimeAndDatePanel.add(Box.createHorizontalGlue());
		if (allDay == false) {
			startTimeAndDatePanel.add(startTimePicker);
		}
				
		timeAndDatePanel.add(startTimeAndDatePanel);
		timeAndDatePanel.add(Box.createVerticalStrut(5));

		/* end of event */
		JPanel endLabelPanel = new JPanel();
		endLabelPanel.setLayout(new BoxLayout(endLabelPanel, BoxLayout.LINE_AXIS));
		JLabel endLabel = new JLabel(CalendarSetting.calTranslate("end"));
		endLabel.setFont(new Font(Font.SANS_SERIF, Font.BOLD, 12));
		endLabelPanel.add(endLabel);
		endLabelPanel.add(Box.createHorizontalGlue());
		timeAndDatePanel.add(endLabelPanel);		

		/* Panel for end date and time */
		endTimeAndDatePanel = new JPanel();
		endTimeAndDatePanel.setLayout(new BoxLayout(endTimeAndDatePanel, BoxLayout.LINE_AXIS));
		endTimeAndDatePanel.add(Box.createHorizontalStrut(10));
		
		endDatePanel = new JPanel();
		endDatePanel.setLayout(new BoxLayout(endDatePanel, BoxLayout.LINE_AXIS));
		JLabel endSpinnerDateLabel = new JLabel(CalendarSetting.calTranslate("date"));
		endSpinnerDateLabel.setLabelFor(endDateSpinner);
		endDatePanel.add(endSpinnerDateLabel);
		endDatePanel.add(Box.createHorizontalStrut(5));
		// date end spinner
		SpinnerDateModel dateModelEnd = new SpinnerDateModel(
				java.util.Date.from( defaultEndDateTime
						.atZone(ZoneId.systemDefault()).toInstant()),	 
				null, null, Calendar.DATE);
		
		endDateSpinner = new JSpinner(dateModelEnd);			
		endDateSpinner.setEditor(new JSpinner.DateEditor(endDateSpinner, dateModel.toPattern()));
		endDateSpinner.setMaximumSize(new Dimension(150, 25));
		endDateSpinner.setMinimumSize(new Dimension(150, 25));
		
		// add a date-picker button
		JButton endDatePickerButton = new JButton(IconManager.loadScaledIcon("date-picker.png", 
				"Date Picker to select a date", 30, 25,"Date Picker", "Date Picker to select a date"));
		endDatePickerButton.setBorder(BorderFactory.createEmptyBorder());
		endDatePickerButton.addActionListener(this);
		endDatePickerButton.setActionCommand("datePickerEnd");
		endDatePickerButton.addFocusListener(BorderButtonFocusListener.getInstance());

		endDatePanel.add(endDateSpinner);
		endDatePanel.add(endDatePickerButton);


		endTimeAndDatePanel.add(endDatePanel);
		if (existingEvent != null && existingEvent.isRecurring() == true) {
			endDatePanel.setVisible(false);
		}
		endTimeAndDatePanel.add(Box.createHorizontalGlue());

		// time end spinner		
		endTimePicker = new TimePicker( defaultEndDateTime.toLocalTime());
		if (allDay == false) {
			endTimeAndDatePanel.add(endTimePicker);
		}

		timeAndDatePanel.add(endTimeAndDatePanel);
		timeAndDatePanel.add(Box.createVerticalStrut(10));

		contentPaneMain.add(timeAndDatePanel);

		
		JComponent[] comps = createSummaryDescriptionPanels();		
		contentPaneMain.add(comps[0]);
		if (comps[1] instanceof JTextField) {
			summaryField = (JTextField) comps[1];
		}
		contentPaneMain.add(Box.createVerticalStrut(10));
		contentPaneMain.add(comps[1]);
		contentPaneMain.add(comps[2]);
		if (comps[3] instanceof JTextArea) {
			descriptionArea = (JTextArea) comps[3];
		}
		contentPaneMain.add(comps[4]);
		
		contentPaneMain.add(Box.createVerticalStrut(10));
				
		JPanel messageLabelPanel = new JPanel();
		messageLabelPanel.setLayout(new BoxLayout(messageLabelPanel, BoxLayout.LINE_AXIS));
		messageLabelPanel.add(mainMessageLabel);
		contentPaneMain.add(messageLabelPanel);		

		contentPaneMain.add(createSetButtonPanel(okButtonText));		
		
		// ===================== RecurrencePanel =======================================
		
		JPanel recPanel = new JPanel();
		recPanel.setLayout(new BoxLayout(recPanel, BoxLayout.PAGE_AXIS));
		recPanel.setBorder(new EmptyBorder(10, 10, 10, 10));

		// display the panel with recurrence rule:
		rp = new RecurrencePanel(existingEvent, defaultStartDateTime.toLocalDate(), this);
		recPanel.add(rp);
		recPanel.add(Box.createVerticalStrut(50));
		
		JPanel recurrenceLabelPanel = new JPanel();
		recurrenceLabelPanel.setLayout(new BoxLayout(recurrenceLabelPanel, BoxLayout.LINE_AXIS));
		recurrenceMessageLabel = new JLabel();
		recurrenceLabelPanel.add(recurrenceMessageLabel);
		recPanel.add(recurrenceLabelPanel);
		
		recPanel.add(createSetButtonPanel(okButtonText));

		scroll = new JScrollPane(contentPaneMain);	
		JScrollPane scrollRecurrence = new JScrollPane(recPanel);

		
		//============================= MORE OPTIONS TAB

		JPanel morePanel = new JPanel();
		morePanel.setLayout(new BoxLayout(morePanel, BoxLayout.PAGE_AXIS));
		morePanel.setBorder(new EmptyBorder(10, 10, 10, 10));
		
		JPanel moreLabelPanel = new JPanel();
		moreLabelPanel.setLayout(new BoxLayout(moreLabelPanel, BoxLayout.LINE_AXIS));
		JLabel moreLabel = new JLabel(CalendarSetting.calTranslate("more_options"));
		Font f = moreLabel.getFont();
		moreLabel.setFont(new Font(f.getFamily(), Font.BOLD, f.getSize()));
		moreLabelPanel.add(moreLabel);
		moreLabelPanel.add(Box.createHorizontalGlue());
		morePanel.add(Box.createVerticalStrut(10));
		morePanel.add(moreLabelPanel);
		morePanel.add(Box.createVerticalStrut(30));
		
		
		
		JComponent[] locUrlPriorityomps = createLocationUrlPriority();
		morePanel.add(locUrlPriorityomps[0]); // panel with location label
		if (locUrlPriorityomps[1] instanceof JTextField){
			locationField = (JTextField) locUrlPriorityomps[1];
			morePanel.add(locUrlPriorityomps[1]);
		}
		morePanel.add(Box.createVerticalStrut(30));
		morePanel.add(locUrlPriorityomps[2]); // panel with url label
		if (locUrlPriorityomps[3] instanceof JTextField) {
			urlField = (JTextField) locUrlPriorityomps[3];
			urlField.getDocument().addDocumentListener(this); // check valid URL
			morePanel.add(urlField);
		}
		morePanel.add(Box.createVerticalStrut(10));
		if (locUrlPriorityomps[4] instanceof JComboBox) {
			priorityComboBox = (JComboBox<String>) locUrlPriorityomps[4];			
		}
		morePanel.add(locUrlPriorityomps[5]);		
		
		morePanel.add(Box.createVerticalStrut(10));		
		
		// STATUS ======
		JPanel statusPanel = new JPanel();
		statusPanel.setLayout(new BoxLayout(statusPanel, BoxLayout.X_AXIS));
		JLabel statusLabel = new JLabel("status");
		statusLabel.setLabelFor(statusComboBox);
		statusPanel.add(Box.createHorizontalStrut(10));
		statusPanel.add(statusLabel);
		statusPanel.add(Box.createHorizontalStrut(20));
		
		statusComboBox = new JComboBox <String>();
		statusComboBox.addItem("no");
		statusComboBox.addItem(CalendarSetting.calTranslate("tentative"));
		statusComboBox.addItem(CalendarSetting.calTranslate("confirmed"));
		statusComboBox.addItem(CalendarSetting.calTranslate("cancelled"));
		statusComboBox.setMaximumSize(new Dimension(100, 25)); 
		if (existingEvent != null && existingEvent.getStatus() != null) {
			String currentStatus = existingEvent.getStatus();
			int index = 0;
			if (currentStatus.equals("TENTATIVE")){
				index = 1;
			} else if (currentStatus.equals("CONFIRMED")){
				index = 2;
			} else if (currentStatus.equals("CANCELLED")){
				index = 3;
			}  else {
				index = 0;
				//new UnexpectedValueException("currentStatus", "String", " is unknown").printStackTraceLines();
			}
			statusComboBox.setSelectedIndex(index);
		} 
		statusPanel.add(statusComboBox);
		statusPanel.add(Box.createHorizontalGlue());
		morePanel.add(statusPanel);		
		morePanel.add(Box.createVerticalStrut(20));	

		moreMessageLabel = new JLabel();
		moreMessageLabel.setForeground(Color.RED);
		JPanel moreMessageLabelPanel = new JPanel();
		moreMessageLabelPanel.setLayout(new BoxLayout(moreMessageLabelPanel, BoxLayout.LINE_AXIS));
		moreMessageLabelPanel.add(moreMessageLabel);
		morePanel.add(moreMessageLabelPanel);

		morePanel.add(createSetButtonPanel(okButtonText));
		
		JScrollPane scrollMore = new JScrollPane(morePanel);		
		
		// VALARM
		alarmMessageLabel = new JLabel();
		JPanel alarmPanel = createAlarmPanel(okButtonText, alarmMessageLabel);
		
		JScrollPane scrollAlarm = new JScrollPane(alarmPanel);	

		//====================== TABS: 
		
		tabbedPane = new JTabbedPane();
		tabbedPane.addTab(CalendarSetting.calTranslate("main"), null, scroll,
				CalendarSetting.calTranslate("main_settings"));
		tabbedPane.addTab(CalendarSetting.calTranslate("more"), null, scrollMore,
				CalendarSetting.calTranslate("more_options"));
		tabbedPane.addTab(CalendarSetting.calTranslate("repeat"), null, scrollRecurrence,
                CalendarSetting.calTranslate("recurrence"));//"Recurrence settings"); 
		tabbedPane.addTab(CalendarSetting.calTranslate("alarm"), null, scrollAlarm,
				CalendarSetting.calTranslate("set_alarm"));
		
		// Set mnemonics depending on translations:
		char mnemo1 = Character.toUpperCase(CalendarSetting.calTranslate("main").charAt(0));
		char mnemo2 = '\0';
		char mnemo3 = '\0';
		char mnemo4 = '\0';
		String moreText = CalendarSetting.calTranslate("more");
		int moreLen = moreText.length();
		for (int i = 0; i < moreLen; i++) {
			mnemo2 = Character.toUpperCase(moreText.charAt(i));
			if (mnemo2 != mnemo1) {
				break;
			} 
		}
		String repeatText = CalendarSetting.calTranslate("repeat");
		int repeatLen = repeatText.length();
		for (int i = 0; i < repeatLen; i++) {
			mnemo3 = Character.toUpperCase(repeatText.charAt(i));
			if (mnemo3 != mnemo1) {
				if ( mnemo2 != '\0' && mnemo3 != mnemo2) {
					break;
				}				
			}
		}
		
		String alarmText = CalendarSetting.calTranslate("alarm");
		int alarmLen = alarmText.length();
		for (int i = 0; i < alarmLen; i++) {
			mnemo4 = Character.toUpperCase(alarmText.charAt(i));
			if (mnemo4 != mnemo3 && mnemo4 != mnemo2 && mnemo4 != mnemo1) {
				break;			
			}
		}
		
		tabbedPane.setMnemonicAt(0, mnemo1);   //KeyEvent.VK_3);
		if (mnemo2 != '\0') {
			tabbedPane.setMnemonicAt(1, mnemo2);
		}
		if (mnemo3 != '\0') {
			tabbedPane.setMnemonicAt(2, mnemo3);
		}
		if (mnemo4 != '\0') {
			tabbedPane.setMnemonicAt(3, mnemo4);
		}
		
		this.getContentPane().add(tabbedPane);	

		//this.pack();
		this.setMinimumSize(new Dimension(570,470));
		//if (location == null) {
		this.setLocationRelativeTo(owner);
		this.setVisible(true);
	}

	@Override
	public void pack() {
		super.pack();
		
		// set scroll pane if larger than screen:
		Dimension prefSize = this.getPreferredSize();
		if (prefSize.width > PswDialogView.getScreenwidth()){
			scroll.setPreferredSize(new Dimension ((int)PswDialogView.getScreenwidth(), (int) prefSize.getHeight()));
		}
		if (prefSize.height > PswDialogView.getScreenheight()) {
			scroll.setPreferredSize(new Dimension ((int) prefSize.getWidth(), (int)PswDialogView.getScreenheight()));
		}		
	}
	
	@Override
	public void stateChanged(ChangeEvent e) {
		if (e.getSource() instanceof JSpinner) {
			JSpinner spinner = (JSpinner) e.getSource();
			if (spinner.getName().equals("startTimeSpinner")) {
				// set end time only if start date is equal or less than end date
				if ((((java.util.Date)startDateSpinner.getValue()).getTime() + (1000*60*60)) < (((java.util.Date)endDateSpinner.getValue()).getTime() )){
					return;
				} else {
					endTimePicker.setTime(startTimePicker.getTime().plusHours(1));
				}
			} else if (spinner.getName().equals("startDateSpinner")) {

				if (isRecurring == true) { //recurringCheck.isSelected() == true) {

					// step 1 a: set date of DTEND to DTSTART if required:
					if (! ((java.util.Date)startDateSpinner.getValue()).equals((java.util.Date)endDateSpinner.getValue())) {
						endDateSpinner.setValue(startDateSpinner.getValue());
					}
					// set 1 b: set time if end is after start:
					if ((((java.util.Date)startDateSpinner.getValue()).getTime() + (1000*60*60)) < (((java.util.Date)endDateSpinner.getValue()).getTime() )){
						// ok
					} else {
						endTimePicker.setTime(startTimePicker.getTime().plusHours(1));
					}

					// step 2: set & reset BYDAY values:
					if (rp != null ) {
						//RecurrencePanel rp = (RecurrencePanel) recPanel.getComponent(0);
						// TODO		
						rp.updateStartDate( ( (java.util.Date) startDateSpinner.getValue())
								.toInstant().atZone(ZoneId.systemDefault()).toLocalDate());
					}

				} else {
					// reset date end if required
					if ( ! ((java.util.Date)startDateSpinner.getValue()).before((java.util.Date)endDateSpinner.getValue())) {
						endDateSpinner.setValue(startDateSpinner.getValue());
						// check if end time is after start time
						if ((((java.util.Date)startDateSpinner.getValue()).getTime() + (1000*60*60)) < (((java.util.Date)endDateSpinner.getValue()).getTime() )){
							// ok
						} else {
							// set one hour later
							endTimePicker.setTime(startTimePicker.getTime().plusHours(1));
						}

					}
				}
			}
		}
	}
	
	/**
	 * Get the current start date of the event to set
	 * 
	 * @return	the current start date of the event
	 */
	public LocalDate getStartDate() {
		return ((java.util.Date) startDateSpinner.getValue())
			.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
	}
	
	/**
	 * Disable or enable the startDateSpinner. The value
	 * can't be changed if the spinner is disabled. 
	 * This is enabled, if a BY_DAY rule is defined, 
	 * because the start day must be included in the rule
	 * 
	 * @param enable	false: start date is not modifiable
	 * 					true: start date can be changed
	 */
	public void setStartDateSpinner(boolean enable) {
		startDateSpinner.setEnabled(enable);
		if (enable == false) {
			JSpinner.DateEditor editor = (DateEditor) startDateSpinner.getEditor();
			editor.getTextField().setDisabledTextColor(Color.BLACK);
		} 
	}
	
	/**
	 * Set if the current event is recurring.
	 * This also changes the display.
	 * 
	 * @param _recurring
	 */
	protected void setRecurring(boolean _recurring) {
		boolean originalRecurring = this.isRecurring;
		this.isRecurring = _recurring;
		if (_recurring == true) {
			if (existingEvent != null) {
				existingEvent.setRecurring(true);
			}
			if (endDatePanel != null) {
				endDatePanel.setVisible(false);
			}
		} else {
			if (existingEvent != null) {
				existingEvent.setRecurring(false);
			}
			if (endDatePanel != null) {
				endDatePanel.setVisible(true);
			}
		}
		if (originalRecurring != _recurring) {
			this.pack();
		}
	}

	@Override
	public void changedUpdate(DocumentEvent de) {}
	
	@Override
	public void insertUpdate(DocumentEvent iu) {
		//TestLog.o(EventSetter.class, "insert");
		Document doc = iu.getDocument();
		if (urlField.getDocument().equals(doc)) {			
			//URL validUrl = null;
			try {
				String url = doc.getText(0, doc.getLength());
				new URL(url);
				urlField.setBorder(new LineBorder(Color.DARK_GRAY, 1));
			} catch (Exception e) {
				urlField.setBorder(new LineBorder(Color.RED, 1));
			}
		}
	}
	
	@Override
	public void removeUpdate(DocumentEvent ru) {
		//TestLog.o(EventSetter.class, "remove");
		Document doc = ru.getDocument();
		if (urlField.getDocument().equals(doc)) {			
			//URL validUrl = null;
			try {
				String url = doc.getText(0, doc.getLength());
				new URL(url);
				urlField.setBorder(new LineBorder(Color.DARK_GRAY, 1));
			} catch (Exception e) {
				urlField.setBorder(new LineBorder(Color.RED, 1));
			}
		}
	}

	@Override
	protected String getSummary() {
		return summaryField.getText();
	}

	@Override
	protected String getDescription() {
		return descriptionArea.getText();
	}

	@Override
	protected String getCalComponentLocation() {
		return locationField.getText();
	}

	@Override
	protected String getUrl() {
		return urlField.getText();
	}

	@Override
	protected String getSelectedCalendar() {
		if (calendarComboBox == null || calendarComboBox.getSelectedItem() == null) {
			TestLog.ve(EventSetter.class, "Missing value", 5);
			return null;
		}
		return (String) calendarComboBox.getSelectedItem();
	}

	@Override
	protected int getPriority() {
		return priorityComboBox.getSelectedIndex();
	}

	@Override
	protected String getStatus() {
		int status = statusComboBox.getSelectedIndex();
		if (status >= 0) {
			if (status == 0) {
				return null;
			} else if (status == 1) {
				return "TENTATIVE";
			} else if (status == 2) {
				return "CONFIRMED";
			} else if (status == 3) {
				return "CANCELLED";
			}
		} 
		return null;
	}

	@Override
	protected boolean isAllDay() {
		if (allDayCheck != null && allDayCheck.isSelected()) {
			return true;
		} else {
			return false;
		}
	}

	@Override
	protected boolean isRecurring() {
		return isRecurring;
	}

	@Override
	protected void setAllDay() {
		// add or remove time spinners
		if (allDayCheck.isSelected()) {
			//setAllDay(true);
			//((PEvent)event).setAllDay(true);
			startTimeAndDatePanel.remove(startTimePicker);
			endTimeAndDatePanel.remove(endTimePicker);	
			this.pack();
		} else {
			//setAllDay(true);
			//((PEvent)event).setAllDay(false);
			startTimeAndDatePanel.add(startTimePicker);
			endTimeAndDatePanel.add(endTimePicker);		
			this.pack();
		}		
	}

	@Override
	protected void setMainMessage(String message, boolean error) {
		if (mainMessageLabel != null) {
			mainMessageLabel.setText(message);
			if (error == true) {
				mainMessageLabel.setForeground(Color.RED);
			} else {
				mainMessageLabel.setForeground(Color.BLACK);
			}
		} else {
			TestLog.ve(EventSetter.class, "Missing message label...", 5);
		}
	}

	@Override
	protected void setMoreMessage(String message, boolean error) {
		if (moreMessageLabel != null) {
			moreMessageLabel.setText(message);
			if (error == true) {
				moreMessageLabel.setForeground(Color.RED);
			} else {
				moreMessageLabel.setForeground(Color.BLACK);
			}
		} else {
			TestLog.ve(EventSetter.class, "Missing message label...", 5);
		}
	}
	
	@Override
	protected void setAlarmMessage(String message, boolean error) {
		if (alarmMessageLabel != null) {
			alarmMessageLabel.setText(message);
			if (error == true) {
				alarmMessageLabel.setForeground(Color.RED);
			} else {
				alarmMessageLabel.setForeground(Color.BLACK);
			}
		} else {
			TestLog.ve(EventSetter.class, "Missing message label...", 5);
		}
	}

	@Override
	protected void setRecurrenceMessage(String message, boolean error) {
		if (recurrenceMessageLabel != null) {
			recurrenceMessageLabel.setText(message);
			if (error == true) {
				recurrenceMessageLabel.setForeground(Color.RED);
			} else {
				recurrenceMessageLabel.setForeground(Color.BLACK);
			}
		} else {
			TestLog.ve(EventSetter.class, "Missing message label...", 5);
		}
	}

	@Override
	protected void setComponentRecurring(boolean _recurring) {
		setRecurring(_recurring);
	}

	@Override
	protected LocalDateTime getStartDateTime() {

		if (startDateSpinner != null) {
			LocalDateTime startDateTime;
			LocalDate startDate = ((SpinnerDateModel)startDateSpinner.getModel()).getDate().toInstant()
					.atZone(ZoneId.systemDefault())
					.toLocalDate();
			if (allDayCheck != null && allDayCheck.isSelected()) {//if (startTimePicker.isShowing() == false) {x
				if (startDate != null) {
					startDateTime = startDate.atStartOfDay();// hours, minutes 00:00
				} else {
					TestLog.ve(EventSetter.class, "Missing start date...", 5);
					return null;
				}
			} else {
				if (startTimePicker != null && startTimePicker.getTime() != null) {
					LocalTime startTime = startTimePicker.getTime();
					startDateTime = LocalDateTime.of(startDate, startTime);
				} else {
					TestLog.ve(EventSetter.class, "Missing start time picker vale", 5);
					startDateTime = startDate.atStartOfDay();// hours, minutes 00:00
				}
			}
			return startDateTime;
		} else {
			TestLog.ve(EventSetter.class, "Can't set start date time", 5);
			return null;
		}
	}

	@Override
	protected LocalDateTime getEndDateTime() {
		LocalDateTime endDateTime;
		if (endDateSpinner != null && endDateSpinner.isEnabled()) {
			LocalDate endDate = ((SpinnerDateModel) endDateSpinner.getModel()).getDate().toInstant()
					.atZone(ZoneId.systemDefault())
					.toLocalDate();
			if (endTimePicker != null && allDayCheck != null && allDayCheck.isSelected() == false) {
				LocalTime endTime = endTimePicker.getTime();
				endDateTime = LocalDateTime.of(endDate, endTime);
			} else {
				endDateTime = endDate.atStartOfDay();// hours, minutes 00:00
			}
			return endDateTime;
		} else {
			TestLog.ve(EventSetter.class, "Missing endDateSpinner");
			return null;
		}
	}

	@Override
	protected void setStartDateTime(LocalDateTime _startDateTime) {
		if (_startDateTime == null) {
			if (startDateSpinner.isEnabled() == false) {
				return;
			}
			// get current set date and convert to LocalDate
			LocalDate startDate = ((java.util.Date) startDateSpinner.getValue())
					.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
			//TestLog.o(EventSetter.class, "startDate: " + startDate);

			// show the DatePicker with local date to start
			LocalDate date = new DatePicker(startDate, true).showDatePicker(this, null);
			// set new until date from DatePicker (convert to Date)
			startDateSpinner.setValue(java.util.Date.from(date.atStartOfDay(ZoneId.systemDefault()).toInstant()));			
		} else {
			TestLog.ve(EventSetter.class, "Unexpected value", 5);
		}
	}

	@Override
	protected void setEndDateTime(LocalDateTime _endDateTime) {
		if (_endDateTime == null) {
			// get current set date and convert to LocalDate
			LocalDate endDate = ((java.util.Date) endDateSpinner.getValue())
					.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
			//TestLog.o(EventSetter.class, "startDate: " + startDate);
			
			// show the DatePicker with local date to start
			LocalDate date = new DatePicker(endDate, true).showDatePicker(this, null);
			// set new until date from DatePicker (convert to Date)
			endDateSpinner.setValue(java.util.Date.from(date.atStartOfDay(ZoneId.systemDefault()).toInstant()));						
		} else {
			TestLog.ve(EventSetter.class, "Unexpected value", 5);
		}		
	}

	@Override
	protected CalendarComponent setRecurrence(CalendarComponent event) {
		if (rp == null ) {
			TestLog.ve(EventSetter.class, "Can't find recurrence panel...", 5);
			return event;
		}
		if (event == null) {
			TestLog.ve(EventSetter.class, "Missing event...", 5);
			return event;
		}
		// An event is recurring if frequency and single exceptions are not null:
		if (rp.getRDates() == null && rp.getFrequency() == null) { // not recurring
			if (event instanceof RecurringPEvent) {
				event = RecurringPEvent.resetRecurrenceProperties((RecurringPEvent) event);
			}
			event.setRecurring(false);
			return event;
		}

		// recurring event:				
		RecurringPEvent rEvent = RecurringPEvent.castToRecurringPEvent((PEvent) event);		

		// set RDATE independent of rule
		if (rp.getRDates() != null) {
			TestLog.v(EventSetter.class, "set  rdates");
			LocalDate[] rDates = rp.getRDates();
			int rDatesLen = rDates.length;
			//LocalDate startDate = rEvent.getDateStart().toLocalDate();
			// check if a date is before start date:
			for (int i=0; i < rDatesLen; i++) {
				if (rDates[i].isBefore(getStartDateTime().toLocalDate())) {
					setErrorMessageToAllTabs( CalendarSetting.calTranslate("single_repetitions") + 
							CalendarSetting.calTranslate("is_before_start_date") + ": " + rDates[i], true);
					this.pack();
					return event;
				}
			}
			rEvent.setRecurrenceDates(rp.getRDates());
		}
		ChronoUnit frequency = rp.getFrequency();
		if (frequency == null && rEvent.getRecurrenceDates() == null) {
			setErrorMessageToAllTabs("Couldn't set recurrence: frequency missed", true);
			TestLog.v(EventSetter.class, "No recurrence frequency or dates", 5);
			this.pack();
			event.setRecurring(false);
		} else {
			rEvent.setFrequency(frequency);
			rEvent.setInterval(rp.getInterval());
			rEvent.setByMonth(rp.getByMonth());
			//String bd = ""; for (String s : rp.getByDayStrings()) bd += s;
			//TestLog.e(EventSetter.class, "byDay: " +bd);
			rEvent.setByDay(rp.getByDayStrings());		
			int count = rp.getCount();
			LocalDateTime until = rp.getUntil();
			if (until != null && ! until.isAfter(rEvent.getDateStart())) {
				setErrorMessageToAllTabs( CalendarSetting.calTranslate("until_date") + " - " + 
						CalendarSetting.calTranslate("is_before_start_date") + ": " + until.toLocalDate(), true);						
				this.pack();
				return event;
			}
			rEvent.setCount(count);	
			rEvent.setUntil(until);		
			rEvent.setExceptionDates(rp.getExceptions());
			event = rEvent;
		}		
		if ( rp.getFrequency() == null && rp.getRDates() == null){
			TestLog.ve(EventSetter.class, "No recurrence frequency or dates", 5);
			if (event instanceof RecurringPEvent) {
				TestLog.ve(EventSetter.class, "No recurrence frequency or dates, Reset recurrence");
				event = RecurringPEvent.resetRecurrenceProperties((RecurringPEvent) event);
			}
			event.setRecurring(false);
		}
		return event;
	}

	@Override
	protected void showHideDue() {
		// not implemented
	}

	@Override
	protected void showHideStart() {
		// not implemented
	}
}
