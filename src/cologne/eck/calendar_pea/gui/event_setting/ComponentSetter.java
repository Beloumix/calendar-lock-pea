package cologne.eck.calendar_pea.gui.event_setting;

/*
 * PeaFactory - Production of Password Encryption Archives
 * Copyright (C) 2021  Axel von dem Bruch
 * 
 * This library is free software; you can redistribute it and/or modify it 
 * under the terms of the GNU General Public License as published 
 * by the Free Software Foundation; either version 2 of the License, 
 * or (at your option) any later version.
 * This library is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
 * or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU General Public License for more details.
 * See:  http://www.gnu.org/licenses/gpl-2.0.html
 * You should have received a copy of the GNU General Public License 
 * along with this library.
 */


import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.KeyboardFocusManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.net.URL;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JCheckBox;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSpinner;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.Timer;
import javax.swing.border.EmptyBorder;
import javax.swing.SpinnerModel;
import javax.swing.SpinnerNumberModel;

import cologne.eck.all_peas.control.PeaControl;
import cologne.eck.all_peas.data.PeaProperties;
import cologne.eck.all_peas.gui.PeaDialog;
import cologne.eck.all_peas.gui.PeaFileChooser;
import cologne.eck.all_peas.vm_specific.JREProperties;
import cologne.eck.calendar_pea.control.AlarmControl;
import cologne.eck.calendar_pea.control.CalendarControl;
import cologne.eck.calendar_pea.control.CalendarSetting;
import cologne.eck.calendar_pea.data.CalendarComponent;
import cologne.eck.calendar_pea.data.PAlarm;
import cologne.eck.calendar_pea.data.PCalendar;
import cologne.eck.calendar_pea.data.PEvent;
import cologne.eck.calendar_pea.data.PTodo;
import cologne.eck.calendar_pea.data.RecurringPEvent;
import cologne.eck.calendar_pea.gui.CalendarComboBox;
import cologne.eck.calendar_pea.gui.CalendarMenuBar;
import cologne.eck.calendar_pea.gui.LockFrameCalendar;
import cologne.eck.calendar_pea.util.WriteICal;
import cologne.eck.tools.FileTools;
import cologne.eck.tools.TestLog;
import cologne.eck.tools.WriteResources;


@SuppressWarnings("serial")
abstract class ComponentSetter extends JDialog 
	implements ActionListener {
	
		//////////// Values that are set in the child classes (EventSetter/TodoSetter)///////////////
		/*
		 * The todo or component from setter if it already exists or null for new event. 
		 * This value was always set in the child class (EventSetter/TodoSetter)
		 */
		protected CalendarComponent existingCalComponent = null;
		/*
		 * True if the is a new todo/event to be created, false for existing todo/event.
		 * This value was always set in the child class (EventSetter/TodoSetter)
		 */
		protected boolean newCalComponent = true;
		/*
		 * The class of the setter: EventSetter or TodoSetter. 
		 * This value was always set in the child class (EventSetter/TodoSetter)
		 */
		protected String calendarComponentClass = null;		
		
		private JPanel alarmPanel;
		private JCheckBox triggerCheck;
		private JSpinner weekSpinner;
		private JSpinner daySpinner;
		private JSpinner hourSpinner;
		private JSpinner minuteSpinner;
		//JComboBox<String> actionBox;
		private JCheckBox audioCheck;
		private JCheckBox displayCheck;
		//private JCheckBox emailCheck;
		private JTextField descriptionField;
		
		private boolean isAlarm() {
			if (triggerCheck != null) {
				return triggerCheck.isSelected();
			} else {
				return false;
			}
		}
		private int getAlarmWeeks() {
			if (weekSpinner != null) {
				return (int) weekSpinner.getModel().getValue();
			} else { 
				return 0;			
			}
		}
		private int getAlarmDays() {
			if (daySpinner != null) {
				return (int) daySpinner.getModel().getValue();
			} else { 
				return 0;			
			}
		}
		private int getAlarmHours() {
			if (hourSpinner != null) {
				return (int) hourSpinner.getModel().getValue();
			} else { 
				return 0;			
			}
		}
		private int getAlarmMinutes() {
			if (minuteSpinner != null) {
				return (int) minuteSpinner.getModel().getValue();
			} else { 
				return 0;			
			}
		}
	/*	private String getAlarmAction() {
			if (actionBox != null) {
				return (String) actionBox.getSelectedItem();
			} else { 
				return null;			
			}
		}*/
/*		private boolean isAudioAction() {
			if (audioCheck != null) {
				return audioCheck.isSelected();
			} else {
				return false;
			}
		}
		private boolean isDisplayAction() {
			if (audioCheck != null) {
				return audioCheck.isSelected();
			} else {
				return false;
			}
		}*/
		private String getAlarmDescription() {
			if (descriptionField != null) {
				return descriptionField.getText();
			} else { 
				return null;			
			}
		}
	
		
		protected abstract String getSummary();
		protected abstract String getDescription();
		protected abstract String getCalComponentLocation();
		protected abstract String getUrl();
		protected abstract String getSelectedCalendar();
		protected abstract int getPriority();
		protected abstract String getStatus();
		protected abstract boolean isAllDay();
		protected abstract boolean isRecurring();
		protected abstract void setAllDay();
		protected abstract void setRecurring(boolean _recurring);
		protected abstract void showHideDue();
		protected abstract void showHideStart();
		
		protected abstract void setMainMessage(String message, boolean error);
		protected abstract void setMoreMessage(String message, boolean error);
		protected abstract void setAlarmMessage(String message, boolean error);
		protected abstract void setRecurrenceMessage(String message, boolean error);
		protected abstract void setComponentRecurring(boolean _recurring);
		
		protected abstract LocalDateTime getStartDateTime();
		protected abstract LocalDateTime getEndDateTime();
		protected abstract void setStartDateTime(LocalDateTime _startDateTime);
		protected abstract void setEndDateTime(LocalDateTime _endDateTime);
		/**
		 * Set recurrence for event if there is a recurrence defined.
		 * 
		 * @param c	the event/todo
		 * 
		 * @return	the event/todo with recurrence or the unchanged event
		 */
		protected abstract CalendarComponent setRecurrence(CalendarComponent c);
				
		/**
		 * Create a panel with a ContactComboBox, register Action...
		 * 
		 * @return	array of components: [0] is calendarComboBox, 
		 * 		[1] is panel that contains calendarComboBox
		 */
		protected JComponent[] createCalendarComboBox() {
			// ComboBox to select the PCard for this Event
			JPanel calendarPanel = new JPanel();
			calendarPanel.setLayout(new BoxLayout(calendarPanel, BoxLayout.X_AXIS));
			JLabel calendarLabel = new JLabel(CalendarSetting.calTranslate("calendar"));
			calendarPanel.add(calendarLabel);
			calendarPanel.add(Box.createHorizontalStrut(20));

			CalendarComboBox calendarComboBox = new CalendarComboBox();
			if (calendarComboBox.getItemCount() <= 0) {
				// no calendars
				return null;
			}
			if (newCalComponent == false) { // set the calendar
				PCalendar c = existingCalComponent.getCalendar();
				String cName = c.getName();
				calendarComboBox.setSelectedCalendar(cName);
			}
			// to check if calendar was changed
			calendarComboBox.addActionListener(this);
			calendarComboBox.setActionCommand("calendarChange");
			calendarPanel.add(calendarComboBox);
			calendarPanel.add(Box.createHorizontalGlue());
			//contentPaneMain.add(calendarPanel);
			JComponent[] comp = new JComponent[2];
			comp[0] = calendarComboBox;
			comp[1] = calendarPanel;
			return comp;
		}
		
		/**
		 * Create the components for summary and description
		 * 
		 * @return an array of components: [0] is summayLabelPanel, 
		 * 	[1] is summaryField, [2] is descriptionLabelPanel, 
		 *  [3] is descriptionArea, [4] is descriptionScroll
		 */
		protected JComponent[] createSummaryDescriptionPanels(){
			JComponent[] comps = new JComponent[6];
			//===============
			// SUMMARY ======
			JPanel summaryLabelPanel = new JPanel();		
			summaryLabelPanel.setLayout(new BoxLayout(summaryLabelPanel, BoxLayout.LINE_AXIS));
			JLabel summaryLabel = new JLabel(CalendarSetting.calTranslate("summary"));
			summaryLabelPanel.add(summaryLabel);
			summaryLabelPanel.add(Box.createHorizontalGlue());	
			comps[0] = summaryLabelPanel;
			
			JTextField summaryField = new JTextField();//50);
			summaryLabel.setLabelFor(summaryField);
			if (existingCalComponent != null && existingCalComponent.getSummary() != null) {
				summaryField.setText(existingCalComponent.getSummary());
			}

			summaryField.setPreferredSize(new Dimension(200, (int)summaryField.getPreferredSize().getHeight()));
			summaryField.setMaximumSize(new Dimension(800, 50));

			comps[1] = summaryField;
			
			//===================
			// DESCRIPTION ======
			JPanel descriptionLabelPanel = new JPanel();
			descriptionLabelPanel.setLayout(new BoxLayout(descriptionLabelPanel, BoxLayout.LINE_AXIS));
			JLabel descriptionLabel = new JLabel(CalendarSetting.calTranslate("description"));
			descriptionLabelPanel.add(descriptionLabel);
			descriptionLabelPanel.add(Box.createHorizontalGlue());
			comps[2] = descriptionLabelPanel;
			
			JTextArea descriptionArea = new JTextArea();
			descriptionLabel.setLabelFor(descriptionArea);
			if (existingCalComponent != null && existingCalComponent.getDescription() != null) {
				descriptionArea.setText(existingCalComponent.getDescription());
			}
			descriptionArea.setLineWrap(true);
			descriptionArea.setWrapStyleWord(true);
			// change behavior of tab key: from TAB to focus for accessibility
			descriptionArea.setFocusTraversalKeys(KeyboardFocusManager.FORWARD_TRAVERSAL_KEYS, null);
			descriptionArea.setFocusTraversalKeys(KeyboardFocusManager.BACKWARD_TRAVERSAL_KEYS, null);
			JScrollPane descriptionScroll = new JScrollPane(descriptionArea);
			descriptionScroll.setPreferredSize(new Dimension(400, 60));
			comps[3] = descriptionArea;
			comps[4] = descriptionScroll;
			return comps;
		}
		
		/**
		 * Create a panel with a set button and - if the event/todo
		 * already exist - a remove button
		 * 
		 * @param okButtonText	text for ok button (set or create)
		 * 
		 * @return	the panel with buttons
		 */
		protected JPanel createSetButtonPanel(String okButtonText) {
			JPanel okButtonPanel = new JPanel();
			okButtonPanel.setLayout(new BoxLayout(okButtonPanel, BoxLayout.LINE_AXIS));		
			okButtonPanel.setMaximumSize(new Dimension(800, 40));
			if (newCalComponent == false) {
				JButton removeButton = new JButton(CalendarSetting.calTranslate("remove_event"));
				removeButton.addActionListener(this);
				removeButton.setActionCommand("remove");
				okButtonPanel.add(removeButton);
				
				JButton exportButton = new JButton(CalendarSetting.calTranslate("export"));
				exportButton.addActionListener(this);
				exportButton.setActionCommand("export");
				okButtonPanel.add(exportButton);
			}
			okButtonPanel.add(Box.createHorizontalGlue());

			JPanel setButtonPanel = new JPanel();
			JButton setButton = new JButton(okButtonText);
			setButton.addActionListener(this);
			setButton.setActionCommand("set");	
			setButtonPanel.add(setButton);
			//Color c = setButtonPanel.getBackground();		
			//setButtonPanel.setBorder(new LineBorder(c.brighter(), 5));
			setButton.setBackground(new Color(179, 255, 179));
			okButtonPanel.add(setButtonPanel);			
			return okButtonPanel;
		}
		
		/**
		 * Create a panel with location, URL,priority
		 * 
		 * @return an array of 6 JComponents: 
		 * [0] is locationLabelPanel,
		 * [1] is locationField, 
		 * [2] is urlLabelPanel, 
		 * [3] is urlField, 
		 * [4] is priorityComboBox, 
		 * [5] is priorityPanel
		 */
		protected JComponent[] createLocationUrlPriority() {
			JComponent[] comps = new JComponent[6];
			// LOCATION ======
			JPanel locationLabelPanel = new JPanel();		
			locationLabelPanel.setLayout(new BoxLayout(locationLabelPanel, BoxLayout.LINE_AXIS));
			JLabel locationLabel = new JLabel(CalendarSetting.calTranslate("location"));
			//summaryLabel.setMaximumSize(new Dimension(800, 30));
			locationLabelPanel.add(locationLabel);
			locationLabelPanel.add(Box.createHorizontalGlue());
			//morePanel.add(locationLabelPanel);
			comps[0] = locationLabelPanel;
			
			
			JTextField locationField = new JTextField();//50);
			locationLabel.setLabelFor(locationField);
			if (existingCalComponent != null && existingCalComponent.getLocation() != null) {
				locationField.setText(existingCalComponent.getLocation());
			}

			locationField.setPreferredSize(new Dimension(200, (int)locationField.getPreferredSize().getHeight()));
			locationField.setMaximumSize(new Dimension(800, 50));	
			comps[1] = locationField;
			
	/*		JPanel categoriesLabelPanel = new JPanel();
			categoriesLabelPanel.setLayout(new BoxLayout(categoriesLabelPanel, BoxLayout.X_AXIS));
			JLabel categoriesLabel = new JLabel("categories: "); // TODO language
			categoriesLabelPanel.add(categoriesLabel);
			categoriesLabelPanel.add(Box.createHorizontalGlue());
			morePanel.add(categoriesLabelPanel);
			
			categoriesField = new JTextField();
			if (existingEvent != null && existingEvent.getCategories() != null) {*/
			/*	String[] categories =   existingEvent.getCategories().split(",");
				int len = categories.length;
				StringBuilder builder = new StringBuilder();
				for (int i = 0; i < len; i++) {
					List<String> catList = categories[i].getValues();
					int listLen = catList.size();
					for (int j = 0; j < listLen; j++) {					
						builder.append(catList.get(j));
						builder.append(", ");
					}
				}
				// delete last ,
				builder.delete(builder.length() - 2, builder.length());
				categoriesField.setText(new String(builder));*/
	/*			categoriesField.setText(existingEvent.getCategories()); // TODO separate
			}
			categoriesField.setPreferredSize(new Dimension(200, (int)categoriesField.getPreferredSize().getHeight()));
			categoriesField.setMaximumSize(new Dimension(800, 50));
			morePanel.add(categoriesField);		
			morePanel.add(Box.createVerticalStrut(10));		*/
			
			/*		JPanel commentLabelPanel = new JPanel();
					commentLabelPanel.setLayout(new BoxLayout(commentLabelPanel, BoxLayout.X_AXIS));
					JLabel commentLabel = new JLabel("comment");
					commentLabelPanel.add(commentLabel);
					commentLabelPanel.add(Box.createHorizontalGlue());
					contentPane.add(commentLabelPanel);
					
					commentField = new JTextField();
					if (event.getComments().isEmpty() == false) {
						String[] comments = (String[]) event.getComments().toArray();
						int len = comments.length;
						StringBuilder builder = new StringBuilder();
						for (int i = 0; i < len; i++) {
							builder.append(comments[i]);
							builder.append(", ");
						}
						// delete last ,
						builder.delete(builder.length() - 2, builder.length());
						commentField.setText(new String(builder));
					}
					contentPane.add(commentField);		
					contentPane.add(Box.createVerticalStrut(10));		*/
					
			/*		JPanel resourcesLabelPanel = new JPanel();
					resourcesLabelPanel.setLayout(new BoxLayout(resourcesLabelPanel, BoxLayout.X_AXIS));
					JLabel resourcesLabel = new JLabel("resources");
					resourcesLabelPanel.add(resourcesLabel);
					resourcesLabelPanel.add(Box.createHorizontalGlue());
					contentPane.add(resourcesLabelPanel);
					
					resourcesField = new JTextField();
					if (event.getResources() != null) {
						Iterator <Resources> iterator = event.getResources().iterator();
						StringBuilder builder = new StringBuilder();
						while (iterator.hasNext()) {
							builder.append(iterator.next().toString());
							builder.append(", ");
						}
						// delete last ,
//						builder.delete(builder.length() - 2, builder.length());
						resourcesField.setText(new String(builder));
					}
					contentPane.add(resourcesField);		
					contentPane.add(Box.createVerticalStrut(10));*/	
			
			JPanel urlLabelPanel = new JPanel();
			urlLabelPanel.setLayout(new BoxLayout(urlLabelPanel, BoxLayout.X_AXIS));
			JLabel urlLabel = new JLabel(PeaProperties.getVmBridge().translate("web") + ": ");
			urlLabelPanel.add(urlLabel);
			urlLabelPanel.add(Box.createHorizontalGlue());
			JButton urlButton = new JButton(PeaProperties.getVmBridge().translate("go_to"));
			urlButton.addActionListener(this);
			urlButton.setActionCommand("openUrl");
			urlLabelPanel.add(urlButton);
			//morePanel.add(urlLabelPanel);
			comps[2] = urlLabelPanel;
			
			JTextField urlField = new JTextField();
			urlLabel.setLabelFor(urlField);
			//urlField.getDocument().addDocumentListener(this); // check valid URL
			if (existingCalComponent != null && existingCalComponent.getUrl() != null) {
				urlField.setText(existingCalComponent.getUrl().toString());
			}
			urlField.setPreferredSize(new Dimension(200, (int)urlField.getPreferredSize().getHeight()));
			urlField.setMaximumSize(new Dimension(800, 50));
			comps[3] = urlField;
			
			//================
			// PRIORITY ======
			JPanel priorityPanel = new JPanel();
			priorityPanel.setLayout(new BoxLayout(priorityPanel, BoxLayout.X_AXIS));
			JLabel priorityLabel = new JLabel(CalendarSetting.calTranslate("priority"));
			priorityPanel.add(Box.createHorizontalStrut(10));
			priorityPanel.add(priorityLabel);
			priorityPanel.add(Box.createHorizontalStrut(20));
			
			JComboBox<String> priorityComboBox = new JComboBox <String>();
			priorityLabel.setLabelFor(priorityComboBox);
			priorityComboBox.addItem("no");
			priorityComboBox.addItem("1");
			priorityComboBox.addItem("2");
			priorityComboBox.addItem("3");
			priorityComboBox.addItem("4");
			priorityComboBox.addItem("5");
			priorityComboBox.addItem("6");
			priorityComboBox.addItem("7");		
			priorityComboBox.addItem("8");
			priorityComboBox.addItem("9");
			priorityComboBox.setMaximumSize(new Dimension(50, 25));
			if (existingCalComponent != null && existingCalComponent.getPriority() > 0 && existingCalComponent.getPriority() < 10) {
				priorityComboBox.setSelectedIndex(existingCalComponent.getPriority());
			}
			priorityPanel.add(priorityComboBox);
			priorityPanel.add(Box.createHorizontalGlue());
			//morePanel.add(priorityPanel);
			comps[4] = priorityComboBox;
			comps[5] = priorityPanel;	
			
			return comps;
		}
		
		protected JPanel createAlarmPanel(String okButtonText, JLabel alarmMessageLabel) {
			alarmPanel = new JPanel();
			alarmPanel.setLayout(new BoxLayout(alarmPanel, BoxLayout.PAGE_AXIS));
			alarmPanel.setBorder(new EmptyBorder(10, 10, 10, 10));
			
			JPanel alarmCheckPanel = new JPanel();
			alarmCheckPanel.setLayout(new BoxLayout(alarmCheckPanel, BoxLayout.LINE_AXIS));
			triggerCheck = new JCheckBox();
			triggerCheck.addActionListener(this);
			triggerCheck.setActionCommand("setAlarm");
			Font f = triggerCheck.getFont();
			triggerCheck.setFont(new Font(f.getFamily(), Font.BOLD, f.getSize()));
			triggerCheck.setText(CalendarSetting.calTranslate("set_alarm"));			
			boolean existingAlarm = false;
			if (existingCalComponent != null && existingCalComponent.getAlarms() != null ) {
				triggerCheck.setSelected(true);
				existingAlarm = true;
			} else {
				triggerCheck.setSelected(false);
			}
			alarmCheckPanel.add(triggerCheck);
			alarmCheckPanel.add(Box.createHorizontalGlue());
			//alarmPanel.add(Box.createVerticalStrut(10));
			alarmPanel.add(alarmCheckPanel);
			alarmPanel.add(Box.createVerticalStrut(20));
			
			
			JPanel triggerPanel = new JPanel();
			triggerPanel.setLayout(new BoxLayout(triggerPanel, BoxLayout.PAGE_AXIS));
			
			JPanel triggerLabelPanel = new JPanel();
			triggerLabelPanel.setLayout(new BoxLayout(triggerLabelPanel, BoxLayout.LINE_AXIS));
			JLabel triggerLabel = new JLabel();
			if (calendarComponentClass.equals("PEvent")) {
					triggerLabel.setText( CalendarSetting.calTranslate("trigger_before_start"));
			} else if (calendarComponentClass.equals("PTodo")) {
				triggerLabel.setText( CalendarSetting.calTranslate("trigger_before_end"));				
			}
			triggerLabelPanel.add(triggerLabel);
			triggerLabelPanel.add(Box.createHorizontalGlue());	
			triggerPanel.add(triggerLabelPanel);
			
			JPanel triggerValuePanel = new JPanel();
			triggerValuePanel.setLayout(new BoxLayout(triggerValuePanel, BoxLayout.LINE_AXIS));
			
			String triggerString = null;
			if (existingCalComponent != null && existingCalComponent.getAlarms() != null ) {
				triggerString = existingCalComponent.getAlarms().get(0).getTrigger();
			}
			Dimension spinnerMaxDim = new Dimension(80, 40);
			Dimension spinnerMinDim = new Dimension(70, 25);
			triggerValuePanel.add(new JLabel(CalendarSetting.calTranslate("weeks") + ": "));
			SpinnerModel weekModel =
			        new SpinnerNumberModel(0, //initial value
			                               0, //min
			                               10, //max
			                               1);                //step
			weekSpinner = new JSpinner(weekModel);
			//weekSpinner.setPreferredSize(new Dimension(70, 25));
			weekSpinner.setMaximumSize(spinnerMaxDim);
			weekSpinner.setMinimumSize(spinnerMinDim);
			if (triggerString != null && triggerString.contains("W")) {
				String wString = triggerString.substring(triggerString.indexOf("P") + 1, triggerString.indexOf("W"));
				int w = Integer.parseInt(wString);
				weekSpinner.setValue(w);
			}
			triggerValuePanel.add(weekSpinner);
			triggerValuePanel.add(Box.createHorizontalStrut(10));
			
			triggerValuePanel.add(new JLabel(CalendarSetting.calTranslate("days") + ": "));
			SpinnerModel dayModel =
			        new SpinnerNumberModel(0, //initial value
			                               0, //min
			                               7, //max
			                               1);                //step
			daySpinner = new JSpinner(dayModel);
			daySpinner.setMaximumSize(spinnerMaxDim);
			daySpinner.setMinimumSize(spinnerMinDim);
			if (triggerString != null && triggerString.contains("D")) {
				String dString = triggerString.substring(triggerString.indexOf("D") - 1, triggerString.indexOf("D"));
				int d = Integer.parseInt(dString);
				daySpinner.setValue(d);
			}
			triggerValuePanel.add(daySpinner);
			triggerValuePanel.add(Box.createHorizontalStrut(10));
			
			triggerValuePanel.add(new JLabel(CalendarSetting.calTranslate("hour") + ": "));
			SpinnerModel hourModel =
			        new SpinnerNumberModel(0, //initial value
			                               0, //min
			                               24, //max
			                               1);                //step
			hourSpinner = new JSpinner(hourModel);
			hourSpinner.setMaximumSize(spinnerMaxDim);
			hourSpinner.setMinimumSize(spinnerMinDim);
			if (triggerString != null && triggerString.contains("H")) {
				int subStart = triggerString.indexOf("H") - 1;
				if ( Character.isDigit(triggerString.charAt(triggerString.indexOf("H") - 2))) {
					subStart = triggerString.indexOf("H") - 2;
				}
				String hString = triggerString.substring(subStart, triggerString.indexOf("H"));
				int h = Integer.parseInt(hString);
				hourSpinner.setValue(h);
			}
			triggerValuePanel.add(hourSpinner);
			triggerValuePanel.add(Box.createHorizontalStrut(10));

			
			triggerValuePanel.add(new JLabel(CalendarSetting.calTranslate("minute") + ": "));
			SpinnerModel minzteModel =
			        new SpinnerNumberModel(0, //initial value
			                               0, //min
			                               60, //max
			                               1);                //step
			minuteSpinner = new JSpinner(minzteModel);
			minuteSpinner.setMaximumSize(spinnerMaxDim);
			minuteSpinner.setMinimumSize(spinnerMinDim);
			if (triggerString != null && triggerString.contains("M")) {
				int subStart = triggerString.indexOf("M") - 1;
				if ( Character.isDigit(triggerString.charAt(triggerString.indexOf("M") - 2))) {
					subStart = triggerString.indexOf("M") - 2;
				}
				String mString = triggerString.substring(subStart, triggerString.indexOf("M"));
				int m = Integer.parseInt(mString);
				minuteSpinner.setValue(m);
			}
			triggerValuePanel.add(minuteSpinner);
			triggerValuePanel.add(Box.createHorizontalStrut(10));

			triggerPanel.add(triggerValuePanel);
			if (existingAlarm == false) {
				triggerPanel.setVisible(false);
			} 
			
			alarmPanel.add(triggerPanel);
			alarmPanel.add(Box.createVerticalStrut(20));
			
			
			JPanel actionPanel = new JPanel();
			actionPanel.setLayout(new BoxLayout(actionPanel, BoxLayout.PAGE_AXIS));
			JPanel actionLabelPanel = new JPanel();
			actionLabelPanel.setLayout(new BoxLayout(actionLabelPanel, BoxLayout.LINE_AXIS));
			JLabel actionLabel = new JLabel(CalendarSetting.calTranslate("action"));
			actionLabelPanel.add(actionLabel);
			actionLabelPanel.add(Box.createHorizontalGlue());	
			actionPanel.add(actionLabelPanel);
			
			audioCheck = new JCheckBox(CalendarSetting.calTranslate("audio"));
			audioCheck.setSelected(false);
			audioCheck.addActionListener(this);
			audioCheck.setActionCommand("audioAlarm");
			displayCheck = new JCheckBox(CalendarSetting.calTranslate("display"));
			displayCheck.setSelected(true); // default is true
			displayCheck.addActionListener(this);
			displayCheck.setActionCommand("displayAlarm");
			if (existingCalComponent != null && existingCalComponent.getAlarms() != null) {
				for (PAlarm a : existingCalComponent.getAlarms()) {
					if (a.getAction().equals(PAlarm.getActionStrings()[0])) {
						audioCheck.setSelected(true);
					}
					if (a.getAction().equals(PAlarm.getActionStrings()[1])) {
						displayCheck.setSelected(true);
					}
				}
			}
	/*				&& existingCalComponent.getAlarm().getAction() != null ) {
				if (existingCalComponent.getAlarm().getAction().contains(PAlarm.getActionStrings()[0])) {
					audioCheck.setSelected(true);
				}
			}

			if (existingCalComponent != null && existingCalComponent.getAlarm() != null
					&& existingCalComponent.getAlarm().getAction() != null ) {
				if (existingCalComponent.getAlarm().getAction().contains(PAlarm.getActionStrings()[1])) {
					displayCheck.setSelected(true);
				}
			}*/
			actionPanel.add(audioCheck);
			actionPanel.add(displayCheck);

		/*	actionBox = new JComboBox<String>();
			actionBox.setPreferredSize(new Dimension(200, (int)actionBox.getPreferredSize().getHeight()));
			actionBox.setMaximumSize(new Dimension(800, 50));
			String[] actions = PAlarm.getActionStrings();
			for (String a : actions) {
				actionBox.addItem(a);
			}
			if (existingCalComponent != null && existingCalComponent.getAlarm() != null
					&& existingCalComponent.getAlarm().getAction() != null ) {
				actionBox.setSelectedItem(existingCalComponent.getAlarm().getAction());
			}
			actionPanel.add(actionBox);*/
			if (existingAlarm == false) {
				actionPanel.setVisible(false);
			}
			alarmPanel.add(actionPanel);
			alarmPanel.add(Box.createVerticalStrut(20));
			
			
			JPanel descriptionLabelPanel = new JPanel();		
			descriptionLabelPanel.setLayout(new BoxLayout(descriptionLabelPanel, BoxLayout.LINE_AXIS));
			JLabel descriptionLabel = new JLabel(CalendarSetting.calTranslate("description"));
			descriptionLabelPanel.add(descriptionLabel);
			descriptionLabelPanel.add(Box.createHorizontalGlue());	
			if (existingAlarm == false) {
				descriptionLabelPanel.setVisible(false);
			}
			alarmPanel.add(descriptionLabelPanel);
			
			descriptionField = new JTextField();//50);
			descriptionLabel.setLabelFor(descriptionField);
			if (existingCalComponent != null && existingCalComponent.getAlarms() != null) {
				for (PAlarm a : existingCalComponent.getAlarms()) {
					if (a.getDescription() != null) {
						descriptionField.setText(a.getDescription());
						break;
					}
				}			
			}
			descriptionField.setPreferredSize(new Dimension(200, (int)descriptionField.getPreferredSize().getHeight()));
			descriptionField.setMaximumSize(new Dimension(800, 50));
			if (existingAlarm == false) {
				descriptionField.setVisible(false);
			}
			alarmPanel.add(descriptionField);

			
			//alarmMessageLabel = new JLabel();
			alarmMessageLabel.setForeground(Color.RED);
			JPanel alarmMessageLabelPanel = new JPanel();
			alarmMessageLabelPanel.setLayout(new BoxLayout(alarmMessageLabelPanel, BoxLayout.LINE_AXIS));
			alarmMessageLabelPanel.add(alarmMessageLabel);
		/*	if (existingCalComponent != null && existingCalComponent.getAlarms() != null 
					&& (existingCalComponent instanceof PEvent) 
					&& ((PEvent) existingCalComponent).isAllDay()) {
				setAlarmMessage(CalendarSetting.calTranslate("alarm_before_noon"), false); 
			} */
			alarmPanel.add(alarmMessageLabelPanel);
			
			alarmPanel.add(Box.createVerticalGlue());
			
			alarmPanel.add(createSetButtonPanel(okButtonText));
			return alarmPanel;
		}
		
/*		protected JComponent[] createDurationPanel() {
			JPanel durationLabelPanel = new JPanel();
			durationLabelPanel.setLayout(new BoxLayout(durationLabelPanel, BoxLayout.X_AXIS));
			JLabel endLabel = new JLabel("duration: " );
			durationLabelPanel.add(endLabel);
			durationLabelPanel.add(Box.createHorizontalGlue());
			timeAndDatePanel.add(durationLabelPanel);
			
			JPanel durationPanel = new JPanel();
			durationPanel.setLayout(new BoxLayout(durationPanel, BoxLayout.X_AXIS));
			durationPanel.add(Box.createHorizontalStrut(10));
			
			Duration duration = event.getDuration().getValue();
			int minutes = 0;
			int hours = 0;
			int days = 0;
			int weeks = 0;
			if (duration.getSeconds() != null) {
				minutes = duration.getMinutes();
			}
			if (duration.getSeconds() != null) {
				hours = duration.getHours();
			}
			if (duration.getSeconds() != null) {
				days = duration.getDays();
			}
			if (duration.getSeconds() != null) {
				weeks = duration.getWeeks();
			}
			durationPanel.add(new JLabel("weeks: "));
			durationPanel.add(Box.createHorizontalStrut(5));			
		    durationWeeksSpinner = new JSpinner();
		    durationWeeksSpinner.setModel(new SpinnerNumberModel(weeks, 0, 52,1)); // value, min, max, step
		    durationWeeksSpinner.setMaximumSize(new Dimension(60, 25));
		    durationWeeksSpinner.setMinimumSize(new Dimension(60, 25));			    
		    durationPanel.add(durationWeeksSpinner);
		    durationPanel.add(Box.createHorizontalStrut(10));		    
			durationPanel.add(new JLabel("days: "));
			durationPanel.add(Box.createHorizontalStrut(5));			
		    durationDaysSpinner = new JSpinner();
		    durationDaysSpinner.setModel(new SpinnerNumberModel(days, 0, 31,1)); // value, min, max, step
		    durationDaysSpinner.setMaximumSize(new Dimension(60, 25));
		    durationDaysSpinner.setMinimumSize(new Dimension(60, 25));			    
		    durationPanel.add(durationDaysSpinner);
		    durationPanel.add(Box.createHorizontalStrut(10));			    
			durationPanel.add(new JLabel("hours: "));
			durationPanel.add(Box.createHorizontalStrut(5));			
		    durationHoursSpinner = new JSpinner();
		    durationHoursSpinner.setModel(new SpinnerNumberModel(hours, 0, 24,1)); // value, min, max, step
		    durationHoursSpinner.setMaximumSize(new Dimension(60, 25));
		    durationHoursSpinner.setMinimumSize(new Dimension(60, 25));				    
		    durationPanel.add(durationHoursSpinner);
		    durationPanel.add(Box.createHorizontalStrut(10));				    
			durationPanel.add(new JLabel("minutes: "));
			durationPanel.add(Box.createHorizontalStrut(5));			
		    durationMinutesSpinner = new JSpinner();
		    durationMinutesSpinner.setModel(new SpinnerNumberModel(minutes, 0, 60,1)); // value, min, max, step
		    durationMinutesSpinner.setMaximumSize(new Dimension(60, 25));
		    durationMinutesSpinner.setMinimumSize(new Dimension(60, 25));			    
		    durationPanel.add(durationMinutesSpinner);
		    durationPanel.add(Box.createHorizontalStrut(10));						
			durationPanel.add(Box.createHorizontalGlue());
			timeAndDatePanel.add(durationPanel);		
		}*/
		
		/**
		 * Set the background color of this dialog
		 * corresponding to the calendar color
		 * 
		 * @param the calendar name or null (new event/todo)
		 */
		protected void setColorOfCalendarComponent(String cal) {
			// set the background color
			if (existingCalComponent != null) {
				if (existingCalComponent.getCalendar() != null &&
						existingCalComponent.getCalendar().getColor() != null) {
					this.getContentPane().setBackground(existingCalComponent.getCalendar().getColor());
				} else {
					TestLog.ve(ComponentSetter.class, "Cant set color...", 5);
				}
			} else { // new event/todo
				if (cal != null) {//calendarComboBox.getSelectedItem() instanceof String) {
					//String cal = (String) calendarComboBox.getSelectedItem();				
					PCalendar c = CalendarControl.getCalendarByName(cal);
					if (c != null) {
						Color color = c.getColor();
						this.getContentPane().setBackground(color);
					} else {
						TestLog.ve(ComponentSetter.class, "Missing color", 5);	
					}
				} else {
					TestLog.ve(ComponentSetter.class, "Invalid item", 5);
				}
			}
		}

		@Override
		public void actionPerformed(ActionEvent ap) {
			String command = ap.getActionCommand();
			//TestLog.v(ComponentSetter.class, command);
			
			if (command.equals("removeMoreMessage")) {
				setMoreMessage("", false);
			} else if (command.equals("setStartDateTime")) {
				showHideStart();
			} else if (command.equals("setEndDateTime")) {
				showHideDue();
			} else if (command.equals("openUrl")) {
				String url = getUrl();
				try {         
					java.awt.Desktop.getDesktop().browse(java.net.URI.create(url));
				} catch (Exception e) {
					setMoreMessage(e.getLocalizedMessage(), true);
					Timer timer1 = new Timer(3000, this);
					timer1.setActionCommand("removeMoreMessage");
					timer1.setRepeats(false);
					timer1.start();
				}
			} else if (command.startsWith("datePicker")) { // TODO setDateStart()

				if (command.equals("datePickerStart")) {
					setStartDateTime(null);
				} else if (command.equals("datePickerEnd")) {
					setEndDateTime(null);			
				}		
				
			} else if (command.startsWith("audioAlarm")) { 
				if (audioCheck.isSelected() == false) {
					if (displayCheck.isSelected() == false) {
						setAlarmMessage("At least one action must be set", true);
					}
				} else {
					setAlarmMessage(null, false);
				}
			} else if (command.startsWith("displayAlarm")) { 
				if (displayCheck.isSelected() == false) {
					if (audioCheck.isSelected() == false) {
						setAlarmMessage("At least one action must be set", true);
					}
				} else {
					setAlarmMessage(null, false);
				}
				
			} else if (command.startsWith("setAlarm")) { 
				if (triggerCheck.isSelected()) {
					Component[] comps = alarmPanel.getComponents();
					for (Component c : comps) {
						c.setVisible(true);
					}
				} else {
					Component[] comps = alarmPanel.getComponents();
					int compsLen = comps.length;
					for (int i = 1; i < compsLen;i++) {
						Component c = comps[i];
						c.setVisible(false);
					}	
				}
			} else {
				// event/todo related action
				// reset messageLabel:
				setMainMessage("", false);
				setMoreMessage("", false);
				setRecurrenceMessage("", false);

				// The event/todo to set or modify
				CalendarComponent calComponent = null;
				// the calendar, this event belongs to before setting (may change)
				PCalendar modifiedCalendar = null;

				if (existingCalComponent == null) {
					// something went wrong, log and create new event/todo instead
					if (newCalComponent == false) {
						TestLog.ve(CalendarComponent.class, "Missing event/todo", 5);
						TestLog.ve(CalendarComponent.class, "Set from existing event/todo to new one...");
						// reset and create new event/todo
						newCalComponent = true;
					}
				}
				// 1. Get the corresponding calendar of the event/todo
				// A. existing event/todo
				if (newCalComponent == false) {
					// First: get calendar of event/todo, this may change
					modifiedCalendar = existingCalComponent.getCalendar();
					if (modifiedCalendar == null) {
						TestLog.ve(CalendarComponent.class, "Missing calendar of existing event/todo...", 5);
					} else {
						//TestLog.v(CalendarComponent.class, "Calendar of event/todo: " );
					}
					// Get the selected calendar.maybe the calendar was changed:
					PCalendar selectedCalendar = null;
					if (getSelectedCalendar() == null) { // get from setter
						TestLog.ve(CalendarComponent.class, "Missing calendar of existing event/todo...", 5);
					} else {
						selectedCalendar = CalendarControl.getCalendarByName(getSelectedCalendar());
						if (selectedCalendar == null) {
							TestLog.v(CalendarComponent.class, "Calendar of event/todo is null", 5);
						} else { 
							// try to replace eventually missing modifiedCalendar
							if (modifiedCalendar == null) {
								TestLog.ve(CalendarComponent.class, "Replace missing calendar of existing event/todo...");
								modifiedCalendar = selectedCalendar;
							}
						}
					}
					if (selectedCalendar != null &&  ! modifiedCalendar.equals(selectedCalendar)) {
						// calendar was changed: event/todo must be removed from original calendar
						// and set to new calendar
						TestLog.v(ComponentSetter.class, "Calendar of event was changed, from " + modifiedCalendar.getName() );
						// remove event
						if (existingCalComponent instanceof PEvent) {
							//modifiedCalendar.removeEvent((PEvent)existingCalComponent);
							CalendarControl.removeEvent((PEvent)calComponent, modifiedCalendar);
						} else if (existingCalComponent instanceof PTodo) {
							//modifiedCalendar.removeTodo((PTodo)existingCalComponent);
							CalendarControl.removeTodo((PTodo)calComponent, modifiedCalendar);
						} 					
						// set new calendar for event
						existingCalComponent.setCalendar(selectedCalendar);
						modifiedCalendar = selectedCalendar;
					}
				} else { // new event/todo
					// Get the selected calendar:
					String selectedCalName = getSelectedCalendar();
					if (selectedCalName == null) {
						TestLog.e(CalendarComponent.class, "Unexpected error", 5);
						TestLog.ve(CalendarComponent.class, "Calendar name of event/todo is null", 5);
						TestLog.ve(ComponentSetter.class, "invalid CalendarComponent", 5);
						setErrorMessageToAllTabs(CalendarSetting.calTranslate("unexpected_error")
								+ " - missing calendar", true);
						this.pack();						
						return;
					}
					PCalendar selectedCalendar = CalendarControl.getCalendarByName(getSelectedCalendar());
					if (selectedCalendar == null) {
						TestLog.e(CalendarComponent.class, "Unexpected error", 5);
						TestLog.ve(CalendarComponent.class, "Calendar of event/todo is null", 5);
						TestLog.ve(ComponentSetter.class, "invalid CalendarComponent", 5);
						setErrorMessageToAllTabs(CalendarSetting.calTranslate("unexpected_error")
								+ " - missing calendar", true);
						this.pack();
						return;
					} else {
						modifiedCalendar = selectedCalendar;
					}
				}
				if (modifiedCalendar == null) {
					TestLog.e(CalendarComponent.class, "Missing calendar of existing event/todo. Can't set todo/event...");
					TestLog.ve(ComponentSetter.class, "invalid CalendarComponent", 5);
					setErrorMessageToAllTabs(CalendarSetting.calTranslate("unexpected_error")
							+ " - missing calendar", true);
					this.pack();
					return;
				}

				// 2. Set the event/todo
				if (calendarComponentClass == null) { // 
					TestLog.e(CalendarComponent.class, "Unexpected error", 5);		
					TestLog.ve(CalendarComponent.class, "CalendarComponentClass is null", 5);
					TestLog.ve(ComponentSetter.class, "invalid CalendarComponent", 5);
					setErrorMessageToAllTabs(CalendarSetting.calTranslate("unexpected_error")
							+ " - missing setter", true);
					this.pack();
					return;
				}
				if (existingCalComponent == null) {
					if (calendarComponentClass.equals("PEvent")) {
						calComponent = new PEvent(modifiedCalendar);
					} else if (calendarComponentClass.equals("PTodo")) {
						
						calComponent =  new PTodo(modifiedCalendar);
					} else {
						TestLog.ve(ComponentSetter.class, "invalid CalendarComponent", 5);
						setErrorMessageToAllTabs(CalendarSetting.calTranslate("unexpected_error")
								+ " - invalid event/todo", true);
						this.pack();
						return;
					}
				} else { // just check existing calComponent and log error
					if (calendarComponentClass.equals("PEvent")) {
						if ( ! (existingCalComponent instanceof PEvent) ) {
							TestLog.ve(ComponentSetter.class, "Unexpected error", 5);
							setErrorMessageToAllTabs(CalendarSetting.calTranslate("unexpected_error")
									+ " - invalid event/todo", true);
							this.pack();
							return;
						} else {
							calComponent = (PEvent) existingCalComponent;							
						}
					} else if (calendarComponentClass.equals("PTodo")) {
						if ( ! (existingCalComponent instanceof PTodo) ) {
							TestLog.ve(ComponentSetter.class, "Unexpected error", 5);
							setErrorMessageToAllTabs(CalendarSetting.calTranslate("unexpected_error")
									+ " - invalid event/todo", true);
							this.pack();
							return;
						} else {
							calComponent = (PTodo) existingCalComponent;	
						}
					} else {
						TestLog.ve(ComponentSetter.class, "Unexpected error", 5);
						setErrorMessageToAllTabs(CalendarSetting.calTranslate("unexpected_error")
								+ " - invalid event/todo", true);
						this.pack();
						return;
					}
				}

				if (command.equals("remove")) {
					if (newCalComponent == false) {

						if (existingCalComponent instanceof PEvent) {
							CalendarControl.removeEvent((PEvent)calComponent, modifiedCalendar);
						} else if (existingCalComponent instanceof PTodo) {
							CalendarControl.removeTodo((PTodo)calComponent, modifiedCalendar);
						} 
						CalendarControl.refreshView();
					}
					this.dispose();
					if (JREProperties.getMainWindow() != null && JREProperties.getMainWindow().isVisible() ) {
						JREProperties.getMainWindow().requestFocus();
					}

				} else if (command.equals("export")) {

					TestLog.v(getClass(), "Export event...");
					if (newCalComponent == false) {

						String[] options = {CalendarSetting.calTranslate("export_as_ics"), 
								CalendarSetting.calTranslate("export_as_text")};

						int exp = PeaDialog.showOptionYAxis(this,//JREProperties.getMainWindow(), 
								CalendarSetting.calTranslate("export"), 
								null, 
								3, options, 0);

						String exportFileName = FileTools.replaceCharsForFileName( existingCalComponent.getDateStart().toString());
						String content = null;
						if (exp == 0) {
							// export as ics file
							exportFileName = exportFileName + ".ics";
							PCalendar exportCal = new PCalendar( FileTools.replaceCharsForFileName(
									existingCalComponent.getDateStart().toString()), 
									exportFileName, null, null);
							if (existingCalComponent instanceof PEvent) {
								exportCal.addEvent((PEvent) existingCalComponent);
							} else {
								exportCal.addTodo((PTodo) existingCalComponent);
							}
							//content = WriteICal.singleICalToString(exportCal, false);
							CalendarMenuBar.exportCalendar(exportCal, this);
						} else if (exp == 1) {
							// export as text file
							exportFileName = exportFileName + ".txt";

							// open file chooser to select a directory to store files
							PeaFileChooser pfc = new PeaFileChooser(this);//JREProperties.getMainWindow());
							pfc.setFileSelectionMode(PeaFileChooser.DIRECTORIES_ONLY);
							pfc.setApproveButtonText(CalendarSetting.calTranslate("select"));
							pfc.customizeFileChooser(CalendarSetting.calTranslate("export"), CalendarSetting.calTranslate("select_folder_for_calendar_file"), false);
							pfc.showOpenDialog(this);
							// get selected directory
							File directory = pfc.getSelectedFile();
							if (directory != null) {
								exportFileName = directory.getAbsolutePath() + File.separator + exportFileName;
								content = WriteICal.calComponentToReadableString(existingCalComponent);
								WriteResources.writeText(content, exportFileName);
							} else {
								return;
							}
						}
					}
					this.dispose();
					if (JREProperties.getMainWindow() != null && JREProperties.getMainWindow().isVisible() ) {
						JREProperties.getMainWindow().requestFocus();
					}

				} else if (command.equals("all-day")) { // TODO implement in EventSetter setAllDay()

					if (calComponent instanceof PEvent) {
						setAllDay(); // hide or show time 
					} 
					// calendarComboBox.setActionCommand("calendarChange");
				} else if (command.equals("calendarChange")) {
					String calName = getSelectedCalendar();
					if (calName != null) {
						PCalendar c = CalendarControl.getCalendarByName(calName);
						if (c != null) {
							Color color = c.getColor();
							if (color != null) {
								this.getContentPane().setBackground(color);
							}
						}
					}			

				} else if (command.equals("set")) {
					
					if (calComponent == null) {
						TestLog.ve(ComponentSetter.class, "event/todo is null", 5);
					}
	
					// at least summary must be set 
					if ( ( getSummary() == null || getSummary().length() == 0) && calComponent instanceof PTodo) {
						TestLog.ve(ComponentSetter.class, "Missing summary for todo");
						setErrorMessageToAllTabs(PeaProperties.getVmBridge().translate("missing") + ": " +
								CalendarSetting.calTranslate("summary"), true);
						this.pack();
						return;
					} 
					//=========================================================
					// 1. get the properties for the event:			

					LocalDateTime startDateTime = null;			
					LocalDate startDate = null;
					if (getStartDateTime() != null) {
						startDate = getStartDateTime().toLocalDate();// TODO VTODO without startDate
					}
					LocalDate endDate = null;
					if (isAllDay() == true) {// && calComponent instanceof PEvent) { // all-day event
						TestLog.v(ComponentSetter.class, "Set all-day event");
						startDateTime = getStartDateTime();
						((PEvent)calComponent).setAllDay(true);
						calComponent.setDateStart(getStartDateTime());

						if (getEndDateTime() != null) {
							endDate = getEndDateTime().toLocalDate();
						}
						// Event must have an end date
						if (endDate == null && calComponent instanceof PEvent) {
							endDate = startDate;
						}

						// for value=date always one date later!!
						// date is treated as time 000000, dtend is treated as exclusive
						if (endDate != null && calComponent instanceof PEvent) {
							LocalDateTime endDateTime = endDate.atStartOfDay().plusDays(1); // hours, minutes 00:00
							((PEvent)calComponent).setDateEnd(endDateTime);
							if (endDateTime.isBefore(startDateTime)) {
								setErrorMessageToAllTabs(CalendarSetting.calTranslate("end") + 
										CalendarSetting.calTranslate("is_before_start_date"), true);
								this.pack();
								return;
							}
						}
					} else {
						if (calComponent instanceof PEvent) {
							((PEvent)calComponent).setAllDay(false);
						}

						startDateTime = getStartDateTime();
						if (startDateTime != null) {
							calComponent.setDateStart(getStartDateTime());
						} else {
							if (calComponent instanceof PEvent) {
								TestLog.ve(ComponentSetter.class, "Missing start date time of event", 5);
							}
						}


						if ( getEndDateTime() != null) {
							endDate = getEndDateTime().toLocalDate();
						}
						if (isRecurring() == true && calComponent instanceof PEvent) { //recurringCheck.isSelected()) { // several days recurring is not allowed yet
							endDate = startDate;
						}

						LocalDateTime endDateTime = null;
						if (endDate != null) {
							endDateTime = LocalDateTime.of(endDate, getEndDateTime().toLocalTime());
							if (startDateTime != null) {
								if (endDateTime != null && endDateTime.isBefore(startDateTime)) {
									TestLog.ve(ComponentSetter.class, "End is before start");
									setErrorMessageToAllTabs(CalendarSetting.calTranslate("end") + 
											CalendarSetting.calTranslate("is_before_start_date"), true);
									this.pack();
									return;
								}
							}
						}
						if (calComponent instanceof PEvent) {
							((PEvent)calComponent).setDateEnd(endDateTime);
						} else if (calComponent instanceof PTodo) {
							((PTodo) calComponent).setDue(endDateTime);
						}
					} // end not all-day event
					// set if this event extends to more than one day or not

					if (calComponent instanceof PEvent
							&& endDate != null && calComponent.getDateStart() != null) {
						if (endDate.isAfter(calComponent.getDateStart().toLocalDate())) {
							((PEvent)calComponent).setSeveralDays(true);					
						} else {
							((PEvent)calComponent).setSeveralDays(false);	
						}	
					}

					calComponent = setRecurrence(calComponent);

					if (getSummary() != null) {
						calComponent.setSummary(getSummary());
					}
					if (getDescription() != null) {
						calComponent.setDescription(getDescription());
					}
					if (getCalComponentLocation() != null) {
						calComponent.setLocation(getCalComponentLocation());
					}
					if (getUrl() != null && getUrl().length() > 0) {
						try {
							URL url = new URL(getUrl());
							calComponent.setUrl(url);
						} catch( Exception e) {
							TestLog.e(ComponentSetter.class, "Invalid URL: " + getUrl());
						}
					}
			
					int priority = getPriority();
					calComponent.setPriority(priority);
					calComponent.setStatus(getStatus());
					// set time stamp:
					calComponent.setTimeStamp(Instant.now());
					
					if (isAlarm() == true) {

						if (audioCheck != null && audioCheck.isSelected() 
								|| displayCheck != null && displayCheck.isSelected()) {
							calComponent.setAlarms(null);
							String trigger = null;
							if (getAlarmWeeks() > 0 || getAlarmDays() > 0 || getAlarmHours() > 0 || getAlarmMinutes() > 0) {
								StringBuilder triggerBuilder = new StringBuilder();
								triggerBuilder.append("-P");
								if (getAlarmWeeks() > 0) {
									triggerBuilder.append(getAlarmWeeks() + "W");
								}
								if (getAlarmDays() > 0) {
									triggerBuilder.append(getAlarmDays() + "D");
								}
								if (getAlarmHours() > 0 || getAlarmMinutes() > 0) {
									triggerBuilder.append("T");
								}
								if (getAlarmHours() > 0) {
									triggerBuilder.append( getAlarmHours() + "H");
								}
								if (getAlarmMinutes() > 0) {
									triggerBuilder.append(getAlarmMinutes() + "M");
								}
								trigger = new String (triggerBuilder);
							}
							LocalDateTime now = LocalDateTime.now();
							LocalDateTime end = now.plusHours(AlarmControl.getUpdateAfterHours());
							// set several alarms if several actions were set:
							if (audioCheck.isSelected()) {
								PAlarm  alarm = new PAlarm(calComponent, trigger, PAlarm.getActionStrings()[0], getAlarmDescription());
								calComponent.addAlarm(alarm);
								// check if alarm is in current schedule period:
								if (calComponent.isRecurring() == false) {
									if (alarm.getTriggerTime().isAfter(now) 
											&& alarm.getTriggerTime().isBefore(end)) {
										AlarmControl.addAlarmToRunningScheduler(alarm);
									}
								} else {
									// TODO TEST
									ArrayList<PAlarm>  a = ((RecurringPEvent) calComponent).getAlarmsOfDay(now, end);
									if (a != null && a.size() > 0) {
										AlarmControl.addAlarmToRunningScheduler(alarm);
									}
								}
							}				
							if (displayCheck.isSelected()) {
								PAlarm  alarm = new PAlarm(calComponent, trigger, PAlarm.getActionStrings()[1], getAlarmDescription());
								calComponent.addAlarm(alarm);
								// check if alarm is in current schedule period:
								if (calComponent.isRecurring() == false) {
									if (alarm.getTriggerTime().isAfter(now) 
											&& alarm.getTriggerTime().isBefore(end)) {
										AlarmControl.addAlarmToRunningScheduler(alarm);
									}
								} else {
									// TODO
									ArrayList<PAlarm>  a = ((RecurringPEvent) calComponent).getAlarmsOfDay(now, end);
									if (a != null && a.size() > 0) {
										AlarmControl.addAlarmToRunningScheduler(alarm);
									}
								}
							}			
						}
					}
					
					if (newCalComponent == true) {
						if (calendarComponentClass.equals("PEvent")) {
							CalendarControl.addEvent((PEvent) calComponent, modifiedCalendar);
						} else if (calendarComponentClass.equals("PTodo")) {
							CalendarControl.addTodo((PTodo) calComponent, modifiedCalendar);
						} else {
							TestLog.ve(ComponentSetter.class, "invalid CalendarComponent", 5);
						}
					} else {
						//TestLog.v(ComponentSetter.class, "existing event modified");
					}
					CalendarControl.refreshView();
					((LockFrameCalendar)PeaControl.getDialog().getLockFrame() ).setMessage(null, false);
					LockFrameCalendar.getFrame().markUnsavedContentChanges();//.setDocChangeUnsaved(true);
					this.dispose();		
					if (JREProperties.getMainWindow() != null && JREProperties.getMainWindow().isVisible() ) {
						JREProperties.getMainWindow().requestFocus();
					}
				}
			}		
		}
		
		protected void setErrorMessageToAllTabs(String message, boolean error) {
			setMainMessage(message, error);
			setMoreMessage(message, error);
			setAlarmMessage(message, error);
			setRecurrenceMessage(message, error);
		}
	}
