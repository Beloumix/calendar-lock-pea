package cologne.eck.calendar_pea.gui.event_setting;

/*
 * PeaFactory - Production of Password Encryption Archives
 * Copyright (C) 2015  Axel von dem Bruch
 * 
 * This library is free software; you can redistribute it and/or modify it 
 * under the terms of the GNU General Public License as published 
 * by the Free Software Foundation; either version 2 of the License, 
 * or (at your option) any later version.
 * This library is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
 * or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU General Public License for more details.
 * See:  http://www.gnu.org/licenses/gpl-2.0.html
 * You should have received a copy of the GNU General Public License 
 * along with this library.
 */

import java.awt.Color;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Collections;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;
import javax.swing.border.MatteBorder;

import cologne.eck.calendar_pea.control.CalendarSetting;
import cologne.eck.calendar_pea.data.RecurringPEvent;

@SuppressWarnings("serial")
class ExceptionPanel extends JPanel implements ActionListener{

	private RecurrencePanel recurrencePanel;
	private RecurringPEvent recurringEvent;

	protected ExceptionPanel(RecurrencePanel _recurrencePanel, RecurringPEvent _recurringEvent, ChronoUnit _freq) {

		this.recurrencePanel = _recurrencePanel;
		this.recurringEvent = _recurringEvent;

		this.setLayout(new BoxLayout(this, BoxLayout.PAGE_AXIS));
		this.setBorder(new MatteBorder(1, 0, 0, 0, Color.GRAY));
		JPanel exButtonPanel = new JPanel();
		exButtonPanel.setLayout(new BoxLayout(exButtonPanel, BoxLayout.LINE_AXIS));
		JButton exceptionsButton = new JButton(CalendarSetting.calTranslate("add_exception"));
		exceptionsButton.addActionListener(this);
		exceptionsButton.setActionCommand("exception");
		exButtonPanel.add(exceptionsButton);
		exButtonPanel.add(Box.createHorizontalGlue());
		this.add(Box.createVerticalStrut(5));
		this.add(exButtonPanel);
		if (recurringEvent != null && recurringEvent.getExceptionDates() != null) {
			// Add existing exceptions:
			LocalDate[] dates = recurringEvent.getExceptionDates();
			int exLength = dates.length;
			for (int i = 0; i < exLength; i++) {
				JPanel datePanel = new JPanel();
				datePanel.setLayout(new BoxLayout(datePanel, BoxLayout.LINE_AXIS));
				if (i == 0) {
					datePanel.add(new JLabel(CalendarSetting.calTranslate("exceptions")));
				}
				datePanel.add(Box.createHorizontalGlue());
				String newDate = CalendarSetting.getDayDateFormatter().format(dates[i]);
				datePanel.add(new JLabel(newDate));
				datePanel.setName(newDate);
				datePanel.add(Box.createHorizontalStrut(5));
				JButton removeButton = new JButton("X");//CalendarSetting.calTranslate("remove"));
				removeButton.setToolTipText(CalendarSetting.calTranslate("remove"));
				removeButton.addActionListener(this);
				removeButton.setActionCommand("remove");
				datePanel.add(removeButton);

				this.add(datePanel);
			}			
		}	
	}

	@Override
	public void actionPerformed(ActionEvent ap) {

		String command = ap.getActionCommand();

		if (command.equals("exception")) {			

			// show the DatePicker 
			LocalDate date = null;
			LocalDate startDate = recurrencePanel.getEventSetter().getStartDate();
			if (startDate.isBefore(LocalDate.now())) {
				// start with current date
				date = new DatePicker(LocalDate.now(), true).showDatePicker(this, null);
			} else {
				// start with startDate
				date = new DatePicker(recurrencePanel.getEventSetter().getStartDate(), true).showDatePicker(this, null);
			}

			JPanel datePanel = new JPanel();
			datePanel.setLayout(new BoxLayout(datePanel, BoxLayout.LINE_AXIS));
			datePanel.add(Box.createHorizontalGlue());
			String newDate = CalendarSetting.getDayDateFormatter().format(date);
			datePanel.add(new JLabel(newDate));
			datePanel.setName(newDate);
			datePanel.add(Box.createHorizontalStrut(5));
			JButton removeButton = new JButton("X");//CalendarSetting.calTranslate("remove"));
			removeButton.setToolTipText(CalendarSetting.calTranslate("remove"));
			removeButton.addActionListener(this);
			removeButton.setActionCommand("remove");
			datePanel.add(removeButton);

			this.add(datePanel);
			this.revalidate();
			this.repaint();
			((Window) SwingUtilities.getRoot(this)).pack();

		} else if (command.equals("remove")) { // by-day-rules, exceptions, rdates

			JButton button = (JButton) ap.getSource();
			JPanel panel = (JPanel) button.getParent();
			JPanel panelParent = (JPanel) panel.getParent();
			panelParent.remove(panel);			
			panelParent.revalidate();
			panelParent.repaint();
			((Window) SwingUtilities.getRoot(this)).pack(); 
		} 
	}

	/**
	 * Get the exceptions of this recurring event as 
	 * a sorted array of LocalDates
	 * 
	 * @return	the exceptions of this recurring event as sorted array
	 */
	public LocalDate[] getExceptions() {
		ArrayList<LocalDate> list = new ArrayList<LocalDate>();
		int exNumber = this.getComponentCount() ;// two Box, one buttonPanel
		for (int i = 0; i < exNumber; i++) {
			// formatted date is name of panel
			if (this.getComponent(i).getName() != null) {
				LocalDate ex = LocalDate.parse(this.getComponent(i).getName(), CalendarSetting.getDayDateFormatter());
				if (ex != null) {
					list.add(ex);
				}
			}
		}
		if (list.isEmpty() == false) {
			Collections.sort(list);
			
			LocalDate[] array = new LocalDate[list.size()];
			array = list.toArray(array);
			return array;
		} else {
			return null;
		}
	}
}