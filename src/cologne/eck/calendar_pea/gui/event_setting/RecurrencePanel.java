package cologne.eck.calendar_pea.gui.event_setting;

/*
 * PeaFactory - Production of Password Encryption Archives
 * Copyright (C) 2015  Axel von dem Bruch
 * 
 * This library is free software; you can redistribute it and/or modify it 
 * under the terms of the GNU General Public License as published 
 * by the Free Software Foundation; either version 2 of the License, 
 * or (at your option) any later version.
 * This library is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
 * or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU General Public License for more details.
 * See:  http://www.gnu.org/licenses/gpl-2.0.html
 * You should have received a copy of the GNU General Public License 
 * along with this library.
 */

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Calendar;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.SpinnerDateModel;
import javax.swing.SwingUtilities;
import javax.swing.border.Border;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;
import javax.swing.border.MatteBorder;

import cologne.eck.all_peas.gui.BorderButtonFocusListener;
import cologne.eck.all_peas.gui.IconManager;
import cologne.eck.calendar_pea.control.CalendarSetting;
import cologne.eck.calendar_pea.data.PEvent;
import cologne.eck.calendar_pea.data.RecurringPEvent;
import cologne.eck.tools.TestLog;


@SuppressWarnings("serial")
class RecurrencePanel extends JPanel implements ActionListener {
	
	private EventSetter eventSetter;
	/*
	 * ComboBox with items for frequency: DAILY, WEEKLY, MONTGLY, YEARLY
	 * Not supported: MINUTELY, HOURLY
	 */
	private JComboBox <String> frequencyComboBox;
	
	/*
	 * Interval 1 - ... depends on frequency
	 */
	private JLabel intervalLabel;
	/*
	 * panel to set interval
	 */
	private JPanel intervalPanel;
	
	/*
	 * Panel to display the end of the recurring event: 
	 * This is only shown, if the event is not permanent.
	 * The containing components depend on the event end: 
	 * repetitions or end date-time
	 */
	private JPanel endRecurrencePanel;
	/*
	 * Label to show the number of occurrences
	 */
	private JLabel countLabel;
	/*
	 * Spinner to show and set the end date of the recurrence
	 */
	private JSpinner untilDateSpinner;
	/*
	 * Combo box with items for the end of the event:
	 * permanent (default), repetitions or end date-time
	 */
	private JComboBox <String> endRecurrenceComboBox;	
	/*
	 * The recurring PEvent to set or null if a new event is created
	 */
	private RecurringPEvent recurringEvent;
	/*
	 * The start date, that was set in EventSetter, used to initialize until DatePicker
	 */
	private LocalDate setStartDate;
	
	/*
	 * panel to define exceptions
	 */
	private ExceptionPanel exceptionPanel;
	
	/*
	 * Panel to define single recurrences
	 */
	private JPanel rDatePanelHolder;
	
	/*
	 * parent panel of all byDayPanels:
	 * for daily and weekly only one panel
	 */
	private ByDayRecurrencePanel byDayPanelHolder;

	
	/**
	 * Create a panel to set a recurrence rule
	 * 
	 * @param e					the event to modify or null
	 * @param _setStartDate		the startDate or null
	 * @param _eventSetter		the EventSetter that called this RecurrencePanel
	 */
	protected RecurrencePanel(PEvent e, LocalDate _setStartDate, EventSetter _eventSetter) {
		
		if (e != null && e instanceof RecurringPEvent) {
			recurringEvent = (RecurringPEvent) e;
		} else {
			if (e ==null) {
				//TestLog.ve(XXXX.class, "Missing event for recurrence...", 5);
			}
		}
		this.setStartDate = _setStartDate;
		this.eventSetter = _eventSetter;
		
		this.setLayout(new BoxLayout(this, BoxLayout.PAGE_AXIS));
		Border emptyBorder = new EmptyBorder(5,5,5,5);
		Border lineBorder = new LineBorder(Color.GRAY);
		
		this.setBorder(BorderFactory.createCompoundBorder(
                lineBorder,  emptyBorder));
		
		// the panel that hold all other components except borders
		JPanel mainPanel = new JPanel();
		mainPanel.setLayout(new BoxLayout(mainPanel, BoxLayout.LINE_AXIS));		
		mainPanel.add(Box.createHorizontalStrut(10));
		
		JLabel selectionLabel = new JLabel(CalendarSetting.calTranslate("repeat_every"));
		selectionLabel.setLabelFor(frequencyComboBox);
		mainPanel.add(selectionLabel);
		mainPanel.add(Box.createHorizontalStrut(10));
		
		frequencyComboBox = new JComboBox <String>();
		frequencyComboBox.getAccessibleContext().
	    setAccessibleDescription(
	    	    "Clicking this component causes a specified repetition of the event.");
		frequencyComboBox.addItem(CalendarSetting.calTranslate("never"));
		frequencyComboBox.addItem(CalendarSetting.calTranslate("daily"));
		frequencyComboBox.addItem(CalendarSetting.calTranslate("weekly"));
		frequencyComboBox.addItem(CalendarSetting.calTranslate("monthly"));
		frequencyComboBox.addItem(CalendarSetting.calTranslate("yearly"));
		frequencyComboBox.setMaximumSize(new Dimension(100, 25));			
		
		getEventSetter().setStartDateSpinner(true);
		ChronoUnit freq = null;

		if (recurringEvent == null) { // set default: no
			frequencyComboBox.setSelectedIndex(0);
		} else {
			// set the existing frequency selected if there is one:
			if (recurringEvent.getFrequency() != null) {
				freq = recurringEvent.getFrequency();
				if (freq.equals(ChronoUnit.DAYS)) {
					frequencyComboBox.setSelectedIndex(1);
				} else if (freq.equals(ChronoUnit.WEEKS)) {
					frequencyComboBox.setSelectedIndex(2);
				} else if (freq.equals(ChronoUnit.MONTHS)) {
					frequencyComboBox.setSelectedIndex(3);
					if (recurringEvent.getByDay() != null) {
						getEventSetter().setStartDateSpinner(false);
					}
				} else if (freq.equals(ChronoUnit.YEARS)) {
					frequencyComboBox.setSelectedIndex(4);
					if (recurringEvent.getByDay() != null) {
						getEventSetter().setStartDateSpinner(false);
					}
				} else {
					frequencyComboBox.setSelectedIndex(0);
				}					
			} else {
				frequencyComboBox.setSelectedIndex(0); // only RDATE
			}
		}

		frequencyComboBox.addActionListener(this);
		frequencyComboBox.setActionCommand("period");
		mainPanel.add(frequencyComboBox);
		mainPanel.add(Box.createHorizontalGlue());

		this.add(mainPanel);		

		int frequencyIndex = frequencyComboBox.getSelectedIndex();
		//TestLog.v(XXXX.class, "frequency: " + frequencyIndex);

		if (frequencyIndex == 0) { // no recurrence
			intervalPanel = new JPanel();// empty panel 
		} else {
			intervalPanel = createIntervalPanel();
			this.add(Box.createVerticalStrut(5));
			this.add(intervalPanel);
		}
		if (frequencyIndex > 1) { // weekly, monthly, yearly - not daily!
			byDayPanelHolder = new ByDayRecurrencePanel(this, recurringEvent, freq);

			this.add(byDayPanelHolder);
		}
		if (frequencyIndex > 1) {
			endRecurrencePanel = createEndRecurrencePanel();
			this.add(Box.createVerticalStrut(10));
			this.add(endRecurrencePanel);
			this.add(Box.createVerticalStrut(10));
		}
		rDatePanelHolder = createRDatePanel();
		this.add(rDatePanelHolder);
		if (frequencyIndex > 1) {
			exceptionPanel = new ExceptionPanel(this, recurringEvent, freq);//createExceptionPanel();
			this.add(exceptionPanel);
		}
	}

// FOR TESTING ONLY	
/*	public static void main(String[] args) {

		PCard cal = new PCard("xy", "xxyy", Color.BLUE);
		RecurringPEvent re = new RecurringPEvent(cal);
		re.setDateStart(LocalDateTime.now().plusDays(21));
		re.setFrequency(ChronoUnit.WEEKS);
		re.setCount(5);  
		LocalDate[] ex = {LocalDate.now().plusDays(5), LocalDate.now().plusDays(20)};
		re.setExceptionDates(ex);
		LocalDate[] rd = {LocalDate.now().plusDays(7), LocalDate.now().plusDays(21)};
		re.setRecurrenceDates(rd);
		String[] byDay = {"-2SU", "1MO"};
		re.setByDay(byDay);
		int[] byMonth = {5};
		re.setByMonth(byMonth);
		re.setUntil(LocalDateTime.now().plusDays(10));
		RecurrencePanel rp = new RecurrencePanel(re, LocalDate.now(), null);
		
		
//		RecurrencePanel rp = new RecurrencePanel(null);
		JFrame frame = new JFrame();
		frame.setVisible(true);
		frame.add(rp);
		frame.pack();
	}*/

	@Override
	public void actionPerformed(ActionEvent ap) {
		String command = ap.getActionCommand();

		// check if recurringEvent exists and probably cast to RecurringPEvent:
		if (recurringEvent != null && ! (recurringEvent instanceof RecurringPEvent) ) {
			recurringEvent = RecurringPEvent.castToRecurringPEvent(recurringEvent);
		}

		if (command.equals("period")) { // a new period was selected
			
			getEventSetter().setStartDateSpinner(true);
			
			// reset: otherwise they will return values
			countLabel = null;
			untilDateSpinner = null;
			if (byDayPanelHolder != null) {
				byDayPanelHolder.resetRule();
			}

			int componentNumber = this.getComponentCount();
			// remove all components other than frequency
			if (componentNumber > 1) { //
				// remove all other components:
				for (int i = componentNumber - 1; i >= 1; i--) {
					this.remove(this.getComponent(i));
				}
			}
			int frequencyIndex = frequencyComboBox.getSelectedIndex();
			if (frequencyIndex == 0) { // "never"
				// only RDATE:
				rDatePanelHolder = createRDatePanel();
				this.add(rDatePanelHolder);
				this.revalidate();
				this.repaint();
				((Window) SwingUtilities.getRoot(this)).pack();					

				if (recurringEvent != null) {
					//recurringEvent.setRecurring(false);
				}
				if (this.getRDates() == null) {
					if (recurringEvent != null) {
						recurringEvent.setRecurring(false);
					}
					eventSetter.setRecurring(false);
				}
				return;
			} else  { //day, week, month, year
				
				if (recurringEvent != null) {
					recurringEvent.setRecurring(true);
				}
				eventSetter.setRecurring(true);

				if (intervalPanel.getParent() != this) {
					// create and add
					intervalPanel = createIntervalPanel();
					this.add(Box.createVerticalStrut(5));
					this.add(intervalPanel);
				} else {
					intervalPanel.removeAll();
					intervalPanel.add(createIntervalPanel());
				}

				if (byDayPanelHolder != null) {
					byDayPanelHolder.removeAll();
				}
				ChronoUnit freq = null;
				if (frequencyIndex == 1) {
					freq = ChronoUnit.DAYS;
				} else if (frequencyIndex == 2) {
					freq = ChronoUnit.WEEKS;
				} else if (frequencyIndex == 3) {
					freq = ChronoUnit.MONTHS;
					if (this.getByDayStrings() != null) {
						getEventSetter().setStartDateSpinner(false);
					}
				} else if (frequencyIndex == 4) {
					freq = ChronoUnit.YEARS;
					if (this.getByDayStrings() != null) {
						getEventSetter().setStartDateSpinner(false);
					}
				} 		
				if (freq != null) {
					if (freq.equals(ChronoUnit.WEEKS) || freq.equals(ChronoUnit.MONTHS) || freq.equals(ChronoUnit.YEARS)) {
						byDayPanelHolder = new ByDayRecurrencePanel(this, recurringEvent, freq);
						this.add(byDayPanelHolder);
					}
					endRecurrencePanel = createEndRecurrencePanel();
					this.add(Box.createVerticalStrut(10));
					this.add(endRecurrencePanel);
					this.add(Box.createVerticalStrut(10));
				}

				rDatePanelHolder = createRDatePanel();
				this.add(rDatePanelHolder);

				if (freq != null) {
					exceptionPanel = new ExceptionPanel(this, recurringEvent, freq);//createExceptionPanel();
					this.add(exceptionPanel);
				}
				this.revalidate();
				this.repaint();
				((Window) SwingUtilities.getRoot(this)).pack();	
			}

			// the end of the event was set:
		} else if (command.equals("END")) {
			
			// reset: otherwise they will return values
			countLabel = null;
			untilDateSpinner = null;

			// figure out, which kind of end was set: 
			// permanent (0), repetitions (1) or end date (2)
			int index = endRecurrenceComboBox.getSelectedIndex();

			if (index == 0){ // permanent event was set

				// remove components from endRecurrencePanel:
				int children = endRecurrencePanel.getComponentCount();
				if (children > 1){
					for (int i = children - 1; i >= 1; i--) {
						endRecurrencePanel.remove(endRecurrencePanel.getComponent(i));
					}
				}
				// add nothing, repaint the panel
				endRecurrencePanel.add(Box.createHorizontalGlue());				
				endRecurrencePanel.revalidate();
				endRecurrencePanel.repaint();
				((Window) SwingUtilities.getRoot(this)).pack(); 

			} else if (index == 1) { // number of repetitions was set
				// show the repetitions panel
				showRepetitionsPanel();//recurringEvent);
			} else if (index == 2) {
				// end date was set
				showUntilDatePanel();//recurringEvent);
			}

		} else if (command.equals("datePicker")) {
			// get current set date and convert to LocalDate
			LocalDate startDate = ((java.util.Date) untilDateSpinner.getValue())
					.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
			//TestLog.o(XXXX.class, "startDate: " + startDate);

			// show the DatePicker with local date to start
			LocalDate date = new DatePicker(startDate, true).showDatePicker(this, null);

			// set new until date from DatePicker (convert to Date)
			untilDateSpinner.setValue(java.util.Date.from(date.atStartOfDay(ZoneId.systemDefault()).toInstant()));			

		} else if (command.equals("plusInterval")) {
			if (! intervalLabel.getText().equals("")) {
				int prevInterval = Integer.parseInt(intervalLabel.getText());
				intervalLabel.setText("" + (prevInterval + 1));
			} else {
				intervalLabel.setText("" + 2);
			}
		} else if (command.equals("minusInterval")) {
			if (! intervalLabel.getText().equals("")) {
				int prevInterval = Integer.parseInt(intervalLabel.getText());
				if (prevInterval <= 2) {
					intervalLabel.setText("");
				} else {			
					intervalLabel.setText("" + (prevInterval - 1));		
				}
			}
		} else if (command.equals("plusRepetitions")) {
			countLabel.setText("" + (Integer.parseInt(countLabel.getText()) + 1));
		} else if (command.equals("minusRepetitions")) {
			int value = Integer.parseInt(countLabel.getText());
			if (value > 1) {
				countLabel.setText("" + (value - 1));
			}

		} else if (command.equals("rdate")) {			

			// show the DatePicker 
			LocalDate date = null;
			if (recurringEvent != null) { // start with dtstart
				date = new DatePicker(recurringEvent.getDateStart().toLocalDate(), true).showDatePicker(this, null);
			} else { // start with now
				date = new DatePicker(LocalDate.now(), true).showDatePicker(this, null);
			}

			JPanel datePanel = new JPanel();
			datePanel.setLayout(new BoxLayout(datePanel, BoxLayout.LINE_AXIS));
			datePanel.add(Box.createHorizontalGlue());
			String dateString = CalendarSetting.getDayDateFormatter().format(date);
			datePanel.add(new JLabel(dateString));
			datePanel.setName(dateString);
			datePanel.add(Box.createHorizontalStrut(5));
			JButton removeButton = new JButton("X");//CalendarSetting.calTranslate("remove"));
			removeButton.setToolTipText(CalendarSetting.calTranslate("remove"));
			removeButton.addActionListener(this);
			removeButton.setActionCommand("remove");
			datePanel.add(removeButton);
			
			if (recurringEvent != null) {
				recurringEvent.setRecurring(true);
			}
			eventSetter.setRecurring(true);
			
			rDatePanelHolder.add(datePanel);
			rDatePanelHolder.revalidate();
			rDatePanelHolder.repaint();
			((Window) SwingUtilities.getRoot(this)).pack();

		} else if (command.equals("remove")) { // only rdates

			JButton button = (JButton) ap.getSource();
			JPanel panel = (JPanel) button.getParent();
			JPanel panelParent = (JPanel) panel.getParent();
			panelParent.remove(panel);			
			panelParent.revalidate();
			panelParent.repaint();
			
			if (frequencyComboBox.getSelectedIndex() == 0 // no period
					//&& 	recurringEvent.getExceptionDates() == null 
					&& this.getRDates() == null) { // no recurrence dates
				if (recurringEvent != null) {
					recurringEvent.setRecurring(false);
				}
				eventSetter.setRecurring(false);
			}			
			((Window) SwingUtilities.getRoot(this)).pack(); 			
		} 
	}
	
	//================== Helper Functions =========================
	
	private JPanel createRDatePanel() {
		rDatePanelHolder = new JPanel();
		rDatePanelHolder.setBorder(new MatteBorder(1, 0, 0, 0, Color.GRAY));
		rDatePanelHolder.setLayout(new BoxLayout(rDatePanelHolder, BoxLayout.PAGE_AXIS));
		JPanel rdateButtonPanel = new JPanel();
		rdateButtonPanel.setLayout(new BoxLayout(rdateButtonPanel, BoxLayout.LINE_AXIS));
		JButton rdateButton = new JButton(CalendarSetting.calTranslate("add_single_repetition"));
		rdateButton.addActionListener(this);
		rdateButton.setActionCommand("rdate");
		rdateButtonPanel.add(rdateButton);
		rdateButtonPanel.add(Box.createHorizontalGlue());
		rDatePanelHolder.add(Box.createVerticalStrut(5));
		rDatePanelHolder.add(rdateButtonPanel);
		if (recurringEvent != null && recurringEvent.getRecurrenceDates() != null) {
			LocalDate[] dates = recurringEvent.getRecurrenceDates();
			int rdLength = dates.length;
			for (int i = 0; i < rdLength; i++) {
			JPanel datePanel = new JPanel();
			datePanel.setLayout(new BoxLayout(datePanel, BoxLayout.LINE_AXIS));
			if (i == 0) {
				datePanel.add(new JLabel(CalendarSetting.calTranslate("single_repetitions")));
			}
			datePanel.add(Box.createHorizontalGlue());
			String dateString = CalendarSetting.getDayDateFormatter().format(dates[i]);
			datePanel.add(new JLabel(dateString));
			datePanel.setName(dateString);
			datePanel.add(Box.createHorizontalStrut(5));
			JButton removeButton = new JButton("X");//CalendarSetting.calTranslate("remove"));
			removeButton.setToolTipText(CalendarSetting.calTranslate("remove"));
			removeButton.addActionListener(this);
			removeButton.setActionCommand("remove");
			datePanel.add(removeButton);
			
			rDatePanelHolder.add(datePanel);
			}
		}
		return rDatePanelHolder; 
	}
		
	private JPanel createIntervalPanel() {
		
		int frequencyIndex = frequencyComboBox.getSelectedIndex();
		JPanel intervalPanel = new JPanel();
		intervalPanel.setLayout(new BoxLayout(intervalPanel, BoxLayout.LINE_AXIS));
		
		int interval = 1;
		if (recurringEvent != null) {
			interval = recurringEvent.getInterval();
		}
		String intervalString = "";
		if (interval > 1) {
			intervalString = intervalString + interval;
		}	
		String freq = null;
		if (frequencyIndex == 1) {
			freq = CalendarSetting.calTranslate("days");
		} else if (frequencyIndex == 2) {
			freq = CalendarSetting.calTranslate("weeks");
		} else if (frequencyIndex == 3) {
			freq = CalendarSetting.calTranslate("months");
		} else if (frequencyIndex == 4) {
			freq = CalendarSetting.calTranslate("years");
		} 
		JLabel everyLabel = new JLabel(CalendarSetting.calTranslate("every") + " ");

		intervalLabel = new JLabel(intervalString);
		JLabel intervalFrequencyLabel = new JLabel(" " + freq);

		JButton plusIntervalButton = new JButton("+");
		plusIntervalButton.addActionListener(this);
		plusIntervalButton.setActionCommand("plusInterval");
		JButton minusIntervalButton = new JButton("-");
		minusIntervalButton.addActionListener(this);
		minusIntervalButton.setActionCommand("minusInterval");
		intervalPanel.add(Box.createHorizontalGlue());
		intervalPanel.add(everyLabel);
		intervalPanel.add(intervalLabel);
		intervalPanel.add(intervalFrequencyLabel);
		intervalPanel.add(Box.createHorizontalStrut(10));
		intervalPanel.add(plusIntervalButton);
		intervalPanel.add(minusIntervalButton);
		return intervalPanel;
	}
	
	/**
	 * Show the panel with combo box for 
	 * permanent / number of repetitions / until the date
	 */
	private JPanel createEndRecurrencePanel() {
		
		endRecurrencePanel = new JPanel();
		endRecurrencePanel.setLayout(new BoxLayout(endRecurrencePanel, BoxLayout.LINE_AXIS));
					
		endRecurrenceComboBox = new JComboBox <String> ();
		endRecurrenceComboBox.setMaximumSize(new Dimension(220, 30));

		endRecurrenceComboBox.addItem(CalendarSetting.calTranslate("permanent"));// 
		endRecurrenceComboBox.addItem(CalendarSetting.calTranslate("occurrences"));
		endRecurrenceComboBox.addItem(CalendarSetting.calTranslate("until_date"));  
		
		endRecurrencePanel.add(endRecurrenceComboBox);

		if (recurringEvent != null) {
			if( recurringEvent.getCount() > -1) {
				endRecurrenceComboBox.setSelectedItem(1);
				showRepetitionsPanel();//recurringEvent);
			} else if (recurringEvent.getUntil() != null) {
				endRecurrenceComboBox.setSelectedItem(2);
				showUntilDatePanel();//recurringEvent);
			} else {
				// permanent:
				endRecurrenceComboBox.setSelectedItem(0);
			}			
		} 
		endRecurrenceComboBox.addActionListener(this);
		endRecurrenceComboBox.setActionCommand("END");
		endRecurrencePanel.add(Box.createHorizontalGlue());

		return endRecurrencePanel;
	}
	
	/**
	 * Show the panel to set repetition number (COUNT) with plus
	 * and minus buttons
	 */
	private void showRepetitionsPanel() {
		
		untilDateSpinner = null;
		// remove other components:
		int children = endRecurrencePanel.getComponentCount();
		if (children > 1){
			for (int i = children - 1; i >= 1; i--) {
				endRecurrencePanel.remove(endRecurrencePanel.getComponent(i));
			}
		}				
		// set combo box to repetitions:
		endRecurrenceComboBox.setSelectedIndex(1); // show item repetitions
		// create the label to show number of repetitions
		countLabel = new JLabel("" + 1);
		if (recurringEvent != null) {
			int existingRepetitions = recurringEvent.getCount();
			if (existingRepetitions < 1) {
				existingRepetitions = 1;
			}
			countLabel.setText("" + existingRepetitions);
		}
		countLabel.setFont(new Font(Font.SANS_SERIF, Font.BOLD, 18));
		countLabel.setBackground(Color.WHITE);
		endRecurrencePanel.add(countLabel);
		// add plus and minus buttons to set repetitions
		JButton plusButton = new JButton("+");
		plusButton.addActionListener(this);
		plusButton.setActionCommand("plusRepetitions");
		JButton minusButton = new JButton("-");
		minusButton.addActionListener(this);
		minusButton.setActionCommand("minusRepetitions");
		endRecurrencePanel.add(Box.createHorizontalStrut(10));
		endRecurrencePanel.add(countLabel);
		endRecurrencePanel.add(Box.createHorizontalStrut(10));
		endRecurrencePanel.add(plusButton);
		endRecurrencePanel.add(Box.createHorizontalStrut(5));
		endRecurrencePanel.add(minusButton);
		endRecurrencePanel.add(Box.createHorizontalGlue());
		// repaint the panel
		endRecurrencePanel.revalidate();
		endRecurrencePanel.repaint();
		Window win = ((Window) SwingUtilities.getRoot(this));
		if (win != null) {
			win.pack();
		}
	}
	
	/**
	 * Show the panel to set the until property of the recurring event
	 */
	private void showUntilDatePanel() {
		
		countLabel = null;
		// remove other components:
		int children = endRecurrencePanel.getComponentCount();
		if (children > 1){
			for (int i = children - 1; i >= 1; i--) {
				//TestLog.o(XXXX.class, "remove: " + i);
				endRecurrencePanel.remove(endRecurrencePanel.getComponent(i));
			}
		}				
		// set combo box to end date:
		endRecurrenceComboBox.setSelectedIndex(2); 

		// create spinner to show and set the date
		SpinnerDateModel dateModelEnd = null; 
		if (recurringEvent == null) {
			// set current date
			dateModelEnd = new SpinnerDateModel(
					java.util.Date
					.from(setStartDate.atStartOfDay().atZone(ZoneId.systemDefault())
							.toInstant()), 
					null, null, Calendar.DATE);
		} else {
			// set existing end date
			if (recurringEvent.getUntil() != null) {
				dateModelEnd =  new SpinnerDateModel(
						java.util.Date.from(recurringEvent.getUntil()
								.atZone(ZoneId.systemDefault()).toInstant()),	 
						null, null, Calendar.DATE);
			} else {
				// maybe set by unknown recurrence rule: set one day later
				TestLog.e(RecurrencePanel.class, "Couln't read date end...");
				dateModelEnd = new SpinnerDateModel(
						java.util.Date
						.from(LocalDateTime.now().plusDays(1).atZone(ZoneId.systemDefault())
								.toInstant()), 	 
						null, null, Calendar.DATE);
			}
		}			
		untilDateSpinner = new JSpinner(dateModelEnd);		
		SimpleDateFormat dateModel = new SimpleDateFormat("dd.MM.yy");
		untilDateSpinner.setEditor(new JSpinner.DateEditor(untilDateSpinner, dateModel.toPattern()));
		untilDateSpinner.setMaximumSize(new Dimension(150, 25));
		untilDateSpinner.setMinimumSize(new Dimension(150, 25));
		endRecurrencePanel.add(Box.createHorizontalStrut(10));
		endRecurrencePanel.add(untilDateSpinner);
		endRecurrencePanel.add(Box.createHorizontalGlue());

		// otherwise popup overlaps dialog
//		endRecurrenceComboBox.getUI().setPopupVisible(endRecurrenceComboBox, false);
		
		// add a date-picker button
		JButton datePickerButton = new JButton(IconManager.loadScaledIcon("date-picker.png", 
				"Date Picker to select a date", 30, 25,"Date Picker", "Date Picker to select a date"));
		datePickerButton.setBorder(BorderFactory.createEmptyBorder());
		datePickerButton.addActionListener(this);
		datePickerButton.setActionCommand("datePicker");
		datePickerButton.addFocusListener(BorderButtonFocusListener.getInstance());
	//	datePickerButton.setContentAreaFilled(true);
		endRecurrencePanel.add(Box.createHorizontalStrut(5));
		endRecurrencePanel.add(datePickerButton);

		// repaint the panel
		endRecurrencePanel.revalidate();
		endRecurrencePanel.repaint();
		Window win = ((Window) SwingUtilities.getRoot(this));
		if (win != null) {
			win.pack();
		}
	}
		
	//========================= Getter & Setter ===============================
	
	/**
	 * Get the end date of the repetition as LocalDate
	 * 
	 * @return	the end date of the repetition as LocalDate or null
	 */
	public LocalDateTime getUntil() {
		
		if (untilDateSpinner != null) {
			return ((java.util.Date) untilDateSpinner.getValue()).toInstant()
			      .atZone(ZoneId.systemDefault())
			      .toLocalDateTime();
		} else {
			return null;
		}
	}
	
	/**
	 * Get the number of occurrences, the user has set
	 * 
	 * @return	the number of occurrences
	 */
	public int getCount() {
		if (countLabel != null) {// && repetitionLabel.isShowing() == true){
			return Integer.parseInt(countLabel.getText() );
		} else {
			return -1;
		}
	}
	
	/**
	 * Get the frequency of a RecurringPEvent from the
	 * frequencyComboBox as a ChronoUnit
	 * 
	 * @return the frequency, the user has set, as a ChronoUnit or null for no frequency
	 */
	public ChronoUnit getFrequency() {

		ChronoUnit frequency = null;
		int index = frequencyComboBox.getSelectedIndex();
		switch (index){
		case 0:  frequency = null; 
		//recurringEvent.setRecurring(false); -> RDATE
		break;
		case 1: frequency = ChronoUnit.DAYS;
		break;
		case 2: frequency = ChronoUnit.WEEKS;
		break;
		case 3: frequency = ChronoUnit.MONTHS;
		break;
		case 4: frequency = ChronoUnit.YEARS;				
		break;
		default: frequency = null;
		break;		
		}
		if (frequency == null) {
			//new UnexpectedValueException("frequency", "String", " is null " ).printDescription();
		}
		return frequency;	
	}
	
	/**
	 * Get the new interval of the recurring event's frequency
	 * 
	 * @return	the new interval of the recurring event's frequency
	 */
	public int getInterval() {
		if (intervalLabel != null && intervalLabel.getText() != null && ! intervalLabel.getText().equals("")) {
			return Integer.parseInt(intervalLabel.getText());
		} else {
			//new UnexpectedValueException("intervalLabel text", "String", "can't be read").printStackTraceLines();
			return 1;
		}
	}
	
	/**
	 * Get the exceptions of this recurring event
	 * 
	 * @return	the exceptions of this recurring event
	 */
	public LocalDate[] getExceptions() {
		if (exceptionPanel == null) {
			return null;
		}
		return exceptionPanel.getExceptions();
	}
	
	/**
	 * Get the recurring dates of this event which are not
	 * defined in a recurrence rule
	 * 
	 * @return	the recurring dates of this event
	 */
	public LocalDate[] getRDates() {
		if (rDatePanelHolder == null) {
			TestLog.ve(RecurrencePanel.class, "Missing rDatePanelHolder", 5);
			return null;
		}
		int rDateNumber = rDatePanelHolder.getComponentCount() ;
		
		ArrayList<LocalDate> list = new ArrayList<LocalDate>();
		for (int i = 2; i < rDateNumber; i++) {
			// formatted date is name of panel
			//recurringDates[i] = LocalDate.parse(rDatePanelHolder.getComponent(i).getName(), CalendarSetting.getDayDateFormatter());
			if (rDatePanelHolder.getComponent(i).getName() != null) {
				LocalDate ex = LocalDate.parse(rDatePanelHolder.getComponent(i).getName(), CalendarSetting.getDayDateFormatter());
				if (ex != null) {
					list.add(ex);
				}
			}
		}
		if (list.isEmpty() == false) {
			LocalDate[] array = new LocalDate[list.size()];
			array = list.toArray(array);
			return array;
		} else {
			return null;
		}
	}
	
	/**
	 * Get the strings defining the BYDAY parameter, 
	 * for example SU, WE or -2SU, +3WE
	 * 
	 * @return	the strings defining the BYDAY parameter
	 */
	public String[] getByDayStrings() {
		
		if (byDayPanelHolder == null) { // daily
			return null;
		} else {		
			return byDayPanelHolder.getByDayStrings();
		}
	}
	
	/**
	 * Get the integers defining the BYMONTH parameter. 
	 * Here: only one integer is possible
	 * 
	 * @return	month definition of recurrence rule (here: only one month)
	 */
	public int[] getByMonth() {
		if (byDayPanelHolder == null) { // daily
			return null;
		} else {	
			return byDayPanelHolder.getByMonth();
		}
	}
	
	public void updateStartDate(LocalDate startDate) {
		@SuppressWarnings("unused")
		ChronoUnit frequency = null;
		int index = frequencyComboBox.getSelectedIndex();
		switch (index){
		case 0:  frequency = null; 
		if (recurringEvent != null) {
			recurringEvent.setRecurring(false);			
		}
		if (getRDates() == null) {
			eventSetter.setRecurring(false);
		}
		break;
		case 1: frequency = ChronoUnit.DAYS;
		if (recurringEvent != null) {
			recurringEvent.setRecurring(true);			
		}
		eventSetter.setRecurring(true);
		break;
		case 2: frequency = ChronoUnit.WEEKS;
		byDayPanelHolder.setWeekDayValues();
		if (recurringEvent != null) {
			recurringEvent.setRecurring(true);			
		}
		eventSetter.setRecurring(true);
		break;
		case 3: frequency = ChronoUnit.MONTHS;
		if (recurringEvent != null) {
			recurringEvent.setRecurring(true);			
		}
		eventSetter.setRecurring(true);
		break;
		case 4: frequency = ChronoUnit.YEARS;	
		if (recurringEvent != null) {
			recurringEvent.setRecurring(true);			
		}
		eventSetter.setRecurring(true);
		break;
		default: frequency = null;
		break;		
		}
	}
	
	/**
	 * Get the currently selected index of the frequency combo box:
	 * 
	 * @return	the selected index of the frequency combo box:
	 *			0 is no frequency, 1 is daily, 2 is weekly, 3 is monthly, 4 is yearly
	 */
	public int getFrequencyIndex() {
		return frequencyComboBox.getSelectedIndex();
	}
	
	/**
	 * Get the EventSetter that is associated with this RecurrencePanel
	 * 
	 * @return	the EventSetter that is associated with this RecurrencePanel
	 */
	public EventSetter getEventSetter() {
		return eventSetter;
	}
}
