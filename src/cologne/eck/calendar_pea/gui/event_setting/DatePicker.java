package cologne.eck.calendar_pea.gui.event_setting;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.Month;
import java.time.format.TextStyle;
import java.util.Locale;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSeparator;
import javax.swing.JSpinner;
import javax.swing.JTextField;
import javax.swing.KeyStroke;
import javax.swing.JFormattedTextField;
import javax.swing.SpinnerListModel;
import javax.swing.SpinnerModel;
import javax.swing.SpinnerNumberModel;
import javax.swing.SwingUtilities;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import cologne.eck.all_peas.data.PeaProperties;
import cologne.eck.all_peas.gui.CloseOnEscapeAction;
import cologne.eck.calendar_pea.control.CalendarSetting;
import cologne.eck.tools.TestLog;
import cologne.eck.tools.UnexpectedValueException;


@SuppressWarnings("serial")
public class DatePicker extends JPanel implements ActionListener, ChangeListener {
	
	
	private boolean showOkButton;
	/*
	 * The selected date if there is one, otherwise null
	 */
	private LocalDate selectedDate;
	
	private JSpinner monthSpinner;
	private JSpinner yearSpinner;
	/*
	 * The panel that contains the buttons for days
	 */
	private JPanel daysPanel;
	/*
	 * The buttom that was last selected or null
	 */
	private JButton selectedDayButton;
	/*
	 * Dialog with DatePicker that returns a date time
	 */
	private JDialog dialog;
	/*
	 * Panel that contains an ok button if a date was selected
	 */
	private JPanel okButtonPanel;
	
	/**
	 * Date Picker to select year, month and day
	 * 
	 * @param startDate	the date to start the DatePicker or null (current date)
	 * @param showOkButton	show an OK button to set the date
	 */
	public DatePicker (LocalDate startDate, boolean _showOkButton) {		
		
		if (startDate == null) {
			// take a date to initialize spinners
			// do not set selectedDate
			startDate= LocalDate.now();
		} else {
			selectedDate = startDate;
		}
		this.showOkButton = _showOkButton;
		this.setLayout(new BoxLayout(this, BoxLayout.PAGE_AXIS));		
		// panel for month and year spinners
		JPanel monthAndYearPanel = new JPanel();
		
        JLabel monthLabel = new JLabel(CalendarSetting.calTranslate("month"));
        monthLabel.setLabelFor(monthSpinner);
        monthAndYearPanel.add(monthLabel);
        String[] months = generateMonthStrings();

        SpinnerListModel monthModel =  new SpinnerListModel(months);
        monthSpinner = new JSpinner(monthModel);
        monthSpinner.setFont(new Font(Font.SANS_SERIF, Font.PLAIN, 16));
        monthSpinner.setPreferredSize(new Dimension(120, 30));
		// center the month names
        if (monthSpinner.getEditor() instanceof JSpinner.ListEditor) {
        	JSpinner.ListEditor le = (JSpinner.ListEditor) monthSpinner.getEditor();
        	if (le.getTextField() instanceof JFormattedTextField) {
        		((JFormattedTextField)le.getTextField()).setHorizontalAlignment(JTextField.CENTER);//.setAlignmentX(CENTER_ALIGNMENT);
        	}
        }
        // set month:
        monthSpinner.setValue(months[startDate.getMonthValue() -1]); // month value starts with 1
        monthSpinner.addChangeListener(this);
        monthAndYearPanel.add(monthSpinner);
		monthAndYearPanel.add(Box.createHorizontalStrut(20));
		
        SpinnerModel yearModel = new SpinnerNumberModel(startDate.getYear(), //initial value
                0, //min
                5000, //max
                1);                //step
        JLabel yearLabel = new JLabel(CalendarSetting.calTranslate("year"));
        yearLabel.setLabelFor(yearSpinner);
		yearSpinner = new JSpinner( yearModel);
		yearSpinner.setFont(new Font(Font.SANS_SERIF, Font.PLAIN, 16));
		// remove the dot after thousand and add whitespace to right
		JSpinner.NumberEditor ne = new JSpinner.NumberEditor(yearSpinner,"#");
		yearSpinner.setEditor(ne);
		if (ne.getTextField() instanceof JFormattedTextField) {
			((JFormattedTextField)ne.getTextField()).setHorizontalAlignment(JTextField.CENTER);//.setAlignmentX(CENTER_ALIGNMENT);
		}
		yearSpinner.setPreferredSize(new Dimension(120, 30));
		yearSpinner.addChangeListener(this);		
		monthAndYearPanel.add(yearLabel);
		monthAndYearPanel.add(yearSpinner);
		
		this.add(monthAndYearPanel);
		this.add(Box.createVerticalStrut(10));
		
		// create the panel for day names: Mo, Tu, We...
		JPanel dayNamesPanel = new JPanel();
		dayNamesPanel.setLayout(new GridLayout(1,0));// only fix the rows
		Locale locale = PeaProperties.getLocale();//((ResourceBundle) JREProperties.getBundle()).getLocale();
		if (locale == null) {
			locale = Locale.getDefault();
		}
		for (int i=1; i< 8;i++) {
			// set day names in users language if available
			dayNamesPanel.add(new JLabel(" "  + DayOfWeek.of(i).getDisplayName(TextStyle.SHORT, locale) ));
		}
		this.add(dayNamesPanel);
		this.add(new JSeparator());
		
		// create the panel for days
		daysPanel = new JPanel();
		daysPanel.setLayout(new GridLayout(0,7));// only fix the columns, otherwise they are ignored
		daysPanel.getAccessibleContext().
	    setAccessibleDescription(
	    	    "Contains buttons with day number. Clicking a button selects the day.");
		int numberOfDays = startDate.getMonth().length(startDate.isLeapYear());// 28 - 31
		int dayOfWeek = startDate.withDayOfMonth(1).getDayOfWeek().getValue();// 1- 7
		int selectedIndex = -1;
		if (selectedDate != null) {
			selectedIndex = selectedDate.getDayOfMonth(); // 1-31
		}
		int firstIndex = dayOfWeek -1 ;// 1-7 and index

		for (int i = 0;i < firstIndex; i++) {
			daysPanel.add(new JLabel("  "));
		}
		for (int i = 1; i <= numberOfDays; i++) {
			JButton dateButton = new JButton("" + i);
			dateButton.setMargin(new Insets(1,1,1,1));
			if (i == selectedIndex) {
				dateButton.setBackground(Color.LIGHT_GRAY);
				selectedDayButton = dateButton;
			} else {
				dateButton.setBackground(Color.WHITE);
			}
			dateButton.addActionListener(this);
			dateButton.setActionCommand("" + i);
			daysPanel.add(dateButton);
		}
		this.add(daysPanel);

		// panel for OK button: This disappears if there
		// is no selected date

		okButtonPanel = new JPanel();
		if (selectedDate != null) {
			if (showOkButton == true) {
				// only add button if date exists
				JButton ok = new JButton(PeaProperties.getVmBridge().translate("ok"));
				ok.addActionListener(this);
				ok.setActionCommand("ok");
				okButtonPanel.setLayout(new BoxLayout(okButtonPanel, BoxLayout.LINE_AXIS));
				okButtonPanel.add(Box.createHorizontalGlue());
				okButtonPanel.add(ok);
			}
		}
		this.add(okButtonPanel);

		this.setMinimumSize(new Dimension(300, 50));
	}
	
	/**
	 * Set the day buttons for a given month of a year
	 * 
	 * @param dateTime	the date time to set the day buttons 
	 */
	private void updateDaysPanel(LocalDateTime dateTime) {
		daysPanel.removeAll();
		daysPanel.setLayout(new GridLayout(0,7));// only fix the columns, otherwise they are ignored
		int numberOfDays = dateTime.getMonth().length(dateTime.toLocalDate().isLeapYear());// 28 - 31
		int dayOfWeek = dateTime.withDayOfMonth(1).getDayOfWeek().getValue();// 1- 7
		int firstIndex = dayOfWeek -1;
		for (int i = 0;i < firstIndex;i++) {
			daysPanel.add(new JLabel("  "));
		}
		for (int i = 1; i <= numberOfDays; i++) {
			JButton dateButton = new JButton("" + i);
			//dateButton.setFont(CalendarSetting.getEventButtonFont());
			dateButton.setMargin(new Insets(1,1,1,1));
			dateButton.setBackground(Color.WHITE);
			dateButton.addActionListener(this);
			dateButton.setActionCommand("" + i);
			daysPanel.add(dateButton);
		}
		this.revalidate();
		this.repaint();
	}

/*
//FOR TESTING ONLY	
	public static void main(String[] args) {
		JFrame frame = new JFrame();
		frame.setVisible(true);
//		frame.add(new DatePicker(LocalDate.now().plusYears(1).minusMonths(1)));
		frame.add(new DatePicker(null));
		frame.pack();
	}
*/
	@Override
	public void actionPerformed(ActionEvent ae) {
		String command = ae.getActionCommand();
		
		if (command.equals("ok")){
			
			if (selectedDate == null) {
				new UnexpectedValueException("selectedDate", "LocalDate", "is null").printStackTraceLines();
				return;
			}
			if (dialog != null && dialog.isVisible()) {
				dialog.setVisible(false);
				dialog.dispose();
				dialog = null;
			} 
			return;

		} else {
			// reset last selected dayButton
			if (selectedDayButton != null) {
				selectedDayButton.setBackground(Color.WHITE);
			}
			//TestLog.o(XXXX.class, ae.getActionCommand());
			int month = -1;
			String[] months = generateMonthStrings();
			for (int i = 0;i < 12; i++) {
				if (monthSpinner.getValue().equals(months[i])) {
					month = i;
					break;
				}
			}
			LocalDate date = LocalDate.of(
					(int) yearSpinner.getValue(), 
					Month.of(month + 1), 
					Integer.parseInt(ae.getActionCommand()));
			selectedDate = date;
			selectedDayButton = (JButton) ae.getSource();
			selectedDayButton.setBackground(Color.LIGHT_GRAY);
			//TestLog.o(XXXX.class, "DatePicker: " + selectedDateTime);

			if (okButtonPanel.getComponentCount() > 0) {
				// there is already an OK button
			} else {
				if (showOkButton == true) {
					JButton ok = new JButton("ok");
					ok.addActionListener(this);
					ok.setActionCommand("ok");
					okButtonPanel.setLayout(new BoxLayout(okButtonPanel, BoxLayout.LINE_AXIS));
					okButtonPanel.add(Box.createHorizontalGlue());
					okButtonPanel.add(ok);
					//this.revalidate();
					//this.repaint();
					SwingUtilities.getWindowAncestor(this).pack();
				}
			}
		}
	}

	@Override
	public void stateChanged(ChangeEvent ce) {
		
		// reset selected date
		selectedDate = null;
		int month = -1;
		// get the month names to compare:
		String[] months = generateMonthStrings();
		// check which name was selected
		for (int i = 0;i < 12; i++) {
			if (monthSpinner.getValue().equals(months[i])) {
				month = i;
				break;
			}
		}
		if (ce.getSource().equals(yearSpinner)) {
			//TestLog.o(XXXX.class, yearSpinner.getValue());
			int year = (int)yearSpinner.getValue();
			if (year < 0 || year > 4000) {
				TestLog.e(DatePicker.class, "Invalid year: " + year);
				yearSpinner.setValue(LocalDate.now().getYear());
				return;
			} 
			LocalDate date = LocalDate.of(
					year, 
					Month.of(month + 1), 
					1);
			updateDaysPanel(date.atStartOfDay());
		} else if (ce.getSource().equals(monthSpinner)) {
			//TestLog.o(XXXX.class, monthSpinner.getValue());
			LocalDate date = LocalDate.of(
					(int) yearSpinner.getValue(), 
					Month.of(month + 1), // index starts with 0, month with 1
					1);
			updateDaysPanel(date.atStartOfDay());
		}
		// remove the ok button:
		okButtonPanel.removeAll();
		//SwingUtilities.getWindowAncestor(this).pack();
	}

	/**
	 * Generate month names in local language
	 * 
	 * @return	the month names as an array of Strings
	 */
	public static String[] generateMonthStrings() {
		String[] months = new java.text.DateFormatSymbols().getMonths();
        int lastMonthIndex = months.length - 1;
 
        if (months[lastMonthIndex] == null
           || months[lastMonthIndex].length() <= 0) { //last item empty
            String[] monthStrings = new String[lastMonthIndex];
            System.arraycopy(months, 0,
                             monthStrings, 0, lastMonthIndex);
            months = monthStrings;
        } 
        return months;
	}
	
	/**
	 * Show a date picker that returns a selected date
	 * 
	 * @param owner		owner of the dialog: JFrame
	 * @param title		the title or null (title = Date)
	 * 
	 * @return	the selected date as LocalDate
	 */
	public LocalDate showDatePicker(Component owner, String title) {
		
		if (owner instanceof JFrame) {
			dialog = new JDialog((JFrame)owner);
		} else {
			dialog = new JDialog();
		}
//		dialog.setUndecorated(true);
		dialog.setAlwaysOnTop(true);
		if (title == null) {
			dialog.setTitle(CalendarSetting.calTranslate("date"));
		} else {
			dialog.setTitle(title);
		}
		// Avoid keyboard trap: Close with ESC
		dialog.getRootPane().getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW)
    		.put(KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), "CLOSE_ON_ESCAPE");
		dialog.getRootPane().getActionMap().put("CLOSE_ON_ESCAPE", new CloseOnEscapeAction() );
		dialog.add(this);//new DatePicker(dateTime));
		dialog.setLocationRelativeTo(owner);
		dialog.pack();
		// must be modal to get a date
		dialog.setModal(true);
		dialog.setVisible(true);		
		
		return selectedDate;
	}

	/**
	 * Get the selected date from the picker
	 * 
	 * @return	the selected date or null
	 */
	public LocalDate getSelectedDate() {
		return selectedDate;
	}
}
