package cologne.eck.calendar_pea.gui.event_setting;

/*
 * PeaFactory - Production of Password Encryption Archives
 * Copyright (C) 2015  Axel von dem Bruch
 * 
 * This library is free software; you can redistribute it and/or modify it 
 * under the terms of the GNU General Public License as published 
 * by the Free Software Foundation; either version 2 of the License, 
 * or (at your option) any later version.
 * This library is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
 * or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU General Public License for more details.
 * See:  http://www.gnu.org/licenses/gpl-2.0.html
 * You should have received a copy of the GNU General Public License 
 * along with this library.
 */

import java.awt.Dimension;
import java.time.LocalTime;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JFormattedTextField;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.JTextField;
import javax.swing.SpinnerModel;
import javax.swing.SpinnerNumberModel;
import javax.swing.text.DefaultFormatter;

import cologne.eck.calendar_pea.control.CalendarSetting;

@SuppressWarnings("serial")
class TimePicker extends JPanel {
	
	private JSpinner hourSpinner;
	private JSpinner minuteSpinner;


	/**
	 * TimePicker to select hours and minutes
	 * 
	 * @param time	the LocalTime to initialize the picker
	 */
	protected TimePicker(LocalTime time) {

		this.setLayout(new BoxLayout(this,BoxLayout.LINE_AXIS));
		// set default values
		int hour = 12;
		int minute = 0;
		if (time != null) {
			// for given time: set values
			hour = time.getHour();
			minute = time.getMinute();
		}
		this.add(Box.createHorizontalStrut(10));
		
        SpinnerModel hourModel = new SpinnerNumberModel(hour, //initial value
                0, //min
                23, //max
                1); //step
        JLabel hourLabel = new JLabel(CalendarSetting.calTranslate("hour"));
        hourLabel.setLabelFor(hourSpinner);
        this.add(hourLabel);
        hourSpinner = new JSpinner(hourModel);
        
        JTextField textField = ((JSpinner.NumberEditor)hourSpinner.getEditor()).getTextField();
		if (textField instanceof JFormattedTextField) {
			// enable for ChangeListener:
			DefaultFormatter formatter = (DefaultFormatter) ((JFormattedTextField) textField).getFormatter();
		    formatter.setCommitsOnValidEdit(true);
			
			((JFormattedTextField)textField)
			 	.setHorizontalAlignment(JTextField.CENTER);//.setAlignmentX(CENTER_ALIGNMENT);
		}
		hourSpinner.setMaximumSize(new Dimension(50, 30));
		hourSpinner.setMinimumSize(new Dimension(50, 30));
		this.add(hourSpinner);		
		this.add(Box.createHorizontalStrut(5));
		
        SpinnerModel minuteModel = new SpinnerNumberModel(minute, //initial value
                0, //min
                59, //max
                1); //step
        JLabel minuteLabel = new JLabel(CalendarSetting.calTranslate("minute"));
        minuteLabel.setLabelFor(minuteSpinner);
        this.add(minuteLabel);
        minuteSpinner = new JSpinner(minuteModel);
        textField = ((JSpinner.NumberEditor)minuteSpinner.getEditor()).getTextField();
		if (textField instanceof JFormattedTextField) {
			((JFormattedTextField)textField)
			 	.setHorizontalAlignment(JTextField.CENTER);//.setAlignmentX(CENTER_ALIGNMENT);
		}
		minuteSpinner.setMaximumSize(new Dimension(50, 30));
		minuteSpinner.setMinimumSize(new Dimension(50, 30));
		this.add(minuteSpinner);		
	}
	
	public JSpinner getHourSpinner() {
		return this.hourSpinner;
	}
	
	/**
	 * Get the current set time in hours and minutes. 
	 * If the user didn't set the time, the default time
	 * 12:00 is returned
	 * 
	 * @return	the time set in this TimePicker in LocalTime
	 */
	public LocalTime getTime() {
		return LocalTime.of((int) hourSpinner.getValue(), (int) minuteSpinner.getValue());
	}
	
	/**
	 * Set the current set time in hours and minutes. 
	 * If the user didn't set the time, the default time
	 * 12:00 is returned
	 * 
	 * @param time	the time set in this TimePicker in LocalTime
	 */
	public void setTime(LocalTime time) {
		hourSpinner.setValue(time.getHour());
		minuteSpinner.setValue(time.getMinute());
	}
}
