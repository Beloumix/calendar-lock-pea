package cologne.eck.calendar_pea.gui.event_setting;

import java.io.File;
import java.time.LocalDate;
import java.awt.Point;
import java.awt.Color;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.KeyStroke;
import javax.swing.border.LineBorder;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;

import cologne.eck.all_peas.data.PeaProperties;
import cologne.eck.all_peas.gui.CloseOnEscapeAction;
import cologne.eck.all_peas.gui.PeaFileChooser;
import cologne.eck.all_peas.vm_specific.JREProperties;
import cologne.eck.calendar_pea.control.CalendarSetting;


@SuppressWarnings("serial")
public class AtRestDialog extends JDialog implements ActionListener {
	
	
	private static boolean deleteDates = false;
	
	private static LocalDate date = null;
	
	private static String fileName = null;
	
	private JLabel dateLabel = null;
	
	private JLabel folderInfoLabel = null;
	
	private JPanel folderPanel = null;
	
	private DatePicker picker = null;
	
	private JLabel folderLabel = null;
	
	public AtRestDialog() {
		
		// reset:
		deleteDates = false;
		date = null;
		fileName = null;
		
		this.setTitle(CalendarSetting.calTranslate("encryption_at_rest")); 
		this.setModal(true);
		
		// Avoid keyboard trap: Close with ESC
		this.getRootPane().getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW)
    		.put(KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), "CLOSE_ON_ESCAPE");
		this.getRootPane().getActionMap().put("CLOSE_ON_ESCAPE", new CloseOnEscapeAction() );
		
		JPanel contentPane = (JPanel) this.getContentPane();
		contentPane.setLayout(new BoxLayout(contentPane, BoxLayout.PAGE_AXIS));
		
		String dateText = CalendarSetting.calTranslate("select_date_to_separate"); 
		
		String dateHint =  CalendarSetting.calTranslate("note_repeat_dates_remain"); 
		String decisionText = CalendarSetting.calTranslate("decide_delete_or_store") + ":"; 
		String folderText = CalendarSetting.calTranslate("select_folder_to_store") + ":"; 
		
		contentPane.add(Box.createVerticalStrut(10));
		
		JPanel optionInfoPanel = new JPanel();
		optionInfoPanel.setLayout(new BoxLayout(optionInfoPanel, BoxLayout.LINE_AXIS));
		optionInfoPanel.add(Box.createHorizontalStrut(10));
		optionInfoPanel.add(new JLabel(decisionText));
		optionInfoPanel.add(Box.createHorizontalGlue());
		contentPane.add(optionInfoPanel);
		
		JPanel atRestPanel = new JPanel();
		atRestPanel.setLayout(new BoxLayout(atRestPanel, BoxLayout.LINE_AXIS));
		atRestPanel.add(Box.createHorizontalStrut(20));
		JRadioButton atRest = new JRadioButton(CalendarSetting.calTranslate("encryption_at_rest")); 
		atRest.getAccessibleContext().
	    setAccessibleDescription(
	    	    "Clicking this component selects the encryption at rest mode.");
		atRest.addActionListener(this);
		atRest.setActionCommand("atRest");
		atRestPanel.add(atRest);
		atRestPanel.add(Box.createHorizontalGlue());
		contentPane.add(atRestPanel);
		
		JPanel deletePanel = new JPanel();
		deletePanel.setLayout(new BoxLayout(deletePanel, BoxLayout.LINE_AXIS));
		deletePanel.add(Box.createHorizontalStrut(20));
		JRadioButton delete = new JRadioButton(PeaProperties.getVmBridge().translate("delete")); 
		delete.getAccessibleContext().
	    setAccessibleDescription(
	    	    "Clicking this component causes selects the delete mode: Old events will be deleted.");
		delete.addActionListener(this);
		delete.setActionCommand("delete");
		deletePanel.add(delete);
		deletePanel.add(Box.createHorizontalGlue());
		contentPane.add(deletePanel);

	    ButtonGroup group = new ButtonGroup();
	    group.add(atRest);
	    group.add(delete);
	    atRest.setSelected(true);
	    
	    contentPane.add(Box.createVerticalStrut(20));
	    
		
		JPanel dateInfoPanel = new JPanel();
		dateInfoPanel.setLayout(new BoxLayout(dateInfoPanel, BoxLayout.LINE_AXIS));
		dateInfoPanel.add(Box.createHorizontalStrut(10));
		dateLabel = new JLabel(dateText);
		dateInfoPanel.add(dateLabel);
		dateInfoPanel.add(Box.createHorizontalGlue());
		contentPane.add(dateInfoPanel);
		
		JPanel dateHintPanel = new JPanel();
		dateHintPanel.setLayout(new BoxLayout(dateHintPanel, BoxLayout.LINE_AXIS));
		dateHintPanel.add(Box.createHorizontalStrut(20));
		dateHintPanel.add(new JLabel(dateHint));
		dateHintPanel.add(Box.createHorizontalGlue());
		contentPane.add(dateHintPanel);			
		
		picker = new DatePicker(null, false); // date now, no OK button
		contentPane.add(picker);		
		
		contentPane.add(Box.createVerticalStrut(20));
		
		
		folderPanel = new JPanel();
		folderPanel.setLayout(new BoxLayout(folderPanel, BoxLayout.LINE_AXIS));
		folderPanel.add(Box.createHorizontalStrut(10));
		folderInfoLabel = new JLabel(folderText);		
		folderPanel.add(folderInfoLabel);		
		folderPanel.add(Box.createHorizontalStrut(10));
		JButton openFolderButton = new JButton(PeaProperties.getVmBridge().translate("open_directory")); 
		openFolderButton.addActionListener(this);
		openFolderButton.setActionCommand("openFolder");
		folderInfoLabel.setLabelFor(openFolderButton);
		folderPanel.add(openFolderButton);
		folderPanel.add(Box.createHorizontalGlue());
		contentPane.add(folderPanel);
		
		contentPane.add(Box.createVerticalStrut(10));
		
		JPanel folderShowPanel = new JPanel();
		folderShowPanel.setLayout(new BoxLayout(folderShowPanel, BoxLayout.LINE_AXIS));
		folderShowPanel.add(Box.createHorizontalStrut(20));
		folderLabel = new JLabel();
		folderShowPanel.add(folderLabel);
		folderShowPanel.add(Box.createHorizontalGlue());
		contentPane.add(folderShowPanel);
		
		contentPane.add(Box.createVerticalStrut(20));
		
		
		JPanel setButtonPanel = new JPanel();
		setButtonPanel.setLayout(new BoxLayout(setButtonPanel, BoxLayout.LINE_AXIS));
		setButtonPanel.add(Box.createHorizontalGlue());
		JButton setButton = new JButton(PeaProperties.getVmBridge().translate("ok"));  
		setButton.addActionListener(this);
		setButton.setActionCommand("set");
		setButtonPanel.add(setButton);
		setButtonPanel.add(Box.createHorizontalStrut(10));
		contentPane.add(setButtonPanel);
		contentPane.add(Box.createVerticalStrut(10));

		
		this.pack();
		//this.setSize(new Dimension(500, 600));
	    if (JREProperties.getMainWindow() != null) {
	    	this.setLocation(JREProperties.getMainWindow().getLocationOnScreen());
	    } else {
	    	this.setLocation(new Point(100,100));
	    }
	    
	}

	/**
	 * Check whether the selected events and todos are to be deleted
	 * or stored at rest
	 * 
	 * @return	true: delete events and todos, false: encrypt at rest
	 */
	public static boolean isDelete() {
		return AtRestDialog.deleteDates;
	}
	/**
	 * Get the date until all events and todos are to be separated
	 * 
	 * @return	the date until events and todos are to be separated
	 */
	public static LocalDate getDateUntil() {
		return AtRestDialog.date;
	}
	/**
	 * Get the file where all data are to be stored
	 * 
	 * @return	the file to store the data at rest or null
	 */
	public static String getFileName() {
		return AtRestDialog.fileName;
	}

	@Override
	public void actionPerformed(ActionEvent ae) {
		String command = ae.getActionCommand();
		if (command.equals("delete")){
			folderPanel.setVisible(false);
			folderLabel.setVisible(false);
			deleteDates = true;
		} else if (command.equals("atRest")){
			folderPanel.setVisible(true);
			folderLabel.setVisible(true);
			deleteDates = false;
			this.pack();
			
		}   else if (command.equals("openFolder")){
			LocalDate date = picker.getSelectedDate();
			if (date == null) {
				dateLabel.setBorder(new LineBorder(Color.RED, 3));
				return;
			} else {
				dateLabel.setBorder(null);
			}
			//TestLog.o(getClass(), "Open folder...");
			PeaFileChooser pfc = new PeaFileChooser(this);
			pfc.setFileSelectionMode(PeaFileChooser.DIRECTORIES_ONLY);
			int chooserResult = pfc.showOpenDialog(this);
			if (chooserResult != PeaFileChooser.APPROVE_OPTION) {
				return;
			}
			File folder = pfc.getSelectedFile();
			if (folder != null) {
				String dir = folder.getAbsolutePath() + File.separator + date.toString() + "-at-rest.ics";
				folderLabel.setText(dir);
				folderInfoLabel.setBorder(null);
				this.pack();
			} else {
				folderLabel.setText(null);
				folderInfoLabel.setBorder(new LineBorder(Color.RED, 3));
			}
		}  else if (command.equals("set")) {
			LocalDate selectedDate = picker.getSelectedDate();
			if (selectedDate == null) {
				dateLabel.setBorder(new LineBorder(Color.RED, 3));
				return;
			} else {
				dateLabel.setBorder(null);
				AtRestDialog.date = selectedDate;
			}
			if (deleteDates == false) {
				if (folderLabel.getText() == null || folderLabel.getText().equals("")) {
					folderInfoLabel.setBorder(new LineBorder(Color.RED, 3));
					return;
				} else {
					folderInfoLabel.setBorder(null);
					AtRestDialog.fileName = folderLabel.getText();
				}
			}			
			//TestLog.o(getClass(), "Date: " + picker.getSelectedDate());
			//TestLog.o(getClass(), "File: " + folderLabel.getText());
			this.dispose();
		}  
	}
}
