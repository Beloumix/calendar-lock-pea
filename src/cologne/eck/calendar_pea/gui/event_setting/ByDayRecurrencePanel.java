package cologne.eck.calendar_pea.gui.event_setting;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.YearMonth;
import java.time.format.TextStyle;
import java.time.temporal.ChronoUnit;
import java.time.temporal.TemporalAdjusters;
import java.util.ArrayList;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.ComboBoxEditor;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;
import javax.swing.border.LineBorder;

import cologne.eck.all_peas.data.PeaProperties;
import cologne.eck.all_peas.gui.PeaDialog;
import cologne.eck.calendar_pea.control.CalendarSetting;
import cologne.eck.calendar_pea.data.RecurringPEvent;
import cologne.eck.tools.TestLog;
import cologne.eck.tools.UnexpectedValueException;


@SuppressWarnings("serial")
class ByDayRecurrencePanel extends JPanel 
	implements ActionListener {
	
	private RecurrencePanel recurrencePanel;
	
	private RecurringPEvent recurringEvent;
	
	/*
	 * Local month names: January...
	 */
	private JComboBox<String> monthNameComboBox;
	
	/*
	 * Hold day check boxes, Monday at index 0, Tuesday at index 1...
	 */
	private JPanel dayCheckBoxPanel;
	
	/*
	 * list of panels that hold BYDAY parameters:
	 * several panels (BYDAY parameters) are possible
	 */
	private ArrayList<JPanel> byDayPanels = new ArrayList<JPanel>();	
	
	protected ByDayRecurrencePanel(RecurrencePanel _recurrencePanel, RecurringPEvent _recurringEvent, ChronoUnit freq) {
		
		this.recurrencePanel = _recurrencePanel;
		this.recurringEvent = _recurringEvent;
		
		if (recurringEvent != null &&
				recurringEvent.getByDay() != null ) {

			this.setLayout(new BoxLayout(this, BoxLayout.PAGE_AXIS));
			this.add(Box.createVerticalStrut(10));

			if ((freq != null && (freq.equals(ChronoUnit.MONTHS))) || (freq != null && freq.equals(ChronoUnit.YEARS))){
				
				JPanel byDayButtonPanel = new JPanel();
				byDayButtonPanel.setLayout(new BoxLayout(byDayButtonPanel, BoxLayout.LINE_AXIS));
				JButton byDayButton = new JButton(CalendarSetting.calTranslate("define_weekdays"));
				byDayButton.getAccessibleContext().
			    setAccessibleDescription(
			    	    "Clicking this component allows to select specific days.");
				byDayButton.addActionListener(this);
				byDayButton.setActionCommand("addByDayPanel");
				byDayButtonPanel.add(byDayButton);
				byDayButtonPanel.add(Box.createHorizontalGlue());
				this.add(byDayButtonPanel);
				String[] byDays = recurringEvent.getByDay();
				String[] startDateMatchingRules = getStartDateMatchingRules();
				// check for start date matching rule:
				int ruleIndexMatch = -1;
				for (int ruleIndex = 0; ruleIndex < byDays.length; ruleIndex++) {
					if (byDays[ruleIndex].equals(startDateMatchingRules[0]) 
							|| byDays[ruleIndex].equals(startDateMatchingRules[1])) {
						ruleIndexMatch = ruleIndex;
						break;
					} 	
				}
				if (ruleIndexMatch != -1) { // there is a rule, that matches the start date
					// add this rule at first:
					this.add(createMonthlyYearlyByDayRulePanel(byDays, ruleIndexMatch, true));
				} else {
					// set rule for start date
					if (startDateMatchingRules[1].startsWith("-1")) { // last weekday
						// set definition of last XXXday
						this.add(createMonthlyYearlyByDayRulePanel(startDateMatchingRules, 1, true));
					} else {
						// set definition of first XXXday
						this.add(createMonthlyYearlyByDayRulePanel(startDateMatchingRules, 0, true));
					}
				}
				// several BYDAY parameters possible: add other rules
				for (int i =0; i < byDays.length; i++) {
					if (i != ruleIndexMatch) {
						this.add(createMonthlyYearlyByDayRulePanel(byDays, i, false));
					}
				}
				// set startDate spinner editable false
				recurrencePanel.getEventSetter().setStartDateSpinner(false);
				
			// NOTE: BYDAY for days is not called
			} else if (freq != null && (freq.equals(ChronoUnit.WEEKS) || freq.equals(ChronoUnit.DAYS))){
				
				recurrencePanel.getEventSetter().setStartDateSpinner(true);
				JPanel byDayButtonPanel = new JPanel();
				byDayButtonPanel.setLayout(new BoxLayout(byDayButtonPanel, BoxLayout.LINE_AXIS));
				JCheckBox byDayButton = new JCheckBox(CalendarSetting.calTranslate("define_weekdays"));
				byDayButton.addActionListener(this);
				byDayButton.getAccessibleContext().
			    setAccessibleDescription(
			    	    "Clicking this component allows to select specific days.");
				byDayButton.setActionCommand("addByDayPanel");
				byDayButtonPanel.add(byDayButton);
				byDayButtonPanel.add(Box.createHorizontalGlue());
				this.add(byDayButtonPanel);
				String[] byDays = recurringEvent.getByDay();
				this.add(createDailyWeeklyByDayRulePanel(byDays));
				setWeekDayValues(); // set current start date
			}
			this.add(Box.createVerticalStrut(10));
		} else {
			
			this.setLayout(new BoxLayout(this, BoxLayout.PAGE_AXIS));
			JPanel byDayButtonPanel = new JPanel();
			byDayButtonPanel.setLayout(new BoxLayout(byDayButtonPanel, BoxLayout.LINE_AXIS));
			JButton byDayButton = new JButton(CalendarSetting.calTranslate("define_weekdays"));
			byDayButton.addActionListener(this);
			byDayButton.getAccessibleContext().
		    setAccessibleDescription(
		    	    "Clicking this component allows to select specific days.");
			byDayButton.setActionCommand("addByDayPanel");
			byDayButtonPanel.add(byDayButton);
			byDayButtonPanel.add(Box.createHorizontalGlue());
			this.add(byDayButtonPanel);			
		}
	}
	
	/**
	 * Create a panel to show and set a daily or weekly BYDAY rule
	 * 
	 * @param byDayStrings	String array of rules or null
	 * 
	 * @return	the panel to show and set the rule
	 */
	private JPanel createDailyWeeklyByDayRulePanel(String[] byDayStrings) {

		JPanel bdp = null;

		int freqIndex = recurrencePanel.getFrequencyIndex();
		if (freqIndex < 0) { // no frequency
			return null;
		} else {

			bdp = new JPanel();
			bdp.setName("byDayWeeklyRulePanel");
			bdp.setLayout(new BoxLayout(bdp, BoxLayout.LINE_AXIS));

			if (dayCheckBoxPanel == null  && (freqIndex == 2 || freqIndex == 1)) { // weekly or daily
				// no integer or +/-, only weekdays
				bdp.add(new JLabel(" → "));//CalendarSetting.calTranslate("on")));
				bdp.add(Box.createHorizontalStrut(5));
				
				dayCheckBoxPanel = new JPanel();
				dayCheckBoxPanel.setLayout(new BoxLayout(dayCheckBoxPanel, BoxLayout.LINE_AXIS));
				dayCheckBoxPanel.add(new JCheckBox(DayOfWeek.MONDAY.getDisplayName(TextStyle.FULL, PeaProperties.getLocale())));
				dayCheckBoxPanel.add(new JCheckBox(DayOfWeek.TUESDAY.getDisplayName(TextStyle.FULL, PeaProperties.getLocale())));
				dayCheckBoxPanel.add(new JCheckBox(DayOfWeek.WEDNESDAY.getDisplayName(TextStyle.FULL, PeaProperties.getLocale())));
				dayCheckBoxPanel.add(new JCheckBox(DayOfWeek.THURSDAY.getDisplayName(TextStyle.FULL, PeaProperties.getLocale())));
				dayCheckBoxPanel.add(new JCheckBox(DayOfWeek.FRIDAY.getDisplayName(TextStyle.FULL, PeaProperties.getLocale())));
				dayCheckBoxPanel.add(new JCheckBox(DayOfWeek.SATURDAY.getDisplayName(TextStyle.FULL, PeaProperties.getLocale())));
				dayCheckBoxPanel.add(new JCheckBox(DayOfWeek.SUNDAY.getDisplayName(TextStyle.FULL, PeaProperties.getLocale())));
				dayCheckBoxPanel.add(Box.createHorizontalGlue());
				bdp.add(dayCheckBoxPanel);
				if (byDayStrings != null) {
					// set values
					for (int dIndex = 0; dIndex < byDayStrings.length; dIndex++) {
						int dayIndex = CalendarSetting.getDayOfWeekValue(byDayStrings[dIndex]) - 1;// index, not value
						if (dayIndex >= 0) {
							((JCheckBox)dayCheckBoxPanel.getComponent(dayIndex)).setSelected(true);
						}
					}

				} else {
					// get value from date start
					if (recurringEvent != null) {
						//setWeekDayValues(recurringEvent.getDateStart().toLocalDate());//, frequency);
					}
				}
			} else {
			}
		} 
		if (bdp != null) {
			bdp.add(Box.createHorizontalGlue());
			byDayPanels.add(bdp);
		}
		return bdp;
	}
	
	/**
	 * Create a panel to show and set a monthly or yearly BYDAY rule
	 * 
	 * @param byDayStrings			all BYDAY rules or null
	 * @param byDayIndex			the index of the rules to show
	 * @param startDateMatching		true, if this rule matches the start date
	 * 								and is therefore not editable
	 * 
	 * @return	the panel to show and set the rule
	 */
	private JPanel createMonthlyYearlyByDayRulePanel(String[] byDayStrings, int byDayIndex, boolean startDateMatching) {

		JPanel bdp = null;
		int freqIndex = recurrencePanel.getFrequencyIndex();
		if (freqIndex < 0) { // no frequency
			return null;
		} else {

			bdp = new JPanel();
			bdp.setName("byDayMonthlyYearlyRulePanel");
			bdp.setLayout(new BoxLayout(bdp, BoxLayout.LINE_AXIS));

			if (freqIndex == 4 || freqIndex == 3) { // yearly or monthly
				
				String byDayString = null;
				if (byDayStrings != null && (freqIndex == 4 || freqIndex == 3)){ 
					// month and year: one rule for one day
					byDayString = byDayStrings[byDayIndex];
				}
				if (byDayPanels == null) {
					new UnexpectedValueException("byDayPanels", "ArrayList<JPanels>", "is null").printDescription();
					return null;
				}
				if (byDayPanels.size() == 0) {
					bdp.add(new JLabel(" → "));//CalendarSetting.calTranslate("on_the")));
				} else {
					bdp.add(new JLabel(" + "));//CalendarSetting.calTranslate("and_on_the")));
				}
				bdp.add(Box.createHorizontalStrut(5));
				
				JComboBox <String> byDayIntegerComboBox = new JComboBox <String>();
				byDayIntegerComboBox.addItem(CalendarSetting.calTranslate("first"));
				byDayIntegerComboBox.addItem(CalendarSetting.calTranslate("second"));
				byDayIntegerComboBox.addItem(CalendarSetting.calTranslate("third"));
				byDayIntegerComboBox.addItem(CalendarSetting.calTranslate("fourth"));
				byDayIntegerComboBox.addItem(CalendarSetting.calTranslate("last"));
				byDayIntegerComboBox.addItem(CalendarSetting.calTranslate("penultimate"));
				byDayIntegerComboBox.addItem(CalendarSetting.calTranslate("pre_penultimate"));
				byDayIntegerComboBox.addItem(CalendarSetting.calTranslate("pre_pre_penultimate"));
				byDayIntegerComboBox.setMaximumSize(new Dimension(100, 25));
				// set value
				if (byDayString != null) { // && byDayPanels.size() > 0) {
					
					if (byDayString.startsWith("+1") || byDayString.startsWith("1")) {
						byDayIntegerComboBox.setSelectedIndex(0);
					} else if (byDayString.startsWith("+2") || byDayString.startsWith("2")) {
						byDayIntegerComboBox.setSelectedIndex(1);
					} else if (byDayString.startsWith("+3") || byDayString.startsWith("3")) {
						byDayIntegerComboBox.setSelectedIndex(2);
					} else if (byDayString.startsWith("+4") || byDayString.startsWith("4")) {
						byDayIntegerComboBox.setSelectedIndex(3);
					} else if (byDayString.startsWith("-1")) {
						byDayIntegerComboBox.setSelectedIndex(4);
					} else if (byDayString.startsWith("-2")) {
						byDayIntegerComboBox.setSelectedIndex(5);
					} else if (byDayString.startsWith("-3") ) {
						byDayIntegerComboBox.setSelectedIndex(6);
					} else if (byDayString.startsWith("-4") ) {
						byDayIntegerComboBox.setSelectedIndex(7);
					} else {
						byDayIntegerComboBox.setSelectedIndex(0);
					} 
				} 

				bdp.add(byDayIntegerComboBox);
				bdp.add(Box.createHorizontalStrut(5));

				JComboBox<String> dayNameComboBox = new JComboBox<String>();
				dayNameComboBox.addItem(DayOfWeek.MONDAY.getDisplayName(TextStyle.FULL, PeaProperties.getLocale()));
				dayNameComboBox.addItem(DayOfWeek.TUESDAY.getDisplayName(TextStyle.FULL, PeaProperties.getLocale()));
				dayNameComboBox.addItem(DayOfWeek.WEDNESDAY.getDisplayName(TextStyle.FULL, PeaProperties.getLocale()));
				dayNameComboBox.addItem(DayOfWeek.THURSDAY.getDisplayName(TextStyle.FULL, PeaProperties.getLocale()));
				dayNameComboBox.addItem(DayOfWeek.FRIDAY.getDisplayName(TextStyle.FULL, PeaProperties.getLocale()));
				dayNameComboBox.addItem(DayOfWeek.SATURDAY.getDisplayName(TextStyle.FULL, PeaProperties.getLocale()));
				dayNameComboBox.addItem(DayOfWeek.SUNDAY.getDisplayName(TextStyle.FULL, PeaProperties.getLocale()));

				dayNameComboBox.setMaximumSize(new Dimension(100, 25));
				if (byDayString != null) {
					// TODO: several weekdays
					//dayNameComboBox.setPopupVisible(true);
					if (byDayString.contains("MO")) {
						dayNameComboBox.setSelectedIndex(0);
					} else if (byDayString.contains("TU")) {
						dayNameComboBox.setSelectedIndex(1);
					} else if (byDayString.contains("WE")) {
						dayNameComboBox.setSelectedIndex(2);
					} else if (byDayString.contains("TH")) {
						dayNameComboBox.setSelectedIndex(3);
					} else if (byDayString.contains("FR")) {
						dayNameComboBox.setSelectedIndex(4);
					} else if (byDayString.contains("SA")) {
						dayNameComboBox.setSelectedIndex(5);
					} else if (byDayString.contains("SU")) {
						dayNameComboBox.setSelectedIndex(6);
					} 
				}

				bdp.add(dayNameComboBox);
				bdp.add(Box.createHorizontalStrut(5));

				if (freqIndex == 4 && byDayPanels.size() == 0) { // yearly,only once
					bdp.add(new JLabel(CalendarSetting.calTranslate("in_month")));
					bdp.add(Box.createHorizontalStrut(5));
					String[] months = DatePicker.generateMonthStrings();
					//recurrencePanel.getEventSetter().getStartDate().getMonthValue();//
					monthNameComboBox = new JComboBox<String>();
					for (int i = 0; i < 12; i++) {
						monthNameComboBox.addItem(months[i]);
					}
					monthNameComboBox.setMaximumSize(new Dimension(100, 25));
					if (recurringEvent != null && recurringEvent.getByMonth() != null) {
						monthNameComboBox.setSelectedIndex(recurringEvent.getByMonth()[0] - 1);// index

					}
					bdp.add(monthNameComboBox);
				}

				if (byDayString == null || startDateMatching == true) { // new rule: set parameters of DTSTART

					// current local date:
					LocalDate startDate = recurrencePanel.getEventSetter().getStartDate();
					if (dayNameComboBox != null) {
						dayNameComboBox.setSelectedIndex(startDate.getDayOfWeek().getValue() - 1);
					}
					if (byDayIntegerComboBox != null) {
						// check which weekday of the month that is
						int dayOfMonth = startDate.getDayOfMonth();
						int index = 0;
						dayOfMonth -= 7;
						while (dayOfMonth > 0) {
							dayOfMonth -= 7;
							index++;
						}
						byDayIntegerComboBox.setSelectedIndex(index);// fifth index is last

						// set byDayString
						//byDayString = "+" + index + ContactConstants.getDayOfWeekShortName(startDate);
					}
					if (monthNameComboBox != null) {
						//TestLog.o(XXXX.class, "startDate: " + startDate+", value: " + (startDate.getMonth().getValue()));
						monthNameComboBox.setSelectedIndex(startDate.getMonth().getValue() - 1);
						monthNameComboBox.setEditable(false);
					}
				}

				JButton removeButton = new JButton("X");//CalendarSetting.calTranslate("remove"));
				removeButton.setToolTipText(CalendarSetting.calTranslate("remove"));
				removeButton.addActionListener(this);

				bdp.add(removeButton);
				if (startDateMatching == true || byDayPanels == null || byDayPanels.size() < 1) {

					removeButton.setActionCommand("removeStart");

					dayNameComboBox.setEnabled(false);
					dayNameComboBox.setForeground(Color.BLACK);
					// reset the foreground color:
					dayNameComboBox.setEditable(true);
					ComboBoxEditor cbeD = dayNameComboBox.getEditor();
					JTextField jtfD = (JTextField) cbeD.getEditorComponent();
					jtfD.setDisabledTextColor(Color.BLACK);//UIManager.getColor("ComboBox.foreground"));


					byDayIntegerComboBox.setEnabled(false);
					byDayIntegerComboBox.setForeground(Color.BLACK);
					
					// reset the foreground color:
					byDayIntegerComboBox.setEditable(true);
					ComboBoxEditor cbeM = byDayIntegerComboBox.getEditor();
					JTextField jtfM = (JTextField) cbeM.getEditorComponent();
					jtfM.setDisabledTextColor(Color.BLACK);//UIManager.getColor("ComboBox.foreground"));
					
					if (monthNameComboBox != null) {
						monthNameComboBox.setEnabled(false);
						monthNameComboBox.setForeground(Color.BLACK);
						
						// reset the foreground color:
						monthNameComboBox.setEditable(true);
						ComboBoxEditor cbe = monthNameComboBox.getEditor();
						JTextField jtf = (JTextField) cbe.getEditorComponent();
						jtf.setDisabledTextColor(Color.BLACK);//UIManager.getColor("ComboBox.foreground"));
					}					
				} else {
					removeButton.setActionCommand("remove");
				}
			} else {
				TestLog.e(ByDayRecurrencePanel.class, "Invalid function call: createMonthlyYearlyByDayRulePanel()");
				new UnexpectedValueException("createMonthlyYearlyByDayRulePanel", "function", " - panel is null: " + (dayCheckBoxPanel == null) + ", freqency index: " + freqIndex).printDescription();
			}
		} // 
		if (bdp != null) {
			bdp.add(Box.createHorizontalGlue());
			byDayPanels.add(bdp);
		}
		return bdp;
	}	
	
	/**
	 * Set the weekday for an recurring event: 
	 * the start date (DTSTART) must
	 * match the recurrence rule, select weekday of 
	 * current start date
	 */
	protected void setWeekDayValues() {//LocalDate date) {

		// get start date from EventSetter:
		LocalDate startDate = recurrencePanel.getEventSetter().getStartDate();

		int startWeekDayIndex = startDate.getDayOfWeek().getValue() -1; // DayOfWeek is 1- 7, but index is needed
		if (dayCheckBoxPanel != null) {
			int checkBoxNumber = dayCheckBoxPanel.getComponentCount() - 1;//last component is Box
			for (int indexInPanel = 0; indexInPanel < checkBoxNumber; indexInPanel++) {
				if (dayCheckBoxPanel.getComponent(indexInPanel) instanceof JCheckBox) {
					JCheckBox check = (JCheckBox) dayCheckBoxPanel.getComponent(indexInPanel);
					if (indexInPanel == startWeekDayIndex) {
						//TestLog.o(XXXX.class, "match startDate: "+ ((JCheckBox) dayCheckBoxPanel.getComponent(startWeekDayIndex )).getText());
						check.setSelected(true);
						check.setEnabled(false);
						check.setForeground(Color.BLACK); // was automatically set to gray
						check.setBorder(new LineBorder(Color.BLACK));
						check.setBorderPainted(true);
					} else {
						if (check.isEnabled() == false) {
							check.setEnabled(true);
							check.setForeground(Color.BLACK);
							check.setBorderPainted(false);
						}
					}
				}					
			}
		} else {
			//TestLog.e(XXXX.class, "Couldn't set weekday: no dayCheckPanel");
		}
	}	
	
	@Override
	public void actionPerformed(ActionEvent ap) {
		String command = ap.getActionCommand();
		if (command.equals("addByDayPanel")) {	
			// show a new rule: If this is the first rule (monthly, yearly), 
			// it must match the start date
			int freqIndex = recurrencePanel.getFrequencyIndex();
			if (recurringEvent != null) {
				if ((freqIndex == 2)) {// && recurringEvent.getByDay() != null) { // daily or weekly
					this.add(createDailyWeeklyByDayRulePanel(recurringEvent.getByDay()));
				} else if (freqIndex == 1) { // daily
					return;
				} else { //monthly, yearly
					//TestLog.o(XXXX.class, "byDayP: " + byDayPanels.size());
					if (byDayPanels.size() == 0 || byDayPanels.isEmpty()) {
						// automatically set start date matching rule as first rule:
						String[] byDays = recurringEvent.getByDay();
						String[] startDateMatchingRules = getStartDateMatchingRules();
						if (byDays == null) {
							// set rule for start date
							if (startDateMatchingRules[1].startsWith("-1")) { // last weekday
								//TestLog.o(XXXX.class, "startDayMatchingRule[1]: " + startDateMatchingRules[1]);
								// set definition of last XXXday
								this.add(createMonthlyYearlyByDayRulePanel(startDateMatchingRules, 1, true));
							} else {
								// set definition of first XXXday
								this.add(createMonthlyYearlyByDayRulePanel(startDateMatchingRules, 0, true));
								//TestLog.o(XXXX.class, "startDayMatchingRule[0]: " + startDateMatchingRules[0]);
							}
						} else { // there are defined BYDAYs
							//TestLog.o(XXXX.class, "matchingRule[0]: " + startDateMatchingRules[0] + ", rule 1: " + startDateMatchingRules[1]);
							// check for start date matching rule:
							int ruleIndexMatch = -1;
							for (int ruleIndex = 0; ruleIndex < byDays.length; ruleIndex++) {
								if (byDays[ruleIndex].equals(startDateMatchingRules[0]) 
										|| byDays[ruleIndex].equals(startDateMatchingRules[1])) {
									ruleIndexMatch = ruleIndex;
									break;
								} 	
							}
							if (ruleIndexMatch != -1) { // there is a rule, that matches the start date
								// add this rule at first:
								this.add(createMonthlyYearlyByDayRulePanel(byDays, ruleIndexMatch, true));
							} else {
								// set rule for start date
								if (startDateMatchingRules[1].startsWith("-1")) { // last weekday
									//TestLog.o(XXXX.class, "startDayMatchingRule[1]: " + startDateMatchingRules[1]);
									// set definition of last XXXday
									this.add(createMonthlyYearlyByDayRulePanel(startDateMatchingRules, 1, true));
								} else {
									// set definition of first XXXday
									this.add(createMonthlyYearlyByDayRulePanel(startDateMatchingRules, 0, true));
									//TestLog.o(XXXX.class, "startDayMatchingRule[0]: " + startDateMatchingRules[0]);
								}
							}
						}
					} else { // not the first BYDAY definition

						this.add(createMonthlyYearlyByDayRulePanel(null, 0, false));
					}
				}
			} else { // recurring event is null
				
				if (freqIndex == 2) {// && recurringEvent.getByDay() != null) { // daily or weekly
					this.add(createDailyWeeklyByDayRulePanel(null));
					
				} else if (freqIndex == 1){ // daily
					return;
				} else { // monthly yearly
					
					if (byDayPanels == null || byDayPanels.isEmpty()) {
						// this must be a start date matching rule
						String[] startDateMatchingRules = getStartDateMatchingRules();

						// set rule for start date
						if (startDateMatchingRules[1].startsWith("-1")) { // last weekday
							// set definition of last XXXday
							this.add(createMonthlyYearlyByDayRulePanel(startDateMatchingRules, 1, true));
						} else {
							// set definition of first XXXday
							this.add(createMonthlyYearlyByDayRulePanel(startDateMatchingRules, 0, true));
						}
					} else { // not the first rule - not the start date matching rule
						this.add(createMonthlyYearlyByDayRulePanel(null, 0, false));
					}
				}
			}
			if (freqIndex == 2) { //  weekly
				// set start Date and disable check box
				setWeekDayValues();//, frequency);
			}
			if (this.getByDayStrings() == null) {
				recurrencePanel.getEventSetter().setStartDateSpinner(true);
			} else {
				recurrencePanel.getEventSetter().setStartDateSpinner(false);
			}
			
			this.revalidate();
			this.repaint();
			((Window) SwingUtilities.getRoot(this)).pack();			

		} else if (command.equals("remove")) { // by-day-rules

			JButton button = (JButton) ap.getSource();
			JPanel panel = (JPanel) button.getParent();
			JPanel panelParent = (JPanel) panel.getParent();
			panelParent.remove(panel);
			if (panel.getName() != null && panel.getName().equals("byDayMonthlyYearlyRulePanel")){
				boolean r = byDayPanels.remove(panel);
				if (r == false) {
					new UnexpectedValueException("r", "boolean", "is false: Panel is not removed").printStackTraceLines();
				} 
			}
			if (this.getByDayStrings() == null) {
				recurrencePanel.getEventSetter().setStartDateSpinner(true);
			} else {
				recurrencePanel.getEventSetter().setStartDateSpinner(false);
			}
			
			panelParent.revalidate();
			panelParent.repaint();
			((Window) SwingUtilities.getRoot(this)).pack(); 
			
		} else if (command.equals("removeStart")) { // by-day-rules

			if (byDayPanels.size() > 1) {
				// if more than one rule: ask to remove all
				int result = PeaDialog.showQuestion(recurrencePanel.getEventSetter(), 
						CalendarSetting.calTranslate("delete_rules"), null, 0);
				if (result == 0) { //YES
					JButton button = (JButton) ap.getSource();
					JPanel panel = (JPanel) button.getParent();
					JPanel panelParent = (JPanel) panel.getParent();
					// remove all BYDAY definitions
					Component[] comps = panelParent.getComponents();
					int compsNum = comps.length;
					for (int i = 0; i < compsNum; i++) {
						// remove only BYDAY definitions:
						if (comps[i].getName() != null 
								&& comps[i].getName().equals("byDayMonthlyYearlyRulePanel") ) {
							panelParent.remove(comps[i]);
							byDayPanels.remove(comps[i]);
						}
					}
					recurrencePanel.getEventSetter().setStartDateSpinner(true);
					panelParent.revalidate();
					panelParent.repaint();
					((Window) SwingUtilities.getRoot(this)).pack(); 
				}
			} else {
				JButton button = (JButton) ap.getSource();
				JPanel panel = (JPanel) button.getParent();
				JPanel panelParent = (JPanel) panel.getParent();
				panelParent.remove(panel);
				if (panel.getName() != null && panel.getName().equals("byDayMonthlyYearlyRulePanel")){
					boolean r = byDayPanels.remove(panel);
					if (r == false) {
						new UnexpectedValueException("r", "boolean", "is false: Panel is not removed").printStackTraceLines();
					} 
				}
				recurrencePanel.getEventSetter().setStartDateSpinner(true);
				panelParent.revalidate();
				panelParent.repaint();
				((Window) SwingUtilities.getRoot(this)).pack(); 
			}
		} 
	}
	
	/**
	 * Get the strings defining the BYDAY parameter, 
	 * for example SU, WE or -2SU, +3WE
	 * without translation
	 * 
	 * @return	the strings defining the BYDAY parameter
	 */
	public String[] getByDayStrings() {
		
		if (recurrencePanel == null) {
			TestLog.ve(ByDayRecurrencePanel.class, "Missing recurrencePanel", 5);
			return null;
		}
		try {
		int freqIndex = recurrencePanel.getFrequencyIndex();
		if (freqIndex == 4 || freqIndex == 3) { //monthly or yearly: one panel = one string

			if (byDayPanels == null) {
				TestLog.ve(ByDayRecurrencePanel.class, "Missing byDayPanels", 5);
				return null;
			}
			int byDaySize = byDayPanels.size();
			if (byDaySize == 0) {
				return null;
			} else {
				String[] result = new String[byDaySize];

				for (int i = 0; i < byDaySize; i++) {
					String byDayValue = "";
					JPanel byDayPanel = byDayPanels.get(i);
					if (byDayPanel.getComponent(2) != null && byDayPanel.getComponent(2) instanceof JComboBox) {
						@SuppressWarnings("unchecked")
						int comboIndex = ((JComboBox<String>) byDayPanel.getComponent(2)).getSelectedIndex();

						String[] byDayIntegers = {"+1", "+2", "+3", "+4", "-1", "-2", "-3", "-4" };
						byDayValue += byDayIntegers[comboIndex];

					} else {
						new UnexpectedValueException("getComponent2", "Component", "is not JComboBox").printDescription();
					}
					if (byDayPanel.getComponent(4) != null && byDayPanel.getComponent(4) instanceof JComboBox) {
						@SuppressWarnings("unchecked")
						int comboIndex = ((JComboBox<String>) byDayPanel.getComponent(4)).getSelectedIndex();

						String[] weekDayShortNames = { "MO", "TU", "WE", "TH", "FR", "SA", "SU"};
						byDayValue += weekDayShortNames[comboIndex];
					} else {
						new UnexpectedValueException("getComponent4", "Component", "is not JComboBox").printDescription();
					}
					result[i] = byDayValue;
				}
				// TODO check duplicate rules
				//TestLog.e(XXXX.class, "ByDay");
				//for(int i=0;i<result.length;i++)TestLog.o(XXXX.class, i + " result: " + result[i]);
				return result;
			}
		} else {
			if (dayCheckBoxPanel != null) {
				String[] result = null;
				ArrayList<String> list = new ArrayList<String>();
				Component[] comps = dayCheckBoxPanel.getComponents();
				String[] weekDays = { "MO", "TU", "WE", "TH", "FR", "SA", "SU"};
				for (int i = 0; i < 7; i++) {
					if ( comps[i] instanceof JCheckBox) {
						if ( ((JCheckBox) comps[i]).isSelected()) {
							list.add(weekDays[i]);
						}
					}
				}
				if (list.isEmpty()) {
					return null;
				} else {
					String[] array = new String[list.size()];
					result = (String[]) list.toArray(array);
					//TestLog.e(XXXX.class, "ByDay");
					//for(int i=0;i<result.length;i++)TestLog.o(XXXX.class, i + " result: " + result[i]);
					return result;
				}
			} else {
				return null;
			}
		}
		} catch (Exception e) {
			TestLog.ve(ByDayRecurrencePanel.class, e.getLocalizedMessage() + " - " + e.toString(), 5);
			return null;
		}
	}
	
	/**
	 * Get the integers defining the BYMONTH parameter. 
	 * Here: only one integer is possible
	 * 
	 * @return	month definition of recurrence rule (here: only one month)
	 */
	public int[] getByMonth() {
		int[] months = null;
		if (monthNameComboBox != null) {
			months = new int[1];
			months[0] = monthNameComboBox.getSelectedIndex() + 1;// +1 because of index
		}
		return months;
	}
	
	protected void resetRule() {
		
		// reset: otherwise they will return values;
		monthNameComboBox = null;
		byDayPanels.clear();// remove all definitions
		dayCheckBoxPanel = null;
	}
	
	/**
	 * Get the rule that matches the current start date or null.
	 * Most dates can be defined positive or negative, this 
	 * function checks both of them-
	 * 
	 * @param byDayStrings	BYDAY values as an array of Strings
	 * 
	 * @return	the rules that matches the current start date
	 * 			as positive (index 0) or negative (last-..., index 1) defined rules	
	 */
	private String[] getStartDateMatchingRules() {//String[] byDayStrings) {
		String[] matchingRules = new String[2];
		
		LocalDate startDate = recurrencePanel.getEventSetter().getStartDate();
		int startWeekDay = startDate.getDayOfWeek().getValue();
		
		// get the positive integer that defines the start date
		// check which weekday of the month that is
		int dayOfMonth = startDate.getDayOfMonth();
		int positiveIndex = 1;
		dayOfMonth -= 7;
		while (dayOfMonth > 0) {
			dayOfMonth -= 7;
			positiveIndex++;
		}
		if (positiveIndex > 4) {
			matchingRules[0] = "invalid";
		} else {
			matchingRules[0] = "+" + positiveIndex + CalendarSetting.getFixedWeekDaysWithoutNull()[startWeekDay - 1];
		}
		
		// get the negative integer that defines that start date
		int negativeIndex = -1;
		//int dayNumberOfMonth = startDate.getMonth().length(startDate.isLeapYear());
		//int weekNumberOfMonth = startDate.getDayOfMonth() / 7;
		
		LocalDate lastWeekDayOfMonth = YearMonth.of(startDate.getYear() , startDate.getMonth()) // Represent the entirety of a specified month.
			    .atEndOfMonth()                                                // Get the date of the last day of that month.
			    .with( TemporalAdjusters.previousOrSame( startDate.getDayOfWeek() ) );  // Move to the previous weekday, or keep if already that weekday.
		while( ! lastWeekDayOfMonth.equals(startDate)) {
			lastWeekDayOfMonth = lastWeekDayOfMonth.minusDays(7);
			negativeIndex--;
		}
		if (negativeIndex < -4) {
			matchingRules[1] = "invalid";
		} else {
			matchingRules[1] = negativeIndex + CalendarSetting.getFixedWeekDaysWithoutNull()[startWeekDay - 1];			
		}
		return matchingRules;
	}
}
