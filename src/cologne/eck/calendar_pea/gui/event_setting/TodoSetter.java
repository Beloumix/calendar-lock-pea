package cologne.eck.calendar_pea.gui.event_setting;

/*
 * PeaFactory - Production of Password Encryption Archives
 * Copyright (C) 2021  Axel von dem Bruch
 * 
 * This library is free software; you can redistribute it and/or modify it 
 * under the terms of the GNU General Public License as published 
 * by the Free Software Foundation; either version 2 of the License, 
 * or (at your option) any later version.
 * This library is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
 * or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU General Public License for more details.
 * See:  http://www.gnu.org/licenses/gpl-2.0.html
 * You should have received a copy of the GNU General Public License 
 * along with this library.
 */

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.KeyEvent;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneId;
import java.util.Calendar;
import java.util.Date;


import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSpinner;
import javax.swing.JTabbedPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.KeyStroke;
import javax.swing.SpinnerDateModel;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.text.Document;

import cologne.eck.all_peas.control.PeaControl;
import cologne.eck.all_peas.gui.BorderButtonFocusListener;
import cologne.eck.all_peas.gui.CloseOnEscapeAction;
import cologne.eck.all_peas.gui.IconManager;
import cologne.eck.all_peas.gui.PswDialogView;
import cologne.eck.all_peas.vm_specific.JREProperties;
import cologne.eck.calendar_pea.control.CalendarSetting;
import cologne.eck.calendar_pea.data.CalendarComponent;
import cologne.eck.calendar_pea.data.PTodo;
import cologne.eck.calendar_pea.gui.CalendarComboBox;
import cologne.eck.calendar_pea.gui.LockFrameCalendar;
import cologne.eck.tools.TestLog;
import cologne.eck.tools.UnexpectedValueException;

@SuppressWarnings("serial")
public class TodoSetter extends ComponentSetter 
	implements DocumentListener, ChangeListener {
	
	private PTodo existingTodo = null;

	private boolean newTodo = false;	
	
	private JTabbedPane tabbedPane;
	private JPanel contentPaneMain;

	private JTextField summaryField;
	private JTextArea descriptionArea;
	private JTextField locationField;
	
	private CalendarComboBox calendarComboBox;
	private JCheckBox startCheck;
	private JPanel timeAndDatePanelStart;
	private JCheckBox dueCheck;
	private JPanel timeAndDatePanelEnd;
	
	private JSpinner startDateSpinner;
	private TimePicker startTimePicker;
	private JSpinner endDateSpinner;
	private TimePicker endTimePicker;
	
	private JLabel mainMessageLabel;
	private JLabel moreMessageLabel;
	private JLabel alarmMessageLabel;
	//private JLabel recurrenceMessageLabel;

	private JScrollPane scroll;
	
	/*
	 * Panels to remove and add time setting for
	 * all-day events
	 */
	private JPanel endDatePanel;
	private JPanel startTimeAndDatePanel;	
	private JPanel endTimeAndDatePanel;	

	
	private JComboBox <String> priorityComboBox;
	private JComboBox <String> statusComboBox;		
	//private JTextField categoriesField;
	private JTextField urlField;

	private boolean dueTimeAndDateIsSet = false;
	private boolean startTimeAndDateIsSet = false;	

	/**
	 * Show a TodoSetter to set date, time, summary and description...
	 * 
	 * @param owner			the owner component
	 * @param _existingTodo		the todo to set or null for new event
	 * @param _newTodo		true, if the todo is not yet listed
	 * @param selectedDate	the date and time to start the TodoSetters date spinner or null
	 */
	@SuppressWarnings("unchecked")
	public TodoSetter(Component owner, //Point location,
			PTodo _existingTodo, boolean _newTodo, 
			LocalDateTime selectedDate) { 		
		
		super.existingCalComponent = _existingTodo;
		super.newCalComponent = _newTodo;
		super.calendarComponentClass = PTodo.class.getSimpleName();
		
		// Avoid keyboard trap: Close with ESC
		this.getRootPane().getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW)
    		.put(KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), "CLOSE_ON_ESCAPE");
		this.getRootPane().getActionMap().put("CLOSE_ON_ESCAPE", new CloseOnEscapeAction() );

		// reset last message:
		((LockFrameCalendar)PeaControl.getDialog().getLockFrame() ).setMessage(null, false);
		mainMessageLabel = new JLabel();
		
		// initialize start and end:
		LocalDateTime defaultStartDateTime = null;
		LocalDateTime defaultEndDateTime = null;
		// add new Event or modify existing Event
		this.newTodo =	_newTodo; 
		
		if (newTodo == false) { // Setter was started by click on todoButton
			TestLog.v(TodoSetter.class, "Modify existing todo...");
			existingTodo = _existingTodo;
			
			if (existingTodo == null) {
				TestLog.e(TodoSetter.class, "Event to add is null!");
				new UnexpectedValueException("existingEvent", "PEvent", "is null").printDescription();
				return;
			}
			if (existingTodo.getCalendar() == null) {
				new UnexpectedValueException("existingEvent.getCalendar()", "PCard", "for existing event is null: " + existingTodo.getSummary()).printDescription();
				return;
			}

			// set date start and end, even if selectedDate != null, 
			// because of recurring and several-days events:
			if (existingTodo.getDateStart() != null) {
				defaultStartDateTime = existingTodo.getDateStart();
			} else {
				defaultStartDateTime = LocalDateTime.now();
			}

			if (existingTodo.getDue() != null) {
				defaultEndDateTime = existingTodo.getDue();
			} else {
				defaultEndDateTime = defaultStartDateTime.plusDays(1);
			}

		} else { // EventSetter was started by click on dayPanel, monthPanel...:	
			TestLog.v(TodoSetter.class, "Create new todo...");
			if (selectedDate != null) {
				defaultStartDateTime = selectedDate;//, LocalTime.of(12, 0));
				defaultEndDateTime = defaultStartDateTime.plusDays(1);
			} else {
				// set date to now and time to default time 12:00
				defaultStartDateTime = LocalDateTime.now().withHour(12).withMinute(0).withSecond(0);
				// set one day later:
				defaultEndDateTime = defaultStartDateTime.plusDays(1);
			}
		}

		this.setTitle("Calendar Lock PEA - " + CalendarSetting.calTranslate("todo") + ": " + JREProperties.getVmBridge().translate("settings"));
		this.setModal(false);
		this.setAlwaysOnTop(true);			

		contentPaneMain = new JPanel();//(JPanel) this.getContentPane();
		this.add(contentPaneMain);
		contentPaneMain.setLayout(new BoxLayout(contentPaneMain, BoxLayout.PAGE_AXIS));
		contentPaneMain.setBorder(new EmptyBorder(10, 10, 10, 10));
		String okButtonText = CalendarSetting.calTranslate("set_event");
		if (newTodo == false) {
			okButtonText = CalendarSetting.calTranslate("change_event");
		}

		// ComboBox to select the PCard for this Event
		JComponent[] calComps = createCalendarComboBox();
		if (calComps[0] instanceof CalendarComboBox) {
			calendarComboBox = (CalendarComboBox) calComps[0];
		}
		if (calComps[1] instanceof JPanel) {
			contentPaneMain.add(calComps[1]);
		}

		// set the background color
		String cal = null;
		if (existingCalComponent == null) {
			cal = (String) calendarComboBox.getSelectedItem();		
		}
		setColorOfCalendarComponent(cal);
		
		JPanel setStartPanel= new JPanel();
		//setStartPanel.setLayout(new BoxLayout(setStartPanel, BoxLayout.LINE_AXIS));
		startCheck = new JCheckBox(CalendarSetting.calTranslate("set_start_date"));
		if (existingTodo != null && existingTodo.getDateStart() != null) {
			startCheck.setSelected(true);
			startTimeAndDateIsSet = true;
		} else {
			startCheck.setSelected(false);
			startTimeAndDateIsSet = false;
		}
		startCheck.addActionListener(this);
		startCheck.setActionCommand("setStartDateTime");
		setStartPanel.add(startCheck);
		//setStartPanel.add(Box.createHorizontalGlue());
		contentPaneMain.add(setStartPanel);


		/* Panel with Spinners to choose start and end time and date */
		timeAndDatePanelStart = new JPanel();
		timeAndDatePanelStart.setLayout(new BoxLayout(timeAndDatePanelStart, BoxLayout.PAGE_AXIS));

		JPanel startLabelPanel = new JPanel();
		startLabelPanel.setLayout(new BoxLayout(startLabelPanel, BoxLayout.LINE_AXIS));
		JLabel startLabel = new JLabel(CalendarSetting.calTranslate("start"));
		startLabel.setFont(new Font(Font.SANS_SERIF, Font.BOLD, 12));
		startLabelPanel.add(startLabel);
		startLabelPanel.add(Box.createHorizontalGlue());
		timeAndDatePanelStart.add(startLabelPanel);

		/* Panel for start date and time */
		startTimeAndDatePanel = new JPanel();
		startTimeAndDatePanel.setLayout(new BoxLayout(startTimeAndDatePanel, BoxLayout.LINE_AXIS));		
		startTimeAndDatePanel.add(Box.createHorizontalStrut(10));		
		/* start date */
		JLabel startSpinnerDateLabel = new JLabel(CalendarSetting.calTranslate("date"));
		startSpinnerDateLabel.setLabelFor(startDateSpinner);
		startTimeAndDatePanel.add(startSpinnerDateLabel);
		startTimeAndDatePanel.add(Box.createHorizontalStrut(5));
		// Spinner for start date: 
		SimpleDateFormat dateModel = new SimpleDateFormat("dd.MM.yy");

		Date defaultStartDate = java.util.Date
				.from(defaultStartDateTime.atZone(ZoneId.systemDefault())
						.toInstant());

		SpinnerDateModel startDateModel = new SpinnerDateModel(defaultStartDate,
					null, null, Calendar.DATE);
		startDateSpinner = new JSpinner(startDateModel);
		startDateSpinner.setName("startDateSpinner");
		startDateSpinner.setEditor(new JSpinner.DateEditor(startDateSpinner, dateModel.toPattern()));
		startDateSpinner.setMaximumSize(new Dimension(150, 25));
		startDateSpinner.setMinimumSize(new Dimension(150, 25));
		startDateSpinner.addChangeListener(this);
		
		// add a date-picker button
		JButton startDatePickerButton = new JButton(IconManager.loadScaledIcon("date-picker.png", 
				"Date Picker to select a date", 30, 25,"Date Picker", "Date Picker to select a date"));
		startDatePickerButton.setBorder(BorderFactory.createEmptyBorder());
		startDatePickerButton.addActionListener(this);
		startDatePickerButton.setActionCommand("datePickerStart");
		startDatePickerButton.addFocusListener(BorderButtonFocusListener.getInstance());
		
		startTimeAndDatePanel.add(startDateSpinner);
		//super.startDateSpinner = this.startDateSpinner;
		this.add(Box.createHorizontalStrut(5));
		startTimeAndDatePanel.add(startDatePickerButton);

		/* start time */
		startTimePicker = new TimePicker(defaultStartDateTime.toLocalTime());
		startTimePicker.getHourSpinner().setName("startHourSpinner");
		startTimePicker.getHourSpinner().addChangeListener(this);

		startTimeAndDatePanel.add(startTimePicker);
		startTimeAndDatePanel.add(Box.createHorizontalGlue());

				
		timeAndDatePanelStart.add(startTimeAndDatePanel);
		timeAndDatePanelStart.add(Box.createVerticalStrut(5));
		contentPaneMain.add(timeAndDatePanelStart);
		
		JPanel setEndPanel= new JPanel();
		//setEndPanel.setLayout(new BoxLayout(setEndPanel, BoxLayout.LINE_AXIS));
		dueCheck = new JCheckBox(CalendarSetting.calTranslate("set_due_date"));
		if (existingTodo != null && existingTodo.getDue() != null) {
			dueCheck.setSelected(true);
			dueTimeAndDateIsSet = true;
		} else {
			dueCheck.setSelected(false);
			dueTimeAndDateIsSet = false;
		}
		dueCheck.addActionListener(this);
		dueCheck.setActionCommand("setEndDateTime");
		setEndPanel.add(dueCheck);
		//setEndPanel.add(Box.createHorizontalGlue());
		contentPaneMain.add(setEndPanel);
		
		timeAndDatePanelEnd = new JPanel();
		timeAndDatePanelEnd.setLayout(new BoxLayout(timeAndDatePanelEnd, BoxLayout.PAGE_AXIS));

		/* end of event */
		JPanel endLabelPanel = new JPanel();
		endLabelPanel.setLayout(new BoxLayout(endLabelPanel, BoxLayout.LINE_AXIS));
		JLabel endLabel = new JLabel(CalendarSetting.calTranslate("end"));
		endLabel.setFont(new Font(Font.SANS_SERIF, Font.BOLD, 12));
		endLabelPanel.add(endLabel);
		endLabelPanel.add(Box.createHorizontalGlue());
		timeAndDatePanelEnd.add(endLabelPanel);		

		/* Panel for end date and time */
		endTimeAndDatePanel = new JPanel();
		endTimeAndDatePanel.setLayout(new BoxLayout(endTimeAndDatePanel, BoxLayout.LINE_AXIS));
		endTimeAndDatePanel.add(Box.createHorizontalStrut(10));
		
		endDatePanel = new JPanel();
		endDatePanel.setLayout(new BoxLayout(endDatePanel, BoxLayout.LINE_AXIS));
		JLabel endSpinnerDateLabel = new JLabel(CalendarSetting.calTranslate("date"));
		endSpinnerDateLabel.setLabelFor(endDateSpinner);
		endDatePanel.add(endSpinnerDateLabel);
		endDatePanel.add(Box.createHorizontalStrut(5));
		// date end spinner
		SpinnerDateModel dateModelEnd = new SpinnerDateModel(
				java.util.Date.from( defaultEndDateTime
						.atZone(ZoneId.systemDefault()).toInstant()),	 
				null, null, Calendar.DATE);
		
		endDateSpinner = new JSpinner(dateModelEnd);			
		endDateSpinner.setEditor(new JSpinner.DateEditor(endDateSpinner, dateModel.toPattern()));
		endDateSpinner.setMaximumSize(new Dimension(150, 25));
		endDateSpinner.setMinimumSize(new Dimension(150, 25));
		
		// add a date-picker button
		JButton endDatePickerButton = new JButton(IconManager.loadScaledIcon("date-picker.png", 
				"Date Picker to select a date", 30, 25,"Date Picker", "Date Picker to select a date"));
		endDatePickerButton.setBorder(BorderFactory.createEmptyBorder());
		endDatePickerButton.addActionListener(this);
		endDatePickerButton.setActionCommand("datePickerEnd");
		endDatePickerButton.addFocusListener(BorderButtonFocusListener.getInstance());
		
		endDatePanel.add(endDateSpinner);
		endDatePanel.add(endDatePickerButton);

		endTimeAndDatePanel.add(endDatePanel);
		if (existingTodo != null && existingTodo.isRecurring() == true) {
			endDatePanel.setVisible(false);
		}
		endTimeAndDatePanel.add(Box.createHorizontalGlue());

		// time end spinner		
		endTimePicker = new TimePicker( defaultEndDateTime.toLocalTime());
		endTimeAndDatePanel.add(endTimePicker);

		timeAndDatePanelEnd.add(endTimeAndDatePanel);
		timeAndDatePanelStart.add(Box.createVerticalStrut(10));
		//contentPaneMain.add(timeAndDatePanelStart);
		contentPaneMain.add(timeAndDatePanelEnd);
		if (existingTodo != null && existingTodo.getDateStart() != null) {
			timeAndDatePanelStart.setVisible(true);
		} else {
			timeAndDatePanelStart.setVisible(false);
		}
		if (existingTodo != null && existingTodo.getDue() != null) {
			timeAndDatePanelEnd.setVisible(true);
		} else {
			timeAndDatePanelEnd.setVisible(false);
		}
		

		JComponent[] comps = createSummaryDescriptionPanels();		
		contentPaneMain.add(comps[0]);
		if (comps[1] instanceof JTextField) {
			summaryField = (JTextField) comps[1];
		}
		contentPaneMain.add(Box.createVerticalStrut(10));
		contentPaneMain.add(comps[1]);
		contentPaneMain.add(comps[2]);
		if (comps[3] instanceof JTextArea) {
			descriptionArea = (JTextArea) comps[3];
		}
		contentPaneMain.add(comps[4]);		
		contentPaneMain.add(Box.createVerticalStrut(10));
				
		JPanel messageLabelPanel = new JPanel();
		messageLabelPanel.setLayout(new BoxLayout(messageLabelPanel, BoxLayout.LINE_AXIS));
		messageLabelPanel.add(mainMessageLabel);
		contentPaneMain.add(messageLabelPanel);	

		contentPaneMain.add(createSetButtonPanel(okButtonText));
		
		scroll = new JScrollPane(contentPaneMain);		
		
		// ===================== RecurrencePanel =======================================
		
/*		recPanel = new JPanel();
		recPanel.setLayout(new BoxLayout(recPanel, BoxLayout.PAGE_AXIS));
		recPanel.setBorder(new EmptyBorder(10, 10, 10, 10));

		// display the panel with recurrence rule:
		RecurrencePanel rp = new RecurrencePanel(existingEvent, defaultStartDateTime.toLocalDate(), this);
		recPanel.add(rp);
		recPanel.add(Box.createVerticalStrut(50));
		
		JPanel recurrenceLabelPanel = new JPanel();
		recurrenceLabelPanel.setLayout(new BoxLayout(recurrenceLabelPanel, BoxLayout.LINE_AXIS));
		recurrenceMessageLabel = new JLabel();
		recurrenceLabelPanel.add(recurrenceMessageLabel);
		recPanel.add(recurrenceLabelPanel);
		
		recPanel.add(createSetButtonPanel(okButtonText));

		scroll = new JScrollPane(contentPaneMain);	
		JScrollPane scrollRecurrence = new JScrollPane(recPanel);
*/
		
		//============================= MORE OPTIONS TAB

		JPanel morePanel = new JPanel();
		morePanel.setLayout(new BoxLayout(morePanel, BoxLayout.PAGE_AXIS));
		morePanel.setBorder(new EmptyBorder(10, 10, 10, 10));
		
		JPanel moreLabelPanel = new JPanel();
		moreLabelPanel.setLayout(new BoxLayout(moreLabelPanel, BoxLayout.LINE_AXIS));
		JLabel moreLabel = new JLabel(CalendarSetting.calTranslate("more_options"));
		Font f = moreLabel.getFont();
		moreLabel.setFont(new Font(f.getFamily(), Font.BOLD, f.getSize()));
		moreLabelPanel.add(moreLabel);
		moreLabelPanel.add(Box.createHorizontalGlue());
		morePanel.add(Box.createVerticalStrut(10));
		morePanel.add(moreLabelPanel);
		morePanel.add(Box.createVerticalStrut(30));		
		
		JComponent[] locUrlPriorityomps = createLocationUrlPriority();
		morePanel.add(locUrlPriorityomps[0]); // panel with location label
		if (locUrlPriorityomps[1] instanceof JTextField){
			locationField = (JTextField) locUrlPriorityomps[1];
			morePanel.add(locUrlPriorityomps[1]);
		}
		morePanel.add(Box.createVerticalStrut(30));
		morePanel.add(locUrlPriorityomps[2]); // panel with url label
		if (locUrlPriorityomps[3] instanceof JTextField) {
			urlField = (JTextField) locUrlPriorityomps[3];
			urlField.getDocument().addDocumentListener(this); // check valid URL
			morePanel.add(urlField);
		}
		morePanel.add(Box.createVerticalStrut(10));
		if (locUrlPriorityomps[4] instanceof JComboBox) {
			priorityComboBox = (JComboBox<String>) locUrlPriorityomps[4];			
		}
		morePanel.add(locUrlPriorityomps[5]);			
		morePanel.add(Box.createVerticalStrut(10));		
		
		// STATUS ======
		JPanel statusPanel = new JPanel();
		statusPanel.setLayout(new BoxLayout(statusPanel, BoxLayout.X_AXIS));
		JLabel statusLabel = new JLabel("status");
		statusLabel.setLabelFor(statusComboBox);
		statusPanel.add(Box.createHorizontalStrut(10));
		statusPanel.add(statusLabel);
		statusPanel.add(Box.createHorizontalStrut(20));
		
		statusComboBox = new JComboBox <String>();
		statusComboBox.addItem("no");
		statusComboBox.addItem(CalendarSetting.calTranslate("needs_action")); // 
		statusComboBox.addItem(CalendarSetting.calTranslate("completed")); // 
		statusComboBox.addItem(CalendarSetting.calTranslate("in_process")); // 
		statusComboBox.addItem(CalendarSetting.calTranslate("cancelled"));
		statusComboBox.setMaximumSize(new Dimension(100, 25)); 
		if (existingCalComponent != null && existingCalComponent.getStatus() != null) {
			String currentStatus = existingCalComponent.getStatus();
			int index = 0;
			if (currentStatus.equals("NEEDS-ACTION")){ 
				index = 1;
			} else if (currentStatus.equals("COMPLETED")){
				index = 2;
			} else if (currentStatus.equals("IN-PROCESS")){
				index = 2;
			} else if (currentStatus.equals("CANCELLED")){
				index = 4;
			}  else {
				index = 0;
				//new UnexpectedValueException("currentStatus", "String", " is unknown").printStackTraceLines();
			}
			statusComboBox.setSelectedIndex(index);
		} 
		statusPanel.add(statusComboBox);
		statusPanel.add(Box.createHorizontalGlue());
		morePanel.add(statusPanel); 		
		
		morePanel.add(Box.createVerticalStrut(20));	

		moreMessageLabel = new JLabel();
		moreMessageLabel.setForeground(Color.RED);
		JPanel moreMessageLabelPanel = new JPanel();
		moreMessageLabelPanel.setLayout(new BoxLayout(moreMessageLabelPanel, BoxLayout.LINE_AXIS));
		moreMessageLabelPanel.add(moreMessageLabel);
		morePanel.add(moreMessageLabelPanel);

		morePanel.add(createSetButtonPanel(okButtonText));
		
		JScrollPane scrollMore = new JScrollPane(morePanel);		
		
		// VALARM
		alarmMessageLabel = new JLabel();
		JPanel alarmPanel = createAlarmPanel(okButtonText, alarmMessageLabel);
		
		JScrollPane scrollAlarm = new JScrollPane(alarmPanel);	
		

		//====================== TABS: 
		
		tabbedPane = new JTabbedPane();
		tabbedPane.addTab(CalendarSetting.calTranslate("main"), null, scroll,
				CalendarSetting.calTranslate("main_settings"));
		tabbedPane.addTab(CalendarSetting.calTranslate("more"), null, scrollMore,
				CalendarSetting.calTranslate("more_options"));
		tabbedPane.addTab(CalendarSetting.calTranslate("alarm"), null, scrollAlarm,
				CalendarSetting.calTranslate("set_alarm"));
		// Set mnemonics depending on translations:
		char mnemo1 = Character.toUpperCase(CalendarSetting.calTranslate("main").charAt(0));
		char mnemo2 = '\0';
		char mnemo3 = '\0';
		String moreText = CalendarSetting.calTranslate("more");
		int moreLen = moreText.length();
		for (int i = 0; i < moreLen; i++) {
			mnemo2 = Character.toUpperCase(moreText.charAt(i));
			if (mnemo2 != mnemo1) {
				break;
			} 
		}
		String alarmText = CalendarSetting.calTranslate("alarm");
		int alarmLen = alarmText.length();
		for (int i = 0; i < alarmLen; i++) {
			mnemo3 = Character.toUpperCase(alarmText.charAt(i));
			if (mnemo3 != mnemo1 && mnemo3 != mnemo2) {
				break;
			} 
		}
		tabbedPane.setMnemonicAt(0, mnemo1);   //KeyEvent.VK_3);
		if (mnemo2 != '\0') {
			tabbedPane.setMnemonicAt(1, mnemo2);
		}
		if (mnemo3 != '\0') {
			tabbedPane.setMnemonicAt(2, mnemo3);
		}
		
		this.getContentPane().add(tabbedPane);
		//this.pack();
		this.setMinimumSize(new Dimension(530,470));
		this.setLocationRelativeTo(owner);
		this.setVisible(true);
	}
	
	@Override
	public void pack() {
		super.pack();
		
		// set scroll pane if larger than screen:
		Dimension prefSize = this.getPreferredSize();
		if (prefSize.width > PswDialogView.getScreenwidth()){
			scroll.setPreferredSize(new Dimension ((int)PswDialogView.getScreenwidth(), (int) prefSize.getHeight()));
		}
		if (prefSize.height > PswDialogView.getScreenheight()) {
			scroll.setPreferredSize(new Dimension ((int) prefSize.getWidth(), (int)PswDialogView.getScreenheight()));
		}		
	}

	@Override
	protected String getSummary() {
		return summaryField.getText();
	}

	@Override
	protected String getDescription() {
		return descriptionArea.getText();
	}

	@Override
	protected String getCalComponentLocation() {
		return locationField.getText();
	}

	@Override
	protected String getUrl() {
		return urlField.getText();
	}

	@Override
	protected boolean isAllDay() {
		return false;
	}

	@Override
	protected boolean isRecurring() {
		return false;
	}

	@Override
	protected void setAllDay() {}

	@Override
	protected void setRecurring(boolean _recurring) {}

	@Override
	protected void setMainMessage(String message, boolean error) {
		if (mainMessageLabel != null) {
			mainMessageLabel.setText(message);
			if (error == true) {
				mainMessageLabel.setForeground(Color.RED);
			} else {
				mainMessageLabel.setForeground(Color.BLACK);
			}
		} else {
			TestLog.ve(TodoSetter.class, "Missing message label...", 5);
		}
	}

	@Override
	protected void setMoreMessage(String message, boolean error) {
		if (moreMessageLabel != null) {
			moreMessageLabel.setText(message);
			if (error == true) {
				moreMessageLabel.setForeground(Color.RED);
			} else {
				moreMessageLabel.setForeground(Color.BLACK);
			}
		} else {
			TestLog.ve(TodoSetter.class, "Missing message label...", 5);
		}
	}
	
	@Override
	protected void setAlarmMessage(String message, boolean error) {
		if (alarmMessageLabel != null) {
			alarmMessageLabel.setText(message);
			if (error == true) {
				alarmMessageLabel.setForeground(Color.RED);
			} else {
				alarmMessageLabel.setForeground(Color.BLACK);
			}
		} else {
			TestLog.ve(EventSetter.class, "Missing message label...", 5);
		}
	}

	@Override
	protected void setRecurrenceMessage(String message, boolean error) {
	/*	if (recurrenceMessageLabel != null) {
			recurrenceMessageLabel.setText(message);
			if (error == true) {
				recurrenceMessageLabel.setForeground(Color.RED);
			} else {
				recurrenceMessageLabel.setForeground(Color.BLACK);
			}
		} else {
		TestLog.ve(TodoSetter.class, "Missing message label...", 5);
		}*/
	}

	@Override
	protected String getSelectedCalendar() {
		if (calendarComboBox == null || calendarComboBox.getSelectedItem() == null) {
			TestLog.ve(TodoSetter.class, "Missing value", 5);
			return null;
		}
		return (String) calendarComboBox.getSelectedItem();
	}

	@Override
	protected LocalDateTime getStartDateTime() {
		
		if (startTimeAndDateIsSet == false) {
			return null;
		}
		LocalDateTime startDateTime = null;
		LocalDate startDate = ((SpinnerDateModel)startDateSpinner.getModel()).getDate().toInstant()
			      .atZone(ZoneId.systemDefault())
			      .toLocalDate();
		LocalTime startTime = startTimePicker.getTime();
		if (startTime == null) {
			startTime = LocalTime.now();			
		}
		startDateTime = LocalDateTime.of(startDate, startTime);
		return startDateTime;
	}
	
	/**
	 * Return start of day, if time spinner is not visible, 
	 * returns null if there is no endDateTime
	 */
	@Override
	protected LocalDateTime getEndDateTime() {

		if (dueTimeAndDateIsSet == true) {

			LocalDateTime endDateTime = null;
			LocalDate endDate = ((SpinnerDateModel) endDateSpinner.getModel()).getDate().toInstant()
					.atZone(ZoneId.systemDefault())
					.toLocalDate();
			LocalTime endTime = endTimePicker.getTime();
			if (endTime != null) {
				endDateTime = LocalDateTime.of(endDate, endTime);
			} else {
				endDateTime = endDate.atStartOfDay();// hours, minutes 00:00
			}
			return endDateTime;
		} else {
			return null;
		}
	}

	@Override
	protected void setStartDateTime(LocalDateTime _startDateTime) {
		if (_startDateTime == null) {
			if (startDateSpinner.isEnabled() == false) {
				return;
			}
			// get current set date and convert to LocalDate
			LocalDate startDate = ((java.util.Date) startDateSpinner.getValue())
					.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
			//TestLog.o(TodoSetter.class, "startDate: " + startDate);

			// show the DatePicker with local date to start
			LocalDate date = new DatePicker(startDate, true).showDatePicker(this, null);
			// set new until date from DatePicker (convert to Date)
			startDateSpinner.setValue(java.util.Date.from(date.atStartOfDay(ZoneId.systemDefault()).toInstant()));
			startTimeAndDateIsSet = true;
			//this.setSize(new Dimension((int)this.getSize().getWidth(), (int) this.getSize().getHeight() + 50));
		} else {
			TestLog.ve(TodoSetter.class, "Unexpected value", 5);
		}
	}

	@Override
	protected void setEndDateTime(LocalDateTime _endDateTime) {
		if (_endDateTime == null) {
			// get current set date and convert to LocalDate
			LocalDate endDate = ((java.util.Date) endDateSpinner.getValue())
					.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
			//TestLog.o(TodoSetter.class, "startDate: " + startDate);
			
			// show the DatePicker with local date to start
			LocalDate date = new DatePicker(endDate, true).showDatePicker(this, null);
			// set new until date from DatePicker (convert to Date)
			endDateSpinner.setValue(java.util.Date.from(date.atStartOfDay(ZoneId.systemDefault()).toInstant()));
			dueTimeAndDateIsSet = true;
		} else {
			TestLog.ve(TodoSetter.class, "Unexpected value", 5);
		}		
	}
	
	@Override
	protected void showHideStart() {
		if (startCheck.isSelected()) {
			timeAndDatePanelStart.setVisible(true);		
			startTimeAndDateIsSet = true;
		} else {
			timeAndDatePanelStart.setVisible(false);
			startTimeAndDateIsSet = false;
		}
	}
	
	@Override
	protected void showHideDue() {
		if (dueCheck.isSelected()) {
			timeAndDatePanelEnd.setVisible(true);			
			dueTimeAndDateIsSet = true;
		} else {
			timeAndDatePanelEnd.setVisible(false);
			dueTimeAndDateIsSet = false;
		}
	}
	
	@Override
	public void changedUpdate(DocumentEvent de) {}
	
	@Override
	public void insertUpdate(DocumentEvent iu) {
		//TestLog.o(EventSetter.class, "insert");
		Document doc = iu.getDocument();
		if (urlField.getDocument().equals(doc)) {			
			//URL validUrl = null;
			try {
				String url = doc.getText(0, doc.getLength());
				new URL(url);
				urlField.setBorder(new LineBorder(Color.DARK_GRAY, 1));
			} catch (Exception e) {
				urlField.setBorder(new LineBorder(Color.RED, 1));
			}
		}
	}
	
	@Override
	public void removeUpdate(DocumentEvent ru) {
		//TestLog.o(EventSetter.class, "remove");
		Document doc = ru.getDocument();
		if (urlField.getDocument().equals(doc)) {			
			//URL validUrl = null;
			try {
				String url = doc.getText(0, doc.getLength());
				new URL(url);
				urlField.setBorder(new LineBorder(Color.DARK_GRAY, 1));
			} catch (Exception e) {
				urlField.setBorder(new LineBorder(Color.RED, 1));
			}
		}
	}
	
	@Override
	public void stateChanged(ChangeEvent e) {
		
		JSpinner spinner = (JSpinner) e.getSource();
		if (spinner.getName().equals("startTimeSpinner")) {
			// set end time only if start date is equal or less than end date
			if ((((java.util.Date)startDateSpinner.getValue()).getTime() + (1000*60*60)) < (((java.util.Date)endDateSpinner.getValue()).getTime() )){
				return;
			} else {
				endTimePicker.setTime(startTimePicker.getTime().plusHours(1));
			}
		} else if (spinner.getName().equals("startDateSpinner")) {

			if (isRecurring() == true) { //recurringCheck.isSelected() == true) {
				
				// step 1 a: set date of DTEND to DTSTART if required:
				if (! ((java.util.Date)startDateSpinner.getValue()).equals((java.util.Date)endDateSpinner.getValue())) {
					endDateSpinner.setValue(startDateSpinner.getValue());
				}
				// set 1 b: set time if end is after start:
				if ((((java.util.Date)startDateSpinner.getValue()).getTime() + (1000*60*60)) < (((java.util.Date)endDateSpinner.getValue()).getTime() )){
					// ok
				} else {
					endTimePicker.setTime(startTimePicker.getTime().plusHours(1));
				}

			} else {
				// reset date end if required
				if ( ! ((java.util.Date)startDateSpinner.getValue()).before((java.util.Date)endDateSpinner.getValue())) {
					endDateSpinner.setValue(startDateSpinner.getValue());
					// check if end time is after start time
					if ((((java.util.Date)startDateSpinner.getValue()).getTime() + (1000*60*60)) < (((java.util.Date)endDateSpinner.getValue()).getTime() )){
						// ok
					} else {
						// set one hour later
						endTimePicker.setTime(startTimePicker.getTime().plusHours(1));
					}					
				}
			}
		}
	}

	@Override
	/**
	 * Set if the current event is recurring.
	 * This also changes the display.
	 * 
	 * @param _recurring
	 */
	protected void setComponentRecurring(boolean _recurring) {
		boolean originalRecurring = this.isRecurring();
		this.setRecurring(_recurring);
		if (_recurring == true) {
			if (existingTodo != null) {
				existingTodo.setRecurring(true);
			}
			if (endDatePanel != null) {
				endDatePanel.setVisible(false);
			}
		} else {
			if (existingTodo != null) {
				existingTodo.setRecurring(false);
			}
			if (endDatePanel != null) {
				endDatePanel.setVisible(true);
			}
		}
		if (originalRecurring != _recurring) {
			this.pack();
		}
	}

	@Override
	protected int getPriority() {
		return priorityComboBox.getSelectedIndex();
	}

	@Override
	protected String getStatus() { // 
		int status = statusComboBox.getSelectedIndex();
		if (status >= 0) {
			if (status == 0) {
				return null;
			} else if (status == 1) {
				return "NEEDS-ACTION";
			} else if (status == 2) {
				return "COMPLETED";
			} else if (status == 3) {
				return "IN-PROCESS";
			} else if (status == 4) {
				return "CANCELLED";
			}			
		} 
		return null;
	}

	@Override
	protected CalendarComponent setRecurrence(CalendarComponent c) {
		// TODO implement recurrence
		return c;
	}
}
