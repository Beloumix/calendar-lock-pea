package cologne.eck.calendar_pea.data;

/*
 * PeaFactory - Production of Password Encryption Archives
 * Copyright (C) 2023  Axel von dem Bruch
 * 
 * This library is free software; you can redistribute it and/or modify it 
 * under the terms of the GNU General Public License as published 
 * by the Free Software Foundation; either version 2 of the License, 
 * or (at your option) any later version.
 * This library is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
 * or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU General Public License for more details.
 * See:  http://www.gnu.org/licenses/gpl-2.0.html
 * You should have received a copy of the GNU General Public License 
 * along with this library.
 */

import java.time.Duration;
import java.time.LocalDateTime;
import java.time.LocalTime;

import cologne.eck.calendar_pea.util.ICalDateTimeHelper;
import cologne.eck.tools.TestLog;

public class PAlarm {
	
	private static final String[] actionStrings = {"AUDIO", "DISPLAY", "EMAIL"};
	
	private CalendarComponent calComp;
	
	// specifies a duration prior to the start of an event or a to-do
	// - can alternatively be set to an absolute calendar date with UTC time
	//		(VALUE=DATE-TIME:19970317T133000Z)
	// may be explicitly set to be relative to the "START" or "END" of the event or to-do with the
    // 		"RELATED" parameter: "START" or "END", default is START
	// 		(RELATED=END:-P2D)
	// => Here: PEvent relative to DTSTART, PTodo relative to DUE
	private String trigger; // REQUIRED
	
	private Duration triggerDuration; 
	private LocalDateTime triggerTime; // for non-recurring events: compute from DTSTART or DTEND
	
	private String action; // REQUIRED: AUDIO, DISPLAY, EMAIL
	
	private String description; // REQUIRED for display property
	
	// optional properties: TODO
	//private int repeat;
	//private Duration duration;
	//private URL attach; // for AUDIO (sound resource) and EMAIL property (message attachments)
	//private String summary; // for EMAIL property (message subject)
	//private String[] attendee; // for EMAIL property (email address of attendees to receive the message)
	
	/**
	 * Alarm component of PEvent or PTodo
	 * 
	 * @param _calComp	the related PEvent or PTodo
	 * @param _trigger		the trigger as string
	 * @param _action			the action as list of strings: AUDIO, DISPLAY, EMAIL
	 * @param _description	the description as String
	 */
	public PAlarm(CalendarComponent _calComp, String _trigger, String _action, String _description) {
		if (_calComp == null) {
			TestLog.e(getClass(), "Missing calendar component for VALARM", 3);
			return;
		}
		if (_trigger == null || _trigger.length() == 0) {
			TestLog.ve(getClass(), "Missing trigger property for VALARM - set to date start or date end");
			_trigger = "-PT0M";
			this.triggerDuration = Duration.ZERO;
			if (_calComp instanceof PEvent) {

				if (((PEvent) _calComp).isRecurring()) {
					// do not set triggerTime - there are several dates
				} else {
					triggerTime = _calComp.getDateStart();
					if (((PEvent) _calComp).isAllDay()){
						// set trigger time to noon (12)
						triggerTime = _calComp.getDateStart().toLocalDate().atTime(LocalTime.NOON);
					}
				}
			} else if (_calComp instanceof PTodo) {
				((PTodo) _calComp).getDue();
			}
		}
		if (_action != null) {
				if ( ! ( _action.equals(actionStrings[0]) 
						|| _action.equals(actionStrings[1]) || _action.equals(actionStrings[2])) ) {
					TestLog.e(getClass(), "Invalid action property for VALARM: " + _action);
					//return;
				} else {
					this.action = _action;
				}
		}
		this.calComp = _calComp;
		this.setTrigger(_trigger);
		if (calComp instanceof PEvent) {
			if (((PEvent) calComp).isRecurring() == false) {
				if (((PEvent) calComp).isAllDay()) {
					// get trigger duration from noon
					triggerTime = ICalDateTimeHelper.durationToDateTime(
						calComp.getDateStart().toLocalDate().atTime(LocalTime.NOON), _trigger);
				} else {
					triggerTime = ICalDateTimeHelper.durationToDateTime(calComp.getDateStart(), _trigger);
				}
			} else {
				triggerTime = null;
			}
		} else if (calComp instanceof PTodo){
			triggerTime = ICalDateTimeHelper.durationToDateTime(((PTodo)calComp).getDue(), _trigger);
		}
		this.triggerDuration = ICalDateTimeHelper.getDurationFromString(_trigger);
		if (_description == null) {
			TestLog.ve(getClass(), "Missing description property for VALARM, set to summary");
			_description = _calComp.getSummary();
		}
		this.setDescription(_description);
		//TestLog.e(getClass(), ((PEvent)calComp).isRecurring() +trigger + ", triggerTime: " + triggerTime + ", triggerDur: " + triggerDuration);
	}
	
	public CalendarComponent getCalComponent() {
		return this.calComp;
	}

	/**
	 * Get the trigger as String
	 * 
	 * @return the trigger as String
	 */
	public String getTrigger() {
		return trigger;
	}

	/**
	 * Set the trigger as String and compute the triggerTime
	 * as point in time (LocalDateTime)
	 * 
	 * @param trigger the trigger to set
	 */
	public void setTrigger(String _trigger) {
		if (_trigger == null || _trigger.length() == 0) {
			//TestLog.e(getClass(), "Missing trigger property for VALARM", 3);
			//return;
			_trigger = "-PT0M";
			//triggerTime = calComp.getDateStart();
			this.triggerDuration = Duration.ZERO;
		} else {
			this.triggerDuration = ICalDateTimeHelper.getDurationFromString(_trigger);
		}
		if (calComp instanceof PEvent) {
			//triggerTime = ICalDateTimeHelper.durationToDateTime(calComp.getDateStart(), _trigger);
			if ( ((PEvent) calComp).isRecurring() == false ) {
				triggerTime = ICalDateTimeHelper.durationToDateTime(calComp.getDateStart(), _trigger);
			} else {
				triggerTime = null;
			} 
		} else if (calComp instanceof PTodo){
			triggerTime = ICalDateTimeHelper.durationToDateTime(((PTodo)calComp).getDue(), _trigger);
		}
		this.trigger = _trigger;
	}

	/**
	 * Get the duration of the trigger:
	 * For events before start, for todos before due. 
	 * 
	 * @return	the duration of the trigger
	 */
	public Duration getTriggerDuration() {
		return triggerDuration;
	}

	/**
	 * Set the duration of the trigger:
	 * For events before start, for todos before due
	 * 
	 * @param triggerDuration
	 */
/*	public void setTriggerDuration(Duration triggerDuration) {
		this.triggerDuration = triggerDuration;
	} */

	/**
	 * Get the point in time to execute the alarm.
	 * This is computed from trigger.
	 * For PEvent before DTSTART, for PTODO before DUE.
	 * This works only for non-recurring events.
	 * 
	 * @return the triggerTime to execute the alarm or maybe null for recurring events
	 */
	public LocalDateTime getTriggerTime() {
		if (triggerTime == null) {
			TestLog.ve(getClass(), "Missing trigger time", 5);
			if (calComp != null && trigger != null && ! (calComp instanceof RecurringPEvent)) {
				if (calComp instanceof PEvent) {
					//triggerTime = ICalDateTimeHelper.durationToDateTime(calComp.getDateStart(), _trigger);
					if ( ((PEvent) calComp).isRecurring() == false ) {
						triggerTime = ICalDateTimeHelper.durationToDateTime(calComp.getDateStart(), trigger);
					} else {
						triggerTime = null;
					} 
				} else if (calComp instanceof PTodo){
					triggerTime = ICalDateTimeHelper.durationToDateTime(((PTodo)calComp).getDue(), trigger);
				}
			}
		}
		return triggerTime;
	}
	
	/**
	 * Get the trigger time of a recurring event.
	 * For all-day events, this is the duration from noon
	 * 
	 * @return	trigger time as LocalTime
	 */
	public LocalTime getTriggerTimeOfRecurringEvent() {
		// TODO TEST
		if (this.getCalComponent().isRecurring() == false) {
			TestLog.ve(getClass(), "Invalid calendar component to get trigger time of recurring event", 5);
			return null;
		}
		if ( ! (this.getCalComponent() instanceof PEvent) ) {
			TestLog.ve(getClass(), "Invalid calendar component: not PEvent", 5);
			return null;
		}
		LocalDateTime ldt = null;
		if ( ((PEvent)this.getCalComponent()).isAllDay()) {
			ldt = this.getCalComponent()
					.getDateStart().toLocalDate().atTime(LocalTime.NOON)
					.minus(this.getTriggerDuration());
		} else {
			ldt = this.getCalComponent()
					.getDateStart()
					.minus(this.getTriggerDuration());
		}
		// set time for this day as triggerTime
		return LocalTime.of(ldt.getHour(), ldt.getMinute());
	}

	/**
	 * Set the point in time to execute the alarm.
	 * This must be computed from trigger property.
	 * For PEvent before DTSTART, for PTODO before DUE
	 * 
	 * @param triggerTime the triggerTime to set
	 */
	//public void setTriggerTime(LocalDateTime triggerTime) {
	//	this.triggerTime = triggerTime;
	//}
	
	/**
	 * Get the valid action strings: 
	 * AUDIO, DISPLAY, EMAIL
	 * 
	 * @return	all valid string for action property: AUDIO, DISPLAY, EMAIL
	 */
	public static String[] getActionStrings() {
		return actionStrings;
	}

	/**
	 * Get the action as String: AUDIO, DISPLAY, EMAIL
	 * 
	 * @return the action as String: AUDIO, DISPLAY, EMAIL
	 */
	public String getAction() {
		return action;
	}

	/**
	 * Set action: AUDIO, DISPLAY, EMAIL
	 * 
	 * @param actions the action to set: AUDIO, DISPLAY, EMAIL 
	 */
	public void setAction(String _action) {
		if (_action == null) {
			TestLog.e(getClass(), "Missing action String... " );
			return;
		}
		if ( (! _action.equals(actionStrings[0]))
				&& (! _action.equals(actionStrings[1])) && (! _action.equals(actionStrings[2] ))){
			TestLog.e(getClass(), "Invalid action String: " + _action);
			return;
		} else {
			this.action = _action;
		}
	}

	/**
	 * Get the description to show in DISPLAY action
	 * or null (Summary is shown)
	 * 
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * Set the description to show in DISPLAY action
	 * or null (Summary is shown)
	 * 
	 * @param description the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}

}
