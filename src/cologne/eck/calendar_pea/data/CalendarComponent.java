package cologne.eck.calendar_pea.data;

/*
 * PeaFactory - Production of Password Encryption Archives
 * Copyright (C) 2021  Axel von dem Bruch
 * 
 * This library is free software; you can redistribute it and/or modify it 
 * under the terms of the GNU General Public License as published 
 * by the Free Software Foundation; either version 2 of the License, 
 * or (at your option) any later version.
 * This library is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
 * or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU General Public License for more details.
 * See:  http://www.gnu.org/licenses/gpl-2.0.html
 * You should have received a copy of the GNU General Public License 
 * along with this library.
 */

import java.net.URL;
import java.time.Duration;
import java.time.Instant;
import java.time.LocalDateTime;
import java.util.ArrayList;

import cologne.eck.tools.TestLog;


public abstract class CalendarComponent {

	/* === DESCRIPTIVE COMPONENTS  === */	

	/* SUMMARY
	 *  a short summary or subject
	 */
	private String summary;
	
	/* DESCRIPTION:
	 *  a more complete description of the
      calendar component than that provided by the "SUMMARY" property
	 */
	private String description;
		
	/*
	 * Location of the event
	 * such as conference or meeting rooms
	 * (or URL to LDAP server - not implemented)
	 * example: LOCATION:Conference Room - F123\, Bldg. 002
	 */
	private String location;
	
	/* PRIORITY
	 * the relative priority, range 0 to 9, 
	 * 0 = undefined, 1 = highest priority
	 */
	private int priority = 0;
	
	/* STATUS:TENTATIVE/CONFIRMED/CANCELLED
	 * status or confirmation
	 */
	private String status = null;
	
	/* ATTACH
	 * associate a document object with the event
	 */
//	private URI attachment;
	
	/* CATEGORIES:
	 * specify categories or subtypes
	 */
//	private String categories;

	/* CLASS:PUBLIC/PRIVATE/CONFIDENTIAL
	 * access classification
	 */
//	private String eventClass;
	
	/*COMMENT:
	 * non-processing information intended
      to provide a comment to the calendar user
	 */
//	private String comment;

	/* GEO:two float values
	 * global position for the activity
	 */
//	private float[] geo;
	
	/* LOCATION
	 * the intended venue for the activity
	 * such as conference or meeting rooms
	 */
//	private String location;
	
	/* RESOURCES
	 * the equipment or resources
      anticipated for an activity
	 */
//	private String resources;
	
	
	/*  === DATE AND TIME COMPONENTS ===  */
	
	
	/* DTSTART:19980118T073000Z
	 * specifies when the calendar component begins
	 */
	protected LocalDateTime dateStart;	
	
	/* DURATION:PT1H0M0S
	 * specifies a positive duration of time
	 */
	private Duration duration;
	
	/* TRANSP:OPAQUE/TRANSPARENT
	 * defines whether or not an event is
      transparent to busy time searches
	 */
//	private boolean transparent;
	
	
	/*  === TIME COMPONENT PROPERTIES ===  */
	
	
	/* TZID:America/New_York
	 * text value that uniquely
      identifies the "VTIMEZONE" calendar component. 
       MUST be specified in a "VTIMEZONE"
      calendar component
	 */
	/* TZNAME:EST
	 * specifies the customary designation for a
      time zone description.
	 */
	/* TZOFFSETFROM:+1345
	 * specifies the offset that is in use prior to
      this time zone observance
	 */
	/* TZOFFSETTO:+1245
	 * specifies the offset that is in use in this
      time zone observance
	 */
	/* TZURL:http://timezones.example.org/tz/America-Los_Angeles.ics
	 * provides a means for a "VTIMEZONE" component
      to point to a network location that can be used to retrieve an up-
      to-date version of itself
	 */
	
	/*  === Relationship Component Properties ===  */
	
	/* UID:19960401T080045Z-4000F192713-0052@example.com  - must
	 * defines the persistent, globally unique
      identifier for the calendar component.
      A good method to assure uniqueness is to put the
      domain name or a domain literal IP address of the host on which
      the identifier was created on the right-hand side of an "@", and
      on the left-hand side, put a combination of the current calendar
      date and time of day (i.e., formatted in as a DATE-TIME value)
      along with some other currently unique (perhaps sequential)
      identifier available on the system
	 */
	protected String uid;
	
	/* URL:http://example.com/pub/calendars/jsmith/mytime.ics
	 * defines a Uniform Resource Locator (URL)
      associated with the iCalendar object
	 */
	private URL url;
	
	/* ATTENDEE;MEMBER="mailto:DEV-GROUP@example.com":
        mailto:joecool@example.com
	 * defines an "Attendee"
	 */
	
	/* CONTACT:Jim Dolittle\, ABC Industries\, +1-919-555-1234
	 * contact information or
      alternately a reference to contact information
	 */
//	private String contact;
	
	/* ORGANIZER
	 * defines the organizer for a calendar
      component
	 */
	
	/*  RECURRENCE-ID;VALUE=DATE:19960401
	 * This property MUST have the same
      value type as the "DTSTART" property contained within the
      recurring component.  Furthermore, this property MUST be specified
      as a date with local time if and only if the "DTSTART" property
      contained within the recurring component is specified as a date
      with local time.
	 */
//	private Duration recurranceID;
	
	/* RELATED-TO:19960401-080045-4000F192713-0052@example.com
	 * a relationship or
      reference between one calendar component and another
	 * Per default: PARENT relation. 
	 * SIBLING or CHILD must be specified by RELTYPE.
	 * A reminder event for a todo is a child of this todo
	 */
	// RELATED-TO;RELTYPE=CHILD:
	private String relatedTo;
	
	
	/* Change Management Component Properties */
	
	/* DTSTAMP:19971210T080000Z - must
	 * specifies the date and time that
      the instance of the iCalendar object was created.
      In the case of
      an iCalendar object that doesn't specify a "METHOD" property, this
      property specifies the date and time that the information
      associated with the calendar component was last revised in the
      calendar store.
	 */
	private Instant timeStamp;
	
	/* CREATED:19960329T133000Z
	 * specifies the date and time that the calendar
      information was created
	 */
//	private LocalDateTime created;
	
	/* LAST-MODIFIED:19960817T133000Z
	 * specifies the date and time that the
      information associated with the calendar component was last
      revised
	 */
//	private LocalDateTime lastModified;
	
	/*   SEQUENCE:2
	 * defines the revision sequence number of the
      calendar component within a sequence of revisions
	 */
//	private int sequence;
	
	
	/*  === Miscellaneous Component Properties  === */
	
	
	/* Non-Standard Properties
	 * x-prop = x-name *(";" icalparameter) ":" value CRLF
	 */
	
	/* REQUEST-STATUS:2.0;Success
	 * defines the status code returned for a
      scheduling request
	 */
//	private String requestStatus;
	
	
	
	/*  === EXTRA  === */

	/* the calendar, this events belongs to */
	protected PCalendar calendar;
	
	/*
	 * Event is recurring
	 */
	private boolean isRecurring = false;
	
	/*
	 * Alarm components
	 */
	private ArrayList<PAlarm> alarms;
	

	
	//============================= GETTER & SETTER ======================//

	/**
	 * Get the summary of the event
	 * 
	 * @return	the summary as String
	 */
	public String getSummary() {
		return summary;
	}
	/**
	 * Set the summary of the event
	 * 
	 * @param summary	the summary as String
	 */
	public void setSummary(String summary) {
		this.summary = summary;
	}
	/**
	 * Get the description of the event
	 * 
	 * @return	the description as String
	 */
	public String getDescription() {
		return description;
	}
	/**
	 * Set the description of the event
	 * 
	 * @param description	the description as String
	 */
	public void setDescription(String description) {
		this.description = description;
	}
	
	/**
	 * Get the location of the event
	 * 
	 * @return the location as String
	 */
	public String getLocation() {
		return location;
	}


	/**
	 * Set the location of the event
	 * 
	 * @param location the location to set as String
	 */
	public void setLocation(String newLocation) {
		this.location = newLocation;
	}



	/**
	 * Get the start of the event
	 * 
	 * @return	the start of the event as LocalDateTime
	 */
	public LocalDateTime getDateStart() {
		return dateStart;
	}
	/**
	 * Set the start of the event.
	 * If an existing event is modified later, the list
	 * of the calendar, this event belongs to, must be sorted.  
	 * 
	 * @return	the start of the event as LocalDateTime
	 */
	public void setDateStart(LocalDateTime dateStart) {
		this.dateStart = dateStart;
	}
	/**
	 * Get the duration of the event
	 * 
	 * @return the duration as Duration
	 */
	public Duration getDuration() {
		return duration;
	}
	/**
	 * Set the duration of the event
	 * 
	 * @param the duration as Duration
	 */
	public void setDuration(Duration duration) {
		this.duration = duration;
	}
	/**
	 * Get the unique identifier of this event
	 * 
	 * @return the unique identifier as String
	 */
	public String getUid() {
		return uid;
	}
	/**
	 * Set the unique identifier of this event
	 * 
	 * @param the unique identifier as String
	 */
 	public void setUid(String uid) {
		this.uid = uid;
	} 


 	/**
	 * @return the relatedTO
	 */
	public String getRelatedTO() {
		return relatedTo;
	}
	/**
	 * @param relatedTO the relatedTO to set
	 */
	public void setRelatedTO(String relatedTO) {
		this.relatedTo = relatedTO;
	}
	/**
 	 * Get the Instant (date and time) of the last modification
 	 * of this event (no METHOD is used), specified 
 	 * in the UTC time format. 
 	 * Get ZonedDateTime by:
 	 * ZonedDateTime utcZdt = timeStamp.atZone(ZoneId.of("UTC"));
 	 * 
 	 * @return	time stamp as Instant of last modification
 	 * 			in UTC time
 	 */
	public Instant getTimeStamp() {
		return timeStamp;
	}
 	/**
 	 * Set the date and time of the last modification
 	 * of this event (no METHOD is used), specified 
 	 * in the UTC time format. 
 	 * Use: 
 	 * Instant.now() to set a new time stamp
 	 * or from time stamp string: 
 	 * String ldtString = dateTimeString.substring(0,dateTimeString.length() - 1); //remove Z
 	 * Instant instant = ZonedDateTime.parse(ldtString, ContactConstants.getIcalDateTimeFormatter().withZone(ZoneId.of("UTC"))).toInstant();
 	 * or: 
 	 * Instant instant = ICalDateTimeHelper.getInstantFromUtcString(String s);
 	 * 
 	 * @param	date and time of last modification
 	 * 			in UTC time
 	 */
	public void setTimeStamp(Instant timeStamp) {
		this.timeStamp = timeStamp;
	}

	/**
	 * Get the calendar, this event belongs to
	 * 
	 * @return	a PCard instance, this event belongs to
	 */
	public PCalendar getCalendar() {
		return calendar;
	}
	/**
	 * Set the calendar, this component belongs to.
	 * If the calendar changes, the component is removed from the
	 * previous calendar, is added to the new calendar and
	 * the new calendar is sorted. 
	 * 
	 * @param calendar	the PCard instance, this component belongs to
	 */
	public abstract void setCalendar(PCalendar _calendar);
	

	/**
	 * Check if this event is recurring
	 * 
	 * @return	true, if event recurs, false, if event is one-time
	 */
	public boolean isRecurring() {
		return isRecurring;
	}
	/**
	 * Set if this event is recurring
	 * 
	 * @param	true, if event recurs, false, if event is one-time
	 */
	public void setRecurring(boolean isRecurring) {
		this.isRecurring = isRecurring;
	}
	
	
	

	/**
	 * Get the relative priority, range 0 to 9, 
	 * 0 = undefined, 1 = highest priority, 9 = lowest
	 * 
	 * @return	the priority for this event (0=undefined)
	 */
	public int getPriority() {
		return priority;
	}
	/**
	 * Set the relative priority, range 0 to 9, 
	 * 0 = undefined, 1 = highest priority, 9 = lowest	 
	 * 
	 * @param priority	the priority for this event (0=undefined)
	 */
	public void setPriority(int priority) {
		this.priority = priority;
	}
	/**
	 * Get the status of confirmation: 
	 * TENTATIVE/CONFIRMED/CANCELLED or null
	 * 
	 * @return 	status as String: 
	 * 						TENTATIVE/CONFIRMED/CANCELLED or null for event
	 * 						NEEDS-ACTION/COMPLETED/IN-PROCESS/CANCELLEDor null for todo
	 */
	public String getStatus() {
		return status;
	}
	/**
	 * Set the status of confirmation: 
	 * TENTATIVE/CONFIRMED/CANCELLED or null
	 * 
	 * @return newStatus 	status as String: 
	 * 						TENTATIVE/CONFIRMED/CANCELLED or null for event
	 * 						NEEDS-ACTION/COMPLETED/IN-PROCESS/CANCELLEDor null for todo
	 */
	public void setStatus(String newStatus) {
		this.status = newStatus;
	}
	/**
	 * Get a comma separated list of categories
	 * associated with this event
	 * 
	 * @return	comma separated list of categories
	 */
/*	public String getCategories() {
		return categories;
	} */
	/**
	 * Set a comma separated list of categories
	 * associated with this event
	 * 
	 * @param categories	comma separated list of categories
	 */
/*	public void setCategories(String categories) {
		this.categories = categories;
	}*/
	/**
	 * Get website associated with this event
	 * 
	 * @return	website associated with this event
	 */
	public URL getUrl() {
		return url;
	}
	/**
	 * Set website associated with this event
	 * 
	 * @param url	website associated with this event
	 */
	public void setUrl(URL url) {
		this.url = url;
	}
	/**
	 * Get the alarm components of this calendar component
	 * 
	 * @return the alarms or null
	 */
	public ArrayList<PAlarm> getAlarms() {
		return alarms;
	}
	/**
	 * Set the alarm component of this calendar component
	 * 
	 * @param alarm the alarm to set
	 */
	public void setAlarms(ArrayList<PAlarm> _alarms) {
		this.alarms = _alarms;
	}
	/**
	 * Add one alarm component to this calendar component
	 * 
	 * @param alarm the alarm to set
	 */
	public void addAlarm(PAlarm _alarm) {
		if (_alarm == null) {
			TestLog.ve(getClass(), "Missing alarm component to add",5);
			return;
		}
		if (alarms == null) {
			alarms = new ArrayList<PAlarm>();
		}
		if (alarms.contains(_alarm)) {
			TestLog.ve(getClass(), "Alarm already set",5);
			return;
		}
		this.alarms.add(_alarm);
	}
}
