package cologne.eck.calendar_pea.data;

/*
 * PeaFactory - Production of Password Encryption Archives
 * Copyright (C) 2021  Axel von dem Bruch
 * 
 * This library is free software; you can redistribute it and/or modify it 
 * under the terms of the GNU General Public License as published 
 * by the Free Software Foundation; either version 2 of the License, 
 * or (at your option) any later version.
 * This library is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
 * or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU General Public License for more details.
 * See:  http://www.gnu.org/licenses/gpl-2.0.html
 * You should have received a copy of the GNU General Public License 
 * along with this library.
 */

import java.time.LocalDateTime;
import java.util.Collections;

import cologne.eck.all_peas.data.PeaProperties;
import cologne.eck.tools.TestLog;
import cologne.eck.tools.UnexpectedValueException;

public class PTodo extends CalendarComponent 
	implements Comparable<PTodo> {
	

	
	/* Purpose:  This property defines the date and time that a to-do is
      expected to be completed.*/
	private LocalDateTime due;
	
	/**
	 * Create a new todo. The UID is set
	 * automatically
	 * 
	 * @param cal	the calendar for this todo
	 */
	public PTodo(PCalendar cal) { //String _uid) {
		
		if (cal == null) {
			new UnexpectedValueException("cal", "PCard", "is null").printDescription();
			TestLog.e(PTodo.class, "Missing calendar for new todo");
			return;			
		} else {
			this.setCalendar(cal);	
		}
		/*
		 * rfc7986
		 * This specification updates
		   [RFC5545] by stating that "UID" values MUST NOT include any data that
		   might identify a user, host, domain, or any other security- or
		   privacy-sensitive information.  It is RECOMMENDED that calendar user
		   agents now generate "UID" values that are hex-encoded random
		   Universally Unique Identifier (UUID) values
		 */
		// get long
		long uidLong = PeaProperties.getRandom().nextLong();//nextBytes(uidBytes);
		// create String
		this.uid =  Long.toHexString(uidLong);
	}
	/**
	 * Get the date time, where this to-do is
      expected to be completed.
      
	 * @return	the due date time
	 */
	public LocalDateTime getDue() {
		return this.due;
	}
	/**
	 * Set the date time, where this to-do is
      expected to be completed.
      
	 * @param _due	the due date time
	 */
	public void setDue(LocalDateTime _due) {
		this.due = _due;
	}
	
	@Override
	public void setCalendar(PCalendar _calendar) {
		if (_calendar == null) {
			new UnexpectedValueException("PCard", "calendar", "is null").printDescription();
		}
		if (this.calendar != null) {
			if ( ! this.calendar.equals(_calendar)) {

				// belonging was changed
				this.calendar.removeTodo(this);
				// add event to new calendar
				boolean result = _calendar.addTodo(this);
				if (result == true) {
					// sort the list of the calendar
					Collections.sort(_calendar.getTodoList());
				}
			}
		}
		this.calendar = _calendar;
	}
	@Override
	public int compareTo(PTodo o) {
		if (o.getDue() != null && this.getDue() != null) {
		return this.due.compareTo(o.due);
		} else {
			if (o.getDue() == null) {
			return -1;
			} else {
				return 1;
			}
		}
	}


}
