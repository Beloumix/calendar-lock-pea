package cologne.eck.calendar_pea.data;


/*
 * PeaFactory - Production of Password Encryption Archives
 * Copyright (C) 2015  Axel von dem Bruch
 * 
 * This library is free software; you can redistribute it and/or modify it 
 * under the terms of the GNU General Public License as published 
 * by the Free Software Foundation; either version 2 of the License, 
 * or (at your option) any later version.
 * This library is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
 * or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU General Public License for more details.
 * See:  http://www.gnu.org/licenses/gpl-2.0.html
 * You should have received a copy of the GNU General Public License 
 * along with this library.
 */


import java.time.LocalDateTime;
import java.util.Collections;

import cologne.eck.all_peas.data.PeaProperties;
import cologne.eck.tools.TestLog;
import cologne.eck.tools.UnexpectedValueException;



/** 
 * @see:
 * https://tools.ietf.org/html/rfc5545
 * Recurring events are handled in RecurringPEvent
 * 
 * @author Axel von dem Bruch
 *
 */


public class PEvent extends CalendarComponent
		implements Comparable<PEvent> {

	// TODO next: category
	// Do not use time zone property - see PCard
	
	
	
	/* DTEND:19960401T150000Z
	 * the date and time that a calendar
      component ends
	 */
	private LocalDateTime dateEnd;
	

	
	/* event is all-day event, default is false  
	 * use date without time:
	 * DTSTART;VALUE=DATE:20100101
	 * or: 
	 * DTSTART;VALUE=DATE:20100101
	 * DTEND;VALUE=DATE:20100101
	 * or set time to zero: (doesn't work in Apples iCal ?)
	 * DTSTART:20100101T000000
	 * DTEND:20100102T000000
	 */
	private boolean allDay = false;
	
	/* event takes longer than one day.
	 *  - Value for performance reasons
	 */
	private boolean severalDays = false;
	

	
	/**
	 * Create a new Event. The UID is set
	 * automatically
	 * 
	 * @param _uid	the UID of the event
	 */
	public PEvent(PCalendar cal) { //String _uid) {
		
		if (cal == null) {
			new UnexpectedValueException("cal", "PCard", "is null").printDescription();
			TestLog.e(PEvent.class, "Missing calendar for new event");
			return;			
		} else {
			this.setCalendar(cal);	
		}
		/*
		 * rfc7986
		 * This specification updates
		   [RFC5545] by stating that "UID" values MUST NOT include any data that
		   might identify a user, host, domain, or any other security- or
		   privacy-sensitive information.  It is RECOMMENDED that calendar user
		   agents now generate "UID" values that are hex-encoded random
		   Universally Unique Identifier (UUID) values
		 */
		// get long
		long uidLong = PeaProperties.getRandom().nextLong();//nextBytes(uidBytes);
		// create String
		this.uid =  Long.toHexString(uidLong);
	}
	
	//============================= GETTER & SETTER ======================//



	/**
	 * Get the end of the event
	 * 
	 * @return	the end of the event as LocalDateTime
	 */
	public LocalDateTime getDateEnd() {
		return dateEnd;
	}

	/**
	 * Set the end of the event
	 * 
	 * @param	the end of the event as LocalDateTime
	 */
	public void setDateEnd(LocalDateTime dateEnd) {
		this.dateEnd = dateEnd;
	}


	/**
	 * Get time stamp of the event
	 * 
	 * @return	the the date and time that
      			the instance of the iCalendar object was created
	 */
/*	public LocalDateTime getTimeStamp() {
		return timeStamp;
	} */
	/**
	 * Set time stamp of the event
	 * 
	 * @param 	timeStampthe the date and time that
      			the instance of the iCalendar object was created
	 */
/*	public void setTimeStamp(LocalDateTime timeStamp) {
		this.timeStamp = timeStamp;
	} */
	/**
	 * Get the calendar, this event belongs to
	 * 
	 * @return	a PCard instance, this event belongs to
	 */
	public PCalendar getCalendar() {
		return calendar;
	}
	/**
	 * Set the calendar, this event belongs to.
	 * If the calendar changes, the event is removed from the
	 * previous calendar, is added to the new calendar and
	 * the new calendar is sorted. 
	 * 
	 * @param calendar	the PCard instance, this event belongs to
	 */
	public void setCalendar(PCalendar _calendar) {
		if (_calendar == null) {
			new UnexpectedValueException("PCard", "calendar", "is null").printDescription();
		}
		if (this.calendar != null) {
			if ( ! this.calendar.equals(_calendar)) {
				// belonging was changed
				this.calendar.removeEvent(this);
				// add event to new calendar
				boolean result = _calendar.addEvent(this);
				if (result == true) {
					// sort the list of the calendar
					Collections.sort(_calendar.getEventList());
				}
			}
		}
		this.calendar = _calendar;
	}
	
	/**
	 * Test if this event is an all-day event
	 * 
	 * @return	true, if this is an all-day event
	 */
	public boolean isAllDay() {
		return allDay;
	}
	/**
	 * Set if this event should be an all-day event.
	 * If an existing event is modified later, the list
	 * of the calendar, this event belongs to, must be sorted. 
	 * 
	 * @param _allDay	true for an all-day event
	 */
	public void setAllDay(boolean _allDay) {
		this.allDay = _allDay;
	}
	
	/**
	 * Check if this event extends to
	 * more than one day
	 * 
	 * @return	true takes more than one day
	 * 			false if this event is in one day only
	 */
	public boolean isSeveralDays() {
		return severalDays;
	}
	
	/**
	 * Set if this event extends to
	 * more than one day
	 * 
	 * @param	true takes more than one day
	 * 			false if this event is in one day only
	 */
	public void setSeveralDays(boolean _severalDays) {
		this.severalDays = _severalDays;
	}

	/** Compare PEvents by dateStart property.
	 * 
	 * @param e		PEvent to compare
	 * 
	 * @return		1 if e is before this PEvent
	 * 				-1 if e is after this PEvent
	 * 				0 if e and PEvent are the same time
	 */
	@Override
	public int compareTo(PEvent e) {
		// compare by dateStart:
		return this.dateStart.compareTo(e.dateStart);

	}
}
