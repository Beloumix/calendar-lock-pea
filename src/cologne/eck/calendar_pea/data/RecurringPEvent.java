package cologne.eck.calendar_pea.data;

/*
 * PeaFactory - Production of Password Encryption Archives
 * Copyright (C) 2015  Axel von dem Bruch
 * 
 * This library is free software; you can redistribute it and/or modify it 
 * under the terms of the GNU General Public License as published 
 * by the Free Software Foundation; either version 2 of the License, 
 * or (at your option) any later version.
 * This library is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
 * or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU General Public License for more details.
 * See:  http://www.gnu.org/licenses/gpl-2.0.html
 * You should have received a copy of the GNU General Public License 
 * along with this library.
 */

import java.time.LocalDate;


import java.time.DayOfWeek;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.temporal.ChronoUnit;
import java.time.temporal.TemporalAdjusters;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;

import cologne.eck.calendar_pea.control.CalendarSetting;
import cologne.eck.tools.TestLog;
import cologne.eck.tools.UnexpectedValueException;

public class RecurringPEvent extends PEvent {
	
	private static final String[] byDayShortNames = {"SU","MO","TU","WE","TH","FR","SA"};
	/*  === Recurrence Component Properties  === */
	
	
	/*  EXDATE:19960402T010000Z,19960403T010000Z,19960404T010000Z
	 * defines the list of DATE-TIME exceptions for
      recurring events
	 */
	private LocalDate[] exceptionDates; 
	
	/* RDATE:19970714T123000Z
	 * oder: RDATE;VALUE=DATE:19970101,19970120,19970217,19970421
	 * defines the list of DATE-TIME values for
      recurring events.
      can appear along with the "RRULE"
	 */
	private LocalDate[] recurrenceDates;
	
	/* DTSTART;TZID=America/New_York:19970902T090000
       RRULE:FREQ=DAILY;UNTIL=19971224T000000Z
	 * defines a rule or repeating pattern for
      recurring events
      daily, weekly, monthly, yearly + until + count
      
      index 0: FREQ (DAILY, WEEKLY, MONTHLY, YEARLY)
      index 1: COUNT int
      NOT IN USE
	 */
//	private String[] recurrenceRule;
	
	/*
	 * The frequency as ChronoUnit: DAILY, WEEKLY, MONTHLY, YEARLY or null
	 * Not implemented: MINUTELY, HOURLY
	 */
	private ChronoUnit frequency;
	
	/*
	 * The number of occurrences: COUNT 
	 * (this includes the start date)
	 */
	private int count = -1; // default: permanent
	
	/*
	 * Last date time, the event occurs: UNTIL
	 */
	private LocalDateTime until;

	/*
	 * INTERVAL: all x weeks... 
	 */
	private int interval = 1;// default: 1, every day/week...
	
	
//	private String bySecond; // BÝSECOND
//	private String byHour;  // BYHOUR
	
	/*
	 * BYDAY: 
	 * BYDAY=SU,MO,TU,WE,TH,FR,SA
	 * COMMA-separated list of days of the week
	 * BYDAY=1FR  the first Friday
	 * BYDAY=1SU,-1SU   first and last Sunday of the month
	 * BYDAY=-2MO   second-to-last Monday of the month
	 * BYDAY=20MO   Every 20th Monday of the year
	 */
	private String[] byDay;
	
	/*
	 * BYDAY with MONTHLY like -2FR, 1SU
	 * as two arrays with corresponding indices:
	 * [0] number like -2,1
	 * [1] DayOfWeek as integers like 1 for MO or 7 for SU
	 */
	private int[][] byDayNumbersAndWeekDays;
	
	/*
	 * BYMONTH: 
	 * BYMONTH=1;
	 * BYMONTH=6,7    Yearly in June and July
	 * BYMONTH=1,2,3   on January, February, and March
	 */
	private int[] byMonth;
	
	//private LocalDateTime lastOccurrence;// the last occurrence of this event or null for endless event
	
	/*
	 * BYMONTHDAY:
	 * BYMONTHDAY=-3   the third-to-the-last day of the month
	 * BYMONTHDAY=2,15   the 2nd and 15th of the month
	 * BYMONTHDAY=1,-1   the first and last day of the month
	 * BYMONTHDAY=10,11,12,13,14,15   the 10th thru 15th of the month
	 * 
	 */
//	private int[] byMonthDay;
	
	/*
	 * BYWEEKNO:
	 * BYWEEKNO=20   week number 20
	 */
//	private String byWeekNumber;
	/*
	 * BYSETPOS:
	 * BYDAY=TU,WE,TH;BYSETPOS=3   The third instance into the month of one of Tuesday, Wednesday, or Thursday
	 * BYDAY=MO,TU,WE,TH,FR;BYSETPOS=-2   The second-to-last weekday of the month
	 */
//	private String bySetPos;
	
	/*
	 * BYYEARDAY:
	 * BYYEARDAY=1,100,200   the 1st, 100th, and 200th day
	 */
//	private String byYearDay;
	
	/*
	 * WKST:
	 * WKST=SU
	 */
//	private String wKST; // weekday
	
	
	
	/**
	 * Create an event with recurrence rule.
	 * 
	 * @param cal	the calendar, this event belongs to
	 */
	private RecurringPEvent(PCalendar cal) {
		super(cal);
		//NO!! super.setSeveralDays(true);
		this.setRecurring(true);
	}
	/**
	 * Check if a day short name is valid, that is
	 * "SU" / "MO" / "TU" / "WE" / "TH" / "FR" / "SA"
	 * 
	 * @param byDayValue	the day name to test
	 * 
	 * @return	true if byDayValue is a valid short name
	 */
	private static final boolean checkByDay(String byDayValue) { 
		boolean isValid = false;
		for (String s : byDayShortNames) {
			String toCheck = byDayValue;
			if (byDayValue.startsWith("+") || byDayValue.startsWith("-")) {
				toCheck = byDayValue.substring(2, byDayValue.length());
			}
			if (s.equals(toCheck)) {
				isValid = true;
				break;
			}
		}
		return isValid;
	}
	
	/**
	 * Downcast a PEvent to a RecurringPEvent. 
	 * 
	 * @param e		the PEvent to cast
	 * 
	 * @return	the RecurringPEvent with all properties of the
	 * 			original PEvent
	 */
	public static RecurringPEvent castToRecurringPEvent(PEvent e) {
		if (e == null) {
			TestLog.ve(RecurringPEvent.class, "Event to cast is null", 5);
			return null;
		}
		try {
			if (e instanceof RecurringPEvent) {
				return (RecurringPEvent) e;
			} else {
				RecurringPEvent re = new RecurringPEvent(e.getCalendar());
				re.setAllDay(e.isAllDay());
				re.setDateEnd(e.getDateEnd());
				re.setDateStart(e.getDateStart());
				re.setDescription(e.getDescription());
				re.setDuration(e.getDuration());
				re.setSummary(e.getSummary());
				re.setUid(e.getUid());
				re.setTimeStamp(e.getTimeStamp());
				re.setSeveralDays(e.isSeveralDays());
				// set recurring:
				re.setRecurring(true);
				// modify the calendar:
				PCalendar cal = e.getCalendar();
				if (cal.getEventList().remove(e) == true) {// event was in list;
					cal.getEventList().add(re);		
				}	
				Collections.sort(cal.getEventList());
				return re;
			}
		} catch (Exception ex) {
			TestLog.ve(RecurringPEvent.class, ex.getLocalizedMessage() + " - " + ex.toString());
			return null;
		}
	}
	/**
	 * Set all recurring properties to default values
	 * and set event as not recurring ("Upcast")
	 * 
	 * @param rEvent	the recurring event to reset
	 * 
	 * @return	the event (not recurring)
	 */
	public static PEvent resetRecurrenceProperties(RecurringPEvent rEvent) {
		if (rEvent == null) {
			TestLog.ve(RecurringPEvent.class, "Missing recurring event to reset", 5);
			return rEvent;
		}
		rEvent.setFrequency(null);
		rEvent.setInterval(1);
		rEvent.setCount(-1);
		rEvent.setUntil(null);
		rEvent.setRecurrenceDates(null);
		rEvent.setExceptionDates(null);
		rEvent.setByDay(null);
		rEvent.setByMonth(null);
		
		rEvent.setRecurring(false);
		return rEvent;
	}

	/**
	 * Get the frequency as ChronoUnit: 
	 * DAYS, WEES, MONTHS, YEARS or null
	 * 
	 * @return 	the frequency as ChronoUnit: 
	 * 			DAYS, WEEkS, MONTHS, YEARS or null
	 */
	public ChronoUnit getFrequency() {
		if (this.isRecurring() == true && this.frequency == null && this.recurrenceDates == null) {
			//new UnexpectedValueException("frequency", "String", "is null for "+ this.getSummary()).printDescription();
			TestLog.ve(RecurringPEvent.class, "No recurrence found for recurring event " + this.getSummary(), 5);
			TestLog.ve(RecurringPEvent.class, "Reset recurrence for event");
			this.setRecurring(false);
			return null;			 
		}
		return frequency;
	}
	
	/**
	 * Get the frequency as String: 
	 * daily, weekly, monthly, yearly or null.
	 * These String are translated in the calendar language bundle.
	 * 
	 * daily, weekly, monthly, yearly or null
	 * 
	 * @return 	the frequency as String: daily, weekly, monthly, yearly or null
	 */
	public String getFrequencyString() {
		if (isRecurring() == true && frequency == null ) {
			if (recurrenceDates == null) {		
				new UnexpectedValueException("frequency", "String", "is null for "+ this.getSummary()).printDescription();
				return null;
			} else {
				return null;
			}
		}
		if (frequency.equals(ChronoUnit.DAYS)) {
			return "daily";
		} else if (frequency.equals(ChronoUnit.WEEKS)) {
			return "weekly";
		} else if (frequency.equals(ChronoUnit.MONTHS)) {
			return "monthly";
		} else if (frequency.equals(ChronoUnit.YEARS)) {
			return "yearly";
		} else {
			return null;
		} 
	}
	/**
	 * Set the frequency as ChronoUnit: 
	 * DAILY, WEEKLY, MONTHLY, YEARLY or null
	 * 
	 * @param 	the frequency as String: 
	 * 			DAILY, WEEKLY, MONTHLY, YEARLY or null
	 */
	public void setFrequency(ChronoUnit frequency) {
		this.frequency = frequency;
		computeByDayNumbersAndWeekdays();
	}
	
	/**
	 * Get the interval of the repetition
	 * e.g. all three weeks
	 * 
	 * @return	the interval of the repetition
	 */
	public int getInterval() {
		return interval;
	}
	/**
	 * Set the interval of the repetition
	 * e.g. all three weeks
	 * 
	 * @param newInterval	the interval of the repetition
	 */
	public void setInterval(int newInterval) {
		if (newInterval < 1) {
			new UnexpectedValueException("newInterval", "int", "smaller than 1").printStackTraceLines();
		} else {
			this.interval = newInterval;
		}
	}

	/**
	 * Get the number of occurrences of this event (COUNT)
	 * (this includes the start date)
	 * 
	 * @return	the number of occurrences (COUNT, inclusive start date)
	 */
	public int getCount() {
		return this.count;
	}
	/**
	 * Set the number of occurrences of this event (COUNT)
	 * (this includes the start date)
	 * 
	 * @param	the number of occurrences (COUNT, inclusive start date)
	 */
	public void setCount(int newCount) {
		if (newCount != this.count){
			this.count = newCount;
		}
	} 

	/**
	 * Get the date until the recurrence occurs.
	 * This is last instance of the recurrence.
	 * 
	 * @return	the end date of the recurrence as LocalDateTime with time zero
	 */
	public LocalDateTime getUntil() {
		return until;
	}
	/**
	 * Set the date until the recurrence occurs
	 * This is last instance of the recurrence.	 
	 * 
	 * @param	the end date of the recurrence as LocalDateTime with time zero
	 */
	public void setUntil(LocalDateTime newUntil) {
		this.until = newUntil;
	}
	/**
	 * Get sorted single recurring dates of this event 
	 * 
	 * @return	sorted single recurring dates as an array of LocalDate
	 */
	public LocalDate[] getRecurrenceDates() {
		return this.recurrenceDates;
	}
	/**
	 * Set (and sort) single recurring dates of this event 
	 * 
	 * @param newRecurrenceDates unsorted single recurring dates as an array of LocalDate
	 */
	public void setRecurrenceDates(LocalDate[] newRecurrenceDates) {
		if (newRecurrenceDates != null) {
			Arrays.sort(newRecurrenceDates);
		}
		//TestLog.o(RecurringPEvent.class, "RDATE set: " +this.getSummary() + " ");for(int i=0;i< newRecurrenceDates.length;i++) TestLog.o(RecurringPEvent.class, newRecurrenceDates[i]);
		this.recurrenceDates = newRecurrenceDates;
	}
	/**
	 * Get sorted exceptions of recurring
	 * 
	 * @return	sorted exceptions of recurring as an array of LocalDate
	 */
	public LocalDate[] getExceptionDates() {
		return exceptionDates;
	}
	/**
	 * Set (and sort) exceptions of recurring
	 * 
	 * @param exceptionDates exceptions of recurring as an array of LocalDate
	 */
	public void setExceptionDates(LocalDate[] newExceptionDates) {
		if (newExceptionDates != null) {
			Arrays.sort(newExceptionDates);
		}
		this.exceptionDates = newExceptionDates;
	}

	/**
	 * Get by-day rule: list of +/-Integer
	 * "SU" / "MO" / "TU" / "WE" / "TH" / "FR" / "SA"
	 * 
	 * @return	list of the by-day parameters 
	 * 			of the recurrence rule as array of Strings
	 */
	public String[] getByDay() {
		return byDay;
	}
	
	/**
	 * Get BYDAY values as integer: 
	 * MO is 1, TU is 2... no such day is -1
	 * 
	 * @return	BYDAY values as int array (MO is 1, SU is 7)
	 */
	public int[] getByDayAsInt() {
		if (this.byDay == null) {
			return null;
		}
		if ( ! (this.frequency.equals(ChronoUnit.WEEKS) || this.frequency.equals(ChronoUnit.WEEKS))) {
			return null;
		}
		int[] dayOfWeekValues = new int[byDay.length];
		for (int byDayIndex = 0; byDayIndex < byDay.length; byDayIndex++) {
			String byDayVal = byDay[byDayIndex];
			dayOfWeekValues[byDayIndex] = CalendarSetting.getDayOfWeekValue(byDayVal);
		}
		// sort to allow binary search
		Arrays.sort(dayOfWeekValues);	
		return dayOfWeekValues;
	}
	
	/**
	 * Set by-day rule: list of +/-Integer
	 * "SU" / "MO" / "TU" / "WE" / "TH" / "FR" / "SA".
	 * Duplicates are removed. No translations of short names are allowed
	 * 
	 * @param byDay	list of the by-day parameters 
	 * 				of the recurrence rule as array of Strings
	 */
	public void setByDay(String[] _byDay) {
		if (_byDay == null) {
			this.byDay = null;
			computeByDayNumbersAndWeekdays();
			return;
		}
		//String bd = ""; for (String s : _byDay) bd += s;
		//TestLog.ve(RecurringPEvent.class, "_bd: " + bd);
		for (String s : _byDay) {
			if (checkByDay(s) == false) {
				// invalid day name
				TestLog.ve(RecurringPEvent.class, "Invalid day short name: " + s, 5);
			}
		}
		// remove duplicates:
		if (_byDay != null && _byDay.length > 0) {
			_byDay = new HashSet<String>(Arrays.asList(_byDay)).toArray(new String[0]);
			if (_byDay[0].startsWith("+") || _byDay[0].startsWith("-")) { // integers: monthly or yearly
				Arrays.sort(_byDay);
			} else { // weekly
				// sort: TODO
			}
		}		
		this.byDay = _byDay;
		computeByDayNumbersAndWeekdays();
	}
	/** 
	 * Automatically set, when BYDAY and MONTHLY is set or modified
	 * Two arrays with same indices: 
	 * [0] integers like -1 for last, 2 for second
	 * [1] integers for days of week: 1 for MO, 7 for SU
	 * 
	 * @return	two arrays: numbers and weekdays
	 */
	public int[][] getByDayNumbersAndWeekDays() {
		return byDayNumbersAndWeekDays;
	}
	
	/**
	 * Get a list of months (1-12)
	 * Note: Currently there is only one month allowed
	 * 
	 * @return	a list of months (1-12), currently only one month
	 */
	public int[] getByMonth() {
		return byMonth;
	}
	/**
	 * Set a list of months (1-12).
	 * Note: Currently there is only one month allowed
	 * 
	 * @param byMonth	a list of months (1-12), currently only one month
	 */
	public void setByMonth(int[] byMonth) {
		this.byMonth = byMonth;
	}
	/**
	 * Get a list of days of the month (1-31)
	 * 
	 * @return	a list of days of the month (1-31)
	 */
/*	public int[] getByMonthDay() {
		return byMonthDay;
	}*/
	/**
	 * Set a list of days of the month (1-31)
	 * 
	 * @param byMonthDay	a list of days of the month (1-31)
	 */
/*	public void setByMonthDay(int[] byMonthDay) {
		this.byMonthDay = byMonthDay;
	}*/
	
	/**
	 * Compute this whenever byDay and frequency is set or modified
	 */
	private void computeByDayNumbersAndWeekdays() {
		if (byDay == null || byDay.length == 0) {
			byDayNumbersAndWeekDays = null;
			return;
		}
		if (frequency == null) {
			byDayNumbersAndWeekDays = null;
			return;
		}
		if (! frequency.equals(ChronoUnit.MONTHS) && ! frequency.equals(ChronoUnit.YEARS)) {
			byDayNumbersAndWeekDays = null;
			return;
		}

		int byDayLen = byDay.length;
		// preparatory work: set weekdays and integers
		int[] byIntegers = new int[byDayLen];// positive and negative
		int[] dayOfWeek = new int[byDayLen]; // 1 for MO				
		// get the defined weekdays, signs and integers
		for (int byDayIndex = 0; byDayIndex < byDayLen; byDayIndex++) {
			String byDayVal = byDay[byDayIndex];
			// weekday:
			String weekDayString = byDayVal.substring(byDayVal.length() - 2, byDayVal.length());
			//dayOfWeek[byDayIndex] = DayOfWeek.of(Arrays.asList(weekDayShortNames).indexOf(weekDayString) + 1);// +1 because of index
			dayOfWeek[byDayIndex] = CalendarSetting.getDayOfWeek(weekDayString).getValue();
			// integers: always one digit + sign - no range check (1-4)
			String intString = byDayVal.substring(0, byDayVal.length() - 2);// include the sign
			byIntegers[byDayIndex] = Integer.parseInt(intString);
		}
		byDayNumbersAndWeekDays = new int[2][byDayLen];
		byDayNumbersAndWeekDays[0] = byIntegers;
		byDayNumbersAndWeekDays[1] = dayOfWeek;
	}
	
	/**
	 * Get the last occurrence of this event or null for endless event 
	 * (or if the end is not found). This is maybe not accurate 
	 * for events with BYDAY or BYMONTH. In case of doubt, a later occurrence
	 * (fallback) is returned to avoid removing actual events.
	 * 
	 * @return the lastOccurrence as LocalDate or null for endless event
	 */
	public LocalDate getLastOccurrence() {

		if (this.frequency != null && this.until == null && this.count == -1) {
			// endless event
			return null;
		}

		// to check if a date is an exception:
		ArrayList<LocalDate> exList = new ArrayList<LocalDate>();
		if (this.exceptionDates != null) {
			exList = new ArrayList<>(Arrays.asList(this.exceptionDates));
		}

		// start with DTSTART date
		LocalDate lastOccurrence = this.getDateStart().toLocalDate();
		if (this.getDateEnd() != null) { 
			// this should now be the same date - in further versions maybe not, 
			// when several day recurrence rule are allowed
			lastOccurrence = this.getDateEnd().toLocalDate();
		}

		// check the list of recurrence dates: 
		if (recurrenceDates != null) {
			try {
				// get last recurrence date, that is no an exception
				int recurrenceDatesLen = recurrenceDates.length;
				// get last recurrence date:
				LocalDate lastExDate = this.recurrenceDates[recurrenceDatesLen - 1];
				int counter = 1;
				while (exList.contains(lastExDate)) {
					lastExDate = null; // do not use this date if loop breaks
					counter++;
					if (counter <= recurrenceDatesLen) {
						lastExDate = this.recurrenceDates[recurrenceDatesLen - counter];
					} else {
						break;
					}
				}
				// check if exception date is after current lastOccurrence:
				if (lastExDate != null && lastExDate.isAfter(lastOccurrence) ) {
					lastOccurrence = lastExDate;
				}
			} catch (Exception e) {
				TestLog.e(getClass(), e.getLocalizedMessage() + " - " + e.toString());
				TestLog.e(getClass(), "Can't check last recurrence date", 5);
				if (frequency == null) {
					// fallback:
					return this.recurrenceDates[recurrenceDates.length - 1];					
				}
			}
		}

		// check frequency: count or until is defined
		if (frequency != null) {
			if (this.until != null && this.until.toLocalDate().isAfter(lastOccurrence)) {
				try {
					// find the last real date before until, that is not an exception				
					LocalDate untilDate = until.toLocalDate();// until as date
					LocalDate lastUntil = this.getDateStart().toLocalDate();// date to find

					LocalDate checkForEx = lastUntil.plus(interval, frequency); // to check if date is an exception

					while ( checkForEx.isBefore(untilDate)) {
						if ( ! exList.contains(checkForEx)) {
							lastUntil = checkForEx;
						}

						checkForEx = lastUntil.plus(interval, frequency);					
					}

					// for BYDAY or BYMONTH the last date is maybe later: 
					// add a full frequency, avoid removing current events					
					if (byDay != null && byMonth != null) {
						TestLog.e(getClass(), "Can't find the accurate last date before until, "
								+ "to avoid removing actual events, the event is maybe kept in case of doubt.");
						lastUntil = lastUntil.plus(1, frequency);
					}
					// check if this is after lastOccurrence:
					if (lastUntil.isAfter(lastOccurrence)) {
						lastOccurrence = lastUntil;
					}
					return lastOccurrence;

				} catch (Exception e) {
					TestLog.e(getClass(), e.getLocalizedMessage() + " - " + e.toString());
					TestLog.e(getClass(), "Can't find last date before until", 5);
					// fallback:
					return until.toLocalDate();// until as date
				}

			} else if (count > -1 ) { // count is set: there is no until date
				// exceptions increase the date!!! 				

				try {
					LocalDate lastCountDate = this.getDateStart().toLocalDate();

					// better performance if checking from the end, because exceptions are rare... 

					int checkedCount = count; // maybe there are several occurrences in a frequency
					if (byDay != null) {
						checkedCount = (int) Math.ceil((double)checkedCount / byDay.length); // round up: avoid removing events from view
					} else if (byMonth != null) {
						checkedCount = (int) Math.ceil((double)checkedCount / byMonth.length); // round up
					}
					if (checkedCount >= 1) {
						lastCountDate = lastCountDate.plus( (checkedCount * interval), frequency);
					}

					// increase if exceptions occur
					if (exceptionDates != null) {
						int exNumber = exceptionDates.length;
						if (byDay != null) { // maybe there are several occurrences in a frequency
							exNumber = (int) Math.ceil((double)exNumber / byDay.length); // round up
						} else if (byMonth != null) {
							exNumber = (int) Math.ceil((double)exNumber / byMonth.length); // round up
						}
						if (exNumber >= 1) {
							lastCountDate = this.getDateStart().toLocalDate().plus( (exNumber * interval) , frequency);
						}
					}

					if (this.byDay == null && this.byMonth == null) {

						// check for exceptions:
						if (lastCountDate.isAfter(lastOccurrence)) {

							// check against exceptions: get the last non-exception date
							while (exList.contains(lastCountDate)) {
								lastCountDate = lastCountDate.minus( (1 * interval) , frequency);
							}
							if (lastCountDate.isAfter(lastOccurrence)) {
								lastOccurrence = lastCountDate;
							}
							return lastOccurrence;
						}
					} else { // TODO
						if (lastCountDate.isAfter(lastOccurrence)) {
							lastOccurrence = lastCountDate;
						}
						return lastOccurrence;
					}
				} catch (Exception e){
					TestLog.e(getClass(), e.getLocalizedMessage() + " - " + e.toString());
					TestLog.e(getClass(), "can't find the last count date", 5);
					// return fallback: 
					int fallbackCount = count;
					if (exceptionDates != null) {
						fallbackCount = count + exceptionDates.length;
					}
					return this.getDateStart().toLocalDate().plus( (fallbackCount * interval), frequency);
				}
			}
		}
		TestLog.e(getClass(), "Can't find the last occurrence of the event...", 5);
		return null; // treat as endless event
	}
	
	/**
	 * get the positive or negative (first/last) x'th weekday of a month of a LocalDate, 
	 * e.g. the second(-last) Tuesday of the month of a date
	 * 
	 * @param dow	the day of the week as DayOfWeek instance
	 * @param xTH	the ordinal value (the second, the third...) with sign
	 * 				this must be in the range 1 - 4 or -1 - -4
	 * @param date	the date to check its month
	 * 
	 * @return	the x'th first weekday of the month of the given LocalDate
	 */
	public static LocalDate getXthWeekDayOfMonth(DayOfWeek dow, int xTH, LocalDate date) {

		return date.with(TemporalAdjusters.dayOfWeekInMonth(xTH, dow));
	}
	

	/**
	 * Get all alarms of this recurring event of a period 
	 * of at most one day, that might include a transition to the next day
	 * 
	 * @param start	start date and time
	 * @param end	end date and time
	 * 
	 * @return	all alarms of this period
	 */
	public ArrayList<PAlarm> getAlarmsOfDay(LocalDateTime start, LocalDateTime end) {
		

		if (this.getAlarms() == null) {
			return null;
		}
		LocalDate lastOccurrence = getLastOccurrence();
		if (lastOccurrence != null && lastOccurrence.isBefore(start.toLocalDate())) {
			return null; 
		}
		
		// add all alarms and remove if they don't fit
		ArrayList<PAlarm> listToCheck = new ArrayList<PAlarm>(this.getAlarms());
		ArrayList<PAlarm> resultList = new ArrayList<PAlarm>();

		LocalTime eventStartTime = this.getDateStart().toLocalTime();
		LocalDateTime allDayEventStartTime = null;
		if (this.isAllDay()) {
			allDayEventStartTime = this.getDateStart().toLocalDate().atTime(LocalTime.NOON);
		}

		// The period might include the transition to the next day
		LocalDate date = start.toLocalDate();
		LocalDate nextDate = end.toLocalDate();
		
		// to check BYDAY: 
		// get days of week as integers:
		int[] daysOfWeek = this.getByDayAsInt();
		
		for (PAlarm pa : listToCheck) {
						
			if (pa == null) {
				TestLog.e(getClass(), "Missing alarm in list", 5);
				continue;
			}
			if (pa.getTriggerDuration() == null) {
				TestLog.e(getClass(), "Missing trigger duration of alarm", 5);
				continue;
			}			
			if (this.getDateStart().minus(pa.getTriggerDuration()).isAfter(end)) {
				continue;
			}
			// special case for all-day events: start is at noon
			if (this.isAllDay() && allDayEventStartTime.minus(pa.getTriggerDuration()).isAfter(end)) {
				continue;
			}

			// EXCLUDE: check exceptions against alarms: 
			if (exceptionDates != null) {
				boolean isException = false;
				for (LocalDate d : exceptionDates) {
					// get LocalDateTime of alarm for exceptionDate
					LocalDateTime dt = null;
					if (this.isAllDay()) {
						dt = d.atTime(LocalTime.NOON) // get event start at noon
								.minus(pa.getTriggerDuration());	// get alarm start

					} else {
						dt = d.atTime(eventStartTime) // get event start
								.minus(pa.getTriggerDuration());	// get alarm start
					}
					if (dt.toLocalDate().equals(date)
							|| dt.toLocalDate().equals(nextDate)) {
						//TestLog.e(getClass(), "Exception: " + dt.toString(),5);
						// do not include this alarm
						isException = true;
					}
				}
				if (isException == true) {
					continue;
				}
			}	

			// INCLUDE: check recurrence dates against alarms: add to resultList
			boolean foundDate = false;
			if (recurrenceDates != null) {
				for (LocalDate d : recurrenceDates) {
					// get LocalDateTime of alarm for recurrenceDate
					LocalDateTime dt = null;
					if (this.isAllDay()) {
						dt = d.atTime(LocalTime.NOON) // get event start at noon
								.minus(pa.getTriggerDuration());	// get alarm start

					} else {
						dt = d.atTime(eventStartTime) // get event start
								.minus(pa.getTriggerDuration());	// get alarm start
					}
					if (dt.isAfter(start) && dt.isBefore(end)) {
						foundDate = true;
						break;
					}
				}
			}
			if (foundDate == true) {
				resultList.add(pa);
				// no need for further check of this alarm:
				continue;
			}		
			
			// EXCLUDE: check if day of week of the alarm of the date to check fits BYDAY
			if (this.byDay != null && (frequency.equals(ChronoUnit.WEEKS) || frequency.equals(ChronoUnit.DAYS))) {

				// Exclude alarm, if date is not the day of week of the alarm:			
				// Compute days of week of all alarms
		//		LocalDate now = LocalDate.now(); // any date, just to compute day of week

				// check all days of week against the date to check:
				boolean byDayFound = false;
				if (daysOfWeek != null) {
					for (int dow : daysOfWeek) {
						// check the event date, not the alarm date:
						DayOfWeek alarmDowStart = start.plus(pa.getTriggerDuration()).getDayOfWeek();
						DayOfWeek alarmDowEnd = end.plus(pa.getTriggerDuration()).getDayOfWeek();
						if (dow == alarmDowStart.getValue()
								|| dow == alarmDowEnd.getValue()) {
							byDayFound = true;
						} 
					}			
				}

				if (byDayFound == false) {
					
					// this can not be an alarm of the date to check
					continue;
				} else {
					// day of week of the date to check might be an alarm date
				}
			}	

			// INCLUDE: frequency
			// check frequency with interval:
			if (frequency == null) {
				continue; // no further check of this alarm
				
			} else {
				// exclusion of until and count was already checked in getLastOccurrence()

				boolean dateFound = false;
				// first alarm to check:
				LocalDateTime checkDate = null;
				if (this.isAllDay()) { // start time at noon
					checkDate = allDayEventStartTime.minus(pa.getTriggerDuration());
				} else {
					checkDate = this.getDateStart().minus(pa.getTriggerDuration());
				}
				if (this.byDay == null && this.byMonth == null) {
					// increment
					while (checkDate.isBefore(end) ){				
						if (checkDate.isAfter(start)) {
							// add to list
							resultList.add(pa);
							dateFound = true;
							break;
						} 
						checkDate =  checkDate.plus(interval, frequency);	
					}
				} else {
					
					if (this.byDay != null && // BYDAY simple case like MO,FR,SU
							(frequency.equals(ChronoUnit.WEEKS) || frequency.equals(ChronoUnit.DAYS)) ) {
						while (checkDate.isBefore(end) ){	
							// check if this is the week of start
							if (checkDate.plusWeeks(1).isAfter(end)) {
								// check all BYDAY values of this week
								for (int i = 0; i < byDay.length; i++) {
									// check next DayOfWeek
									LocalDateTime byDayCheck = checkDate.with(
											TemporalAdjusters.nextOrSame(DayOfWeek.of( getByDayAsInt()[i])));
									if (byDayCheck.isAfter(start) && byDayCheck.isBefore(end)) {
										// add to list
										resultList.add(pa);
										dateFound = true;
										break;
									} else {
									}
								}
								if (dateFound == true) {
									break;
								}								
							}							
							checkDate =  checkDate.plus(interval, frequency);	
						}
					} else if (this.byDay != null && 
							(frequency.equals(ChronoUnit.MONTHS) || frequency.equals(ChronoUnit.YEARS ))) {
						// BYDAY with ordinal integers like -2FR,1MO
						int[] bdNum = byDayNumbersAndWeekDays[0];
						int[] dows =  byDayNumbersAndWeekDays[1];
						for (int i = 0; i < byDay.length; i++) {
							// get days to check:
							LocalDate x = getXthWeekDayOfMonth(DayOfWeek.of(dows[i]), bdNum[i], start.toLocalDate());
							LocalDate y = getXthWeekDayOfMonth(DayOfWeek.of(dows[i]), bdNum[i], end.toLocalDate());
							//TestLog.e(getClass(), "x: " + x + ", y: " + y + ", start: " + start.plus(pa.getTriggerDuration()).toLocalDate()
							//+ ", end: " + end.plus(pa.getTriggerDuration()).toLocalDate());
							if (x.equals(start.plus(pa.getTriggerDuration()).toLocalDate()) || x.equals(end.plus(pa.getTriggerDuration()).toLocalDate())
									|| y.equals(start.plus(pa.getTriggerDuration()).toLocalDate()) || y.equals(end.plus(pa.getTriggerDuration()).toLocalDate()) ){
								// check time: 
								// check the time:
								LocalTime startTime = LocalTime.of(start.getHour(), start.getMinute());
								LocalTime endTime = LocalTime.of(end.getHour(), end.getMinute());
								//System.err.println("event time: " + eventStartTime + ", start: " + startTime + ", end: " + endTime);
								if ( (eventStartTime.equals(startTime) || eventStartTime.isAfter(startTime))
										&& (eventStartTime.equals(endTime) || eventStartTime.isBefore(endTime))){
									// add to list
									resultList.add(pa);
									dateFound = true;
									break;
								} else {
								}
							}
						}						
						
					} else if (this.byMonth != null && frequency.equals(ChronoUnit.YEARS)) {
						// TODO not tested: Can't be set in Calendar Lock PEA
						for (int i = 0; i < byMonth.length; i++) {
							//System.err.println("start month: " + start.getMonth().getValue() + ", byMonth: " + byMonth[i]);
							if (start.getMonth().getValue() == byMonth[i]) {
								//System.err.println("start day of month: " + start.getDayOfMonth() + ", event day: " + this.getDateStart().toLocalDate().getDayOfMonth());
								
								// is the same date for all-day events
								if (start.getDayOfMonth() == this.getDateStart().toLocalDate().getDayOfMonth()) {
									
									// check the time:
									LocalTime startTime = LocalTime.of(start.getHour(), start.getMinute());
									LocalTime endTime = LocalTime.of(end.getHour(), end.getMinute());
									//System.err.println("event time: " + eventStartTime + ", start: " + startTime + ", end: " + endTime);
									if (eventStartTime.minus(pa.getTriggerDuration()).isAfter(startTime)
											&& eventStartTime.minus(pa.getTriggerDuration()).isBefore(endTime)){
										// add to list
										resultList.add(pa);
										dateFound = true;
										break;
									}	
								}
							}
						}
					} else {
						TestLog.e(getClass(), "BYDAY / BYMONTH value that cannot be processed " + this.getSummary());// + this.getByDay() + " " + this.getByMonth());
					}
				}
				if (dateFound == true) {
					continue;
				}
			}
		}
		if (resultList.isEmpty()) {
			return null;
		} else {
			return resultList;
		}
	}
}
