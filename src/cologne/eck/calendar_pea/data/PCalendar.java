package cologne.eck.calendar_pea.data;

/*
 * PeaFactory - Production of Password Encryption Archives
 * Copyright (C) 2015  Axel von dem Bruch
 * 
 * This library is free software; you can redistribute it and/or modify it 
 * under the terms of the GNU General Public License as published 
 * by the Free Software Foundation; either version 2 of the License, 
 * or (at your option) any later version.
 * This library is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
 * or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU General Public License for more details.
 * See:  http://www.gnu.org/licenses/gpl-2.0.html
 * You should have received a copy of the GNU General Public License 
 * along with this library.
 */

import java.awt.Color;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;

import cologne.eck.all_peas.data.PeaProperties;
import cologne.eck.calendar_pea.control.CalendarControl;
import cologne.eck.calendar_pea.util.RecurringEventComposer;
import cologne.eck.tools.ExceptionHelper;
import cologne.eck.tools.TestLog;
//import cologne.eck.tools.Help;
import cologne.eck.tools.UnexpectedValueException;





/** 
 * @see:
 * https://tools.ietf.org/html/rfc5545
 * https://tools.ietf.org/html/draft-daboo-icalendar-extensions-06
 * and: https://tools.ietf.org/html/rfc7986
 * 
 * @author Axel von dem Bruch
 *
 */




public class PCalendar {
	
	private HashSet<String> associatedFileNames = new HashSet<String>();
	
	/* VERSION - must
	 * the highest version number or the minimum and maximum range of the
      iCalendar specification that is required in order to interpret the
      iCalendar object
	 */
	private static String version = "2.0";
	
	/* CALSCALE - optional
	 * defines the calendar scale used for the
      calendar information specified in the iCalendar object 
	 */
//	private static String scale = "GREGORIAN";
	
	/* METHOD:PUBLISH/REQUEST/CANCEL/REPLY/ADD/REFRESH/COUNTER/DECLINECOUNTER
	 * - optional
	 * https://tools.ietf.org/html/rfc2446#section-3.2
	 * 
	 */
//	private static String method = "REQUEST";
	
	
	
	// ===========  NOT DEFINED IN RFC 5545, but in extensions-06  ================ //
	
	/*
	 * The name of the calendar
	 */
	private String name; // no language support here
	/*
	 * If a calendar name is missing, the program creates one:
	 * "Default" + calendarNameIndex
	 */
	private static int calendarNameIndex = 0;
	
	/*
	 * The description of the calendar
	 */
	private String description; // no language support
	
	/* UID:19960401T080045Z-4000F192713-0052@example.com
	 * defines the persistent, globally unique
      identifier for the calendar.
      It is RECOMMENDED that the right-hand
      side contain some domain identifier
	 */
	private String calUid;
	
	/* COLOR:255:0:255
	 * The value MUST be three COLON-separated INTEGER
      values.
	 */
	private Color color = Color.WHITE;//new Color(179, 230, 25);
	
	/* This property specifies the date and time that the
     * information associated with the calendar was last revised (see RFC 7986).
	 */
//	private Instant lastModified;
	
	/* IMAGE;VALUE=URI;DISPLAY=BACKGROUND;FMTTYPE=image/png:h
    ttp://example.com/images/party.png
	 * an image associated with the
      calendar
	 * URI or BINARY
	 */
//	private Image image;
	
	/* TIMEZONE-ID:America/New_York
	 * specifies the default time zone identifier
      for the iCalendar object as a whole
	 */
//	private String timeZone;
	
	
	/*
	 * The PEvents of the calendar
	 */
	private ArrayList<PEvent> eventList = new ArrayList<PEvent>();
	
	
	/*
	 * The Todoss of the calendar
	 */
	private ArrayList<PTodo> todoList = new ArrayList<PTodo>();
	
	/**
	 * Create a new calendar
	 * 
	 * @param _name			the name of the calendar - is set to default if missing
	 * @param _description	the description of the calendar - optional
	 * @param _color		the color of the calendar - optional
	 * @param _fileNames	an ArrayList of file names, where changes of this calendar are to be stored
	 */
	public PCalendar(String _name, String _description, Color _color, HashSet<String> _fileNames) {
		
		if (_name == null || _name.equals("")) {
			// set the name: avoid equal names (if UID is different)
			_name = "default" + calendarNameIndex;
			while (CalendarControl.getCalendarByName(_name) != null) { // there is a calendar with this name
				if (this.calUid != null &&  CalendarControl.getCalendarByUID( this.calUid) != null) { // calUid exists
					// calUID exists, do not rename but merge
					break;
				}
				calendarNameIndex++;
				_name = "default" + calendarNameIndex;
			}			
		}
		if (_fileNames == null) {
			new UnexpectedValueException("ArrayList<Strin>", "fileNames", "is null").printDescription();
		} else {
			associatedFileNames.addAll(_fileNames);
		}
		this.name = _name;
		this.description = _description;
		if (_color == null) {
			this.color = Color.WHITE;
		} else {
			this.color = _color;			
		}		
		/*
		 * rfc7986
		 * This specification updates
		   [RFC5545] by stating that "UID" values MUST NOT include any data that
		   might identify a user, host, domain, or any other security- or
		   privacy-sensitive information.  It is RECOMMENDED that calendar user
		   agents now generate "UID" values that are hex-encoded random
		   Universally Unique Identifier (UUID) values
		 */
		// get long
		long uidLong = PeaProperties.getRandom().nextLong();//nextBytes(uidBytes);
		// create String
		this.calUid =  Long.toHexString(uidLong);
		if (this.calUid == null || this.calUid.length() == 0) {
			new UnexpectedValueException("calUid", "String", "missing calendar UID (null)").printStackTraceLines();
		}
		//TestLog.o(XXXX.class, "calUid: " + this.calUid);
	}
	
	
	// ============ METHODS ======= //
	
	
	/**
	 * Set the list of events of this calendar
	 * 
	 * @param newList list of events as ArrayList
	 */
	public final void setEventList(ArrayList<PEvent> newList) {
		// always sort the list
		Collections.sort(newList);
		eventList = newList;
	}
	
	/**
	 * Set the list of todos of this calendar
	 * 
	 * @param newList list of events as ArrayList
	 */
	public final void setTodoList(ArrayList<PTodo> newList) {
		// always sort the list
		Collections.sort(newList);
		todoList = newList;
	}
	
	/**
	 * Get the list of events of this calendar
	 * 
	 * @return newList list of events as ArrayList
	 */
	public final ArrayList<PEvent> getEventList() {
		return eventList;
	}
	
	
	/**
	 * Get the list of todos of this calendar
	 * 
	 * @return newList list of events as ArrayList
	 */
	public final ArrayList<PTodo> getTodoList() {
		return todoList;
	}
	/**
	 * Add an Event to the list and sort the list if required
	 * 
	 * @param e		the Event to add
	 * 
	 * @return		true if this collection changed as a result of the call
	 */	 
	public final boolean addEvent(PEvent e) {
		boolean result = eventList.add(e);
		if (result == true) {
			Collections.sort(eventList);
		}
		return result;
	}
	
	/**
	 * Add a todo to the list and sort the list if required
	 * 
	 * @param e		the todo to add
	 * 
	 * @return		true if this collection changed as a result of the call
	 */	 
	public final boolean addTodo(PTodo todo) {
		boolean result = todoList.add(todo);
		if (result == true) {
			Collections.sort(todoList);
		}
		return result;
	}
	
	/**
	 * Remove an Event from the list
	 * 
	 * @param e		the Event to remove from the list
	 * 
	 * @return		true if this list contained the specified element
	 */
	public final boolean removeEvent( PEvent e) {
		return eventList.remove(e);
	}
	
	/**
	 * Remove a todo from the list
	 * 
	 * @param e		the todo to remove from the list
	 * 
	 * @return		true if this list contained the specified element
	 */
	public final boolean removeTodo( PTodo todo) {
		return todoList.remove(todo);
	}

	/**
	 * Get the name of this calendar
	 * 
	 * @return	the name of the calendar as String
	 */
	public String getName() {
		return name;
	}
	
	/**
	 * Set the name of this calendar
	 * 
	 * @param	the name of the calendar as String
	 */
	public void setName(String name) {
		this.name = name;
	}
	
	/**
	 * Get the unique identifier of this calendar
	 * 
	 * @return	the unique identifier as String
	 */
	public String getCalUid() {
		return calUid;
	}
	
	/**
	 * Set the unique identifier of this calendar
	 * 
	 * @param	the unique identifier as String
	 */
	public void setCalUid(String calUid) {
		this.calUid = calUid;
	}
	
	/**
	 * Get the description of this calendar
	 * 
	 * @return	the description of this calendar as String
	 */
	public String getDescription() {
		return description;
	}
	
	/**
	 * Set the description of this calendar
	 * 
	 * @param	the description of this calendar as String
	 */
	public void setDescription(String description) {
		this.description = description;
	}


	/**
	 * Get the color of this calendar
	 * 
	 * @return	the color that is used for every event of this calendar
	 */
	public Color getColor() {
		return color;
	}
	
	/**
	 * Set the color of this calendar
	 * 
	 * @param	the color that is used for every event of this calendar
	 */
	public void setColor(Color _color) {
		this.color = _color;
	}
	
	/** Get the version of this calendar
	 * 
	 * @return	the version as String
	 */
	public static String getVersion() {
		return version;		
	}
	
	/**
	 * Get the scale of this calendar
	 * 
	 * @return	the scale as String
	 */
/*	public static String getScale() {
		return scale;
	}*/

	/**
	 * Get the method of this calendar
	 * 
	 * @return the method as String
	 */
/*	public static String getMethod() {
		return method;
	}*/
	
	/**
	 * Merge the existing calendar with another calendar. 
	 * Take the event with the newest time stamp. 
	 * 
	 * @param newCal	the calendar to merge with this calendar
	 * @param newFileName	the name of the file, this calendar is stored in
	 * 
	 * @return	the number of elements added to the existing calendar
	 */
	public int mergeCalendar(PCalendar newCal, String newFileName) {
		
		if (newCal == null) {
			new UnexpectedValueException("newCal", "PCard", "is null").printDescription();
			return 0;
		}
		
		// merge the files to store changes:
		try {
			//this.associatedFileNames.add(newFileName);
			this.associatedFileNames.add(newFileName);
			// probably several files:
			this.associatedFileNames.addAll(newCal.getAssociatedFileNamesAsSet());
		} catch (Exception e) {
			TestLog.e(PCalendar.class, "PCard " + new ExceptionHelper(e).getInfos());
		}
		
		// 1. events:
		ArrayList<PEvent> bothList = new ArrayList<PEvent>();
		bothList.addAll(this.eventList);

		int previousListSize = bothList.size();
		ArrayList<PEvent> newList = newCal.getEventList();
		//Help.printEventList(this.getName() + ": current eventList", eventList, false, true, false);
		//Help.printEventList(newCal.getName() + ": newList", newList, false, true, false);
		if (newList.size() == 0) {
			TestLog.e(PCalendar.class, "Empty calendar to merge: " + newCal.getName() + " in:" + newFileName);
			return 0;
		}
		// append newList to the eventList:
		boolean result = bothList.addAll(newList);
		if (result == false) {
			// no element was added
			return 0;
		}
		newList.clear(); // clear the new list
		// set color of existing calendar
		//newCal.setColor(this.color);
		
		//Help.printEventList("resultList before sort", eventList, false, true, false);
		// sort list by UID
		Collections.sort(bothList, new UIDComparator());
		//Help.printEventList("resultList after sort "+ this.getName() + " " + this.getCalUid(), bothList, false, true, false);
		
		// resulting list with elements with newest time stamp
		ArrayList<PEvent> resultList = new ArrayList<PEvent>();
		
		int concatenateSize = bothList.size();

		// iterator over eventList and extra-increment listIndex if duplicate found
		for (int listIndex = 0; listIndex < concatenateSize; listIndex++) {			
			PEvent eventToAdd = bothList.get(listIndex); // maybe modified in while loop
			String uid = eventToAdd.getUid();
			if ((listIndex + 1) >= concatenateSize) { // last element of list
				eventToAdd.setCalendar(this);
				resultList.add(eventToAdd);
				break;
			}
			PEvent nextEvent = bothList.get(listIndex + 1);
			if (! nextEvent.getUid().equals(uid)) {
				eventToAdd.setCalendar(this);
				resultList.add(eventToAdd);
				//continue; // check next event for duplicates, don't increment listIndex
			} else {
				// find the most up to date event and add only this event
				Instant eTimeStamp = eventToAdd.getTimeStamp();
				Instant nextTimeStamp = nextEvent.getTimeStamp();
				if (eTimeStamp.isAfter(nextTimeStamp)) {
					eventToAdd.setCalendar(this);
					resultList.add(eventToAdd);
				} else {
					nextEvent.setCalendar(this);
					resultList.add(nextEvent);
				}
				listIndex++;
			}
		}
		if (resultList.size() == 0 && this.eventList.size() > 0) {
			new UnexpectedValueException("ArrayList<PEvent>", "resultList", "is null, but eventList size is " + this.eventList.size()).printDescription();
		}
		this.eventList.clear();
		// set resulting list as new eventList
		Collections.sort(resultList);// the list was sorted by UID
		this.eventList = resultList;		
		
		// 2. Todos: // TODO start check
		ArrayList<PTodo> bothTodoList = new ArrayList<PTodo>();
		bothTodoList.addAll(this.todoList);
		
		int previousTodoListSize = bothTodoList.size();
		ArrayList<PTodo> newTodoList = newCal.getTodoList();
		//Help.printEventList(this.getName() + ": current eventList", eventList, false, true, false);
		//Help.printEventList(newCal.getName() + ": newList", newList, false, true, false);
		if (newTodoList.size() == 0) {
			TestLog.e(PCalendar.class, "Empty calendar to merge: " + newCal.getName() + " in:" + newFileName);
			return 0;
		}
		// append newList to the eventList:
		boolean todoResult = bothTodoList.addAll(newTodoList);
		if (todoResult == false) {
			// no element was added
			return 0;
		}
		newTodoList.clear(); // clear the new list
		// set color of existing calendar
		//newCal.setColor(this.color);
		
		//Help.printEventList("resultList before sort", eventList, false, true, false);
		// sort list by UID
		Collections.sort(bothTodoList, new UIDComparator());
		//Help.printEventList("resultList after sort "+ this.getName() + " " + this.getCalUid(), bothList, false, true, false);
		
		// resulting list with elements with newest time stamp
		ArrayList<PTodo> resultTodoList = new ArrayList<PTodo>();
		
		int concatenateTodoSize = bothTodoList.size();

		// iterator over eventList and extra-increment listIndex if duplicate found
		for (int listIndex = 0; listIndex < concatenateTodoSize; listIndex++) {			
			PTodo todoToAdd = bothTodoList.get(listIndex); // maybe modified in while loop
			String uid = todoToAdd.getUid();
			if ((listIndex + 1) >= concatenateTodoSize) { // last element of list
				todoToAdd.setCalendar(this);
				resultTodoList.add(todoToAdd);
				break;
			}
			PTodo nextTodo = bothTodoList.get(listIndex + 1);
			if (! nextTodo.getUid().equals(uid)) {
				todoToAdd.setCalendar(this);
				resultTodoList.add(todoToAdd);
				//continue; // check next event for duplicates, don't increment listIndex
			} else {
				// find the most up to date event and add only this event
				Instant eTimeStamp = todoToAdd.getTimeStamp();
				Instant nextTimeStamp = nextTodo.getTimeStamp();
				if (eTimeStamp.isAfter(nextTimeStamp)) {
					todoToAdd.setCalendar(this);
					resultTodoList.add(todoToAdd);
				} else {
					nextTodo.setCalendar(this);
					resultTodoList.add(nextTodo);
				}
				listIndex++;
			}
		}
		if (resultTodoList.size() == 0 && this.todoList.size() > 0) {
			//new UnexpectedValueException("ArrayList<PTodo>", "resultTodoList", "is null, but todoList size is " + this.todoList.size()).printDescription();
			TestLog.ve(getClass(), "Invalid size of list: " + todoList.size(), 5);
		}
		this.todoList.clear();
		// set resulting list as new eventList
		Collections.sort(resultTodoList);// the list was sorted by UID
		this.todoList = resultTodoList;		// TODO end 
		
		
		
		//Help.printEventList("resultList at end", this.eventList, false, true, false);
		// update
		CalendarControl.refreshView();
		// return the number of elements added to the previous list:
		return (resultList.size() - previousListSize) + (resultTodoList.size() - previousTodoListSize);
	}
	
	/**
	 * Separate the events and todos before a given date in an extra calendar 
	 * with the original name + "_until_" + beginDate.toString()
	 * and remove these events or todos from the given calendar. 
	 * 
	 * @param beginDate		the first date not to include in separated list
	 * @param fileNameWithPath		the file to store the separated list
	 * 
	 * @return	a PCard with events and todos that end before the given date or null
	 * 			if this PCalender would be empty
	 */
	public PCalendar separateEventsBefore( 
			LocalDate beginDate, String fileNameWithPath){
		
		if (eventList.size() == 0) {
			return null;
		}						
		// PEvents:
		// resulting event list
		ArrayList<PEvent> atRestEventList = new ArrayList<PEvent>();	
		// check the event, add to rest lists and remove from this list
		int eventListSize = eventList.size();
		for (int index = 0; index < eventListSize; index++) {
			
			PEvent e = eventList.get(index);			
			LocalDate eventStartDate = e.getDateStart().toLocalDate();
			LocalDate eventEndDate = null;
			if (e.getDateEnd() != null) { // recurring event
				eventEndDate = e.getDateEnd().toLocalDate();
			} 

			if ( eventStartDate.isBefore(beginDate)) {

				// Recurring event: check dates only
				if (e.isRecurring()) {
					RecurringPEvent re =  null;
					if (e instanceof RecurringPEvent) {
						re = (RecurringPEvent) e;
					} else {
						re = RecurringPEvent.castToRecurringPEvent(e);
					}
					// short step for performance: birthdays and other endless events
					if (re.getRecurrenceDates() == null && re.getExceptionDates() == null
							&& re.getFrequency() != null && re.getCount() == -1 && re.getUntil() == null) {
						continue;
					}
					if (re.getLastOccurrence().isBefore(beginDate)) {
						atRestEventList.add(re);
					}
					
					// use RecuirringEventComposer:
				/*	RecurringEventComposer rc = new RecurringEventComposer(re, true, null, beginDate, Integer.MAX_VALUE);
					rc.processRecurringEvent();
					boolean endsBefore = rc.endsBefore();
					if (endsBefore == true) {
						//removeEvent(re);
						atRestEventList.add(re);
					} */

				} else { // not recurring
					
					// Several days event
					if (e.isSeveralDays()) {
						if (eventStartDate.isBefore(beginDate)) { 
							// check if event end reaches end of period
							if (eventEndDate.isBefore(beginDate) ) { // DTEND exclusive
								atRestEventList.add(e);
								TestLog.v(getClass(), "Add at rest: " + e.getSummary());								
							} else { // event end before beginDateTime
								// do nothing
							} 
						} else { 
							// do nothing
						}
						
					} else { // not recurring, not several days, start before period end
						if (eventStartDate.isBefore(beginDate)) { // need to check
							if (eventEndDate != null && eventEndDate.isAfter(beginDate) ) { // DTEND exclusive
								// do nothing
							} else {
								atRestEventList.add(e);								
							}
						} else {  // no need to check
							// do nothing
						}
					}
				}
			} else {
				// start date is after: do not include
				continue;
			}
		}
		// Now remove these events from the current list (do not remove before - index is used):
		for (PEvent removeE : atRestEventList) {
			this.eventList.remove(removeE);
		}
		
		// PTodos:
		// usually a todo is removed when no longer in use:
		// store at rest only todos with due date and status completed
		int todoSize = todoList.size();
		// resulting todo list
		ArrayList<PTodo> atRestTodoList = new ArrayList<PTodo>();
		for (int todoListIndex = 0; todoListIndex < todoSize; todoListIndex++) {			
			PTodo t = todoList.get(todoListIndex);
			if ( t != null) {
				if (t.getDue() != null && t.getStatus() != null && t.getStatus().equals("COMPLETED")) { // need to check
					if (t.getDue().toLocalDate().isAfter(beginDate)) {
						// ok
					} else {									
						atRestTodoList.add(t);
					}
				} 
			} else {
				TestLog.ve(getClass(), "Todo is null", 5);
			}
		}
		// Now remove todos from list:
		for (PTodo removeT : atRestTodoList) {
			this.todoList.remove(removeT);
		}
		
		// Create the at rest calendar with events and todos:
		if (atRestEventList.isEmpty() == false || atRestTodoList.isEmpty() == false) {
			// file name set for a new Calendar: 
			HashSet<String> atRestSet = new HashSet<String>();
			atRestSet.add(fileNameWithPath);
			PCalendar restCal = new PCalendar(this.name + "_until_" + beginDate.toString(), this.description, this.color, atRestSet);
			restCal.setEventList(atRestEventList);
			restCal.setTodoList(atRestTodoList);
			TestLog.v(getClass(), (atRestEventList.size() + atRestTodoList.size()) + " events and todos to store at rest from " + this.name);
			return restCal;
		} else {
			TestLog.v(getClass(), "There are no events or todos to store at rest for " + this.name);
			return null;
		}
	}	
	
	/**
	 * The events between a time period of days, separated in days
	 * 
	 * @param beginDate		the start date of the time period, including this date
	 * @param numberOfDays	the number of requested days
	 * 
	 * @return	an ArrayList of daily ArrayLists of events
	 */
	public ArrayList<ArrayList<PEvent>> getEventsOfDays(
			LocalDate beginDate, int numberOfDays){
		
		if (eventList.size() == 0) {
			return null;
		}
	
		//LocalDate lastDate = beginDate.plusDays(numberOfDays);// inclusive this date
		LocalDate endDateExclusive = beginDate.plusDays(numberOfDays); // exclusive this date
		//		TestLog.o(XXXX.class, "beginDate: " + beginDate +", endDateExcl: " + endDateExclusive + ", numberOfDays: " + numberOfDays);
		
		// to check "before": subtract one second to include events on time 00:00:00		
		LocalDateTime beginDateTime = beginDate.atStartOfDay();//.minusSeconds(1);
		LocalDateTime endDateTimeExclusive = endDateExclusive.atStartOfDay();// exclusive
		
		// One item in this list is one day
		ArrayList<ArrayList<PEvent>> resultLists = new ArrayList<ArrayList<PEvent>>(numberOfDays);

		// Create ArrayLists and add
		for (int day = 0; day < numberOfDays; day++) {
			// this holds events for one day separated in three lists: one day, several day and recurring
			resultLists.add(new ArrayList<PEvent>());
		}
		
		//TestLog.o(XXXX.class, "Get events between " + beginDateTime + " (inclusive) and " + endDateTimeExclusive.minusSeconds(1) + " (inclusive)");
	
		// check the events and add to lists
		int size = eventList.size();
		for (int eventListIndex = 0; eventListIndex < size; eventListIndex++) {
			
			PEvent e = eventList.get(eventListIndex);
			
			LocalDateTime eventStartDateTime = e.getDateStart();
			LocalDateTime eventEndDateTime = null;
			if (e.getDateEnd() != null) { // recurring event
				eventEndDateTime = e.getDateEnd();
			}

			if ( eventStartDateTime.isBefore(endDateTimeExclusive)) {

				// Recurring event: check dates only
				if (e.isRecurring()) {
					RecurringPEvent re =  null;
					if (e instanceof RecurringPEvent) {
						re = (RecurringPEvent) e;
					} else {
						re = RecurringPEvent.castToRecurringPEvent(e);
					}
					new RecurringEventComposer(re, //false, 
							resultLists, beginDate, numberOfDays).processRecurringEvent();
					

				} else { // not recurring, event start is before endDateTime
						// ( eventList.get(eventListIndex).getDateStart().isBefore(endDateTime))

					// Several days event
					if (e.isSeveralDays()) {

						if (eventStartDateTime.isBefore(beginDateTime)) { 
							// check if event end reaches end of period
							if (eventEndDateTime.isAfter(beginDateTime) ) { // DTEND exclusive
								// The "DTEND" property for a "VEVENT" calendar component specifies the non-inclusive end of the event.								
								// add this event to list
								LocalDateTime dayStartToCheck = beginDateTime;
								int dayIndex = 0;
								while(dayStartToCheck.isBefore(eventEndDateTime)) {
									// add to severalList of this day:
									resultLists.get(dayIndex).add(e);
									dayStartToCheck = dayStartToCheck.plusDays(1);
									dayIndex++;
									if (! dayStartToCheck.isBefore(endDateTimeExclusive)) {
										break;
									}
								}	
							} else { // event end before beginDateTime
								continue;
							} 
						} else { //if (eventStartDateTime.isAfter(beginDateTime)
								 //	|| eventStartDateTime.equals(beginDateTime)) {
							// get first day to add = index in resultList
							int resultListIndex = (int) ChronoUnit.DAYS.between(beginDateTime, eventStartDateTime);
							LocalDateTime checkDateTime = eventStartDateTime.plusDays(0);// copy to process
							int additionalDays = 0;
							LocalDateTime eventEnd = e.getDateEnd();
							while (checkDateTime.isBefore(endDateTimeExclusive)) {
								resultLists.get(resultListIndex + additionalDays).add(e);// add to rcurringList
								checkDateTime = checkDateTime.plusDays(1);
								// check if this at dayStart is after DTEND
								if (eventEnd != null && ( ! checkDateTime.withHour(0).withMinute(0).isBefore(eventEnd) ) ) {
									break;
								}
								additionalDays++;
							}
						}
					} else { // not recurring, not several days, start before period end
						// ( eventList.get(eventListIndex).getDateStart().isBefore(endDateTime))
						if (eventStartDateTime.isBefore(beginDateTime)) {
							continue;// not several days: end date is also before beginDateTime
						} else {  // add once
							// get index of day in resultLists
							int resultListIndex = (int) ChronoUnit.DAYS.between(beginDateTime, eventStartDateTime) ;
							resultLists.get(resultListIndex).add(e);
						}
					}
				}
			}			
		}
		return resultLists;		
	}
	/**
	 * Get all file names, this calendar was loaded from
	 * and where updates are to be stored
	 * 
	 * @return	the list of file names associated with this calendar
	 */
	public HashSet<String> getAssociatedFileNamesAsSet() {
		return associatedFileNames;
	}
	/**
	 * Get all file names, this calendar was loaded from
	 * and where updates are to be stored
	 * 
	 * @return	an array of file names associated with this calendar
	 */
	public String[] getAssociatedFileNames() {
		return associatedFileNames.toArray(new String[associatedFileNames.size()]);
	}
	/**
	 * Set all file names, this calendar was loaded from
	 * and where updates are to be stored
	 *  
	 * @param associatedFileNames 	the list of file names associated with this calendar
	 */
	public void setAssociatedFileNames(HashSet<String> _filesOfCalendar) {
		this.associatedFileNames = _filesOfCalendar;
	}
	/**
	 * Add one file name to the file names, this calendar was loaded from
	 * and where updates are to be stored
	 *  
	 * @param _fileNameToAdd 	file name to add to list of files of this calendar
	 */
	public void addAssociatedFileName(String _fileNameToAdd) {
		this.associatedFileNames.add(_fileNameToAdd);
	}

	// this class is used to find duplicate events from different calendars
	private class UIDComparator implements Comparator<CalendarComponent> {

		@Override
		public int compare(CalendarComponent e1, CalendarComponent e2) {
			// compare the UID instead of the date start
			return e1.getUid().compareTo(e2.getUid());
		}
	}
}
