package start;

/* Automatically produced file */
/* This class is newly created for each PEA */

import java.io.*;
import javax.swing.JOptionPane;
import cologne.eck.all_peas.data.*;
import cologne.eck.all_peas.vm_specific.JREBridge;
import cologne.eck.tools.Zeroizer;


public class Start {

private static final String CLASS_NAME = "calendar_pea/control/CalendarPEAControl";
private static final String JAR_FILE_NAME = "calendar";
private static int requiredJavaVersion = 8;
private static Runtime run = Runtime.getRuntime();

public static void main(String[] args) {

   System.out.println("Starting PEA " + JAR_FILE_NAME +" by new process...");
   // check java version:
   // Show error dialog only, when version is definitely to low 
   PeaProperties.setVmBridge(new JREBridge());
   String jvString = null;
   try {
      jvString = System.getProperty("java.version");
   } catch (Exception e) {
      System.err.println( "Could not check java version... " );
   }

  try {
     int javaVersion = 0;
     if (jvString.contains(".")) { //old format 1.X or Windows e.g. 16.0.1 
       String[] splitted = jvString.split("\\.");
       if (splitted.length > 1) {
         if (Integer.parseInt(splitted[0]) == 1) { // old version 1.X.X 
           javaVersion = Integer.parseInt(splitted[1]);// get second 
         } else {  
         javaVersion = Integer.parseInt(splitted[0]); 
       } 
    } else { 
       javaVersion = Integer.parseInt(jvString); 
    } 
  } else { // new format from from java 9 
    javaVersion = Integer.parseInt(jvString); 
  }
  if (javaVersion < requiredJavaVersion) { 
    JOptionPane.showMessageDialog(null,
    "Wrong java version. Required java version: " + requiredJavaVersion + ", available: " + javaVersion  + "   ( " + System.getProperty("java.version") +  " ) ",
    null, JOptionPane.ERROR_MESSAGE);
    System.exit(1); }
} catch (Exception e) {
   System.err.println( "Could not check java version: " + System.getProperty("java.version"));
   System.err.println( "Warning: The used version may be inappropriate..." + jvString);
}

// check if there is an instance of this program already running:
boolean warn = false;
// check for windows and unix:
String OS = System.getProperty("os.name").toLowerCase();

String procs = "";
if (OS.contains("windows")){
  String line;
//    boolean startProcConsidered = false;
  try { // read processes, look for this PEA 
    Process procCheckWin = Runtime.getRuntime().exec("wmic PROCESS where \"name like '%java%'\" get Processid,Caption,Commandline");
    BufferedReader inputCheckWin = new BufferedReader(new InputStreamReader(procCheckWin.getInputStream()));
    while ((line = inputCheckWin.readLine()) != null) {
      if(line.contains(CLASS_NAME)){
        procs = procs + line + "\n";
             warn = true;
             System.out.println( "instance already running:" + procs);
             break;
      }
    }
    inputCheckWin.close();
  } catch (IOException ioe) {
JOptionPane.showMessageDialog(null, ioe.toString() + " - check process failed");
    ioe.printStackTrace();
}
} else { // should work for most Unix: Linux, BSD, Solaris, Mac OS except OS X 
  try {//  
    Process checkPro = run.exec("pgrep -f "+ JAR_FILE_NAME + ".jar");
    checkPro.waitFor();
    BufferedReader buffCheck = new BufferedReader(new InputStreamReader(checkPro.getInputStream()));
    buffCheck.readLine(); // this is always existent: the ID of this process
    int checkProID = Integer.parseInt(buffCheck.readLine()); // next line: there should be no ID
    if( checkProID > 100){
      System.out.println("" + checkProID);
      procs = procs + "Process: " + checkProID + "\n";
      warn = true;
      System.out.println( "instance already running");
       buffCheck.close();
    }
  } catch (NumberFormatException e) { 
    // thats ok, there is no such process
  } catch (Exception e) { 
JOptionPane.showMessageDialog(null, e.getMessage() + " - check process failed");
    //... this will be thrown for rarely used systems with no pgrep
  }
}

if(warn == true){ 
  Object[] options = {PeaProperties.getVmBridge().translate("start_nevertheless"), PeaProperties.getVmBridge().translate("cancel")};
  int n = JOptionPane.showOptionDialog(null,
    PeaProperties.getVmBridge().translate("running_instance") + "\n" + procs,
    PeaProperties.getVmBridge().translate("warning"), JOptionPane.YES_NO_OPTION, JOptionPane.WARNING_MESSAGE, null, 
    options, options[1]);
  if(n != 0) { System.exit(0);}
}

//check command line mode:
boolean containsFiles = false;
boolean containsPassword = false;
boolean addPassword = false;
char[] psw = null;
if (args.length > 0) {
	
	for(int i = 0; i < args.length; i++) {
		String s = args[i];
		// check if passphrase is present:
		if (s.equals("--passphrase") || s.equals("--password")) {
			containsPassword = true;
		}
		if (s.equals("--files") ) {
			containsFiles = true;
		}
	}
	if (containsFiles == true && containsPassword == false) {		
		Console console = System.console();
		if (console != null) {
			try {
				//TestLog.o(CommandLineHandler.class, "Enter the password:");
				psw = console.readPassword(PeaProperties.getVmBridge().translate("enter_password"));
				addPassword = true;
			} catch (Exception e) {
				System.err.println( e.getLocalizedMessage() + " - " + e.toString());
			}
		}
	}
}

String[] commands = new String[args.length + 4 + ((addPassword == true)? 2 : 0)];
commands[0] = "java";
commands[1] = "-cp";
commands[2] = JAR_FILE_NAME + ".jar";
commands[3] = "cologne.eck." + CLASS_NAME;System.arraycopy(args, 0, commands, 4, args.length);
if (addPassword == true) {
	commands[commands.length - 2] = "--passphrase";
	if (psw == null) { // null password
		commands[commands.length - 1] =  "\"\"";
	} else {
		commands[commands.length - 1] =  "\"" + new String(psw) + "\"";
		Zeroizer.zero(psw);
	}
}


try {
  // run the program
   ProcessBuilder pb = new ProcessBuilder();
   @SuppressWarnings("unused")
Process p = pb.inheritIO().command(commands).start();
   System.exit(0);
} catch (IOException ie) {
  JOptionPane.showMessageDialog(null,
  "Execution failed."
  + ie.toString(),
  null,JOptionPane.ERROR_MESSAGE);
  ie.printStackTrace();
  System.exit(0);
} catch (Exception ire) { // InterruptedException  //for terminal only
  JOptionPane.showMessageDialog(null,
  "Execution failed."
  + ire.toString(),
  null,JOptionPane.ERROR_MESSAGE);
  ire.printStackTrace();
  System.exit(0);
} finally{    System.exit(0);
}}}
