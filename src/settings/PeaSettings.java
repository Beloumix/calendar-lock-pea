package settings;// TODO remove when exporting

import java.nio.charset.Charset;

import org.bouncycastle.crypto.BlockCipher;
import org.bouncycastle.crypto.Digest;
import org.bouncycastle.crypto.digests.*;
import org.bouncycastle.crypto.engines.*;

import cologne.eck.all_peas.data.PeaProperties;
import cologne.eck.peafactory.crypto.kdf.*;

/*
 * This class is only used by Peafactory internally
 * and is not modified by FileModifier
 * 
 */
public class PeaSettings {

	
	//private static final String CLASS_NAME = "cologne.eck.image_pea.ImageControl";
	//private static final String FILE_TYPE = "image";
	
	//private static final String CLASS_NAME = "cologne.eck.note_pea.EditorControl";
	//private static final String FILE_TYPE = "text file";
	
	private static final String CLASS_NAME = "cologne.eck.calendar_pea.control.CalendarPEAControl";
	private static final String FILE_TYPE = "calendar";
	
	//private static final String CLASS_NAME = "cologne.eck.contact_pea.control.ContactPEAControl";
	//private static final String FILE_TYPE = "contact";	
	
	//private static final String CLASS_NAME = "cologne.eck.file_pea.FileControl";
	//private static final String FILE_TYPE = "file";
	
	private static boolean keyFileProperty = false;
	private static final String EXTERNAL_FILE_PATH = null;
	private static final String JAR_FILE_NAME = "default";
	private static final String LABEL_TEXT = null;
	
	private static final byte[] PROGRAM_RANDOM_BYTES = "PEAFACTORY - PRODUCTION OF PASSWORD ENCRYPTED ARCHIVES - FIXED STRING USED TO INITIALIZE THE SALT FOR THE KEY DERIVATION FUNCTION"
			.substring(0, KeyDerivation.getSaltSize() ).getBytes(Charset.forName("US-ASCII"));
	private static final byte[] FILE_IDENTIFIER = "PEA-VE-2".getBytes(Charset.forName("US-ASCII"));
	private static final int PEA_VERSION = 2;
	
	private static final BlockCipher CIPHER_ALGO = new ThreefishEngine(512);
	private static final Digest HASH_ALGO = new Blake2bDigest();
	private static final KeyDerivation KDF_SCHEME = new CatenaKDF();	
	private static final int ITERATIONS = CatenaKDF.getLambdaDragonfly();//2
	private static final int MEMORY = CatenaKDF.getGarlicDragonfly();//18;
	private static final int PARALLELIZATION = 0;
	private static final String VERSION_STRING = "Dragonfly-Full";
	

	public final static Digest getHashAlgo() { 
		return HASH_ALGO; 
	}
	public final static BlockCipher getCipherAlgo() { 
		return CIPHER_ALGO; 
	}
	public final static KeyDerivation getKdfScheme() { 
		return KDF_SCHEME; 
	}
	public final static int getIterations() { 
		return ITERATIONS; 
	}
	public final static int getMemory() { 
		return MEMORY; 
	}
	public final static int getParallelization() { 
		return PARALLELIZATION; 
	}
	public final static String getVersionString() { 
		return VERSION_STRING; 
	}
	public final static byte[] getProgramRandomBytes() { 
		return PROGRAM_RANDOM_BYTES; 
	}
	public final static byte[] getFileIdentifier() { 
		return FILE_IDENTIFIER; 
	}
	public final static String getJarFileName() { 
		return JAR_FILE_NAME; 
	}
	public final static String getLabelText() { 
		return LABEL_TEXT; 
	}
	public final static boolean getKeyFileProperty() { 
		return keyFileProperty; 
		}
	public final static void setKeyFileProperty(boolean prop) { 
		if(PeaProperties.isTestMode() == true){ 
			keyFileProperty = prop; 
		}
	}
	public final static String getExternalFilePath() { 
		return EXTERNAL_FILE_PATH; 
	}
	public final static int getPEA_VERSION() { 
		return PEA_VERSION; 
	}
	public final static String getCLASS_NAME() { 
		return CLASS_NAME; 
	}
	public final static String getFILE_TYPE() { 
		return FILE_TYPE; 
	}
}
