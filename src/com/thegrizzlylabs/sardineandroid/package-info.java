/**
 * https://github.com/thegrizzlylabs/sardine-android
 * https://github.com/thegrizzlylabs/sardine-android/tree/master/src/main/java/com/thegrizzlylabs/sardineandroid
 *  on 4 Mar 2020
 *  
 *  Modifications:
 *  Replace android.text.TextUtils in OkHttpSardine
 *          //if (!TextUtils.isEmpty(lockToken)) {
 *       if (lockToken != null && lockToken.length() > 0) {
 *       
 *  Replace android.util.Log in ResourcesResponseHandler
 *          //Log.w(TAG, String.format("Ignore resource with invalid URI %s", davResponse.getHref()));
 *       System.out.println(String.format("Ignore resource with invalid URI %s", davResponse.getHref()));
 *       
 *  Comment out android build versions in SardineUtil
 *      public static Charset standardUTF8() {
       // if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.KITKAT) {
       //     return StandardCharsets.UTF_8;
       // } else {
            return Charset.forName("UTF-8");
       // }
    }
    
    Remove Companion from later okhttp version in DavResource
            //return StatusLine.Companion.parse(response.getStatus()).code;
         return StatusLine.parse(response.getStatus()).code;
 *
 * 
 * Download  19.02.2021
 * 
 * External jars:
 * 
 * okhttp-3.14.0.jar  okhttp version without Kotlin
 * from
 * https://repo1.maven.org/maven2/com/squareup/okhttp3/okhttp/3.14.0/
 * 
 * Needs javax.annotations
 * https://repo1.maven.org/maven2/com/google/code/findbugs/jsr305/3.0.2/
 * 
 * Needs org.codehaus.mojo.animal_sniffer.IgnoreJRERequirement.java
 * https://github.com/mojohaus/animal-sniffer/blob/master/animal-sniffer-annotations/src/main/java/org/codehaus/mojo/animal_sniffer/IgnoreJRERequirement.java
 * 
 * Deleted 2 classes: 
 * okhttp3.internal.platform.AndroidPlatform
 * okhttp3.internal.platform.ConscryptPlatform
 * Commented out: okhttp3.internal.platform.Platform
 * line 202-214
 * 
 * okio-1.17.5.jar
 * from: 
 * https://repo1.maven.org/maven2/com/squareup/okio/okio/1.17.5/
 * 
 * simple-xml-2.7.1.jar
 * from:
 * http://simple.sourceforge.net/download.php
 *  Last Update: 2015-09-16 
 *  Deleted: because of extra dependencies
 *  org.simpleframework.xml.stream.PullProvider.java
 *  org.simpleframework.xml.stream.PullReader.java
 *  Modified:
 *  org.simpleframework.xml.stream.ProviderFactory.java
 *  line 50
 *  return null; instead of return new PullProvider();
 * 
 */
/**
 * @author axl
 *
 */
package com.thegrizzlylabs.sardineandroid;
